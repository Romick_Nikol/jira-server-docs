---
aliases:
- /server/jira/platform/preparing-for-jira-7.1-38438396.html
- /server/jira/platform/preparing-for-jira-7.1-38438396.md
category: devguide
confluence_id: 38438396
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=38438396
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=38438396
date: '2017-12-08'
legacy_title: Preparing for JIRA 7.1
platform: server
product: jira
subcategory: updates
title: Preparing for JIRA 7.1
---
# Preparing for JIRA 7.1

This page covers changes in **JIRA 7.1** that can affect add-on compatibility and functionality. This includes changes to the JIRA platform and the JIRA applications (JIRA Core, JIRA Software, JIRA Service Desk). 

As a rule, Atlassian makes every effort to inform add-on developers of known API changes **as far in advance as possible**. Where possible, we attach release targets. Unless otherwise indicated, every change on this page is expected to be released with the first public release of the JIRA Server 7.1 products. We also make release milestones available prior to the release:

-   For JIRA Server, the JIRA development team releases a number of EAP milestones prior to the final release, for customers and developers to keep abreast of upcoming changes. For more information on these releases, see [JIRA EAP Releases](/server/jira/platform/jira-eap-releases-35160569.html).
-   For JIRA Cloud, the JIRA team releases updates to the JIRA Cloud products on a weekly basis. Add-ons that integrate with the JIRA Cloud products via Atlassian Connect should use the JIRA REST API, which is subject to the <a href="https://developer.atlassian.com/display/HOME/Atlassian+REST+API+policy" class="external-link">Atlassian REST API Policy</a>.

We will update this page as development progresses, so please stay tuned for more updates. We also recommend following the JIRA news on [Atlassian Developers blog](https://developer.atlassian.com/blog/categories/jira/) for important announcements and helpful articles.

## Summary

The risk level indicates the level of certainty we have that things will break if you are in the "Affected" column and you don't make the necessary changes.

<table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<thead>
<tr class="header">
<th>Change. Platform/Application</th>
<th>Risk level. Affects</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="#webhooks-for-comments">Change: Webhooks for comments</a></p>
<p>Platform/Application: JIRA platform</p></td>
<td><p>Risk level: N/A</p>
<p> Affects: N/A <em>(new feature)</em></p></td>
</tr>
<tr class="even">
<td><p><a href="#conditions-to-check-for-jira-applications">Change: Conditions to check for JIRA applications</a></p>
<p>Platform/Application: JIRA platform</p></td>
<td><p>Risk level: N/A</p>
<p> Affects: N/A <em>(new feature)</em></p></td>
</tr>
<tr class="odd">
<td><p><a href="#set-entity-properties-on-issue-creation">Change: Set entity properties on issue creation</a></p>
<p>Platform/Application: JIRA platform</p></td>
<td><p>Risk level: N/A</p>
<p> Affects: N/A <em>(new feature)</em></p></td>
</tr>
<tr class="even">
<td><p><a href="#new-atlas-run-cloud-command">Change: New atlas-run-cloud command</a></p>
<p>Platform/Application: JIRA platform*</p></td>
<td><p>Risk level: N/A</p>
<p> Affects: N/A <em>(new feature)</em> </p></td>
</tr>
<tr class="odd">
<td><p><a href="#new-asynchronous-startup">Change: New asynchronous startup</a></p>
<p>Platform/Application: JIRA platform</p></td>
<td><p>Risk level: Low</p>
<p> Affects: Functional test suites</p></td>
</tr>
<tr class="even">
<td><p><a href="#new-create-board-method">Change: New 'Create board' method</a></p>
<p>Platform/Application: JIRA Software</p></td>
<td><p>Risk level: N/A</p>
<p> Affects: N/A <em>(new feature)</em></p></td>
</tr>
<tr class="odd">
<td><p>Change: <a href="#webhooks-for-agile-boards">Webhooks for Agile boards</a></p>
<p>Platform/Application: JIRA Software</p></td>
<td><p>Risk level: N/A</p>
<p> Affects: N/A <em>(new feature)</em></p></td>
</tr>
<tr class="even">
<td><p><a href="#new-jira-service-desk-rest-api">Change: New JIRA Service Desk REST API</a></p>
<p>Platform/Application: JIRA Service Desk</p></td>
<td><p>Risk level: N/A</p>
<p> Affects: N/A <em>(new feature)</em></p></td>
</tr>
</tbody>
</table>

<sup>\*\ This\ is\ a\ JIRA\ platform\ feature,\ but\ it\ currently\ only\ works\ for\ JIRA\ Software.</sup>

## JIRA platform changes

As the JIRA applications are built on the JIRA platform, the changes in this section affect all JIRA products.

#### Webhooks for comments

You can now register comment-related events for a webhook in JIRA Core. This lets JIRA Core notify your add-ons or web applications whenever a comment is created, updated, or deleted. Just choose the desired events when creating or updating your webhook. Read the [webhooks documentation](https://developer.atlassian.com/display/JIRADEV/Webhooks) for details. 

#### Conditions to check for JIRA applications

If you are developing an [Atlassian Connect](https://developer.atlassian.com/static/connect/docs/latest/index.html) add-on, you can now set the `can_use_application` condition to detect whether a particular JIRA application is licensed and active for the current user. This For example, the following condition would check whether JIRA Software is installed and enabled, and whether the current user can use it according to the application license:

``` javascript
{
    "condition": "can_use_application",
    "params": {
        "applicationKey": "jira-software"
    }
}
```

#### Set entity properties on issue creation

Entity properties are used to add key/value stores to JIRA entities, like issues and projects. Previously, it was only possible to add entity properties to an issue entity after the issue has already been created. In this release, you can now add these properties when creating an issue (REST API only), e.g.

``` javascript
{
    "fields": {
       "project":
       {
          "key": "TEST"
       },
       "summary": "Example issue",
       "description": "This is an example issue",
       "issuetype": {
          "name": "Bug"
       }
    }
    "properties": [
        "propertykey" : {}, // valid JSON object here
        "secondPropertykey" : {} // another valid JSON object here
    ]
}
```

For more information on entity properties in JIRA, see [JIRA Entity Properties overview](/server/jira/platform/entity-properties).

#### New asynchronous startup

By default, JIRA now displays a progress page during its startup instead of holding incoming requests. Most add-on developers should not be directly affected by this change, but test suites that do not expect any response from JIRA at all until it is fully initialized may be confused by these responses. Options for dealing with this include:

-   Manually poll the context path with `curl` or something similar until you are no longer redirected to `startup.jsp`
-   Use `jira-testkit` versions `7.0.97` and later, which will recognize the startup page and retry transparently if you use it to perform a data restore or verify that JIRA `isSetup()`
-   Run JIRA with `-Dcom.atlassian.jira.startup.LauncherContextListener.SYNCHRONOUS=``true` to disable this feature 
-   Use AMPS  6.0.9, 6.1.7, 6.2.2, or later, which will disable the asynchronous startup for you.

#### New atlas-run-cloud command

If you are building Connect add-ons, you'll be happy to know that we've added a new `atlas-run-cloud` command. This command can be used to start a local Cloud instance of an Atlassian application (latest version). It currently only works for JIRA Software, but we'll be adding other JIRA products to it in future. If you want to try it out, just run the following command:

`atlas-run-cloud --application jira-software`

For more information on it, see [atlas-run-cloud](https://developer.atlassian.com/display/DOCS/atlas-run-cloud).

## JIRA Core changes

There are no JIRA Core-specific changes.

## JIRA Software changes

These changes are specific to the JIRA Software application and do not affect the other JIRA products.

#### New 'Create board' method

The JIRA Software REST API now has a 'Create board' method. Check it out in the <a href="https://docs.atlassian.com/greenhopper/REST/cloud/index.html#agile/1.0/board" class="external-link">REST API documentation</a>.

#### Webhooks for Agile boards

You can now register board-related events for a webhook in JIRA Software. This lets JIRA Software notify your add-ons or web applications whenever a board is created, updated, deleted, or has its configuration changed. Just choose the desired events when creating or updating your webhook. Read the [webhooks documentation](/server/jira/platform/webhooks) to learn more.

## JIRA Service Desk changes

These changes are specific to the JIRA Service Desk application and do not affect the other JIRA products.

#### New JIRA Service Desk REST API

JIRA Service Desk now has a public REST API. Need to get the issues in a service desk queue? Or add a comment to a customer request? The new REST API has a range of resources for interacting with JIRA Service Desk.  
The JIRA Service Desk REST API is currently experimental, but will eventually become stable (see our REST API policy for more details). Check it out below:

-   <a href="https://docs.atlassian.com/jira-servicedesk/REST/latest/" class="external-link">JIRA Service Desk REST API</a>
