---
aliases:
- /server/jira/platform/tutorial-smarter-integration-testing-with-testkit-13632862.html
- /server/jira/platform/tutorial-smarter-integration-testing-with-testkit-13632862.md
category: devguide
confluence_id: 13632862
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=13632862
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=13632862
date: '2017-12-08'
guides: tutorials
legacy_title: Tutorial - Smarter integration testing with TestKit
platform: server
product: jira
subcategory: learning
title: Smarter integration testing with TestKit
---
# Smarter integration testing with TestKit

{{% tip %}}

Level of experience: Intermediate

Our tutorials are classified as 'beginner', 'intermediate' and 'advanced'. This one is at 'intermediate' level. If you have never developed a plugin before, you may find this one a bit difficult.

{{% /tip %}}

{{% warning %}}

TestKit should not be used on production servers, as it creates an administrator-level "backdoor" into JIRA.

{{% /warning %}}

The source code of the plugin used in this tutorial is available in the Atlassian public source repository. You can get the source code from <a href="https://bitbucket.org/atlassian_tutorial/jira-testkit-example" class="uri external-link">bitbucket.org/atlassian_tutorial/jira-testkit-example</a>.

## Making JIRA ready for tests

Have you ever wondered how to set up complex test scenarios in JIRA? For example add multiple user accounts, groups and permissions, and check if your plugin correctly handles security constraints?

Have you ever been tired preparing different XML dumps for each configuration?

To solve problems like that you can use TestKit, our addition to test infrastructure that helps developers write integration tests with ease.

We've been using TestKit internally in JIRA for some time and as it matured and covered more and more features, we decided that it's time to share this with the ecosystem.

In the following document, we'll help you set up your plugin to use TestKit, and show you how to use it with some examples.

## Benefits of using TestKit instead of FuncTestCase

As you probably noticed, [FuncTestCase](/server/jira/platform/writing-integration-tests-for-your-jira-add-on#tests) provides a lot of administrative methods through built-in administration attribute. You can still use them. They are all web-based, meaning that when you want to add a user, FuncTestCase goes to the page, fills in the form, submits it, and so on.

Instead TestKit talks with JIRA using REST, exchanging only crucial data to fullfil the task. It's a lot faster. And if you have many tests, you will notice the time difference when switching from FuncTestCase to TestKit.

Also TestKit is actively maintained and you can expect new features coming in.

{{% note %}}

<a href="https://bitbucket.org/atlassian/jira-testkit" class="external-link">JIRA TestKit</a> is Open Source so you can easily modify it and adapt to your needs, also you can share your code with us or other developers.

{{% /note %}}

## Setting up the plugin

In your pom.xml search for maven-jira-plugin configuration, you need to extend it adding jira-testkit-plugin:

``` xml
<plugin>
    <groupId>com.atlassian.maven.plugins</groupId>
    <artifactId>maven-jira-plugin</artifactId>
    <configuration>
        <pluginArtifacts>
            <pluginArtifact>
                <groupId>com.atlassian.jira.tests</groupId>
                <artifactId>jira-testkit-plugin</artifactId>
                <version>${testkit.version}</version>
            </pluginArtifact>
        </pluginArtifacts>
[...]
```

Also add jira-testkit-client to your project dependencies, remember to use it only with test scope:

``` xml
<dependency>
    <groupId>com.atlassian.jira.tests</groupId>
    <artifactId>jira-testkit-client</artifactId>
    <version>${testkit.version}</version>
    <scope>test</scope>
</dependency>
```

{{% note %}}

Check JIRA <a href="https://bitbucket.org/atlassian/jira-testkit" class="external-link">TestKit repository</a> to see what's the current version and please set **testkit.version** property accordingly.

{{% /note %}}

## Using TestKit in your tests

You learned in the previous sections about writing integration tests. Here's an example of an integration tests using TestKit:

``` java
package it;
import com.atlassian.jira.functest.framework.FuncTestCase;
import com.atlassian.jira.testkit.client.Backdoor;
import com.atlassian.jira.testkit.client.util.TestKitLocalEnvironmentData;
import com.atlassian.jira.testkit.client.util.TimeBombLicence;

public class MyPluginTest extends FuncTestCase {
    @Override
    protected void setUpTest() {
        super.setUpTest();
        Backdoor testKit = new Backdoor(new TestKitLocalEnvironmentData());
        testKit.restoreBlankInstance(TimeBombLicence.LICENCE_FOR_TESTING);
        testKit.usersAndGroups().addUser("test-user");
        testKit.websudo().disable();
        testKit.subtask().enable();
    }

    public void testIntegration() {
        // Put your test logic here
    }
}
```

In the above example you can notice how easy is to use TestKit. You can use data loading methods to populate JIRA with different configurations, then you can customize it further. Here we load a blank instance, then add a user, disable websudo and enable sub-tasks.

There's more - as you browse through the API you'll notice methods for managing permission schemes, projects, application properties, mail servers, dashboards, etc.

{{% tip %}}

Go and play with it!

You should now be able to write integration tests for JIRA using TestKit. Oh and don't forget to have a chocolate!

{{% /tip %}}

