---
aliases:
- /server/jira/platform/38441060.html
- /server/jira/platform/38441060.md
category: devguide
confluence_id: 38441060
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=38441060
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=38441060
date: '2017-12-08'
legacy_title: JIRA Software 7.1 EAP 01 (m02) Release Notes
platform: server
product: jira
subcategory: updates
title: JIRA Software 7.1 EAP 01 (m02) Release Notes
---
# JIRA Software 7.1 EAP 01 (m02) Release Notes

**15 January 2016**

Atlassian is proud to present **JIRA Software 7.1 EAP 01 (m02**). This public development release is part of our [Early Access Program (EAP)](/server/jira/platform/jira-eap-releases-35160569.html) leading up to the official **JIRA 7.1** release. We are making these EAP milestones publicly available so that developers can start assessing the impact of the changes that we are making.

For the full list of changes for JIRA 7.1, read the developer change management guide: [Preparing for JIRA 7.1](/server/jira/platform/preparing-for-jira-7-1). If you are new to JIRA, you should also read our [Java API Policy for JIRA](/server/jira/platform/java-api-policy-for-jira-4227213.html) and the [Atlassian REST API policy](https://developer.atlassian.com/display/HOME/Atlassian+REST+API+policy).

[<img src="/server/jira/platform/images/download-eap-btn.png" class="image-center confluence-thumbnail" width="120" />](#downloads)

***[Upgrade notes (including known issues)](#upgrade-notes-including-known-issues)***

{{% warning %}}

Before you install/upgrade: Atlassian does not support upgrades both 'from' and 'to' EAP releases. EAP releases should not be used in production environments as they are not officially supported. For all production use and testing of JIRA, please use the <a href="https://confluence.atlassian.com/display/JIRASOFTWARE/JIRA+Software+7.0.x+release+notes" class="external-link">latest official release</a> instead.

{{% /warning %}}{{% note %}}

**We want your feedback!**

To provide feedback, create an issue in <a href="https://jira.atlassian.com/browse/JRA" class="external-link">our issue tracker</a> with the following details:

-   **Issue Type**: `Suggestion`
-   **Component**: `JIRA 7.1 EAP`

{{% /note %}}

## Epic change to epic selection

Doing work across multiple projects and teams means your list of epics can grow to epic proportions. That's why we've made it easier to find the epic you want to link to when creating or editing issues, and made searches for epic-linked issues much simpler.

#### Linking an issue to an epic

When you create or edit an issue and link it to an epic, you'll find that:

-   Done epics are now automatically excluded from the list unless you select them to appear
-   Open epics from your current project appear at the top of the list as suggestions
-   When you start typing an epic name the list results match what you type

#### <img src="/server/jira/platform/images/epics1.png" height="250" />

#### Issue navigator basic search

When you do a basic search for an issue in the issue navigator and select the **Epic Link** filter option:

-   Open and done epics appear alphabetically in the list
-   When you start typing, the list shows only matches for what you type

To show only epics from specific projects, refine the list using the **Project** filter.

<img src="/server/jira/platform/images/epics2.png" height="250" />

Just like before, you can also search for any issue with **No** **Epic Link**.

## Temporary removal of the Manage Sprints permission

The <a href="https://confluence.atlassian.com/display/Cloud/2015/12/04/JIRA+Software+sprint+management+-+first+release" class="external-link">Manage Sprints permission</a> is currently unavailable due to a number of issues. This feature will be reintroduced when the issues are resolved. Note, the Manage Sprints permission will still appear in the Permission Schemes page, but the permission will not be effective until we reintroduce the functionality. Until then, the sprint checks in JIRA Software will use the 'Administer Projects' permission, as they previously did.

## Webhooks for Agile boards

You can now register board-related events for a webhook in JIRA Software. This lets JIRA Software notify your add-ons or web applications whenever a board is created, updated, deleted, or has its configuration changed. Just choose the desired events when creating or updating your webhook. Read the [webhooks documentation](/server/jira/platform/webhooks) to learn more.

## New 'Create board' method

The JIRA Software REST API now has a 'Create board' method. Check it out in the <a href="https://docs.atlassian.com/greenhopper/REST/cloud/index.html#agile/1.0/board" class="external-link">REST API documentation</a>.

## JIRA Core changes that affect JIRA Software

All of the changes in the JIRA Core 7.1 EAP are also in the JIRA Software 7.1 EAP unless otherwise stated below. Read the [JIRA Core 7.1 EAP 01 (m01) Release Notes](/server/jira/platform/jira-core-7-1-eap-01-m01-release-notes) for details. 

Notes:

-   '[Viewing your versions](https://developer.atlassian.com/display/JIRADEV/JIRA+Core+7.1+EAP+01+%28m02%29+Release+Notes#JIRACore7.1EAP01(m02)ReleaseNotes-Viewingyourversions)' -- The changes described in this section can be found in the **Releases** page in the JIRA Software application.

## Upgrade notes (including known issues)

If you are upgrading from JIRA 6.3.x or earlier, read <a href="https://confluence.atlassian.com/display/JIRA/Skipping+Major+Versions+When+Upgrading+JIRA" class="external-link">Skipping Major Versions When Upgrading JIRA</a>. Remember that EAPs are **not supported** and **should not be used in production environments**.

Please also take note of the following information when installing or upgrading to this EAP release:

-   **Known issue: Estimates are not calculated for boards with 'Original Time Estimate' and 'Remaining Estimate and Time Spent' configured** (<a href="https://jira.atlassian.com/browse/GHS-13035" class="external-link">GHS-13035</a>)-- If your board to uses the 'Original Time Estimate' **estimation statistic** and 'Remaining Estimate and Time Spent' for **time tracking**, estimates will not be calculated throughout the project. That is, you won't see any estimates in the backlog icons (![](/server/jira/platform/images/image2016-1-15-12:2:13.png)), against versions in the backlog, in the 'Workload by assignee' dialog in the backlog, etc.  
    To work around this issue, do the following:
    1.  Navigate to **JIRA administration** &gt; **Issues** &gt; **Time tracking**  
        *Keyboard shortcut: g + g "time tracking"*
    2.  Deactivate time tracking.
    3.  In the time tracking settings, make sure that the **Time format** = 'pretty'
    4.  Re-activate time tracking.

## Downloads

-   <a href="http://www.atlassian.com/software/jira/downloads/binary/atlassian-jira-software-7.1.0-m02-jira-7.1.0-m02-x32.exe" class="external-link">JIRA Software 7.1.0-m02 (Windows 32 Bit Installer, EXE)</a>
-   <a href="http://www.atlassian.com/software/jira/downloads/binary/atlassian-jira-software-7.1.0-m02-jira-7.1.0-m02-x64.exe" class="external-link">JIRA Software 7.1.0-m02 (Windows 64 Bit Installer, EXE)</a>
-   <a href="http://www.atlassian.com/software/jira/downloads/binary/atlassian-jira-software-7.1.0-m02-jira-7.1.0-m02-x32.bin" class="external-link">JIRA Software 7.1.0-m02 (Linux 32 Bit Installer, BIN)</a>
-   <a href="http://www.atlassian.com/software/jira/downloads/binary/atlassian-jira-software-7.1.0-m02-jira-7.1.0-m02-x64.bin" class="external-link">JIRA Software 7.1.0-m02 (Linux 64 Bit Installer, BIN)</a>
-   <a href="http://www.atlassian.com/software/jira/downloads/binary/atlassian-jira-software-7.1.0-m02-jira-7.1.0-m02.zip" class="external-link">JIRA Software 7.1.0-m02 (ZIP Archive)</a>
-   <a href="http://www.atlassian.com/software/jira/downloads/binary/atlassian-jira-software-7.1.0-m02-jira-7.1.0-m02.tar.gz" class="external-link">JIRA Software 7.1.0-m02 (TAR.GZ Archive)</a>
