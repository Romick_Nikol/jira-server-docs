---
title: Frequently Asked Rpc Questions and Known Issues 4227103
aliases:
    - /server/jira/platform/frequently-asked-rpc-questions-and-known-issues-4227103.html
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=4227103
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=4227103
confluence_id: 4227103
platform: server
product: jira
category: devguide
subcategory:
date: "2018-04-25"
---
# JIRA Developer Documentation : Frequently Asked RPC Questions and Known Issues

{{% warning %}}

This page has been archived, as it does not apply to the latest version of JIRA (Server or Cloud). The functionality described on this page may be unsupported or may not be present in JIRA at all.

{{% /warning %}}

{{% note %}}

JIRA's SOAP and XML-RPC remote APIs were removed in JIRA 7.0 for Server ( [see announcement](https://developer.atlassian.com/display/JIRADEV/SOAP+and+XML-RPC+API+Deprecation+Notice)).
We encourage you to use JIRA's REST APIs to interact with JIRA remotely (see [migration guide](https://developer.atlassian.com/display/JIRADEV/JIRA+SOAP+to+REST+Migration+Guide)).

{{% /note %}}

This page contains frequently asked questions and known issues about RPC, in relation to JIRA.

## Common Problems

#### Couldn't build RPC Plugin due to NullPointerException

If you are getting a `NullPointerException` and a stack trace which looks similar with the one described [this document](/server/jira/platform/why-am-i-getting-exception-looking-up-public-key-exception-while-building-a-plugin.snippet), you may need to disable unit testing in `project.xml` for a smooth build, as mentioned in the [document](/server/jira/platform/why-am-i-getting-exception-looking-up-public-key-exception-while-building-a-plugin.snippet) referenced previously.

#### Getting InfrastructureException or ClassCastException upon invocation

If you are getting a `ClassCastException` or `com.atlassian.jira.InfrastructureException` upon invocation, you may have provided the incorrect data types for the arguments. To resolve this problem, please correct the data types of the arguments accordingly.

If this is not the case and this error is occurring, then it is highly likely that autotyping is the culprit. To resolve this problem, you will need to disable autotyping or alternatively, explicitly declare the data types for the arguments.

## SOAP

#### Converting to type as defined in WSDL

You may occasionally encounter problems mapping the correct data type of your programming language to the one defined in WSDL. If you are running into difficulties related to data mapping, we recommend that you consult the user guide or API documentation of the client library you are using, e.g. <a href="http://ws.apache.org/axis/java/user-guide.html#HowYourJavaTypesMapToSOAPXMLTypes" class="external-link">WSDL&lt;-&gt;Java mapping</a>, <a href="http://pywebsvcs.sourceforge.net/apidocs/SOAPpy/Types.html" class="external-link">Python's SOAPpy</a>, etc, for assistance.

#### Changing the authentication token timeout value

If you wish to change the timeout value of the authentication token (which is obtained once you've logged in), you will need to update the value for the
`DEFAULT_TIMEOUT` constant in the current `com.atlassian.jira.rpc.auth.TokenManager` implementation, i.e. `com.atlassian.jira.rpc.auth.TokenManagerImpl`.

#### Providing schemes to method's arguments

Users are often unsure whether they should construct or retrieve the scheme object(s) which will be passed to an argument. For your understanding, internally, the RPC plugin recognises just the `id` of the scheme, hence the `name`, `type`, `description`, etc. are usually insignificant.

#### Could not update an issue with a RemoteIssue object

Unfortunately, you can only **create an issue** with a RemoteIssue object. You cannot **update an issue** with a RemoteIssue object. The `createIssue()` method expects a `RemoteIssue` object as its argument, however the `updateIssue()` method expects an array of `RemoteFieldValue` objects (as per the WDSL).

#### Setting the value of cascading select customfield

Setting the value of a cascading select custom field can be complicated, as the create issue and update issue scenarios are different. Please refer to the points below for assistance in setting up your cascading select custom field correctly:

-   While creating an issue, the parent field is set by providing the `customfieldId` and `values` (single-element array with option's id) and the child field is set by providing the `customfieldId`, `key` (as `1`) and `values`.

<!-- -->

-   While updating an issue, the parent field is set by providing value (a single-element array with option's id) to the `customfieldId` and the child field is set by providing value to the `customfieldId:key` (`key` as `1`, e.g. `customfield_10000:1`).

The same logic applies to XML-RPC, [JIRA:please refer to this example](#could-not-create-and-update-issue-with-same-dictionary).

#### Could not invoke methods due to session invalidation

A known issue exists where methods may be prevented from being invoked due to session invalidation, i.e. an `IllegalStateException` occurs when trying to invoke the RPC methods:

    java.lang.IllegalStateException: getAttribute: Session already invalidated
        at org.apache.axis.message.SOAPFaultBuilder.createFault(SOAPFaultBuilder.java:221)
        at org.apache.axis.message.SOAPFaultBuilder.endElement(SOAPFaultBuilder.java:128)
        at org.apache.axis.encoding.DeserializationContext.endElement(DeserializationContext.java:1087)
        at com.sun.org.apache.xerces.internal.parsers.AbstractSAXParser.endElement(AbstractSAXParser.java:633)
        at com.sun.org.apache.xerces.internal.impl.XMLNSDocumentScannerImpl.scanEndElement(XMLNSDocumentScannerImpl.java:719)
        at com.sun.org.apache.xerces.internal.impl.XMLDocumentFragmentScannerImpl$FragmentContentDispatcher.dispatch(XMLDocumentFragmentScannerImpl.java:1685)
        at com.sun.org.apache.xerces.internal.impl.XMLDocumentFragmentScannerImpl.scanDocument(XMLDocumentFragmentScannerImpl.java:368)
        at com.sun.org.apache.xerces.internal.parsers.XML11Configuration.parse(XML11Configuration.java:834)
        at com.sun.org.apache.xerces.internal.parsers.XML11Configuration.parse(XML11Configuration.java:764)
        at com.sun.org.apache.xerces.internal.parsers.XMLParser.parse(XMLParser.java:148)
        at com.sun.org.apache.xerces.internal.parsers.AbstractSAXParser.parse(AbstractSAXParser.java:1242)
        at javax.xml.parsers.SAXParser.parse(SAXParser.java:375)
        at org.apache.axis.encoding.DeserializationContext.parse(DeserializationContext.java:227)
        at org.apache.axis.SOAPPart.getAsSOAPEnvelope(SOAPPart.java:696)
        at org.apache.axis.Message.getSOAPEnvelope(Message.java:424)
        at org.apache.axis.handlers.soap.MustUnderstandChecker.invoke(MustUnderstandChecker.java:62)
        at org.apache.axis.client.AxisClient.invoke(AxisClient.java:206)
        at org.apache.axis.client.Call.invokeEngine(Call.java:2754)
        at org.apache.axis.client.Call.invoke(Call.java:2737)
        at org.apache.axis.client.Call.invoke(Call.java:2413)
        at org.apache.axis.client.Call.invoke(Call.java:2336)
        at org.apache.axis.client.Call.invoke(Call.java:1793)
        at com.atlassian.jira.rpc.soapclient.JirasoapserviceV2SoapBindingStub.getFieldsForAction(JirasoapserviceV2SoapBindingStub.java:3414)

Please refer to the bug report being tracked at <a href="http://jira.atlassian.com/browse/JRA-8009" class="external-link">JRA-8009</a>. We would appreciate any comments added to this issue, if you encounter this problem.

#### Getting fixed targetNamespace

Getting a fixed targetNamespace is a known issue, which is being tracked at <a href="http://jira.atlassian.com/browse/JRA-10849" class="external-link">JRA-10849</a>. The `targetNamespace` of the WSDL is defined by the first request to it.

#### Having concurrency problem with multiple issue update

Constantly or simultaneously updating an issue causes data inconsistency of the issue. This is a known issue which is currently being tracked at <a href="http://jira.atlassian.com/browse/JRA-11382" class="external-link">JRA-11382</a>.

#### Could not invoke user-group methods due to deserialisation or invalid element problems

If you are using Python (SOAPpy) and encountering deserialisation or invalid element problems while invoking the `addUserToGroup()` method, e.g.

    <Fault soapenv:Server.userException: org.xml.sax.SAXException: No deserializer defined for array type {http://soapinterop.org/xsd}SOAPStruct:
    <SOAPpy.Types.structType detail at 1085258540>: {'hostname': 'atlassian01.contegix.com', 'faultData': <SOAPpy.Types.structType faultData at
    1084456684>: {'exception': None, 'message': 'No deserializer defined for array type {http://soapinterop.org/xsd}SOAPStruct'}}>
    Traceback (most recent call last):
    File "./soapclient.py", line 58, in ?
    soap.addUserToGroup(auth, group, user)

    SOAPpy.Types.faultType: <Fault soapenv:Server.userException: org.xml.sax.SAXException: Invalid element in com.atlassian.jira.rpc.soap.beans.
    RemoteGroup - email: <SOAPpy.Types.structType detail at 29991600>: {'hostname': 'bruwa116', 'faultData': <SOAPpy.Types.structType faultData
    at 29991680>: {'exception': None, 'message': 'Invalid element in com.atlassian.jira.rpc.soap.beans.RemoteGroup - email'}}>

you should be able to resolve this by following the workarounds described in these issues -- <a href="http://jira.atlassian.com/browse/JRA-7920" class="external-link">JRA-7920</a> and <a href="http://jira.atlassian.com/browse/JRA-7971" class="external-link">JRA-7971</a>

#### Using the addActorsToProjectRole method

If you are having problems using the `addActorsToProjectRole` method, you may find the following advice helpful:
The `JiraSoapService.addActorsToProjectRole()` method internally invokes the `ProjectRoleService.addActorsToProjectRole()` method. You must provide either an array of users or groups based on the actorType value, i.e. <a href="http://docs.atlassian.com/software/jira/docs/api/latest/constant-values.html#com.atlassian.jira.security.roles.actor.UserRoleActor.TYPE" class="external-link">UserRoleActor.TYPE</a> or <a href="http://docs.atlassian.com/software/jira/docs/api/latest/constant-values.html#com.atlassian.jira.security.roles.actor.GroupRoleActor.TYPE" class="external-link">GroupRoleActor.TYPE</a>.

For example,

``` javascript
jiraSoapService.addActorsToProjectRole(token, new String[]{"admin", "foo", "bar"}, projectRole, project, UserRoleActor.TYPE);
```

#### Could not upload attachments to an issue due to OutOfMemoryError

If your Java (Axis) SOAP client is throwing an `OutOfMemoryError` while attaching files to an issue, Axis 1.3 may be the cause. This known issue has been commonly reported by users and is being tracked at <a href="http://jira.atlassian.com/browse/JRA-11693" class="external-link">JRA-11693</a>.

#### Getting content-type complaint with .NET client

This seems to be the problem with the .NET web service library. We would appreciate your contributions to tracking issue, <a href="http://jira.atlassian.com/browse/JRA-11515" class="external-link">JRA-11515</a>, if you encounter a similar problem.

#### Setting the reporter value upon issue creation

If you are not able to set the reporter value (to someone other than the current SOAP client) even though it is provided, you may be using the old RPC plugin (3.6.x and older). This <a href="http://jira.atlassian.com/browse/JRA-8794" class="external-link">improvement (JRA-8794)</a> is delivered in the latest version (3.7.x onwards).

#### Other Known RPC Issues

Unfortunately there are a number of known RPC issues, which are currently unsupported. These have already been raised in <a href="http://jira.atlassian.com/secure/IssueNavigator.jspa?sorter/field=votes&amp;sorter/order=DESC" class="external-link">our issue tracker</a> and are listed below.

{{% note %}}

If you would like to try manually patching or customising the RPC plugin to address these issues yourself, the <a href="http://repository.atlassian.com/atlassian-jira-rpc-plugin/distributions/" class="external-link">source code of the RPC plugin</a> is freely available for you to modify.

{{% /note %}}

 

##### Logging work to an issue

Unfortunately, this is not supported at the moment. Feel free contribute to the issue at <a href="http://jira.atlassian.com/browse/JRA-7260" class="external-link">JRA-7260</a> with your comments and vote.

##### Updating user information

Unfortunately, this is not supported at the moment. Feel free contribute to the issue at <a href="http://jira.atlassian.com/browse/JRA-11252" class="external-link">JRA-11252</a> with your comments and vote.

##### Deleting version

Unfortunately, this is not supported at the moment. Feel free contribute to the issue at <a href="http://jira.atlassian.com/browse/JRA-11532" class="external-link">JRA-11532</a> with your comments and vote.

##### Creating subtask

Unfortunately, this is not supported at the moment. Feel free contribute to the issue at <a href="http://jira.atlassian.com/browse/JRA-6896" class="external-link">JRA-6896</a> with your comments and vote.

##### Changing status and resolution of an issue

The only way to set the status and resolution of an issue is by invoking the `progressWorkflowAction()` method. Feel free contribute to the issue at <a href="http://jira.atlassian.com/browse/JRA-10472" class="external-link">JRA-10472</a> with your comments and vote.

##### Posting comment while progressing workflow

Unfortunately, this is not supported at the moment. Feel free contribute to the issue at <a href="http://jira.atlassian.com/browse/JRA-11278" class="external-link">JRA-11278</a> with your comments and vote.

##### Setting comment "Viewable By" security

Unfortunately, this is not supported at the moment. Feel free contribute to the issue at <a href="http://jira.atlassian.com/browse/JRA-11278" class="external-link">JRA-11278</a> with your comments and vote.

##### Getting required and available fields for issue creation

Unfortunately, this is not supported at the moment. Feel free contribute to the issue at <a href="http://jira.atlassian.com/browse/JRA-11597" class="external-link">JRA-11597</a> with your comments and vote.

## XML-RPC

Please take note that a number of the frequently asked XML-RPC questions are similar to the respective SOAP questions above, as some of the functions of the XML-RPC service are underpinned by the SOAP service. Hence, the questions may not be mentioned again below.

#### Could not create and update issue with same Dictionary

If you are running into problems creating and updating issues with the same Directory, you may be specifying the arguments incorrectly. Internally, JIRA's <a href="http://docs.atlassian.com/software/jira/docs/api/rpc-jira-plugin/latest/com/atlassian/jira/rpc/xmlrpc/XmlRpcService.html" class="external-link">XmlRpcService</a> uses SOAP to create and update an issue. Due to this reason, the Dictionary (or Hashtable, or Map) argument in `createIssue()` and `updateIssue()` are different [JIRA:as explained above](#could-not-update-an-issue-with-a-remoteissue-object).

This Perl XMLRPC::Lite example demonstrates how an issue is created and updated:

``` javascript
# Create an issue with a RemoteIssue structure

$jira->call("jira1.createIssue", $auth, {
  "project" => "MYC",
  "type" => 1,
  "reporter" => "admin",
  "assignee" => "admin",
  "summary" => "Issue created via Perl XMLRPC :)",
  "customFieldValues" => [
    {"customfieldId" => "customfield_10000",
     "values" => [SOAP::Data->type(string => "10000")]
    },
    {"customfieldId" => "customfield_10000",
     "key" => "1",
     "values" => [SOAP::Data->type(string => "10002")]
    }
  ]
});

# Update an issue with field-values pairs

$jira->call("jira1.updateIssue", $auth, "MYC-3",
  {"customfield_10000" => [SOAP::Data->type(string => "10000")],
   "customfield_10000:1" => [SOAP::Data->type(string => "10002")]
  }
);
```

#### Setting the value of cascading select customfield

Please refer to the [JIRA:explanation in SOAP section](#setting-the-value-of-cascading-select-customfield).

## Need more help?

If you can't find an answer to your question on this page, you may want to try searching the <a href="http://forum.atlassian.com/search.jspa?q=" class="external-link">Atlassian forums</a> and <a href="http://jira.atlassian.com/secure/QuickSearch.jspa?searchString=" class="external-link">Issue Tracker</a>. These two resources often contain helpful discussion on RPC topics that may solve your problem.

The following documents may also help answer your question:

-   [Creating a JIRA SOAP Client](/server/jira/platform/creating-a-jira-soap-client-4227095.html)
-   [Creating an XML-RPC Client](/server/jira/platform/creating-an-xml-rpc-client-4227133.html)
