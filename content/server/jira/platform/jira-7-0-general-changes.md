---
aliases:
- /server/jira/platform/jira-7.0-general-changes-34669642.html
- /server/jira/platform/jira-7.0-general-changes-34669642.md
category: devguide
confluence_id: 34669642
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=34669642
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=34669642
date: '2017-12-08'
legacy_title: JIRA 7.0 - General changes
platform: server
product: jira
subcategory: updates
title: JIRA 7.0 - General changes
---
# JIRA 7.0 - General changes

This page covers the **general developer changes** for **JIRA 7.0**, that are not covered in the API changes or plugin framework changes.

The changes are part of a larger set of developer changes to JIRA for 7.0. You should also read [Preparing for JIRA 7.0](/server/jira/platform/preparing-for-jira-7-0) for an overview of the release, as well as [JIRA 7.0 - API changes](/server/jira/platform/jira-7-0-api-changes) and [JIRA 7.0 - Platform changes](/server/jira/platform/jira-7-0-platform-changes).

## Summary of changes

The risk level indicates the level of certainty we have that things will break if you are in the "Affected" column and you don't make the necessary changes.

<table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<thead>
<tr class="header">
<th>Change <em>(click to view more details)</em></th>
<th>Affected</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>JIRA importer plugin can be extended to include data from plugins, using the JIRA Project Importer SPI<br />
<em>(see the <a href="/server/jira/platform/extending-the-jira-import-plugin">Guide - Extending the JIRA Import plugin</a>)</em> </p></td>
<td><p>Add-ons that want to include their data in project imports</p>
<p><strong>When:</strong> 7.0</p>
<p><strong>Risk level:</strong> N/A</p></td>
</tr>
<tr class="even">
<td><a href="#project-type-key-required-for-project-templates">Project type key required for project templates</a></td>
<td><p>Add-ons that provide project templates for the 'Create project' dialog will be affected.</p>
<p><strong>When:</strong> 7.0</p>
<p><strong>Risk level:</strong> medium</p></td>
</tr>
<tr class="odd">
<td><p><a href="#project-template-module-not-supported-for-the-creation-for-project-templates">project-template module not supported for the creation of project templates</a></p></td>
<td><p>Add-ons that define project templates using a <code>web-item</code> + <code>project-template</code> tags.</p>
<p><strong>When:</strong> 7.0</p>
<p><strong>Risk level:</strong> medium</p></td>
</tr>
<tr class="even">
<td><a href="#software-plugins-no-longer-bundled">Software plugins no longer bundled</a> <em>(a</em> <em>number of plugins that offer features supporting software teams will no longer be bundled with JIRA)</em></td>
<td><p>Add-ons that depend on any of those plugins to be present.</p>
<p><strong>When:</strong> 7.0</p>
<p><strong>Risk level:</strong> medium</p></td>
</tr>
<tr class="odd">
<td><a href="#quartz-scheduler-removed">Quartz scheduler removed</a></td>
<td><p>Add-ons that import any <code>org.quartz</code> classes.</p>
<p><strong>When:</strong> 7.0</p>
<p><strong>Risk level:</strong> medium</p></td>
</tr>
<tr class="even">
<td><a href="#most-lucene-classes-removed-from-api">Most Lucene classes removed from API</a></td>
<td><p>Add-ons that use Lucene directly.</p>
<p><strong>When:</strong> 7.0</p>
<p><strong>Risk level:</strong> medium</p></td>
</tr>
<tr class="odd">
<td><p><a href="#project-tab-panels-deprecated">Project tab panels deprecated</a></p></td>
<td><p>Any add-on implementing pluggable project tab panels.</p>
<p><strong>When:</strong> 7.0</p>
<p><strong>Risk level:</strong> medium</p></td>
</tr>
<tr class="even">
<td><p><a href="#componenttabpanelmoduledescriptor-and-versiontabpanelmoduledescriptor-removed">ComponentTabPanelModuleDescriptor and VersionTabPanelModuleDescriptor removed</a></p></td>
<td><p>Very few add-ons were providing these tab panels, so the impact should be limited.</p>
<p><strong>When:</strong> 7.0</p>
<p><strong>Risk level:</strong> medium</p></td>
</tr>
<tr class="odd">
<td><p><a href="#project-keys-allow-numbers-by-default">Project keys allow numbers by default</a></p></td>
<td><p>Add-ons that make assumptions about project key syntax, and assume that it is only uppercase letters.</p>
<p><strong>When:</strong> 7.0</p>
<p><strong>Risk level:</strong> low</p></td>
</tr>
<tr class="even">
<td><a href="#dom-changes">DOM changes</a></td>
<td><p>Add-ons that make assumptions about JIRA's DOM structure.</p>
<p><strong>When:</strong> 7.0</p>
<p><strong>Risk level:</strong> low</p></td>
</tr>
<tr class="odd">
<td>Support ending for HSQLDB <em>(see <a href="https://confluence.atlassian.com/display/JIRA/End+of+Support+Announcements+for+JIRA#EndofSupportAnnouncementsforJIRA-201502HSQLDB" class="external-link">end of support announcement</a> in the JIRA documentation)</em></td>
<td><p>Add-ons testing against JIRA's built-in HSQLDB database.<br />
Note, this built-in database is only used for evaluation purposes.</p>
<p><strong>When:</strong> 7.0</p>
<p><strong>Risk level:</strong> low</p></td>
</tr>
<tr class="even">
<td>Support ending for Oracle JDK 1.7 <em>(see <a href="https://confluence.atlassian.com/display/JIRA/End+of+Support+Announcements+for+JIRA#EndofSupportAnnouncementsforJIRA-201502JDK1.7" class="external-link">end of support announcement</a> in the JIRA documentation)</em></td>
<td><p>JIRA Server installations on Oracle JDK 1.7.</p>
<p><strong>When:</strong> 7.0</p>
<p><strong>Risk level:</strong> low</p></td>
</tr>
<tr class="odd">
<td>Support ending for Oracle 11g <em>(see <a href="https://confluence.atlassian.com/display/JIRA/End+of+Support+Announcements+for+JIRA#EndofSupportAnnouncementsforJIRA-201502ORACLE11.2" class="external-link">end of support announcement</a> in the JIRA documentation)</em></td>
<td><p>JIRA Server installations on Oracle 11g databases.</p>
<p><strong>When:</strong> 7.0</p>
<p><strong>Risk level:</strong> low</p></td>
</tr>
<tr class="even">
<td>Support ending for Internet Explorer 9 <em>(see <a href="https://confluence.atlassian.com/display/JIRA/End+of+Support+Announcements+for+JIRA#EndofSupportAnnouncementsforJIRA-201502IE9" class="external-link">end of support announcement</a> in the JIRA documentation)</em></td>
<td><p>JIRA installations on Internet Explorer 9.</p>
<p><strong>When:</strong> 7.0</p>
<p><strong>Risk level:</strong> low</p></td>
</tr>
<tr class="odd">
<td>Dangermode removed<br />
<em>(this functionality was only used in Atlassian's functional tests, and therefore shouldn't affect any plugins)</em> </td>
<td><p>Nobody should be affected.</p>
<p><strong>When:</strong> 7.0</p>
<p><strong>Risk level:</strong> low</p></td>
</tr>
</tbody>
</table>

## Details

### Project type key required for project templates

In JIRA 7.0, all the project templates must provide the key for the type of the projects created with the template. If your add-on is defining a project template, it will need to be modified to include the project type key:

-   If your template is defined via a web-item that is placed on the jira.project.templates section, it needs to specify a param with a name of "`projectTypeKey`" and the value of the project type key. For example:

``` xml
<web-item key="jira-test-item" i18n-name-key="jira-wfb-item.title" section="jira.project.templates">
  <label key="jira-test-item.title"/>
  <param name="projectTypeKey" value="business"/>
  ...
</web-item>
```

-   If your template is defined via a `project-blueprint` tag on your atlassian-plugin.xml, it needs to specify a `projectTypeKey` tag with the project type key as its content. For example:

``` xml
<project-blueprint key="jira-issuetracking" weight="30">
  <label key="jira.issuetracking.project.item"/>
  <projectTypeKey>business</projectTypeKey>
  ...
</project-blueprint>
```

The project type key is mandatory in both cases. If a template doesn't provide a project type, it will not appear on the 'Create project' dialog within JIRA.

###  `project-template` module not supported for the creation for project templates

JIRA will not support the definition of project templates using the old **project-template** module. In 7.0, all project templates provided by add-ons must be defined using the newer [<strong>project-blueprint</strong> module](https://developer.atlassian.com/jiradev/jira-platform/projects/tutorial-creating-a-project-template).

### Software plugins no longer bundled

A number of plugins that offer features supporting software teams, namely the **jira-software-plugin, jira-development-integration-plugin, jira-fisheye-plugin, jira-bamboo-plugin, **and **jira-dvcs-connector-plugin** will no longer be bundled with JIRA from 7.0. If your add-on depends on any of these plugins being installed, you can do either of the following:

-   modify your add-on to work correctly in the absence of any of the above plugins, or 
-   transition your add-on to be a JIRA Software plugin, instead of a JIRA Core plugin. 

To test your add-on against JIRA Software, use the new AMPS version that adds support for applications through the new `applications` element. See [Configure AMPS to run JIRA Core with additional applications installed](https://developer.atlassian.com/display/DOCS/Configure+AMPS+to+run+JIRA+Core+with+additional+applications+installed) for more details.

### Quartz scheduler removed

Direct access to Quartz was deprecated with the introduction of the `atlassian-scheduler` API in JIRA 6.3. In JIRA 7.0, the Quartz Scheduler is no longer used by JIRA, so none of its classes are available, even to those add-ons that depend on `jira-core`. Although the SAL `PluginScheduler` remains available, we encourage you to migrate to the `atlassian-scheduler` API. Add-ons that allowed the user entry of cron expressions and used Quartz's `CronTrigger` class to validate the syntax will no longer be able to do so; however, the `atlassian-scheduler` API provides a `CronExpressionValidator` service that may be used as a replacement for this.

### Most Lucene classes removed from API

Direct access to Lucene classes is discouraged, and a number of classes which use Lucene directly have been moved to the `jira-lucene-dmz` maven artifact.

Add-ons can use `jira-lucene-dmz `to assist migration, but these classes are not covered by the API guarantee. While they are maintained in the short term, they may become deprecated or broken in future minor releases, as improved search and statistics alternatives are rolled out. The intent is that improved alternative APIs, for genuine add-on use cases, will be made available before the Lucene-dependent versions are fully removed.

The affected classes are:

-   The `SearchProvider` interface -- the methods `searchOverrideSecurity` and `searchCountOverrideSecurity` are now now available on `SearchService` class.
-   Some `StatisticsMapper`subclasses -- add-ons needing this functionality must rely on `jira-lucene-dmz`.
-   Various implementations of the lucene `Collector` interface -- add-ons needing this functionality must rely on `jira-lucene-dmz`.
-   The `SearchProviderFactory` for obtaining `IndexSearcher` implementations for direct Lucene access -- add-ons needing this functionality must rely on `jira-lucene-dmz`.
-   Minor Lucene related utility classes -- currently, no replacement is available.

The `IssueIndexManager` has been removed from `jira-api `and is now `jira-core` -- the new `jira-api `class `IssueIndexingService` exposes `reIndex...` and `deIndex...` methods suitable for add-on use. Additionally, other Lucene0related utility classes have been moved to `jira-core`.

### Project tab panels deprecated

Pluggable project tab panels have been deprecated in JIRA 7.0. They will still continue to work, but will no longer render in the top level sidebar navigation and will instead be grouped into a 'Add-ons' top level section. Add-on vendors should migrate their add-ons to the new web-item based approach of integrating with JIRA's new project centric view, described in [Design guide - JIRA project-centric view](/server/jira/platform/designing-for-the-jira-project-centric-view). Project tab panel support will be removed in a future major release of JIRA.

### ComponentTabPanelModuleDescriptor and VersionTabPanelModuleDescriptor removed

The new project-centric view no longer provides an individual version or component view that supports these pluggable tab panels. The usage of these was very low, so these plugin points have been removed.

### Project keys allow numbers by default

Prior to JIRA 7.0, the default configuration for project keys permitted only uppercase alphabetical characters, i.e. `[A-Z][A-Z]+`. In JIRA 7.0, the default regular expression for project keys will be changed to allow numbers (but only after an initial uppercase letter), i.e. `[A-Z][A-Z0-9]+`.

The reason for this change is that the default configuration could only be modified by system administrators. This meant that customers using JIRA Cloud (where the system administrator permissions are not exposed) were forced to stay with the default configuration.

### DOM changes

The `jira-ajax-menu` class will no longer be on application header menu triggers.
