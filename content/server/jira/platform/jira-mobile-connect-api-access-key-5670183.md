---
title: JIRA Mobile Connect API Access Key 5670183
aliases:
    - /server/jira/platform/jira-mobile-connect-api-access-key-5670183.html
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=5670183
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=5670183
confluence_id: 5670183
platform: server
product: jira
category: devguide
subcategory:
date: "2018-04-25"
---
# JIRA Developer Documentation : JIRA Mobile Connect API Access Key

The [JIRA Mobile Connect](https://developer.atlassian.com/display/JMC/JIRA+Mobile+Connect+Developer+Documentation) (JMC) plugin uses a secure API access key to control access to JIRA from your app. When you enable the JIRA Mobile Connect plugin for a JIRA project, an API access key is generated for you.

Since JIRA Mobile Connect allows anonymous access for creating issues in a project via the REST API, the API key is used to ensure only users of your app can actually create issues.

## Configuring the API Access Key in your App

To configure the API access key in your app, you need to pass it to JMC via the configure method.

The easiest way to get started, is to copy the snippet of code displayed in the JIRA Settings Admin screen for JIRA Mobile Connect.

For example:

``` javascript
[[JMC sharedInstance]
    configureJiraConnect:@"http://connect.onjira.com/"
              projectKey:@"NERDS"
                  apiKey:@"591451a6-bc59-4ca9-8840-b67f8c1e440f"];
```

See also the [advanced configuration options](/server/jira/platform/advanced-configuration-8946512.html).
