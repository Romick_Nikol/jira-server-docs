---
aliases:
- /server/jira/platform/4227141.html
- /server/jira/platform/4227141.md
category: devguide
confluence_id: 4227141
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=4227141
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=4227141
date: '2017-12-08'
legacy_title: I have a GenericValue issue. How do I convert this to an Issue object?
platform: server
product: jira
subcategory: other
title: I have a GenericValue issue. How do I convert this to an Issue object?
---
# I have a GenericValue issue. How do I convert this to an Issue object?

### <img src="http://confluence.atlassian.com/images/icons/favicon.png" class="confluence-external-resource" /> I have a GenericValue issue. How do I convert this to an Issue object?

JIRA's API is moving slowly from representing issues as GenericValues (essentially untyped hashmaps) to the <a href="http://docs.atlassian.com/software/jira/docs/api/latest/com/atlassian/jira/issue/Issue.html" class="external-link">Issue interface</a>). As such, you may find yourself having a GenericValue, but needing an Issue to pass to some more recent part of the API.

Note: often there are two methods available, one returning a GenericValue (eg. called `getIssue`) and another returning an Issue (eg. called `getIssueObject`). You may be able to avoid the conversion by picking the right method.

If you find yourself with a GenericValue, the best way to convert your issue is by declaring an <a href="http://docs.atlassian.com/software/jira/docs/api/latest/com/atlassian/jira/issue/IssueFactory.html" class="external-link">IssueFactory</a> in your object's constructor to set an `issueFactory` field in your object, and then call `issueFactory.getIssue(GenericValue)` to get the Issue.

If you're writing code that isn't managed by Pico container, and so won't have an IssueFactory passed in through the constructor, then you can call the static method <a href="http://docs.atlassian.com/software/jira/docs/api/latest/com/atlassian/jira/issue/IssueImpl.html#getIssueObject(org.ofbiz.core.entity.GenericValue)" class="external-link">IssueImpl.getIssueObject(GenericValue)</a>
