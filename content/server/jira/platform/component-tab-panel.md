---
aliases:
- /server/jira/platform/component-tab-panel-plugin-module-4227195.html
- /server/jira/platform/component-tab-panel-plugin-module-4227195.md
category: reference
confluence_id: 4227195
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=4227195
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=4227195
date: '2017-12-08'
legacy_title: Component Tab Panel Plugin Module
platform: server
product: jira
subcategory: modules
title: Component tab panel
---
# Component tab panel

<table>
<colgroup>
<col style="width: 20%" />
<col style="width: 80%" />
</colgroup>
<tbody>
<tr class="odd">
<td><p>Available:</p></td>
<td><p>The Component Tab Panel plugin module is available in JIRA 3.10 and later.</p></td>
</tr>
<tr class="even">
<td>Deprecated:</td>
<td>This plugin module is not available in JIRA 7.1 and later. There is no replacement.</td>
</tr>
</tbody>
</table>

The Component Tab Panel plugin module allows you to add new tabs to the 'Browse Component' page.

![](/server/jira/platform/images/componenttabpanels.png)  
  
All these tab panels ('Open Issues', 'Road Map', 'Change Log' and 'Popular Issues') are implemented as plugins. New component tab panels can be implemented to display component-specific info.

Here is an example panel module descriptor:

``` xml
<!--
The class defined should implement
com.atlassian.jira.plugin.componentpanel.ComponentTabPanel
and it may be useful to use the functionality provided by
com.atlassian.jira.plugin.componentpanel.impl.GenericTabPanel.
-->
    <component-tabpanel key="component-openissues-panel" i18n-name-key="componentpanels.openissues.name"
                        name="Open Issues Panel" class="com.atlassian.jira.plugin.componentpanel.impl.GenericTabPanel">
        <description key="componentpanels.openissues.description">Show the open issues for this component.</description>
        <label key="common.concepts.openissues"/>
        <!-- this is a number defining the order of all panels.
             The system panels are 10, 20, 30 and 40. -->
        <order>10</order>
        <!-- this template produces the HTML for the panel -->
        <resource type="velocity" name="view"
                  location="templates/plugins/jira/projectentitypanels/openissues-component-panel.vm"/>
        <!-- this properties files contains i18n keys for the panel -->
        <resource type="i18n" name="i18n" location="com.atlassian.jira.plugins.componentpanels.openissues"/>
    </component-tabpanel>
```
