---
aliases:
- /server/jira/platform/deprecation-notice-worklog-data-in-issue-related-events-for-webhooks-41225798.html
- /server/jira/platform/deprecation-notice-worklog-data-in-issue-related-events-for-webhooks-41225798.md
category: devguide
confluence_id: 41225798
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=41225798
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=41225798
date: '2017-12-08'
legacy_title: Deprecation notice - Worklog data in issue-related events for webhooks
platform: server
product: jira
subcategory: updates
title: Deprecation notice - Worklog data in issue-related events for webhooks
---
# Deprecation notice - Worklog data in issue-related events for webhooks

**On March 1, 2017** we will be making changes to how worklog data is sent in webhooks, in JIRA Cloud only:

-   The **Issue** webhook in JIRA will no longer contain `worklog` & `comments` objects in the issue representation (this mainly applies to the **updated** event)
-   The **"worklog updated" **event for the **Issue** webhook will no longer be available
-   The **Worklog** webhook in JIRA will now only include the 100 most recent items.

These features will be removed six months after this notice is published, as described in the [Atlassian REST API Policy](https://developer.atlassian.com/display/HOME/Atlassian+REST+API+policy).

### Why?

Currently worklog data is sent with the issue webhook by loading all worklogs to memory and serializing them to JSON. This presents a scalability issue when issues have a large number of worklogs.

This functionality also duplicates the richer **worklog** webhooks, which have more fine-grained events and are more concise.

Our intent is to give developers who need worklog data the specific objects they need, while making webhooks more reliable for everyone.

### Replacement

If you currently use webhook objects from **issue** webhooks, you should migrate to listen for created, updated, and deleted events from the **worklog** webhook.

You can learn more about webhooks in the JIRA Cloud developer documentation: [Webhooks - Atlassian Developers](https://developer.atlassian.com/jiracloud/webhooks-39987038.html)

You can report feature requests and bugs for JIRA Cloud and webhooks in the <a href="https://ecosystem.atlassian.net/projects/ACJIRA" class="external-link">ACJIRA project on ecosystem.atlassian.net</a>.
