---
title: Dialog Box Plugin Module 4227198
aliases:
    - /server/jira/platform/dialog-box-plugin-module-4227198.html
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=4227198
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=4227198
confluence_id: 4227198
platform: server
product: jira
category: devguide
subcategory:
date: "2018-04-25"
---
# JIRA Developer Documentation : Dialog Box Plugin Module

{{% warning %}}

This is a draft restricted to atlassian-staff. 

{{% /warning %}}

<table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<tbody>
<tr class="odd">
<td><p>Available:</p></td>
<td><p>JIRA 4.2 and later</p></td>
</tr>
</tbody>
</table>

## Purpose of this Module Type

*Describe the purpose of this module type here*

## Configuration

The root element for the report plugin module is `name-of-element`. It allows the following attributes and child elements for configuration:

#### Attributes

<table>
<colgroup>
<col style="width: 25%" />
<col style="width: 25%" />
<col style="width: 25%" />
<col style="width: 25%" />
</colgroup>
<thead>
<tr class="header">
<th><p>Name</p></th>
<th><p>Required</p></th>
<th><p>Description</p></th>
<th><p>Default</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p> </p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p> </p></td>
</tr>
</tbody>
</table>

#### Elements

<table>
<colgroup>
<col style="width: 33%" />
<col style="width: 33%" />
<col style="width: 33%" />
</colgroup>
<thead>
<tr class="header">
<th><p>Name</p></th>
<th><p>Required</p></th>
<th><p>Description</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p> </p></td>
<td><p> </p></td>
<td><p> </p></td>
</tr>
</tbody>
</table>

### Internationalisation

**Need to verify if this applies to this plugin module.** It is possible to include an i18n resource in your `atlassian-plugin.xml` to translate keyboard shortcut descriptions (via their '`key`' attributes) into multiple languages. For examples code on including i18n resources in your `atlassian-plugin.xml` file, please refer to [Downloadable Plugin Resources](/server/jira/platform/downloadable-add-on-resources).

## Examples

*Very brief explanation about the origins/purpose of these examples.*

``` javascript
 
```
