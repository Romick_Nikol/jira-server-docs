---
aliases:
- /server/jira/platform/preparing-for-jira-5.2-13632834.html
- /server/jira/platform/preparing-for-jira-5.2-13632834.md
category: devguide
confluence_id: 13632834
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=13632834
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=13632834
date: '2017-12-08'
legacy_title: Preparing for JIRA 5.2
platform: server
product: jira
subcategory: updates
title: Preparing for JIRA 5.2
---
# Preparing for JIRA 5.2

## Changes to handling Message-IDs on notification emails

This change should only affect developers writing custom handlers for incoming emails.

See https://jira.atlassian.com/browse/JRA-9979 for background.

JIRA used to keep a record of the Message-ID for every single outgoing and incoming email in a DB table in order that we could match up the In-Reply-To header of incoming emails with the Issue that the notification was sent about.

This was extremely inefficient, so now we craft a custom Message-ID that we can parse to find the associated Issue instead.

#### What does this mean for developers of custom email handlers?

Not much.  
You can still parse the message subject to try to find an issue key with `ServiceUtils.findIssueObjectInString(subject)`.

You can also still check the incoming In-Reply-To header for an associated issue using `MailThreadManager.getAssociatedIssueObject(message)`.  
This method will now try to parse out an Issue ID from the crafted Message-ID if it is a new message.  
If it is an old email being replied to, it will still look up the old table for up to 4 weeks after the site is upgraded. (After 4 weeks the data is deleted to reclaim space).

Note that the bundled handlers first try to parse the subject, and only if that doesn't produce an Issue, it tries the In-Reply-To header.

If someone was dealing with the NotificationInstance DB table directly, then they should move to using the API methods referred to above.

Some mail handlers would call `MailThreadManager.createMailThread()` to add data to the NotificationInstance table about incoming emails. This is no longer required; the method is deprecated and is now a no-op.

## Changes to rendering of block macros

This change should only affect developers writing custom macros.

See https://jira.atlassian.com/browse/RNDR-78 for background.

Previously, the wiki renderer would ignore the isInline() return value for macros that were found within a paragraph.  However, this caused invalid HTML to be generated in some circumstances where &lt;p&gt;...&lt;/p&gt; paragraph tags were implicitly generated around block macros, such as {code}, and this caused problems rendering the content correctly in some browsers.  With this change, the paragraph tags will no longer be generated around block macros.

#### What does this mean for developers of custom macros?

Since the isInline() method is used to determine the behavior, you should make certain that your macros have the correct return value.  Previously this return value was ignored, so it is possible that developers may have set it incorrectly.  Macros that can appear inside a line, such as {colour}, must return true. Macros that cannot, such as the JiraIssuesMacro, should return false. If you are unsure, then err on the side of returning true.

## Newlines in issue Summary no longer allowed

The regular JIRA UI never allowed you to enter newlines in an issue's Summary field. However, the JIRA backend never enforced that constraint in any way so you could do so via SOAP, REST, XML-RPC, or direct calls to the IssueService. Newlines in the Summary can lead to <a href="http://en.wikipedia.org/wiki/HTTP_response_splitting" class="external-link">response splitting attacks</a>. JIRA has other measures in place to prevent the attacks but as an additional level of defense in depth, JIRA 5.2 adds more validation in the IssueService to prevent the creation of issues with newlines in the Summary.

#### What does this mean for plugin developers?

This validation happens as part of the normal issue creation validation. If you're already handling errors then this shouldn't require a change. Well behaved plugins should go a step further and not allow users to enter newlines in the first place-and thus avoid the possibility of validation errors entirely.

In the extremely unlikely event that your plugin is *forcing* a newline on users, they will no longer be able to create issues until that is remediated.

## Easy way to re-use an existing Custom Field Searcher

Previously if a plugin created a new custom field type but wanted to re-use an existing searcher, it was required to define a new searcher anyway just to be able to configure the relationship between the searcher and the custom field type.

Since v5.2 you can also define a searcher relationship from the custom field type configuration making it much simper to re-use existing searchers.

See the [Custom field plugin module](/server/jira/platform/custom-field) documentation for details.

## New ISSUE\_COMMENT\_DELETED\_ID event

A new event, `ISSUE_COMMENT_DELETED_ID`, has been added along with a new matching method, `issueCommentDeleted(final IssueEvent event)`, in `AbstractIssueEventListener`. This event is now raised when a comment is deleted. Previously when a comment was deleted the generic `ISSUE_UPDATE_ID` event was raised.
