---
aliases:
- /server/jira/platform/4227110.html
- /server/jira/platform/4227110.md
category: devguide
confluence_id: 4227110
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=4227110
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=4227110
date: '2017-12-08'
legacy_title: Retrieving issue's links
platform: server
product: jira
subcategory: other
title: Retrieving issue's links
---
# Retrieving issue's links

{{% note %}}

Redirection Notice

This page will redirect to <https://developer.atlassian.com/x/bIB> in about 2 seconds.

{{% /note %}}










































