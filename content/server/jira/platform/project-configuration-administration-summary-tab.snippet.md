---
aliases:
- /server/jira/platform/--project-configuration-administration-summary-tab-4227101.html
- /server/jira/platform/--project-configuration-administration-summary-tab-4227101.md
category: devguide
confluence_id: 4227101
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=4227101
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=4227101
date: '2017-12-08'
legacy_title: __Project Configuration Administration Summary Tab
platform: server
product: jira
subcategory: other
title: Project Configuration Administration Summary Tab
---
# Project Configuration Administration Summary Tab

<img src="/server/jira/platform/images/project-configuration-administration-summary-tab.png" width="600" />
