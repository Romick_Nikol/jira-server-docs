---
aliases:
- /server/jira/platform/search-request-view-plugin-module-4227179.html
- /server/jira/platform/search-request-view-plugin-module-4227179.md
category: reference
confluence_id: 4227179
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=4227179
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=4227179
date: '2017-12-08'
legacy_title: Search Request View Plugin Module
platform: server
product: jira
subcategory: modules
title: Search request view
---
# Search request view

<table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<tbody>
<tr class="odd">
<td><p>Available:</p></td>
<td><p>Search Request View plugin modules are available in JIRA 3.7 and later.</p></td>
</tr>
</tbody>
</table>

Search request view plugin modules are used to display different representations of search results in the issue navigator. They will be displayed as a link at the top of the issue navigator. Once clicked, JIRA will render the search request view.  
![](/server/jira/platform/images/searchrequestviews.png)

Here's what the bundled 'Printable' view looks like:  
<img src="/server/jira/platform/images/printable.png" class="confluence-thumbnail" />

Search request views can be used to render the search results in any possible representation. Some popuplar choices are for example RSS or XML.

## The Search Request View Plugin Module

A search request view plugin consists of 3 components:

-   The plugin module definition in [atlassian-plugin.xml](/server/framework/atlassian-sdk/configuring-the-plugin-descriptor/).
-   The view implementation class defining any logic needed to render a view.
-   Any number of view templates defining how to render the view.

Let's attempt a sample implementation, to render the results of a search request in this format:

``` xml
<issues filtername="My filter">
   <issue>
      <key>HSP-1</key>
      <summary>Sample issue</summary>
   </issue>
   <issue>
      <key>MKY-1<key>
      <summary>Another sample issue</summary>
   </issue>
</issues>
```

#### Plugin Module Definition

First a definition of the plugin is needed in your plugin's `atlassian-plugin.xml`:

``` xml
...

    <search-request-view key="simple-searchrequest-xml" name="Simple XML"
                         class="com.atlassian.jira.sample.searchrequest.SimpleSearchRequestXmlView" state='enabled'
                         fileExtension="xml" contentType="text/xml">
        <resource type="velocity" name="header" location="templates/searchrequest-xml-header.vm"/>
        <resource type="velocity" name="singleissue" location="templates/searchrequest-xml-singleissue.vm"/>
        <resource type="velocity" name="footer" location="templates/searchrequest-xml-footer.vm"/>
        <order>100</order>
    </search-request-view>

...
```

The search-request-view module is fairly straight-forward. It defines a unique **key** for the module, a **name** which will be used for the link in the issue navigator and the view implementation **class**. Further it also defines what **contentType** to return when displaying this view and a number of view resources. Finally the **order** can be used to control in which order the links appear in the IssueNavigator.

#### Search Request View Implementation Class

The view implementation class below extends a convenience class available in JIRA to make writing search request views easier, namely <a href="http://docs.atlassian.com/software/jira/docs/api/latest/com/atlassian/jira/plugin/searchrequestview/AbstractSearchRequestView.html" class="external-link">AbstractSearchRequestView</a>. All that's left to do then is to implement the `writeSearchResults()` method.

If a search request view needs more control (e.g. control the HTTP headers used in the returned view), then an implementation class can implement the <a href="http://docs.atlassian.com/software/jira/docs/api/latest/com/atlassian/jira/plugin/searchrequestview/SearchRequestView.html" class="external-link">SearchRequestView</a> interface directly.

**SimpleSearchRequestXmlView.java**

``` java
...

/**
 * Sample implementation of a simple XML search request view.
 * <p/>
 * Note that this class extends {@link com.atlassian.jira.plugin.searchrequestview.AbstractSearchRequestView}. This
 * isn't necessary but makes things a lot simpler.  It is also possible to implement the {@link
 * com.atlassian.jira.plugin.searchrequestview.SearchRequestView} interface directly.
 */
public class SimpleSearchRequestXmlView extends AbstractSearchRequestView
{
    private final JiraAuthenticationContext authenticationContext;
    private final SearchProviderFactory searchProviderFactory;
    private final IssueFactory issueFactory;
    private final SearchProvider searchProvider;

    public SimpleSearchRequestXmlView(JiraAuthenticationContext authenticationContext, SearchProviderFactory searchProviderFactory,
            IssueFactory issueFactory, SearchProvider searchProvider)
    {
        this.authenticationContext = authenticationContext;
        this.searchProviderFactory = searchProviderFactory;
        this.issueFactory = issueFactory;
        this.searchProvider = searchProvider;
    }

    public void writeSearchResults(final SearchRequest searchRequest, final SearchRequestParams searchRequestParams, final Writer writer)
    {
        final Map defaultParams = JiraVelocityUtils.getDefaultVelocityParams(authenticationContext);

        //Need to put the filtername into the velocity context.  This may be null if this is an anoymous filter.
        final Map headerParams = new HashMap(defaultParams);
        headerParams.put("filtername", searchRequest.getName());
        try
        {
            //First we need to write the header
            writer.write(descriptor.getHtml("header", headerParams));

            //now lets write the search results.  This basically iterates over each issue in the search results and writes
            //it to the writer using the format defined by this plugin.  To ensure that this doesn't result in huge
            //memory consumption only one issue should be loaded into memory at a time.  This can be guaranteed by using a
            //Hitcollector.
            final Searcher searcher = searchProviderFactory.getSearcher(SearchProviderFactory.ISSUE_INDEX);
            final Map issueParams = new HashMap(defaultParams);
            //This hit collector is responsible for writing out each issue as it is encountered in the search results.
            //It will be called for each search result by the underlying Lucene search code.
            final DocumentHitCollector hitCollector = new IssueWriterHitCollector(searcher, writer, issueFactory)
            {
                protected void writeIssue(Issue issue, Writer writer) throws IOException
                {
                    //put the current issue into the velocity context and render the single issue view
                    issueParams.put("issue", issue);
                    writer.write(descriptor.getHtml("singleissue", issueParams));
                }
            };
            //now run the search that's defined in the issue navigator and pass in the hitcollector from above which will
            //write out each issue in the format specified in this plugin.
            searchProvider.searchAndSort(searchRequest.getQuery(), authenticationContext.getUser(), hitCollector, searchRequestParams.getPagerFilter());

            //finally lets write the footer.
            writer.write(descriptor.getHtml("footer", Collections.emptyMap()));
        }
        catch (IOException e)
        {
            throw new RuntimeException(e);
        }
        catch (SearchException e)
        {
            throw new RuntimeException(e);
        }
    }
}
```

#### View templates

Finally, a number of view templates are needed to display our new view.

First, the header needs to display the **filtername** as an attribute of the `<issues>` tag or simply 'Anonymous' if the view isn't displaying a saved filter:

**searchrequest-xml-header.vm**

``` xml
#set($displayName = 'Anonymous')
#if($filtername)
    #set($displayName = $textutils.htmlEncode($filtername))
#end
<issues filtername="$displayName">
```

Then to render each issue, we need to construct an `<issue>` tag for each individual issue:

**searchrequest-xml-singleissue.vm**

``` xml
    <issue>
        <key>$!issue.key</key>
        <summary>$!issue.summary</summary>
    </issue>
```

Finally the footer should close the `<issues>` tag:

**searchrequest-xml-footer.vm**

``` xml
</issues>
```

#### Limiting the number of search results shown

While JIRA provides an application property to limit the number of issues returned from its built-in search request views <!-- (see Limiting the number of issues returned from a search view such as an RSS feed)-->, this limit is not automatically applied to plugin search views accessed from the issue navigator.

##### For JIRA 4.3.0 and later:

You can get the value of this property using the <a href="http://docs.atlassian.com/jira/4.3.4/com/atlassian/jira/web/action/issue/IssueSearchLimits.html#getMaxResults()" class="external-link">getMaxResults()</a> method of the `IssueSearchLimits` interface, after this interface has been injected into your constructor. For example:

``` java
public SimpleSearchRequestXmlView(JiraAuthenticationContext authenticationContext, SearchProviderFactory searchProviderFactory,
        IssueFactory issueFactory, SearchProvider searchProvider, IssueSearchLimits issueSearchLimits)
{
    this.authenticationContext = authenticationContext;
    this.searchProviderFactory = searchProviderFactory;
    this.issueFactory = issueFactory;
    this.searchProvider = searchProvider;
    this.issueSearchLimits = issueSearchLimits;
}

...

{

    ...

    int maxIssues = issueSearchLimits.getMaxResults();

    ...
    // Actually do the search, limiting the size instead of getting all search results:
    searchProvider.searchAndSort(searchRequest.getQuery(), authenticationContext.getUser(), hitCollector, new PagerFilter(maxIssues));

}
```

##### For JIRA 4.2.x and earlier:

Use the <a href="http://docs.atlassian.com/jira/4.3.4/com/atlassian/jira/config/properties/ApplicationProperties.html#getDefaultBackedString(java.lang.String)" class="external-link">getDefaultBackedString()</a> method of the `ApplicationProperties` interface to obtain the <a href="http://docs.atlassian.com/jira/4.3.4/com/atlassian/jira/config/properties/APKeys.html#JIRA_SEARCH_VIEWS_DEFAULT_MAX" class="external-link">JIRA_SEARCH_VIEWS_DEFAULT_MAX</a> property of the `APKeys` interface, after the `ApplicationProperties` interface has been injected into your constructor.

``` java
...

{

    ...

    final String defaultMax = applicationProperties.getDefaultBackedString(APKeys.JIRA_SEARCH_VIEWS_DEFAULT_MAX);
    int maxIssues = DEFAULT_MAX_RESULTS;
    if (!StringUtils.isBlank(defaultMax))
    {
        maxIssues = Integer.valueOf(defaultMax);
    }

    ...
    // Actually do the search, limiting the size instead of getting all search results:
    searchProvider.searchAndSort(searchRequest.getQuery(), authenticationContext.getUser(), hitCollector, new PagerFilter(maxIssues));

}
```

## Example

<!-- The full source for the sample plugin above can be downloaded <a href="#here" class="unresolved">here</a>. If you wish to just try the plugin out feel free to download the plugin <a href="#jar" class="unresolved">jar</a>.-->

Here's what the sample plugin looks like in action:  
<img src="/server/jira/platform/images/searchrequestviewssimplexml.png" class="image-center" />

And this is a sample search result rendered by the plugin:  
<img src="/server/jira/platform/images/simplexmlview.png" class="image-center" />
