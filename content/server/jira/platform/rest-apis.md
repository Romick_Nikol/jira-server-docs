---
aliases:
- /server/jira/platform/about-the-jira-rest-apis-7372883.html
- /server/jira/platform/about-the-jira-rest-apis-7372883.md
category: reference
confluence_id: 7372883
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=7372883
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=7372883
date: '2018-05-31'
legacy_title: About the JIRA REST APIs
platform: server
product: jira
subcategory: api
title: REST APIs
---
# REST APIs

The Jira REST APIs are used to interact with the Jira Server applications remotely, for example, when
configuring webhooks.
The Jira Server platform provides the REST API for common features, like issues and workflows.

To get started, read the reference documentation: [Jira Server platform REST API](https://docs.atlassian.com/software/jira/docs/api/REST/latest/).

The Jira Software and Jira Service Desk applications have REST APIs for their application-specific features,
like sprints (Jira Software) or customer requests (Jira Service Desk).

*   [Jira Software Server REST API](https://docs.atlassian.com/software/jira/docs/api/REST/latest/)
*   [Jira Service Desk Server REST API](https://docs.atlassian.com/jira-servicedesk/REST/server/)

If you haven't used the Jira REST APIs before, make sure you read the [Atlassian REST API policy](/platform/marketplace/atlassian-rest-api-policy/).

{{% note %}}
The Jira Server platform REST API resources are also documented in this WADL file: [jira-rest-plugin.wadl](https://docs.atlassian.com/jira/REST/server/jira-rest-plugin.wadl).
{{% /note %}}

## Authentication and authorization

*   **Authentication**. The following authentication methods are supported for the Jira REST APIs: 
      * [OAuth 1.0a](/server/jira/platform/oauth)
      * [Basic authentication](/server/jira/platform/basic-authentication)
*   **Authorization** is based on the user used in the authentication process when you call the Jira REST APIs.

For more information on authentication and authorization, read the [Security overview](/server/jira/platform/security-overview).

## URI structure

Jira REST APIs provide access to resources (that is, data entities) via URI paths. To use a REST API, your application
makes an HTTP request and parse the response.

The Jira REST API uses JSON as its communication format and the standard HTTP methods
like `GET`, `PUT`, `POST`, and `DELETE`. URIs for Jira REST API resource have the following structure:

``` bash
http://host:port/context/rest/api-name/api-version/resource-name
```

Currently there are two API names available, which will be discussed later on this page:

*   `auth:` – for authentication-related operations.
*   `api:` – for everything else.

The current API version is `2`. However, there is also a symbolic version called `latest` that resolves to the latest
version supported by the given Jira instance.

As an example, if you wanted to retrieve the JSON representation of issue 
JRA-9 from Atlassian's public issue tracker, you would access:

``` bash
https://jira.atlassian.com/rest/api/latest/issue/JRA-9
```

## Using the REST APIs

The following topics describe how the Jira REST APIs are structured and how you can interact with them.

### Expansion

To simplify API responses, the Jira REST API uses resource expansion. This means that the API will only return parts
of the resource when explicitly requested. This helps you avoid problems that can occur when you request too little
information (for example, you need to make many requests) or too much information (for example, performance impact on Jira).

You can use the `expand` query parameter to specify a comma-separated list of entities that you want expanded, identifying
each of them by name. For example, appending `?expand=names,renderedFields` to an issue's URI requests the inclusion of
the translated field names and the HTML-rendered field values in the response.

The following example expands the `name` and `renderedFields` fields for issue JRA-9:

``` bash
https://jira.atlassian.com/rest/api/latest/issue/JRA-9?expand=names,renderedFields
```

To find out which fields are expandable, look at the `expand` property in the parent object. In the following
example, the `widgets` field is expandable:

``` javascript
{
    "expand": "widgets",
    "self": "http://jira.atlassian.com/rest/api/resource/KEY-1",
    "widgets": { "widgets": [], "size": 5 }
}
```

You can use the dot notation to specify expansion of entities within another entity. For example, `?expand=widgets.fringels` 
would expand the `widgets` collection and also the `fringels` property on each widget.

### Pagination

Jira uses pagination to limit the response size for resources that return a potentially large collection of items.
A request to a paged API will result in a `values` array wrapped in a JSON object with some paging metadata, for example:

``` javascript
{
    "startAt" : 0,
    "maxResults" : 10,
    "total": 200,
    "values": [
        { /* result 0 */ },
        { /* result 1 */ },
        { /* result 2 */ }
    ]
}
```

*   `startAt:` – the item used as the first item in the page of results.
*   `maxResults:` – number of items to return per page.
*   `total:` – total number of items to return, subject to server-enforced limits. This number *may change* as the client
    requests the subsequent pages. A client should always assume that the requested page can be empty. REST API consumers
    should also consider the field to be optional. In cases when calculating this value is too expensive it may not be
    included in the response.
*   `isLastPage:` – indicates whether the current page returned is the last page of results.

Clients can use the `startAt`, `maxResults`, and `total` parameters to retrieve the desired number of results.
Note that each API resource or method may have a different limit on the number of items returned, which means
you can ask for more than you are given. The actual number of items returned is an implementation detail
and this can be changed over time.

### Ordering

Some resources support ordering by a specific field. This is provided by the `orderBy` query parameter.

Ordering can be ascending or descending. To specify the type of ordering, use the "+" or "-" symbols for ascending or
descending respectively. By default, ordering is ascending. For example, `?orderBy=+name` will order the results
by name in ascending order. 

### Self links

Many fields have a `self` link that takes you to the canonical location for that resource.
For example:

``` javascript
"reporter": {
    "self": "http://jira.atlassian.com/rest/api/2/user?username=admin",
    "name": "admin",
    "emailAddress": "admin@example.com",
    "displayName": "Administrator",
    "active": true
},
```

Making a GET request to the `self` link can sometimes provide you with additional information about the field.
For example, if we make a GET request for the `self` link for the `reporter` field above, the response will
contain additional information about the user, including the timezone and groups. 

### Special request and response headers

*   `X-AUSERNAME` – response header that contains either username of the authenticated user or 'anonymous'.
*   `X-Atlassian-Token` – methods that accept multipart/form-data will only process requests with
`X-Atlassian-Token: no-check` header.

### Error responses

Most resources will return a response body in addition to the status code. Usually, the JSON schema of the
entity returned is the following:

``` javascript
{
    "id": "https://docs.atlassian.com/jira/REST/schema/error-collection#",
    "title": "Error Collection",
    "type": "object",
    "properties": {
        "errorMessages": {
            "type": "array",
            "items": {
                "type": "string"
            }
        },
        "errors": {
            "type": "object",
            "patternProperties": {
                ".+": {
                    "type": "string"
                }
            },
            "additionalProperties": false
        },
        "status": {
            "type": "integer"
        }
    },
    "additionalProperties": false
}
```

### Field input formats

**Summary**: A system field that is a single line of text.

``` javascript
"summary": "This is an example summary"
```

**Description**: A system field that is multiple lines of text.

``` javascript
"description": "This is an example description with multiples lines of text\n separated by\n line feeds"
```

**Components**: A system field that is multiple values addressed by 'name'.

``` javascript
"components" : [ { "name": "Active Directory"} , { "name": "Network Switch" } ]
```

**Due date**: A system field that is a date in 'YYYY-MM-DD' format.

``` javascript
"duedate" : "2015-11-18"
```

**Labels**: A system field that is an array of string values.

``` javascript
"labels" : ["examplelabelnumber1", "examplelabelnumber2"]
```

**Checkbox custom field**: A custom field that allows you to select a multiple values from a defined list of values.
You can address them by `value` or by `ID`.

``` javascript
"customfield_11440" : [{ "value" : "option1"}, {"value" : "option2"}]
```

or

``` javascript
"customfield_11440" : [{ "id" : 10112}, {"id" : 10115}]
```

**Date picker custom field**: A custom field that is a date in `YYYY-MM-DD` format.

``` javascript
"customfield_11441" : "2015-11-18"
```

**Date time picker custom field**: A custom field that is a date time in ISO 8601 `YYYY-MM-DDThh:mm:ss.sTZD` format.

``` javascript
"customfield_11442" : "2015-11-18T14:39:00.000+1100"
```

**Labels custom field**: A custom field that is an array of strings.

``` javascript
"customfield_11443" : [ "rest_label1", "rest_label2" ]
```

**Number custom field**: A custom field that contains a number.

``` javascript
"customfield_11444" : 123
```

**Radio button custom field**: A custom field that allows you to select a single value from a defined list of
values. You can address them by `value` or by `ID`.

``` javascript
"customfield_11445" : { "value": "option2" }
```

or

``` javascript
"customfield_11445" : { "id": 10112 }
```

**Cascading select custom field**: A custom field that allows you to select a single parent value and then a
related child value. You can address them by `value` or by `ID`.

``` javascript
"customfield_11447" : { "value": "parent_option1", "child": { "value" : "p1_child1"} }
```

or

``` javascript
"customfield_11447" : { "id": 10112, "child": { "id" : 10115 } }
```

**Multi-select custom field**: A custom field that allows you to select a multiple values from a defined list of
values. You can address them by `value` or by `ID`.

``` javascript
"customfield_11448" : [ { "value": "option1" }, { "value": "option2" } ]
```

or

``` javascript
"customfield_11448" : [ { "id": 10112 }, { "id": 10115 } ]
```

**Single-select custom field**: A custom field that allows you to select a single value from a defined list of
values. You can address them by `value` or by `ID`.

``` javascript
"customfield_11449" : { "value": "option3" }
```

or

``` javascript
"customfield_11449" : { "id": 10112 }
```

**Multi-line text custom field**: A custom field that allows multiple lines of text.

``` javascript
"customfield_11450": "An example of multiples lines of text\n separated by\n line feeds"
```

**Text custom field**: A custom field that allows a single line of text.

``` javascript
"customfield_11450": "An example of a single line of text"
```

**URL custom field**: A custom field that allows a URL to be entered.

``` javascript
"customfield_11452" : "http://www.atlassian.com"
```

**Single-user picker custom field**: A custom field that allows a single user to be selected.

``` javascript
"customfield_11453" : { "name":"tommytomtomahawk" }
```

**Multi-user picker custom field**: A custom field that allows multiple users to be selected.

``` javascript
"customfield_11458" : [ { "name":"inigomontoya" }, { "name":"tommytomtomahawk" }]
```

## Examples

The [Jira REST API examples](/server/jira/platform/jira-rest-api-examples) guide contains a range of examples,
including examples of requests for creating issues, updating issues, searching for issues, and more.

We've also provided a simple example below to get you started. The example shows you how to create an issue using the Jira
REST API. The sample code uses [curl](https://curl.haxx.se) to make requests, but you can
use any tool you prefer.

Note:

*   The input file is denoted by the `--data @filename` syntax. The data is shown separately, and uses the JSON format.
*   Make sure the content type in the request is set to `application/json`, as shown in the example.
*   POST the JSON to your Jira server. In the example, the server is `http://localhost:8080/jira/rest/api/2/issue/`.
*   The example uses basic authentication with admin/admin credentials.
*   You'll need to add a project to the instance before running and get the project ID of the project to which you want
    to add the issue beforehand.

To create an issue using the Jira REST API, follow these steps:

1.  Create the data file that contains the POST data. For this example, we'll assume the file is named `data.txt`.
1.  Add the following JSON to the file:

    ``` javascript
    {
        "fields": {
           "project":
           {
              "id": "10000"
           },
           "summary": "No REST for the Wicked.",
           "description": "Creating of an issue using ids for projects and issue types using the REST API",
           "issuetype": {
              "id": "3"
           }
       }
    }
    ```

    In this data, the project ID is 10000 and the issue type in our case is 3, which represents a task. 
    You should pick an ID of a project in your instance and whichever issue type you prefer.

    Note that instead of the `id` you can also use the key and name for the `project` and `issuetype` respectively.
    For example,`"key": "TEST"` for the project and `"name": "Task"` for the `issuetype`.

1.  In Terminal window, run the following command:

    ``` bash
    curl -u admin:admin -X POST --data @data.txt -H "Content-Type: application/json" http://localhost:8080/jira/rest/api/2/issue/
    ```

    As before, adjust details for your environment, such as the hostname or port of the Jira instance. Note that
    a cloud instance or most public instances would require the use of HTTPS and, of course, valid credentials
    for the instance.

1.  When your issue is created, check the response that will look something like this: 

    ``` javascript
    {
       "id":"10009",
       "key":"TEST-10",
        "self":"http://localhost:8080/jira/rest/api/2/issue/10009"
    } 
    ```

    That's it! You can use the issue ID, issue key, and the URL to the issue for additional requests, if you wish.

To get an issue you just created, use `http://localhost:8080/jira/rest/api/2/issue/{issueIdOrKey}` endpoint:

``` bash
curl -u admin:admin http://localhost:8080/jira/rest/api/2/issue/TEST-10 | python -mjson.tool
```

We use `python -mjson.tool` to pretty print json.

## Webhooks

Webhooks are user-defined callbacks over HTTP that are specific to the Jira REST API. Jira webhooks allow the Jira REST
API to inform a remote application when changes have occurred, for example, an issue transition. This saves the application from
having to periodically poll Jira (via the REST API). To learn more, read the [Webhooks](/server/jira/platform/webhooks) page.

## Jira REST clients

*   [Jira REST Java Client Library](https://ecosystem.atlassian.net/wiki/display/JRJC/Home)
is a sample open source implementation (Apache license) of Jira REST client developed by Atlassian. This implementation
evolves together with the Jira REST API itself and helps driving, validating, and testing the Jira REST API.
If you want to use Jira REST API capabilities from Java or any JVM-based language program, try it out and
give us feedback.
