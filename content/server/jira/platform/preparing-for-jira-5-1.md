---
aliases:
- /server/jira/platform/preparing-for-jira-5.1-10421392.html
- /server/jira/platform/preparing-for-jira-5.1-10421392.md
category: devguide
confluence_id: 10421392
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=10421392
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=10421392
date: '2017-12-08'
legacy_title: Preparing for JIRA 5.1
platform: server
product: jira
subcategory: updates
title: Preparing for JIRA 5.1
---
# Preparing for JIRA 5.1

This documentation is intended for developers integrating with JIRA who want to start getting ready for JIRA 5.1.

## How do I get JIRA 5.1?

Please watch the <a href="http://confluence.atlassian.com/display/JIRA/EAP+Releases" class="external-link">JIRA EAP releases</a> for pre-release versions of JIRA 5.1 that you can use for testing purposes.

## In depth overview

JIRA 5.1 is the first major release of JIRA following the introduction of our stable Java API and stable REST API. Any plugins developed on top of the stable APIs in 5.0 should be binary compatible with JIRA 5.1.

Please read through the information below to see if any of this content is relevant to your plugin.

If you are using a plugin developed by a third party, please check with the plugin's author to see if the plugin has been tested with JIRA 5.1.

**Please Note:**

-   This is not the complete list of changes for JIRA 5.1 - it only describes changes in JIRA 5.1 that will affect plugin developers.
-   For details about which versions of Atlassian's Plugin Development Platform and its components (such as the Plugin Framework, Shared Access Layer, Atlassian User Interface and the Atlassian REST plugin) are included in JIRA 5.1, please refer to [About the Platform](https://developer.atlassian.com/display/DOCS/About+the+Platform). Be aware that this list of matrices may not yet be up to date for JIRA 5.1.

### Custom Fields that use CSS or JavaScript Web Resources

We are re-iterating the following paragraph from the [Preparing for JIRA 5.0](/server/jira/platform/preparing-for-jira-5-0) document, since custom field types with incorrectly located JavaScript resources will not function properly on the 'view issue' page in JIRA 5.1, specifically with the new inline edit capabilities in JIRA 5.1.

Plugin developers commonly use CSS or JavaScript web resources for custom fields, in order to modify the look and feel of these fields or provide these fields with dynamic interaction capabilities. Prior to JIRA 5.0, this would typically have been achieved using using the <a href="http://docs.atlassian.com/atlassian-plugins-webresource/2.10.1/atlassian-plugins-webresource/apidocs/com/atlassian/plugin/webresource/WebResourceManager.html" class="external-link">WebResourceManager</a> from a custom field's `"edit"` HTML. However, this is no longer supported in JIRA 5.0 and any custom field types that do so will not have their requested web resources included in JIRA 5.0's new **Edit Issue** and **Create Issue** dialog boxes. See [Custom Fields that use CSS or JavaScript Web Resources in JIRA 5.0](/server/jira/platform/custom-fields-that-use-css-or-javascript-web-resources-in-jira-5-0) for details on how to update such custom fields for JIRA 5.0 and JIRA 5.1.

### Inline edit and custom field types

Custom field types provided by plugins which included their JavaScript resources in the correct location should automatically work with the new inline edit capabilities in JIRA 5.1. In addition, if custom field types wish to provide automatic saving when their field loses focus, they can implement additional methods on their field to achieve this.

Nevertheless, if your plugin exposes any content on JIRA's 'view issue' page, please take note of the information on [Working with Inline Edit in JIRA 5.1](/server/jira/platform/working-with-inline-edit-in-jira-5-1) to ensure that your plugin maintains compatibility with the inline edit features introduced in JIRA 5.1.

### Velocity 1.6 Upgrade and Automatic Html Escaping Feature

Jira has been upgraded to the 1.6 version of Velocity Engine. This has allowed us to provide an automatic html escaping facility through the velocity-html-safe library used by both Confluence and the Atlassian Template Renderer.

No changes to your existing templates are required for JIRA 5.1. If you would like to start using the feature for new templates you only need to include the `#enable_html_escaping()` velocity directive in your template and all velocity references will be automatically escaped unless:

-   they end with a html suffix.
-   the method being called has an @HtmlSafe annotation.
-   it's a method from library classes that is known to be html-safe.

Detailed documentation on this feature and a migration strategy can be found in the JIRA [HTML escaping for velocity templates](/server/jira/platform/jira-6-0-html-escaping-for-velocity-templates) documentation.

### Indexing of issues during a workflow transition has changed.

In JIRA 5.0 and earlier indexing of issues was stopped during the processing of an issue transition. This meant that if a workflow post function created an issue or amended an issue other than the issue being transitioned, those changes were not correctly indexed. There were documented workarounds for this behaviour that entailed re-enabling indexing in the function and disabling it again.

From JIRA 5.1 on this behaviour is changed. Indexing is now suspended during the workflow transition, with all indexing requests being queue up and processed at the end of the transition. Plugin developers should no longer enable and disable indexing in workflow post functions.
