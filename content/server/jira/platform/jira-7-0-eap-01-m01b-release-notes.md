---
aliases:
- /server/jira/platform/35160580.html
- /server/jira/platform/35160580.md
category: devguide
confluence_id: 35160580
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=35160580
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=35160580
date: '2017-12-08'
legacy_title: JIRA 7.0 EAP 01 (m01b) Release Notes
platform: server
product: jira
subcategory: updates
title: JIRA 7.0 EAP 01 (m01b) Release Notes
---
# JIRA 7.0 EAP 01 (m01b) Release Notes

**28 July 2015**

Atlassian is proud to present **JIRA 7.0 EAP 01 (m01b**). This public development release is part of our [Early Access Program (EAP)](/server/jira/platform/jira-eap-releases-35160569.html) leading up to the official **JIRA 7.0** release. We are making these EAP milestones publicly available so that developers can start assessing the impact of the changes that we are making.

If you are a **JIRA developer**, please see [Preparing for JIRA 7.0](/server/jira/platform/preparing-for-jira-7-0)  as well as our [Java API Policy for JIRA](/server/jira/platform/java-api-policy-for-jira-4227213.html).

<a href="http://www.atlassian.com/software/jira/download-eap" class="external-link"><img src="/server/jira/platform/images/download-eap-btn.png" class="image-center confluence-thumbnail" width="120" /></a>

***[Upgrade notes (including known issues)](#upgrade-notes-including-known-issues)***

{{% warning %}}

Before you install/upgrade: Atlassian does not support upgrades both 'from' and 'to' EAP releases. EAP releases should not be used in production environments as they are not officially supported. For all production use and testing of JIRA, please use <a href="https://confluence.atlassian.com/display/JIRA/JIRA+6.4+Release+Notes" class="external-link">the latest official release</a> instead.

{{% /warning %}}{{% note %}}

**We want your feedback!**

To provide feedback, create an issue in <a href="https://jira.atlassian.com/browse/JRA" class="external-link">our issue tracker</a> with the following details:

-   **Issue Type**: `Suggestion`
-   **Component**: `JIRA 7 EAP`

{{% /note %}}

## Features

### Project sidebar

The project sidebar that we previewed in <a href="https://confluence.atlassian.com/display/JIRA/JIRA+6.4+Release+Notes" class="external-link">JIRA 6.4</a> is here to stay. We built this new navigation experience to make it easier for you find what need in your projects. It's even better, if you are using JIRA Agile: your backlog, sprints, and reports are now just a click away. If you've used the sidebar with JIRA Agile before, you'll notice that boards that include multiple projects now have a project sidebar as well, albeit a simpler version.

### Dashboard items

We're making your dashboards faster with dashboard items in this release. Dashboard items are similar to gadgets, but they use more modern technologies and load faster. They are also simpler to develop, if you want to build your own (read the [developer guide](https://developer.atlassian.com/display/JIRADEV/Guide+-+Building+a+dashboard+item+for+a+JIRA+Connect+add-on)). We haven't replaced all of the gadgets with dashboard items yet, but this release includes a few, like the pie chart gadget and the administration gadget. 

### Changes to project templates

We've made a few changes to the project templates that are used when creating a project. Some templates have been renamed and we've added issue types to others. We've also introduced a default issue type, which is used when creating issues, until you select a different one. Check out the changes below:

<table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Software Development</td>
<td><ul>
<li>Renamed to &quot;Basic software development&quot; </li>
<li>&quot;Epic&quot; added to issue types</li>
<li>Default issue type: &quot;Bug&quot;</li>
</ul></td>
</tr>
<tr class="even">
<td>Agile Software Development - Scrum</td>
<td><ul>
<li>Renamed to &quot;Scrum software development&quot;</li>
<li>Default issue type: &quot;Story&quot;</li>
</ul></td>
</tr>
<tr class="odd">
<td>Agile Software Development - Kanban</td>
<td><ul>
<li>Renamed to &quot;Kanban software development&quot;</li>
<li>Default issue type: &quot;Story&quot;</li>
</ul></td>
</tr>
</tbody>
</table>

## Upgrade notes (including known issues)

Please take note of the following information when installing or upgrading to this EAP release:

-   **Known issue with 7.0 EAP 01 (m01) **-- The 7.0 EAP 01 (m01) EAP milestone includes an incorrect ehcache.xml configuration file, which prevents JIRA from starting. If you have been using this milestone, we recommend that you use the 7.0 EAP 01 (m01b) milestone instead. If you are building via AMPS this means that you will need to specify the version as `7.0.0-m01b`, e.g. `atlas-run-standalone --product jira --version 7.0.0-m01b`.

-   **Known issue with the license generated automatically using JIRA's "Set it up for me" setup path** -- JIRA will treat the generated license as if there are no seats remaining, and you will not be able to create issues. To work around this, get a regular JIRA evaluation license via <a href="http://my.atlassian.com/" class="external-link">my.atlassian.com</a>, then add this to JIRA on the **Licenses** page or during setup by choosing the **"I'll set it up myself"** path.   
    This issue does not affect JIRA if you build it via AMPS, using a command like `atlas-run-standalone --product jira --version 7.0.0-m01`.

In addition to the points above, if you are upgrading from JIRA 6.3.x or earlier, read <a href="https://confluence.atlassian.com/display/JIRA/Skipping+Major+Versions+When+Upgrading+JIRA" class="external-link">Skipping Major Versions When Upgrading JIRA</a>. Remember that EAPs are **not supported** and **should not be used in production environments**.
