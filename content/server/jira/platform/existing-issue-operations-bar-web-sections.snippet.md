---
aliases:
- /server/jira/platform/--existing-issue-operations-bar-web-sections-4227188.html
- /server/jira/platform/--existing-issue-operations-bar-web-sections-4227188.md
category: devguide
confluence_id: 4227188
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=4227188
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=4227188
date: '2017-12-08'
legacy_title: __Existing_Issue_Operations_Bar_Web_Sections
platform: server
product: jira
subcategory: other
title: Existing issue operations bar web sections
---
# Existing issue operations bar web sections

<img src="/server/jira/platform/images/4391013.png" class="gliffy-macro-image" />
