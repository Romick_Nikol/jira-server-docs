---
aliases:
- /server/jira/platform/how-to-set-default-comment-security-level-4227161.html
- /server/jira/platform/how-to-set-default-comment-security-level-4227161.md
category: devguide
confluence_id: 4227161
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=4227161
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=4227161
date: '2017-12-08'
legacy_title: How to Set Default Comment Security Level
platform: server
product: jira
subcategory: other
title: How to set default comment security level
---
# How to set default comment security level

{{% note %}}

Custom JavaScript is outside the scope of Atlassian support - this document is for informational purposes only

{{% /note %}} {{% note %}}

The JavaScript below no longer functions correctly for JIRA 4.2+ Please see the comments below for a velocity template workaround and follow <a href="http://jira.atlassian.com/browse/JRA-23365" class="external-link">JRA-23365</a>

{{% /note %}}

While there is no nice way of setting the default comment security level through the admin section, a bit of creative javascripting can come to the rescue. It's not the best solution but it works.

First, you will need to examine the HTML code of the issue page, and find the user visibility select list. For example:

    <select name="commentLevel" id="commentLevel">
    ...
        <option value="role:10002">Administrators</option>
    ...
        <option value="group:jira-administrators" >jira-administrators</option>

Take note of the value for the option you want to set. Then add this as the **Announcement Banner** under **Options and Settings**, and your field should come preselected.

``` javascript
<script language="JavaScript">
<!--
window.onload = function()
{
    var commentLevelSelect = document.getElementById("commentLevel");
    if (commentLevelSelect)
    {
        commentLevelSelect.value = 'group:jira-administrators';
    }
}
//-->
</script>

<style>
.alertHeader{display:none;}
</style>
```

Of course, you need to change the `commentLevelSelect.value` from `group:jira-administrators` to the one you want.
