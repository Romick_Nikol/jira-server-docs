---
aliases:
- /server/jira/platform/custom-field-plugin-module-4227184.html
- /server/jira/platform/custom-field-plugin-module-4227184.md
category: reference
confluence_id: 4227184
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=4227184
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=4227184
date: '2017-12-08'
legacy_title: Custom field plugin module
platform: server
product: jira
subcategory: modules
title: Custom field
---
# Custom field

## Purpose of this module type

The custom field plugin module allows you to add new custom field types and searchers to JIRA.

1.  Custom field types - these define the type of a custom field
2.  Custom field seachers - these define the ways a custom field type can be searched

## Custom field type configuration

The root element for the custom field type plugin module is `customfield-type`. It allows the following attributes and child elements for configuration:

#### Attributes

<table>
<colgroup>
<col style="width: 20%" />
<col style="width: 80%" />
</colgroup>
<thead>
<tr class="header">
<th><p>Name</p></th>
<th><p>Description</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>class</p></td>
<td><p>The class which implements this plugin module. The class you need to provide depends on the module type. For example, Confluence theme, layout and colour-scheme modules can use classes already provided in Confluence. So you can write a theme-plugin without any Java code. But for macro and listener modules you need to write your own implementing class and include it in your plugin. See the plugin framework guide to <a href="https://developer.atlassian.com/display/DOCS/Creating+Plugin+Module+Instances">creating plugin module instances</a>.</p>
<p>The Java class of the custom field type module.</p>
<p>Classes must implement <a href="http://docs.atlassian.com/jira/4.0/com/atlassian/jira/issue/customfields/CustomFieldType.html" class="external-link">com.atlassian.jira.issue.customfields.CustomFieldType</a>,</p>
<p>but there are several concrete implementations that should address the majority of users' needs, including text fields, text areas, user pickers, etc.</p>
<p>See the <a href="http://docs.atlassian.com/jira/4.0/com/atlassian/jira/issue/customfields/CustomFieldType.html" class="external-link">CustomFieldType javadoc</a> for details.</p></td>
</tr>
<tr class="even">
<td><p>key</p></td>
<td><p>The unique identifier of the plugin module. You refer to this key to use the resource from other contexts in your plugin, such as from the plugin Java code or JavaScript resources.</p>
<p> </p>
<pre><code>&lt;component-import key=&quot;appProps&quot;
interface=&quot;com.atlassian.sal.api.ApplicationProperties&quot;/&gt;</code></pre>
<p> </p>
<p>In the example, <code>appProps</code> is the key for this particular module declaration, for <code>component-import</code>, in this case.</p>
<p>I.e. the identifier of the custom field type module.</p></td>
</tr>
<tr class="odd">
<td><p>i18n-name-key</p></td>
<td>The localisation key for the human-readable name of the plugin module.</td>
</tr>
<tr class="even">
<td><p>name</p></td>
<td><p>The human-readable name of the plugin module.</p>
<p>I.e. the human-readable name of the custom field type module.</p>
<p><strong>Default:</strong> the plugin key.</p></td>
</tr>
</tbody>
</table>

**\*class and key attributes are required.**

#### Elements

<table>
<colgroup>
<col style="width: 20%" />
<col style="width: 80%" />
</colgroup>
<thead>
<tr class="header">
<th><p>Name</p></th>
<th><p>Description</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>description</p></td>
<td><p>A human-readable description of this custom field type plugin module. May be specified as the value of this element for plain text or with the <code>key</code> attribute to use the value of a key from the i18n system.</p></td>
</tr>
<tr class="even">
<td><p>resource type=&quot;velocity&quot;</p></td>
<td><p>Velocity templates that implement the custom field views.</p></td>
</tr>
<tr class="odd">
<td>valid-searcher</td>
<td><p>(Since JIRA v5.2)</p>
<p>This provides an alternative way to define a relationship between a Custom Field Type and a Custom Field Searcher (see also the <code>valid-customfield-type</code> element in Custom Field Searcher below).</p>
<p>Defines the searchers used for this custom field type. The <code>package</code> and <code>key</code> attributes together specify the module-complete key under which the custom field searcher is registered.</p>
<p>This is most useful for when you have a new Custom Field Type that wants to use an existing searcher from core JIRA or another plugin.</p></td>
</tr>
</tbody>
</table>

## Example

Here is the custom field JIRA defines for selecting users (taken from `system-customfieldtypes-plugin.xml`):

``` xml
<customfield-type key="userpicker" name="User Picker"
class="com.atlassian.jira.issue.customfields.impl.UserCFType">
   <description>
      Choose a user from the user base via a popup picker window.
   </description>

   <!-- this template is used on the view issue page -->
   <resource type="velocity" name="view"
   location="templates/plugins/fields/view-user.vm" />
   <!-- this template is used on the create/edit issue pages -->
   <resource type="velocity" name="edit"
   location="templates/plugins/fields/edit-userpicker.vm" />
   <!-- this template is used when viewing an issue as XML -->
   <resource type="velocity" name="xml"
   location="templates/plugins/fields/xml-user.vm" />
</customfield-type>
```

## Custom field searcher configuration

The root element for the custom field searcher plugin module is `customfield-searcher`. It allows the following attributes and child elements for configuration:

#### Attributes

<table>
<colgroup>
<col style="width: 20%" />
<col style="width: 80%" />
</colgroup>
<thead>
<tr class="header">
<th><p>Name</p></th>
<th><p>Description</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>class</p></td>
<td><p>The class which implements this plugin module. The class you need to provide depends on the module type. For example, Confluence theme, layout and colour-scheme modules can use classes already provided in Confluence. So you can write a theme-plugin without any Java code. But for macro and listener modules you need to write your own implementing class and include it in your plugin. See the plugin framework guide to <a href="https://developer.atlassian.com/display/DOCS/Creating+Plugin+Module+Instances">creating plugin module instances</a>. The Java class of the custom field searcher.</p>
<p>Classes must implement <a href="http://docs.atlassian.com/jira/4.0/com/atlassian/jira/issue/customfields/CustomFieldSearcher.html" class="external-link">com.atlassian.jira.issue.customfields.CustomFieldSearcher</a>,</p>
<p>but there are several concrete implementations that can handle searching for JIRA's built-in fields.</p>
<p>See the <a href="http://docs.atlassian.com/jira/4.0/com/atlassian/jira/issue/customfields/CustomFieldSearcher.html" class="external-link">CustomFieldSearcher javadoc</a> for more details.</p></td>
</tr>
<tr class="even">
<td><p>key</p></td>
<td><p>The unique identifier of the plugin module. You refer to this key to use the resource from other contexts in your plugin, such as from the plugin Java code or JavaScript resources.</p>
<p> </p>
<pre><code>&lt;component-import key=&quot;appProps&quot;
interface=&quot;com.atlassian.sal.api.ApplicationProperties&quot;/&gt;</code></pre>
<p> </p>
<p>In the example, <code>appProps</code> is the key for this particular module declaration, for <code>component-import</code>, in this case.</p>
<p>I.e. the identifier of the custom field searcher module.</p></td>
</tr>
<tr class="odd">
<td><p>i18n-name-key</p></td>
<td>The localisation key for the human-readable name of the plugin module.</td>
</tr>
<tr class="even">
<td><p>name</p></td>
<td><p>The human-readable name of the plugin module.</p>
<p>I.e. the human-readable name of the custom field searcher module.</p>
<p><strong>Default:</strong> the plugin key.</p></td>
</tr>
</tbody>
</table>

**\*class and key attributes are required.**

#### Elements

<table>
<colgroup>
<col style="width: 20%" />
<col style="width: 80%" />
</colgroup>
<thead>
<tr class="header">
<th><p>Name</p></th>
<th><p>Description</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>description</p></td>
<td><p>A human-readable description of this custom field searcher plugin module. May be specified as the value of this element for plain text or with the <code>key</code> attribute to use the value of a key from the i18n system.</p></td>
</tr>
<tr class="even">
<td><p>resource type=&quot;velocity&quot;</p></td>
<td><p>Velocity templates that implement the custom field searcher views.</p></td>
</tr>
<tr class="odd">
<td><p>valid-customfield-type</p></td>
<td><p>Defines the custom field types this searcher can apply to. The <code>package</code> and <code>key</code> attributes together specify the module-complete key under which the custom field is registered; if the searcher is defined in the same <code>atlassian-plugin.xml</code> as the type (which is the usual case), then <code>package</code> should be the same as the <code>key</code> attribute on <code>&lt;atlassian-plugin&gt;</code>.</p>
<p>Note that since JIRA v5.2 it is also possible to define this relationship in the opposite direction, that is to define searchers that are appropriate for a given custom field type. Strictly speaking this field is no longer mandatory - you need to define the relationship here <em>or</em> in the custom field type configuration. Practically, however, we would normally expect to see a custom field searcher define the applicable custom field types that it knows about. The alternative mechanism is intended for new custom field types defined in other plugins to re-use existing searchers.</p></td>
</tr>
</tbody>
</table>

**\*valid-customfield-type element is required.**

## Example

Here is the custom field searcher JIRA defines for searching users (also taken from `system-customfieldtypes-plugin.xml`):

``` xml
<customfield-searcher key="userpickersearcher" name="User Picker Searcher"
class="com.atlassian.jira.issue.customfields.searchers.UserPickerSearcher">
   <description>
      Allow to search for a user using a userpicker.
   </description>

   <!-- this template is used on the issue navigator search form -->
   <resource type="velocity" name="search"
   location="templates/plugins/fields/search-userpicker.vm" />
   <!-- this element defines the valid custom field types for this searcher -->
   <valid-customfield-type
   package="com.atlassian.jira.plugin.system.customfieldtypes" key="userpicker" />
</customfield-searcher>
```

## Velocity context for custom fields

Below is a list of available objects in the Velocity context for custom fields.

<table>
<colgroup>
<col style="width: 20%" />
<col style="width: 80%" />
</colgroup>
<thead>
<tr class="header">
<th><p>Object</p></th>
<th><p>Description</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>customField</p></td>
<td><p><code>com.atlassian.jira.issue.fields.CustomField</code> object. Information on the current field.</p>
<p> </p>
<p><strong>edit:</strong> yes, <strong>view:</strong> yes, <strong>col-view:</strong> yes</p></td>
</tr>
<tr class="even">
<td><p>issue</p></td>
<td><p><code>com.atlassian.jira.issue.Issue</code> object. Pretty much everything about the issue.</p>
<p> </p>
<p><strong>edit:</strong> yes, <strong>view:</strong> yes, <strong>col-view:</strong> yes</p></td>
</tr>
<tr class="odd">
<td><p>fieldLayoutItem</p></td>
<td><p><code>com.atlassian.jira.issue.fields.layout.field.FieldLayoutItem</code>. Is field required? hidden?</p>
<p> </p>
<p><strong>edit:</strong> yes, <strong>view:</strong> no, <strong>col-view:</strong> no</p></td>
</tr>
<tr class="even">
<td><p>action</p></td>
<td><p>Calling action.</p>
<p> </p>
<p><strong>edit:</strong> yes, <strong>view:</strong> no, <strong>col-view:</strong> no</p></td>
</tr>
<tr class="odd">
<td><p>displayParameters</p></td>
<td><p>Custom parameters to the template, such as whether to display headers or not</p>
<p> </p>
<p><strong>edit:</strong> yes, <strong>view:</strong> no, <strong>col-view:</strong> no</p></td>
</tr>
<tr class="even">
<td><p>value</p></td>
<td><p>String value of the custom field. Thus this is a String for Date and other single valued fields, List of Strings for Multi selects and CustomFieldParams full of Strings for Cascading selects</p>
<p> </p>
<p><strong>edit:</strong> yes, <strong>view:</strong> yes, <strong>col-view:</strong> yes</p></td>
</tr>
<tr class="odd">
<td><p>customFieldParams</p></td>
<td><p>This is where the value is pulled from, for convenience</p>
<p> </p>
<p><strong>edit:</strong> yes, <strong>view:</strong> yes, <strong>col-view:</strong> yes</p></td>
</tr>
<tr class="even">
<td><p>config</p></td>
<td><p><code>com.atlassian.jira.issue.customfields.config.CustomFieldConfig</code> object.</p>
<p> </p>
<p><strong>edit:</strong> yes, <strong>view:</strong> no, <strong>col-view:</strong> no</p></td>
</tr>
<tr class="odd">
<td><p>configs</p></td>
<td><p>The various configuration items for that context. This include things like, default values, select list options and other configurable options</p>
<p> </p>
<p><strong>edit:</strong> yes, <strong>view:</strong> no, <strong>col-view:</strong> no</p></td>
</tr>
<tr class="even">
<td><p>i18n</p></td>
<td><p><code>com.atlassian.jira.web.bean.I18nBean</code> for your internationalisation needs</p>
<p> </p>
<p><strong>edit:</strong> yes, <strong>view:</strong> yes, <strong>col-view:</strong> yes</p></td>
</tr>
<tr class="odd">
<td><p>descriptor</p></td>
<td><p>The module descriptor of the current field</p>
<p> </p>
<p><strong>edit:</strong> yes, <strong>view:</strong> yes, <strong>col-view:</strong> yes</p></td>
</tr>
<tr class="even">
<td><p>textutils</p></td>
<td><p><code>com.opensymphony.util.TextUtils</code> for text manipulation needs</p>
<p> </p>
<p><strong>edit:</strong> yes, <strong>view:</strong> yes, <strong>col-view:</strong> yes</p></td>
</tr>
<tr class="odd">
<td><p>outlookdate</p></td>
<td><p><code>com.atlassian.jira.web.util.OutlookDate</code> for formatting dates, JIRA style</p>
<p> </p>
<p><strong>edit:</strong> yes, <strong>view:</strong> yes, <strong>col-view:</strong> yes</p></td>
</tr>
<tr class="even">
<td><p>authcontext</p></td>
<td><p><code>com.atlassian.jira.security.JiraAuthenticationContext</code> for authentication information</p>
<p> </p>
<p><strong>edit:</strong> yes, <strong>view:</strong> yes, <strong>col-view:</strong> yes</p></td>
</tr>
<tr class="odd">
<td><p>dateutils</p></td>
<td><p><code>com.atlassian.core.util.DateUtils</code> more date functions</p>
<p> </p>
<p><strong>edit:</strong> yes, <strong>view:</strong> yes, <strong>col-view:</strong> yes</p></td>
</tr>
<tr class="even">
<td><p>req</p></td>
<td><p><code>HttpServletRequest</code> object.</p>
<p> </p>
<p><strong>edit:</strong> yes, <strong>view:</strong> yes, <strong>col-view:</strong> yes</p></td>
</tr>
<tr class="odd">
<td><p>baseurl</p></td>
<td><p>The <code>getContextPath</code> of the req object</p>
<p> </p>
<p><strong>edit:</strong> yes, <strong>view:</strong> yes, <strong>col-view:</strong> yes</p></td>
</tr>
<tr class="even">
<td><p>constantsManager</p></td>
<td><p>The <code>com.atlassian.jira.config.ConstantsManager</code> object for managing &quot;constants&quot; (issue types, resolutions etc.)</p>
<p> </p>
<p><strong>edit:</strong> yes, <strong>view:</strong> yes, <strong>col-view:</strong> yes</p></td>
</tr>
<tr class="odd">
<td><p>projectManager</p></td>
<td><pre><code>com.atlassian.jira.project.ProjectManager</code></pre>
<p><strong>edit:</strong> yes, <strong>view:</strong> yes, <strong>col-view:</strong> yes</p></td>
</tr>
<tr class="even">
<td><p>applicationProperties</p></td>
<td><p><code>com.atlassian.jira.config.properties.ApplicationProperties</code></p>
<p> </p>
<p><strong>edit:</strong> yes, <strong>view:</strong> yes, <strong>col-view:</strong> yes</p></td>
</tr>
<tr class="odd">
<td><p>jirautils</p></td>
<td><p><code>com.atlassian.jira.util.JiraUtils</code> a random set of methods. has isPublic method</p>
<p> </p>
<p><strong>edit:</strong> yes, <strong>view:</strong> yes, <strong>col-view:</strong> yes</p></td>
</tr>
<tr class="even">
<td><p>jirakeyutils</p></td>
<td><p><code>com.atlassian.jira.util.JiraKeyUtils</code> an object used for parsing keys</p>
<p> </p>
<p><strong>edit:</strong> yes, <strong>view:</strong> yes, <strong>col-view:</strong> yes</p></td>
</tr>
<tr class="odd">
<td><p>buildutils</p></td>
<td><p><code>com.atlassian.jira.util.BuildUtils</code> has information on build numbers, editions etc.</p>
<p> </p>
<p><strong>edit:</strong> yes, <strong>view:</strong> yes, <strong>col-view:</strong> yes</p></td>
</tr>
<tr class="even">
<td><p>velocityhelper</p></td>
<td><p><code>com.atlassian.jira.util.JiraVelocityHelper</code> random set of utility methods</p>
<p> </p>
<p><strong>edit:</strong> yes, <strong>view:</strong> yes, <strong>col-view:</strong> yes</p></td>
</tr>
<tr class="odd">
<td><p>userutils</p></td>
<td><p><code>com.atlassian.core.user.UserUtils</code> untility for getting users</p>
<p> </p>
<p><strong>edit:</strong> yes, <strong>view:</strong> yes, <strong>col-view:</strong> yes</p></td>
</tr>
</tbody>
</table>

\+ Other velocity parameters from the custom field type

## Adding searching to a custom field

Custom fields need a **custom field searcher** before you can search them in the Issue Navigator. If a custom field type is a going to behave somewhat like an existing type, then you can reuse an existing searcher. For example, if you're creating a custom field type that's like a multi select list and want to reuse the default multi-select searcher, you could add the block below to your
[atlassian-plugin.xml](/server/framework/atlassian-sdk/configuring-the-plugin-descriptor/) (taken from `system-customfieldtypes-plugin.xml`).

``` xml
<customfield-searcher key="multiselectsearcher" name="Multi Select Searcher"
    i18n-name-key="admin.customfield.searcher.multiselectsearcher.name"
    class="com.atlassian.jira.issue.customfields.searchers.MultiSelectSearcher">
    <description key="admin.customfield.searcher.multiselectsearcher.desc">Search for multiple values using a single select list.</description>

    <resource type="velocity" name="search" location="templates/plugins/fields/edit-searcher/search-multiselect.vm"/>
    <resource type="velocity" name="view" location="templates/plugins/fields/view-searcher/view-searcher-multioption.vm"/>
    <resource type="velocity" name="label" location="templates/plugins/fields/view-searcher/label-searcher-basictext.vm"/>
    <valid-customfield-type package="YOUR CUSTOM PACKAGE" key="YOUR CUSTOM FIELD KEY"/>
</customfield-searcher>
```

which will enable searching for any text based, multi-select custom field type. When creating the custom field, you will now be able to select the multi-select searcher and your custom field should then be displayed in the Issue Navigator.

Note that "YOUR CUSTOM PACKAGE" refers to the package (ie. the module key) of the custom field that the searcher applies to, which is the "key" attribute of the top-level atlassian-plugin element. "YOUR CUSTOM FIELD KEY" refers to the "key" attribute of the customfield element.

## Notes

Types and searchers can be combined in different ways to produce new custom fields, for example a "user" custom field could take a simple text searcher (to enter the username as text) or a more complex "user picker searcher" (where the user is picked from a popup window).

*   For more details, see the [Tutorial - Creating a custom field type](/server/jira/platform/creating-a-custom-field-type).
*   For more information about the `atlassian-plugin.xml` file, see the
 [Configuring the app descriptor](/server/framework/atlassian-sdk/configuring-the-plugin-descriptor/) page.
*   To learn more about the custom field Velocity templates, see <a href="https://developers.atlassian.com/jiradev/jira-platform/building-jira-add-ons/jira-plugins2-overview/jira-plugin-module-types/custom-field-plugin-module/custom-field-velocity-context-unwrapped">Custom field Velocity context unwrapped</a>
