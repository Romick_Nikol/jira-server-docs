---
title: Portlet Plugin Module Deprecated 4227185
aliases:
    - /server/jira/platform/portlet-plugin-module-deprecated-4227185.html
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=4227185
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=4227185
confluence_id: 4227185
platform: server
product: jira
category: devguide
subcategory:
date: "2018-04-25"
---
# JIRA Developer Documentation : Portlet Plugin Module - deprecated

<table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<tbody>
<tr class="odd">
<td><p>Deprecated:</p></td>
<td><p>In JIRA 4.0, portlets were replaced by gadgets. For details, please see <a href="https://developer.atlassian.com/display/GADGETS/Gadgets+and+JIRA+Portlets">Gadgets and JIRA Portlets</a>.<br />
In JIRA 5.0, all portlet-specific code has been removed from JIRA plugin points and APIs. See <a href="/server/jira/platform/java-api-changes-in-jira-5-0">Java API Changes in JIRA 5.0</a>.</p></td>
</tr>
</tbody>
</table>

## Purpose of this Module Type

A portlet plugin module defines a portlet that users can add to their Dashboards.

## Configuration

The root element for the portlet plugin module is `portlet`. It allows the following attributes and child elements for configuration:

#### Attributes

<table>
<colgroup>
<col style="width: 25%" />
<col style="width: 25%" />
<col style="width: 25%" />
<col style="width: 25%" />
</colgroup>
<thead>
<tr class="header">
<th><p>Name</p></th>
<th><p>Required</p></th>
<th><p>Description</p></th>
<th><p>Default</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>class</p></td>
<td><p>yes</p></td>
<td><p>The class which implements this plugin module. The class you need to provide depends on the module type. For example, Confluence theme, layout and colour-scheme modules can use classes already provided in Confluence. So you can write a theme-plugin without any Java code. But for macro and listener modules you need to write your own implementing class and include it in your plugin. See the plugin framework guide to <a href="https://developer.atlassian.com/display/DOCS/Creating+Plugin+Module+Instances">creating plugin module instances</a>. The Java class of the component. Portlet implementation classes are required to implement the <code>com.atlassian.jira.portlet.Portlet</code> interface, but we strongly recommend that you extend the convenience class <code>com.atlassian.jira.portlet.PortletImpl</code>.</p></td>
<td><p> </p></td>
</tr>
<tr class="even">
<td><p>key</p></td>
<td><p>yes</p></td>
<td><p>The unique identifier of the plugin module. You refer to this key to use the resource from other contexts in your plugin, such as from the plugin Java code or JavaScript resources.</p>
<p> </p>
<pre><code>&lt;component-import key=&quot;appProps&quot; interface=&quot;com.atlassian.sal.api.ApplicationProperties&quot;/&gt;</code></pre>
<p> </p>
<p>In the example, <code>appProps</code> is the key for this particular module declaration, for <code>component-import</code>, in this case. I.e. the identifier of the component.</p></td>
<td><p>N/A</p></td>
</tr>
<tr class="odd">
<td><p>i18n-name-key</p></td>
<td><p> </p></td>
<td>The localisation key for the human-readable name of the plugin module.</td>
<td><p> </p></td>
</tr>
<tr class="even">
<td><p>name</p></td>
<td><p> </p></td>
<td><p>The human-readable name of the plugin module. I.e. the human-readable name of the component.</p></td>
<td><p>The plugin key.</p></td>
</tr>
</tbody>
</table>

#### Elements

<table>
<colgroup>
<col style="width: 33%" />
<col style="width: 33%" />
<col style="width: 33%" />
</colgroup>
<thead>
<tr class="header">
<th><p>Name</p></th>
<th><p>Required</p></th>
<th><p>Description</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>description</p></td>
<td><p> </p></td>
<td><p>A human-readable description of this portlet module. May be specified as the value of this element for plain text or with the <code>key</code> attribute to use the value of a key from the i18n system.</p></td>
</tr>
<tr class="even">
<td><p>label</p></td>
<td><p>yes</p></td>
<td><p>The user-visible name of this portlet. May be specified as the value of this element for plain text or with the <code>key</code> attribute to use the value of a key from the i18n system.</p></td>
</tr>
<tr class="odd">
<td><p>thumbnail</p></td>
<td><p> </p></td>
<td><p>An optional thumbnail image used to preview the portlet for users.</p></td>
</tr>
<tr class="even">
<td><p>permission</p></td>
<td><p> </p></td>
<td><p>Permission required to add this portlet to a dashboard. Value must be one of the fields of <code>com.atlassian.jira.security.Permissions</code>.</p></td>
</tr>
<tr class="odd">
<td><p>objectdescriptor</p></td>
<td><p> </p></td>
<td><p>Allows configuration of this portlet with an object descriptor class (see <a href="/server/jira/platform/object-configurable-parameters">this page</a> for more details). We recommend using a <code>key</code> attribute containing the localized name of your plugin.</p></td>
</tr>
<tr class="even">
<td><p>lazy</p></td>
<td><p> </p></td>
<td><p>Whether to load this portlet lazily.</p></td>
</tr>
</tbody>
</table>

## Example

``` javascript
<portlet key="assignedtome" name="Assigned Issues"
    class="com.atlassian.jira.portal.portlets.AssignedToMePortlet">
    <description key="portlet.assignedtome.description">i18n description</description>
    <!-- this template produces the eventual HTML of the portlet -->
    <resource type="velocity" name="view" location="templates/plugins/jira/portlets/assignedtome.vm" />
    <label key="portlet.assignedtome.name" />
    <!-- an optional thumbmail image used to preview the portlet for users -->
    <thumbnail>portlets/dashboard/thumbnails/assigned.gif</thumbnail>
    <!--
        the permissions required to add this portlet
        (optional - not often required)
     -->
    <permission>assignable</permission>
    <objectdescriptor key="portlet.assignedtome.display.name" />
    <!-- same as the properties of the report plugin module -->
    <properties>
        <property>
            <key>numofentries</key>
            <name>portlet.assignedtome.field.numofentries.name</name>
            <description>portlet.assignedtome.field.numofentries.description</description>
            <type>long</type>
            <default>10</default>
        </property>
    </properties>
</portlet>
```
