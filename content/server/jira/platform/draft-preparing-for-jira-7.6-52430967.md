---
title: Draft Preparing for JIRA 7.6 52430967
aliases:
    - /server/jira/platform/draft-preparing-for-jira-7.6-52430967.html
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=52430967
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=52430967
confluence_id: 52430967
platform: server
product: jira
category: devguide
subcategory:
date: "2018-04-25"
---
# JIRA Developer Documentation : DRAFT - Preparing for JIRA 7.6

This page covers changes in **JIRA 7.6** that can affect add-on compatibility and functionality. This includes changes to the JIRA platform and the JIRA applications (JIRA Core, JIRA Software, JIRA Service Desk).

As a rule, Atlassian makes every effort to inform add-on developers of known API changes **as far in advance as possible**. Where possible, we attach release targets. Unless otherwise indicated, every change on this page is expected to be released with the first public release of the JIRA Server 7.6 products. We also make release milestones available prior to the release:

-   For JIRA Server, the JIRA development team releases a number of EAP milestones prior to the final release, for customers and developers to keep abreast of upcoming changes. For more information on these releases, see [JIRA EAP Releases](/server/jira/platform/jira-eap-releases-35160569.html).
-   For JIRA Cloud, the JIRA team releases updates to the JIRA Cloud products on a weekly basis. Add-ons that integrate with the JIRA Cloud products via Atlassian Connect should use the JIRA REST API, which is subject to the <a href="https://developer.atlassian.com/display/HOME/Atlassian+REST+API+policy" class="external-link">Atlassian REST API Policy</a>.

We will update this page as development progresses, so please stay tuned for more updates. We also recommend following the JIRA news on [Atlassian Developers blog](https://developer.atlassian.com/blog/categories/jira/) for important announcements and helpful articles.

## Summary

The risk level indicates the level of certainty we have that things will break if you are in the "Affected" column and you don't make the necessary changes.

<table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<thead>
<tr class="header">
<th>Change. Platform/Application</th>
<th>Risk level. Affects</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>Change: Priority schemes (UI changes)</p>
<p>Platform/Application: JIRA Core, JIRA Software</p></td>
<td><p>Risk level: medium</p>
<p>Affects: Add-on developers using APIs to manage priorities in JIRA, and administrators.</p>
<p>With JIRA 7.6, we're introducing priority schemes that change the way priorities are managed. Now, by using priority schemes, you can associate different sets of priorities with your project.</p>
<p>This doesn't change your configuration until you create some schemes and make use of them. After the upgrade, all priorities are moved to the default priority scheme, which works like a global list of priorities (always includes all existing priorities). The default priority scheme can't be edited or deleted.</p>
<p>See <a href="/server/jira/platform/jira-core-7-6-eap-rc-release-notes">Priority schemes - release notes</a>.</p></td>
</tr>
<tr class="even">
<td><p>Change: Priority schemes (API changes)</p>
<p>Platform/Application: JIRA Core, JIRA Software</p>
<p> </p></td>
<td><p>Risk level: medium</p>
<p>Affects: Add-on developers using APIs to manage priorities in JIRA.</p>
<p>Together with priority schemes, we've added and changed some APIs so that plugins can manage them.</p>
<p>See <a href="#priority-schemes-api-changes-for-plugin-developers">API changes for plugin developers</a>.</p></td>
</tr>
<tr class="odd">
<td><p>Change: API changes - experimental API</p>
<p>Platform/Application: JIRA Service Desk</p></td>
<td><p>Risk level: low</p>
<p>With an upcoming release of JIRA Service Desk, a number of the methods in our API will be moving out of the experimental state and in to standard API state. For most of these components the API will remain unchanged, but for some there will be adjustments which will represent a breaking change. </p>
<p>See <a href="#what-s-changing">API changes - experimental API</a>.</p></td>
</tr>
<tr class="even">
<td><p>Change: Content-Security-Policy and X-Frame-Options headers</p>
<p>Platform/Application: JIRA, all</p></td>
<td><p>Risk level: low</p>
<p>To prevent clickjacking, JIRA introduces new security headers. This change might affect you, if you embed any resources from JIRA on other sites.</p>
<p>See <a href="#content-security-policy-and-x-frame-options-headers">Security headers</a></p></td>
</tr>
</tbody>
</table>

## JIRA platform changes

***All changes in JIRA platform are also relevant for JIRA Core and JIRA Software. ***

### Priority schemes - UI changes

For more information about managing priority schemes in the UI, see [release notes](/server/jira/platform/jira-core-7-6-eap-rc-release-notes).

### Priority schemes - API changes for plugin developers

If you're a plugin developer, and want your plugin to manage priority schemes, take a look at this list of API changes. We've not only added new APIs that allow plugins to manage priority schemes, but we also changed the existing ones, which will most likely make your plugins incompatible. This is for priority schemes, the APIs for managing priorities don't change, but bear in mind that the current global list of priorities now becomes the default priority scheme, so your plugins might retrieve inaccurate data regarding priorities themselves. 

#### What's new?

We're adding these APIs to allow plugins to manage priority schemes. 

<table>
<colgroup>
<col style="width: 33%" />
<col style="width: 33%" />
<col style="width: 33%" />
</colgroup>
<thead>
<tr class="header">
<th>Classname</th>
<th>Method signature</th>
<th>Javadoc</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><code>com.atlassian.jira.issue.fields.config.manager.PrioritySchemeManager</code></td>
<td><p><code>FieldConfigScheme createWithDefaultMapping(String name, String description, List&lt;String&gt; optionIds);</code></p></td>
<td><a href="https://docs.atlassian.com/jira/7.6.0-EAP01/com/atlassian/jira/issue/fields/config/manager/PrioritySchemeManager.html#createWithDefaultMapping-java.lang.String-java.lang.String-java.util.List-" class="external-link">Doc</a></td>
</tr>
<tr class="even">
<td> </td>
<td><p><code>FieldConfigScheme updateWithDefaultMapping(FieldConfigScheme fieldConfigScheme, List&lt;String&gt; optionIds);</code></p></td>
<td><a href="https://docs.atlassian.com/jira/7.6.0-EAP01/com/atlassian/jira/issue/fields/config/manager/PrioritySchemeManager.html#updateWithDefaultMapping-com.atlassian.jira.issue.fields.config.FieldConfigScheme-java.util.List-" class="external-link">Doc</a></td>
</tr>
<tr class="odd">
<td> </td>
<td><p><code>FieldConfig getFieldConfigForDefaultMapping(FieldConfigScheme fieldConfigScheme);</code></p></td>
<td><a href="https://docs.atlassian.com/jira/7.6.0-EAP01/com/atlassian/jira/issue/fields/config/manager/PrioritySchemeManager.html#getFieldConfigForDefaultMapping-com.atlassian.jira.issue.fields.config.FieldConfigScheme-" class="external-link">Doc</a></td>
</tr>
<tr class="even">
<td> </td>
<td><code>void delete(FieldConfigScheme fieldConfigScheme);</code></td>
<td><a href="https://docs.atlassian.com/jira/7.6.0-EAP01/com/atlassian/jira/issue/fields/config/manager/PrioritySchemeManager.html#delete-com.atlassian.jira.issue.fields.config.FieldConfigScheme-" class="external-link">Doc</a></td>
</tr>
<tr class="odd">
<td> </td>
<td><p><code>FieldConfigScheme getScheme(Project project);</code></p></td>
<td><a href="https://docs.atlassian.com/jira/7.6.0-EAP01/com/atlassian/jira/issue/fields/config/manager/PrioritySchemeManager.html#getScheme-com.atlassian.jira.project.Project-" class="external-link">Doc</a></td>
</tr>
<tr class="even">
<td> </td>
<td><p><code>FieldConfigScheme getScheme(Long id);</code></p></td>
<td><a href="https://docs.atlassian.com/jira/7.6.0-EAP01/com/atlassian/jira/issue/fields/config/manager/PrioritySchemeManager.html#getScheme-java.lang.Long-" class="external-link">Doc</a></td>
</tr>
<tr class="odd">
<td> </td>
<td><code>List&lt;FieldConfigScheme&gt; getAllSchemes();</code></td>
<td><a href="https://docs.atlassian.com/jira/7.6.0-EAP01/com/atlassian/jira/issue/fields/config/manager/PrioritySchemeManager.html#getAllSchemes--" class="external-link">Doc</a></td>
</tr>
<tr class="even">
<td> </td>
<td><code>boolean isDefaultScheme(FieldConfigScheme fieldConfigScheme);</code></td>
<td><a href="https://docs.atlassian.com/jira/7.6.0-EAP01/com/atlassian/jira/issue/fields/config/manager/PrioritySchemeManager.html#isDefaultScheme-com.atlassian.jira.issue.fields.config.FieldConfigScheme-" class="external-link">Doc</a></td>
</tr>
<tr class="odd">
<td> </td>
<td><code>FieldConfigScheme getDefaultScheme();</code></td>
<td><a href="https://docs.atlassian.com/jira/7.6.0-EAP01/com/atlassian/jira/issue/fields/config/manager/PrioritySchemeManager.html#getDefaultScheme--" class="external-link">Doc</a></td>
</tr>
<tr class="even">
<td> </td>
<td><code>String getDefaultOption(IssueContext issueContext);</code></td>
<td><a href="https://docs.atlassian.com/jira/7.6.0-EAP01/com/atlassian/jira/issue/fields/config/manager/PrioritySchemeManager.html#getDefaultOption-com.atlassian.jira.issue.context.IssueContext-" class="external-link">Doc</a></td>
</tr>
<tr class="odd">
<td> </td>
<td><code>String getDefaultOption(FieldConfig fieldConfig);</code></td>
<td><a href="https://docs.atlassian.com/jira/7.6.0-EAP01/com/atlassian/jira/issue/fields/config/manager/PrioritySchemeManager.html#getDefaultOption-com.atlassian.jira.issue.fields.config.FieldConfig-" class="external-link">Doc</a></td>
</tr>
<tr class="even">
<td> </td>
<td><code>void setDefaultOption(FieldConfig fieldConfig, String optionId);</code></td>
<td><a href="https://docs.atlassian.com/jira/7.6.0-EAP01/com/atlassian/jira/issue/fields/config/manager/PrioritySchemeManager.html#setDefaultOption-com.atlassian.jira.issue.fields.config.FieldConfig-java.lang.String-" class="external-link">Doc</a></td>
</tr>
<tr class="odd">
<td> </td>
<td><code>List&lt;String&gt; getOptions(IssueContext issueContext);</code></td>
<td><a href="https://docs.atlassian.com/jira/7.6.0-EAP01/com/atlassian/jira/issue/fields/config/manager/PrioritySchemeManager.html#getOptions-com.atlassian.jira.issue.context.IssueContext-" class="external-link">Doc</a></td>
</tr>
<tr class="even">
<td> </td>
<td><code>List&lt;String&gt; getOptions(FieldConfig fieldConfig);</code></td>
<td><a href="https://docs.atlassian.com/jira/7.6.0-EAP01/com/atlassian/jira/issue/fields/config/manager/PrioritySchemeManager.html#getOptions-com.atlassian.jira.issue.fields.config.FieldConfig-" class="external-link">Doc</a></td>
</tr>
<tr class="odd">
<td> </td>
<td><code>void setOptions(FieldConfig fieldConfig, List&lt;String&gt; optionIds);</code></td>
<td><a href="https://docs.atlassian.com/jira/7.6.0-EAP01/com/atlassian/jira/issue/fields/config/manager/PrioritySchemeManager.html#getOptions-com.atlassian.jira.issue.fields.config.FieldConfig-" class="external-link">Doc</a></td>
</tr>
<tr class="even">
<td> </td>
<td><code>void addOptionToDefault(String optionId);</code></td>
<td><a href="https://docs.atlassian.com/jira/7.6.0-EAP01/com/atlassian/jira/issue/fields/config/manager/PrioritySchemeManager.html#addOptionToDefault-java.lang.String-" class="external-link">Doc</a></td>
</tr>
<tr class="odd">
<td> </td>
<td><code>void removeOptionFromAllSchemes(String optionId);</code></td>
<td><a href="https://docs.atlassian.com/jira/7.6.0-EAP01/com/atlassian/jira/issue/fields/config/manager/PrioritySchemeManager.html#removeOptionFromAllSchemes-java.lang.String-" class="external-link">Doc</a></td>
</tr>
<tr class="even">
<td> </td>
<td><code>Collection&lt;FieldConfigScheme&gt; getAllRelatedSchemes(String optionId);</code></td>
<td><a href="https://docs.atlassian.com/jira/7.6.0-EAP01/com/atlassian/jira/issue/fields/config/manager/PrioritySchemeManager.html#getAllRelatedSchemes-java.lang.String-" class="external-link">Doc</a></td>
</tr>
<tr class="odd">
<td> </td>
<td><code>void assignProject(@Nonnull FieldConfigScheme priorityFieldConfig, @Nonnull Project project);</code></td>
<td><a href="https://docs.atlassian.com/jira/7.6.0-EAP01/com/atlassian/jira/issue/fields/config/manager/PrioritySchemeManager.html#assignProject-com.atlassian.jira.issue.fields.config.FieldConfigScheme-com.atlassian.jira.project.Project-" class="external-link">Doc</a></td>
</tr>
<tr class="even">
<td> </td>
<td><code>Set&lt;Project&gt; getProjectsWithScheme(@Nonnull FieldConfigScheme fieldConfigScheme);</code></td>
<td><a href="https://docs.atlassian.com/jira/7.6.0-EAP01/com/atlassian/jira/issue/fields/config/manager/PrioritySchemeManager.html#getProjectsWithScheme-com.atlassian.jira.issue.fields.config.FieldConfigScheme-" class="external-link">Doc</a></td>
</tr>
<tr class="odd">
<td> </td>
<td><code>void unassignProject(@Nonnull FieldConfigScheme scheme, @Nonnull Project project);</code></td>
<td><a href="https://docs.atlassian.com/jira/7.6.0-EAP01/com/atlassian/jira/issue/fields/config/manager/PrioritySchemeManager.html#unassignProject-com.atlassian.jira.issue.fields.config.FieldConfigScheme-com.atlassian.jira.project.Project-" class="external-link">Doc</a></td>
</tr>
</tbody>
</table>

We're also adding new events for whenever a priority scheme is created, updated, or deleted.

<table>
<colgroup>
<col style="width: 33%" />
<col style="width: 33%" />
<col style="width: 33%" />
</colgroup>
<thead>
<tr class="header">
<th>Classname</th>
<th>Event</th>
<th>Javadoc</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><code>com.atlassian.jira.issue.fields.config.manager.PrioritySchemeManager</code></td>
<td><code>com.atlassian.jira.event.issue.fields.config.manager.PrioritySchemeCreatedEvent</code></td>
<td><a href="https://docs.atlassian.com/jira/7.6.0-EAP01/com/atlassian/jira/event/issue/field/config/manager/PrioritySchemeCreatedEvent.html" class="external-link">Doc</a></td>
</tr>
<tr class="even">
<td> </td>
<td><p><code>com.atlassian.jira.event.issue.fields.config.manager.PrioritySchemeDeletedEvent</code></p></td>
<td><a href="https://docs.atlassian.com/jira/7.6.0-EAP01/com/atlassian/jira/event/issue/field/config/manager/PrioritySchemeDeletedEvent.html" class="external-link">Doc</a></td>
</tr>
<tr class="odd">
<td> </td>
<td><code>com.atlassian.jira.event.issue.fields.config.manager.PrioritySchemeUpdatedEvent</code></td>
<td><a href="https://docs.atlassian.com/jira/7.6.0-EAP01/com/atlassian/jira/event/issue/field/config/manager/PrioritySchemeUpdatedEvent.html" class="external-link">Doc</a></td>
</tr>
</tbody>
</table>

#### What's changing?

Some APIs also receive additional options, so that priorities are added or removed from priority schemes.

| Classname                                   | Method signature | Change                                                                                      | Javadoc                                                                                                                                                                                                                |
|---------------------------------------------|------------------|---------------------------------------------------------------------------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `com.atlassian.jira.config.PriorityManager` | `createPriority` | In addition to creating a priority, also adds this priority to the default priority scheme. | <a href="https://docs.atlassian.com/jira/7.6.0-EAP01/com/atlassian/jira/config/PriorityManager.html#createPriority-java.lang.String-java.lang.String-java.lang.String-java.lang.String-" class="external-link">Doc</a> |
|                                             | `removePriority` | In addition to removing a priority, also removes this priority from all priority schemes.   | <a href="https://docs.atlassian.com/jira/7.6.0-EAP01/com/atlassian/jira/config/PriorityManager.html#removePriority-java.lang.String-java.lang.String-" class="external-link">Doc</a>                                   |

#### What's deprecated?

These APIs are deprecated, and replaced by the new PrioritySchemeManager class.

<table>
<colgroup>
<col style="width: 25%" />
<col style="width: 25%" />
<col style="width: 25%" />
<col style="width: 25%" />
</colgroup>
<thead>
<tr class="header">
<th>Classname</th>
<th>Method signature</th>
<th>Replacement</th>
<th>Javadoc</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><code>com.atlassian.jira.config.PriorityManager</code></td>
<td><code>setDefaultPriority</code></td>
<td><p><code>com.atlassian.jira.issue.fields.config.manager.PrioritySchemeManager</code></p>
<p><code>#setDefaultOption(FieldConfig, String)</code></p></td>
<td><a href="https://docs.atlassian.com/jira/7.6.0-EAP01/com/atlassian/jira/config/PriorityManager.html#setDefaultPriority-java.lang.String-" class="external-link">Doc</a></td>
</tr>
<tr class="even">
<td> </td>
<td><code>getDefaultPriority</code></td>
<td><p><code>com.atlassian.jira.issue.fields.config.manager.PrioritySchemeManager</code></p>
<p><code>#getDefaultOption(IssueContext)</code></p></td>
<td><a href="https://docs.atlassian.com/jira/7.6.0-EAP01/com/atlassian/jira/config/PriorityManager.html#getDefaultPriority--" class="external-link">Doc</a></td>
</tr>
<tr class="odd">
<td><code>com.atlassian.jira.config.ConstantsManager</code></td>
<td><code>getDefaultPriority</code></td>
<td><p><code>com.atlassian.jira.issue.fields.config.manager.PrioritySchemeManager</code></p>
<p><code>#getDefaultOption(com.atlassian.jira.issue.context.IssueContext)</code>, or</p>
<p><code>com.atlassian.jira.issue.fields.config.manager.PrioritySchemeManager</code></p>
<p><code>#getDefaultOption(com.atlassian.jira.issue.fields.config.FieldConfig)</code><br />
</p></td>
<td><a href="https://docs.atlassian.com/jira/7.6.0-EAP01/com/atlassian/jira/config/ConstantsManager.html#getDefaultPriority--" class="external-link">Doc</a></td>
</tr>
</tbody>
</table>

### Content-Security-Policy and X-Frame-Options headers

To prevent clickjacking, JIRA will add the `X-Frame-Options` and `Content-Security-Policy` security headers to each HTTP response. The headers will block the content from being embedded in iframes, which might also affect pages that you actually wanted to be displayed this way. If you embed any resources from JIRA on other sites (e.g. dashboards), they might not work after the upgrade. To fix this, you can either disable this feature, or choose resources to be excluded from receiving the security headers. <a href="https://confluence.atlassian.com/pages/viewpage.action?pageId=939919914" class="external-link">Learn more</a>

 

------------------------------------------------------------------------

 

## JIRA Service Desk changes

JIRA Service Desk has a number of elements of its API which are currently defined as experimental but that is about to change. With the 3.7.0 release a number of components were either deprecated for removal, or updated with details about how they will represent a breaking change in a future release. Well, those changes are coming now in the JIRA Service Desk 3.9.0 release. This release will also be moving the majority of the components in our API out of the experimental state and into a standard public API state. 

#### What's Changing?

A number of components will have breaking changes. The changes are relatively minor and revolve around changes to IDs that will be returning integers rather than longs. This helps us to align with the values that we are currently storing in the database and will stop us having to do a lot of unnecessary type conversion in the future. 

<table style="width:100%;">
<colgroup>
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
</colgroup>
<thead>
<tr class="header">
<th><p>Classname</p></th>
<th><p>Breaking change</p></th>
<th><p>Old method signature</p></th>
<th><p>Old javadoc</p></th>
<th><p>New method signature</p></th>
<th><p>New javadoc</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>ServiceDeskService</p></td>
<td><p>The getServiceDeskById method will require an integer rather than a long as the serviceDeskId.</p></td>
<td><p><code>Either&lt;AnError, ServiceDesk&gt; getServiceDeskById(ApplicationUser user, long </code><code class="sourceCode java">serviceDeskId);</code></p></td>
<td><p><a href="https://docs.atlassian.com/jira-servicedesk/3.6.2-REL-0004/com/atlassian/servicedesk/api/ServiceDeskService.html#getServiceDeskById-com.atlassian.jira.user.ApplicationUser-long-" class="external-link">https://docs.atlassian.com/jira-servicedesk/3.7.0/com/atlassian/servicedesk/api/ServiceDeskService.html#getServiceDeskById-com.atlassian.jira.user.ApplicationUser-long-</a></p></td>
<td><p><code class="sourceCode java">Either&lt;AnError, ServiceDesk&gt; <span class="fu">getServiceDeskById</span>(ApplicationUser user, int </code><code class="sourceCode java">serviceDeskId);</code></p></td>
<td><p><a href="https://docs.atlassian.com/jira-servicedesk/3.9.0-m0124/com/atlassian/servicedesk/api/ServiceDeskService.html#getServiceDeskById-com.atlassian.jira.user.ApplicationUser-int-" class="uri external-link">https://docs.atlassian.com/jira-servicedesk/3.9.0-m0124/com/atlassian/servicedesk/api/ServiceDeskService.html#getServiceDeskById-com.atlassian.jira.user.ApplicationUser-int-</a></p></td>
</tr>
<tr class="even">
<td><p>PortalService</p></td>
<td><p>The getPortalForId method will request and integer rather than a long as the portalId.</p></td>
<td><p><code class="sourceCode java">Either&lt;AnError, Portal&gt; <span class="fu">getPortalForId</span>(ApplicationUser user, </code><code class="sourceCode java"><span class="dt">long</span></code><code class="sourceCode java">portalId);</code></p></td>
<td><p><a href="https://docs.atlassian.com/jira-servicedesk/3.6.2-REL-0004/com/atlassian/servicedesk/api/portal/PortalService.html#getPortalForId-com.atlassian.jira.user.ApplicationUser-long-" class="external-link">https://docs.atlassian.com/jira-servicedesk/3.7.0/com/atlassian/servicedesk/api/portal/PortalService.html#getPortalForId-com.atlassian.jira.user.ApplicationUser-long-</a></p></td>
<td><p><code class="sourceCode java">Either&lt;AnError, Portal&gt; <span class="fu">getPortalForId</span>(ApplicationUser user, </code><code class="sourceCode java"><span class="dt">int</span></code><code class="sourceCode java">portalId);</code></p></td>
<td><p><a href="https://docs.atlassian.com/jira-servicedesk/3.9.0-m0124/com/atlassian/servicedesk/api/portal/PortalService.html#getPortalForId-com.atlassian.jira.user.ApplicationUser-int-" class="uri external-link">https://docs.atlassian.com/jira-servicedesk/3.9.0-m0124/com/atlassian/servicedesk/api/portal/PortalService.html#getPortalForId-com.atlassian.jira.user.ApplicationUser-int-</a></p></td>
</tr>
<tr class="odd">
<td><p>Queue</p></td>
<td><p>The return value for this method will return an integer rather than a long.</p></td>
<td><p><code>long getId()</code></p></td>
<td><p><a href="https://docs.atlassian.com/jira-servicedesk/3.6.2-REL-0004/com/atlassian/servicedesk/api/queue/Queue.html#getId--" class="external-link">https://docs.atlassian.com/jira-servicedesk/3.7.0/com/atlassian/servicedesk/api/queue/Queue.html#getId--</a></p></td>
<td><p>int getId()</p></td>
<td><p><a href="https://docs.atlassian.com/jira-servicedesk/3.9.0-m0124/com/atlassian/servicedesk/api/queue/Queue.html#getId--" class="uri external-link">https://docs.atlassian.com/jira-servicedesk/3.9.0-m0124/com/atlassian/servicedesk/api/queue/Queue.html#getId--</a></p></td>
</tr>
<tr class="even">
<td><p>The return value for this method will return an integer rather than a long.</p></td>
<td><p><code>long getServiceDeskId()</code></p></td>
<td><p><a href="https://docs.atlassian.com/jira-servicedesk/3.6.2-REL-0004/com/atlassian/servicedesk/api/queue/Queue.html#getServiceDeskId--" class="external-link">https://docs.atlassian.com/jira-servicedesk/3.7.0/com/atlassian/servicedesk/api/queue/Queue.html#getServiceDeskId--</a></p></td>
<td><p>int getServiceDeskId()</p></td>
<td><p><a href="https://docs.atlassian.com/jira-servicedesk/3.9.0-m0124/com/atlassian/servicedesk/api/queue/Queue.html#getServiceDeskId--" class="uri external-link">https://docs.atlassian.com/jira-servicedesk/3.9.0-m0124/com/atlassian/servicedesk/api/queue/Queue.html#getServiceDeskId--</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>QueueQuery</p></td>
<td><p>The return value for this method will return an integer rather than a long.</p></td>
<td><p><code>Optional&lt;Long&gt; queueId();</code></p></td>
<td><p><a href="https://docs.atlassian.com/jira-servicedesk/3.7.0/com/atlassian/servicedesk/api/queue/QueueQuery.html#queueId--" class="uri external-link">https://docs.atlassian.com/jira-servicedesk/3.7.0/com/atlassian/servicedesk/api/queue/QueueQuery.html#queueId--</a></p></td>
<td><p>Optional&lt;Integer&gt; queueId();</p></td>
<td><p><a href="https://docs.atlassian.com/jira-servicedesk/3.9.0-m0124/com/atlassian/servicedesk/api/queue/QueueQuery.html#queueId--" class="uri external-link">https://docs.atlassian.com/jira-servicedesk/3.9.0-m0124/com/atlassian/servicedesk/api/queue/QueueQuery.html#queueId--</a></p></td>
</tr>
<tr class="even">
<td><p>The return value for this method will return an integer rather than a long.</p></td>
<td><p><code class="sourceCode java"><span class="dt">long</span></code><code class="sourceCode java"><span class="fu">serviceDeskId</span>();</code></p></td>
<td><p><a href="https://docs.atlassian.com/jira-servicedesk/3.7.0/com/atlassian/servicedesk/api/queue/QueueQuery.html#serviceDeskId--" class="uri external-link">https://docs.atlassian.com/jira-servicedesk/3.7.0/com/atlassian/servicedesk/api/queue/QueueQuery.html#serviceDeskId--</a></p></td>
<td><p><code class="sourceCode java"><span class="dt">int</span></code><code class="sourceCode java"><span class="fu">serviceDeskId</span>();</code></p></td>
<td><p><a href="https://docs.atlassian.com/jira-servicedesk/3.9.0-m0124/com/atlassian/servicedesk/api/queue/QueueQuery.html#serviceDeskId--" class="uri external-link">https://docs.atlassian.com/jira-servicedesk/3.9.0-m0124/com/atlassian/servicedesk/api/queue/QueueQuery.html#serviceDeskId--</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>QueueQuery Builder interface</p></td>
<td><p>The serviceDeskID interface will require an integer rather than a long as the serviceDeskId</p></td>
<td><p><code class="sourceCode java">Builder <span class="fu">serviceDeskId</span>(</code><code class="sourceCode java"><span class="dt">long</span></code><code class="sourceCode java">serviceDeskId);</code></p></td>
<td><p><a href="https://docs.atlassian.com/jira-servicedesk/3.7.0/com/atlassian/servicedesk/api/queue/QueueQuery.Builder.html#serviceDeskId-long-" class="uri external-link">https://docs.atlassian.com/jira-servicedesk/3.7.0/com/atlassian/servicedesk/api/queue/QueueQuery.Builder.html#serviceDeskId-long-</a></p></td>
<td><p><code class="sourceCode java">Builder <span class="fu">serviceDeskId</span>(</code><code class="sourceCode java"><span class="dt">int</span></code><code class="sourceCode java">serviceDeskId);</code></p></td>
<td><p><a href="https://docs.atlassian.com/jira-servicedesk/3.9.0-m0124/com/atlassian/servicedesk/api/queue/QueueQuery.Builder.html#serviceDeskId-int-" class="uri external-link">https://docs.atlassian.com/jira-servicedesk/3.9.0-m0124/com/atlassian/servicedesk/api/queue/QueueQuery.Builder.html#serviceDeskId-int-</a></p></td>
</tr>
<tr class="even">
<td><p>The queueId interface will require an integer rather than a long as the queueId</p></td>
<td><p><code class="sourceCode java">Builder <span class="fu">queueId</span>(</code><code class="sourceCode java"><span class="dt">long</span></code><code class="sourceCode java">queueId);</code></p></td>
<td><p><a href="https://docs.atlassian.com/jira-servicedesk/3.7.0/com/atlassian/servicedesk/api/queue/QueueQuery.Builder.html#queueId-long-" class="uri external-link">https://docs.atlassian.com/jira-servicedesk/3.7.0/com/atlassian/servicedesk/api/queue/QueueQuery.Builder.html#queueId-long-</a></p></td>
<td><p><code class="sourceCode java">Builder <span class="fu">queueId</span>(</code><code class="sourceCode java"><span class="dt">int</span></code><code class="sourceCode java">queueId);</code></p></td>
<td><p><a href="https://docs.atlassian.com/jira-servicedesk/3.9.0-m0124/com/atlassian/servicedesk/api/queue/QueueQuery.Builder.html#queueId-int-" class="uri external-link">https://docs.atlassian.com/jira-servicedesk/3.9.0-m0124/com/atlassian/servicedesk/api/queue/QueueQuery.Builder.html#queueId-int-</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>QueueRequestQuery</p></td>
<td><p>The return values for this method will return an integer rather than a long</p></td>
<td><p><code class="sourceCode java"><span class="dt">long</span></code><code class="sourceCode java"><span class="fu">queueId</span>();</code></p></td>
<td><p><a href="https://docs.atlassian.com/jira-servicedesk/3.7.0/com/atlassian/servicedesk/api/queue/QueueRequestQuery.html#queueId--" class="uri external-link">https://docs.atlassian.com/jira-servicedesk/3.7.0/com/atlassian/servicedesk/api/queue/QueueRequestQuery.html#queueId--</a></p></td>
<td><p><code class="sourceCode java"><span class="dt">int</span></code><code class="sourceCode java"><span class="fu">queueId</span>();</code></p></td>
<td><p><a href="https://docs.atlassian.com/jira-servicedesk/3.9.0-m0124/com/atlassian/servicedesk/api/queue/QueueRequestQuery.html#queueId--" class="uri external-link">https://docs.atlassian.com/jira-servicedesk/3.9.0-m0124/com/atlassian/servicedesk/api/queue/QueueRequestQuery.html#queueId--</a></p></td>
</tr>
<tr class="even">
<td><p>The return values for this method will return an integer rather than a long</p></td>
<td><p><code class="sourceCode java"><span class="dt">long</span></code><code class="sourceCode java"><span class="fu">serviceDeskId</span>();</code></p></td>
<td><p><a href="https://docs.atlassian.com/jira-servicedesk/3.7.0/com/atlassian/servicedesk/api/queue/QueueRequestQuery.html#serviceDeskId--" class="uri external-link">https://docs.atlassian.com/jira-servicedesk/3.7.0/com/atlassian/servicedesk/api/queue/QueueRequestQuery.html#serviceDeskId--</a></p></td>
<td><p><code class="sourceCode java"><span class="dt">int</span></code><code class="sourceCode java"><span class="fu">serviceDeskId</span>();</code></p></td>
<td><p><a href="https://docs.atlassian.com/jira-servicedesk/3.9.0-m0124/com/atlassian/servicedesk/api/queue/QueueRequestQuery.html#serviceDeskId--" class="uri external-link">https://docs.atlassian.com/jira-servicedesk/3.9.0-m0124/com/atlassian/servicedesk/api/queue/QueueRequestQuery.html#serviceDeskId--</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>QueueRequestQuery Builder interface</p></td>
<td><p>The serviceDeskID interface will require an integer rather than a long as the serviceDeskId</p></td>
<td><p><code class="sourceCode java">Builder <span class="fu">serviceDeskId</span>(</code><code class="sourceCode java"><span class="dt">long</span></code><code class="sourceCode java">serviceDeskId);</code></p></td>
<td><p><a href="https://docs.atlassian.com/jira-servicedesk/3.7.0/com/atlassian/servicedesk/api/queue/QueueRequestQuery.Builder.html#serviceDeskId-long-" class="uri external-link">https://docs.atlassian.com/jira-servicedesk/3.7.0/com/atlassian/servicedesk/api/queue/QueueRequestQuery.Builder.html#serviceDeskId-long-</a></p></td>
<td><p><code class="sourceCode java">Builder <span class="fu">serviceDeskId</span>(</code><code class="sourceCode java"><span class="dt">int</span></code><code class="sourceCode java">serviceDeskId);</code></p></td>
<td><p><a href="https://docs.atlassian.com/jira-servicedesk/3.9.0-m0124/com/atlassian/servicedesk/api/queue/QueueRequestQuery.Builder.html#serviceDeskId-int-" class="uri external-link">https://docs.atlassian.com/jira-servicedesk/3.9.0-m0124/com/atlassian/servicedesk/api/queue/QueueRequestQuery.Builder.html#serviceDeskId-int-</a></p></td>
</tr>
<tr class="even">
<td><p>The queueId interface will require an integer rather than a long as the queueId</p></td>
<td><p><code class="sourceCode java">Builder <span class="fu">queueId</span>(</code><code class="sourceCode java"><span class="dt">long</span></code><code class="sourceCode java">queueId);</code></p></td>
<td><p><a href="https://docs.atlassian.com/jira-servicedesk/3.7.0/com/atlassian/servicedesk/api/queue/QueueRequestQuery.Builder.html#queueId-long-" class="uri external-link">https://docs.atlassian.com/jira-servicedesk/3.7.0/com/atlassian/servicedesk/api/queue/QueueRequestQuery.Builder.html#queueId-long-</a></p></td>
<td><p><code class="sourceCode java">Builder <span class="fu">queueId</span>(</code><code class="sourceCode java"><span class="dt">int</span></code><code class="sourceCode java">queueId);</code></p></td>
<td><p><a href="https://docs.atlassian.com/jira-servicedesk/3.9.0-m0124/com/atlassian/servicedesk/api/queue/QueueRequestQuery.Builder.html#queueId-int-" class="uri external-link">https://docs.atlassian.com/jira-servicedesk/3.9.0-m0124/com/atlassian/servicedesk/api/queue/QueueRequestQuery.Builder.html#queueId-int-</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>RequestType</p></td>
<td><p>The getPortalId method will return an integer instead of a long.</p></td>
<td><p><code class="sourceCode java"><span class="dt">long</span></code><code class="sourceCode java"><span class="fu">getPortalId</span>();</code></p></td>
<td><p><a href="https://docs.atlassian.com/jira-servicedesk/3.7.0/com/atlassian/servicedesk/api/requesttype/RequestType.html#getPortalId--" class="uri external-link">https://docs.atlassian.com/jira-servicedesk/3.7.0/com/atlassian/servicedesk/api/requesttype/RequestType.html#getPortalId--</a></p></td>
<td><p><code class="sourceCode java"><span class="dt">int</span></code><code class="sourceCode java"><span class="fu">getPortalId</span>();</code></p></td>
<td><p><a href="https://docs.atlassian.com/jira-servicedesk/3.9.0-m0124/com/atlassian/servicedesk/api/requesttype/RequestType.html#getPortalId--" class="uri external-link">https://docs.atlassian.com/jira-servicedesk/3.9.0-m0124/com/atlassian/servicedesk/api/requesttype/RequestType.html#getPortalId--</a></p></td>
</tr>
<tr class="even">
<td><p>SlaInformation</p></td>
<td><p>The getId method will return an integer instead of a long.</p></td>
<td><p><code>long getId();</code></p></td>
<td><p><a href="https://docs.atlassian.com/jira-servicedesk/3.7.0/com/atlassian/servicedesk/api/sla/info/SlaInformation.html#getId--" class="uri external-link">https://docs.atlassian.com/jira-servicedesk/3.7.0/com/atlassian/servicedesk/api/sla/info/SlaInformation.html#getId--</a></p></td>
<td><p>int getId();</p></td>
<td><p><a href="https://docs.atlassian.com/jira-servicedesk/3.9.0-m0124/com/atlassian/servicedesk/api/sla/info/SlaInformation.html#getId--" class="uri external-link">https://docs.atlassian.com/jira-servicedesk/3.9.0-m0124/com/atlassian/servicedesk/api/sla/info/SlaInformation.html#getId--</a></p></td>
</tr>
<tr class="odd">
<td><p>SlaInformationQuery</p></td>
<td><p>The id method will return an integerinstead of a long.</p></td>
<td><p>Optional&lt;Long&gt; id();</p></td>
<td><p><a href="https://docs.atlassian.com/jira-servicedesk/3.7.0/com/atlassian/servicedesk/api/sla/info/SlaInformationQuery.html#id--" class="uri external-link">https://docs.atlassian.com/jira-servicedesk/3.7.0/com/atlassian/servicedesk/api/sla/info/SlaInformationQuery.html#id--</a></p></td>
<td><p>Optional&lt;Integer&gt; id();</p></td>
<td><p><a href="https://docs.atlassian.com/jira-servicedesk/3.9.0-m0124/com/atlassian/servicedesk/api/sla/info/SlaInformationQuery.html#id--" class="uri external-link">https://docs.atlassian.com/jira-servicedesk/3.9.0-m0124/com/atlassian/servicedesk/api/sla/info/SlaInformationQuery.html#id--</a></p></td>
</tr>
<tr class="even">
<td><p>SlaInformationQuery Builder</p></td>
<td><p>The id interface will require an integer instead of a long.</p></td>
<td><p>Builder id(Long slaMetricId);</p></td>
<td><p><a href="https://docs.atlassian.com/jira-servicedesk/3.7.0/com/atlassian/servicedesk/api/sla/info/SlaInformationQuery.Builder.html#id-java.lang.Long-" class="uri external-link">https://docs.atlassian.com/jira-servicedesk/3.7.0/com/atlassian/servicedesk/api/sla/info/SlaInformationQuery.Builder.html#id-java.lang.Long-</a></p></td>
<td><p>Builder id(Integer slaMetricId);</p></td>
<td><p><a href="https://docs.atlassian.com/jira-servicedesk/3.9.0-m0124/com/atlassian/servicedesk/api/sla/info/SlaInformationQuery.Builder.html#id-java.lang.Integer-" class="uri external-link">https://docs.atlassian.com/jira-servicedesk/3.9.0-m0124/com/atlassian/servicedesk/api/sla/info/SlaInformationQuery.Builder.html#id-java.lang.Integer-</a></p></td>
</tr>
</tbody>
</table>

#### What's been removed?

As well as the breaking changes listed above, we've also removed a number of existing experimental API that were deprecated previously. This is being done to make the new public API state, both cleaner and more useable.What's been removed? 

The following classes have been removed:

-   CreateInternalCommentParameters

-   CreatePublicCommentParameters

-   ServiceDeskCommentParameters

-   ValidatedInternalCommentParameters

-   ValidatedPublicCommentParameters

-   RequestTypeUpdateParameters

-   RequestTypeQueryParameters

In addition, the following methods have been removed. Most of these have equivalents which can now be used as a direct replacement. The ones that don't have been deemed insufficient for making public, as the usage was not recommended.

<table>
<colgroup>
<col style="width: 20%" />
<col style="width: 20%" />
<col style="width: 20%" />
<col style="width: 20%" />
<col style="width: 20%" />
</colgroup>
<thead>
<tr class="header">
<th><p>Classname</p></th>
<th><p>Method signature</p></th>
<th><p>Old javadoc</p></th>
<th><p>Replacement</p></th>
<th><p>New javadoc</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>ServiceDeskCommentService</p></td>
<td><p><code class="sourceCode java">Either&lt;AnError, ValidatedPublicCommentParameters&gt; <span class="fu">validatePublicComment</span>(</code><code class="sourceCode java"><span class="at">@Nonnull</span></code><code class="sourceCode java">ApplicationUser user, </code><code class="sourceCode java"><span class="at">@Nonnull</span></code><code class="sourceCode java">CreatePublicCommentParameters commentInternalParameters);</code></p></td>
<td><p><a href="https://docs.atlassian.com/jira-servicedesk/3.7.0/com/atlassian/servicedesk/api/comment/ServiceDeskCommentService.html#validatePublicComment-com.atlassian.jira.user.ApplicationUser-com.atlassian.servicedesk.api.comment.CreatePublicCommentParameters-" class="uri external-link">https://docs.atlassian.com/jira-servicedesk/3.7.0/com/atlassian/servicedesk/api/comment/ServiceDeskCommentService.html#validatePublicComment-com.atlassian.jira.user.ApplicationUser-com.atlassian.servicedesk.api.comment.CreatePublicCommentParameters-</a></p></td>
<td><p><code>#createServiceDeskComment(ApplicationUser, ServiceDeskCommentCreateParameters)</code></p></td>
<td><p><a href="https://docs.atlassian.com/jira-servicedesk/3.9.0-m0124/com/atlassian/servicedesk/api/comment/ServiceDeskCommentService.html#createServiceDeskComment-com.atlassian.jira.user.ApplicationUser-com.atlassian.servicedesk.api.comment.ServiceDeskCommentCreateParameters-" class="uri external-link">https://docs.atlassian.com/jira-servicedesk/3.9.0-m0124/com/atlassian/servicedesk/api/comment/ServiceDeskCommentService.html#createServiceDeskComment-com.atlassian.jira.user.ApplicationUser-com.atlassian.servicedesk.api.comment.ServiceDeskCommentCreateParameters-</a></p></td>
</tr>
<tr class="even">
<td><p><code class="sourceCode java">Either&lt;AnError, ValidatedInternalCommentParameters&gt; <span class="fu">validateInternalComment</span>(</code><code class="sourceCode java"><span class="at">@Nonnull</span></code><code class="sourceCode java">ApplicationUser user, </code><code class="sourceCode java"><span class="at">@Nonnull</span></code><code class="sourceCode java">CreateInternalCommentParameters commentPublicParameters);</code></p></td>
<td><p><a href="https://docs.atlassian.com/jira-servicedesk/3.7.0/com/atlassian/servicedesk/api/comment/ServiceDeskCommentService.html#validateInternalComment-com.atlassian.jira.user.ApplicationUser-com.atlassian.servicedesk.api.comment.CreateInternalCommentParameters-" class="uri external-link">https://docs.atlassian.com/jira-servicedesk/3.7.0/com/atlassian/servicedesk/api/comment/ServiceDeskCommentService.html#validateInternalComment-com.atlassian.jira.user.ApplicationUser-com.atlassian.servicedesk.api.comment.CreateInternalCommentParameters-</a></p></td>
<td><p><code>#createServiceDeskComment(ApplicationUser, ServiceDeskCommentCreateParameters)</code></p></td>
<td><p><a href="https://docs.atlassian.com/jira-servicedesk/3.9.0-m0124/com/atlassian/servicedesk/api/comment/ServiceDeskCommentService.html#createServiceDeskComment-com.atlassian.jira.user.ApplicationUser-com.atlassian.servicedesk.api.comment.ServiceDeskCommentCreateParameters-" class="uri external-link">https://docs.atlassian.com/jira-servicedesk/3.9.0-m0124/com/atlassian/servicedesk/api/comment/ServiceDeskCommentService.html#createServiceDeskComment-com.atlassian.jira.user.ApplicationUser-com.atlassian.servicedesk.api.comment.ServiceDeskCommentCreateParameters-</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><code class="sourceCode java">Either&lt;AnError, ServiceDeskComment&gt; <span class="fu">createPublicComment</span>(</code><code class="sourceCode java"><span class="at">@Nonnull</span></code><code class="sourceCode java">ApplicationUser user, </code><code class="sourceCode java"><span class="at">@Nonnull</span></code><code class="sourceCode java">ValidatedPublicCommentParameters commentParameters);</code></p></td>
<td><p><a href="https://docs.atlassian.com/jira-servicedesk/3.7.0/com/atlassian/servicedesk/api/comment/ServiceDeskCommentService.html#createPublicComment-com.atlassian.jira.user.ApplicationUser-com.atlassian.servicedesk.api.comment.ValidatedPublicCommentParameters-" class="uri external-link">https://docs.atlassian.com/jira-servicedesk/3.7.0/com/atlassian/servicedesk/api/comment/ServiceDeskCommentService.html#createPublicComment-com.atlassian.jira.user.ApplicationUser-com.atlassian.servicedesk.api.comment.ValidatedPublicCommentParameters-</a></p></td>
<td><p><code>#createServiceDeskComment(ApplicationUser, ServiceDeskCommentCreateParameters)</code></p></td>
<td><p><a href="https://docs.atlassian.com/jira-servicedesk/3.9.0-m0124/com/atlassian/servicedesk/api/comment/ServiceDeskCommentService.html#createServiceDeskComment-com.atlassian.jira.user.ApplicationUser-com.atlassian.servicedesk.api.comment.ServiceDeskCommentCreateParameters-" class="uri external-link">https://docs.atlassian.com/jira-servicedesk/3.9.0-m0124/com/atlassian/servicedesk/api/comment/ServiceDeskCommentService.html#createServiceDeskComment-com.atlassian.jira.user.ApplicationUser-com.atlassian.servicedesk.api.comment.ServiceDeskCommentCreateParameters-</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><code class="sourceCode java">Either&lt;AnError, ServiceDeskComment&gt; <span class="fu">createInternalComment</span>(</code><code class="sourceCode java"><span class="at">@Nonnull</span></code><code class="sourceCode java">ApplicationUser user, </code><code class="sourceCode java"><span class="at">@Nonnull</span></code><code class="sourceCode java">ValidatedInternalCommentParameters commentParameters);</code></p></td>
<td><p><a href="https://docs.atlassian.com/jira-servicedesk/3.7.0/com/atlassian/servicedesk/api/comment/ServiceDeskCommentService.html#createInternalComment-com.atlassian.jira.user.ApplicationUser-com.atlassian.servicedesk.api.comment.ValidatedInternalCommentParameters-" class="uri external-link">https://docs.atlassian.com/jira-servicedesk/3.7.0/com/atlassian/servicedesk/api/comment/ServiceDeskCommentService.html#createInternalComment-com.atlassian.jira.user.ApplicationUser-com.atlassian.servicedesk.api.comment.ValidatedInternalCommentParameters-</a></p></td>
<td><p><code>#createServiceDeskComment(ApplicationUser, ServiceDeskCommentCreateParameters)</code></p></td>
<td><p><a href="https://docs.atlassian.com/jira-servicedesk/3.9.0-m0124/com/atlassian/servicedesk/api/comment/ServiceDeskCommentService.html#createServiceDeskComment-com.atlassian.jira.user.ApplicationUser-com.atlassian.servicedesk.api.comment.ServiceDeskCommentCreateParameters-" class="uri external-link">https://docs.atlassian.com/jira-servicedesk/3.9.0-m0124/com/atlassian/servicedesk/api/comment/ServiceDeskCommentService.html#createServiceDeskComment-com.atlassian.jira.user.ApplicationUser-com.atlassian.servicedesk.api.comment.ServiceDeskCommentCreateParameters-</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>CustomerContextService</p></td>
<td><p><code>&lt;T&gt; T inCustomerContext(Callable&lt;T&gt; callable);</code></p></td>
<td><p><a href="https://docs.atlassian.com/jira-servicedesk/3.7.0/com/atlassian/servicedesk/api/customer/CustomerContextService.html#inCustomerContext-java.util.concurrent.Callable-" class="uri external-link">https://docs.atlassian.com/jira-servicedesk/3.7.0/com/atlassian/servicedesk/api/customer/CustomerContextService.html#inCustomerContext-java.util.concurrent.Callable-</a></p></td>
<td><p><code>#runInCustomerContext(NoExceptionsCallable)</code></p></td>
<td><p><a href="https://docs.atlassian.com/jira-servicedesk/3.9.0-m0124/com/atlassian/servicedesk/api/customer/CustomerContextService.html#runInCustomerContext-com.atlassian.servicedesk.api.customer.NoExceptionsCallable-" class="uri external-link">https://docs.atlassian.com/jira-servicedesk/3.9.0-m0124/com/atlassian/servicedesk/api/customer/CustomerContextService.html#runInCustomerContext-com.atlassian.servicedesk.api.customer.NoExceptionsCallable-</a></p></td>
</tr>
<tr class="even">
<td><p><code class="sourceCode java"><span class="dt">void</span></code><code class="sourceCode java"><span class="fu">inCustomerContext</span>(<span class="bu">Runnable</span> runnable);</code></p></td>
<td><p><a href="https://docs.atlassian.com/jira-servicedesk/3.7.0/com/atlassian/servicedesk/api/customer/CustomerContextService.html#inCustomerContext-java.lang.Runnable-" class="uri external-link">https://docs.atlassian.com/jira-servicedesk/3.7.0/com/atlassian/servicedesk/api/customer/CustomerContextService.html#inCustomerContext-java.lang.Runnable-</a></p></td>
<td><p><code>#runInCustomerContext(Runnable)</code></p></td>
<td><p><a href="https://docs.atlassian.com/jira-servicedesk/3.9.0-m0124/com/atlassian/servicedesk/api/customer/CustomerContextService.html#runInCustomerContext-java.lang.Runnable-" class="uri external-link">https://docs.atlassian.com/jira-servicedesk/3.9.0-m0124/com/atlassian/servicedesk/api/customer/CustomerContextService.html#runInCustomerContext-java.lang.Runnable-</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><code>&lt;T&gt; T outOfCustomerContext(Callable&lt;T&gt; callable);</code></p></td>
<td><p><a href="https://docs.atlassian.com/jira-servicedesk/3.7.0/com/atlassian/servicedesk/api/customer/CustomerContextService.html#outOfCustomerContext-java.util.concurrent.Callable-" class="uri external-link">https://docs.atlassian.com/jira-servicedesk/3.7.0/com/atlassian/servicedesk/api/customer/CustomerContextService.html#outOfCustomerContext-java.util.concurrent.Callable-</a></p></td>
<td><p><code>#runOutOfCustomerContext(NoExceptionsCallable)</code></p></td>
<td><p><a href="https://docs.atlassian.com/jira-servicedesk/3.9.0-m0124/com/atlassian/servicedesk/api/customer/CustomerContextService.html#runOutOfCustomerContext-com.atlassian.servicedesk.api.customer.NoExceptionsCallable-" class="uri external-link">https://docs.atlassian.com/jira-servicedesk/3.9.0-m0124/com/atlassian/servicedesk/api/customer/CustomerContextService.html#runOutOfCustomerContext-com.atlassian.servicedesk.api.customer.NoExceptionsCallable-</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><code class="sourceCode java"><span class="dt">void</span></code> outOfCustomerContext<code class="sourceCode java">(<span class="bu">Runnable</span> runnable);</code></p></td>
<td><p><a href="https://docs.atlassian.com/jira-servicedesk/3.7.0/com/atlassian/servicedesk/api/customer/CustomerContextService.html#outOfCustomerContext-java.lang.Runnable-" class="uri external-link">https://docs.atlassian.com/jira-servicedesk/3.7.0/com/atlassian/servicedesk/api/customer/CustomerContextService.html#outOfCustomerContext-java.lang.Runnable-</a></p></td>
<td><p>#runOutOfCustomerContext(Runnable)</p></td>
<td><p><a href="https://docs.atlassian.com/jira-servicedesk/3.9.0-m0124/com/atlassian/servicedesk/api/customer/CustomerContextService.html#runOutOfCustomerContext-java.lang.Runnable-" class="uri external-link">https://docs.atlassian.com/jira-servicedesk/3.9.0-m0124/com/atlassian/servicedesk/api/customer/CustomerContextService.html#runOutOfCustomerContext-java.lang.Runnable-</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>ServiceDeskLicence</p></td>
<td><p><code>String getLicenseType();</code></p></td>
<td><p><a href="https://docs.atlassian.com/jira-servicedesk/3.7.0/com/atlassian/servicedesk/api/license/ServiceDeskLicense.html#getLicenseType--" class="uri external-link">https://docs.atlassian.com/jira-servicedesk/3.7.0/com/atlassian/servicedesk/api/license/ServiceDeskLicense.html#getLicenseType--</a></p></td>
<td><p><code>This method is not applicable anymore. It will always be ABP</code></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>ServiceDeskCustomerRequestService</p></td>
<td><p><code class="sourceCode java">Either&lt;AnError, CustomerRequest&gt; <span class="fu">getRequestForKey</span>(</code><code class="sourceCode java"><span class="at">@Nullable</span></code><code class="sourceCode java">ApplicationUser user, </code><code class="sourceCode java"><span class="at">@Nonnull</span></code><code class="sourceCode java"><span class="bu">String</span> issueKey);</code></p></td>
<td><p><a href="https://docs.atlassian.com/jira-servicedesk/3.7.0/com/atlassian/servicedesk/api/request/ServiceDeskCustomerRequestService.html#getRequestForKey-com.atlassian.jira.user.ApplicationUser-java.lang.String-" class="uri external-link">https://docs.atlassian.com/jira-servicedesk/3.7.0/com/atlassian/servicedesk/api/request/ServiceDeskCustomerRequestService.html#getRequestForKey-com.atlassian.jira.user.ApplicationUser-java.lang.String-</a></p></td>
<td><p><code>#getCustomerRequests(ApplicationUser, CustomerRequestQuery)</code></p></td>
<td><p><a href="https://docs.atlassian.com/jira-servicedesk/3.9.0-m0124/com/atlassian/servicedesk/api/request/ServiceDeskCustomerRequestService.html#getCustomerRequests-com.atlassian.jira.user.ApplicationUser-com.atlassian.servicedesk.api.request.CustomerRequestQuery-" class="uri external-link">https://docs.atlassian.com/jira-servicedesk/3.9.0-m0124/com/atlassian/servicedesk/api/request/ServiceDeskCustomerRequestService.html#getCustomerRequests-com.atlassian.jira.user.ApplicationUser-com.atlassian.servicedesk.api.request.CustomerRequestQuery-</a></p></td>
</tr>
<tr class="odd">
<td><p><code class="sourceCode java">Either&lt;AnError, CustomerRequest&gt; <span class="fu">getRequestForIssue</span>(</code><code class="sourceCode java"><span class="at">@Nullable</span></code><code class="sourceCode java">ApplicationUser user, </code><code class="sourceCode java"><span class="at">@Nonnull</span></code><code class="sourceCode java">Issue issue);</code></p></td>
<td><p><a href="https://docs.atlassian.com/jira-servicedesk/3.7.0/com/atlassian/servicedesk/api/request/ServiceDeskCustomerRequestService.html#getRequestForIssue-com.atlassian.jira.user.ApplicationUser-com.atlassian.jira.issue.Issue-" class="uri external-link">https://docs.atlassian.com/jira-servicedesk/3.7.0/com/atlassian/servicedesk/api/request/ServiceDeskCustomerRequestService.html#getRequestForIssue-com.atlassian.jira.user.ApplicationUser-com.atlassian.jira.issue.Issue-</a></p></td>
<td><p><code>#getCustomerRequests(ApplicationUser, CustomerRequestQuery)</code></p></td>
<td><p><a href="https://docs.atlassian.com/jira-servicedesk/3.9.0-m0124/com/atlassian/servicedesk/api/request/ServiceDeskCustomerRequestService.html#getCustomerRequests-com.atlassian.jira.user.ApplicationUser-com.atlassian.servicedesk.api.request.CustomerRequestQuery-" class="uri external-link">https://docs.atlassian.com/jira-servicedesk/3.9.0-m0124/com/atlassian/servicedesk/api/request/ServiceDeskCustomerRequestService.html#getCustomerRequests-com.atlassian.jira.user.ApplicationUser-com.atlassian.servicedesk.api.request.CustomerRequestQuery-</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><code>RequestTypeUpdateParameters.Builder requestTypeUpdateParametersBuilder();</code></p></td>
<td><p><a href="https://docs.atlassian.com/jira-servicedesk/3.7.0/com/atlassian/servicedesk/api/request/ServiceDeskCustomerRequestService.html#requestTypeUpdateParametersBuilder--" class="uri external-link">https://docs.atlassian.com/jira-servicedesk/3.7.0/com/atlassian/servicedesk/api/request/ServiceDeskCustomerRequestService.html#requestTypeUpdateParametersBuilder--</a></p></td>
<td><p><code>#newUpdateBuilder()</code></p></td>
<td><p><a href="https://docs.atlassian.com/jira-servicedesk/3.9.0-m0124/com/atlassian/servicedesk/api/request/ServiceDeskCustomerRequestService.html#newUpdateBuilder--" class="uri external-link">https://docs.atlassian.com/jira-servicedesk/3.9.0-m0124/com/atlassian/servicedesk/api/request/ServiceDeskCustomerRequestService.html#newUpdateBuilder--</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><code class="sourceCode java">Either&lt;AnError, CustomerRequest&gt; <span class="fu">updateRequestType</span>(</code><code class="sourceCode java"><span class="at">@Nullable</span></code><code class="sourceCode java">ApplicationUser user, RequestTypeUpdateParameters requestTypeUpdateParameters);</code></p></td>
<td><p><a href="https://docs.atlassian.com/jira-servicedesk/3.7.0/com/atlassian/servicedesk/api/request/ServiceDeskCustomerRequestService.html#updateRequestType-com.atlassian.jira.user.ApplicationUser-com.atlassian.servicedesk.api.request.RequestTypeUpdateParameters-" class="uri external-link">https://docs.atlassian.com/jira-servicedesk/3.7.0/com/atlassian/servicedesk/api/request/ServiceDeskCustomerRequestService.html#updateRequestType-com.atlassian.jira.user.ApplicationUser-com.atlassian.servicedesk.api.request.RequestTypeUpdateParameters-</a></p></td>
<td><p><code>#updateCustomerRequest(ApplicationUser, CustomerRequestUpdateParameters)</code></p></td>
<td><p><a href="https://docs.atlassian.com/jira-servicedesk/3.9.0-m0124/com/atlassian/servicedesk/api/request/ServiceDeskCustomerRequestService.html#updateCustomerRequest-com.atlassian.jira.user.ApplicationUser-com.atlassian.servicedesk.api.request.CustomerRequestUpdateParameters-" class="uri external-link">https://docs.atlassian.com/jira-servicedesk/3.9.0-m0124/com/atlassian/servicedesk/api/request/ServiceDeskCustomerRequestService.html#updateCustomerRequest-com.atlassian.jira.user.ApplicationUser-com.atlassian.servicedesk.api.request.CustomerRequestUpdateParameters-</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><code>Either&lt;AnError, CustomerRequest&gt; getRequestForIssueOverrideSecurity(Issue issue);</code></p></td>
<td><p><a href="https://docs.atlassian.com/jira-servicedesk/3.7.0/com/atlassian/servicedesk/api/request/ServiceDeskCustomerRequestService.html#getRequestForIssueOverrideSecurity-com.atlassian.jira.issue.Issue-" class="uri external-link">https://docs.atlassian.com/jira-servicedesk/3.7.0/com/atlassian/servicedesk/api/request/ServiceDeskCustomerRequestService.html#getRequestForIssueOverrideSecurity-com.atlassian.jira.issue.Issue-</a></p></td>
<td><p><code>#getCustomerRequests(ApplicationUser, CustomerRequestQuery)</code></p></td>
<td><p><a href="https://docs.atlassian.com/jira-servicedesk/3.9.0-m0124/com/atlassian/servicedesk/api/request/ServiceDeskCustomerRequestService.html#getCustomerRequests-com.atlassian.jira.user.ApplicationUser-com.atlassian.servicedesk.api.request.CustomerRequestQuery-" class="uri external-link">https://docs.atlassian.com/jira-servicedesk/3.9.0-m0124/com/atlassian/servicedesk/api/request/ServiceDeskCustomerRequestService.html#getCustomerRequests-com.atlassian.jira.user.ApplicationUser-com.atlassian.servicedesk.api.request.CustomerRequestQuery-</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><code>Either&lt;AnError, CustomerRequest&gt; getRequestForKeyOverrideSecurity(String issueKey);</code></p></td>
<td><p><a href="https://docs.atlassian.com/jira-servicedesk/3.7.0/com/atlassian/servicedesk/api/request/ServiceDeskCustomerRequestService.html#getRequestForKeyOverrideSecurity-java.lang.String-" class="uri external-link">https://docs.atlassian.com/jira-servicedesk/3.7.0/com/atlassian/servicedesk/api/request/ServiceDeskCustomerRequestService.html#getRequestForKeyOverrideSecurity-java.lang.String-</a></p></td>
<td><p><code>#getCustomerRequests(ApplicationUser, CustomerRequestQuery)</code></p></td>
<td><p><a href="https://docs.atlassian.com/jira-servicedesk/3.9.0-m0124/com/atlassian/servicedesk/api/request/ServiceDeskCustomerRequestService.html#getCustomerRequests-com.atlassian.jira.user.ApplicationUser-com.atlassian.servicedesk.api.request.CustomerRequestQuery-" class="uri external-link">https://docs.atlassian.com/jira-servicedesk/3.9.0-m0124/com/atlassian/servicedesk/api/request/ServiceDeskCustomerRequestService.html#getCustomerRequests-com.atlassian.jira.user.ApplicationUser-com.atlassian.servicedesk.api.request.CustomerRequestQuery-</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>RequestTypeGroup</p></td>
<td><p><code>RequestTypeGroup withOrder(Option&lt;Integer&gt; order);</code></p></td>
<td><p><a href="https://docs.atlassian.com/jira-servicedesk/3.7.0/com/atlassian/servicedesk/api/requesttype/RequestTypeGroup.html#withOrder-com.atlassian.fugue.Option-" class="uri external-link">https://docs.atlassian.com/jira-servicedesk/3.7.0/com/atlassian/servicedesk/api/requesttype/RequestTypeGroup.html#withOrder-com.atlassian.fugue.Option-</a></p></td>
<td><p><code>This deprecated and Experimental API has been removed.</code></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>RequestTypeService</p></td>
<td><p><code class="sourceCode java">Either&lt;AnError, <span class="bu">List</span>&lt;RequestType&gt;&gt; <span class="fu">getRequestTypes</span>(</code><code class="sourceCode java"><span class="at">@Nonnull</span></code><code class="sourceCode java">ApplicationUser user, </code><code class="sourceCode java"><span class="at">@Nonnull</span></code><code class="sourceCode java">RequestTypeQueryParameters requestTypeQueryParameters);</code></p></td>
<td><p><a href="https://docs.atlassian.com/jira-servicedesk/3.7.0/com/atlassian/servicedesk/api/requesttype/RequestTypeService.html#getRequestTypes-com.atlassian.jira.user.ApplicationUser-com.atlassian.servicedesk.api.requesttype.RequestTypeQueryParameters-" class="uri external-link">https://docs.atlassian.com/jira-servicedesk/3.7.0/com/atlassian/servicedesk/api/requesttype/RequestTypeService.html#getRequestTypes-com.atlassian.jira.user.ApplicationUser-com.atlassian.servicedesk.api.requesttype.RequestTypeQueryParameters-</a></p></td>
<td><p><code>#getRequestTypes(ApplicationUser, RequestTypeQuery)</code></p></td>
<td><p><a href="https://docs.atlassian.com/jira-servicedesk/3.9.0-m0124/com/atlassian/servicedesk/api/requesttype/RequestTypeService.html#getRequestTypes-com.atlassian.jira.user.ApplicationUser-com.atlassian.servicedesk.api.requesttype.RequestTypeQuery-" class="uri external-link">https://docs.atlassian.com/jira-servicedesk/3.9.0-m0124/com/atlassian/servicedesk/api/requesttype/RequestTypeService.html#getRequestTypes-com.atlassian.jira.user.ApplicationUser-com.atlassian.servicedesk.api.requesttype.RequestTypeQuery-</a></p></td>
</tr>
<tr class="even">
<td><p><code>RequestTypeQueryParameters.Builder queryBuilder();</code></p></td>
<td><p><a href="https://docs.atlassian.com/jira-servicedesk/3.7.0/com/atlassian/servicedesk/api/requesttype/RequestTypeService.html#queryBuilder--" class="uri external-link">https://docs.atlassian.com/jira-servicedesk/3.7.0/com/atlassian/servicedesk/api/requesttype/RequestTypeService.html#queryBuilder--</a></p></td>
<td><p><code>#newQueryBuilder()</code></p></td>
<td><p><a href="https://docs.atlassian.com/jira-servicedesk/3.9.0-m0124/com/atlassian/servicedesk/api/requesttype/RequestTypeService.html#newQueryBuilder--" class="uri external-link">https://docs.atlassian.com/jira-servicedesk/3.9.0-m0124/com/atlassian/servicedesk/api/requesttype/RequestTypeService.html#newQueryBuilder--</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><code class="sourceCode java">Either&lt;AnError, RequestType&gt; <span class="fu">getRequestTypeForRequest</span>(</code><code class="sourceCode java"><span class="at">@Nonnull</span></code><code class="sourceCode java">ApplicationUser user, </code><code class="sourceCode java"><span class="at">@Nonnull</span></code><code class="sourceCode java">CustomerRequest customerRequest);</code></p></td>
<td><p><a href="https://docs.atlassian.com/jira-servicedesk/3.7.0/com/atlassian/servicedesk/api/requesttype/RequestTypeService.html#getRequestTypeForRequest-com.atlassian.jira.user.ApplicationUser-com.atlassian.servicedesk.api.request.CustomerRequest-" class="uri external-link">https://docs.atlassian.com/jira-servicedesk/3.7.0/com/atlassian/servicedesk/api/requesttype/RequestTypeService.html#getRequestTypeForRequest-com.atlassian.jira.user.ApplicationUser-com.atlassian.servicedesk.api.request.CustomerRequest-</a></p></td>
<td><p><code>#getRequestTypes(ApplicationUser, RequestTypeQuery)</code></p></td>
<td><p><a href="https://docs.atlassian.com/jira-servicedesk/3.9.0-m0124/com/atlassian/servicedesk/api/requesttype/RequestTypeService.html#getRequestTypes-com.atlassian.jira.user.ApplicationUser-com.atlassian.servicedesk.api.requesttype.RequestTypeQuery-" class="uri external-link">https://docs.atlassian.com/jira-servicedesk/3.9.0-m0124/com/atlassian/servicedesk/api/requesttype/RequestTypeService.html#getRequestTypes-com.atlassian.jira.user.ApplicationUser-com.atlassian.servicedesk.api.requesttype.RequestTypeQuery-</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><code class="sourceCode java">Either&lt;AnError, RequestType&gt; <span class="fu">getRequestTypeForIssue</span>(</code><code class="sourceCode java"><span class="at">@Nonnull</span></code><code class="sourceCode java">ApplicationUser user, </code><code class="sourceCode java"><span class="at">@Nonnull</span></code><code class="sourceCode java">Issue issue);</code></p></td>
<td><p><a href="https://docs.atlassian.com/jira-servicedesk/3.7.0/com/atlassian/servicedesk/api/requesttype/RequestTypeService.html#getRequestTypeForIssue-com.atlassian.jira.user.ApplicationUser-com.atlassian.jira.issue.Issue-" class="uri external-link">https://docs.atlassian.com/jira-servicedesk/3.7.0/com/atlassian/servicedesk/api/requesttype/RequestTypeService.html#getRequestTypeForIssue-com.atlassian.jira.user.ApplicationUser-com.atlassian.jira.issue.Issue-</a></p></td>
<td><p><code>#getRequestTypes(ApplicationUser, RequestTypeQuery)</code></p></td>
<td><p><a href="https://docs.atlassian.com/jira-servicedesk/3.9.0-m0124/com/atlassian/servicedesk/api/requesttype/RequestTypeService.html#getRequestTypes-com.atlassian.jira.user.ApplicationUser-com.atlassian.servicedesk.api.requesttype.RequestTypeQuery-" class="uri external-link">https://docs.atlassian.com/jira-servicedesk/3.9.0-m0124/com/atlassian/servicedesk/api/requesttype/RequestTypeService.html#getRequestTypes-com.atlassian.jira.user.ApplicationUser-com.atlassian.servicedesk.api.requesttype.RequestTypeQuery-</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><code>Either&lt;AnError, List&lt;RequestType&gt;&gt; getAllRequestTypes(ApplicationUser user);</code></p></td>
<td><p><a href="https://docs.atlassian.com/jira-servicedesk/3.7.0/com/atlassian/servicedesk/api/requesttype/RequestTypeService.html#getAllRequestTypes-com.atlassian.jira.user.ApplicationUser-" class="uri external-link">https://docs.atlassian.com/jira-servicedesk/3.7.0/com/atlassian/servicedesk/api/requesttype/RequestTypeService.html#getAllRequestTypes-com.atlassian.jira.user.ApplicationUser-</a></p></td>
<td><p><code>#getRequestTypes(ApplicationUser, RequestTypeQuery)</code></p></td>
<td><p><a href="https://docs.atlassian.com/jira-servicedesk/3.9.0-m0124/com/atlassian/servicedesk/api/requesttype/RequestTypeService.html#getRequestTypes-com.atlassian.jira.user.ApplicationUser-com.atlassian.servicedesk.api.requesttype.RequestTypeQuery-" class="uri external-link">https://docs.atlassian.com/jira-servicedesk/3.9.0-m0124/com/atlassian/servicedesk/api/requesttype/RequestTypeService.html#getRequestTypes-com.atlassian.jira.user.ApplicationUser-com.atlassian.servicedesk.api.requesttype.RequestTypeQuery-</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><code class="sourceCode java">Either&lt;AnError, RequestType&gt; <span class="fu">getRequestTypeForRequestOverrideSecurity</span>(</code><code class="sourceCode java"><span class="at">@Nonnull</span></code><code class="sourceCode java">CustomerRequest customerRequest);</code></p></td>
<td><p><a href="https://docs.atlassian.com/jira-servicedesk/3.7.0/com/atlassian/servicedesk/api/requesttype/RequestTypeService.html#getRequestTypeForRequestOverrideSecurity-com.atlassian.servicedesk.api.request.CustomerRequest-" class="uri external-link">https://docs.atlassian.com/jira-servicedesk/3.7.0/com/atlassian/servicedesk/api/requesttype/RequestTypeService.html#getRequestTypeForRequestOverrideSecurity-com.atlassian.servicedesk.api.request.CustomerRequest-</a></p></td>
<td><p><code>#getRequestTypes(ApplicationUser, RequestTypeQuery)</code></p></td>
<td><p><a href="https://docs.atlassian.com/jira-servicedesk/3.9.0-m0124/com/atlassian/servicedesk/api/requesttype/RequestTypeService.html#getRequestTypes-com.atlassian.jira.user.ApplicationUser-com.atlassian.servicedesk.api.requesttype.RequestTypeQuery-" class="uri external-link">https://docs.atlassian.com/jira-servicedesk/3.9.0-m0124/com/atlassian/servicedesk/api/requesttype/RequestTypeService.html#getRequestTypes-com.atlassian.jira.user.ApplicationUser-com.atlassian.servicedesk.api.requesttype.RequestTypeQuery-</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><code class="sourceCode java">Either&lt;AnError, RequestType&gt; <span class="fu">getRequestTypeForIssueOverrideSecurity</span>(</code><code class="sourceCode java"><span class="at">@Nonnull</span></code><code class="sourceCode java">Issue issue);</code></p></td>
<td><p><a href="https://docs.atlassian.com/jira-servicedesk/3.7.0/com/atlassian/servicedesk/api/requesttype/RequestTypeService.html#getRequestTypeForIssueOverrideSecurity-com.atlassian.jira.issue.Issue-" class="uri external-link">https://docs.atlassian.com/jira-servicedesk/3.7.0/com/atlassian/servicedesk/api/requesttype/RequestTypeService.html#getRequestTypeForIssueOverrideSecurity-com.atlassian.jira.issue.Issue-</a></p></td>
<td><p><code>#getRequestTypes(ApplicationUser, RequestTypeQuery)</code></p></td>
<td><p><a href="https://docs.atlassian.com/jira-servicedesk/3.9.0-m0124/com/atlassian/servicedesk/api/requesttype/RequestTypeService.html#getRequestTypes-com.atlassian.jira.user.ApplicationUser-com.atlassian.servicedesk.api.requesttype.RequestTypeQuery-" class="uri external-link">https://docs.atlassian.com/jira-servicedesk/3.9.0-m0124/com/atlassian/servicedesk/api/requesttype/RequestTypeService.html#getRequestTypes-com.atlassian.jira.user.ApplicationUser-com.atlassian.servicedesk.api.requesttype.RequestTypeQuery-</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><code class="sourceCode java">Either&lt;AnError, RequestType&gt; <span class="fu">createRequestType</span>(</code><code class="sourceCode java"><span class="dt">long</span></code><code class="sourceCode java">projectId, </code><code class="sourceCode java"><span class="dt">int</span></code><code class="sourceCode java">issueTypeId, <span class="bu">String</span> name, <span class="bu">String</span> description, </code><code class="sourceCode java"><span class="dt">long</span></code><code class="sourceCode java">iconId, <span class="bu">List</span>&lt;<span class="bu">Integer</span>&gt; groupIds);</code></p></td>
<td><p><a href="https://docs.atlassian.com/jira-servicedesk/3.7.0/com/atlassian/servicedesk/api/requesttype/RequestTypeService.html#createRequestType-long-int-java.lang.String-java.lang.String-long-java.util.List-" class="uri external-link">https://docs.atlassian.com/jira-servicedesk/3.7.0/com/atlassian/servicedesk/api/requesttype/RequestTypeService.html#createRequestType-long-int-java.lang.String-java.lang.String-long-java.util.List-</a></p></td>
<td><p><code>#createRequestType(ApplicationUser, RequestTypeCreateParameters)</code></p></td>
<td><p><a href="https://docs.atlassian.com/jira-servicedesk/3.9.0-m0124/com/atlassian/servicedesk/api/requesttype/RequestTypeService.html#createRequestType-com.atlassian.jira.user.ApplicationUser-com.atlassian.servicedesk.api.requesttype.RequestTypeCreateParameters-" class="uri external-link">https://docs.atlassian.com/jira-servicedesk/3.9.0-m0124/com/atlassian/servicedesk/api/requesttype/RequestTypeService.html#createRequestType-com.atlassian.jira.user.ApplicationUser-com.atlassian.servicedesk.api.requesttype.RequestTypeCreateParameters-</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><code>Either&lt;AnError, List&lt;RequestType&gt;&gt; getRequestTypesForGroup(Integer groupId, Long projectId);</code></p></td>
<td><p><a href="https://docs.atlassian.com/jira-servicedesk/3.7.0/com/atlassian/servicedesk/api/requesttype/RequestTypeService.html#getRequestTypesForGroup-java.lang.Integer-java.lang.Long-" class="uri external-link">https://docs.atlassian.com/jira-servicedesk/3.7.0/com/atlassian/servicedesk/api/requesttype/RequestTypeService.html#getRequestTypesForGroup-java.lang.Integer-java.lang.Long-</a></p></td>
<td><p><code>#getRequestTypes(ApplicationUser, RequestTypeQuery)</code></p></td>
<td><p><a href="https://docs.atlassian.com/jira-servicedesk/3.9.0-m0124/com/atlassian/servicedesk/api/requesttype/RequestTypeService.html#getRequestTypes-com.atlassian.jira.user.ApplicationUser-com.atlassian.servicedesk.api.requesttype.RequestTypeQuery-" class="uri external-link">https://docs.atlassian.com/jira-servicedesk/3.9.0-m0124/com/atlassian/servicedesk/api/requesttype/RequestTypeService.html#getRequestTypes-com.atlassian.jira.user.ApplicationUser-com.atlassian.servicedesk.api.requesttype.RequestTypeQuery-</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><code>Either&lt;AnError, List&lt;RequestType&gt;&gt; getRequestTypesByGroup(Integer groupId, Integer serviceDeskId);</code></p></td>
<td><p><a href="https://docs.atlassian.com/jira-servicedesk/3.7.0/com/atlassian/servicedesk/api/requesttype/RequestTypeService.html#getRequestTypesByGroup-java.lang.Integer-java.lang.Integer-" class="uri external-link">https://docs.atlassian.com/jira-servicedesk/3.7.0/com/atlassian/servicedesk/api/requesttype/RequestTypeService.html#getRequestTypesByGroup-java.lang.Integer-java.lang.Integer-</a></p></td>
<td><p><code>#getRequestTypes(ApplicationUser, RequestTypeQuery)</code></p></td>
<td><p><a href="https://docs.atlassian.com/jira-servicedesk/3.9.0-m0124/com/atlassian/servicedesk/api/requesttype/RequestTypeService.html#getRequestTypes-com.atlassian.jira.user.ApplicationUser-com.atlassian.servicedesk.api.requesttype.RequestTypeQuery-" class="uri external-link">https://docs.atlassian.com/jira-servicedesk/3.9.0-m0124/com/atlassian/servicedesk/api/requesttype/RequestTypeService.html#getRequestTypes-com.atlassian.jira.user.ApplicationUser-com.atlassian.servicedesk.api.requesttype.RequestTypeQuery-</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><code>Either&lt;AnError, RequestType&gt; getRequestTypeByIdAndProjectId(Integer requestTypeId, Long projectId);</code></p></td>
<td><p><a href="https://docs.atlassian.com/jira-servicedesk/3.7.0/com/atlassian/servicedesk/api/requesttype/RequestTypeService.html#getRequestTypeByIdAndProjectId-java.lang.Integer-java.lang.Long-" class="uri external-link">https://docs.atlassian.com/jira-servicedesk/3.7.0/com/atlassian/servicedesk/api/requesttype/RequestTypeService.html#getRequestTypeByIdAndProjectId-java.lang.Integer-java.lang.Long-</a></p></td>
<td><p><code>#getRequestTypes(ApplicationUser, RequestTypeQuery)</code></p></td>
<td><p><a href="https://docs.atlassian.com/jira-servicedesk/3.9.0-m0124/com/atlassian/servicedesk/api/requesttype/RequestTypeService.html#getRequestTypes-com.atlassian.jira.user.ApplicationUser-com.atlassian.servicedesk.api.requesttype.RequestTypeQuery-" class="uri external-link">https://docs.atlassian.com/jira-servicedesk/3.9.0-m0124/com/atlassian/servicedesk/api/requesttype/RequestTypeService.html#getRequestTypes-com.atlassian.jira.user.ApplicationUser-com.atlassian.servicedesk.api.requesttype.RequestTypeQuery-</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><code>Either&lt;AnError, RequestType&gt; deleteRequestTypeByIdAndProjectId(Integer requestTypeId, Long projectId);</code></p></td>
<td><p><a href="https://docs.atlassian.com/jira-servicedesk/3.7.0/com/atlassian/servicedesk/api/requesttype/RequestTypeService.html#deleteRequestTypeByIdAndProjectId-java.lang.Integer-java.lang.Long-" class="uri external-link">https://docs.atlassian.com/jira-servicedesk/3.7.0/com/atlassian/servicedesk/api/requesttype/RequestTypeService.html#deleteRequestTypeByIdAndProjectId-java.lang.Integer-java.lang.Long-</a></p></td>
<td><p><code>#deleteRequestType(ApplicationUser, RequestTypeDeleteParameters)</code></p></td>
<td><p><a href="https://docs.atlassian.com/jira-servicedesk/3.9.0-m0124/com/atlassian/servicedesk/api/requesttype/RequestTypeService.html#deleteRequestType-com.atlassian.jira.user.ApplicationUser-com.atlassian.servicedesk.api.requesttype.RequestTypeDeleteParameters-" class="uri external-link">https://docs.atlassian.com/jira-servicedesk/3.9.0-m0124/com/atlassian/servicedesk/api/requesttype/RequestTypeService.html#deleteRequestType-com.atlassian.jira.user.ApplicationUser-com.atlassian.servicedesk.api.requesttype.RequestTypeDeleteParameters-</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><code>Either&lt;AnError, RequestType&gt; updateRequestTypeByIdAndProjectId(Integer requestTypeId, Long projectId, RequestType requestType);</code></p></td>
<td><p><a href="https://docs.atlassian.com/jira-servicedesk/3.7.0/com/atlassian/servicedesk/api/requesttype/RequestTypeService.html#updateRequestTypeByIdAndProjectId-java.lang.Integer-java.lang.Long-com.atlassian.servicedesk.api.requesttype.RequestType-" class="uri external-link">https://docs.atlassian.com/jira-servicedesk/3.7.0/com/atlassian/servicedesk/api/requesttype/RequestTypeService.html#updateRequestTypeByIdAndProjectId-java.lang.Integer-java.lang.Long-com.atlassian.servicedesk.api.requesttype.RequestType-</a></p></td>
<td><p><code>#updateRequestType(ApplicationUser, RequestTypeUpdateParameters)</code></p></td>
<td><p><a href="https://docs.atlassian.com/jira-servicedesk/3.9.0-m0124/com/atlassian/servicedesk/api/requesttype/RequestTypeService.html#updateRequestType-com.atlassian.jira.user.ApplicationUser-com.atlassian.servicedesk.api.requesttype.RequestTypeUpdateParameters-" class="uri external-link">https://docs.atlassian.com/jira-servicedesk/3.9.0-m0124/com/atlassian/servicedesk/api/requesttype/RequestTypeService.html#updateRequestType-com.atlassian.jira.user.ApplicationUser-com.atlassian.servicedesk.api.requesttype.RequestTypeUpdateParameters-</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>FpKit</p></td>
<td><p><code class="sourceCode java"><span class="bu">Callable</span>&lt;Unit&gt; <span class="fu">runnableToCallable</span>(<span class="bu">Runnable</span> runnable)</code></p></td>
<td><a href="https://docs.atlassian.com/jira-servicedesk/3.7.0/com/atlassian/servicedesk/api/util/fp/FpKit.html#runnableToCallable-java.lang.Runnable-" class="uri external-link">https://docs.atlassian.com/jira-servicedesk/3.7.0/com/atlassian/servicedesk/api/util/fp/FpKit.html#runnableToCallable-java.lang.Runnable-</a></td>
<td><code>This deprecated and Experimental API has been removed.</code></td>
<td> </td>
</tr>
</tbody>
</table>
