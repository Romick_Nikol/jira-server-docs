---
date: '2018-04-24'
platform: server
category: devguide
product: jira
subcategory: intro
title: "Integrating with Portfolio for Jira"
---
# Integrating with Portfolio for Jira

Portfolio for Jira helps you build a plan, see the big picture, track progress, and easily share with stakeholders. And now you can also integrate with Portfolio for Jira through our APIs.

## Portfolio Team Management API

Use the [Team Management API](https://docs.atlassian.com/portfolio-for-jira-server/REST/2.13.0/teams/)
to interact with all of the team information available in Portfolio for Jira.

## Portfolio Live Plans API

Use the [Live Plans API](https://docs.atlassian.com/portfolio-for-jira-server/REST/2.13.0/jpo/)
to interact with the live plans in Portfolio for Jira.

## Portfolio Java API

For those of you who want to dive even deeper, check out our [Java API](https://docs.atlassian.com/portfolio-for-jira-server/javadoc/2.13.0/)