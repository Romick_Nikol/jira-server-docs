---
aliases:
- /server/jira/platform/38442129.html
- /server/jira/platform/38442129.md
category: devguide
confluence_id: 38442129
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=38442129
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=38442129
date: '2017-12-08'
legacy_title: JIRA Core 7.1 EAP 01 (m01) Release Notes
platform: server
product: jira
subcategory: updates
title: JIRA Core 7.1 EAP 01 (m01) Release Notes
---
# JIRA Core 7.1 EAP 01 (m01) Release Notes

**14 January 2016**

Atlassian is proud to present **JIRA Core 7.1 EAP 01 (m01**). This public development release is part of our [Early Access Program (EAP)](/server/jira/platform/jira-eap-releases-35160569.html) leading up to the official **JIRA Core 7.1** release. We are making these EAP milestones publicly available so that developers can start assessing the impact of the changes that we are making.

For the full list of changes for JIRA 7.1, read the developer change management guide: [Preparing for JIRA 7.1](/server/jira/platform/preparing-for-jira-7-1). If you are new to JIRA, you should also read our [Java API Policy for JIRA](/server/jira/platform/java-api-policy-for-jira-4227213.html) and the [Atlassian REST API policy](https://developer.atlassian.com/display/HOME/Atlassian+REST+API+policy).

[<img src="/server/jira/platform/images/download-eap-btn.png" class="image-center confluence-thumbnail" width="120" />](#downloads)

***[Upgrade notes (including known issues)](#upgrade-notes-including-known-issues)***

{{% warning %}}

Before you install/upgrade: Atlassian does not support upgrades both 'from' and 'to' EAP releases. EAP releases should not be used in production environments as they are not officially supported. For all production use and testing of JIRA, please use the <a href="https://confluence.atlassian.com/display/JIRACORE/JIRA+Core+7.0.x+release+notes" class="external-link">latest official release</a> instead.

{{% /warning %}}{{% note %}}

**We want your feedback!**

To provide feedback, create an issue in <a href="https://jira.atlassian.com/browse/JRA" class="external-link">our issue tracker</a> with the following details:

-   **Issue Type**: `Suggestion`
-   **Component**: `JIRA 7.1 EAP`

{{% /note %}}

# Changes

## Project issue view

The new "project issue view" for JIRA Core gives you an expanded view of any issue from within a project. Just click the ![](/server/jira/platform/images/icon-jira-issue-expand.png) icon or an issue link in an issue...

<img src="/server/jira/platform/images/jiracore-projectissueview-links.png" width="625" />

And the target issue will be displayed like this...

<img src="/server/jira/platform/images/jiracore-projectissueview-annotated.png" width="600" />

This new view works exactly like the regular issue view (e.g. keyboard shortcuts, pagination, inline editing, etc) but you'll get much more space without losing the project sidebar.

## Granting project permissions

We've updated the way that you edit your project permission scheme. Granting permissions in now done in a dialog -- click the **Edit** link to display the '**Grant permission**' dialog, and select the permissions you want to grant, and to whom. Removing someone from a permission is a similar process -- find the permission, click **Remove** and choose what group, role, or user you want to remove.

## Viewing your versions

The dropdown for selecting quick filters on the **Versions** (JIRA Core) page has been removed and persistent 'sticky' labels added in its place. This allows you to quickly filter your versions by Released or Unreleased. The search bar has also been improved.

## Access to additional advanced settings in JIRA Cloud

We've listened to your requests, and we've implemented <a href="https://jira.atlassian.com/browse/JRA-26866" class="external-link"><img src="https://jira.atlassian.com/secure/viewavatar?size=xsmall&amp;avatarId=51505&amp;avatarType=issuetype" class="icon confluence-external-resource" />JRA-26866</a> - Administrators in JIRA Cloud should be able to access General configuration &gt; Advanced configuration RESOLVED . This allows JIRA administrators to edit some of the <a href="https://confluence.atlassian.com/display/AdminJIRACloud/Configuring+advanced+settings" class="external-link">advanced settings</a> in their JIRA Cloud instance, that were previously restricted to Atlassian Support. Giving JIRA administrators this increased flexibility to manage their instance has been a long requested feature, and we're happy to finally make it a reality.

## Comment webhooks

You can now register comment-related events for a webhook in JIRA Core. This lets JIRA Core notify your add-ons or web applications whenever a comment is created, updated, or deleted. Just choose the desired events when creating or updating your webhook. Read the [webhooks documentation](/server/jira/platform/webhooks) for details. 

## Atlassian Connect add-ons: Checking for JIRA applications

If you are developing an [Atlassian Connect](https://developer.atlassian.com/static/connect/docs/latest/index.html) add-on, you can now set the `can_use_application` condition to detect whether a particular JIRA application is licensed and active for the current user. This For example, the following condition would check whether JIRA Software is installed and enabled, and whether the current user can use it according to the application license:

``` javascript
{
    "condition": "can_use_application",
    "params": {
        "applicationKey": "jira-software"
    }
}
```

## Entity properties on issue creation

Entity properties are used to add key/value stores to JIRA entities, like issues and projects. Previously, it was only possible to add entity properties to an issue entity after the issue has already been created. In this release, you can now add these properties when creating an issue (REST API only), e.g.

``` javascript
{
    "fields": {
       "project":
       {
          "key": "TEST"
       },
       "summary": "Example issue",
       "description": "This is an example issue",
       "issuetype": {
          "name": "Bug"
       }
    }
    "properties": [
        "propertykey" : {}, // valid JSON object here
        "secondPropertykey" : {} // another valid JSON object here
    ]
}
```

For more information on entity properties in JIRA, see [JIRA Entity Properties overview](/server/jira/platform/entity-properties).

# Upgrade notes (including known issues)

There are currently no known issues with this milestone.

if you are upgrading from JIRA 6.3.x or earlier, read <a href="https://confluence.atlassian.com/display/JIRA/Skipping+Major+Versions+When+Upgrading+JIRA" class="external-link">Skipping Major Versions When Upgrading JIRA</a>. Remember that EAPs are **not supported** and **should not be used in production environments**.

## Downloads

-   <a href="http://www.atlassian.com/software/jira/downloads/binary/atlassian-jira-core-7.1.0-m01-x32.exe" class="external-link">JIRA Core 7.1.0-m01 (Windows 32 Bit Installer, EXE)</a>
-   <a href="http://www.atlassian.com/software/jira/downloads/binary/atlassian-jira-core-7.1.0-m01-x64.exe" class="external-link">JIRA Core 7.1.0-m01 (Windows 64 Bit Installer, EXE)</a>
-   <a href="http://www.atlassian.com/software/jira/downloads/binary/atlassian-jira-core-7.1.0-m01-x32.bin" class="external-link">JIRA Core 7.1.0-m01 (Linux 32 Bit Installer, BIN)</a>
-   <a href="http://www.atlassian.com/software/jira/downloads/binary/atlassian-jira-core-7.1.0-m01-x64.bin" class="external-link">JIRA Core 7.1.0-m01 (Linux 64 Bit Installer, BIN)</a>
-   <a href="http://www.atlassian.com/software/jira/downloads/binary/atlassian-jira-core-7.1.0-m01.zip" class="external-link">JIRA Core 7.1.0-m01 (ZIP Archive)</a>
-   <a href="http://www.atlassian.com/software/jira/downloads/binary/atlassian-jira-core-7.1.0-m01.tar.gz" class="external-link">JIRA Core 7.1.0-m01 (TAR.GZ Archive)</a>
