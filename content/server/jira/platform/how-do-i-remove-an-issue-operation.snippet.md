---
aliases:
- /server/jira/platform/4227120.html
- /server/jira/platform/4227120.md
category: devguide
confluence_id: 4227120
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=4227120
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=4227120
date: '2017-12-08'
legacy_title: How do I remove an issue operation?
platform: server
product: jira
subcategory: other
title: How do I remove an issue operation?
---
# How do I remove an issue operation?

At the moment the easiest way to achieve this would be to edit the `includes/panels/issue/operations.jsp` file under the JIRA web application and remove the code that prints out the link to your operation. For example, if you want to remove the "Assign to" link, the code starts with:

``` javascript
<webwork:if test="hasIssuePermission('assign', issue) == true">

...

</webwork:if>
```

Simply remove that block and this will be removed from view. You may need to restart your application server for this change to be reflected.
