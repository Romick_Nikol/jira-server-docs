---
title: JIRA Architecture 32344051
aliases:
    - /server/jira/platform/jira-architecture-32344051.html
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=32344051
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=32344051
confluence_id: 32344051
platform: server
product: jira
category: devguide
subcategory:
date: "2018-04-25"
---
# JIRA Developer Documentation : JIRA architecture

The topics in this section will help you understand the architecture of the JIRA platform and how JIRA development works in general. You'll find information on JIRA's dependencies, authentication, web fragments, templates, and more.*Not sure if you are in the right place?*

-   *If you haven't developed for JIRA before, start with our [Getting started](/server/jira/platform/getting-started) page.*
-   *If you are looking for guides, tutorials and reference information for the different parts of the JIRA platform, e.g. projects, issues, etc, see [Tutorials and guides](/server/jira/platform/tutorials_and_guides).*

<img src="/server/jira/platform/images/jira-architecture.png" width="500" />

## Topics in this section

<table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<tbody>
<tr class="odd">
<td><h3 id="JIRAarchitecture-JIRATechnicalOverview"><strong><a href="/server/jira/platform/architecture-overview">JIRA Technical Overview</a></strong></h3></td>
<td>A very high level overview of JIRA's dependencies and the role each one plays in JIRA. </td>
</tr>
<tr class="even">
<td><h3 id="JIRAarchitecture-"><strong><a href="/server/jira/platform/web-fragments-4227124.html">Web fragments</a></strong>  </h3></td>
<td><p>A web fragment is a link, a section of links or an area of HTML content (or 'panel') in a particular location of the JIRA web interface. This page describes web fragments in detail and lists the different web fragment locations available in JIRA.</p></td>
</tr>
<tr class="odd">
<td><h3 id="JIRAarchitecture-TemplatesandJSPs"><strong><a href="/server/jira/platform/jira-templates-and-jsps">Templates and JSPs</a></strong>  </h3></td>
<td><p>The templates (Velocity) and JSPs in the JIRA distribution can be modified to change how JIRA looks and behaves. For example, you could customise the email templates used by JIRA. This section describes how to modify JIRA's JSPs and templates, and includes a number of tutorials.</p></td>
</tr>
<tr class="even">
<td><h3 id="JIRAarchitecture-SecurityoverviewSecurity-overview_32345317.html"><strong><a href="/server/jira/platform/security-overview">Security overview</a></strong></h3></td>
<td><p>This section provides you with information on how to implement authentication for your add-on (Plugins2 or Connect). This includes information on how to specify required JIRA permissions for your add-on.</p></td>
</tr>
<tr class="odd">
<td><h3 id="JIRAarchitecture-.1"><strong><a href="/server/jira/platform/database-schema">Database schema</a></strong>  </h3></td>
<td>This section provides the database schema for JIRA, as well as instructions on how to query for different information.</td>
</tr>
<tr class="even">
<td><h3 id="JIRAarchitecture-.2"><strong><a href="/server/jira/platform/jira-mobile-connect-4653058.html">JIRA Mobile Connect</a></strong>  </h3></td>
<td>JIRA Mobile Connect is a free open source library that you can embed into any iOS application, that provides features like user feedback, crash reporting, and more. This section contains information on how to integrate JIRA Mobile Connect with your iOS app.</td>
</tr>
</tbody>
</table>
