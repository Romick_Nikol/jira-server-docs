---
aliases:
- /server/jira/platform/jira-7.0-rc3-release-notes-35719333.html
- /server/jira/platform/jira-7.0-rc3-release-notes-35719333.md
category: devguide
confluence_id: 35719333
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=35719333
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=35719333
date: '2017-12-08'
legacy_title: JIRA 7.0 RC3 Release Notes
platform: server
product: jira
subcategory: updates
title: JIRA 7.0 RC3 Release Notes
---
# JIRA 7.0 RC3 Release Notes

**15 September 2015**

Atlassian is proud to present **JIRA 7.0 RC3** *(note, there was no 7.0 RC1 or RC2 release)*. This public development release is part of our [Early Access Program (EAP)](/server/jira/platform/jira-eap-releases-35160569.html) leading up to the official **JIRA 7.0** release. We are making these EAP milestones publicly available so that developers can start assessing the impact of the changes that we are making.

A release candidate (RC) is a more mature build of a development release, and generally does not introduce new changes. The intent of an RC is to provide bug fixes and tweaks to features introduced in earlier EAP releases.

If you are a **JIRA developer**, please see [Preparing for JIRA 7.0](/server/jira/platform/preparing-for-jira-7-0) as well as our [Java API Policy for JIRA](/server/jira/platform/java-api-policy-for-jira-4227213.html).

### <a href="http://www.atlassian.com/software/jira/download-eap" class="external-link"><img src="/server/jira/platform/images/download-eap-btn.png" class="image-center confluence-thumbnail" width="120" /></a>

***[Upgrade notes (including known issues)](#upgrade-notes-including-known-issues)***

{{% warning %}}

Before you install/upgrade: Atlassian does not support upgrades both 'from' and 'to' EAP releases. EAP releases should not be used in production environments as they are not officially supported. For all production use and testing of JIRA, please use <a href="https://confluence.atlassian.com/display/JIRA/JIRA+6.4+Release+Notes" class="external-link">the latest official release</a> instead.

{{% /warning %}}

{{% note %}}

**We want your feedback!**

To provide feedback, create an issue in <a href="https://jira.atlassian.com/browse/JRA" class="external-link">our issue tracker</a> with the following details:

-   **Issue Type**: `Suggestion`

-   **Component**: `JIRA 7 EAP`

{{% /note %}}

## Applications

JIRA 7.0 introduces a fundamental change to JIRA: **applications**. Applications are now fundamental to JIRA's licensing, user management, and user interface. 

-   **Licensing -- **JIRA 7.0 eliminates the single JIRA license in favor of a base platform where multiple applications can be licensed individually. You can install one or more applications in JIRA via the  _Manage applications_  page. The base JIRA application, *JIRA Core*, provides basic project tracking capabilities for business teams. All JIRA applications include access to JIRA Core, or you can license JIRA Core only for users that need basic access to JIRA.
-   **User management --** Users consume a seat in a JIRA application license when they are granted access to a JIRA application. In JIRA Server, the new  _Application access_  page allows administrators to grant access to licensed applications to specific groups. 
-   **User interface --** In JIRA 7.0, we will be solidifying the concept of  _project types_  in JIRA, as well as expanding the capabilities of project types to provide application-specific features available in that project. Installing and licensing an application in JIRA allows user to create projects of that application's project type. For example, if there were an application called "JIRA Space Program," administrators could create new projects with the "Space Program" project type. Users with access to JIRA Space Program will have access to Space Program-specific features in Space Program projects. 

We recommend that you read the [Preparing for JIRA 7.0](/server/jira/platform/preparing-for-jira-7-0) guide for details on how these changes will affect you.

These changes are enabled **by default** in this EAP release. More information will be provided about licensing and projects in JIRA 7.0 as we get closer to the public release date.

### JIRA Agile REST API

We're pleased to announce that JIRA Agile now has a public REST API. The new officially supported REST API includes a number of improvements over the old private JIRA Agile REST APIs:

-   New resources -- We've added a number of new resources to help you interact with JIRA Agile, like the epic and sprint resources.
-   Online documentation -- You don't need to mess around looking at private internal API calls anymore. The documentation is available online and will be updated with each release.
-   New documentation theme -- The documentation is now easier to parse with expandable sections for resources and better styling.

Check out the documentation for the latest version here: https://docs.atlassian.com/greenhopper/REST/cloud/

If you are currently using the private JIRA Agile REST API, see the instructions below:

Be aware that the resources that are marked as private in  [JIRA Agile REST Scopes](https://developer.atlassian.com/static/connect/docs/latest/scopes/jira-agile-rest-scopes.html)  have been deprecated. These private resources will no longer be whitelisted on the **1st November 2015**. You *must migrate* to the new public REST API before this date.

Note, there is feature parity between the whitelisted private resources and the new public REST resources.

Be aware that the private JIRA Agile REST API will remain experimental and may break at any time. You can continue to use the private API, but we recommend that you migrate to the new public REST API.

Note, feature parity does not exist between the private and public REST APIs, and we cannot promise that there will be feature parity in future.

If you have problems using the new JIRA Agile REST API, create an issue in the **ACJIRA** project on <a href="https://ecosystem.atlassian.net/browse/ACJIRA" class="external-link">ecosystem.atlassian.net</a> *(not the issue tracker shown at the top of this page)*.

{{% note %}}

*You will need to use the JIRA EAP that includes JIRA Agile to try the new JIRA Agile REST API. See [JIRA + JIRA Agile EAP downloads](#jira-jira-agile-eap-downloads) below.*

{{% /note %}}

### Project types

Every project in JIRA now has a project type. You'll see this in a number of places in this milestone, e.g. when viewing projects or creating projects.

<img src="/server/jira/platform/images/jira-projecttypes.png" width="870" height="93" />

This change means that you must now provide a project type key when creating a project. This applies to both the Java API and REST API for JIRA. For more information, see [Preparing for JIRA 7.0 - API changes](/server/jira/platform/jira-7-0-api-changes#projecttypes).

### Shared configurations for projects

When you create a project, you can now choose to share the configuration of an existing project with the new project.

<img src="/server/jira/platform/images/jira-createproject-sharedconfig.png" width="435" />

The shared configuration consists of the following:

-   Issue Type scheme
-   Issue Type Screen scheme
-   Field Configuration scheme
-   Workflow scheme
-   Permission scheme
-   Notification scheme
-   Issue Security scheme

### Project sidebar

The project sidebar that we previewed in <a href="https://confluence.atlassian.com/display/JIRA/JIRA+6.4+Release+Notes" class="external-link">JIRA 6.4</a> is here to stay. We built this new navigation experience to make it easier for you to find what you need in your projects. It's even better, if you are using JIRA Agile: your backlog, sprints, and reports are now just a click away. If you've used the sidebar with JIRA Agile before, you'll notice that boards that include multiple projects now have a project sidebar as well, albeit a simpler version.

### Dashboard items

We're making your dashboards faster with dashboard items in this release. Dashboard items are similar to gadgets, but they use more modern technologies and load faster. They are also simpler to develop, if you want to build your own (read the [developer guide](https://developer.atlassian.com/display/JIRADEV/Guide+-+Building+a+dashboard+item+for+a+JIRA+Connect+add-on)). We haven't replaced all of the gadgets with dashboard items yet, but this release includes a few, like the pie chart gadget and the administration gadget. 

### Changes to project templates

We've made a few changes to the project templates that are used when creating a project. Some templates have been updated with new names and icons. We've also introduced a default issue type, which is used when creating issues, until you select a different one. Check out the changes below:

**New project template: "Basic software development"**

<table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Workflow</td>
<td><img src="/server/jira/platform/images/software-dev-gt-wf-col.png" class="image-center" /></td>
</tr>
<tr class="even">
<td>Issue types</td>
<td><ul>
<li>Bug, Task, Sub-task, Improvement, New Feature, Epic</li>
<li>Default issue type = Bug</li>
</ul></td>
</tr>
<tr class="odd">
<td>Notes</td>
<td><ul>
<li>JIRA Agile is required for this project type.</li>
</ul></td>
</tr>
</tbody>
</table>

**Updated project templates:**

<table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Agile Software Development - Scrum</td>
<td><ul>
<li>Renamed to &quot;Scrum software development&quot;</li>
<li>Default issue type: &quot;Story&quot;</li>
</ul></td>
</tr>
<tr class="even">
<td>Agile Software Development - Kanban</td>
<td><ul>
<li>Renamed to &quot;Kanban software development&quot;</li>
<li>Default issue type: &quot;Story&quot;</li>
</ul></td>
</tr>
<tr class="odd">
<td>Software Development</td>
<td><ul>
<li>This project template has been removed</li>
</ul></td>
</tr>
</tbody>
</table>

{{% note %}}

*You will need to use the JIRA EAP that includes JIRA Agile to see these three project templates. See [JIRA + JIRA Agile EAP downloads](#jira-jira-agile-eap-downloads) below.*

{{% /note %}}

### Attaching images and files

We've improved the way you can attach an image or file to an issue by allowing you to now drag and drop an image or file to any part of an issue screen. You can also use your regular paste options (using your keyboard or right-clicking) to paste a file or image from your clipboard to the issue. Due to this, we've removed the **Attach screenshot** menu item, and the **Attach file** menu item now opens a File browser dialog to allow you to browse to your file or image.

<img src="/server/jira/platform/images/attachimage.png" width="435" />

If you're using JIRA Service Desk, agents will also be able to add attachments directly to issues by dragging and dropping, or by pasting, and this will open an additional **Add attachments** dialog that gives you the ability to add a comment to the issue that relates to the image.

<img src="https://confluence.atlassian.com/download/attachments/777010089/sdimagecreate.png?version=2&amp;modificationDate=1440487920840&amp;api=v2" class="confluence-external-resource" width="435" />

### Inline issue create

Some time ago, we released a feature to JIRA Agile known as inline issue create, which gave users the ability to create issues on the fly without the need to click the Create button. In this release, we've added the ability to create issues inline to all projects. Simply click the **Create** link at the bottom of your issue link, add a summary and press enter to create your issue for that project.

### Crowd upgrade

Crowd has been upgraded to version 2.8.3, which fixes this bug: <a href="https://jira.atlassian.com/browse/JRA-43968" class="external-link">JRA-43968</a>.

### Soy upgrade

Atlassian Soy Template Renderer has been upgrade from 4.0.4 (in m03) to 4.1.2 in this upgrade. This new version includes performance improvements. See [JIRA 7.0 - Platform changes](/server/jira/platform/jira-7-0-platform-changes) for details.

### New guide: Extending the JIRA Import plugin

This guide helps you extend the JIRA importer plugin to include data from your plugins during project imports. Read the guide here: [Guide - Extending the JIRA Import plugin](/server/jira/platform/extending-the-jira-import-plugin).

### New tutorial: Omit web-resources when your plugin is unlicensed

If your plugin is not licensed in a JIRA instance, you can ensure that web-resources are not loaded to the front end. Omitting these files will improve the performance of the pages that your plugin affects, when the license is missing. Read the tutorial here: [Omit web-resources when your plugin is unlicensed tutorial](https://developer.atlassian.com/display/MARKET/Tutorial%3A+Omit+web-resources+when+your+plugin+is+unlicensed).

## JIRA + JIRA Agile EAP downloads

The following JIRA EAP downloads include JIRA Agile. 

-   <a href="http://www.atlassian.com/software/jira/downloads/binary/atlassian-jira-software-7.0.0-rc3-jira-7.0.0-rc3-x32.exe" class="external-link">7.0.0-RC3 (Windows 32 Bit Installer, EXE)</a>
-   <a href="http://www.atlassian.com/software/jira/downloads/binary/atlassian-jira-software-7.0.0-rc3-jira-7.0.0-rc3-x64.exe" class="external-link">7.0.0-RC3 (Windows 64 Bit Installer, EXE)</a>
-   <a href="http://www.atlassian.com/software/jira/downloads/binary/atlassian-jira-software-7.0.0-rc3-jira-7.0.0-rc3-x32.bin" class="external-link">7.0.0-RC3 (Linux 32 Bit Installer, BIN)</a>
-   <a href="http://www.atlassian.com/software/jira/downloads/binary/atlassian-jira-software-7.0.0-rc3-jira-7.0.0-rc3-x64.bin" class="external-link">7.0.0-RC3 (Linux 64 Bit Installer, BIN)</a>
-   <a href="http://www.atlassian.com/software/jira/downloads/binary/atlassian-jira-software-7.0.0-rc3-jira-7.0.0-rc3.zip" class="external-link">7.0.0-RC3 (ZIP Archive)</a>
-   <a href="http://www.atlassian.com/software/jira/downloads/binary/atlassian-jira-software-7.0.0-rc3-jira-7.0.0-rc3.tar.gz" class="external-link">7.0.0-RC3 (TAR.GZ Archive)</a>

## Upgrade notes (including known issues)

Please take note of the following information when installing or upgrading to this EAP release:

-   **Known issue with 7.0 EAP 01 (m01) **-- The 7.0 EAP 01 (m01) EAP milestone includes an incorrect ehcache.xml configuration file, which prevents JIRA from starting. If you have been using this milestone, we recommend that you use the latest milestone instead.
-   **REST API browser does not work with JIRA 7.0** -- We recommend browsing our online REST API documentation instead: [JIRA APIs](/server/jira/platform/jira-apis-32344053.html).

In addition to the points above, if you are upgrading from JIRA 6.3.x or earlier, read <a href="https://confluence.atlassian.com/display/JIRA/Skipping+Major+Versions+When+Upgrading+JIRA" class="external-link">Skipping Major Versions When Upgrading JIRA</a>. Remember that EAPs are **not supported** and **should not be used in production environments**.
