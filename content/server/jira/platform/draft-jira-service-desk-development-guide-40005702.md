---
title: Draft JIRA Service Desk Development Guide 40005702
aliases:
    - /server/jira/platform/draft-jira-service-desk-development-guide-40005702.html
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=40005702
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=40005702
confluence_id: 40005702
platform: server
product: jira
category: devguide
subcategory:
date: "2018-04-25"
---
# JIRA Developer Documentation : DRAFT - JIRA Service Desk development guide

## About JIRA Service Desk Development

You can develop plugins for JIRA Service Desk just as you can for other types of Atlassian applications. However, developing for JIRA Service Desk introduces a few wrinkles to the usual development process, as outlined below.

JIRA Service Desk is an application that is built on the JIRA platform and has its own APIs and plugin points. Therefore, to extend JIRA Service Desk, you actually need to create a JIRA plugin that interacts with these integration points in some way.

For now, JIRA Service Desk exposes a few plugin module types for extending Automation in JIRA Service Desk, [JIRA Service Desk: Automation Rule Components](https://developer.atlassian.com/jiradev/jira-platform/jira-service-desk/automation-rule-components).

## JIRA Service Desk plugin development flow

The high level steps for developing JIRA Service Desk plugins with the SDK are as follows:

1.  Create your plugin project as you would any other JIRA plugin, for instance, by using the `atlas-create-jira-plugin` SDK command. This command creates the plugin project files and skeleton code.
2.  When ready to build and test your plugin, run the `atlas-package` command from the project root directory. This command creates a packaged JAR file that you can install into JIRA (in the `target` sub-directory)
3.  Configure AMPS to run JIRA with JIRA Service Desk installed. If you haven't done this before, see [Configure AMPS to run JIRA Core with additional applications installed](https://developer.atlassian.com/display/DOCS/Configure+AMPS+to+run+JIRA+Core+with+additional+applications+installed).
4.  Start up JIRA. For early development and testing, you can use the development instance that the SDK gives you when you run the following command *(set the version to 7.0.0 or later)*: `atlas-run-standalone --product jira --version 7.0.0.` 
5.  Log in to JIRA as an administrator, using the default username and password combination of admin/admin.  
6.  Install the plugin you built in step \#2 with the `atlas-package` command. There are two ways that you can do this:
    1.  Go to the Administration Console and open the "Manage Add-ons" page. On the page, click **Upload add-on** and choose the JAR file that the `atlas-package` command generated for you. You'll find the JAR file in the `target` directory of your project home after running `atlas-package`.
    2.  Using the SDK command line tools: using you terminal, step into your project home directory and simply run `atlas-install-plugin` (after having packaged your plugin JAR using the `atlas-package` SDK command). This command will simply upload your plugin to UPM (and can considerably shorten your development flow).

You should now be able to see your add-on in the JIRA "Manage Add-ons" page as an installed plugin. Every time you change your plugin code or resources, reload your plugin repeating step \#6 above.

## JIRA Service Desk APIs

JIRA Service Desk has its own APIs, separate from the JIRA platform APIs.

-   **REST API** for **JIRA Service Desk Cloud**: <a href="https://docs.atlassian.com/jira-servicedesk/REST/cloud/" class="uri external-link">https://docs.atlassian.com/jira-servicedesk/REST/cloud/</a> *(<a href="https://docs.atlassian.com/jira-servicedesk/REST/" class="external-link">all versions</a>)*
-   **Java API** for **JIRA Service Desk Server (latest version)**: <a href="https://docs.atlassian.com/jira-servicedesk/server/" class="uri external-link">https://docs.atlassian.com/jira-servicedesk/server/</a> *(<a href="https://docs.atlassian.com/jira-servicedesk/" class="external-link">all versions</a>)*

You can find the links to the other JIRA API documentation here: [JIRA APIs](/server/jira/platform/jira-apis-32344053.html).
