---
aliases:
- /server/jira/platform/user-profile-plugin-module-15336252.html
- /server/jira/platform/user-profile-plugin-module-15336252.md
category: reference
confluence_id: 15336252
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=15336252
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=15336252
date: '2017-12-08'
legacy_title: User Profile Plugin Module
platform: server
product: jira
subcategory: modules
title: User profile
---
# User profile

<table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<tbody>
<tr class="odd">
<td><p>Available:</p></td>
<td><p>JIRA 5.2.1 and later</p></td>
</tr>
</tbody>
</table>

## Purpose of this Module Type

A user profile plugin module defines the content displayed in the 'Details', 'Preferences' and 'Assigned Open Issues per Project' panels on the <a href="https://confluence.atlassian.com/display/JIRA/Managing+your+User+Profile" class="external-link">User Profile screen</a> in JIRA. You can also define custom web panels to be displayed in the same column. Note, you cannot modify the Activity Stream nor add any web panels to the column that it is in.

<img src="/server/jira/platform/images/user-profile-summary-page.png" width="500" height="392" />

## Configuration

#### Overriding the default web panels on the User Profile page

The 'Details', 'Preferences' and 'Assigned Open Issues per Project' sections on the User Profile page are implemented as standard [web panels](/server/jira/platform/web-panel). The defaults for these web panels are shown below.

To override the default web panels, you need to do two things:

1.  For each web panel that you want to override, define the web-panel in your [atlassian-plugin.xml](/server/framework/atlassian-sdk/configuring-the-plugin-descriptor/)
specifying the appropriate key/location and the desired resource (as shown in the defaults below).
2.  Set the **weight** attribute to less than or equal to the default value of the desired web panel. For example, the default weight of the Preference web panel is 200. If you want to override it, you must set the weight less than or equal to 200 when defining your web panel.

**Default 'Details' web panel:**

``` xml
<web-panel key="custom-user-details" location="webpanels.user.profile.summary.details" weight="100">
    <resource name="view" type="velocity" location="view/details-profile.vm"/>
    <context-provider class="com.atlassian.jira.plugins.userprofile.DetailsUserProfilePanel"/>
</web-panel>  
```

**Default 'Preferences' web panel:**

``` xml
<web-panel key="custom-user-prefs" location="webpanels.user.profile.summary.preferences" weight="200">
    <resource name="view" type="velocity" location="view/preferences-profile.vm"/>
    <context-provider class="com.atlassian.jira.plugins.userprofile.PreferencesUserUserProfilePanel"/>
    <condition class="com.atlassian.jira.plugins.userprofile.conditions.ProfileUserEqualsCurrentUser" />
</web-panel>
```

**Default 'Assigned Open Issues per Project' web panel:**

``` xml
<web-panel key="custom-user-assigned" location="webpanels.user.profile.summary.assigned" weight="300">
    <resource name="view" type="velocity" location="view/assigned-and-open.vm"/>
    <context-provider class="com.atlassian.jira.plugins.userprofile.AssignedAndOpenUserProfilePanel"/>
    <condition class="com.atlassian.jira.plugins.userprofile.conditions.UserCanBrowseProfileUserProjects" />
</web-panel>
```

#### Adding a custom web panels to the User Profile page

To add a custom web panel, do the following:

1.  Define the custom web panel in your **atlassian-plugin.xml** specifying the appropriate key/location and the desired resource (as shown in the example below).
2.  Define the location of the custom web panel by setting the **weight** attribute to the desired value. The **weight** attribute for a custom web panel defines where the web panel will display in the left column of the User Profile page, from top to bottom.  
    For example, if you have not changed the default weights of the 'Details', 'Preferences' and 'Assigned Open Issues per Project' web panels, and you set the weight of your custom web panel to 150, it will display between the 'Details' and 'Preferences' web panels.  

**Example custom web panel for User Profile page:**

``` xml
<web-panel key="custom-profile-panel" location="webpanels.user.profile.summary.custom" weight="150">
    <resource name="view" type="velocity" location="templates/webpanel/custom2.vm"></resource>
</web-panel>
```
