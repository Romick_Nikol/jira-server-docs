---
aliases:
- /server/jira/platform/guide-jira-activity-streams-7897198.html
- /server/jira/platform/guide-jira-activity-streams-7897198.md
category: devguide
confluence_id: 7897198
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=7897198
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=7897198
date: '2017-12-08'
guides: guides
legacy_title: Guide - JIRA Activity Streams
platform: server
product: jira
subcategory: learning
title: Extending JIRA activity streams
---
# Extending JIRA activity streams

<table>
<colgroup>
<col style="width: 20%" />
<col style="width: 80%" />
</colgroup>
<tbody>
<tr class="odd">
<td><p>Available:</p></td>
<td><p>JIRA 4.4 and later</p></td>
</tr>
<tr class="even">
<td><p>Changes:</p></td>
<td><p>Please refer to the <a href="https://developer.atlassian.com/display/STREAMS">Activity Streams documentation</a> and especially the <a href="https://developer.atlassian.com/display/DOCS/Activity+Streams+Version+Matrix">version matrix</a> to see which version of Activity Streams is bundled with each version of JIRA.</p></td>
</tr>
</tbody>
</table>

This information is for developers who want to interact programmatically with Atlassian activity streams.

## What are Activity Streams?

An activity stream is a list of events, displayed to a user in an attractive and interactive interface. They are usually recent activities by the current user and other people using the same social site(s) or system(s).

An example is the <a href="https://confluence.atlassian.com/display/JIRA/Adding+the+Activity+Stream+Gadget" class="external-link">Activity Stream gadget</a> used on JIRA dashboards.

<img src="/server/jira/platform/images/activitystreamgadget-display.png" class="confluence-thumbnail" />

## Coding with Activity Streams

Take a look at the [Activity Streams documentation](https://developer.atlassian.com/display/STREAMS) to see how you can interact with Activity Streams in Atlassian products. In brief:

-   [Consume an activity stream](https://developer.atlassian.com/display/DOCS/Consuming+an+Activity+Streams+Feed) from an Atlassian application.
-   [Add your own inline actions](https://developer.atlassian.com/display/DOCS/Making+Pluggable+Inline+Actions+for+Activity+Streams) to an activity stream.
-   Add activities to a third party feed using the [Java API](https://developer.atlassian.com/display/DOCS/Adding+Activities+to+a+Third+Party+feed+with+the+Java+API) or the [REST API](https://developer.atlassian.com/display/DOCS/Adding+Activities+to+a+Third+Party+Feed+with+the+REST+API).
-   [Build your own Activity Streams provider](https://developer.atlassian.com/display/DOCS/Making+your+own+Activity+Streams+Provider).
