---
aliases:
- /server/jira/platform/preparing-for-jira-6.0-15335602.html
- /server/jira/platform/preparing-for-jira-6.0-15335602.md
category: devguide
confluence_id: 15335602
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=15335602
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=15335602
date: '2017-12-08'
legacy_title: Preparing for JIRA 6.0
platform: server
product: jira
subcategory: updates
title: Preparing for JIRA 6.0
---
# Preparing for JIRA 6.0

## Overview

6.0 is a major release for JIRA. As a result, there are a number of "breaking changes" that may break plugins. These include changes to JIRA's stable (Java) API, changes to core JIRA CSS styles, changes to JavaScript components that build UI, changes to HTML markup patterns.

**Note:** This documentation is intended for our developer audience. If you are looking for end user documentation, go <a href="https://confluence.atlassian.com/display/JIRA/JIRA+Documentation" class="external-link">here</a> instead

**You may need to update your plugin to work with JIRA 6.0.**

We will update this page as the release progresses. Please watch this page or check regularly to keep on top of changes. Please also see our [Java API Policy for JIRA](/server/jira/platform/java-api-policy-for-jira-4227213.html) for technical information on the JIRA Java APIs and our approach to changing them.

The major change for JIRA 6.0 is the redesign of the JIRA look and feel, according to the new <a href="https://developer.atlassian.com/design/" class="external-link">Atlassian Design Guidelines</a>. Details on this change and others are summarised below.

#### Atlassian Design Guidelines implementation

The JIRA look and feel is being redesigned in JIRA 6.0 according to the new <a href="https://developer.atlassian.com/design/" class="external-link">Atlassian Design Guidelines</a> (ADG). We are moving away from JIRA-specific markup patterns in favor of those provided by [AUI](https://developer.atlassian.com/display/AUI/Atlassian+User+Interface+%28AUI%29+Developer+Documentation). This move is an essential part of implementing the [Atlassian Design Guidelines](https://developer.atlassian.com/design/) in JIRA. Adopting AUI's patterns enables developers to quickly build UI in JIRA that complies with the ADG and works across Atlassian's product suite. 

Please see the following guides for details:  

-   [ADG Migration from 5.x to 6.0](/server/jira/platform/adg-migration-from-5-x-to-6-0) -- This guide provides an overview of the ADG changes and how you can manage the changes for your plugin.
-   <a href="https://developer.atlassian.com/display/JIRADEV/Bonfire+ADG+Migration+Guidelines" class="external-link">JIRA Agile ADG Migration Guidelines</a> -- This guide is a case study of the strategies that the Atlassian JIRA Agile team are using to support the JIRA Agile plugin in JIRA 5.x (pre-ADG) and JIRA 6.0 (post-ADG). Note, JIRA Agile was previously named Bonfire.
-   <a href="https://developer.atlassian.com/display/JIRADEV/Using+Soy+Templates" class="external-link">Using Soy Templates</a> -- This page contains basic information on how to use Soy templates, if you want to use them. We use Soy templates to render AUI and JIRA markup patterns.

#### Inline Edit for JIRA Plugin Developers

With inline edit enabled, document content changes frequently and there are several situations where plugins may need to reconfigure elements (e.g. bind event handlers, create elements). With JIRA 6.0, there are several additional `reason` arguments.

Additionally, this statement is no longer true: "It is guaranteed that neither context nor any of its descendants have previously been passed to a `NEW_CONTENT_ADDED` handler, so it is safe to assume that they have not been configured."

For more information, please see [Guide - Inline edit for JIRA plugins](/server/jira/platform/extending-inline-edit-for-jira-plugins).

#### Rename Users

JIRA has introduced the ability to edit the username of a User in 6.0. Plugin developers will need to make their plugin work in the case of users with changing usernames; for details, see [Renamable Users in JIRA 6.0](/server/jira/platform/renamable-users-in-jira-6-0). Plugin developers that have trouble with unit tests throwing `IllegalStateException` can also find information about the [ComponentAccessor](/server/jira/platform/renamable-users-in-jira-6-0#component-accessor) on that page.

#### New plugin points for the User Profile page

We have implemented new plugin points for the web panels in the User Profile page. You can override the default 'Details', 'Preferences' and 'Assigned Open Issues per Project' web panels as well as add your own custom web panels.

-   [User Profile Plugin Module](/server/jira/platform/user-profile)

#### Check your plugins for API compatibility

The <a href="http://checkup.atlassian.com/" class="external-link">Atlassian Plugin Checkup</a> lets you test your plugin's compatibility with different versions of our products. It helps you identify API changes between versions of a product. Also see the [Plugins 3 Migration Guide](/server/jira/platform/plugins-3-migration-guide) for details on breaking changes.

#### Java and REST APIs

No features have been removed and we haven't broken any REST APIs in JIRA 6.0. We've added a few items to our REST APIs, but those changes have been very minor. JIRA 7.0 will be our next API breaking release.

We have made changes to JIRA's stable (Java) API. For details on this, see the [Clirr results](https://developer.atlassian.com/static/clirr/jira6clirr/clirr-report.html). Also see our <a href="https://developer.atlassian.com/display/JIRADEV/Java+API+Policy+for+JIRA" class="external-link">Java API policy for JIRA</a> for technical information on the JIRA Java APIs and our approach to changing them.

The Java API changes of note are:

-   We removed or modified a number of previously deprecated methods.
-   We removed all "backend webwork actions" (deprecated since at least JIRA 5.0).
-   We changed the expected input and output type of user-based custom fields from a User object to an `ApplicationUser` object (this is for the [Renamable Users in JIRA 6.0](/server/jira/platform/renamable-users-in-jira-6-0) functionality).

#### Velocity files must encode the results of methods

JIRA 6.0 will force velocity files to encode the results of methods. To prepare for this, enable HTML encoding on all of your plugin's velocity files. See the following page for instructions:

-   [HTML escaping for velocity templates](/server/jira/platform/jira-6-0-html-escaping-for-velocity-templates)

#### IssueTableService has moved

JIRA 6.0 will see class `com.atlassian.jira.bc.issue.table.IssueTableService` and its supporting classes move from JIRA core to the Issue Navigator plugin. IssueTableService can be found at `com.atlassian.jira.plugin.issuenav.service.issuetable.IssueTableService` and imported by creating a <a href="https://developer.atlassian.com/display/PLUGINFRAMEWORK/Component+Import+Plugin+Module" class="current ancestor-link">Component Import Plugin Module</a>.

#### atlassian-jira module has been removed

When we introduced the JIRA API in JIRA 5.0 we added new modules, `jira-api` and `jira-core`, but we left a shaded `atlassian-jira` module for legacy reasons.  This has now been removed, so you will have to explicitly depend on the `jira-core` module if you need to depend on implementation classes. Be aware that we can and do change implementation classes often, including in bugfix releases.

#### Module Descriptor Changes

If your plugin creates a new Module Descriptor and subclasses `AbstractModuleDescriptor` or `WebResourceModuleDescriptor`, you may have to make changes to your code as we have removed the deprecated no-args constructor from these classes.  You should use the constructor that takes a `ModuleFactory` instead.

#### atlassian-plugin.xml Changes

The rules for keys that are used in your `atlassian-plugin.xml` are much stricter in JIRA 6.0. You must now make sure that the keys that you use for your plugin module descriptors are unique. If they are not unique, the plugin will fail to load and you will see an exception in JIRA's application logs, like the following:

``` java
java.lang.IllegalArgumentException: duplicate key: <the-duplicate-key-in-error>
```

CollectionUtil class has been removed

Previously atlassian-plugin-core contained a series of collection helper classes called com.atlassian.plugin.util.collect.CollectionUtil.   This has been removed.  Instead you should migrate your code to use the (much better) Google Guava collection classes in com.google.common.base.\*

#### CalculatedCFType no longer implements SortableCustomField by default

{{% warning %}}

`SortableCustomField` has major performance problems, and should be avoided where possible.

{{% /warning %}}

If you want your custom field to be sortable you should create a `CustomFieldSearcher`, and implement `NaturallyOrderedCustomFieldSearcher` or  `SortableCustomFieldSearcher.`

If you really want your calculated Custom Field to implement `SortableCustomField`, you can still declare it yourself (that is, opt-in). The method has been left in place (for now), it is only the `implements` declaration in the class definition that has been removed.

#### Configurable maximum length for Project name and key

In JIRA 6.0, the length of project names and project keys is configurable. When a JIRA administrator changes the maximum length of these fields, it doesn't affect any existing projects. While creating a project via the ProjectService, the lengths of these values are validated.

The currently configured maximum length for project keys and names in an instance can be retrieved using either of these functions:

-   `com.atlassian.jira.bc.project.ProjectService.getMaximumKeyLength()`
-   `com.atlassian.jira.bc.project.ProjectService.getMaximumNameLength()`

The following constants are deprecated and will be incorrect if the value is changed in the configuration:

-   `ProjectService.MAX_NAME_LENGTH`
-   `ProjectService.MAX_KEY_LENGTH`

#### JSON-P no longer supported

JSON-P will not be supported in JIRA 6 for security reasons:  it is possible for an attacker use this to view information in a JIRA instance that they do not have permission to see.

If you have built integrations that rely on JSON-P, you can override this by setting the `atlassian.allow.jsonp` <a href="https://confluence.atlassian.com/display/JIRA/Recognized+System+Properties+for+JIRA" class="external-link">system property</a> to `true`.

#### SOAP and XML-RPC deprecated in JIRA 6.0

Please note, the SOAP and XML-RPC remote APIs are being deprecated in JIRA 6.0. They will continue to work without change during the JIRA 6.x lifecycle, however we will not fix bugs nor implement new features related to these APIs. When JIRA 7.0 is released, the code will be removed from JIRA and these APIs will become unsupported by Atlassian. See [SOAP and XML-RPC API Deprecation Notice](/server/jira/platform/soap-and-xml-rpc-api-deprecation-notice) for more details.
