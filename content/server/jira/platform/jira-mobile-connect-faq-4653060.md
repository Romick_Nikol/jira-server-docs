---
title: JIRA Mobile Connect Faq 4653060
aliases:
    - /server/jira/platform/jira-mobile-connect-faq-4653060.html
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=4653060
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=4653060
confluence_id: 4653060
platform: server
product: jira
category: devguide
subcategory:
date: "2018-04-25"
---
# JIRA Developer Documentation : JIRA Mobile Connect FAQ

# General Questions

### How much does JIRA Mobile Connect cost?

It's free. The library is open source and available for anyone to fork on <a href="https://bitbucket.org/atlassian/jiraconnect-ios" class="external-link">Bitbucket</a>.

### Do mobile device users count towards my JIRA user license?

No. All mobile device users are represented by one user in JIRA, so this consumes one JIRA user license. In JIRA, UUIDs are used to distinguish actual mobile devices.

### Can apps go into the Apple Store with  JIRA Mobile Connect ?

Yes. There are already Apps in the App Store using JIRA Mobile Connect for Feedback and Crash Reporting.

### Other Questions?

Have more questions about JIRA Mobile Connect? Check out <a href="https://answers.atlassian.com/tags/jira-mobile-connect/" class="external-link">Atlassian Answers</a> or simply <a href="https://answers.atlassian.com/questions/ask/?tags=jira-mobile-connect" class="external-link">ask us</a>.

# Usage Questions

### What data does JIRA Mobile Connect collect?

JIRA Mobile Connect collects the following data:

-   Device Version (iPad, iPhone, iPodTouch..)
-   iOS Version (5.0, 4.3.3, 4.2, 4.0..)
-   Your App's Version (As defined in the Info.plist for your App)
-   Any custom data you also wish to capture. These get mapped to custom fields in JIRA for searching, sorting and reporting.
-   Any custom attachments your App may wish to include with each issue.
-   Location (opt-in)
-   Crash Reports

The user can also send image attachments and voice recordings. You can disable either or both of these.

### Can users keep location private?

The collection of location data is entirely opt-in. 

### Can JIRA Mobile Connect collect feedback without a crash or user action?

This has been requested previously and is on our Roadmap. Future versions of JIRA Mobile Connect will allow issue creation programmatically from your App.

### Does JIRA Mobile Connect support landscape mode?

Yes.

### Does JIRA Mobile Connect support multiple applications?

Yes. JIRA supports multiple projects. Each application maps to a single project.

### Does JIRA Mobile Connect support multiple attachments?

Yes. You can include multiple screenshots and voice recordings per feedback.

### What languages does JIRA Mobile Connect support?

English, German currently. Feel free to <a href="https://bitbucket.org/atlassian/jiraconnect-ios/src/84e8daa2a6a8/JIRAConnect/JMCClasses/Resources/en.lproj" class="external-link">add your own</a> and send us a pull request!

# Technical Questions

### How much size does JIRA Mobile Connect add to the binaries?

JIRA Mobile Connect will add about 250KB to your app size. For example, the Angry Nerds sample app is 606KB without JMC integration; and 848KB when JMC is added.
If your app already contains some of the 3rd party libraries JMC bundles, then this difference will be less.

### Where do you pass / store credentials to JIRA?

When the user first installs your App, JIRA Mobile Connect creates a UUID. This is stored in NSUserDefaults on the device and sent with each request to identify the user/device.

### Which crash reporting library does JIRA Mobile Connect use?

JIRA Mobile Connect uses PLCrashReporter by Plausible Labs.

### Does JIRA Mobile Connect offer aggregation?

JIRA Mobile Connect currently does not aggregate or group issues based on similarity.
JIRA however offers powerful search and bulk edit functionality to ease the pain of dealing with many similar reports.

### Does JIRA Mobile Connect symbolicate the crash logs?

No, JIRA Mobile Connect currently does not automatically do this. You can however drag a crash report from JIRA, into XCode on the machine that built the app to symbolicate logs.

# Feedback

### How do I provide feedback on JIRA Mobile Connect?

We have setup JIRA project to <a href="http://connect.onjira.com/browse/CONNECT" class="external-link">collect your feedback</a>. Please file a ticket for any issues or feature improvements you have.