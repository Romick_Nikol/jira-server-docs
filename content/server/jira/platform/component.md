---
aliases:
- /server/jira/platform/component-plugin-module-4227192.html
- /server/jira/platform/component-plugin-module-4227192.md
category: reference
confluence_id: 4227192
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=4227192
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=4227192
date: '2017-12-08'
legacy_title: Component Plugin Module
platform: server
product: jira
subcategory: modules
title: Component
---
# Component

<table>
<colgroup>
<col style="width: 20%" />
<col style="width: 80%" />
</colgroup>
<tbody>
<tr class="odd">
<td><p>Status:</p></td>
<td><p>This feature is <strong>DEPRECATED</strong> in favor of <a href="https://bitbucket.org/atlassian/atlassian-spring-scanner/src/1.2.x/?_ga=2.149702068.308953800.1528098287-1447739025.1521018839">Atlassian Spring Scanner</a>.</p></td>
</tr>
</tbody>
</table>

#### Introduction

A component plugin module defines a Java component which will be injected into the component system used by your plugin.

#### Components under Plugins2

A component in a Plugins2 plugin will be installed into the Spring container for your plugin.  
It will be "private" by default. This means classes in your plugin will be able to get that component dependency-injected, but other plugins will not.  
However the component can be declared public, which allows other Plugins2 plugins to [import](/server/jira/platform/component-import) a dependency on your component.

See the [Plugins2 Component Plugin Module](https://developer.atlassian.com/display/DOCS/Component+Plugin+Module) documentation for details.

#### Replacing core components considered harmful!

A number of plugin developers in the past used Plugins1 Components to replace/override core JIRA components.  
This is not guaranteed to work, and can cause subtle problems that would be very difficult to trace even if it "appears" to work.  
Additionally, the behaviour can change wildly from version to version because of changes in the startup order of components and component inter-dependencies.
