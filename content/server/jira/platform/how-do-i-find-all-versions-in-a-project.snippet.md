---
aliases:
- /server/jira/platform/4227106.html
- /server/jira/platform/4227106.md
category: devguide
confluence_id: 4227106
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=4227106
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=4227106
date: '2017-12-08'
legacy_title: How do I find all Versions in a Project?
platform: server
product: jira
subcategory: other
title: How do I find all Versions in a Project?
---
# How do I find all Versions in a Project?

Given an issue, you can retrieve all versions of its Project.

``` javascript
GenericValue project = projectManager.getProject(myIssueGV);
List versions = versionManager.getVersions(project);
Collections.sort(versions);
```
