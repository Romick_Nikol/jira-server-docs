---
aliases:
- /server/jira/platform/preparing-for-jira-5.0-4227155.html
- /server/jira/platform/preparing-for-jira-5.0-4227155.md
category: devguide
confluence_id: 4227155
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=4227155
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=4227155
date: '2017-12-08'
legacy_title: Preparing for JIRA 5.0
platform: server
product: jira
subcategory: updates
title: Preparing for JIRA 5.0
---
# Preparing for JIRA 5.0

This documentation is intended for developers integrating with JIRA who want to start getting ready for JIRA 5.0.

## How do I get JIRA 5.0?

Please watch the <a href="http://confluence.atlassian.com/display/JIRA/EAP+Releases" class="external-link">JIRA EAP releases</a> and <a href="http://confluence.atlassian.com/display/JIRA/Beta+Releases" class="external-link">JIRA Beta releases</a> for pre-release versions of JIRA 5.0 that you can use for testing purposes.

## In depth overview

JIRA 5.0 introduces several changes that may break existing plugins which are not bundled with JIRA. If you are using or have been involved in the development of such a plugin, it may need to be updated to work with JIRA 5.0. Please read through the information below to see if any of this content is relevant to your plugin.

If you are using a plugin developed by a third party, please check with the plugin's author to see if the plugin has been tested with JIRA 5.0.

**Please Note:**

-   This is not the complete list of changes for JIRA 5.0 - it only describes changes in JIRA 5.0 that will affect plugin developers.
-   For details about which versions of Atlassian's Plugin Development Platform and its components (such as the Plugin Framework, Shared Access Layer, Atlassian User Interface and the Atlassian REST plugin) are included in JIRA 5.0, please refer to [Plugin Development Platform Version Matrix](https://developer.atlassian.com/display/DOCS/About+the+Platform). Be aware that this list of matrices may not yet be up to date for JIRA 5.0.

### JIRA 5.0 REST API

The JIRA REST API takes a major step forward in JIRA 5.0, with the addition of REST API methods to create, edit and update issues. Check out a quick example of creating an issue, and a guide to issues and fields in the [JIRA 5.0 REST API Overview](/server/jira/platform/jira-rest-api-in-jira-5-0). For the full details, see the <a href="http://docs.atlassian.com/jira/REST/5.0-beta1/" class="external-link">REST API documentation</a>.

Please provide feedback on the REST API <a href="https://jira.atlassian.com/browse/JRA-22139" class="external-link">on this issue</a>.

### JIRA 5.0 Java API module

In JIRA 5.0, we're clearly defining what the JIRA API is. Before JIRA 5.0, the JIRA API was any public interface or class within the JIRA source that seemed useful to add-on developers. While some parts of the source are built to be more stable, there was no clear indication in JIRA of what was or wasn't stable.

We are now creating an explicit "stable" API for JIRA which lives in a separate Maven module called "jira-api". See the [Java API Policy for JIRA 5.0 onwards](/server/jira/platform/java-api-policy-for-jira-4227213.html).

Please provide feedback on the Java API <a href="https://jira.atlassian.com/browse/JRA-25596" class="external-link">on this issue</a>.

### JIRA 5.0 Java API Changes

As part of clearly defining the public, supported Java API for JIRA, several third-party libraries have been upgraded, previously deprecated classes have been removed and several existing plugin points have changed behaviour and/or interface. See [Java API Changes in JIRA 5.0](/server/jira/platform/java-api-changes-in-jira-5-0).

### JIRA 5.0 Markup, CSS and Decorator changes

JIRA 5.0 includes significant changes to markup, CSS and decorators. We're moving towards a common markup pattern for all our applications. See [Changes to markup, CSS and Decorators in JIRA 5.0](/server/jira/platform/changes-to-markup-css-and-decorators-in-jira-5-0).

### Custom Fields that use CSS or JavaScript Web Resources

Plugin developers commonly use CSS or JavaScript web resources for custom fields, in order to modify the look and feel of these fields or provide these fields with dynamic interaction capabilities. Prior to JIRA 5.0, this would typically have been achieved using using the <a href="http://docs.atlassian.com/atlassian-plugins-webresource/2.10.1/atlassian-plugins-webresource/apidocs/com/atlassian/plugin/webresource/WebResourceManager.html" class="external-link">WebResourceManager</a> from a custom field's `"edit"` HTML. However, this is no longer supported in JIRA 5.0 and any custom field types that do so will not have their requested web resources included in JIRA 5.0's new **Edit Issue** and **Create Issue** dialog boxes. See [Custom Fields that use CSS or JavaScript Web Resources in JIRA 5.0](/server/jira/platform/custom-fields-that-use-css-or-javascript-web-resources-in-jira-5-0) for details on how to update such custom fields for JIRA 5.0.

### Changes to Issue Tab Panels

JIRA 5.0 can load issue tabs using an AJAX request, without having to reload the rest of the view issue page. All built-in issue tab panels support this new mechanism, and we encourage plugin developers to update their <a href="http://confluence.atlassian.com/x/4cUB" class="external-link">issue-tabpanel modules</a> in order to provide a consistent and improved user experience.

In most cases it's simply a matter of enabling the feature in your `atlassian-plugin.xml`. See [Loading Issue Tab Panels with AJAX](/server/jira/platform/loading-issue-tab-panels-with-ajax) for more details.

### Remote Issue Links API

JIRA 5.0 introduces a new feature called Remote Issue Links. Similar to the existing links that associate one JIRA issue with another, Remote Issue Links associate a JIRA issue with a remote object such as a help desk ticket or a test case. See [Guide - JIRA Remote Issue Links](/server/jira/platform/creating-remote-issue-links).

### Activity Streams API

A new Activity Streams API is available in JIRA 5.0 for creating activities via a simple REST API call or Java Method.

To get started, check out [a few cool demos of the new Activity Streams APIs](/server/jira/platform/activity-streams-demos), and take a look at the tutorial on the [REST API](https://developer.atlassian.com/display/DOCS/Adding+Activities+to+a+Third+Party+Feed+with+the+REST+API) and the [Java API](https://developer.atlassian.com/display/DOCS/Adding+Activities+to+a+Third+Party+feed+with+the+Java+API).

### Changes to Active Objects

Thank you for all of your help, testing, and feedback on the [Active Objects](https://developer.atlassian.com/display/AO) (AO) data storage layer over the last few months. As a result of all this hard work, we have done a significant refactoring of the AO library in order to make it a rock solid addition to the Atlassian developers platform. **Active Objects 0.18.4** is the first AO version in this series that you should use. Previous versions had bugs that will cause problems.

Please refer to the [AO 0.18.x Upgrade Guide](https://developer.atlassian.com/display/DOCS/AO+0.18.x+Upgrade+Guide) and [How to upgrade AO to 0.18.x in JIRA 5](https://developer.atlassian.com/display/DOCS/How+to+upgrade+AO+to+0.18.x+in+JIRA+5).

### Changes to the Jackson dependency

Plugins may run into two problems with the way the <a href="http://jackson.codehaus.org/" class="external-link">Jackson</a> JSON library is included in JIRA 5.0.

-   Plugins may trying to import a version of Jackson via OSGi that is not in JIRA.
-   A Plugin's functional tests may fail to run because the wrong version of Jackson is used.

The solution to the first problem is to ensure your OSGi import for Jackson is at 1.4.4. Due to <a href="https://studio.atlassian.com/browse/REST-181" class="external-link">REST-181</a>, the rest-module was exporting into OSGi Jackson version 1.4.4 under an incorrect version number (2.5.0). Plugins that were attempting to import Jackson 1.6.x were successfully wired to the wrong version of Jackson. In JIRA 5.0, the only version of Jackson available in OSGi is 1.4.4.

The second problem is caused when plugins have a test dependency on JIRA's func-tests, in order to write their own functional tests. JIRA's func-tests require Jackson 1.6.1, but maven will create the test classpath with 1.4.4. The solution is to add a test dependency on Jackson 1.6.1.

### Changes to the RPC Plugin

In JIRA 5.0, the RPC plugin was converted from a version 1 to a version 2 plugin framework plugin. Developers whose plugins depended on components provided by JIRA's RPC plugin in a version of JIRA prior to 5.0 will need to update their plugins to make them compatible with the version 2 plugin framework. This is likely to involve adding `<component-import>` declaration elements for components that need to be constructor-injected. See [Converting from Version 1 to Version 2 (OSGi) Plugins](https://developer.atlassian.com/display/DOCS/Converting+from+Version+1+to+Version+2+%28OSGi%29+Plugins) and the [Component Import Plugin Module](https://developer.atlassian.com/display/DOCS/Component+Import+Plugin+Module) type documentation for more information.

If you are interested in the implementation of the RPC plugin, you can obtain the source code for this plugin from https://svn.atlassian.com/svn/public/atlassian/rpc-jira-plugin.

### Changes to MultiMap

We have had to make a breaking change to MultiMap in order to prepare for JDK 7.  
The `put()` method will conflict with Map.put() after generics erasure when attempting to compile under JDK 7.  
In order to fix this we have renamed the method to `putSingle()`.

``` java
  boolean putSingle(K key, V value);
```

### Migrating a plugin from JIRA 4.x to JIRA 5.x

Do you have a plugin that runs in version 4.x of JIRA, and you want to convert it to run in version 5.0 of JIRA? See [How to migrate a plugin from JIRA 4.x to JIRA 5.0](/server/jira/platform/how-to-migrate-a-plugin-from-jira-4-x-to-jira-5-0).
