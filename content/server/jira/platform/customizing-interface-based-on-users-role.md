---
aliases:
- /server/jira/platform/4227169.html
- /server/jira/platform/4227169.md
category: devguide
confluence_id: 4227169
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=4227169
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=4227169
date: '2017-12-08'
guides: tutorials
legacy_title: Customising interface based on user's role
platform: server
product: jira
subcategory: learning
title: Customizing interface based on user's role
---
# Customizing interface based on user's role

Sometimes it is useful to remove certain elements from JIRA's user interface if the user does not belong to a certain group. Most of the time this can be achieved by editing certain JSP / Velocity files. The JSP files are easy to edit as they are text files, and one does not need access to JIRA's source code.

In velocity, you can hide certain elements from the UI by surrounding the relevant code with:

``` java
#if ($authcontext.user.inGroup('jira-administrators'))
...
#end
```

So to hide the security level you need to edit the file `WEB-INF\classes\templates\jira\issue\field\comment-edit.vm` and end up with something like this.

``` java
#if ($authcontext.user.inGroup('jira-administrators'))
    #if ($commentLevels && !$commentLevels.isEmpty())
        #controlHeader ($action 'commentLevel' $i18n.getText('comment.viewable') false $displayParameters.get('noHeader'))
        ...
        #controlFooter ($action '' $displayParameters.get('noHeader'))
    #end
#end
```

You need to update **both** places in the file and then restart JIRA.

{{% note %}}

If you make changes such as this, you will need to **remember** to port them to a new version of JIRA when you upgrade

{{% /note %}}
