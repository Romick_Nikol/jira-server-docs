---
title: JIRA REST API Example Query Issues 6291606
aliases:
    - /server/jira/platform/jira-rest-api-example-query-issues-6291606.html
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=6291606
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=6291606
confluence_id: 6291606
platform: server
product: jira
category: devguide
subcategory:
date: "2018-04-25"
---
# JIRA Developer Documentation : JIRA REST API Example - Query issues

<table>
<colgroup>
<col style="width: 20%" />
<col style="width: 80%" />
</colgroup>
<tbody>
<tr class="odd">
<td><p>Applicable:</p></td>
<td><p>JIRA 5.0 and later.</p></td>
</tr>
</tbody>
</table>

The REST API allows you to send a JQL query and receive a subset issues.

### Query of issues assigned to user 'fred'

In this example, we supply a single URL parameter (`jql`) which contains the JQL query.

##### Request

``` javascript
curl -D- \
  -u fred:fred \
  -X GET \
  -H "Content-Type: application/json" \
  http://kelpie9:8081/rest/api/2/search?jql=assignee=fred
```

##### Response

``` javascript
{
    "expand": "schema,names",
    "startAt": 0,
    "maxResults": 50,
    "total": 6,
    "issues": [
        {
            "expand": "html",
            "id": "10230",
            "self": "http://kelpie9:8081/rest/api/2/issue/BULK-62",
            "key": "BULK-62",
            "fields": {
                "summary": "testing",
                "timetracking": null,
                "issuetype": {
                    "self": "http://kelpie9:8081/rest/api/2/issuetype/5",
                    "id": "5",
                    "description": "The sub-task of the issue",
                    "iconUrl": "http://kelpie9:8081/images/icons/issue_subtask.gif",
                    "name": "Sub-task",
                    "subtask": true
                },
.
.
.
                },
                "customfield_10071": null
            },
            "transitions": "http://kelpie9:8081/rest/api/2/issue/BULK-62/transitions",
        },
        {
            "expand": "html",
            "id": "10004",
            "self": "http://kelpie9:8081/rest/api/2/issue/BULK-47",
            "key": "BULK-47",
            "fields": {
                "summary": "Cheese v1 2.0 issue",
                "timetracking": null,
                "issuetype": {
                    "self": "http://kelpie9:8081/rest/api/2/issuetype/3",
                    "id": "3",
                    "description": "A task that needs to be done.",
                    "iconUrl": "http://kelpie9:8081/images/icons/task.gif",
                    "name": "Task",
                    "subtask": false
                },
.
.
.
                  "transitions": "http://kelpie9:8081/rest/api/2/issue/BULK-47/transitions",
        }
    ]
}
```

### Query of issues assigned to user 'fred' whilst restricting the number of results

In this example, we supply two additional URL parameters (to `jql`) -- `startAt` and `maxResults`, which respectively specify the starting issue returned in the JQL results and the number of issues from that starting issue.

##### Request

``` javascript
curl -D- \
  -u fred:fred \
  -X GET \
  -H "Content-Type: application/json" \
  http://kelpie9:8081/rest/api/2/search?jql=assignee=fred&startAt=2&maxResults=2
```

##### Response

``` javascript
{
    "expand": "schema,names",
    "startAt": 2,
    "maxResults": 2,
    "total": 6,
    "issues": [
        {
            "expand": "html",
            "id": "10123",
            "self": "http://kelpie9:8081/rest/api/2/issue/BULK-38",
            "key": "BULK-38",
            "fields": {
                "summary": "aaaaa",
                "timetracking": null,
                "issuetype": {
                    "self": "http://kelpie9:8081/rest/api/2/issuetype/5",
                    "id": "5",
                    "description": "The sub-task of the issue",
                    "iconUrl": "http://kelpie9:8081/images/icons/issue_subtask.gif",
                    "name": "Sub-task",
                    "subtask": true
                },
.
.
.
            },
            "transitions": "http://kelpie9:8081/rest/api/2/issue/BULK-38/transitions",
        },
        {

            "expand": "html",
            "id": "10108",
            "self": "http://kelpie9:8081/rest/api/2/issue/BULK-32",
            "key": "BULK-32",
            "fields": {
                 "summary": "subtasks are important, too",
                 "timetracking": null,
                 "issuetype": {
                     "self": "http://kelpie9:8081/rest/api/2/issuetype/5",
                     "id": "5",
                     "description": "The sub-task of the issue",
                     "iconUrl": "http://kelpie9:8081/images/icons/issue_subtask.gif",
                     "name": "Sub-task",
                     "subtask": true
                 },
.
.
.
            },
            "transitions": "http://kelpie9:8081/rest/api/2/issue/BULK-32/transitions",
        }
    ]
}
```

### Query of issues assigned to user 'fred' with ordered results

In this example, we specify a sort order for the results using an "order by" clause in the JQL query itself.

{{% note %}}

Ordering JQL results is not conducted via a URL parameter in your REST API call.

{{% /note %}}

##### Request

``` javascript
curl -D- \
  -u fred:fred \
  -X GET \
  -H "Content-Type: application/json" \
  http://kelpie9:8081/rest/api/2/search?jql=assignee=fred+order+by+duedate
```

##### Response

``` javascript
{
    "expand": "schema,names",
    "startAt": 0,
    "maxResults": 50,
    "total": 6,
    "issues": [
        {
            "expand": "html",
            "id": "10123",
            "self": "http://kelpie9:8081/rest/api/2/issue/BULK-38",
            "key": "BULK-38",
            "fields": {
                "summary": "aaaaa",
                "timetracking": null,
                "issuetype": {
                    "self": "http://kelpie9:8081/rest/api/2/issuetype/5",
                    "id": "5",
                    "description": "The sub-task of the issue",
                    "iconUrl": "http://kelpie9:8081/images/icons/issue_subtask.gif",
                    "name": "Sub-task",
                    "subtask": true
                },
.
.
.
            },
            "transitions": "http://kelpie9:8081/rest/api/2/issue/BULK-38/transitions",
        },
        {

            "expand": "html",
            "id": "10108",
            "self": "http://kelpie9:8081/rest/api/2/issue/BULK-32",
            "key": "BULK-32",
            "fields": {
                 "summary": "subtasks are important, too",
                 "timetracking": null,
                 "issuetype": {
                     "self": "http://kelpie9:8081/rest/api/2/issuetype/5",
                     "id": "5",
                     "description": "The sub-task of the issue",
                     "iconUrl": "http://kelpie9:8081/images/icons/issue_subtask.gif",
                     "name": "Sub-task",
                     "subtask": true
                 },
.
.
.
            },
            "transitions": "http://kelpie9:8081/rest/api/2/issue/BULK-32/transitions",
        }
    ]
}
```

### Query of issues whilst restricting the result's fields

In this example, we supply an additional URL parameter (to `jql`) -- `fields`, which lists the JIRA fields returned in the JQL results. Each JIRA field in the list should be comma-separated -- e.g. `fields=id,key`.

{{% note %}}

Be aware that some extra data is always returned in the JSON response.

{{% /note %}}

##### Request

``` javascript
curl -D- \
  -u fred:fred \
  -X GET \
  -H "Content-Type: application/json" \
  'http://kelpie9:8081/rest/api/2/search?jql=project=QA+order+by+duedate&fields=id,key'
```

##### Response

``` javascript
{
    "expand": "schema,names",
    "startAt": 0,
    "maxResults": 50,
    "total": 18,
    "issues": [
        {
            "expand": "html",
            "id": "10050",
            "self": "http://kelpie9:8081/rest/api/2/issue/QA-19",
            "key": "QA-19",
            "transitions": "http://kelpie9:8081/rest/api/2/issue/QA-19/transitions"
        },
        {
            "expand": "html",
            "id": "10051",
            "self": "http://kelpie9:8081/rest/api/2/issue/QA-20",
            "key": "QA-20",
            "transitions": "http://kelpie9:8081/rest/api/2/issue/QA-20/transitions"
        },
.
.
.
        {
            "expand": "html",
            "id": "10053",
            "self": "http://kelpie9:8081/rest/api/2/issue/QA-22",
            "key": "QA-22",
            "transitions": "http://kelpie9:8081/rest/api/2/issue/QA-22/transitions"
        },
        {
            "expand": "html",
            "id": "10389",
            "self": "http://kelpie9:8081/rest/api/2/issue/QA-35",
            "key": "QA-35",
            "transitions": "http://kelpie9:8081/rest/api/2/issue/QA-35/transitions"
        }
    ]
}
```

### Query using POST

If the JQL query is too large to specify in a URL parameter, you should POST your JQL query (in JSON format) to the REST API '`search`' resource instead. Any additional URL parameters (to `url`) described above, should be included in your JSON-formatted JQL query.

##### Request

``` javascript
curl -D- \
  -u admin:admin \
  -X POST \
  -H "Content-Type: application/json" \
  --data '{"jql":"project = QA","startAt":0,"maxResults":2,"fields":["id","key"]}' \
  "http://kelpie9:8081/rest/api/2/search"
```

##### Response

``` javascript
{
    "maxResults": 2,
    "startAt": 0,
    "total": 18,
    "expand": "schema,names",
    "issues": [
        {
            "expand": "html",
            "id": "10393",
            "key": "QA-36",
            "self": "http://kelpie9:8081/rest/api/2/issue/QA-36",
            "transitions": "http://kelpie9:8081/rest/api/2/issue/QA-36/transitions"
        },
        {
            "expand": "html",
            "id": "10389",
            "key": "QA-35",
            "self": "http://kelpie9:8081/rest/api/2/issue/QA-35",
            "transitions": "http://kelpie9:8081/rest/api/2/issue/QA-35/transitions"
        }
    ]
}
```
