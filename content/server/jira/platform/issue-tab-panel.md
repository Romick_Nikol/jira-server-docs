---
aliases:
- /server/jira/platform/issue-tab-panel-plugin-module-4227190.html
- /server/jira/platform/issue-tab-panel-plugin-module-4227190.md
category: reference
confluence_id: 4227190
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=4227190
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=4227190
date: '2018-05-30'
legacy_title: Issue Tab Panel Plugin Module
platform: server
product: jira
subcategory: modules
title: "Issue tab panel"
---
# Issue tab panel

<table>
<colgroup>
<col style="width: 20%" />
<col style="width: 80%" />
</colgroup>
<tbody>
<tr class="odd">
<td><p>Available:</p></td>
<td><p>Jira 3.0 and later.</p></td>
</tr>
<tr class="even">
<td><p>Changed:</p></td>
<td><p>Jira 5.0 – added the <code>&lt;supports-ajax-load&gt;</code> configuration element.</p></td>
</tr>
<tr class="odd">
<td><p> </p></td>
<td><p>Jira 5.0 – added the <a href="https://docs.atlassian.com/software/jira/docs/api/latest/com/atlassian/jira/plugin/issuetabpanel/IssueTabPanel2.html" class="external-link">IssueTabPanel2 API</a>.</p></td>
</tr>
<tr class="even">
<td><p> </p></td>
<td><p>Jira 6.0 – added the <a href="https://docs.atlassian.com/software/jira/docs/api/latest/com/atlassian/jira/plugin/issuetabpanel/IssueTabPanel3.html" class="external-link">IssueTabPanel3 API</a>.</p></td>
</tr>
</tbody>
</table>

The issue tab panel plugin module allows you to add new tab panels to the View Issue screen.

<img src="/server/jira/platform/images/jira-7-7-issuetabs.png" class="image-center" />

You can add a new tab with a plugin displaying information about a single issue
(most likely pulled from an external source).

Here is an example descriptor:

``` xml
<issue-tabpanel key="custom-issue-tabpanel" name="Custom Tab Panel" class="com.atlassian.plugins.tutorial.IssueTabCustom">
    <description>Show a custom panel.</description>
    <label>Custom panel</label>
    <supports-ajax-load>true</supports-ajax-load>
</issue-tabpanel>
```

For more information about the `atlassian-plugin.xml` file, see the
 [Configuring the app descriptor](/server/framework/atlassian-sdk/configuring-the-plugin-descriptor/) page.

Here is a simple Java implementation:

``` java
public class IssueTabCustom extends AbstractIssueTabPanel3 {
    @Override
    public boolean showPanel(ShowPanelRequest showPanelRequest) {
        return true;
    }

    @Override
    public List<IssueAction> getActions(GetActionsRequest getActionsRequest) {
        return Lists.newArrayList(new GenericMessageAction("first"), new GenericMessageAction(
                    this.descriptor.getI18nBean().getText("com.atlassian.plugins.tutorial.custom.issue.tab.panel.example")));
    }
}
```

The module class specified in the `class="..."` attribute must implement the [IssueTabPanel3](https://docs.atlassian.com/software/jira/docs/api/latest/com/atlassian/jira/plugin/issuetabpanel/IssueTabPanel3.html)
interface.

To customize look of your items, implement your own [IssueAction](https://docs.atlassian.com/software/jira/docs/api/latest/com/atlassian/jira/plugin/issuetabpanel/IssueAction.html).

For more details, see [Loading Issue Tab Panels with AJAX](/server/jira/platform/loading-issue-tab-panels-with-ajax).
