---
aliases:
- /server/jira/platform/how-to-create-a-new-custom-field-type-b-22512099.html
- /server/jira/platform/how-to-create-a-new-custom-field-type-b-22512099.md
category: devguide
confluence_id: 22512099
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=22512099
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=22512099
date: '2017-12-08'
legacy_title: How to create a new custom field type - B
platform: server
product: jira
subcategory: other
title: How to create a new custom field type - B
---
# How to create a new custom field type - B

|                       |           |
|-----------------------|-----------|
| Level of experience   | BEGINNER  |
| Time estimate         | 0:30      |
| Atlassian application | JIRA 4.X+ |

## Tutorial overview

In this tutorial, you create a plugin that adds a custom field type to JIRA issues. Your new field type is only editable by JIRA admins, and can be viewed by all users. You'll implement your new type as a field in JIRA. Your new field will appear as follows: 

<img src="/server/jira/platform/images/admin-create-issue.png" width="500" />

When you create this custom field plugin, you'll learn: 

-   What kinds of custom field types are available in JIRA.
-   How to extend the GenericTextCFT custom field type.
-   How to use the `customfield-type` plugin module type for JIRA.

**Prerequisite knowledge**

You need to have installed the Atlassian Plugin SDK to complete this tutorial. You should be able to finish this tutorial if you haven't created a plugin before. Familiarity with Java development and JIRA is also helpful. 

**Getting the plugin source code**

We encourage you to work through the tutorial. If you'd like to skip ahead or check your work when you're done, you can find the plugin source on Atlassian Bitbucket. Bitbucket serves as a public Git repository containing the tutorial's source code. To clone the repository for this plugin, issue the following command: 

    git clone https://rbuchanan@bitbucket.org/atlassian_tutorial/jira-custom-field-tutorial.git

If you don't have Git, you can <a href="https://bitbucket.org/atlassian_tutorial/jira-custom-field-tutorial/get/master.zip" class="external-link">download the source code</a> for this plugin project.

{{% note %}}

About these Instructions

You can use any supported combination of OS and IDE to create this plugin. These instructions were written using Eclipse Indigo on Mac OS X. If you are using another OS or IDE combination, you should use the equivalent operations for your specific environment.

This tutorial was last tested with **JIRA 6.1.2.** 

{{% /note %}}

## Step 1. Create the plugin project and trim the skeleton

In this step you'll generate a JIRA plugin skeleton using the `atlas-create-jira-plugin` command from the SDK. This command generates a directory with skeleton files, and holds your plugin. Some skeleton files aren't necessary for this tutorial, so you'll delete them in this step. 

1.  Open a terminal window on your local machine.   
      
2.  Change to your Eclipse (or equivalent IDE) workspace directory. 

        cd ~/atlastutorial/

3.  Create a JIRA plugin skeleton: 

        atlas-create-jira-plugin

4.  When prompted, enter the following information to identify your plugin: 

    <table>
    <colgroup>
    <col style="width: 50%" />
    <col style="width: 50%" />
    </colgroup>
    <tbody>
    <tr class="odd">
    <td><p>group-id</p></td>
    <td><pre><code>com.atlassian.jira.plugin.customfield.example</code></pre></td>
    </tr>
    <tr class="even">
    <td><p>artifact-id</p></td>
    <td><p>jira-custom-field-example</p></td>
    </tr>
    <tr class="odd">
    <td><p>version</p></td>
    <td><p><code>1.0-SNAPSHOT</code></p></td>
    </tr>
    <tr class="even">
    <td><p>package</p></td>
    <td><pre><code>com.atlassian.jira.plugin.customfield.example</code></pre></td>
    </tr>
    </tbody>
    </table>

5.  Confirm your entries when prompted with `Y` or `y`.

    The command builds the plugin skeleton and notifies you of a successful build:

        [INFO] ------------------------------------------------------------------------
        [INFO] BUILD SUCCESSFUL
        [INFO] ------------------------------------------------------------------------
        [INFO] Total time: 1 minute 11 seconds
        [INFO] Finished at: Thu Aug 28 11:30:23 PDT 2013
        [INFO] Final Memory: 82M/217M
        [INFO] ------------------------------------------------------------------------

6.  Change to the `jira-custom-field-example` directory created by the previous step.

        cd jira-custom-field-example

    This directory contains several test directories you don't use in the tutorial.

7.  Delete the test directories.

    <table>
    <colgroup>
    <col style="width: 50%" />
    <col style="width: 50%" />
    </colgroup>
    <tbody>
    <tr class="odd">
    <td>Linux/Mac OS X instructions</td>
    <td><div class="preformatted panel" style="border-width: 1px;">
    <div class="preformattedContent panelContent">
    <pre><code>rm -rf ./src/test/java
    rm -rf ./src/test/resources/</code></pre>
    </div>
    </div></td>
    </tr>
    <tr class="even">
    <td>Windows</td>
    <td><div class="preformatted panel" style="border-width: 1px;">
    <div class="preformattedContent panelContent">
    <pre><code>rmdir ./src/test/java
    rmdir ./src/test/resources/</code></pre>
    </div>
    </div></td>
    </tr>
    </tbody>
    </table>

    The generation step also created Java class files you don't need. 

8.  Delete the unnecessary Java files. 

    <table>
    <colgroup>
    <col style="width: 50%" />
    <col style="width: 50%" />
    </colgroup>
    <tbody>
    <tr class="odd">
    <td>Linux/Mac OS X</td>
    <td><div class="preformatted panel" style="border-width: 1px;">
    <div class="preformattedContent panelContent">
    <pre><code>rm ./src/main/java/com/atlassian/jira/plugin/customfield/example/*.java</code></pre>
    </div>
    </div></td>
    </tr>
    <tr class="even">
    <td>Windows</td>
    <td><div class="preformatted panel" style="border-width: 1px;">
    <div class="preformattedContent panelContent">
    <pre><code>rmdir ./src/main/java/com/atlassian/jira/plugin/customfield/example/*.java</code></pre>
    </div>
    </div></td>
    </tr>
    </tbody>
    </table>

    You'll build a Java class for your plugin in future steps.

     

9.  Edit the `src/main/resources/atlassian-plugin.xml` descriptor file.  
    This file contains some unneeded declarations.  
      
10. Locate the generated `myPluginComponent` declaration from `atlassian-plugin.xml` file:

    ``` javascript
        <!-- publish our component -->
        <component key="myPluginComponent" class="com.atlassian.jira.plugin.customfield.example.MyPluginComponentImpl" public="true">
            <interface>com.atlassian.jira.plugin.customfield.example.MyPluginComponent</interface>
        </component>
    ```

11. Delete the declaration.  
      
12. Check your work.

    Here's how your `atlassian-plugin.xml` descriptor file should appear up to this point:

    ``` javascript
    <atlassian-plugin key="${project.groupId}.${project.artifactId}" name="${project.name}" plugins-version="2">
        <plugin-info>
            <description>${project.description}</description>
            <version>${project.version}</version>
            <vendor name="${project.organization.name}" url="${project.organization.url}" />
            <param name="plugin-icon">images/pluginIcon.png</param>
            <param name="plugin-logo">images/pluginLogo.png</param>
        </plugin-info>
        <!-- add our i18n resource -->
        <resource type="i18n" name="i18n" location="jira-custom-field-example"/>
        
        <!-- add our web resources -->
        <web-resource key="jira-custom-field-example-resources" name="jira-custom-field-example Web Resources">
            <dependency>com.atlassian.auiplugin:ajs</dependency>
            
            <resource type="download" name="jira-custom-field-example.css" location="/css/jira-custom-field-example.css"/>
            <resource type="download" name="jira-custom-field-example.js" location="/js/jira-custom-field-example.js"/>
            <resource type="download" name="images/" location="/images"/>
            <context>jira-custom-field-example</context>
        </web-resource>
        
        <!-- import from the product container -->
        <component-import key="applicationProperties" interface="com.atlassian.sal.api.ApplicationProperties" />
        
    </atlassian-plugin>
    ```

13. Save and close the file.  
      

### Optional: Import your project into your IDE

 

1.   Make your project available to Eclipse.

        atlas-mvn eclipse:eclipse

    You'll see a successful build message: 

        [INFO] ------------------------------------------------------------------------
        [INFO] BUILD SUCCESSFUL
        [INFO] ------------------------------------------------------------------------
        [INFO] Total time: 54 seconds
        [INFO] Finished at: Tue Jul 16 11:03:59 PDT 2013
        [INFO] Final Memory: 82M/224M
        [INFO] ------------------------------------------------------------------------

2.  Start Eclipse.  
    You can open a new terminal window to perform this action.

        $ cd ~/eclipse
        $ ./eclipse

3.  Click **File &gt; Import**.   
    Eclipse starts the **Import** wizard.  
      
4.  Expand the **General** folder tree to choose **Existing Projects into Workspace**.  
      
5.  Click **Next. **  
    **  
    **
6.  Click **Browse **and enter the root directory of your workspace.  
    Your Atlassian plugin folder should now appear under **Projects.**  
      
7.  Ensure your plugin checkbox is ticked and click **Finish**.   
    Eclipse imports your project, and it's now visible in your Package Explorer view.

 

## Step 2. Create a Java class for your custom field

You'll write a Java class to process and manage your custom field data. Custom fields in JIRA inherit their properties from the `CustomFieldType` interface. This interface provides methods to store and retrieve custom field values from the database.

There are multiple implementations of `CustomFieldType` from Atlassian. For example, there is the `MultiSelectCFType`, `DateTimeCFType`, and `NumberCFType` among others. For a complete list of implementations, you can see the <a href="http://docs.atlassian.com/software/jira/docs/api/latest/com/atlassian/jira/issue/customfields/CustomFieldType.html" class="external-link">CustomFieldType</a> interface Javadoc. You'll extend the `GenericTextCFType` class to add an admin-editable text field.

On your local machine, do the following: 

1.  Create a class called `JiraCustomField.java` in the `src/main/java/com/atlassian/jira/plugin/customfield/example/` directory.  
      
2.  Edit the class file in Eclipse or your favorite editor.  
      
3.  Add the following code to your class: 

    ``` javascript
    package com.atlassian.jira.plugin.customfield.example;

    import com.atlassian.jira.issue.customfields.impl.GenericTextCFType;
    import com.atlassian.jira.issue.customfields.manager.GenericConfigManager;
    import com.atlassian.jira.issue.customfields.persistence.CustomFieldValuePersister;


    public class JiraCustomField extends GenericTextCFType {

            public JiraCustomField(CustomFieldValuePersister customFieldValuePersister,
                            GenericConfigManager genericConfigManager) {

                    super(customFieldValuePersister, genericConfigManager);
            }


    }
    ```

    ![(info)](/server/jira/platform/images/icons/emoticons/information.png) Copying and pasting from Confluence (which hosts this tutorial) can have unintended results. If you prefer to copy Java code for this class, use [this file](https://dac-lf.prod.atl-paas.net/server/jira/platform/attachments/22512099/24215644.java) instead.

4.  Save and close `JiraCustomField.java`.  
      
5.  Update your project changes if using an IDE.

        atlas-mvn eclipse:eclipse

## Step 3. Create a Velocity template to manage your custom field

You'll use Velocity templates to define how your custom field appears in the JIRA user interface. You'll design your custom field so that only administrators can edit your custom field. In this step, you'll code your Velocity template so that: 

-   All users are allowed to see the field's value.
-   The field displays `N/A` as the default value.
-   Members of the **jira-administrators** group are able to edit the field.

JIRA custom fields use four view modes, all controlled by Velocity templates native to JIRA source code: 

<table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<thead>
<tr class="header">
<th><div class="tablesorter-header-inner">
View type
</div></th>
<th><div class="tablesorter-header-inner">
Description
</div></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>view</td>
<td>Provides a basic read-only view of the field value. The system default is the <code>view-basictext.vm</code> file.</td>
</tr>
<tr class="even">
<td>column view</td>
<td>Read-only view for displaying in the issue navigator. The issue navigator displays the view template if you omit this template. This template is optional.</td>
</tr>
<tr class="odd">
<td>edit</td>
<td>Renders the edit widget in issue creation, editing issues, and editing defaults. The system default is the <code>edit-basictext.vm</code> file.</td>
</tr>
<tr class="even">
<td>XML</td>
<td>Shows the value in XML format for RSS or XML exports.The system default is the <code>xml-basictext.vm</code> file.</td>
</tr>
</tbody>
</table>

All view templates are required except for the column view. You can create templates for the view types, or use JIRA-native default templates. Since you're going to specify that only administrators can edit the field, you'll encapsulate this logic in a Velocity template for the edit view.

From your terminal window, do the following: 

1.  Create a `templates` directory under `src/main/resources`.

        mkdir src/main/resources/templates

2.  Create a file called `edit-jiraadminonlytext.vm `under your new `templates` directory. 

    You can also do this from the command line.

        cat > src/main/resources/templates/edit-jiraadminonlytext.vm

3.  Add the following text into the `edit-jiraadminonlytext.vm` file.

    ``` javascript
    #controlHeader ($action $customField.id $customField.name $fieldLayoutItem.required $displayParameters.noHeader)
    #if ($jiraUserUtils.getGroupNamesForUser($authcontext.loggedInUser.name).contains('jira-administrators'))
    <input type="text"
    name="$customField.id"
    value="$!value" />
    #else
    #if($value && ! $value.equals(""))
    #set ($displayValue = ${value})
    #else
    #set ($displayValue = 'N/A')
    #end
    <span title="This field is editable only by JIRA administrators">$!displayValue</span>
    <input type="hidden"
    name="$customField.id"
    value="$!value" />
    #end
    #controlFooter ($action $fieldLayoutItem.fieldDescription $displayParameters.noHeader)
    ```

    This code wraps the input field in an `if/else` block. Users part of the jira-administrators group can input text, while those outside the group have editing permissions removed.

4.  Save and close the `edit-jiraadminonlytext.vm` file.  
      

## Step 4. Add a `customfield-type` plugin module to your descriptor file

In this step you'll add a `customfield-type` plugin module to the `atlassian-plugin.xml` descriptor file. This file is known as the descriptor file because it describes a plugin to the Atlassian system. Your custom field is available in JIRA after you add the module to the descriptor file.

The descriptor file also allows your plugin to leverage host application resources. In this tutorial, you'll use JIRA-native default templates for the view and XML view types, and your custom Velocity template for the edit view type. You'll specify these templates accordingly in the `customfield-type` plugin module.

1.  Edit `atlassian-plugin.xml` under `src/main/resources`.  
      
2.  Locate the closing `</atlassian-plugin>` tag.   
      
3.  Add the opening and closing tags for your `customfield-type.`

    Place the block before the end of the closing `</atlassian-plugin>` tag. You'll give your custom field a unique key, name, and define `JiraCustomField` as the implementing Java class.

    ``` javascript
       <customfield-type key="admintextfield" name="Admin Editable Text Field" class="com.atlassian.jira.plugin.customfield.example.JiraCustomField">
        </customfield-type>
    ```

4.  Add a description inside the first `customfield-type` tag. 

    ``` javascript
           <description>A text field only editable by those with admin permissions</description>
    ```

    This description appears in JIRA when you configure it to use your field. 

      

5.  Define resources for each required view type inside your module.

    The first resource you'll add references your own Velocity template: 

    ``` javascript
           <resource type="velocity" name="edit" location="templates/edit-jiraadminonlytext.vm"/>
    ```

     Next, recall that JIRA has native Velocity templates to support fields. Use the following file paths to reference JIRA-native templates for view and edit.

    ``` javascript
           <resource type="velocity" name="view" location="templates/plugins/fields/view/view-basictext.vm"/>
            <resource type="velocity" name="xml" location="templates/plugins/fields/xml/xml-basictext.vm"/>
    ```

      

6.  Check your work.  

    Here's the completed `atlassian-plugin.xml` descriptor file:

    ``` javascript
    <atlassian-plugin key="${project.groupId}.${project.artifactId}" name="${project.name}" plugins-version="2">
        <plugin-info>
            <description>${project.description}</description>
            <version>${project.version}</version>
            <vendor name="${project.organization.name}" url="${project.organization.url}" />
            <param name="plugin-icon">images/pluginIcon.png</param>
            <param name="plugin-logo">images/pluginLogo.png</param>
        </plugin-info>
        <!-- add our i18n resource -->
        <resource type="i18n" name="i18n" location="jira-custom-field-example"/>
        
        <!-- add our web resources -->
        <web-resource key="jira-custom-field-example-resources" name="jira-custom-field-example Web Resources">
            <dependency>com.atlassian.auiplugin:ajs</dependency>
            
            <resource type="download" name="jira-custom-field-example.css" location="/css/jira-custom-field-example.css"/>
            <resource type="download" name="jira-custom-field-example.js" location="/js/jira-custom-field-example.js"/>
            <resource type="download" name="images/" location="/images"/>
            <context>jira-custom-field-example</context>
        </web-resource>
        
        <!-- import from the product container -->
        <component-import key="applicationProperties" interface="com.atlassian.sal.api.ApplicationProperties" />
       
        <customfield-type key="admintextfield" name="Admin Editable Text Field" class="com.atlassian.jira.plugin.customfield.example.JiraCustomField">
                <description>A text field only editable by those with admin permissions</description>
                <resource type="velocity" name="view" location="templates/plugins/fields/view/view-basictext.vm"/>
                <resource type="velocity" name="edit" location="templates/edit-jiraadminonlytext.vm"/>
                <resource type="velocity" name="xml" location="templates/plugins/fields/xml/xml-basictext.vm"/>
        </customfield-type>
        
    </atlassian-plugin>
    ```

7.  Save and close your descriptor file.  
8.  Update your project changes if using an IDE.

        atlas-mvn eclipse:eclipse

## Step 5. Build, install, and run your plugin

In this step you'll use the command `atlas-run` to build your plugin and launch a JIRA instance. After the you launch JIRA, you'll log in as an admin. 

1.  Open a terminal and navigate to your project root. 

        cd jira-custom-field-example

2.  Start JIRA. 

        atlas-run

    This builds your plugin code, starts a JIRA instance, and installs your plugin. This command goes out to the Maven repository and verifies you have the latest Atlassian software components, which can take a few minutes. Your command line outputs the URL of your local JIRA instance at completion. 

3.  Locate the URL for JIRA.   
    JIRA instances default to <a href="http://localhost:2990/jira." class="external-link">http://localhost:2990/jira</a>

        [INFO] jira started successfully in 194s at http://localhost:2990/jira
        [INFO] Type Ctrl-D to shutdown gracefully
        [INFO] Type Ctrl-C to exit

4.  Navigate to your JIRA instance in a browser window.  
    We recommend using Google Chrome or Mozilla Firefox for consistency.  
      
5.  Log in with **admin / admin**.  
      
6.  If a pop-up window appears to **Select project type**, click **Cancel**.

## Step 6. Configure JIRA to use your custom field type

Even though your custom field is installed in JIRA, you need to configure it. First you'll add your custom field type, and then a custom field to make use of the type. You can configure many custom fields to use one custom field type - there are many use cases where having an admin-only editable field is appropriate, so you can reuse your field type in multiple fields. Here, you'll create one custom field and configure the JIRA instance to use it globally.

You should have your local JIRA instance running, and be logged in as an admin.

1.  Choose ![](/server/jira/platform/images/image2013-8-26-12:48:49.png) &gt; **Issues.**  
    **  
    **
2.  Choose **Custom Fields** from the left-hand navigation.  
      
3.  Click **Add Custom Field.**  
    The **Select a field type** dialog appears.  
    ** **
4.  Choose **Advanced**.  
      
5.  Choose your custom field, **Admin Editable Text Field** from the list and click **Next**.  
    <img src="/server/jira/platform/images/choose-your-plugin.png" title="Choose your plugin and click next" alt="Choose your plugin and click next" width="500" />  
    The **Create Custom Field - Details **page displays.
6.  Add the values for your custom field as follows. 

    <table>
    <colgroup>
    <col style="width: 50%" />
    <col style="width: 50%" />
    </colgroup>
    <thead>
    <tr class="header">
    <th>Field</th>
    <th>Value</th>
    </tr>
    </thead>
    <tbody>
    <tr class="odd">
    <td><strong>Field Name</strong></td>
    <td><pre><code>Special admins only</code></pre></td>
    </tr>
    <tr class="even">
    <td><strong>Description</strong></td>
    <td><pre><code>A custom text field created for a tutorial.</code></pre></td>
    </tr>
    <tr class="odd">
    <td><strong>Issue Types</strong></td>
    <td><pre><code>Any issue type</code></pre></td>
    </tr>
    <tr class="even">
    <td><strong>Context</strong></td>
    <td><pre><code>Global context</code></pre></td>
    </tr>
    </tbody>
    </table>

      
    When you are finished, the page looks similar to this:   
    <img src="/server/jira/platform/images/enter-field-name-details.png" title="Add custom field details" alt="Add custom field details" width="500" />  
    **  
    **

7.  Click **Finish. **  
    **The Associate field Special admins only to screens** page appears.  
      
8.  Tick the checkboxes for all three screens:  
    <img src="/server/jira/platform/images/associate-fields.png" title="Associate all fields and click next" alt="Associate all fields and click next" width="500" />
9.  Click **Update** to complete the custom field addition.

 

## Step 7. Add a project and a test user

You can test the field as an admin, but not as a regular user. You need to create a user without admin permissions so you can test your field. When you have both user types configured, you can test the logic in your edit Velocity template.

1.  From any page in your local JIRA instance, click  ![](/server/jira/platform/images/image2013-8-26-12:48:49.png) &gt; **User Management.**  
    **  
    **
2.  Click **Create User.**  
    The system shows you the **Create New User** dialog. 
3.  Complete the fields for the user dialog:

    <table>
    <colgroup>
    <col style="width: 50%" />
    <col style="width: 50%" />
    </colgroup>
    <thead>
    <tr class="header">
    <th>Field</th>
    <th>Value</th>
    </tr>
    </thead>
    <tbody>
    <tr class="odd">
    <td><strong>Username</strong></td>
    <td><pre><code>testuser</code></pre></td>
    </tr>
    <tr class="even">
    <td><strong>Password</strong></td>
    <td><pre><code>testuser</code></pre></td>
    </tr>
    <tr class="odd">
    <td><strong>Confirm</strong></td>
    <td><pre><code>testuser</code></pre></td>
    </tr>
    <tr class="even">
    <td><strong>Full Name</strong></td>
    <td><pre><code>Not an Admin</code></pre></td>
    </tr>
    <tr class="odd">
    <td><strong>Email</strong></td>
    <td>Enter any valid email. It is only stored until you clean out your plugin directory.</td>
    </tr>
    </tbody>
    </table>

    The dialog should appear as follows, with the addition of your email address: 

    <img src="/server/jira/platform/images/create-a-test-user.png" title="create a test user and click next" alt="create a test user and click next" width="500" />

4.  Click **Create**.  
      
5.  Next, create a project.  
    Click **Projects &gt; Create Project**.  
    Now you'll be able to create an issue to test your field.  
      
6.  Name your project as you like.  
    The key is automatically generated. For example, if you name your project "Test," your key will be TEST.  
      
7.  Click **Submit.**  
      

## Step 8. Test your custom field by creating an issue

Now you're ready to test your plugin by creating an issue. You'll create an issue first as an admin, and if you've followed the tutorial to this point, you'll be able to enter a value in your Special admins only field. Finally, you'll log in as your test user to verify you can see the field, but not edit it. 

While still logged in as an admin: 

1.  Click **Create Issue.**  
    The **Create Issue** dialog appears.** **  
    **  
    **
2.  Enter the required **Summary** field.** **  
    **  
    **
3.  Scroll to the bottom of the **Create Issue** form.   
    You should see your custom field labeled Special admins only.  
      
4.  Enter a simple string for your custom field value.  
    <img src="/server/jira/platform/images/admin-create-issue.png?effects=drop-shadow" title="Create an issue as an admin" alt="Create an issue as an admin" width="500" />
5.  Click **Create** to save your issue.  
      
6.  Choose **Search for issues**.  
      
7.  Locate the issue you just created.  
    Here, you'll check if the JIRA-native view template works correctly.   
      
8.  Verify you can see your field.  
      
9.  Log out.  
      
10. Login with your `testuser` credentials.  
      
11. Locate the issue you created under the admin account.  
      
12. Click **Edit** to modify the issue.  
      
13. Verify you're unable to make changes to your custom field. You should see the value you entered, but be unable to change it:   
    <img src="/server/jira/platform/images/create-issue-non-admin.png" title="create an issue as a non-admin" alt="create an issue as a non-admin" width="500" />

