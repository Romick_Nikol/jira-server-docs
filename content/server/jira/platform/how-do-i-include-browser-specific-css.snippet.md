---
aliases:
- /server/jira/platform/how-do-i-include-browser-specific-css-4227119.html
- /server/jira/platform/how-do-i-include-browser-specific-css-4227119.md
category: devguide
confluence_id: 4227119
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=4227119
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=4227119
date: '2017-12-08'
legacy_title: How do I include browser-specific CSS
platform: server
product: jira
subcategory: other
title: How do I include browser-specific CSS
---
# How do I include browser-specific CSS

JIRA 4.3 and later adds classes to the HTML tag on page load. This means that you can now put all browser-specific CSS fixes in the main stylesheet near related styles. Having styles in one file increases maintainability, and reduces the number of requests -- which helps pages load faster.

Prior to 4.3 you would put IE styles in the IE stylesheet and prefix with \* or \_ depending on the browser. In JIRA 4.3 and later you can use the following:

-   .msie (all versions of IE)
-   .msie-7 (just IE7)
-   .msie-8 (just IE8)
-   .msie-gt-7 (IE8 and IE9)
-   .msie-lt-8 (IE7, IE6, IE5.5, etc)
-   .mozilla (all versions of Firefox. There are no version-specific options like IE)
-   .webkit (all versions of Chrome/Safari. There are no version-specific options like IE)
-   .opera (note: not a supported browser)
