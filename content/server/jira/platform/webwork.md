---
aliases:
- /server/jira/platform/webwork-plugin-module-4227203.html
- /server/jira/platform/webwork-plugin-module-4227203.md
category: reference
confluence_id: 4227203
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=4227203
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=4227203
date: '2017-12-08'
legacy_title: Webwork plugin module
platform: server
product: jira
subcategory: modules
title: Webwork
---
# Webwork

## Purpose of this Module Type

A webwork plugin module defines a URL-addressible 'action', allowing JIRA's user-visible functionality to be extended or partially overridden.

## Configuration

The root element for the WebWork plugin module is `webwork1`. It allows the following attributes and child elements for configuration:

#### Attributes

<table>
<colgroup>
<col style="width: 20%" />
<col style="width: 80%" />
</colgroup>
<thead>
<tr class="header">
<th><p>Name</p></th>
<th><p>Description</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>class</p></td>
<td><p>The class which implements this plugin module. The class you need to provide depends on the module type. For example, Confluence theme, layout and colour-scheme modules can use classes already provided in Confluence. So you can write a theme-plugin without any Java code. But for macro and listener modules you need to write your own implementing class and include it in your plugin. See the plugin framework guide to <a href="https://developer.atlassian.com/display/DOCS/Creating+Plugin+Module+Instances">creating plugin module instances</a>. The Java class of the module. For this module, it's fine to use <code>Object</code>, as the real brains are in the action classes below.</p>
<p><strong>Required: yes</strong></p>
<p><strong>Default: -</strong></p></td>
</tr>
<tr class="even">
<td><p>key</p></td>
<td><p>The unique identifier of the plugin module. You refer to this key to use the resource from other contexts in your plugin, such as from the plugin Java code or JavaScript resources.</p>
<pre><code>&lt;component-import key=&quot;appProps&quot;
interface=&quot;com.atlassian.sal.api.ApplicationProperties&quot;/&gt;</code></pre>
<p>In the example, <code>appProps</code> is the key for this particular module declaration, for <code>component-import</code>, in this case. I.e. the identifier for this module.</p>
<p><strong>Required: yes</strong></p>
<p><strong>Default: N/A</strong></p></td>
</tr>
<tr class="odd">
<td><p>i18n-name-key</p></td>
<td><p>The localisation key for the human-readable name of the plugin module.</p>
<p><strong>Required: -</strong></p>
<p><strong>Default: -</strong></p></td>
</tr>
<tr class="even">
<td><p>name</p></td>
<td><p>The human-readable name of the plugin module. I.e. the human-readable name of this module. This must be unique across all JIRA add-ons.</p>
<p><strong>Required: -</strong></p>
<p><strong>Default:</strong> The plugin key.</p></td>
</tr>
<tr class="odd">
<td>roles-required</td>
<td><p>The roles-required attribute can be used to only allow access to certain web actions to users which have been granted certain permissions. See <a href="#notes">Notes</a> for more information.</p>
<p><strong>Required: -</strong></p>
<p><strong>Default: -</strong></p></td>
</tr>
</tbody>
</table>

#### Elements

<table>
<colgroup>
<col style="width: 20%" />
<col style="width: 80%" />
</colgroup>
<thead>
<tr class="header">
<th><p>Name</p></th>
<th><p>Description</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>description</p></td>
<td><p>A human-readable description of this WebWork module. May be specified as the value of this element for plain text or with the <code>key</code> attribute to use the value of a key from the i18n system.</p>
<p><strong>Required: -</strong></p></td>
</tr>
<tr class="even">
<td><p>actions</p></td>
<td><p>Specifies WebWork 1 &lt;action&gt;s to define. Must contain at least one &lt;action&gt; element.</p>
<p><strong>Required: yes</strong></p></td>
</tr>
</tbody>
</table>

#### `<action>` Element Attributes

<table>
<colgroup>
<col style="width: 20%" />
<col style="width: 80%" />
</colgroup>
<thead>
<tr class="header">
<th><p>Name</p></th>
<th><p>Description</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>name</p></td>
<td><p>Full name of the class that implements the WebWork action. Actions in JIRA must extend the class <code>com.atlassian.jira.web.action.JiraWebActionSupport</code>. The class <strong>must not</strong> live in a package that JIRA has already reserved; authors should avoid the <code>com.atlassian</code> namespace altogether. The simple name of the class must be unique across all JIRA add-ons. That is, if one add-on has an action class <code>MyEditAction</code> then no other add-on should have an action class <code>MyEditAction</code>. It is recommended that each add-on use a prefix to make their action classes unique.</p>
<p><strong>Required: yes</strong></p></td>
</tr>
<tr class="even">
<td><p>alias</p></td>
<td><p>The path from which this action may be invoked in JIRA. For example, an action with alias <code>MyNewJiraAction</code> would be invoked by the URL <code>http://&lt;my-jira-server&gt;/secure/MyNewJiraAction.jspa</code>.</p>
<p><strong>Required: yes</strong></p></td>
</tr>
<tr class="odd">
<td>roles-required</td>
<td><p>The roles-required attribute can be used to only allow access to certain web actions to users which have been granted certain permissions. See <a href="#notes">Notes</a> for more information.</p>
<p><strong>Required: -</strong></p></td>
</tr>
</tbody>
</table>

#### `<action>` Element Elements

<table>
<colgroup>
<col style="width: 20%" />
<col style="width: 80%" />
</colgroup>
<thead>
<tr class="header">
<th><p>Name</p></th>
<th><p>Description</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>view</p></td>
<td><p>Directs where to send the user when the action completes. The <code>name</code> attribute maps to the return value from the overridden action methods (see the WebWork documentation for more details; common values are <code>error</code>, <code>input</code>, and <code>success</code>). The element's value is the path to the renderable view that is sent to the user (see <a href="#notes">Notes</a> for more information).</p>
<p><strong>Required: -</strong></p></td>
</tr>
</tbody>
</table>

## Example

Here is a sample webwork plugin module that is placed in [atlassian-plugin.xml](/server/framework/atlassian-sdk/configuring-the-plugin-descriptor/):

``` xml
<webwork1 key="qquserissue" name="Quick Create User Issue" class="java.lang.Object">
    <actions>
        <action name="com.atlassian.jira.toolkit.action.QuickCreateUserIssueAction" alias="QuickCreateUserIssue">
            <view name="createuserissue">/templates/quickcreateuser.vm</view>
        </action>
    </actions>
</webwork1>
```

Webwork plugins effectively extend the actions defined in the JIRA `WEB-INF/classes/actions.xml` file. You should look there for examples of what is possible. There is also a <a href="https://bitbucket.org/mdoar/webwork-sample" class="external-link">Webwork Sample plugin</a> that contains many other basic examples.

{{% warning %}}

Use your own package for your action classes!

In the past, plugin authors could rely on a bit of magic: putting their action class in the package `com.atlassian.jira.web.action` was enough to have JIRA find it without specifying the fully qualified class name in `<action name="">`. This was never a good idea, and in a Plugins2 plugin, it will simply not work. Always create a separate package space for your code and stay out of the `com.atlassian` namespace.

{{% /warning %}}

{{% warning %}}

Make sure your template names are unique!

Your views are placed in a common pool, and JIRA simply retrieves the first template that fully matches the path, so if there are two plugins with templates /templates/admin.vm then one of them will be hidden by the other.

{{% /warning %}}

{{% warning %}}

Avoid complex inheritance!

You can override existing actions without worry, but you cannot override an already overridden action. JIRA's WebWork implementation isn't smart enough to resolve polymorphic action hierarchies.

{{% /warning %}}

## Sample Code

-   The JIRA sample plugin shows how to display the project leads in each project Administrator's page
-   The <a href="https://bitbucket.org/mdoar/webwork-sample" class="external-link">Webwork Sample plugin</a> contains many simple examples of using Webwork actions

## Notes

-   ### **Renderable Views**

    The value of `<view>` should be a <a href="http://velocity.apache.org" class="external-link">Velocity</a> template; in the above example, the template `templates/quickcreateuser.vm` lives in the plugin artifact under that path. JSP views cannot be used from inside plugins; they can be used if they are installed into the JIRA webapp, but this complicates installation, upgrading, and troubleshooting. Use Velocity if you can.

<!-- -->

-   ### **Roles Required**

    The roles-required attribute can be used to only allow access to certain web actions to users which have been granted certain permissions. The permissions are the short names of com.atlassian.jira.security.Permissions based permissions, and will only work for global based permissions. The three that are most useful are "admin", "sysadmin", and "use".

    You can add a roles-required attribute to the parent webwork element, or to a child action element (or both!).

    ``` xml
    <webwork1 key="reference-actions" name="Reference WebWork Action" class="java.lang.Object" roles-required="use"> <actions> <action name="com.atlassian.jira.dev.reference.plugin.actions.PreparedReferenceAction" alias="PreparedReferenceAction" roles-required="sysadmin"> <view name="success">templates/actions/prepared-reference-action.vm</view> </action> <action name="com.atlassian.jira.dev.reference.plugin.actions.ReferenceAction" alias="ReferenceAction"> <view name="success">templates/actions/reference-action.vm</view> </action> </actions> </webwork1>
    ```
