---
aliases:
- /server/jira/platform/4227164.html
- /server/jira/platform/4227164.md
category: devguide
confluence_id: 4227164
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=4227164
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=4227164
date: '2017-12-08'
legacy_title: Why am I getting 'Exception looking up public key' exception while building
  a plugin?
platform: server
product: jira
subcategory: other
title: Why am I getting 'Exception looking up public key' exception while building
  a plugin?
---
# Why am I getting 'Exception looking up public key' exception while building a plugin?

### <img src="http://confluence.atlassian.com/images/icons/favicon.png" class="confluence-external-resource" /> Why am I getting 'Exception looking up public key' exception while building a plugin?

Some plugins such as the RPC Plugin execute unit-testing while 'build' (e.g. `maven jar`) is under progress. As you may not require it sometimes, such error message may turn out to be unpleasant:

    2006-11-02 16:41:31,375 ERROR [atlassian.license.decoder.LicenseDecoder] Exception looking up public key: null
    java.lang.NullPointerException
    at com.atlassian.license.decoder.LicenseDecoder.getPublicKey(LicenseDecoder.java:51)
    at com.atlassian.license.decoder.LicenseDecoder.getLicense(LicenseDecoder.java:26)
    at com.atlassian.license.LicenseManager.setLicense(LicenseManager.java:127)
    at com.atlassian.jira.rpc.soap.service.TestProjectServiceImpl.setUp(TestProjectServiceImpl.java:108)
    at org.jmock.core.VerifyingTestCase.runBare(Unknown Source)
    at junit.framework.TestResult$1.protect(TestResult.java:106)
    at junit.framework.TestResult.runProtected(TestResult.java:124)
    at junit.framework.TestResult.run(TestResult.java:109)
    at junit.framework.TestCase.run(TestCase.java:118)
    at junit.framework.TestSuite.runTest(TestSuite.java:208)
    at junit.framework.TestSuite.run(TestSuite.java:203)
    at org.apache.tools.ant.taskdefs.optional.junit.JUnitTestRunner.run(JUnitTestRunner.java:325)
    at org.apache.tools.ant.taskdefs.optional.junit.JUnitTask.executeInVM(JUnitTask.java:848)
    at org.apache.tools.ant.taskdefs.optional.junit.JUnitTask.execute(JUnitTask.java:556)
    at org.apache.tools.ant.taskdefs.optional.junit.JUnitTask.execute(JUnitTask.java:532)
    at org.apache.tools.ant.Task.perform(Task.java:341)
    at org.apache.commons.jelly.tags.ant.AntTag.doTag(AntTag.java:185)
    at org.apache.commons.jelly.impl.TagScript.run(TagScript.java:279)
    at org.apache.commons.jelly.impl.ScriptBlock.run(ScriptBlock.java:135)
    at org.apache.commons.jelly.TagSupport.invokeBody(TagSupport.java:233)
    at org.apache.commons.jelly.tags.core.IfTag.doTag(IfTag.java:88)
    at org.apache.commons.jelly.impl.TagScript.run(TagScript.java:279)
    at org.apache.commons.jelly.impl.ScriptBlock.run(ScriptBlock.java:135)
    at org.apache.maven.jelly.tags.werkz.MavenGoalTag.runBodyTag(MavenGoalTag.java:79)
    at org.apache.maven.jelly.tags.werkz.MavenGoalTag$MavenGoalAction.performAction(MavenGoalTag.java:110)
    at com.werken.werkz.Goal.fire(Goal.java:639)
    at com.werken.werkz.Goal.attain(Goal.java:575)
    at com.werken.werkz.Goal.attainPrecursors(Goal.java:488)
    at com.werken.werkz.Goal.attain(Goal.java:573)
    at com.werken.werkz.Goal.attainPrecursors(Goal.java:488)
    at com.werken.werkz.Goal.attain(Goal.java:573)
    at org.apache.maven.plugin.PluginManager.attainGoals(PluginManager.java:671)
    at org.apache.maven.MavenSession.attainGoals(MavenSession.java:263)
    at org.apache.maven.cli.App.doMain(App.java:488)
    at org.apache.maven.cli.App.main(App.java:1239)
    at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)
    at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:39)
    at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:25)
    at java.lang.reflect.Method.invoke(Method.java:324)
    at com.werken.forehead.Forehead.run(Forehead.java:551)
    at com.werken.forehead.Forehead.main(Forehead.java:581)
    2006-11-02 16:41:31,375 WARN [com.atlassian.license.LicenseManager] Attempt to set invalid license. Ensure that you are calling setLicense(license, appName) - not (appName, license)
    java.lang.RuntimeException: Exception getting verification from file - possible classloader problem, or corrupt JIRA installation

To have a smooth build, you may like to turn unit-testing off:

    maven jar:install -Dmaven.test.skip=true




















