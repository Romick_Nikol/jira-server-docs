---
aliases:
- /server/jira/platform/jira-remote-api-reference-24085283.html
- /server/jira/platform/jira-remote-api-reference-24085283.md
category: devguide
confluence_id: 24085283
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=24085283
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=24085283
date: '2017-12-08'
legacy_title: JIRA Remote API Reference
platform: server
product: jira
subcategory: other
title: JIRA remote API reference
---
# JIRA remote API reference

{{% note %}}

Redirection Notice

This page will redirect to <https://developer.atlassian.com/x/YoB> in about 2 seconds.

{{% /note %}}
