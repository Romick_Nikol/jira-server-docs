---
title: JIRA 7.6 Eap Release Notes 51921384
aliases:
    - /server/jira/platform/jira-7.6-eap-release-notes-51921384.html
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=51921384
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=51921384
confluence_id: 51921384
platform: server
product: jira
category: devguide
subcategory:
date: "2018-04-25"
---
# JIRA Developer Documentation : JIRA 7.6 EAP release notes

Find the release notes for the JIRA 7.6 EAP milestones below. For more information about EAP releases, see [JIRA EAP Releases](/server/jira/platform/jira-eap-releases-35160569.html).

-   [JIRA Core 7.6 EAP RC release notes](/server/jira/platform/jira-core-7-6-eap-rc-release-notes)
-   [JIRA Software 7.6 EAP RC release notes](/server/jira/platform/jira-software-7-6-eap-rc-release-notes)
-   [JIRA Core 7.6 EAP release notes](/server/jira/platform/jira-core-7-6-eap-release-notes)
-   [JIRA Software 7.6 EAP release notes](/server/jira/platform/jira-software-7-6-eap-release-notes)
