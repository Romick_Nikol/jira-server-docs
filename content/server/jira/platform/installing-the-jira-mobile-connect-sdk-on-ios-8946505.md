---
title: Installing the JIRA Mobile Connect Sdk On Ios 8946505
aliases:
    - /server/jira/platform/installing-the-jira-mobile-connect-sdk-on-ios-8946505.html
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=8946505
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=8946505
confluence_id: 8946505
platform: server
product: jira
category: devguide
subcategory:
date: "2018-04-25"
---
# JIRA Developer Documentation : Installing the JIRA Mobile Connect SDK on iOS

This page describes how to install the [JIRA Mobile Connect](https://developer.atlassian.com/display/JMC/JIRA+Mobile+Connect+Developer+Documentation) SDK into your iOS app.

## Requirements

-   Xcode 4
-   iOS app (access to the code)

## Installing the JIRA Mobile Connect SDK in your Project

### Downloading the SDK

You can clone the JIRA Mobile Connect SDK from <a href="https://bitbucket.org/atlassian/jiraconnect-ios" class="external-link">Bitbucket</a>:

``` javascript
hg clone https://bitbucket.org/atlassian/jiraconnect-ios
```

(It's a Mercurial repository. New to Bitbucket? See <a href="http://confluence.atlassian.com/display/BITBUCKET/Getting+Started+with+Bitbucket" class="external-link">Getting Started with Bitbucket</a>.)

Alternatively, download the latest release from this location: <a href="https://bitbucket.org/atlassian/jiraconnect-ios/get/tip.zip" class="uri external-link">https://bitbucket.org/atlassian/jiraconnect-ios/get/tip.zip</a>

### Configuration in XCode

1.  Open your project in Xcode, right click on your Classes group, and select `'Add Files to YourProjectName'`.
2.  Browse to the `jiraconnect-ios` clone directory, and add the entire `JIRAConnect/JMCClasses` directory to your project.
3.  If the project you are integrating already contains `Reachability` or `PLCrashReporter` (both third party libraries), delete them from the `JIRAConnect/JMCClasses/Libraries` directory that you just copied into your project.
4.  Open the project (top most) element in the file/groups tree and select your app under `'Targets'`.
5.  Click `'Build Phases'` --&gt; Expand `'Link Binary with Libraries'` --&gt; `+`.
6.  Add the following iOS built-in frameworks:
    -   `CFNetwork`
    -   `SystemConfiguration`
    -   `MobileCoreServices`
    -   `CoreGraphics`
    -   `AVFoundation`
    -   `CoreLocation`
    -   `libsqlite3` (used to cache issues on the device)
7.  Add the `CrashReporter.framework`:
    -   Click `+` --&gt; `'Add Other'`
    -   Browse to `jiraconnect-ios` then `JIRAConnect/JMCClasses/Libraries/` and select `CrashReporter.framework`
    -   Click `'Open'`
8.  If your project does not already have a JSON library, also copy the classes from `JIRAConnect/Support/SBJSON/Classes` to your project.
9.  Compile your App, and ensure there are no errors.

At this stage, your app will not have any JIRA Mobile Connect functionality. The next steps describe how to embed JIRA Mobile Connect in your code and enable all functionality.

**Note:**
If you use automatic reference counting (ARC) you will need to disable it for the JIRA Mobile Connect code:

-   In the `'Build Phases'` view, expand `'Compile Sources'`
-   For all JMC source files, set `-fno-objc-arc` as the compiler flags

### Embedding JIRA Mobile Connect in your Code

1.  Import the JMC.h header file into your ApplicationDelegate
    ``` javascript
    #import "JMC.h"
    ```

2.  Find the following line in your ApplicationDelegate.m:
    ``` javascript
    - (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
    ```

3.  Add the following line to the `didFinishLaunchingWithOptions` method. Simply copying the line from your JIRA project settings is the easiest way to do this. **Note:** Haven't installed the JIRA Mobile Connect plugin on your JIRA instance yet? [See here](https://developer.atlassian.com/display/JMC/Enabling+JIRA+Mobile+Connect#EnablingJIRAMobileConnect-Enabling-JMC-Plugin).
    ``` javascript
    [[JMC sharedInstance] configureJiraConnect:@"https://connect.onjira.com/"
                              projectKey:@"NERDS"
                                  apiKey:@"591451a6-bc59-4ca9-8840-b67f8c1e440f"];
    ```

**Notes for manual configuration:**

-   Replace the string `@"https://connect.onjira.com"` with the location of the JIRA instance you wish to connect to. It is **highly** recommend that you use **https** to ensure secure communication between JMC and the mobile user.
-   Replace the string `@"NERDS"` with the **project key** of the project you wish to use for collecting feedback from users or testers.
-   If the JIRA Mobile Connect plugin in JIRA has an API key enabled, update the above `apiKey` parameter with the key for your project - found in the JIRA project settings.

### Setting up a Feedback Trigger Mechanism

Provide a trigger mechanism to allow users invoke the 'Feedback' view. This typically goes on the 'About' or 'Info' view. The UIViewController returned by JMC viewController is designed to be presented modally. If your info ViewController is in a UINavigationController stack, then you can use the following snippet to show both the feedback view, and the history view.

``` javascript
#import "JMC.h"

- (void)viewDidLoad
{
    self.navigationItem.rightBarButtonItem =
    [[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCompose
                                                   target:self
                                                   action:@selector(showFeedback)] autorelease];
}

-(void) showFeedback
{
    [self presentModalViewController:[[JMC sharedInstance] viewController] animated:YES];
}
```

`[[JMC sharedInstance] viewController]` will return the 'Create Issue' view until the user creates feedback. From then on, the 'Issue Inbox' view is displayed, from where the user can tap the 'Create' icon to send more feedback.

If you would like your users to always access the 'Create Issue' view, then you can do so by presenting the
`[[JMC sharedInstance] feedbackViewController]` directly. The following snippet is an example of how to present just the create issue ViewController (without the inbox).

``` javascript
    - (IBAction)triggerCreateIssueView
    {
        [self presentModalViewController:[[JMC sharedInstance] feedbackViewController] animated:YES];
    }
```

Alternatively, use `[[JMC sharedInstance] issuesViewController]` to present the inbox directly.

## Advanced Configuration

Please refer to the [Advanced Configuration](/server/jira/platform/advanced-configuration-8946512.html) document.

## Issues?

Please raise any issues and feature requests in our issue tracker for the JIRA Mobile Connect SDK: <a href="http://connect.onjira.com/browse/CONNECT" class="uri external-link">http://connect.onjira.com/browse/CONNECT</a>

## Questions?

If you have any questions about JIRA Mobile Connect, please raise them on <a href="https://answers.atlassian.com/tags/jira-mobile-connect/" class="external-link">Atlassian Answers</a>.

## Third Party Package, License, Copyright and Creator

`SBJSON` Copyright © 2009-2010 <a href="http://code.google.com/p/json-framework/" class="external-link">Stig Brautaset</a>. All Rights reserved.

`plcrashreporter` MIT Copyright © 2008-2009 <a href="http://code.google.com/p/plcrashreporter/" class="external-link">Plausible Labs Cooperative, Inc.</a>

`crash-reporter` Copyright © 2009 Andreas Linde & Kent Sutherland.

`UIImageCategories` Created by <a href="http://vocaro.com/trevor/blog/2009/10/12/resize-a-uiimage-the-right-way/" class="external-link">Trevor Harmon</a>.

`FMDB` MIT Copyright © 2008 <a href="http://github.com/ccgus/fmdb" class="external-link">Flying Meat Inc.</a>
