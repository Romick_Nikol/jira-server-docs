---
aliases:
- /server/jira/platform/jira-software-7.4-eap-release-notes-48991312.html
- /server/jira/platform/jira-software-7.4-eap-release-notes-48991312.md
category: devguide
confluence_id: 48991312
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=48991312
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=48991312
date: '2017-12-08'
legacy_title: JIRA Software 7.4 EAP release notes
platform: server
product: jira
subcategory: updates
title: JIRA Software 7.4 EAP release notes
---
# JIRA Software 7.4 EAP release notes

**13 April 2017**

Atlassian is proud to present **JIRA Software 7.4 EAP**. This public development release is part of our [Early Access Program (EAP)](/server/jira/platform/jira-eap-releases-35160569.html) leading up to the official **JIRA Software 7.4** release. We are making these EAP and Release Candidate milestones publicly available so that developers can start assessing the impact of the changes that we are making.

For the full list of changes for JIRA 7.4, read the developer change management guide: [Preparing for JIRA 7.4](/server/jira/platform/preparing-for-jira-7-4). If you are new to JIRA, you should also read our [Java API Policy for JIRA](/server/jira/platform/java-api-policy-for-jira-4227213.html) and the [Atlassian REST API policy](https://developer.atlassian.com/display/HOME/Atlassian+REST+API+policy).

<a href="https://www.atlassian.com/software/jira/download-eap" class="external-link"><img src="/server/jira/platform/images/download-eap-btn.png" class="image-center confluence-thumbnail" width="120" /></a>

{{% warning %}}

Before you install/upgrade: Atlassian does not support upgrades both 'from' and 'to' EAP releases. EAP releases should not be used in production environments as they are not officially supported. For all production use and testing of JIRA, please use the <a href="https://confluence.atlassian.com/display/JIRASOFTWARE/JIRA+Software+7.0.x+release+notes" class="external-link">latest official release</a> instead.

{{% /warning %}}

{{% note %}}

**We want your feedback!**

To provide feedback, create an issue in <a href="https://jira.atlassian.com/secure/CreateIssue!default.jspa?selectedProjectId=10240" class="external-link">our issue tracker</a> with the following details:

-   **Issue Type**: `Suggestion`
-   **Component**: `JIRA 7.4 EAP`

{{% /note %}}

## Extended project administration

We've added an additional option to the **Administer Projects** permission. When granted, the **Extended project administration** option will allow project administrators for the associated projects to:

-   Edit the project's workflows, with the following restrictions:  
    -   The workflow must not be shared with any other projects, or be a system workflow.
    -   To add a status, the status must already exist in the JIRA instance i.e. the project admin can't create new statuses or edit existing statuses.
    -   To delete a status, the status must not be used by any of the project's issues.
    -   The project admin can create, update or delete transitions, but they can't select or update a screen used by the transition, or edit a transition's properties, conditions, validators or post-functions.
-   Edit screens, with the following restrictions:
    -   The screen must not be a default system screen.
    -   The screen must not be shared with any other projects, or used as a transition screen in workflows.
    -   The project admin can add, remove and rearrange system fields.
    -   The project admin can add, remove and rearrange existing custom fields, but they cannot create custom fields.

The **Extended project administration** option can be granted by selecting ![](/server/jira/platform/images/bluecog-icon.png) **&gt; Issues &gt; Permission schemes**, and then choosing the relevant permission scheme to edit. Note that it will be granted to all project administrators for the projects that use that particular permission scheme.

## Kanban backlog

We've implemented the ability to create a Kanban backlog that displays on a separate screen to your Kanban board. This frees up your board from the weight of a lengthy To Do list, and allows the developers to concentrate on the issues that need to be worked on. You can turn on the Kanban backlog by accessing your standard Kanban board and selecting <img src="/server/jira/platform/images/more.png" class="confluence-thumbnail" width="25" />** &gt; Board settings &gt; Columns**. You'll see instructions on how to set up your Kanban backlog.

## HTML export

We've implemented an exporter which exports JIRA issues in an HTML format. When viewing a list of issues, select <img src="/server/jira/platform/images/xport.png" class="confluence-thumbnail" width="80" /> **&gt; HTML (All fields)** or <img src="/server/jira/platform/images/xport.png" class="confluence-thumbnail" width="80" /> **&gt; HTML (Current fields)**. A JIRA administrator can disable the HTML export option by setting the property to 'false' at ![](/server/jira/platform/images/bluecog-icon.png) **&gt; System** and clicking **Advanced Settings**.

## In-app notification

We've implemented a system add-on which provides targeted notifications within your JIRA application, predominantly to JIRA administrators. These notifications will alert you when new JIRA versions are available, and of upcoming license renewals.

## Upgrades

We don't support upgrading to or from EAP releases, and you should never use an EAP release in production. If you do intend to upgrade an instance to an EAP release, you should review  _all_  applicable upgrades notes for the production releases, and upgrade in the specified order:

-   <a href="https://confluence.atlassian.com/display/JIRASOFTWARE/JIRA+Software+7.3.x+upgrade+notes" class="external-link">Upgrading to JIRA Software 7.3</a>
-   <a href="https://confluence.atlassian.com/display/JIRASOFTWARE/JIRA+Software+7.2.x+upgrade+notes" class="external-link">Upgrading to JIRA Software 7.2</a>
-   <a href="https://confluence.atlassian.com/display/JIRASOFTWARE/JIRA+Software+7.1.x+upgrade+notes" class="external-link">Upgrading to JIRA Software 7.1</a>
-   <a href="https://confluence.atlassian.com/migration/jira-7/server_jira_upgrade" class="external-link">Upgrading to JIRA Software 7.0</a>
