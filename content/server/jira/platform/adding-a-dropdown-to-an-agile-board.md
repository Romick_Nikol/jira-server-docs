---
aliases:
- /server/jira/platform/tutorial-adding-a-dropdown-to-an-agile-board-33744890.html
- /server/jira/platform/tutorial-adding-a-dropdown-to-an-agile-board-33744890.md
category: devguide
confluence_id: 33744890
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=33744890
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=33744890
date: '2017-12-08'
guides: tutorials
legacy_title: Tutorial - Adding a dropdown to an agile board
platform: server
product: jira
subcategory: learning
title: Adding a dropdown to an agile board
---
# Adding a dropdown to an agile board

In this tutorial, you will add a new dropdown to your JIRA Software Server add-on. This dropdown will appear in the top right corner of an Agile board, next to the **Board** dropdown. This is done via a plugin point (added in JIRA Agile 6.6.50). 

To add this new dropdown, you will be creating a new web-section. The location of the web-section will be `jira.agile.board.tools`.  You will then add a web-item or web-panel inside the web-section. That's it! See the screenshot below to see what it looks like.

*Screenshot: Example web-section with a web-panel in jira.agile.board.tools.*

<img src="/server/jira/platform/images/screen-shot-2015-06-19-at-13.43.15.png" width="435" />

{{% note %}}

About these Instructions

You can use any supported combination of OS and IDE to create this plugin. These instructions were written using Sublime Text editor on Mac OS X. If you are using a different IDE and/or OS, you should use the equivalent operations for your specific environment.

This tutorial was last tested with JIRA 6.5-OD-07 and JIRA Agile 6.7.6.

{{% /note %}}

## Step 1. Create a web-section in your plugin.xml

Your web-section should look similar to this example:

``` xml
<web-section location="jira.agile.board.tools" key="my.custom-board-tool-section" weight="10"> 
    <label key="gh.board.tools.reference.button"/> 
    <condition class="com.atlassian.greenhopper.client.web.condition.BoardModeCondition"> 
        <param name="mode">work</param> 
    </condition> 
</web-section>
```

## Step 2. Add a web panel, which will be placed in your new web-section

Your web-panel should look similar to this example:

``` xml
<web-panel key="board-tools-webpanel" location="my.custom-board-tool-section" weight="100">
    <resource name="view" type="velocity" location="templates/board-tools-webpanel.vm" />
    <context-provider class="com.atlassian.jira.plugin.webfragment.contextproviders.DefaultVelocityContextProvider" />
</web-panel>
```

## Step 3. Create a velocity template that will be rendered inside the web-panel

Three context parameters are available in the template. These are` $board.id,` `$board.type, $board.screen`. 

The mode parameter is an enumeration with the following possible values:

-   **plan**, which is tied to the Backlog page

-   **work**, which is tied to Active Sprints page

-   **report**, which is tied to the Reports page

``` xml
<div> We are viewing a board with id $board.id in $board.screen mode. </div>
```

Context parameters can also be used in web-item links, as shown below:

``` xml
<web-item section="my.custom-board-tool-section" key="my-custom-web-item" weight="10">
    <label key="gh.rapid.operations.configure"/>
    <link>/secure/RapidView.jspa?rapidView=${board.id}</link>
</web-item>
```

## (Optional) Add conditions

You can add a condition so that the web-section is only displayed if the board is in a specific mode. The following code shows an example where the web-section only displays on the Active Sprints/Kanban board. See the screenshot below for an example of this.

Note, the possible values for the **mode** parameter are **plan**, **work** and **report**.

``` xml
<condition class="com.atlassian.greenhopper.client.web.condition.BoardModeCondition">
    <param name="mode">work</param>
</condition>
```

*Screenshot: Active sprints view with a dropdown provided by a plugin*

<img src="/server/jira/platform/images/screen-shot-2015-06-19-at-13.56.42.png" width="870" /> 

**Congratulations!** You've just added a dropdown to an Agile board in your Connect add-on.
