---
aliases:
- /server/jira/platform/6848642.html
- /server/jira/platform/6848642.md
category: devguide
confluence_id: 6848642
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=6848642
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=6848642
date: '2017-12-08'
legacy_title: Changes to markup, CSS and Decorators in JIRA 5.0
platform: server
product: jira
subcategory: updates
title: Changes to markup, CSS and Decorators in JIRA 5.0
---
# Changes to markup, CSS and Decorators in JIRA 5.0

JIRA 5.0 milestone 4 includes significant changes to markup, CSS and decorators. We're moving towards a common markup pattern and theme for all our applications. This will provide a baseline which each product can build upon.

Some of the key points regarding these changes are:

-   **Consistent markup/CSS across JIRA pages:** one common template/set of styles no matter what part of JIRA you are integrating with!
-   **Better performance for the client and server:** IE7 hacks and JS crutches removed now that IE7 is no longer a supported browser
-   **More stability surrounding the markup/CSS:** new template/styles are much more generic and flexible enough that changes to it should be rare
-   **Structure/semantics of the markup is improved:** use of HTML5 elements like header/footer/sections and better IDs/Classes make things a lot clearer
-   **Default styles reflect Look & Feel:** fall back on JIRA's styles to stay consistent with the L&F colours, font sizes, etc (headings now use the L&F heading colour)

## New HTML structure

Atlassian intends to adopt this new template across all products, this will provide a much more consistent user experience when moving from one product to another as well as lower the learning curve for plugin writers when writing a plugin for an app they're not familiar with. In the future there may be the potential for a cross product plugin which only needs a single set of markup. 

Some side-by-side comparisons of various page structures in JIRA are shown below (click to enlarge). You can see that the markup is now a lot more consistent. In order to change how the template displays on certain pages we now use a CSS class of "page-type-foo" on the body. This replaces all the old classes like "nl", "type-a", "type-aa", etc. All pages now get a common look and feel, the baseline of which will be soon provided by AUI. JIRA will then extend the baseline styles with the customisations required. This method of changing how the template behaves is also recommended for plugins.

The nodes highlighted in blue are where most of the "content" starts. The new markup is longer in most cases due to the inclusion of a new wrapping div (&lt;div id="page"&gt;). This is primarily there as a styling hook for people theming our products but it also brings JIRA more inline with some of the other products. You'll notice that there are now 3 distinct areas - \#header, \#content and \#footer. This is now consistent across all of JIRA (some decorators don't include the header/footer - see the section on decorators below).

We realize that these changes will be extensive for plugins which provide additional UI elements inside of JIRA. However we believe that this new approach will make JIRA much easier to work with going forward. Because of the scale of the changes, we are providing a migration guide so you know exactly how to migrate from JIRA 4.4 markup to JIRA 5.0.

<img src="/server/jira/platform/images/viewissie.png" class="confluence-thumbnail" />

<img src="/server/jira/platform/images/userprofile.png" class="confluence-thumbnail" />

<img src="/server/jira/platform/images/message.png" class="confluence-thumbnail" />

<img src="/server/jira/platform/images/issuenav.png" class="confluence-thumbnail" />

<img src="/server/jira/platform/images/browseproject.png" class="confluence-thumbnail" />

<img src="/server/jira/platform/images/administration.png" class="confluence-thumbnail" />

## Changes to the Decorators

The table below shows the old and new decorators side-by-side. The example images use the following markup and simply change the decorator applied.

``` xml
<html>
<head>
  <title>Title element in header</title>
  <meta name="decorator" content="none" />
</head>
<body>
 <h1>h1 text in the body</h1>
 <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec eros turpis, adipiscing quis molestie ut, convallis nec turpis. Quisque sollicitudin purus dui. Integer nec lectus in velit volutpat pulvinar et a nunc. Etiam a nisl sed mauris ultrices scelerisque semper vitae lectus. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Duis fermentum auctor luctus. Cras augue leo, sodales mattis tempor ut, condimentum ut erat. Quisque a libero vitae massa bibendum consequat. Aliquam laoreet enim ut arcu eleifend venenatis. Morbi in adipiscing arcu. Vestibulum mollis, nisi ac hendrerit scelerisque, leo enim lacinia erat, vel bibendum nisi ligula sed purus. Nunc a dui dui. Vestibulum pharetra convallis urna, et accumsan mi feugiat at. Duis tincidunt, massa vitae imperdiet faucibus, augue augue ultrices eros, quis condimentum ligula erat quis nibh. Sed urna ante, cursus vel vestibulum vitae, consectetur ut mauris.</p>
 <p>Maecenas ultrices elit placerat lacus porta at interdum dui dictum. Quisque aliquet tincidunt justo, ac iaculis magna volutpat congue. Vestibulum suscipit justo sed justo posuere porttitor. Nullam ac purus sed quam porttitor bibendum in ut orci. Vivamus fermentum tempor venenatis. Etiam turpis urna, egestas quis rhoncus id, bibendum a justo. Mauris eu ligula et eros auctor facilisis sit amet in enim. Integer et felis ipsum. Nam dictum dictum gravida. Donec nec odio vel purus dapibus sodales at non ante. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Nulla facilisi. Integer eu tincidunt nisl. Aenean enim ligula, blandit et hendrerit nec, dictum vitae massa. Sed in mauris elit, eget malesuada mauris. </p>
</body>
</html>  
```

<table>
<colgroup>
<col style="width: 25%" />
<col style="width: 25%" />
<col style="width: 25%" />
<col style="width: 25%" />
</colgroup>
<thead>
<tr class="header">
<th><p>JIRA 4.4.x</p></th>
<th><p> </p></th>
<th><p>JIRA 5</p></th>
<th><p> </p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>Decorator name<br />
Example<br />
Notes</p></td>
<td><p>Contexts applied</p></td>
<td><p>Decorator name<br />
Example<br />
Notes</p></td>
<td><p>Contexts applied</p></td>
</tr>
<tr class="even">
<td><p><strong>atl.admin</strong></p></td>
<td><p>atl.admin<br />
jira.admin</p></td>
<td><p><strong>atl.admin</strong><br />
class=&quot;page-type-admin&quot;</p></td>
<td><p>atl.admin<br />
jira.admin</p></td>
</tr>
<tr class="odd">
<td><p><strong>atl.general</strong></p></td>
<td><p>atl.general<br />
jira.general</p></td>
<td><p><strong>atl.general</strong></p></td>
<td><p>atl.general<br />
jira.general</p></td>
</tr>
<tr class="even">
<td><p><strong>atl.popup</strong></p></td>
<td><p>atl.popup<br />
jira.popup</p></td>
<td><p><strong>atl.popup</strong><br />
class=&quot;page-type-popup&quot;</p></td>
<td><p>atl.popup<br />
jira.popup</p></td>
</tr>
<tr class="odd">
<td><p><strong>atl.userprofile</strong></p></td>
<td><p>atl.userprofile<br />
jira.userprofile<br />
atl.general<br />
jira.general</p></td>
<td><p><strong>atl.userprofile</strong></p></td>
<td><p>atl.userprofile<br />
jira.userprofile<br />
atl.general<br />
jira.general</p></td>
</tr>
<tr class="even">
<td><p><strong>none</strong><br />
Doesn't affect markup and includes no resources</p></td>
<td><p>--</p></td>
<td><p><strong>none</strong><br />
Doesn't affect markup and includes no resources</p></td>
<td><p>--</p></td>
</tr>
<tr class="odd">
<td><p><strong>admin</strong><br />
Same as &quot;atl.admin&quot;</p></td>
<td><p>atl.admin<br />
jira.admin</p></td>
<td><p><strong>admin</strong><br />
same as &quot;atl.admin&quot;<br />
class=&quot;page-type-admin&quot;</p></td>
<td><p>atl.admin<br />
jira.admin</p></td>
</tr>
<tr class="even">
<td><p><strong>general</strong><br />
Same as &quot;atl.general&quot;</p></td>
<td><p>atl.general<br />
jira.general</p></td>
<td><p><strong>general</strong><br />
Same as &quot;atl.general&quot;</p></td>
<td><p>atl.general<br />
jira.general</p></td>
</tr>
<tr class="odd">
<td><p><strong>main</strong></p></td>
<td><p>atl.general<br />
jira.general</p></td>
<td><p><strong>main</strong><br />
Same as &quot;atl.general&quot;<br />
Mapped to &quot;atl.general&quot; in 5.0<br />
DEPRECATED</p></td>
<td><p>atl.general<br />
jira.general</p></td>
</tr>
<tr class="even">
<td><p><strong>insecure</strong><br />
Same as &quot;main&quot;<br />
Mapped to &quot;main&quot; in 4.4</p></td>
<td><p>atl.general<br />
jira.general</p></td>
<td><p><strong>insecure</strong><br />
Same as &quot;atl.general&quot;<br />
Mapped to &quot;atl.general&quot; in 5.0<br />
DEPRECATED</p></td>
<td><p>atl.general<br />
jira.general</p></td>
</tr>
<tr class="odd">
<td><p><strong>frontpage</strong></p></td>
<td><p>atl.general<br />
jira.general</p></td>
<td><p><strong>frontpage</strong><br />
Same as &quot;atl.general&quot;<br />
Mapped to &quot;atl.general&quot; in 5.0<br />
DEPRECATED</p></td>
<td><p>atl.general<br />
jira.general</p></td>
</tr>
<tr class="even">
<td><p><strong>notitle</strong></p></td>
<td><p>atl.general<br />
jira.general</p></td>
<td><p><strong>notitle</strong><br />
Same as &quot;atl.general&quot;<br />
Mapped to &quot;atl.general&quot; in 5.0<br />
DEPRECATED</p></td>
<td><p>atl.general<br />
jira.general</p></td>
</tr>
<tr class="odd">
<td><p><strong>setup</strong><br />
Not for plugin use</p></td>
<td><p>--</p></td>
<td><p><strong>setup</strong><br />
Not for plugin use</p></td>
<td><p>--</p></td>
</tr>
<tr class="even">
<td><p><strong>setupgh</strong><br />
Not for plugin use</p></td>
<td><p>--</p></td>
<td><p><strong>setupgh</strong><br />
Not for plugin use</p></td>
<td><p>--</p></td>
</tr>
<tr class="odd">
<td><p><strong>dialog</strong></p></td>
<td><p>--</p></td>
<td><p><strong>dialog</strong></p></td>
<td><p>--</p></td>
</tr>
<tr class="even">
<td><p><strong>dialog-iframe</strong><br />
DEPRECATED</p></td>
<td><p>--</p></td>
<td><p><strong>dialog-iframe</strong><br />
DEPRECATED</p></td>
<td><p>--</p></td>
</tr>
<tr class="odd">
<td><p><strong>error</strong></p></td>
<td><p>atl.error<br />
jira.error</p></td>
<td><p><strong>error</strong><br />
class=&quot;page-type-message&quot;<br />
If you push messages to the error collection they'll be shown below the content (see second screenshot)</p></td>
<td><p>atl.error<br />
jira.error</p></td>
</tr>
<tr class="even">
<td><p><strong>message</strong></p></td>
<td><p>atl.message<br />
atl.error<br />
jira.message<br />
jira.error</p></td>
<td><p><strong>message</strong><br />
class=&quot;page-type-message&quot;</p></td>
<td><p>atl.message<br />
atl.error<br />
jira.message<br />
jira.error</p></td>
</tr>
<tr class="odd">
<td><p><strong>issueaction</strong><br />
Not for plugin use</p></td>
<td><p>atl.general<br />
jira.general</p></td>
<td><p><strong>issueaction</strong><br />
Not for plugin use</p></td>
<td><p>atl.general<br />
jira.general</p></td>
</tr>
<tr class="even">
<td><p><strong>navigator</strong><br />
Not for plugin use</p></td>
<td><p>jira.navigator<br />
atl.general<br />
jira.general</p></td>
<td><p><strong>navigator</strong><br />
Not for plugin use</p></td>
<td><p>jira.navigator<br />
atl.general<br />
jira.general</p></td>
</tr>
<tr class="odd">
<td><p><strong>printable</strong></p></td>
<td><p>atl.general<br />
jira.general</p></td>
<td><p><strong>printable</strong></p></td>
<td><p>atl.general<br />
jira.general</p></td>
</tr>
<tr class="even">
<td><p><strong>popup</strong><br />
Same as &quot;atl.popup&quot;</p></td>
<td><p>atl.popup<br />
jira.popup</p></td>
<td><p><strong>popup</strong><br />
Same as &quot;atl.popup&quot;<br />
class=&quot;page-type-popup&quot;<br />
No header/footer in 5.0</p></td>
<td><p>atl.popup<br />
jira.popup</p></td>
</tr>
<tr class="odd">
<td><p><strong>issuesummary</strong><br />
DEPRECATED</p></td>
<td><p>atl.general<br />
jira.general</p></td>
<td><p>--<br />
Removed in 5.0</p></td>
<td><p>--</p></td>
</tr>
<tr class="even">
<td><p><strong>genericaction</strong></p></td>
<td><p>atl.general<br />
jira.general</p></td>
<td><p><strong>generication</strong><br />
Same as &quot;atl.general&quot;<br />
Mapped to &quot;atl.general&quot; in 5.0<br />
DEPRECATED</p></td>
<td><p>atl.general<br />
jira.general</p></td>
</tr>
<tr class="odd">
<td><p><strong>inline</strong><br />
Not recommended for use</p></td>
<td><p>atl.inline.dialog<br />
jira.inline.dialog</p></td>
<td><p>Not changed</p></td>
<td><p>atl.inline.dialog<br />
jira.inline.dialog</p></td>
</tr>
<tr class="even">
<td><p> </p></td>
<td><p> </p></td>
<td><p><strong>login</strong><br />
Not for plugin use</p></td>
<td><p>--</p></td>
</tr>
<tr class="odd">
<td><p> </p></td>
<td><p> </p></td>
<td><p><strong>panel-general</strong><br />
<img src="/server/jira/platform/images/icons/emoticons/star_yellow.png" alt="(star)" class="emoticon-yellow-star" /> new in 5.0</p></td>
<td><p>atl.general<br />
jira.general</p></td>
</tr>
<tr class="even">
<td><p> </p></td>
<td><p> </p></td>
<td><p><strong>panel-admin</strong><br />
<img src="/server/jira/platform/images/icons/emoticons/star_yellow.png" alt="(star)" class="emoticon-yellow-star" /> new in 5.0<br />
class=&quot;page-type-admin&quot;</p></td>
<td><p>atl.admin<br />
jira.admin</p></td>
</tr>
<tr class="odd">
<td><p> </p></td>
<td><p> </p></td>
<td><p><strong>blank</strong><br />
<img src="/server/jira/platform/images/icons/emoticons/star_yellow.png" alt="(star)" class="emoticon-yellow-star" /> new in 5.0<br />
No header/footer/content panel but does have resources<br />
class=&quot;page-type-blank&quot;</p></td>
<td><p>--</p></td>
</tr>
<tr class="even">
<td><p> </p></td>
<td><p> </p></td>
<td><p><strong>content-panel-only</strong><br />
<img src="/server/jira/platform/images/icons/emoticons/star_yellow.png" alt="(star)" class="emoticon-yellow-star" /> new in 5.0<br />
No header/footer but has the content panel and resources</p></td>
<td><p>--</p></td>
</tr>
</tbody>
</table>

