---
title: "JIRA Eap Releases"
product: jira
platform: server
category: devguide
subcategory: updates
date: "2018-02-16"
aliases:
    - /server/jira/platform/jira-eap-releases-35160569.html
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=35160569
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=35160569
confluence_id: 35160569

---
# JIRA Developer Documentation : JIRA EAP Releases

An **Early Access Program (EAP) release** is a public development release leading up to the official release of a JIRA version. Development releases are a snapshot of our work in progress, primarily focused on allowing JIRA users to see the new features in advance and provide us with some useful feedback. It also gives add-on developers an opportunity to test and fix their add-ons in advance of an official release.

EAP releases, which include more mature Beta and Release Candidate (RC) builds are available for <a href="http://www.atlassian.com/software/jira/download-eap" class="external-link">download</a> and their release notes are listed below. Your help with testing them is very much appreciated!   
*Note, Beta and Release Candidate builds generally do not introduce new changes, and provide bug fixes and tweaks to features introduced in earlier EAP releases instead.*

**About JIRA Labs** -- The **JIRA EAP** is for customers who download and install JIRA Server. The **JIRA Labs** program extends feature previews to JIRA Cloud and JIRA plugin customers as well. For information about JIRA Labs, please see <a href="https://confluence.atlassian.com/display/Cloud/Labs+features+in+JIRA" class="external-link">Labs features in JIRA</a>.

<center><a href="/server/jira/platform/">EAP release notes</a></center>


