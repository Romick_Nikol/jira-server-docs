---
aliases:
- /server/jira/platform/jira-service-desk-3.9-eap-release-notes-51921397.html
- /server/jira/platform/jira-service-desk-3.9-eap-release-notes-51921397.md
category: devguide
confluence_id: 51921397
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=51921397
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=51921397
date: '2017-12-08'
legacy_title: JIRA Service Desk 3.9 EAP release notes
platform: server
product: jira
subcategory: updates
title: JIRA Service Desk 3.9 EAP release notes
---
# JIRA Service Desk 3.9 EAP release notes

**3 October 2017**

Atlassian is proud to present JIRA **Service Desk 3.9 EAP**. This public development release is part of our [Early Access Program (EAP)](/server/jira/platform/jira-eap-releases-35160569.html) leading up to the official **Jira Service Desk 3.9** release. We are making these EAP and Release Candidate milestones publicly available so that developers can start assessing the impact of the changes that we are making.

Jira Service Desk 3.9 also includes the Jira 7.6 platform changes, and for the full list of changes in Jira 7.6, read the developer change management guide: [Preparing for JIRA 7.6](/server/jira/platform/preparing-for-jira-7-6). If you are new to Jira, you should also read our [Java API Policy for Jira](/server/jira/platform/java-api-policy-for-jira-4227213.html) and the [Atlassian REST API policy](https://developer.atlassian.com/display/HOME/Atlassian+REST+API+policy).

<a href="https://www.atlassian.com/software/jira/download-eap" class="external-link"><img src="/server/jira/platform/images/download-eap-btn.png" class="image-center confluence-thumbnail" width="120" /></a>

<img src="/server/jira/platform/images/icons/emoticons/forbidden.png" alt="(minus)" class="emoticon-minus" /> *Before you install/upgrade: Atlassian does not support upgrades both 'from' and 'to' EAP releases. EAP releases should not be used in production environments as they are not officially supported. For all production use and testing of Jira, please use the <a href="https://confluence.atlassian.com/display/JIRASOFTWARE/JIRA+Software+7.0.x+release+notes" class="external-link">latest official release</a> instead.*

## Changes

## Priorities per project

We're currently working on a quite old and even more famous issue: Priorities per project, so a way to assign separate, unique priorities to each of your projects (as opposed to having a common set of priorities, like it is now.)

Jira Service Desk 3.9 will introduce a new scheme that lets Jira admins add new priorities and choose projects they're assigned to, at the same time allowing some priorities to be shared globally. We're still in the early stages of developing this feature, and this EAP milestone doesn't bring any changes to the UI.

What it brings is necessary changes to API. To manage priority schemes, we added new APIs, deleted some, and improved some more, so they can handle the new schemes. If you're an app developer and your app is using the priorities' API in any way, take a look at the complete list of changes you should be aware of: [Preparing for JIRA 7.6](/server/jira/platform/preparing-for-jira-7-6).
