---
aliases:
- /server/jira/platform/jira-templates-and-jsps-4227146.html
- /server/jira/platform/jira-templates-and-jsps-4227146.md
category: devguide
confluence_id: 4227146
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=4227146
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=4227146
date: '2017-12-08'
legacy_title: JIRA templates and JSPs
platform: server
product: jira
subcategory: blocks
title: Jira templates and JSPs
---
# Jira templates and JSPs

If you wish to change Jira's behaviour by modifying the JSPs or Velocity templates, you can do so simply by changing files in the Jira distribution. Jira will pick up changes to its JSPs the next time that page is displayed: restarting isn't necessary. On the other hand, you will need to restart Jira in order to see changes to the Velocity templates.

However, keep in mind that the next time you upgrade Jira - or need a new installation for any reason - you will have to manually copy any changes you have made to the JSPs or templates into the new installation of Jira. If the JSPs or templates have changed in the newer version, you will have to port your customization into them.

## Velocity templates

<a href="http://velocity.apache.org/engine/devel/user-guide.html" class="external-link">Velocity</a> is a server-side templating language used by Jira to render page content. Velocity allows Java objects to be called alongside standard HTML. The following table lists the Velocity contexts available for use in the XML descriptor and Velocity views.

<table>
<colgroup>
<col style="width: 20%" />
<col style="width: 80%" />
</colgroup>
<thead>
<tr class="header">
<th><p>Name</p></th>
<th><p>Description</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>user</p></td>
<td><p>Currently logged in user.</p>
<p><strong>Type:</strong> User.</p></td>
</tr>
<tr class="even">
<td><p>helper</p></td>
<td><p>Convenient class which holds information such as the current request and selected project.</p>
<p><strong>Type:</strong> JiraHelper.</p></td>
</tr>
<tr class="odd">
<td><p>xmlutils</p></td>
<td><p>Utilities for basic XML reading.</p>
<p><strong>Type:</strong> XMLUtils.</p></td>
</tr>
<tr class="even">
<td><p>textutils</p></td>
<td><p>Utilities for common String manipulations.</p>
<p><strong>Type:</strong> <a href="http://www.opensymphony.com/oscore/api/com/opensymphony/util/TextUtils.html" class="external-link">TextUtils</a>.</p></td>
</tr>
<tr class="odd">
<td><p>urlcodec</p></td>
<td><p>Utility for encoding a string.</p>
<p><strong>Type:</strong> <a href="http://docs.atlassian.com/software/jira/docs/api/latest/com/atlassian/jira/util/URLCodec.html" class="external-link">JiraUrlCodec</a>.</p></td>
</tr>
<tr class="even">
<td><p>outlookdate</p></td>
<td><p>Class to give a nice String representation of a date.</p>
<p><strong>Type:</strong> <a href="http://docs.atlassian.com/software/jira/docs/api/latest/com/atlassian/jira/web/util/OutlookDate.html" class="external-link">OutlookDate</a>.</p></td>
</tr>
<tr class="odd">
<td><p>authcontext</p></td>
<td><p>User locale dependant utility class. Can get the current user, locale, I18nBean (for internationalisation) and OutlookDate.</p>
<p><strong>Type:</strong> <a href="http://docs.atlassian.com/software/jira/docs/api/latest/com/atlassian/jira/security/JiraAuthenticationContext.html" class="external-link">JiraAuthenticationContext</a>.</p></td>
</tr>
<tr class="even">
<td><p>dateutils</p></td>
<td><p>Utilities for displaying date/time.</p>
<p><strong>Type:</strong> DateUtils.</p></td>
</tr>
<tr class="odd">
<td><p>externalLinkUtil</p></td>
<td><p>A simple utility class that lets you resolve external links that may need to change, because of partner sites and such.</p>
<p><strong>Type:</strong> <a href="http://docs.atlassian.com/software/jira/docs/api/latest/com/atlassian/jira/web/util/ExternalLinkUtilImpl.html" class="external-link">ExternalLinkUtil</a>.</p></td>
</tr>
<tr class="even">
<td><p>requestContext</p></td>
<td><p>A context that allows for different implementations depending on whether it is running in the scope of a web request, or via email.</p>
<p><strong>Type:</strong> VelocityRequestContext.</p></td>
</tr>
<tr class="odd">
<td><p>req</p></td>
<td><p>current request</p>
<p><strong>Type:</strong> <a href="http://java.sun.com/j2ee/1.4/docs/api/javax/servlet/http/HttpServletRequest.html" class="external-link">HttpServletRequest</a>.</p></td>
</tr>
<tr class="even">
<td><p>baseurl</p></td>
<td><p>The base URL for this instance (velocityRequestContext.getBaseUrl()).</p>
<p><strong>Type:</strong> String.</p></td>
</tr>
</tbody>
</table>

## Contents of the Velocity context

The contents of the Velocity context used to process web templates and email templates is listed below.

### Web templates

This is the listing of the contents of the Velocity context used to process web templates.

<table>
<colgroup>
<col style="width: 40%" />
<col style="width: 20%" />
<col style="width: 5%" />
<col style="width: 20%" />
</colgroup>
<thead>
<tr class="header">
<th><p>Velocity variable &amp; Jira Object/Value</p></th>
<th><p>Description</p></th>
<th><p>As of Version </p></th>
<th><p> Condition</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>constantsManager</p>
<p>com.atlassian.jira.config .ConstantsManager</p></td>
<td><p> Manager for issue types, statuses, priorities and resolutions.</p></td>
<td><p> 3.4</p></td>
<td><p> always present</p></td>
</tr>
<tr class="even">
<td><p>projectManager</p>
<p>com.atlassian.jira.project .ProjectManager</p></td>
<td><p> Implementations of this interface are responsible for all management of project entities within Jira.</p></td>
<td><p> 3.4</p></td>
<td><p> always present</p></td>
</tr>
<tr class="odd">
<td><p>customFieldManager</p>
<p>com.atlassian.jira.issue .CustomFieldManager</p></td>
<td><p> Functions for working with CustomFields</p></td>
<td><p> 3.5</p></td>
<td><p> always present</p></td>
</tr>
<tr class="even">
<td><p>applicationProperties</p>
<p>com.atlassian.jira.config.properties .ApplicationProperties</p></td>
<td><p> provides access to Jira properties stored in the DB</p></td>
<td><p> 3.4</p></td>
<td><p> always present </p></td>
</tr>
<tr class="odd">
<td><p> jirautils </p>
<p>com.atlassian.jira.util.JiraUtils</p></td>
<td><p> Miscellaneous utility methods.</p></td>
<td><p> 3.4</p></td>
<td><p> always present</p></td>
</tr>
<tr class="even">
<td><p> jirakeyutils</p>
<p>com.atlassian.jira.util.JiraKeyUtils</p></td>
<td><p> utilities to determine the validity of Jira project/issue keys</p></td>
<td><p> 3.4</p></td>
<td><p> always present </p></td>
</tr>
<tr class="odd">
<td><p> buildutils</p>
<p>com.atlassian.jira.util.BuildUtils</p></td>
<td><p> provides information on the running version of Jira</p></td>
<td><p> 3.4</p></td>
<td><p> always present </p></td>
</tr>
<tr class="even">
<td><p> velocityhelper</p>
<p>com.atlassian.jira.util .JiraVelocityHelper</p></td>
<td><p> A simple class store methods we want to expose to velocity templates</p></td>
<td><p> 3.4</p></td>
<td><p> always present</p></td>
</tr>
<tr class="odd">
<td><p> userutils</p>
<p>com.atlassian.core.user.UserUtils</p></td>
<td><p> A utility class for operating on users.</p></td>
<td><p> 3.4</p></td>
<td><p> always present</p></td>
</tr>
<tr class="even">
<td><p> textutils</p>
<p>com.opensymphony.util.TextUtils</p></td>
<td><p> lots of utility methods for manipulating text</p></td>
<td><p> 3.4</p></td>
<td><p> always present </p></td>
</tr>
<tr class="odd">
<td><p> params</p>
<p>java.util.Map</p></td>
<td><p> parameters of the IssueEvent that triggered this email notification </p></td>
<td><p> 3.4</p></td>
<td><p> always present</p></td>
</tr>
<tr class="even">
<td><p> issue</p>
<p>org.ofbiz.core.entity .GenericValue</p></td>
<td><p> a GenericValue representing the issue which triggered this email notification </p></td>
<td><p> 3.4</p></td>
<td><p> always present</p></td>
</tr>
<tr class="odd">
<td><p> issueObject</p>
<p>com.atlassian.jira.issue .MutableIssue</p></td>
<td><p>an Issue object representing the issue which triggered this email notification</p></td>
<td><p> 3.5.2</p></td>
<td><p> always present</p></td>
</tr>
<tr class="even">
<td><p> remoteUser</p>
<p>com.opensymphony.user.User</p></td>
<td><p> the logged in user if they exist </p></td>
<td><p> 3.4</p></td>
<td><p> remoteUser != null </p></td>
</tr>
<tr class="odd">
<td><p> renderedDescription</p>
<p>java.lang.String</p></td>
<td><p> the rendered description field, it a renderer has been specified </p></td>
<td><p> 3.4</p></td>
<td><p> renderer specifed for issue and field </p></td>
</tr>
<tr class="even">
<td><p> renderedEnvironment</p>
<p>java.lang.String</p></td>
<td><p> the rendered environment field, it a renderer has been specified </p></td>
<td><p> 3.4</p></td>
<td><p> renderer specifed for issue and field</p></td>
</tr>
<tr class="odd">
<td><p> timeoriginalestimate</p>
<p>java.lang.String</p></td>
<td><p>The DateUtils .getDurationPretty value of timeoriginalestimate from issue or &quot;None&quot; if null</p></td>
<td><p> 3.4</p></td>
<td><p> always present</p></td>
</tr>
<tr class="even">
<td><p> timeestimate</p>
<p> java.lang.String</p></td>
<td><p> The DateUtils .getDurationPretty value of timeestimate from issue or &quot;None&quot; if null</p></td>
<td><p> 3.4</p></td>
<td><p> always present</p></td>
</tr>
<tr class="odd">
<td><p> timespent</p>
<p>java.lang.String</p></td>
<td><p> The DateUtils .getDurationPretty value of timespent from issue or &quot;None&quot; if null</p></td>
<td><p> 3.4</p></td>
<td><p> always present</p></td>
</tr>
<tr class="even">
<td><p> duedate</p>
<p> java.sql.Timestamp</p></td>
<td><p> Duedate from the issue</p></td>
<td><p> 3.4</p></td>
<td><p> always present</p></td>
</tr>
<tr class="odd">
<td><p> versions</p>
<p>List( org.ofbiz.core.entity .GenericValue )</p></td>
<td><p> A list of GenericValues representing the Affected Versions of the issue</p></td>
<td><p> 3.4</p></td>
<td><p> always present</p></td>
</tr>
<tr class="even">
<td><p> fixfors</p>
<p>List( org.ofbiz.core.entity .GenericValue )</p></td>
<td><p> A list of GenericValues representing the Fix Versions of the issue</p></td>
<td><p> 3.4</p></td>
<td><p> always present</p></td>
</tr>
<tr class="odd">
<td><p> components</p>
<p>List( org.ofbiz.core.entity .GenericValue )</p></td>
<td><p> A list of GenericValues representing the Components of the issue</p></td>
<td><p> 3.4</p></td>
<td><p> always present</p></td>
</tr>
<tr class="even">
<td><p> assignee</p>
<p>com.opensymphony.user.User</p></td>
<td><p> The assignee of the issue</p></td>
<td><p> 3.4</p></td>
<td><p> assignee != null </p></td>
</tr>
<tr class="odd">
<td><p> reporter</p>
<p>com.opensymphony.user.User</p></td>
<td><p> The reporter of the issue</p></td>
<td><p> 3.4</p></td>
<td><p> reporter != null</p></td>
</tr>
<tr class="even">
<td><p> renderedComment</p>
<p>java.lang.String</p></td>
<td><p> the rendered comment field, it a renderer has been specified. The comment is the comment associated with the change</p></td>
<td><p> 3.4</p></td>
<td><p> renderer specifed for issue and field and comment created</p></td>
</tr>
<tr class="odd">
<td><p> comment</p>
<p>org.ofbiz.core.entity .GenericValue</p></td>
<td><p> generic value representing the comment associated with the change</p></td>
<td><p> 3.4</p></td>
<td><p> comment created</p></td>
</tr>
<tr class="even">
<td><p> commentauthor</p>
<p> com.opensymphony.user.User</p></td>
<td><p> the author of the comment</p></td>
<td><p> 3.4</p></td>
<td><p> comment created</p></td>
</tr>
<tr class="odd">
<td><p> timelogged</p>
<p> java.lang.String</p></td>
<td><p> The DateUtils .getDurationPretty value of timelogged from issue or &quot;None&quot; if null</p></td>
<td><p> 3.4</p></td>
<td><p> always present</p></td>
</tr>
<tr class="even">
<td><p> changelog</p>
<p>org.ofbiz.core.entity .GenericValue</p></td>
<td><p> generic value representing the changelog associated with the change</p></td>
<td><p> 3.4</p></td>
<td><p> changelog != null</p></td>
</tr>
<tr class="odd">
<td><p> changelogauthor</p>
<p>com.opensymphony.user.User</p></td>
<td><p> The change log author of the issue</p></td>
<td><p> 3.4</p></td>
<td><p> changelog != null</p></td>
</tr>
<tr class="even">
<td><p> visibilitylevel</p>
<p>java.lang.String</p></td>
<td><p> The security level of the comment</p></td>
<td><p> 3.4</p></td>
<td><p> comment created</p></td>
</tr>
<tr class="odd">
<td><p> i18n</p>
<p>com.atlassian.jira.util.I18nHelper</p></td>
<td><p> Bean that searches for i18n text in JiraWebActionSupport .properties</p></td>
<td><p> 3.4</p></td>
<td><p> always present</p></td>
</tr>
<tr class="even">
<td><p> dateutils</p>
<p>com.atlassian.core.util.DateUtils</p></td>
<td><p>methods for working with dates</p></td>
<td><p> 3.4</p></td>
<td><p> always present</p></td>
</tr>
<tr class="odd">
<td><p> attachments</p>
<p>List( org.ofbiz.core.entity .GenericValue )</p></td>
<td><p> list of generic values that represents the attachments associated with the issue</p></td>
<td><p> 3.4</p></td>
<td><p> always present</p></td>
</tr>
<tr class="even">
<td><p> security</p>
<p>org.ofbiz.core.entity .GenericValue</p></td>
<td><p> generic value representing the security level, if any, associated with this issue</p></td>
<td><p> 3.4</p></td>
<td><p> if( isEnterpriseEdition ) </p></td>
</tr>
<tr class="odd">
<td><p>mailPluginsHelper</p>
<p>com.atlassian.jira.mail .JiraMailPluginsHelperImpl</p></td>
<td><p>provides access to isPluginModuleEnabled()</p></td>
<td><p>3.7</p></td>
<td><p>always present</p></td>
</tr>
</tbody>
</table>

### Email templates

This is the listing of the contents of the Velocity Context used to process email templates.

-   The `TemplateContext` provides access to some general object references for use in the templates - e.g. Utilities,  Change Log details, etc.
-   The `TemplateIssue` provides access to the Issue object - further extended by helper methods for displaying information in rendered HTML format (i.e. environment, description and comments) and users who may not exist in the system. All issue related details can be retrieved from the TemplateIssue.

The following tables lists other objects that are also available within the context:

<table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<thead>
<tr class="header">
<th><p>Velocity Variable &amp;</p>
<p>Jira Object/Value</p></th>
<th><p>Description</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>userutils</p>
<p>com.atlassian.core.user .UserUtils</p></td>
<td><p>collection of utility methods for querying, creating, and removing users</p></td>
</tr>
<tr class="even">
<td><p>velocityhelper</p>
<p>com.atlassian.jira.util .JiraVelocityHelper</p></td>
<td><p>a collection of miscellaneous methods (e.g. urlencode, indentTextBlock, printChangelog</p></td>
</tr>
<tr class="odd">
<td><p>textutils</p>
<p>com.opensymphony.util .TextUtils</p></td>
<td><p>collection of utility functions for manipulating text</p></td>
</tr>
<tr class="even">
<td><p>constantsManager</p>
<p>com.atlassian.jira.config .ConstantsManager</p></td>
<td><p>Jira's constants for priorities, resolutions, issue types, statuses</p></td>
</tr>
<tr class="odd">
<td><p>projectManager</p>
<p>com.atlassian.jira.project .ProjectManager</p></td>
<td><p>management of project entities (e.g. getProjectByName, getComponents, getCategories)</p></td>
</tr>
<tr class="even">
<td><p>customFieldManager</p>
<p>com.atlassian.jira.issue .CustomFieldManager</p></td>
<td><p>functions for working with custom fields</p></td>
</tr>
<tr class="odd">
<td><p>applicationProperties</p>
<p>com.atlassian.jira.config.properties .ApplicationProperties</p></td>
<td><p>access Jira's application properties</p></td>
</tr>
<tr class="even">
<td><p>jirautils</p>
<p>com.atlassian.jira.util .JiraUtils</p></td>
<td><p>a handful of miscellaneous utility methods (e.g. isPublicMode, isSetup)</p></td>
</tr>
<tr class="odd">
<td><p>jirakeyutils</p>
<p>com.atlassian.jira.util .JiraKeyUtils</p></td>
<td><p>methods for working with project keys</p></td>
</tr>
<tr class="even">
<td><p><del>buildutils</del></p>
<p><del>com.atlassian.jira.util .BuildUtils</del></p></td>
<td><p><em>buildutils</em> is no longer available within the context. <em>build</em> variable should be used instead (see below)</p></td>
</tr>
<tr class="odd">
<td><p>build</p>
<p><a href="https://docs.atlassian.com/software/jira/docs/api/latest/com/atlassian/jira/util/BuildUtilsInfo.html" class="external-link">com.atlassian.jira.util .BuildUtilsInfo</a></p></td>
<td><p><strong>Since Jira 6.1</strong> Jira build and edition information</p></td>
</tr>
<tr class="even">
<td><p>jiraUserUtils</p>
<p>com.atlassian.jira.user.util .UserUtil</p></td>
<td><p>user level operations (e.g. getActiveUserCount, getDisplayableNameSafely, addUserToGroup)</p></td>
</tr>
<tr class="odd">
<td><p>mailPluginsHelper</p>
<p>com.atlassian.jira.mail .JiraMailPluginsHelperImpl</p></td>
<td><p>method for checking whether plugins are enabled</p></td>
</tr>
<tr class="even">
<td><p>userformat</p>
<p>com.atlassian.jira.plugin.profile .UserFormatManager</p></td>
<td><p>formatting user names</p></td>
</tr>
<tr class="odd">
<td><p>context</p>
<p><a href="http://docs.atlassian.com/software/jira/docs/api/latest/com/atlassian/jira/mail/TemplateContext.html" class="external-link">com.atlassian.jira.mail .TemplateContext</a></p></td>
<td><p>The Template Context object itself</p></td>
</tr>
<tr class="even">
<td><p>issue</p>
<p><a href="http://docs.atlassian.com/software/jira/docs/api/latest/com/atlassian/jira/mail/TemplateIssue.html" class="external-link">com.atlassian.jira.mail .TemplateIssue</a></p></td>
<td><p>An Issue object with extended helper methods for display - this is the issue for which the email notification event was triggered</p></td>
</tr>
<tr class="odd">
<td><p>params</p>
<p>java.util.Map</p></td>
<td><p>Parameters of the IssueEvent that triggered this email notification</p></td>
</tr>
<tr class="even">
<td><p>remoteUser</p>
<p>com.opensymphony.user.User</p></td>
<td><p>The user who has triggered this event</p></td>
</tr>
<tr class="odd">
<td><p>stringUtils</p>
<p>org.apache.commons.lang .StringUtils</p></td>
<td><p>Utility object providing formatting methods</p></td>
</tr>
<tr class="even">
<td><p>padSize</p>
<p>java.lang.Integer</p></td>
<td><p>Used in text emails for padding the display</p></td>
</tr>
<tr class="odd">
<td><p>timeSpentFieldId</p>
<p>java.lang.String</p></td>
<td><p>The Id of the TimeSpentField - used in changelog for work logged notifications</p></td>
</tr>
<tr class="even">
<td><p>attachments</p>
<p>Collection( org.ofbiz.core.entity.GenericValue )</p></td>
<td><p>Collection of Generic Values that represents the attachments associated with the issue</p></td>
</tr>
<tr class="odd">
<td><p>htmlComment</p>
<p>java.lang.String</p></td>
<td><p>The comment associated with this event as a rendered HTML formatted string. A basic string is returned if the comment cannot be rendered correctly - including links for any referenced Jira issues.</p></td>
</tr>
<tr class="even">
<td><p>comment</p>
<p><a href="http://docs.atlassian.com/software/jira/docs/api/latest/com/atlassian/jira/issue/comments/Comment.html" class="external-link">com.atlassian.jira.issue.comments .Comment</a></p></td>
<td><p>The comment associated with this event. The <a href="http://docs.atlassian.com/software/jira/docs/api/latest/com/atlassian/jira/issue/comments/Comment.html#getBody()" class="external-link">body</a> of the comment will be the raw unprocessed comment as entered by the user (e.g. the raw wiki markup)</p></td>
</tr>
<tr class="odd">
<td><p>commentauthor</p>
<p>com.opensymphony.user.User</p></td>
<td><p>The author of the comment </p></td>
</tr>
<tr class="even">
<td><p>roleVisibilityLevel</p>
<p>java.lang.String</p></td>
<td><p>The comment's project role level (overwritten by worklog)</p></td>
</tr>
<tr class="odd">
<td><p>groupVisibilityLevel</p>
<p>java.lang.String</p></td>
<td><p>The comment's group level (overwritten by worklog)</p></td>
</tr>
<tr class="even">
<td><p>originalhtmlComment</p>
<p>java.lang.String</p></td>
<td><p> </p></td>
</tr>
<tr class="odd">
<td><p>originalcommentauthor</p>
<p>com.opensymphony.user.User</p></td>
<td><p> </p></td>
</tr>
<tr class="even">
<td><p>originalroleVisibilityLevel</p>
<p> </p></td>
<td><p>comment (overwritten by worklog)</p></td>
</tr>
<tr class="odd">
<td><p>originalgroupVisibilityLevel</p>
<p> </p></td>
<td><p>comment (overwritten by worklog)</p></td>
</tr>
<tr class="even">
<td><p>worklog</p>
<p>com.atlassian.jira.issue.worklog .Worklog</p></td>
<td><p> </p></td>
</tr>
<tr class="odd">
<td><p>timeSpentUpdated</p>
<p>java.lang.Boolean</p></td>
<td><p> </p></td>
</tr>
<tr class="even">
<td><p>startDateUpdated</p>
<p>java.lang.Boolean</p></td>
<td><p> </p></td>
</tr>
<tr class="odd">
<td><p>commentUpdated</p>
<p>java.lang.Boolean</p></td>
<td><p> </p></td>
</tr>
<tr class="even">
<td><p>visibilityUpdated</p>
<p>java.lang.Boolean</p></td>
<td><p> </p></td>
</tr>
<tr class="odd">
<td><p>originalworklog</p>
<p>com.atlassian.jira.issue.worklog .Worklog</p></td>
<td><p> </p></td>
</tr>
<tr class="even">
<td><p>changelog</p>
<p>org.ofbiz.core.entity.GenericValue</p></td>
<td><p>The Generic value representing the changelog associated with the change</p></td>
</tr>
<tr class="odd">
<td><p>changelogauthor</p>
<p>com.opensymphony.user.User</p></td>
<td><p>The change log author</p></td>
</tr>
<tr class="even">
<td><p>security</p>
<p>org.ofbiz.core.entity.GenericValue</p></td>
<td><p>The Generic value representing the security level, if any, associated with this issue</p></td>
</tr>
<tr class="odd">
<td><p>rendererManager</p>
<p><a href="http://docs.atlassian.com/software/jira/docs/api/latest/com/atlassian/jira/issue/RendererManager.html" class="external-link">com.atlassian.jira.issue .RendererManager</a></p></td>
<td><p>Allows use of renderers such as wiki renderer. (e.g. $rendererManager.getRenderedContent(&quot;atlassian-wiki-renderer&quot;, &quot;<strong>bold wiki</strong>&quot;, $issue.getIssueRenderContext()))</p></td>
</tr>
<tr class="even">
<td><p>recipient</p>
<p>com.opensymphony.user.User</p></td>
<td><p>The recipient of the email</p></td>
</tr>
<tr class="odd">
<td><p>i18n</p>
<p>com.atlassian.jira.utilI .I18nHelper</p></td>
<td><p>i18n translation helper for the current recipient</p></td>
</tr>
<tr class="even">
<td><p>dateformatter</p>
<p>com.atlassian.jira.web.util .OutlookDate</p></td>
<td><p>Date and time formatter for the current recipient's locale</p></td>
</tr>
<tr class="odd">
<td><p>eventTypeName</p>
<p>java.lang.String</p></td>
<td><p><em>Available from Jira 4.1.</em> The localised event type name. (For example, for the Issue Assigned event, the key <a href="http://mail.event.type.issueassigned.name/" class="external-link">mail.event.type.issueassigned.name</a> will be used.) If a translation is not available, the event type name will be used</p></td>
</tr>
<tr class="even">
<td><p>attachmentsManager</p>
<p>com.atlassian.jira.mail.util .MailAttachmentsManager</p></td>
<td><em>Available from Jira 7.0.3</em>. If this object is present, the mail attachments manager can be used to add attachments to an email and generate Content-IDs for those attachments.</td>
</tr>
</tbody>
</table>

## Related tutorials

The following tutorials provide guidelines and give examples of how to modify Jira's JSPs and templates:

-   [Adding custom fields to email](https://developer.atlassian.com/server/jira/platform/adding-custom-fields-to-email/)
-   [Adding JavaScript to all pages for Google Analytics](https://developer.atlassian.com/server/jira/platform/adding-javascript-to-all-pages-for-google-analytics/)
-   [Creating a custom release notes template containing release comments](https://developer.atlassian.com/server/jira/platform/creating-a-custom-release-notes-template-containing-release-comments/)
-   [Customizing interface based on user's role](https://developer.atlassian.com/server/jira/platform/customizing-interface-based-on-users-role/)
-   [Customizing Jira Excel output](https://developer.atlassian.com/server/jira/platform/customizing-jira-excel-output/)
