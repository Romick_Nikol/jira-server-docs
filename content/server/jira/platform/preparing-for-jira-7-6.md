---
aliases:
- /server/jira/platform/preparing-for-jira-7.6-51921368.html
- /server/jira/platform/preparing-for-jira-7.6-51921368.md
category: devguide
confluence_id: 51921368
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=51921368
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=51921368
date: '2017-12-08'
legacy_title: Preparing for JIRA 7.6
platform: server
product: jira
subcategory: updates
title: Preparing for JIRA 7.6
---
# Preparing for JIRA 7.6

This page covers changes in **JIRA 7.6** that can affect add-on compatibility and functionality. This includes changes to the JIRA platform and the JIRA applications (JIRA Core, JIRA Software, JIRA Service Desk).

As a rule, Atlassian makes every effort to inform add-on developers of known API changes **as far in advance as possible**. Where possible, we attach release targets. Unless otherwise indicated, every change on this page is expected to be released with the first public release of the JIRA Server 7.6 products. We also make release milestones available prior to the release:

-   For JIRA Server, the JIRA development team releases a number of EAP milestones prior to the final release, for customers and developers to keep abreast of upcoming changes. For more information on these releases, see [JIRA EAP Releases](/server/jira/platform/jira-eap-releases-35160569.html).
-   For JIRA Cloud, the JIRA team releases updates to the JIRA Cloud products on a weekly basis. Add-ons that integrate with the JIRA Cloud products via Atlassian Connect should use the JIRA REST API, which is subject to the <a href="https://developer.atlassian.com/display/HOME/Atlassian+REST+API+policy" class="external-link">Atlassian REST API Policy</a>.

We will update this page as development progresses, so please stay tuned for more updates. We also recommend following the JIRA news on [Atlassian Developers blog](https://developer.atlassian.com/blog/categories/jira/) for important announcements and helpful articles.

## Summary

The risk level indicates the level of certainty we have that things will break if you are in the "Affected" column and you don't make the necessary changes.

<table>
<colgroup>
<col style="width: 30%" />
<col style="width: 70%" />
</colgroup>
<thead>
<tr class="header">
<th>Change. Platform/Application</th>
<th>Risk level. Affects</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>Change: Priority schemes (UI changes)</p>
<p>Platform/Application: JIRA Core, JIRA Software</p></td>
<td><p>Risk level: medium</p>
<p>Affects: Add-on developers using APIs to manage priorities in JIRA, and administrators.</p>
<p>With JIRA 7.6, we're introducing priority schemes that change the way priorities are managed. Now, by using priority schemes, you can associate different sets of priorities with your project.</p>
<p>This doesn't change your configuration until you create some schemes and make use of them. After the upgrade, all priorities are moved to the default priority scheme, which works like a global list of priorities (always includes all existing priorities). The default priority scheme can't be edited or deleted.</p>
<p>See <a href="/server/jira/platform/jira-core-7-6-eap-rc-release-notes">Priority schemes - release notes</a>.</p></td>
</tr>
<tr class="even">
<td><p>Change: Priority schemes (API changes)</p>
<p> Platform/Application: JIRA Core, JIRA Software</p>
<p> </p></td>
<td><p>Risk level: medium</p>
<p>Affects: Add-on developers using APIs to manage priorities in JIRA.</p>
<p>Together with priority schemes, we've added and changed some APIs so that plugins can manage them.</p>
<p>See <a href="#priority-schemes-api-changes-for-plugin-developers">API changes for plugin developers</a>.</p></td>
</tr>
<tr class="odd">
<td><p>Change: API changes - experimental API</p>
<p> Platform/Application: JIRA Service Desk</p></td>
<td><p>Risk level:low</p>
<p>Affects: With an upcoming release of JIRA Service Desk, a number of the methods in our API will be moving out of the experimental state and in to standard API state. For most of these components the API will remain unchanged, but for some there will be adjustments which will represent a breaking change. </p>
<p>See <a href="#what-s-changing">API changes - experimental API</a>.</p></td>
</tr>
</tbody>
</table>

## JIRA platform changes

**All changes in JIRA platform are also relevant for JIRA Core and JIRA Software.**

### Priority schemes - UI changes

For more information about managing priority schemes in the UI, see [release notes](/server/jira/platform/jira-core-7-6-eap-rc-release-notes).

### Priority schemes - API changes for plugin developers

If you're a plugin developer, and want your plugin to manage priority schemes, take a look at this list of API changes. We've not only added new APIs that allow plugins to manage priority schemes, but we also changed the existing ones, which will most likely make your plugins incompatible. This is for priority schemes, the APIs for managing priorities don't change, but bear in mind that the current global list of priorities now becomes the default priority scheme, so your plugins might retrieve inaccurate data regarding priorities themselves. 

#### What's new?

We're adding these APIs to allow plugins to manage priority schemes. 

 **Classname** - com.atlassian.jira.issue.fields.config.manager.PrioritySchemeManager

**Method signatures:**

<table>
<colgroup>
<col style="width: 90%" />
<col style="width: 10%" />
</colgroup>
<thead>
<tr class="header">
<th>Method signature</th>
<th>Javadoc</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><pre><code>FieldConfigScheme createWithDefaultMapping(
  String name,
  String description,
  List&lt;String&gt; optionIds
);</code></pre></td>
<td><a href="https://docs.atlassian.com/jira/7.6.0-EAP01/com/atlassian/jira/issue/fields/config/manager/PrioritySchemeManager.html#createWithDefaultMapping-java.lang.String-java.lang.String-java.util.List-" class="external-link">Doc</a></td>
</tr>
<tr class="even">
<td><pre><code>FieldConfigScheme updateWithDefaultMapping(
  FieldConfigScheme fieldConfigScheme,
  List&lt;String&gt; optionIds
);</code></pre></td>
<td><a href="https://docs.atlassian.com/jira/7.6.0-EAP01/com/atlassian/jira/issue/fields/config/manager/PrioritySchemeManager.html#updateWithDefaultMapping-com.atlassian.jira.issue.fields.config.FieldConfigScheme-java.util.List-" class="external-link">Doc</a></td>
</tr>
<tr class="odd">
<td><pre><code>FieldConfig getFieldConfigForDefaultMapping(
  FieldConfigScheme fieldConfigScheme
);</code></pre></td>
<td><a href="https://docs.atlassian.com/jira/7.6.0-EAP01/com/atlassian/jira/issue/fields/config/manager/PrioritySchemeManager.html#getFieldConfigForDefaultMapping-com.atlassian.jira.issue.fields.config.FieldConfigScheme-" class="external-link">Doc</a></td>
</tr>
<tr class="even">
<td><pre><code>void delete(
  FieldConfigScheme fieldConfigScheme
);</code></pre></td>
<td><a href="https://docs.atlassian.com/jira/7.6.0-EAP01/com/atlassian/jira/issue/fields/config/manager/PrioritySchemeManager.html#delete-com.atlassian.jira.issue.fields.config.FieldConfigScheme-" class="external-link">Doc</a></td>
</tr>
<tr class="odd">
<td><pre><code>FieldConfigScheme getScheme(
  Project project
);</code></pre></td>
<td><a href="https://docs.atlassian.com/jira/7.6.0-EAP01/com/atlassian/jira/issue/fields/config/manager/PrioritySchemeManager.html#getScheme-com.atlassian.jira.project.Project-" class="external-link">Doc</a></td>
</tr>
<tr class="even">
<td><pre><code>FieldConfigScheme getScheme(
  Long id
);</code></pre></td>
<td><a href="https://docs.atlassian.com/jira/7.6.0-EAP01/com/atlassian/jira/issue/fields/config/manager/PrioritySchemeManager.html#getScheme-java.lang.Long-" class="external-link">Doc</a></td>
</tr>
<tr class="odd">
<td><pre><code>List&lt;FieldConfigScheme&gt; getAllSchemes();</code></pre></td>
<td><a href="https://docs.atlassian.com/jira/7.6.0-EAP01/com/atlassian/jira/issue/fields/config/manager/PrioritySchemeManager.html#getAllSchemes--" class="external-link">Doc</a></td>
</tr>
<tr class="even">
<td><pre><code>boolean isDefaultScheme(
  FieldConfigScheme fieldConfigScheme
);</code></pre></td>
<td><a href="https://docs.atlassian.com/jira/7.6.0-EAP01/com/atlassian/jira/issue/fields/config/manager/PrioritySchemeManager.html#isDefaultScheme-com.atlassian.jira.issue.fields.config.FieldConfigScheme-" class="external-link">Doc</a></td>
</tr>
<tr class="odd">
<td><pre><code>FieldConfigScheme getDefaultScheme();</code></pre></td>
<td><a href="https://docs.atlassian.com/jira/7.6.0-EAP01/com/atlassian/jira/issue/fields/config/manager/PrioritySchemeManager.html#getDefaultScheme--" class="external-link">Doc</a></td>
</tr>
<tr class="even">
<td><pre><code>String getDefaultOption(
  IssueContext issueContext
);</code></pre></td>
<td><a href="https://docs.atlassian.com/jira/7.6.0-EAP01/com/atlassian/jira/issue/fields/config/manager/PrioritySchemeManager.html#getDefaultOption-com.atlassian.jira.issue.context.IssueContext-" class="external-link">Doc</a></td>
</tr>
<tr class="odd">
<td><pre><code>String getDefaultOption(
	FieldConfig fieldConfig
);</code></pre></td>
<td><a href="https://docs.atlassian.com/jira/7.6.0-EAP01/com/atlassian/jira/issue/fields/config/manager/PrioritySchemeManager.html#getDefaultOption-com.atlassian.jira.issue.fields.config.FieldConfig-" class="external-link">Doc</a></td>
</tr>
<tr class="even">
<td><pre><code>void setDefaultOption(
  FieldConfig fieldConfig,
  String optionId
);</code></pre></td>
<td><a href="https://docs.atlassian.com/jira/7.6.0-EAP01/com/atlassian/jira/issue/fields/config/manager/PrioritySchemeManager.html#setDefaultOption-com.atlassian.jira.issue.fields.config.FieldConfig-java.lang.String-" class="external-link">Doc</a></td>
</tr>
<tr class="odd">
<td><pre><code>List&lt;String&gt; getOptions(
  IssueContext issueContext
);</code></pre></td>
<td><a href="https://docs.atlassian.com/jira/7.6.0-EAP01/com/atlassian/jira/issue/fields/config/manager/PrioritySchemeManager.html#getOptions-com.atlassian.jira.issue.context.IssueContext-" class="external-link">Doc</a></td>
</tr>
<tr class="even">
<td><pre><code>List&lt;String&gt; getOptions(
  FieldConfig fieldConfig
);</code></pre></td>
<td><a href="https://docs.atlassian.com/jira/7.6.0-EAP01/com/atlassian/jira/issue/fields/config/manager/PrioritySchemeManager.html#getOptions-com.atlassian.jira.issue.fields.config.FieldConfig-" class="external-link">Doc</a></td>
</tr>
<tr class="odd">
<td><pre><code>void setOptions(
  FieldConfig fieldConfig,
  List&lt;String&gt; optionIds
);</code></pre></td>
<td><a href="https://docs.atlassian.com/jira/7.6.0-EAP01/com/atlassian/jira/issue/fields/config/manager/PrioritySchemeManager.html#getOptions-com.atlassian.jira.issue.fields.config.FieldConfig-" class="external-link">Doc</a></td>
</tr>
<tr class="even">
<td><pre><code>void addOptionToDefault(
  String optionId
);</code></pre></td>
<td><a href="https://docs.atlassian.com/jira/7.6.0-EAP01/com/atlassian/jira/issue/fields/config/manager/PrioritySchemeManager.html#addOptionToDefault-java.lang.String-" class="external-link">Doc</a></td>
</tr>
<tr class="odd">
<td><pre><code>void removeOptionFromAllSchemes(
  String optionId
);</code></pre></td>
<td><a href="https://docs.atlassian.com/jira/7.6.0-EAP01/com/atlassian/jira/issue/fields/config/manager/PrioritySchemeManager.html#removeOptionFromAllSchemes-java.lang.String-" class="external-link">Doc</a></td>
</tr>
<tr class="even">
<td><pre><code>Collection&lt;FieldConfigScheme&gt; getAllRelatedSchemes(
  String optionId
);</code></pre></td>
<td><a href="https://docs.atlassian.com/jira/7.6.0-EAP01/com/atlassian/jira/issue/fields/config/manager/PrioritySchemeManager.html#getAllRelatedSchemes-java.lang.String-" class="external-link">Doc</a></td>
</tr>
<tr class="odd">
<td><pre><code>void assignProject(
  @Nonnull FieldConfigScheme priorityFieldConfig,
  @Nonnull Project project
);</code></pre></td>
<td><a href="https://docs.atlassian.com/jira/7.6.0-EAP01/com/atlassian/jira/issue/fields/config/manager/PrioritySchemeManager.html#assignProject-com.atlassian.jira.issue.fields.config.FieldConfigScheme-com.atlassian.jira.project.Project-" class="external-link">Doc</a></td>
</tr>
<tr class="even">
<td><pre><code>Set&lt;Project&gt; getProjectsWithScheme(
  @Nonnull FieldConfigScheme fieldConfigScheme
);</code></pre></td>
<td><a href="https://docs.atlassian.com/jira/7.6.0-EAP01/com/atlassian/jira/issue/fields/config/manager/PrioritySchemeManager.html#getProjectsWithScheme-com.atlassian.jira.issue.fields.config.FieldConfigScheme-" class="external-link">Doc</a></td>
</tr>
<tr class="odd">
<td><pre><code>void unassignProject(
  @Nonnull FieldConfigScheme scheme,
  @Nonnull Project project
);</code></pre></td>
<td><a href="https://docs.atlassian.com/jira/7.6.0-EAP01/com/atlassian/jira/issue/fields/config/manager/PrioritySchemeManager.html#unassignProject-com.atlassian.jira.issue.fields.config.FieldConfigScheme-com.atlassian.jira.project.Project-" class="external-link">Doc</a></td>
</tr>
</tbody>
</table>

We're also adding new events for whenever a priority scheme is created, updated, or deleted.

**Classname** - com.atlassian.jira.issue.fields.config.manager.PrioritySchemeManager

<table>
<colgroup>
<col style="width: 90%" />
<col style="width: 10%" />
</colgroup>
<thead>
<tr class="header">
<th>Event</th>
<th>Javadoc</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><pre><code>com
  .atlassian
  .jira
  .event
  .issue
  .fields
  .config
  .manager
  .PrioritySchemeCreatedEvent</code></pre></td>
<td><a href="https://docs.atlassian.com/jira/7.6.0-EAP01/com/atlassian/jira/event/issue/field/config/manager/PrioritySchemeCreatedEvent.html" class="external-link">Doc</a></td>
</tr>
<tr class="even">
<td><pre><code>com
  .atlassian
  .jira
  .event
  .issue
  .fields
  .config
  .manager
  .PrioritySchemeDeletedEvent</code></pre></td>
<td><a href="https://docs.atlassian.com/jira/7.6.0-EAP01/com/atlassian/jira/event/issue/field/config/manager/PrioritySchemeDeletedEvent.html" class="external-link">Doc</a></td>
</tr>
<tr class="odd">
<td><pre><code>com
  .atlassian
  .jira
  .event
  .issue
  .fields
  .config
  .manager
  .PrioritySchemeUpdatedEvent</code></pre></td>
<td><a href="https://docs.atlassian.com/jira/7.6.0-EAP01/com/atlassian/jira/event/issue/field/config/manager/PrioritySchemeUpdatedEvent.html" class="external-link">Doc</a></td>
</tr>
</tbody>
</table>

#### What's changing?

Some APIs also receive additional options, so that priorities are added or removed from priority schemes.

**Classname** - com.atlassian.jira.config.PriorityManager 

| Method signature | Change                                                                                      | Javadoc                                                                                                                                                                                                                |
|------------------|---------------------------------------------------------------------------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `createPriority` | In addition to creating a priority, also adds this priority to the default priority scheme. | <a href="https://docs.atlassian.com/jira/7.6.0-EAP01/com/atlassian/jira/config/PriorityManager.html#createPriority-java.lang.String-java.lang.String-java.lang.String-java.lang.String-" class="external-link">Doc</a> |
| `removePriority` | In addition to removing a priority, also removes this priority from all priority schemes.   | <a href="https://docs.atlassian.com/jira/7.6.0-EAP01/com/atlassian/jira/config/PriorityManager.html#removePriority-java.lang.String-java.lang.String-" class="external-link">Doc</a>                                   |



#### What's deprecated?

These APIs are deprecated, and replaced by the new PrioritySchemeManager class.

##### **com.atlassian.jira.config.PriorityManager**

Method signatures:

-   **setDefaultPriority**  
    Replacement: 

    `com.atlassian.jira.issue.fields.config.manager.PrioritySchemeManager` 

    `#setDefaultOption(FieldConfig, String)`

    <a href="https://docs.atlassian.com/jira/7.6.0-EAP01/com/atlassian/jira/config/PriorityManager.html#setDefaultPriority-java.lang.String-" class="external-link">Javadoc</a>

-   **getDefaultPriority**  
    Replacement: 

    `com.atlassian.jira.issue.fields.config.manager.PrioritySchemeManager`

    `#getDefaultOption(IssueContext)`

    <a href="https://docs.atlassian.com/jira/7.6.0-EAP01/com/atlassian/jira/config/PriorityManager.html#getDefaultPriority--" class="external-link">Javadoc</a>

##### com.atlassian.jira.config.ConstantsManager

Method signatures:

-   **getDefaultPriority**  
    Replacement: 

    `com.atlassian.jira.issue.fields.config.manager.PrioritySchemeManager`

    `#getDefaultOption(com.atlassian.jira.issue.context.IssueContext)`, or

    `com.atlassian.jira.issue.fields.config.manager.PrioritySchemeManager`

    `#getDefaultOption(com.atlassian.jira.issue.fields.config.FieldConfig)`

    <a href="https://docs.atlassian.com/jira/7.6.0-EAP01/com/atlassian/jira/config/ConstantsManager.html#getDefaultPriority--" class="external-link">Javadoc</a>

## JIRA Service Desk changes

JIRA Service Desk has a number of elements of its API which are currently defined as experimental but that is about to change. With the 3.7.0 release a number of components were either deprecated for removal, or updated with details about how they will represent a breaking change in a future release. Well, those changes are coming now in the JIRA Service Desk 3.9.0 release. This release will also be moving the majority of the components in our API out of the experimental state and into a standard public API state. 

#### What's Changing?

A number of components will have breaking changes. The changes are relatively minor and revolve around changes to IDs that will be returning integers rather than longs. This helps us to align with the values that we are currently storing in the database and will stop us having to do a lot of unnecessary type conversion in the future. 

<table>
<colgroup>
<col style="width: 20%" />
<col style="width: 40%" />
<col style="width: 40%" />
</colgroup>
<thead>
<tr class="header">
<th><p>Classname</p></th>
<th><p>Breaking change</p></th>
<th><p>New method signature</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>ServiceDeskService</p></td>
<td><p>The getServiceDeskById method will require an integer rather than a long as the serviceDeskId.</p>
<p><strong>Old method signature:</strong></p>
<p><code>Either&lt;<br>
&nbsp;&nbsp;AnError, ServiceDesk<br>
&gt;<br>
getServiceDeskById(<br>
&nbsp;&nbsp;ApplicationUser user,<br>
&nbsp;&nbsp;long serviceDeskId<br>
);</code></p>
<p><strong>Old javadoc:</strong></p>
<p><a href="https://docs.atlassian.com/jira-servicedesk/3.6.2-REL-0004/com/atlassian/servicedesk/api/ServiceDeskService.html#getServiceDeskById-com.atlassian.jira.user.ApplicationUser-long-" class="external-link">ServiceDeskService: getServiceDeskById</a></p></td>
<td><p><code>Either&lt;<br>
&nbsp;&nbsp;AnError, ServiceDesk<br>
&gt;<br>
getServiceDeskById(<br>
&nbsp;&nbsp;ApplicationUser user,<br>
&nbsp;&nbsp;int serviceDeskId<br>
);</code></p>
<p><strong>New javadoc:</strong></p>
<p><a href="https://docs.atlassian.com/jira-servicedesk/3.9.0-m0124/com/atlassian/servicedesk/api/ServiceDeskService.html#getServiceDeskById-com.atlassian.jira.user.ApplicationUser-int-" class="external-link">ServiceDeskService: getServiceDeskById</a></p></td>
</tr>
<tr class="even">
<td><p>PortalService</p></td>
<td><p>The getPortalForId method will request and integer rather than a long as the portalId.</p>
<p><strong>Old method signature:</strong></p>
<p><code>Either&lt;<br>
&nbsp;&nbsp;AnError, Portal<br>
&gt; <br>
getPortalForId(<br>
&nbsp;&nbsp;ApplicationUser user,<br>
&nbsp;&nbsp;long portalId<br>
);</code></p>
<p><strong>Old javadoc:</strong></p>
<p><a href="https://docs.atlassian.com/jira-servicedesk/3.6.2-REL-0004/com/atlassian/servicedesk/api/portal/PortalService.html#getPortalForId-com.atlassian.jira.user.ApplicationUser-long-" class="external-link">PortalService: getPortalForId</a> </p></td>
<td><p><code>Either&lt;<br>
&nbsp;&nbsp;AnError, Portal<br>
&gt;<br>
getPortalForId(<br>
&nbsp;&nbsp;ApplicationUser user,<br>
&nbsp;&nbsp;int portalId<br>
);</code></p>
<p><strong>New javadoc:</strong></p>
<p><a href="https://docs.atlassian.com/jira-servicedesk/3.9.0-m0124/com/atlassian/servicedesk/api/portal/PortalService.html#getPortalForId-com.atlassian.jira.user.ApplicationUser-int-" class="external-link">PortalService: getPortalForId</a></p></td>
</tr>
<tr class="odd">
<td><p>Queue</p></td>
<td><p>The return value for this method will return an integer rather than a long.</p>
<p><strong>Old method signature:</strong></p>
<p><code>long getId()</code></p>
<p><strong>Old javadoc:</strong></p>
<p><a href="https://docs.atlassian.com/jira-servicedesk/3.6.2-REL-0004/com/atlassian/servicedesk/api/queue/Queue.html#getId--" class="external-link">Queue: getId</a> </p></td>
<td><p><code>int getId()</code></p>
<p><strong>New javadoc:</strong></p>
<p><a href="https://docs.atlassian.com/jira-servicedesk/3.9.0-m0124/com/atlassian/servicedesk/api/queue/Queue.html#getId--" class="external-link">Queue: getId</a></p></td>
</tr>
<tr class="even">
<td></td>
<td><p>The return value for this method will return an integer rather than a long.</p>
<p><strong>Old method signature:</strong></p>
<code>long getServiceDeskId()</code></p>
<p><strong>Old javadoc:</strong></p>
<p><a href="https://docs.atlassian.com/jira-servicedesk/3.6.2-REL-0004/com/atlassian/servicedesk/api/queue/Queue.html#getServiceDeskId--" class="external-link">Queue: getServiceDeskId</a></p></td>
<td><p><code>int getServiceDeskId()</code></p>
<p><strong>New javadoc:</strong></p>
<p><a href="https://docs.atlassian.com/jira-servicedesk/3.9.0-m0124/com/atlassian/servicedesk/api/queue/Queue.html#getServiceDeskId--" class="external-link">Queue: getServiceDeskId</a></p></td>
</tr>
<tr class="odd">
<td><p>QueueQuery</p></td>
<td><p>The return value for this method will return an integer rather than a long.</p>
<p><strong>Old method signature:</strong></p>
<p><code>Optional&lt;Long&gt; queueId();</code></p>
<p><strong>Old javadoc:</strong></p>
<div>
<a href="https://docs.atlassian.com/jira-servicedesk/3.7.0/com/atlassian/servicedesk/api/queue/QueueQuery.html#queueId--" class="external-link">QueueQuery: queueId</a> 
</div></td>
<td><p><code>Optional&lt;Integer&gt; queueId(); </code></p>
<p><strong>New javadoc:</strong></p>
<p> <a href="https://docs.atlassian.com/jira-servicedesk/3.9.0-m0124/com/atlassian/servicedesk/api/queue/QueueQuery.html#queueId--" class="external-link">QueueQuery: queueId</a></p>
<p><br />
</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>The return value for this method will return an integer rather than a long.</p>
<p><strong>Old method signature:</strong></p>
<p><code>long serviceDeskId();</code></p>
<p><strong>Old javadoc:</strong></p>
<p><a href="https://docs.atlassian.com/jira-servicedesk/3.7.0/com/atlassian/servicedesk/api/queue/QueueQuery.html#serviceDeskId--" class="external-link">QueueQuery: serviceDeskId</a></p>
</td>
<td><p><code>int serviceDeskId();</code></p>
<p><strong>New javadoc:</strong></p>
<p><a href="https://docs.atlassian.com/jira-servicedesk/3.9.0-m0124/com/atlassian/servicedesk/api/queue/QueueQuery.html#serviceDeskId--" class="external-link">QueueQuery: serviceDeskId</a></p></td>
</tr>
<tr class="odd">
<td><p>QueueQuery Builder interface</p></td>
<td><p>The serviceDeskID interface will require an integer rather than a long as the serviceDeskId</p>
<p><strong>Old method signature:</strong></p>
<p><code>Builder serviceDeskId(<br>
&nbsp;&nbsp;longserviceDeskId<br>
);</code></p>
<p><strong>Old javadoc:</strong></p>
<a href="https://docs.atlassian.com/jira-servicedesk/3.7.0/com/atlassian/servicedesk/api/queue/QueueQuery.Builder.html#serviceDeskId-long-" class="external-link">QueueQuery.Builder: serviceDeskId</a> </td>
<td><p><code>Builder serviceDeskId(<br>
&nbsp;&nbsp;int serviceDeskId<br>
);</code></p>
<p><strong>New javadoc:</strong></p>
<div>
<a href="https://docs.atlassian.com/jira-servicedesk/3.9.0-m0124/com/atlassian/servicedesk/api/queue/QueueQuery.Builder.html#serviceDeskId-int-" class="external-link">QueueQuery.Builder: serviceDeskId</a>
</div></td>
</tr>
<tr class="even">
<td></td>
<td><p>The queueId interface will require an integer rather than a long as the queueId</p>
<p><strong>Old method signature:</strong></p>
<p><code>Builder queueId(<br>
&nbsp;&nbsp;long queueId<br>
);</code></p>
<p><strong>Old javadoc:</strong></p>
<p><a href="https://docs.atlassian.com/jira-servicedesk/3.7.0/com/atlassian/servicedesk/api/queue/QueueQuery.Builder.html#queueId-long-" class="external-link">QueueQuery.Builder: queueId</a></p></td>
<td><p><code>Builder queueId(<br>
&nbsp;&nbsp;int queueId<br>
);</code>
</p>
<p><strong>New javadoc:</strong></p>
<p><a href="https://docs.atlassian.com/jira-servicedesk/3.9.0-m0124/com/atlassian/servicedesk/api/queue/QueueQuery.Builder.html#queueId-int-" class="external-link">QueueQuery.Builder: queueId</a></p>
<p> </p></td>
</tr>
<tr class="odd">
<td><p>QueueRequestQuery</p></td>
<td><p>The return values for this method will return an integer rather than a long</p>
<p><strong>Old method signature:</strong></p>
<p><code class="sourceCode java"><span class="dt">long</span></code><code class="sourceCode java"><span class="fu">queueId</span>();</code></p>
<p><strong>Old javadoc:</strong></p>
<div>
<a href="https://docs.atlassian.com/jira-servicedesk/3.7.0/com/atlassian/servicedesk/api/queue/QueueRequestQuery.html#queueId--" class="external-link">QueueRequestQuery: queueId</a> 
</div></td>
<td><p><code class="sourceCode java"><span class="dt">int</span></code><code class="sourceCode java"><span class="fu">queueId</span>();</code></p>
<p><strong>New javadoc:</strong></p>
<div>
<a href="https://docs.atlassian.com/jira-servicedesk/3.9.0-m0124/com/atlassian/servicedesk/api/queue/QueueRequestQuery.html#queueId--" class="external-link">QueueRequestQuery: queueId</a>
</div></td>
</tr>
<tr class="even">
<td></td>
<td><p>The return values for this method will return an integer rather than a long</p>
<p><strong>Old method signature:</strong></p>
<p><code>long serviceDeskId();</code></p>
<p><strong>Old javadoc:</strong></p>
<p><a href="https://docs.atlassian.com/jira-servicedesk/3.7.0/com/atlassian/servicedesk/api/queue/QueueRequestQuery.html#serviceDeskId--" class="external-link">QueueRequestQuery: serviceDeskId</a></p></td>
<td>
<p><code>int serviceDeskId();</code></p>
<p><strong>New javadoc:</strong></p>
<p><a href="https://docs.atlassian.com/jira-servicedesk/3.9.0-m0124/com/atlassian/servicedesk/api/queue/QueueRequestQuery.html#serviceDeskId--" class="external-link">QueueRequestQuery: serviceDeskId</a></p>
<div>
<strong><br />
</strong>
</div></td>
</tr>
<tr class="odd">
<td><p>QueueRequestQuery Builder interface</p></td>
<td><p>The serviceDeskID interface will require an integer rather than a long as the serviceDeskId</p>
<p><strong>Old method signature:</strong></p>
<p><code>Builder serviceDeskId(<br>
&nbsp;&nbsp;longserviceDeskId<br>
);</code></p>
<p><strong>Old javadoc:</strong></p>
<div>
<a href="https://docs.atlassian.com/jira-servicedesk/3.7.0/com/atlassian/servicedesk/api/queue/QueueRequestQuery.Builder.html#serviceDeskId-long-" class="external-link">QueueRequestQuery.Builder: serviceDeskId</a> 
</div></td>
<td><p><code>Builder serviceDeskId(<br>
&nbsp;&nbsp;int serviceDeskId<br>
);</code></p>
<p><strong>New javadoc:</strong></p>
<div>
<a href="https://docs.atlassian.com/jira-servicedesk/3.9.0-m0124/com/atlassian/servicedesk/api/queue/QueueRequestQuery.Builder.html#serviceDeskId-int-" class="external-link">QueueRequestQuery.Builder: serviceDeskId</a>
</div></td>
</tr>
<tr class="even">
<td></td>
<td><p>The queueId interface will require an integer rather than a long as the queueId</p>
<p><strong>Old method signature:</strong></p>
<p><code>Builder queueId(<br>
&nbsp;&nbsp;long queueId<br>
);</code></p>
<p><strong>Old javadoc:</strong></p>
<p><a href="https://docs.atlassian.com/jira-servicedesk/3.7.0/com/atlassian/servicedesk/api/queue/QueueRequestQuery.Builder.html#queueId-long-" class="external-link">QueueRequestQuery.Builder: queueId</a></p></td>
<td><p>
<p><code>Builder queueId(<br>
&nbsp;&nbsp;int queueId<br>
);</code>
</p>
<p><strong>New javadoc:</strong></p><a href="https://docs.atlassian.com/jira-servicedesk/3.9.0-m0124/com/atlassian/servicedesk/api/queue/QueueRequestQuery.Builder.html#queueId-int-" class="external-link">QueueRequestQuery.Builder: queueId</a></p>
<div>
<strong><br />
</strong>
</div></td>
</tr>
<tr class="odd">
<td><p>RequestType</p></td>
<td><p>The getPortalId method will return an integer instead of a long.</p>
<p><strong>Old method signature:</strong></p>
<p><code class="sourceCode java"><span class="dt">long</span></code><code class="sourceCode java"><span class="fu">getPortalId</span>();</code></p>
<p><strong>Old javadoc:</strong></p>
<div>
<a href="https://docs.atlassian.com/jira-servicedesk/3.7.0/com/atlassian/servicedesk/api/requesttype/RequestType.html#getPortalId--" class="external-link">RequestType: getPortalId</a> 
</div></td>
<td><p><code class="sourceCode java"><span class="dt">int</span></code><code class="sourceCode java"><span class="fu">getPortalId</span>();</code></p>
<p><strong>New javadoc:</strong></p>
<div>
<a href="https://docs.atlassian.com/jira-servicedesk/3.9.0-m0124/com/atlassian/servicedesk/api/queue/QueueRequestQuery.Builder.html#serviceDeskId-int-" class="external-link">QueueRequestQuery.Builder: serviceDeskId</a>
</div></td>
</tr>
<tr class="even">
<td><p>SlaInformation</p></td>
<td><p>The getId method will return an integer instead of a long.</p>
<p><strong>Old method signature:</strong></p>
<p><code>long getId();</code></p>
<p><strong>Old javadoc:</strong></p>
<div>
<a href="https://docs.atlassian.com/jira-servicedesk/3.7.0/com/atlassian/servicedesk/api/sla/info/SlaInformation.html#getId--" class="external-link">SlaInformation: getId</a> 
</div></td>
<td><p><code>int getId();</code></p>
<p><strong>New javadoc:</strong></p>
<p><a href="https://docs.atlassian.com/jira-servicedesk/3.9.0-m0124/com/atlassian/servicedesk/api/sla/info/SlaInformation.html#getId--" class="external-link">SlaInformation: getId</a></p></td>
</tr>
<tr class="odd">
<td><p>SlaInformationQuery</p></td>
<td><p>The id method will return an integerinstead of a long.</p>
<p><strong>Old method signature:</strong></p>
<p><code>Optional&lt;Long&gt; id();</code></p>
<p><strong>Old javadoc:</strong></p>
<div>
<a href="https://docs.atlassian.com/jira-servicedesk/3.7.0/com/atlassian/servicedesk/api/sla/info/SlaInformationQuery.html#id--" class="external-link">SlaInformationQuery: id</a> 
</div></td>
<td><p><code>Optional&lt;Integer&gt; id();</code></p>
<p><strong>New javadoc:</strong></p>
<div>
<a href="https://docs.atlassian.com/jira-servicedesk/3.9.0-m0124/com/atlassian/servicedesk/api/sla/info/SlaInformationQuery.html#id--" class="external-link">SlaInformationQuery: id</a>
</div>
<p> </p></td>
</tr>
<tr class="even">
<td><p>SlaInformationQuery Builder</p></td>
<td><p>The id interface will require an integer instead of a long.</p>
<p><strong>Old method signature:</strong></p>
<p><code>Builder id(Long slaMetricId);</code></p>
<p><strong>Old javadoc:</strong></p>
<div>
<a href="https://docs.atlassian.com/jira-servicedesk/3.7.0/com/atlassian/servicedesk/api/sla/info/SlaInformationQuery.Builder.html#id-java.lang.Long-" class="external-link">SlaInformationQuery.Builder: id</a> 
</div></td>
<td><p><code>Builder id(Integer slaMetricId);</code></p>
<p><strong>New javadoc:</strong></p>
<p><a href="https://docs.atlassian.com/jira-servicedesk/3.9.0-m0124/com/atlassian/servicedesk/api/sla/info/SlaInformationQuery.Builder.html#id-java.lang.Integer-" class="external-link">SlaInformationQuery.Builder: id</a></p>
<p> </p></td>
</tr>
</tbody>
</table>

#### What's been removed?

As well as the breaking changes listed above, we've also removed a number of existing experimental API that were deprecated previously. This is being done to make the new public API state, both cleaner and more useable.What's been removed? 

The following classes have been removed:

-   `CreateInternalCommentParameters`
-   `CreatePublicCommentParameters`
-   `ServiceDeskCommentParameters`
-   `ValidatedInternalCommentParameters`
-   `ValidatedPublicCommentParameters`
-   `RequestTypeUpdateParameters`
-   `RequestTypeQueryParameters`

In addition, the following methods have been removed. Most of these have equivalents which can now be used as a direct replacement. The ones that don't have been deemed insufficient for making public, as the usage was not recommended.

<table>
<colgroup>
<col style="width: 40%" />
<col style="width: 40%" />
<col style="width: 20%" />
</colgroup>
<thead>
<tr class="header">
<th><p>Classname</p></th>
<th><p>Method signature</p></th>
<th><p>Replacement</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>ServiceDeskCommentService</p></td>
<td><p><code>Either&lt;<br>
&nbsp;&nbsp;AnError,<br>
&nbsp;&nbsp;ValidatedPublicCommentParameters<br>
&gt;<br>
validatePublicComment(<br>
&nbsp;&nbsp;@Nonnull<br>
&nbsp;&nbsp;&nbsp;&nbsp;ApplicationUser user,<br>
&nbsp;&nbsp;@Nonnull<br>
&nbsp;&nbsp;&nbsp;&nbsp;CreatePublicCommentParameters<br>
&nbsp;&nbsp;&nbsp;&nbsp;commentInternalParameters<br>
);</code></p>
<p><strong>Old javadoc:</strong></p>
<p><a href="https://docs.atlassian.com/jira-servicedesk/3.7.0/com/atlassian/servicedesk/api/comment/ServiceDeskCommentService.html#validatePublicComment-com.atlassian.jira.user.ApplicationUser-com.atlassian.servicedesk.api.comment.CreatePublicCommentParameters-" class="external-link">ServiceDeskCommentService: validatePublicComment</a> </p></td>
<td><p><code>#createServiceDeskComment(<br>
&nbsp;&nbsp;ApplicationUser,<br>
&nbsp;&nbsp;ServiceDeskCommentCreateParameters<br>
)</code></p>
<p><strong>New javadoc:</strong></p>
<p><a href="https://docs.atlassian.com/jira-servicedesk/3.9.0-m0124/com/atlassian/servicedesk/api/comment/ServiceDeskCommentService.html#createServiceDeskComment-com.atlassian.jira.user.ApplicationUser-com.atlassian.servicedesk.api.comment.ServiceDeskCommentCreateParameters-" class="external-link">ServiceDeskCommentService: createServiceDeskComment</a></p></td>
</tr>
<tr class="even">
<td><p><code>Either&lt;<br>
&nbsp;&nbsp;AnError,<br>
&nbsp;&nbsp;ValidatedInternalCommentParameters<br>
&gt;<br>
validateInternalComment(<br>
&nbsp;&nbsp;@Nonnull<br>
&nbsp;&nbsp;&nbsp;&nbsp;ApplicationUser user,<br>
&nbsp;&nbsp;@Nonnull<br>
&nbsp;&nbsp;&nbsp;&nbsp;CreateInternalCommentParameters<br>
&nbsp;&nbsp;&nbsp;&nbsp;commentPublicParameters<br>
);</code></p></td>
<td><p><a href="https://docs.atlassian.com/jira-servicedesk/3.7.0/com/atlassian/servicedesk/api/comment/ServiceDeskCommentService.html#validateInternalComment-com.atlassian.jira.user.ApplicationUser-com.atlassian.servicedesk.api.comment.CreateInternalCommentParameters-" class="external-link">ServiceDeskCommentService: validateInternalComment</a></p>
<p><strong>Old javadoc:</strong></p>
<p><code>
#createServiceDeskComment(<br>
&nbsp;&nbsp;ApplicationUser,<br>
&nbsp;&nbsp;ServiceDeskCommentCreateParameters<br>
)</code></p></td>
<td><p><a href="https://docs.atlassian.com/jira-servicedesk/3.9.0-m0124/com/atlassian/servicedesk/api/comment/ServiceDeskCommentService.html#createServiceDeskComment-com.atlassian.jira.user.ApplicationUser-com.atlassian.servicedesk.api.comment.ServiceDeskCommentCreateParameters-" class="external-link">ServiceDeskCommentService: createServiceDeskComment</a></p></td>
</tr>
<tr class="odd">
<td><p><code>Either&lt;<br>
&nbsp;&nbsp;AnError,<br>
&nbsp;&nbsp;ServiceDeskComment<br>
&gt;<br>
createPublicComment(<br>
&nbsp;&nbsp;@Nonnull<br>
&nbsp;&nbsp;&nbsp;&nbsp;ApplicationUser<br>
&nbsp;&nbsp;&nbsp;&nbsp;user,<br>
&nbsp;&nbsp;@Nonnull<br>
&nbsp;&nbsp;&nbsp;&nbsp;ValidatedPublicCommentParameters<br>
&nbsp;&nbsp;&nbsp;&nbsp;commentParameters<br>
);</code></p></td>
<td><p><a href="https://docs.atlassian.com/jira-servicedesk/3.7.0/com/atlassian/servicedesk/api/comment/ServiceDeskCommentService.html#createPublicComment-com.atlassian.jira.user.ApplicationUser-com.atlassian.servicedesk.api.comment.ValidatedPublicCommentParameters-" class="external-link">ServiceDeskCommentService: createPublicComment</a></p>
<p><strong>Old javadoc:</strong></p>
<p><code>#createServiceDeskComment(<br>
&nbsp;&nbsp;ApplicationUser,<br>
&nbsp;&nbsp;ServiceDeskCommentCreateParameters<br>
)</code></p></td>
<td><p><a href="https://docs.atlassian.com/jira-servicedesk/3.9.0-m0124/com/atlassian/servicedesk/api/comment/ServiceDeskCommentService.html#createServiceDeskComment-com.atlassian.jira.user.ApplicationUser-com.atlassian.servicedesk.api.comment.ServiceDeskCommentCreateParameters-" class="external-link">ServiceDeskCommentService: createServiceDeskComment</a></p></td>
</tr>
<tr class="even">
<td><p><code>Either&lt;<br>
&nbsp;&nbsp;AnError,<br>
&nbsp;&nbsp;ServiceDeskComment<br>
&gt;<br>
createInternalComment(<br>
&nbsp;&nbsp;@Nonnull<br>
&nbsp;&nbsp;&nbsp;&nbsp;ApplicationUser user,<br>
&nbsp;&nbsp;@Nonnull<br>
&nbsp;&nbsp;&nbsp;&nbsp;ValidatedInternalCommentParameters<br>
&nbsp;&nbsp;&nbsp;&nbsp;commentParameters<br>
);</code></p></td>
<td><p><a href="https://docs.atlassian.com/jira-servicedesk/3.7.0/com/atlassian/servicedesk/api/comment/ServiceDeskCommentService.html#createInternalComment-com.atlassian.jira.user.ApplicationUser-com.atlassian.servicedesk.api.comment.ValidatedInternalCommentParameters-" class="external-link">ServiceDeskCommentService: createInternalComment</a></p>
<p><strong>Old javadoc:</strong></p>
<p><code>#createServiceDeskComment(<br>
&nbsp;&nbsp;ApplicationUser,<br>
&nbsp;&nbsp;ServiceDeskCommentCreateParameters<br>
)</code></p></td>
<td><p><a href="https://docs.atlassian.com/jira-servicedesk/3.9.0-m0124/com/atlassian/servicedesk/api/comment/ServiceDeskCommentService.html#createServiceDeskComment-com.atlassian.jira.user.ApplicationUser-com.atlassian.servicedesk.api.comment.ServiceDeskCommentCreateParameters-" class="external-link">ServiceDeskCommentService: createServiceDeskComment</a></p></td>
</tr>
<tr class="odd">
<td><p>CustomerContextService</p></td>
<td><p><code>&lt;T&gt; T inCustomerContext(<br>
&nbsp;&nbsp;Callable&lt;T&gt; callable<br>
);</code></p>
<p><strong>Old javadoc:</strong></p>
<div>
<a href="https://docs.atlassian.com/jira-servicedesk/3.7.0/com/atlassian/servicedesk/api/customer/CustomerContextService.html#inCustomerContext-java.util.concurrent.Callable-" class="external-link">CustomerContextService: inCustomerContext</a> 
</div></td>
<td><p><code>#runInCustomerContext(<br>
&nbsp;&nbsp;NoExceptionsCallable<br>
)</code></p>
<p><strong>New javadoc:</strong></p>
<div>
<a href="https://docs.atlassian.com/jira-servicedesk/3.9.0-m0124/com/atlassian/servicedesk/api/customer/CustomerContextService.html#runInCustomerContext-com.atlassian.servicedesk.api.customer.NoExceptionsCallable-" class="external-link">CustomerContextService: runInCustomerContext</a> 
</div></td>
</tr>
<tr class="even">
<td><p><code>void inCustomerContext(<br>
&nbsp;&nbsp;Runnable runnable<br>
);</code></p></td>
<td><p><a href="https://docs.atlassian.com/jira-servicedesk/3.7.0/com/atlassian/servicedesk/api/customer/CustomerContextService.html#inCustomerContext-java.lang.Runnable-" class="external-link">CustomerContextService: inCustomerContext</a></p>
<p><strong>Old javadoc:</strong></p>
<p><code>#runInCustomerContext(<br>
&nbsp;&nbsp;Runnable<br>
)</code></p></td>
<td><p><a href="https://docs.atlassian.com/jira-servicedesk/3.9.0-m0124/com/atlassian/servicedesk/api/customer/CustomerContextService.html#runInCustomerContext-java.lang.Runnable-" class="external-link">CustomerContextService: runInCustomerContext</a></p></td>
</tr>
<tr class="odd">
<td><p><code>&lt;T&gt; T outOfCustomerContext(<br>
&nbsp;&nbsp;Callable&lt;T&gt; callable<br>
);</code></p></td>
<td><p><a href="https://docs.atlassian.com/jira-servicedesk/3.7.0/com/atlassian/servicedesk/api/customer/CustomerContextService.html#outOfCustomerContext-java.util.concurrent.Callable-" class="external-link">CustomerContextService: outOfCustomerContext</a></p>
<p><strong>Old javadoc:</strong></p>
<p><code>#runOutOfCustomerContext(<br>
&nbsp;&nbsp;NoExceptionsCallable<br>
)</code></p></td>
<td><p><a href="https://docs.atlassian.com/jira-servicedesk/3.9.0-m0124/com/atlassian/servicedesk/api/customer/CustomerContextService.html#runOutOfCustomerContext-com.atlassian.servicedesk.api.customer.NoExceptionsCallable-" class="external-link">CustomerContextService: runOutOfCustomerContext</a></p></td>
</tr>
<tr class="even">
<td><p><code>void outOfCustomerContext(<br>
&nbsp;&nbsp;Runnable runnable)<br>
;</code></p></td>
<td><p><a href="https://docs.atlassian.com/jira-servicedesk/3.7.0/com/atlassian/servicedesk/api/customer/CustomerContextService.html#outOfCustomerContext-java.lang.Runnable-" class="external-link">CustomerContextService: outOfCustomerContext</a></p>
<p><strong>Old javadoc:</strong></p></td>
<td><p><a href="https://docs.atlassian.com/jira-servicedesk/3.9.0-m0124/com/atlassian/servicedesk/api/customer/CustomerContextService.html#runOutOfCustomerContext-java.lang.Runnable-" class="external-link">CustomerContextService: runOutOfCustomerContext</a></p></td>
</tr>
<tr class="odd">
<td><p>ServiceDeskLicence</p></td>
<td><p><code>String getLicenseType();</code></p>
<p><strong>Old javadoc:</strong></p>
<p><code>#runOutOfCustomerContext(<br>
&nbsp;&nbsp;Runnable<br>
)</code></p></td>
<td><p>This method is not applicable anymore. It will always be ABP</code></p></td>
</tr>
<tr class="even">
<td><p>ServiceDeskCustomerRequestService</p></td>
<td><p><code>Either&lt;<br>
&nbsp;&nbsp;AnError,<br>
&nbsp;&nbsp;CustomerRequest<br>
&gt;<br>
getRequestForKey(<br>
&nbsp;&nbsp;@Nullable<br>
&nbsp;&nbsp;&nbsp;&nbsp;ApplicationUser<br>
&nbsp;&nbsp;&nbsp;&nbsp;user,<br>
&nbsp;&nbsp;@Nonnull<br>
&nbsp;&nbsp;&nbsp;&nbsp;String<br>
&nbsp;&nbsp;&nbsp;&nbsp;issueKey<br>
);</code></p>
<p><strong>Old javadoc:</strong></p>
<p>
<a href="https://docs.atlassian.com/jira-servicedesk/3.7.0/com/atlassian/servicedesk/api/request/ServiceDeskCustomerRequestService.html#getRequestForKey-com.atlassian.jira.user.ApplicationUser-java.lang.String-" class="external-link">ServiceDeskCustomerRequestService: getRequestForKey</a> 
</p></td>
<td><p><code>#getCustomerRequests(<br>
&nbsp;&nbsp;ApplicationUser,<br>
&nbsp;&nbsp;CustomerRequestQuery<br>
)</code></p>
<p><strong>New javadoc:</strong></p>
<a href="https://docs.atlassian.com/jira-servicedesk/3.9.0-m0124/com/atlassian/servicedesk/api/request/ServiceDeskCustomerRequestService.html#getCustomerRequests-com.atlassian.jira.user.ApplicationUser-com.atlassian.servicedesk.api.request.CustomerRequestQuery-" class="external-link">ServiceDeskCustomerRequestService: getCustomerRequests</a></td>
</tr>
<tr class="odd">
<td><p><code>Either&lt;<br>
&nbsp;&nbsp;AnError,<br>
&nbsp;&nbsp;CustomerRequest<br>
&gt;<br>
getRequestForIssue(<br>
&nbsp;&nbsp;@Nullable<br>
&nbsp;&nbsp;&nbsp;&nbsp;ApplicationUser<br>
&nbsp;&nbsp;&nbsp;&nbsp;user,<br>
&nbsp;&nbsp;@Nonnull<br>
&nbsp;&nbsp;&nbsp;&nbsp;Issue<br>
&nbsp;&nbsp;&nbsp;&nbsp;issue<br>
);</code></p></td>
<td><p><a href="https://docs.atlassian.com/jira-servicedesk/3.7.0/com/atlassian/servicedesk/api/request/ServiceDeskCustomerRequestService.html#getRequestForIssue-com.atlassian.jira.user.ApplicationUser-com.atlassian.jira.issue.Issue-" class="external-link">ServiceDeskCustomerRequestService: getRequestForIssue</a></p>
<p><strong>Old javadoc:</strong></p>
<p><code>#getCustomerRequests(<br>
&nbsp;&nbsp;ApplicationUser,<br>
&nbsp;&nbsp;CustomerRequestQuery<br>
)</code></p></td>
<td><p><a href="https://docs.atlassian.com/jira-servicedesk/3.9.0-m0124/com/atlassian/servicedesk/api/request/ServiceDeskCustomerRequestService.html#getCustomerRequests-com.atlassian.jira.user.ApplicationUser-com.atlassian.servicedesk.api.request.CustomerRequestQuery-" class="external-link">ServiceDeskCustomerRequestService: getCustomerRequests</a></p></td>
</tr>
<tr class="even">
<td><p><code>RequestTypeUpdateParameters<br>
&nbsp;&nbsp;.Builder<br>
&nbsp;&nbsp;&nbsp;&nbsp;requestTypeUpdateParametersBuilder();</code></p></td>
<td><p><a href="https://docs.atlassian.com/jira-servicedesk/3.7.0/com/atlassian/servicedesk/api/request/ServiceDeskCustomerRequestService.html#requestTypeUpdateParametersBuilder--" class="external-link">ServiceDeskCustomerRequestService: requestTypeUpdateParametersBuilder</a></p>
<p><strong>Old javadoc:</strong></p>
<p><code>#newUpdateBuilder()</code></p></td>
<td><p><a href="https://docs.atlassian.com/jira-servicedesk/3.9.0-m0124/com/atlassian/servicedesk/api/request/ServiceDeskCustomerRequestService.html#newUpdateBuilder--" class="external-link">ServiceDeskCustomerRequestService: newUpdateBuilder</a></p></td>
</tr>
<tr class="odd">
<td><p><code>Either&lt;<br>
&nbsp;&nbsp;AnError,<br>
&nbsp;&nbsp;CustomerRequest<br>
&gt; (<br>
&nbsp;&nbsp;@Nullable<br>
&nbsp;&nbsp;&nbsp;&nbsp;ApplicationUser user,<br>
&nbsp;&nbsp;RequestTypeUpdateParameters<br>
&nbsp;&nbsp;&nbsp;&nbsp;requestTypeUpdateParameters<br>
);</code></p></td>
<td><p><a href="https://docs.atlassian.com/jira-servicedesk/3.7.0/com/atlassian/servicedesk/api/request/ServiceDeskCustomerRequestService.html#updateRequestType-com.atlassian.jira.user.ApplicationUser-com.atlassian.servicedesk.api.request.RequestTypeUpdateParameters-" class="external-link">ServiceDeskCustomerRequestService: updateRequestType</a></p>
<p><strong>Old javadoc:</strong></p>
<p><code>#updateCustomerRequest(<br>
&nbsp;&nbsp;ApplicationUser,<br>
&nbsp;&nbsp;CustomerRequestUpdateParameters<br>
)</code></p></td>
<td><p><a href="https://docs.atlassian.com/jira-servicedesk/3.9.0-m0124/com/atlassian/servicedesk/api/request/ServiceDeskCustomerRequestService.html#updateCustomerRequest-com.atlassian.jira.user.ApplicationUser-com.atlassian.servicedesk.api.request.CustomerRequestUpdateParameters-" class="external-link">ServiceDeskCustomerRequestService: updateCustomerRequest</a></p></td>
</tr>
<tr class="even">
<td><p><code>Either&lt;<br>
&nbsp;&nbsp;AnError,<br>
&nbsp;&nbsp;CustomerRequest<br>
&gt;<br>
getRequestForIssueOverrideSecurity(<br>
&nbsp;&nbsp;Issue issue<br>
);</code></p></td>
<td><p><a href="https://docs.atlassian.com/jira-servicedesk/3.7.0/com/atlassian/servicedesk/api/request/ServiceDeskCustomerRequestService.html#getRequestForIssueOverrideSecurity-com.atlassian.jira.issue.Issue-" class="external-link">ServiceDeskCustomerRequestService: getRequestForIssueOverrideSecurity</a></p>
<p><strong>Old javadoc:</strong></p>
<p><code>#getCustomerRequests(<br>
&nbsp;&nbsp;ApplicationUser,<br>
&nbsp;&nbsp;CustomerRequestQuery<br>
)</code></p></td>
<td><p><a href="https://docs.atlassian.com/jira-servicedesk/3.9.0-m0124/com/atlassian/servicedesk/api/request/ServiceDeskCustomerRequestService.html#getCustomerRequests-com.atlassian.jira.user.ApplicationUser-com.atlassian.servicedesk.api.request.CustomerRequestQuery-" class="external-link">ServiceDeskCustomerRequestService: getCustomerRequests</a></p></td>
</tr>
<tr class="odd">
<td><p><code>Either&lt;<br>
&nbsp;&nbsp;AnError,<br>
&nbsp;&nbsp;CustomerRequest<br>
&gt;<br>
getRequestForKeyOverrideSecurity(<br>
&nbsp;&nbsp;String issueKey<br>
);</code></p></td>
<td><p><a href="https://docs.atlassian.com/jira-servicedesk/3.7.0/com/atlassian/servicedesk/api/request/ServiceDeskCustomerRequestService.html#getRequestForKeyOverrideSecurity-java.lang.String-" class="external-link">ServiceDeskCustomerRequestService: getRequestForKeyOverrideSecurity</a></p>
<p><strong>Old javadoc:</strong></p>
<p><code>#getCustomerRequests(<br>
&nbsp;&nbsp;ApplicationUser,<br>
&nbsp;&nbsp;CustomerRequestQuery<br>
)</code></p></td>
<td><p><a href="https://docs.atlassian.com/jira-servicedesk/3.9.0-m0124/com/atlassian/servicedesk/api/request/ServiceDeskCustomerRequestService.html#getCustomerRequests-com.atlassian.jira.user.ApplicationUser-com.atlassian.servicedesk.api.request.CustomerRequestQuery-" class="external-link">ServiceDeskCustomerRequestService: getCustomerRequests</a></p></td>
</tr>
<tr class="even">
<td><p>RequestTypeGroup</p></td>
<td><p><code>RequestTypeGroup withOrder(<br>
&nbsp;&nbsp;Option&lt;Integer&gt; order<br>
);</code></p>
<p><strong>Old javadoc:</strong></p>
<div>
<a href="https://docs.atlassian.com/jira-servicedesk/3.7.0/com/atlassian/servicedesk/api/requesttype/RequestTypeGroup.html#withOrder-com.atlassian.fugue.Option-" class="external-link">RequestTypeGroup: withOrder</a> 
</div></td>
<td><p>This deprecated and Experimental API has been removed.</p></td>
</tr>
<tr class="odd">
<td><p>RequestTypeService</p></td>
<td><p><code>Either&lt;<br>
&nbsp;&nbsp;AnError,<br>
&nbsp;&nbsp;List&lt;<br>
&nbsp;&nbsp;&nbsp;&nbsp;RequestType<br>
&nbsp;&nbsp;&gt;<br>
&gt;<br>
getRequestTypes(<br>
&nbsp;&nbsp;@Nonnull<br>
&nbsp;&nbsp;&nbsp;&nbsp;ApplicationUser user,<br>
&nbsp;&nbsp;@Nonnull<br>
&nbsp;&nbsp;&nbsp;&nbsp;RequestTypeQueryParameters<br>
&nbsp;&nbsp;&nbsp;&nbsp;requestTypeQueryParameters<br>
);</code></p>
<p><strong>Old javadoc:</strong></p>
<div>
<a href="https://docs.atlassian.com/jira-servicedesk/3.7.0/com/atlassian/servicedesk/api/requesttype/RequestTypeService.html#getRequestTypes-com.atlassian.jira.user.ApplicationUser-com.atlassian.servicedesk.api.requesttype.RequestTypeQueryParameters-" class="external-link">RequestTypeService: getRequestTypes</a> 
</div></td>
<td><p><code>#getRequestTypes(<br>
&nbsp;&nbsp;ApplicationUser,<br>
&nbsp;&nbsp;RequestTypeQuery<br>
)</code></p>
<p><strong>New javadoc:</strong></p>
<p>
<a href="https://docs.atlassian.com/jira-servicedesk/3.9.0-m0124/com/atlassian/servicedesk/api/requesttype/RequestTypeService.html#getRequestTypes-com.atlassian.jira.user.ApplicationUser-com.atlassian.servicedesk.api.requesttype.RequestTypeQuery-" class="external-link">RequestTypeService: getRequestTypes</a> 
</p></td>
</tr>
<tr class="even">
<td><p><code>RequestTypeQueryParameters<br>
&nbsp;&nbsp;.Builder queryBuilder();</code></p></td>
<td><p><a href="https://docs.atlassian.com/jira-servicedesk/3.7.0/com/atlassian/servicedesk/api/requesttype/RequestTypeService.html#queryBuilder--" class="external-link">RequestTypeService: queryBuilder</a></p>
<p><strong>Old javadoc:</strong></p>
<p><code>#newQueryBuilder()</code></p></td>
<td><p><a href="https://docs.atlassian.com/jira-servicedesk/3.9.0-m0124/com/atlassian/servicedesk/api/requesttype/RequestTypeService.html#newQueryBuilder--" class="external-link">RequestTypeService: newQueryBuilder</a></p></td>
</tr>
<tr class="odd">
<td><p><code>Either&lt;<br>
&nbsp;&nbsp;AnError,<br>
&nbsp;&nbsp;RequestType<br>
&gt;<br>
getRequestTypeForRequest(<br>
&nbsp;&nbsp;@Nonnull<br>
&nbsp;&nbsp;&nbsp;&nbsp;ApplicationUser user,<br>
&nbsp;&nbsp;@Nonnull<br>
&nbsp;&nbsp;&nbsp;&nbsp;CustomerRequest<br>
&nbsp;&nbsp;&nbsp;&nbsp;customerRequest<br>
);</code></p></td>
<td><p><a href="https://docs.atlassian.com/jira-servicedesk/3.7.0/com/atlassian/servicedesk/api/requesttype/RequestTypeService.html#getRequestTypeForRequest-com.atlassian.jira.user.ApplicationUser-com.atlassian.servicedesk.api.request.CustomerRequest-" class="external-link">RequestTypeService: getRequestTypeForRequest</a></p>
<p><strong>Old javadoc:</strong></p>
<p><code>#getRequestTypes(<br>
&nbsp;&nbsp;ApplicationUser,<br>
&nbsp;&nbsp;RequestTypeQuery<br>
)</code></p></td>
<td><p><a href="https://docs.atlassian.com/jira-servicedesk/3.9.0-m0124/com/atlassian/servicedesk/api/requesttype/RequestTypeService.html#getRequestTypes-com.atlassian.jira.user.ApplicationUser-com.atlassian.servicedesk.api.requesttype.RequestTypeQuery-" class="external-link">RequestTypeService: getRequestTypes</a></p></td>
</tr>
<tr class="even">
<td><p><code>Either&lt;<br>
&nbsp;&nbsp;AnError,<br>
&nbsp;&nbsp;RequestType<br>
&gt;<br>
getRequestTypeForIssue(<br>
&nbsp;&nbsp;@Nonnull<br>
&nbsp;&nbsp;&nbsp;&nbsp;ApplicationUser user,<br>
&nbsp;&nbsp;@Nonnull Issue issue<br>
);</code></p></td>
<td><p><a href="https://docs.atlassian.com/jira-servicedesk/3.7.0/com/atlassian/servicedesk/api/requesttype/RequestTypeService.html#getRequestTypeForIssue-com.atlassian.jira.user.ApplicationUser-com.atlassian.jira.issue.Issue-" class="external-link">RequestTypeService: getRequestTypeForIssue</a></p>
<p><strong>Old javadoc:</strong></p>
<p><code>#getRequestTypes(<br>
&nbsp;&nbsp;ApplicationUser,<br>
&nbsp;&nbsp;RequestTypeQuery<br>
)</code></p></td>
<td><p><a href="https://docs.atlassian.com/jira-servicedesk/3.9.0-m0124/com/atlassian/servicedesk/api/requesttype/RequestTypeService.html#getRequestTypes-com.atlassian.jira.user.ApplicationUser-com.atlassian.servicedesk.api.requesttype.RequestTypeQuery-" class="external-link">RequestTypeService: getRequestTypes</a></p></td>
</tr>
<tr class="odd">
<td><p><code>Either&lt;<br>
&nbsp;&nbsp;AnError,<br>
&nbsp;&nbsp;List&lt;<br>
&nbsp;&nbsp;&nbsp;&nbsp;nRequestType<br>
&nbsp;&nbsp;&gt;<br>
&gt;<br>
getAllRequestTypes(<br>
&nbsp;&nbsp;ApplicationUser user<br>
);</code></p></td>
<td><p><a href="https://docs.atlassian.com/jira-servicedesk/3.7.0/com/atlassian/servicedesk/api/requesttype/RequestTypeService.html#getAllRequestTypes-com.atlassian.jira.user.ApplicationUser-" class="external-link">RequestTypeService: getAllRequestTypes</a></p>
<p><strong>Old javadoc:</strong></p>
<p><code>#getRequestTypes(<br>
&nbsp;&nbsp;ApplicationUser,<br>
&nbsp;&nbsp;RequestTypeQuery<br>
)</code></p></td>
<td><p><a href="https://docs.atlassian.com/jira-servicedesk/3.9.0-m0124/com/atlassian/servicedesk/api/requesttype/RequestTypeService.html#getRequestTypes-com.atlassian.jira.user.ApplicationUser-com.atlassian.servicedesk.api.requesttype.RequestTypeQuery-" class="external-link">RequestTypeService: getRequestTypes</a></p></td>
</tr>
<tr class="even">
<td><p><code>Either&lt;<br>
&nbsp;&nbsp;AnError,<br>
&nbsp;&nbsp;RequestType<br>
&gt;<br>
getRequestTypeForRequestOverrideSecurity(<br>
&nbsp;&nbsp;@Nonnull<br>
&nbsp;&nbsp;&nbsp;&nbsp;CustomerRequest<br>
&nbsp;&nbsp;&nbsp;&nbsp;customerRequest<br>
);</code></p></td>
<td><p><a href="https://docs.atlassian.com/jira-servicedesk/3.7.0/com/atlassian/servicedesk/api/requesttype/RequestTypeService.html#getRequestTypeForRequestOverrideSecurity-com.atlassian.servicedesk.api.request.CustomerRequest-" class="external-link">RequestTypeService: getRequestTypeForRequestOverrideSecurity</a></p>
<p><strong>Old javadoc:</strong></p>
<div>
<strong><br />
</strong>
</div></td>
<td><p><a href="https://docs.atlassian.com/jira-servicedesk/3.9.0-m0124/com/atlassian/servicedesk/api/requesttype/RequestTypeService.html#getRequestTypes-com.atlassian.jira.user.ApplicationUser-com.atlassian.servicedesk.api.requesttype.RequestTypeQuery-" class="external-link">RequestTypeService: getRequestTypes</a></p></td>
</tr>
<tr class="odd">
<td><p><code>Either&lt;<br>
&nbsp;&nbsp;AnError,<br>
&nbsp;&nbsp;RequestType<br>
&gt;<br>
getRequestTypeForIssueOverrideSecurity(<br>
&nbsp;&nbsp;@Nonnull Issue issue<br>
);</code></p></td>
<td><p><a href="https://docs.atlassian.com/jira-servicedesk/3.7.0/com/atlassian/servicedesk/api/requesttype/RequestTypeService.html#getRequestTypeForIssueOverrideSecurity-com.atlassian.jira.issue.Issue-" class="external-link">RequestTypeService: getRequestTypeForIssueOverrideSecurity</a></p>
<p><strong>Old javadoc:</strong></p>
<p><code>#getRequestTypes(<br>
&nbsp;&nbsp;ApplicationUser,<br>
&nbsp;&nbsp;RequestTypeQuery<br>
)</code></p></td>
<td><p><a href="https://docs.atlassian.com/jira-servicedesk/3.9.0-m0124/com/atlassian/servicedesk/api/requesttype/RequestTypeService.html#getRequestTypes-com.atlassian.jira.user.ApplicationUser-com.atlassian.servicedesk.api.requesttype.RequestTypeQuery-" class="external-link">RequestTypeService: getRequestTypes</a></p></td>
</tr>
<tr class="even">
<td><p><code>Either&lt;<br>
&nbsp;&nbsp;AnError,<br>
&nbsp;&nbsp;RequestType<br>
&gt;<br>
createRequestType(<br>
&nbsp;&nbsp;long projectId,<br>
&nbsp;&nbsp;int issueTypeId,<br>
&nbsp;&nbsp;name,<br>
&nbsp;&nbsp;description,<br>
&nbsp;&nbsp;long iconId,<br>
&nbsp;&nbsp;Integer groupIds<br>
);</code></p></td>
<td><p><a href="https://docs.atlassian.com/jira-servicedesk/3.7.0/com/atlassian/servicedesk/api/requesttype/RequestTypeService.html#createRequestType-long-int-java.lang.String-java.lang.String-long-java.util.List-" class="external-link">RequestTypeService: createRequestType</a></p>
<p><strong>Old javadoc:</strong></p>
<p><code>#createRequestType(<br>
&nbsp;&nbsp;ApplicationUser,<br>
&nbsp;&nbsp;RequestTypeCreateParameters<br>
)</code></p></td>
<td><p><a href="https://docs.atlassian.com/jira-servicedesk/3.9.0-m0124/com/atlassian/servicedesk/api/requesttype/RequestTypeService.html#createRequestType-com.atlassian.jira.user.ApplicationUser-com.atlassian.servicedesk.api.requesttype.RequestTypeCreateParameters-" class="external-link">RequestTypeService: createRequestType</a></p></td>
</tr>
<tr class="odd">
<td><p><code>Either&lt;<br>
&nbsp;&nbsp;AnError,<br>
&nbsp;&nbsp;List&lt;<br>
&nbsp;&nbsp;&nbsp;&nbsp;RequestType<br>
&nbsp;&nbsp;&gt;<br>
&gt;<br>
getRequestTypesForGroup(<br>
&nbsp;&nbsp;Integer groupId,<br>
&nbsp;&nbsp;Long projectId<br>
);</code></p></td>
<td><p><a href="https://docs.atlassian.com/jira-servicedesk/3.7.0/com/atlassian/servicedesk/api/requesttype/RequestTypeService.html#getRequestTypesForGroup-java.lang.Integer-java.lang.Long-" class="external-link">RequestTypeService: getRequestTypesForGroup</a></p>
<p><strong>Old javadoc:</strong></p>
<p><code>#getRequestTypes(<br>
&nbsp;&nbsp;ApplicationUser,<br>
&nbsp;&nbsp;RequestTypeQuery<br>
)</code></p></td>
<td><p><a href="https://docs.atlassian.com/jira-servicedesk/3.9.0-m0124/com/atlassian/servicedesk/api/requesttype/RequestTypeService.html#getRequestTypes-com.atlassian.jira.user.ApplicationUser-com.atlassian.servicedesk.api.requesttype.RequestTypeQuery-" class="external-link">RequestTypeService: getRequestTypes</a></p></td>
</tr>
<tr class="even">
<td><p><code>Either&lt;<br>
&nbsp;&nbsp;AnError,<br>
&nbsp;&nbsp;List&lt;<br>
&nbsp;&nbsp;&nbsp;&nbsp;RequestType<br>
&nbsp;&nbsp;&gt;<br>
&gt;<br>
getRequestTypesByGroup(<br>
&nbsp;&nbsp;Integer groupId,<br>
&nbsp;&nbsp;Integer serviceDeskId<br>
);</code></p></td>
<td><p><a href="https://docs.atlassian.com/jira-servicedesk/3.7.0/com/atlassian/servicedesk/api/requesttype/RequestTypeService.html#getRequestTypesByGroup-java.lang.Integer-java.lang.Integer-" class="external-link">RequestTypeService: getRequestTypesByGroup</a></p>
<p><strong>Old javadoc:</strong></p>
<p><code>#getRequestTypes(<br>
&nbsp;&nbsp;ApplicationUser,<br>
&nbsp;&nbsp;RequestTypeQuery<br>
)</code></p></td>
<td><p><a href="https://docs.atlassian.com/jira-servicedesk/3.9.0-m0124/com/atlassian/servicedesk/api/requesttype/RequestTypeService.html#getRequestTypes-com.atlassian.jira.user.ApplicationUser-com.atlassian.servicedesk.api.requesttype.RequestTypeQuery-" class="external-link">RequestTypeService: getRequestTypes</a></p></td>
</tr>
<tr class="odd">
<td><p><code>Either&lt;<br>
&nbsp;&nbsp;AnError,<br>
&nbsp;&nbsp;RequestType<br>
&gt;<br>
getRequestTypeByIdAndProjectId (<br>
&nbsp;&nbsp;Integer requestTypeId,<br>
&nbsp;&nbsp;Long projectId<br>
);</code></p></td>
<td><p><a href="https://docs.atlassian.com/jira-servicedesk/3.7.0/com/atlassian/servicedesk/api/requesttype/RequestTypeService.html#getRequestTypeByIdAndProjectId-java.lang.Integer-java.lang.Long-" class="external-link">RequestTypeService: getRequestTypeByIdAndProjectId</a></p>
<p><strong>Old javadoc:</strong></p>
<p><code>#getRequestTypes(<br>
&nbsp;&nbsp;ApplicationUser,<br>
&nbsp;&nbsp;RequestTypeQuery<br>
)</code></p></td>
<td><p><a href="https://docs.atlassian.com/jira-servicedesk/3.9.0-m0124/com/atlassian/servicedesk/api/requesttype/RequestTypeService.html#getRequestTypes-com.atlassian.jira.user.ApplicationUser-com.atlassian.servicedesk.api.requesttype.RequestTypeQuery-" class="external-link">RequestTypeService: getRequestTypes</a></p></td>
</tr>
<tr class="even">
<td><p><code>Either&lt;<br>
&nbsp;&nbsp;AnError,<br>
&nbsp;&nbsp;RequestType<br>
&gt;<br>
deleteRequestTypeByIdAndProjectId(<br>
&nbsp;&nbsp;Integer requestTypeId,<br>
&nbsp;&nbsp;Long projectId<br>
);</code></p></td>
<td><p><a href="https://docs.atlassian.com/jira-servicedesk/3.7.0/com/atlassian/servicedesk/api/requesttype/RequestTypeService.html#deleteRequestTypeByIdAndProjectId-java.lang.Integer-java.lang.Long-" class="external-link">RequestTypeService: deleteRequestTypeByIdAndProjectId</a></p>
<p><strong>Old javadoc:</strong></p>
<p><code>#deleteRequestType(<br>
&nbsp;&nbsp;ApplicationUser,<br>
&nbsp;&nbsp;RequestTypeDeleteParameters<br>
)</code></p></td>
<td><p><a href="https://docs.atlassian.com/jira-servicedesk/3.9.0-m0124/com/atlassian/servicedesk/api/requesttype/RequestTypeService.html#deleteRequestType-com.atlassian.jira.user.ApplicationUser-com.atlassian.servicedesk.api.requesttype.RequestTypeDeleteParameters-" class="external-link">RequestTypeService: deleteRequestType</a></p></td>
</tr>
<tr class="odd">
<td><p><code>Either&lt;<br>
&nbsp;&nbsp;AnError,<br>
&nbsp;&nbsp;RequestType<br>
&gt;<br>
updateRequestTypeByIdAndProjectId(<br>
&nbsp;&nbsp;Integer requestTypeId,<br>
&nbsp;&nbsp;Long projectId,<br>
&nbsp;&nbsp;RequestType requestType<br>
);</code></p></td>
<td><p><a href="https://docs.atlassian.com/jira-servicedesk/3.7.0/com/atlassian/servicedesk/api/requesttype/RequestTypeService.html#updateRequestTypeByIdAndProjectId-java.lang.Integer-java.lang.Long-com.atlassian.servicedesk.api.requesttype.RequestType-" class="external-link">RequestTypeService: updateRequestTypeByIdAndProjectId</a><strong><br />
</strong></p>
<p><strong>Old javadoc:</strong></p>
<p><code>#updateRequestType(<br>
&nbsp;&nbsp;ApplicationUser,<br>
&nbsp;&nbsp;RequestTypeUpdateParameters<br>
)</code></p></td>
<td><p><a href="https://docs.atlassian.com/jira-servicedesk/3.9.0-m0124/com/atlassian/servicedesk/api/requesttype/RequestTypeService.html#updateRequestType-com.atlassian.jira.user.ApplicationUser-com.atlassian.servicedesk.api.requesttype.RequestTypeUpdateParameters-" class="external-link">RequestTypeService: updateRequestType</a></p></td>
</tr>
<tr class="even">
<td><p>FpKit</p></td>
<td><p><code>Callable Unit runnableToCallable(<br>
&nbsp;&nbsp;Runnable runnable<br>
)</code></p>
<p><strong>Old javadoc:</strong></p>
<div>
<a href="https://docs.atlassian.com/jira-servicedesk/3.7.0/com/atlassian/servicedesk/api/util/fp/FpKit.html#runnableToCallable-java.lang.Runnable-" class="external-link">FpKit: runnableToCallable</a> 
</div></td>
<td><p>This deprecated and Experimental API has been removed.</p></td>
</tr>
</tbody>
</table>
