---
aliases:
- /server/jira/platform/jira-data-center-32345322.html
- /server/jira/platform/jira-data-center-32345322.md
category: devguide
confluence_id: 32345322
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=32345322
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=32345322
date: '2017-12-08'
legacy_title: JIRA Data Center
platform: server
product: jira
subcategory: datacenter
title: About Jira Data Center
---
# About Jira Data Center

<a href="https://confluence.atlassian.com/display/ENTERPRISE/JIRA+Data+Center" class="external-link">Jira Data Center</a> is the enterprise implementation of Jira, providing high availability and performance at scale. This section contains developer information specific to Jira Data Center, including guidelines for building plugins for high availability and clustered environments.

-   [Plugin Guide to Jira High Availability and Clustering](/server/jira/platform/developing-for-high-availability-and-clustering)
-   [Preliminary Plugin Guide to Archiving API](/server/jira/platform/jira-archiver)
-   [Zero downtime upgrades for Jira Data Center applications](/server/jira/platform/zero-downtime-upgrades-for-jira-data-center-applications)

