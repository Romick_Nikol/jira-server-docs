---
aliases:
- /server/jira/platform/how-do-i-create-a-report-with-custom-issue-set-using-issuenavigator-4227153.html
- /server/jira/platform/how-do-i-create-a-report-with-custom-issue-set-using-issuenavigator-4227153.md
category: devguide
confluence_id: 4227153
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=4227153
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=4227153
date: '2017-12-08'
legacy_title: How do I create a report with custom issue set using IssueNavigator
platform: server
product: jira
subcategory: other
title: How do I create a report with custom issue set using IssueNavigator
---
# How do I create a report with custom issue set using IssueNavigator

This code could be cleaned up, but it shows how to generate an IssueNavigator table from a List of issues. This will be very helpful to anyone trying to generate a list of issues who wants to reuse the excellent IssueNavigator table we all know and love.

``` javascript
    /**
     * Using the list of issues we've compiled generate a report using the IssueNavigator.
     * @param action
     * @param params
     * @param issues
     * @return
     * @throws Exception
     */
    protected String generateIssueTableslForReport(ProjectActionSupport action, Map params, List issues) throws Exception {
        HttpServletRequest request = ActionContext.getRequest();
        IssueTableLayoutBean layoutBean = issueNavigator.getTableLayoutFactory().getStandardLayout(issueNavigator.getSearchRequest(), issueNavigator.getRemoteUser());
        IssueTableWebComponent itwc = new IssueTableWebComponent();
        StringBuffer sb = new StringBuffer();
        List tempList = new LinkedList();

        // break up what gets returned into sets of issues revolving around an issue
        // of type feature
        for (int i = 0; i < issues.size(); i++) {
            Issue issue = (Issue) issues.get(i);
            if (issue.getIssueType().get("name").equals(FLUID_ISSUE_TYPE_FEATURE)) {
                if (tempList.size() > 0) {
                    sb.append("<br/>");
                    sb.append(itwc.getHtml(layoutBean, tempList, null));
                    sb.append("<br/>");
                    tempList.clear();
                }
            }
            // add this feature to the list
            tempList.add(issue);
        }
        // write out the rest of the issues
        sb.append("<br/>");
        sb.append(itwc.getHtml(layoutBean, tempList, null));
        sb.append("<br/>");
        return sb.toString();
    }
```
