---
aliases:
- /server/jira/platform/4227152.html
- /server/jira/platform/4227152.md
category: devguide
confluence_id: 4227152
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=4227152
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=4227152
date: '2017-12-08'
legacy_title: How do I get a handle to object X (specifically IssueNavigator)?
platform: server
product: jira
subcategory: other
title: How do I get a handle to object X (specifically IssueNavigator)?
---
# How do I get a handle to object X (specifically IssueNavigator)?

#### How do I get a handle to object X (specifically IssueNavigator)?

For JIRA plugins in many cases you can simply pass an object to your plugin's constructor. However some objects are not by default available. In these cases you have to add the &lt;component&gt; to your atlassian-plugin.xml file like so:

``` javascript
    <component key="issueNavigator" name="Issue Navigator" class="com.atlassian.jira.web.action.issue.IssueNavigator">      <interface>com.atlassian.jira.web.action.issue.IssueNavigator</interface>
    </component>
```

There are many examples in the atlassian-plugin.xml files within the sample plugins found in the JIRA development kit.
