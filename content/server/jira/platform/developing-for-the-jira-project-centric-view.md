---
aliases:
- /server/jira/platform/development-guide-jira-project-centric-view-31508342.html
- /server/jira/platform/development-guide-jira-project-centric-view-31508342.md
category: devguide
confluence_id: 31508342
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=31508342
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=31508342
date: '2017-12-08'
guides: guides
legacy_title: Development guide - JIRA project-centric view
platform: server
product: jira
subcategory: learning
title: Developing for the JIRA project-centric view
---
# Developing for the JIRA project-centric view

This page provides you with development guidelines for adding your add-on to the new project-centric view in JIRA.

Read the [Design guide - JIRA project-centric view](/server/jira/platform/designing-for-the-jira-project-centric-view) **before** you read this page. The design guide will help you provide the best user experience for your add-on in the project-centric view.

## Overview

The project-centric view will be introduced in JIRA 6.4, but can be disabled on both the user level or on the global level by an administrator.

For both JIRA Cloud and JIRA Server, individual users will be able to switch it on or off. Administrators will also be able to override this setting (i.e. enable/disable for all users). This means that from JIRA 6.4,** your add-on will need to cater for instances where the project-centric view is enabled, as well as instances where it is disabled.**

The project-centric view features the following components:

-   A sidebar header 
-   Sidebar content section, including project navigation, project links and favorite filters for the user
-   Sidebar administration link (if the currently logged in user is a project administrator)
-   Page content area

*Screenshot: Example JIRA project-centric view*

<img src="/server/jira/platform/images/jira-project-annotated.png" width="700" />

## Guidelines

Most add-ons will just need to add links for their project-related content to the sidebar. If you want to do this, read the [Basic implementation](#basic-implementation) section below. 

If you want a more customized implementation, e.g. you want your add-on's sidebar link to open a particular board in the project, read the [Advanced implementation](#advanced-implementation) section below.

### Basic implementation

To add a link for your add-on to the sidebar, you will need to add the link to the sidebar as a web-item, then provide the content in the main page content area.

#### Step 1. Add the link to the sidebar as a web-item

1.  Add your link as a [web-item](/server/jira/platform/web-item) to the `jira.project.sidebar.plugins.navigation`` `[web-section](/server/jira/platform/web-section).
2.  The following context parameters are available for your web-item. Specify the parameters that you need.

    | Parameter                         | Description                                       |
    |-----------------------------------|---------------------------------------------------|
    | project                           | The current project object                        |
    | projectKey                        | The current project key                           |
    | pathEncodedProjectKey             | URL path encoded project key                      |
    | queryEncodedProjectKey            | URL query param encoded project key               |
    | Pluggable Scope Filter Parameters | See [Scope filters](#scope-filters) section below |

    For more details, see the `ProjectContextPopulator` in the jira-projects plugin.

For example, here is the web-item definition for the 'Releases' link shown in the *Example JIRA project-centric view* screenshot above:

``` xml
<web-item key="release-page" i18n-name-key="version.release" section="jira.project.sidebar.plugins.navigation" weight="300">
    <label key="project.page.release.title"/>
    <link>/projects/$pathEncodedProjectKey?selectedItem=com.atlassian.jira.jira-projects-plugin:release-page</link>
    <param name="iconClass" value="aui-icon-large icon-sidebar-release"/>
    <condition class="com.atlassian.jira.projects.page.release.VersionsDefinedAndVisibleCondition"/>
</web-item>
```

#### Step 2. Provide the content

1.  Define a [web-panel](/server/jira/platform/web-item) for your content.
2.  Reference the web-panel from the sidebar link that you added as a web-item, in the previous step.

For example, here is the web-item and web-panel definitions for the 'Releases' link shown in the *Example JIRA project-centric view* screenshot above.

``` xml
<web-item key="release-page" i18n-name-key="version.release" section="jira.project.sidebar.plugins.navigation" weight="300">
    <label key="project.page.release.title"/>
    <link>/projects/$pathEncodedProjectKey?selectedItem=com.atlassian.jira.jira-projects-plugin:release-page</link>
    <param name="iconClass" value="aui-icon-large icon-sidebar-release"/>
    <condition class="com.atlassian.jira.projects.page.release.VersionsDefinedAndVisibleCondition"/>
</web-item>
 
<web-panel key="release-page-key" location="com.atlassian.jira.jira-projects-plugin:release-page">
    <resource name="view" type="soy" location=":project-subpages-soy/JIRA.Projects.Release.Templates.page"/>
    <context-provider class="com.atlassian.jira.projects.page.release.ReleasePageContextProvider"/>
    <condition class="com.atlassian.jira.projects.page.release.VersionsDefinedAndVisibleCondition"/>
</web-panel>
```

Note how the `link` property in the `release-page` web-item references the `location` property in the `release-page-key` web-panel. The `ProjectPageServlet` will take care of wiring up the `selectedItem` with the web-panel location and rendering the panel when the web-item URL is requested.  Additional web-resources (JS, CSS) can also be included on the page by adding them to the `jira.project.sidebar` &lt;context/&gt;.

This mechanism is the same for [atlassian-connect ](https://developer.atlassian.com/static/connect/docs/index.html)add-ons.  For example the following atlassian connect descriptor would render a sidebar link and content when selected.

``` javascript
{
     "name": "Hello World",
     "description": "Atlassian Connect add-on",
     "key": "com.example.myaddon",
     "baseUrl": "http://localhost:8000",
     "vendor": {
         "name": "Example, Inc.",
         "url": "http://example.com"
     },
     "authentication": {
         "type": "none"
     },
     "apiVersion": 1,
     "modules": {         
        "webItems": [
            {
                "location": "jira.project.sidebar.plugins.navigation",
                "key": "connect-provided-link-to-panel",
                "url": "/projects/{project.key}?selectedItem=com.example.myaddon__connect-provided-link-to-panel",
                "context": "product",
                "name": {
                    "value": "My Web Item"
                }
            }
        ],
        "webPanels" : [
            {
                "key": "connect-provided-web-panel",
                "url": "/helloworld.html?pkey={project.key}",
                "location": "com.example.myaddon__connect-provided-link-to-panel",                
                "name": {
                    "value": "My Web Panel"
                }
            }
        ]        
     }
 }
 
```

### Advanced implementation

The project-centric view allows you to extend both the sidebar and page content, as described below.

#### Extending the sidebar

If you want to customise the sidebar further for your add-on, it can be extended in more advanced ways by using scope filters, the JavaScript API, or the Serverside Rendering API. The sidebar is rendered and provided by the new jira-projects plugin. Hence, to use any of the advanced methods for extending the sidebar, you will need to include the **jira-projects-api** jar in your add-on's dependencies:

``` xml
<dependency>
    <groupId>com.atlassian.jira</groupId>
    <artifactId>jira-projects-api</artifactId>
    <version>${latest.jira.projects.plugin.version}</version>
    <scope>provided</scope>
</dependency>
```

#### Advanced methods for extending the sidebar:

##### **Scope filters**

JIRA projects provide an additional scope filter, to allow users to select a project-specific Agile board.  Scope filters can add additional context used to render web-items and page content via the new `project-scope-filter-context-provider` plugin module.  This is useful if web-items in the sidebar require additional context about which agile board has been selected for example.

**Example:**

In JIRA Agile's example, the following scope filter context provider provides additional params for web-items defined in JIRA Agile that plugin into the sidebar:

``` xml
<project-scope-filter-context-provider key="board-scope-filter" class="com.atlassian.greenhopper.web.sidebar.AgileBoardScopeFilterContextProvider" />
```

This simply implements the following interface provided by the `jira-projects-api`:

``` java
/**
 * Provides a context map with scope filter params that may be needed to render project links and pages.  For example
 * JIRA may decide to put the currently selected board into the context which will then be used to add
 * Agile reports to the reports page.
 *
 * @since v1.6.0
 */
public interface ProjectScopeFilterContextProvider
{
    /**
     * Given a project, return context with scope filter params.
     *
     * @param project the currently selected project
     * @return context param map
     */
    Map<String, Object> getContext(final Project project);
}
```

Additional context provided by this mechanism can then be used when rendering links. For example, by using an additional `selectedBoardId` param in the URL as follows:

``` xml
<web-item key="project-sidebar-work-scrum" name="Scrum Work Mode Button in the Project Sidebar" section="jira.project.sidebar.plugins.navigation" weight="8">
    <label key="gh.rapid.pool.link.scrum.label"/>
    <link>/secure/RapidBoard.jspa?projectKey=$pathEncodedProjectKey&amp;rapidView=$selectedBoardId</link>
    <param name="iconClass" value="aui-icon-large agile-icon-work"/>
    <condition class="com.atlassian.greenhopper.web.sidebar.IsScrumBoardCondition"/>
</web-item>
```

**JavaScript API**

The sidebar also provides a rich javascript API to allow add-ons to override default behaviour client-side if required.  For more details, see [Sidebar JavaScript API for JIRA project-centric view](/server/jira/platform/using-the-sidebar-javascript-api-for-the-jira-project-centric-view).

**Serverside Rendering API**

The sidebar is rendered by a number of re-usable Java components that can be injected.  Rendering is broken down into the following components:

<img src="/server/jira/platform/images/jira-sidebar-renderers.png" width="350" />

The main entry point to render a project centric sidebar is provided by the `ProjectSidebarRenderer` component and its `render(projectKey, selectedItemId)` method.

#### Extending the page content

**Full control of rendering**

You should only use this option for advanced add-ons, where you need full control of the URL state. An example of this is JIRA's agile board, where the sidebar allows users to switch between Backlog, Active Sprints and Reports via sidebar links on the client side.

 This approach lets you implement advanced navigation, like sub-navigation within the page. However, in many cases, it is better to implement the basic web-panel (described above) instead -- here are some of the reasons why:

-   The web-panel is simpler to implement, especially if you have a simple (static) add-on.
-   The sidebar is rendered for you. In addition, the context for your web-panel is populated with the same project parameters and scopefilter parameters as for web-items.  
    *If you choose to implement full control of rendering, your add-on will need to render the sidebar.*
-   The sidebar client-side web-resources are already included.  
    *If you choose to implement full control of rendering, your add-on must include the sidebar client-side web-resources in the `jira.project.sidebar` context .*
-   `ProjectPageServlet` in the jira-projects plugin selects the correct item in the sidebar based on the `selectedItem` URL parameter.  
    *If you choose to implement full control of rendering, your add-on's code will have to handle selecting the correct item.*

**To implement full control of rendering for your add-on's page content:**

1.  Render the entire page, including the sitemesh decorator using an existing plugin mechanism (most likely a servlet or webwork action).
2.  In this servlet or webwork action, get the `ProjectSidebarRenderer` component injected and render the sidebar HTML using an appropriate AUI page structure. The jira-projects plugin provides a `JIRA.Projects.Templates.page` soy template to make this easier
3.  Require all web-resources in the jira.project.sidebar web-resource context.

#### Content sub-navigation component

In project-centric navigation model, once users arrive at certain project pages, they can filter the content on that particular page by using the subnavigation control. For example, in the screenshot in the right, once the reports page is loaded, users can select to narrow down what they see and choose to display only the information regarding the Control Chart.

The jira-projects plugin exposes the **content subnavigator** as a reusable component. The subnavigator is a component that gets rendered on the client side, using JavaScript code. In order to use this component, your plugin must declare a dependency with `com.atlassian.jira.jira-projects-plugin:subnavigator,` so that the required JavaScript resources are downloaded and available on the browser.

<img src="/server/jira/platform/images/jira-subnavigator.png" height="250" />

The procedure for implementing this component is best explained by example: consider the reports page shown in the screenshot on the right. This is how it is bringing the resources required to use the `subnavigator `on the client side:

``` xml
 <web-resource key="reports-page-subnavigator">
     <context>com.atlassian.jira.project.reports.page</context>
     <dependency>com.atlassian.jira.jira-projects-plugin:sidebar</dependency>
     <dependency>com.atlassian.jira.jira-projects-plugin:subnavigator</dependency>
     <resource location="/page/project/report/subnavigator-init.js" name="reports-subnavigator-init.js" type="download"/>
     <transformation extension="js">
         <transformer key="jsI18n"/>
     </transformation>
 </web-resource>
```

As you can see, there is a dependency declared on the `reports.page` web resource, so that whenever the page is loaded, the subnavigator resources are also downloaded.

Once the page is loaded, you can initialize the subnavigator in your JavaScript code like this:

``` javascript
var subnavigator = new JIRA.Projects.Subnavigator({
    id: "reports",                                       // identifier for the subnavigator component 
    triggerPlaceholder: triggerPlaceholder,              // css selector where the trigger of the the component will be rendered
    contentPlaceholder: contentPlaceholder,              // css selector where the options of the component will be rendered
    itemGroups: [{                                       // array of arrays, each of one containing a list of items to be displayed by the container
        id: "report-id",                                 
        label: "report-label",                           // text that will be displayed for the item
        description: "report-description",                // text that will be displayed on a tooltip when the mouse is over the item
        link: "report-url"                               // link to follow when the item is selected
    }],
    selectedItem: "selected-report-id"                   // id of the initially selected item
});
subnavigator.show();
 
subnavigator.on("itemSelected", function(event) {        // listening for changes on the selected item
    event.preventDefault();         
    console.log(event.item);                             // the event will give you access to the selected item
});
```

The most detailed and up to date documentation around this component can be found in the source code on `src/main/resources/static/components/subnavigator/Subnavigator.js `

The `jira-projects-page-objects` also provides a `Subnavigator.java` that you can use as a PageObject for the component on your web driver tests.

## Need help?

Try the following resources:

-   Post your question on our forums: <a href="http://answers.atlassian.com/" class="external-link">Atlassian Answers</a>. This is probably the fastest way to get help, as many of our customers and staff are active on Atlassian Answers.
