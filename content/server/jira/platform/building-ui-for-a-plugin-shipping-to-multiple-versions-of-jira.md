---
aliases:
- /server/jira/platform/building-ui-for-a-plugin-shipping-to-multiple-versions-of-jira-16974620.html
- /server/jira/platform/building-ui-for-a-plugin-shipping-to-multiple-versions-of-jira-16974620.md
category: devguide
confluence_id: 16974620
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=16974620
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=16974620
date: '2017-12-08'
legacy_title: Building UI for a plugin shipping to multiple versions of JIRA
platform: server
product: jira
subcategory: updates
title: Building UI for a plugin shipping to multiple versions of JIRA
---
# Building UI for a plugin shipping to multiple versions of JIRA

## Before you begin

Please refer to the following documents, which should answer some of your questions:

-   [Atlassian Design Guidelines (ADG)](https://developer.atlassian.com/design/) - the big picture
-   [Preparing for JIRA 6.0](https://developer.atlassian.com/display/JIRADEV/Preparing+for+JIRA+6.0) - general JIRA 6.0 + ADG information
-   [JIRA Agile ADG Migration Guidelines](https://developer.atlassian.com/display/JIRADEV/Bonfire+ADG+Migration+Guidelines) - a case study on shipping to multiple versions of JIRA. Note, JIRA Agile was previously named Bonfire.

## Some suggestions

-   ### Use AUI components exclusively

    The markup patterns for AUI components for most things are practically identical between JIRA 5.x and JIRA 6.0. There should be a minimum of fuss for these things.

-   ### Don't extend or override AUI patterns

    Extending or overriding AUI/JIRA's UI patterns are **the \#1 reason** a UI "breaks" across versions of JIRA.

-   ### Minimise your dependencies

    If you're shipping bespoke UI, it's best to depend on as little as possible - ideally nothing other than your own plugin's assets. If you need something "like this, but a bit different," build it entirely yourself within your plugin. Don't depend on JIRA's or AUI's UI assets whatsoever. If you own the code that renders your UI, the chances of your UI breaking when you deploy it are greatly minimised.

-   ### Put your markup in small, composable templates

    If your markup is static, it will be difficult for you to update your plugin. Instead, pull as many UI concepts as you can out into templates and make template calls. This affords you the ability to swap out templates based on the version of JIRA you're using.

## On determining what is safe to rely upon

When building across multiple versions of anything (of which JIRA is a subset), you need to determine your lowest common version for each dependency. Once you know this, you can determine the set of (UI) assets that will be available across all the versions you intend to support.

### AUI assets across JIRA versions

<table>
<colgroup>
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 80%" />
</colgroup>
<thead>
<tr class="header">
<th>JIRA version</th>
<th>AUI version</th>
<th>AUI components available</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>5.0.0</td>
<td>3.5.5</td>
<td>Dialog, Dropdown, Group+Item, Forms, Icons, Inline-dialog (popup), Messages, Tables, Tabs, Toolbar</td>
</tr>
<tr class="even">
<td>5.1.x</td>
<td>3.5.5</td>
<td>Dialog, Dropdown, Group+Item, Forms, Icons, Inline-dialog (popup), Messages, Tables, Tabs, Toolbar</td>
</tr>
<tr class="odd">
<td>5.1.8</td>
<td>3.7-m6</td>
<td>Dialog, Dropdown, Dropdown2, Group+Item, Forms, Icons, Inline-dialog (popup), Messages, RESTful Table, Tables, Tabs, Toolbar</td>
</tr>
<tr class="even">
<td>5.2.x</td>
<td>5.0-m10</td>
<td><p>Badges, Dialog, Dropdown, Dropdown2, Group+Item, Forms, Icons, Inline-dialog (popup), Labels, Lozenges, Messages, RESTful Table, Tables, Tabs, Toolbar</p></td>
</tr>
<tr class="odd">
<td>6.0-m06</td>
<td>5.0-m25</td>
<td><p>Avatars, Badges, Breadcrumbs (Navigation), Buttons, Dialog, Dropdown, Dropdown2, Group+Item, Forms, Icons, Inline-dialog (popup), Labels, Lozenges, Messages, RESTful Table, Tables, Tabs, Toolbar, Toolbar2, Tooltips</p>
<p>Multi-step Progress Tracker, Page Layouts, Page Panel/Content Layouts, Navigation (Horizontal, Vertical)</p></td>
</tr>
</tbody>
</table>
