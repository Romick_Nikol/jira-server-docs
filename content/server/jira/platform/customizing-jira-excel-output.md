---
aliases:
- /server/jira/platform/customising-jira-excel-output-4227126.html
- /server/jira/platform/customising-jira-excel-output-4227126.md
category: devguide
confluence_id: 4227126
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=4227126
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=4227126
date: '2017-12-08'
guides: tutorials
legacy_title: Customising JIRA Excel Output
platform: server
product: jira
subcategory: learning
title: Customizing JIRA Excel output
---
# Customizing JIRA Excel output

Microsoft Office 2000 supports Hypertext Markup Language (HTML) as a native file format, allowing Microsoft Excel to save and open HTML files as spreadsheets with all the document formatting preserved. The document can be formatted through a combination of HTML, XML and Cascading Style Sheets (CSS).

The format of the Excel output from JIRA can be modified by editing the template file *searchrequest-excel-header.vm* (located within *src/etc/java/templates/plugins/searchrequestviews/* of your JIRA home folder). Editing this file is essentially editing a HTML template file with tables and styles.

By default the template file *searchrequest-excel-header.vm* should contain:

**searchrequest-excel-header.vm**

``` xml
...
<style>
@page
{
mso-page-orientation:landscape;
margin:.25in .25in .5in .25in;
mso-header-margin:.5in;
mso-footer-margin:.25in;
mso-footer-data:"&R&P of &N";
mso-horizontal-page-align:center;
mso-vertical-page-align:center;
}

br
{
    mso-data-placement:same-cell;
}

td
{
    vertical-align: top;
}
</style>
...
```

The above formats the default excel output to have the page orientation as landscape, and multi-lined text will be kept within one cell.  
Further examples are provided [JIRA:below](#jira-below).

## Microsoft Office HTML and XML Reference

Microsoft Office HTML and XML reference contains a thorough listing of available styles and details on how to use them. The reference manual is available for download from <a href="http://msdn.microsoft.com/library/default.asp?url=/library/en-us/dnoffxml/html/ofxml2k.asp" class="uri external-link">msdn.microsoft.com/library/default.asp?url=/library/en-us/dnoffxml/html/ofxml2k.asp</a> 

## Example 1. Customising Page Setup

Excel uses CSS and @page definitions to store some page setup settings and XML to store others. Below is a sample to setup the page with the following document format respectively.

-   Set page orientation as landscape (default is portrait)
-   Set the top, right, bottom and left margins as .75in .25in .75in .25in respectively
-   Set header margin as .5in
-   Set footer margin as .4in
-   Set a custom footer displaying page number, date and time all centered.

{{% note %}}

Details of the default values, and special symbols to use for styles can be found in the <a href="http://msdn.microsoft.com/library/default.asp?url=/library/en-us/dnoffxml/html/ofxml2k.asp" class="external-link">Microsoft Reference Manual</a>.

{{% /note %}}

``` xml
<style>
...
@page
{
mso-page-orientation: landscape;
margin:.75in .25in .75in .25in;
mso-header-margin:.5in;
mso-footer-margin:.4in;
mso-footer-data:"&C Page &P of &N Date: &D Time: &T";
<!-- other @page styles ... -->
}
</style>
```

## Example 2. Customising Cell Formatting

Both CSS and HTML are used to customise the cells. A global style that is applied to all table cells can be applied using TD definition.

``` xml
<style>
...
TD
{
mso-numberformat:general;
text-align: left;
vertical-align: middle;
color:red
<!-- other TD styles ... -->
}
</style>
```

The above applies the following styles to all table cells respectively:

-   Sets the number format of all table cells as *general*
-   Horizontally aligns the text in the cell to the left
-   Vertically aligns the text in the cell to the middle
-   Set the text color to be red

{{% note %}}

Individual or group of cells may have other styles applied by adding a class value into the appropriate HTML tags

{{% /note %}}
