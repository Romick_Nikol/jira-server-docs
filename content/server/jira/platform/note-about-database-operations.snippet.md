---
aliases:
- /server/jira/platform/-note-about-database-operations-4227104.html
- /server/jira/platform/-note-about-database-operations-4227104.md
category: devguide
confluence_id: 4227104
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=4227104
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=4227104
date: '2017-12-08'
legacy_title: _Note About Database Operations
platform: server
product: jira
subcategory: other
title: Note about database operations
---
# Note about database operations

{{% warning %}}

Direct database queries are not recommended in JIRA. Instead, we recommend adding or modifying data via [JIRA's REST APIs](/server/jira/platform/rest-apis), [JIRA RPC Services](/server/jira/platform/jira-rpc-services-4227114.html) or <a href="http://confluence.atlassian.com/display/JIRA/Jelly+Tags" class="external-link">Jelly Tags</a>. Check out the <a href="https://plugins.atlassian.com/plugin/details/6398" class="external-link">Command Line Interface</a> and <a href="https://plugins.atlassian.com/plugin/details/10751" class="external-link">Python CLI</a> for existing remote scripting tools. If you absolutely must modify data in your database via direct database queries, always <a href="http://confluence.atlassian.com/display/JIRA/Backing+Up+Data" class="external-link">back up</a> your data before performing any modification to the database.

{{% /warning %}}
