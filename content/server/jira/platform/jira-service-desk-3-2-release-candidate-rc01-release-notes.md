---
aliases:
- /server/jira/platform/40503049.html
- /server/jira/platform/40503049.md
category: devguide
confluence_id: 40503049
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=40503049
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=40503049
date: '2017-12-08'
legacy_title: JIRA Service Desk 3.2 Release Candidate (RC01) release notes
platform: server
product: jira
subcategory: updates
title: JIRA Service Desk 3.2 Release Candidate (RC01) release notes
---
# JIRA Service Desk 3.2 Release Candidate (RC01) release notes

**02 July 2016**

Atlassian is proud to present **JIRA Service Desk 3.2 RC01**. This public development release is part of our [Early Access Program (EAP)](/server/jira/platform/jira-eap-releases-35160569.html) leading up to the official **JIRA Service Desk 3.2** release. We are making these EAP and Release Candidate milestones publicly available so that developers can start assessing the impact of the changes that we are making.

JIRA Service Desk 3.2 also includes JIRA 7.2 platform changes, and for the full list of changes for JIRA 7.2, read the developer change management guide: [Preparing for JIRA 7.2](/server/jira/platform/preparing-for-jira-7-2). If you are new to JIRA, you should also read our [Java API Policy for JIRA](/server/jira/platform/java-api-policy-for-jira-4227213.html) and the [Atlassian REST API policy](https://developer.atlassian.com/display/HOME/Atlassian+REST+API+policy).

[<img src="/server/jira/platform/images/download-eap-btn.png" class="image-center confluence-thumbnail" width="120" />](#downloads)

{{% warning %}}

Before you install/upgrade: Atlassian does not support upgrades both 'from' and 'to' EAP releases. EAP releases should not be used in production environments as they are not officially supported. For all production use and testing of JIRA, please use the <a href="https://confluence.atlassian.com/display/JIRASOFTWARE/JIRA+Software+7.0.x+release+notes" class="external-link">latest official release</a> instead.

{{% /warning %}}

{{% note %}}

**We want your feedback!**

To provide feedback, create an issue in <a href="https://jira.atlassian.com/browse/JRA" class="external-link">our issue tracker</a> with the following details:

-   **Issue Type**: `Suggestion`
-   **Component**: `JIRA 7.2 EAP`

{{% /note %}}

## Approvals

 We've added a configurable approval step that can be added to your work flows. This lets JIRA Service Desk admins define if an approval is required, how many approvals are needed, and even decide on specific approvers for a request! We've also added some JQL options that allow you to easily search for requests that require or required approval. From an approvers point of view, they'll have a list of requests that require their approval, and can make a decision based on the information on the request.

## Reports

 We've added some further functionality to custom reports in service desk projects, you now have the ability to drill deeper into the information on data points, and really work out what's going on in your project. If you still want to massage the data further, you can also export the resuts of the custom report in a CSV format.

## Knowledge base

We've extended our integration with Confluence, and you now have more features available when using Confluence as a knowledge base (KB). You can now search for, view, and share KB articles directly in a request.

## Customer portal updates

We've updated functionality in the customer portals, allowing your customers to do more with their requests. Customers can remove participants, transition issues, and turn off request notifications now directly in the portal, as well as search for KB articles and request types.

## Sidebar updates

We've continued the work on the project sidebar for agents and admins, and we've added the following:

-   You can now raise a request directly from the sidebar
-   Admins can invite new users to the project directly from the sidebar
-   Project administration is now called project settings

## Requests

We've added the ability to group requests in the project settings menu via drag and drop. You can also use custom icons and images now for request icons, instead of the default JIRA Service Desk icons.

## Automation

We're still working on adding more functionality to automation for service desk projects, and we've added the following functionality:

-   Webhooks to 3rd party apps and add-ons
-   Custom email notifications
-   New WHEN, IF and THEN actions
-   Some rules are now enabled by default

## JIRA Core changes that affect JIRA Service Desk

All of the changes in the JIRA Core 7.2 RC01 are also in the JIRA Service Desk 3.2 RC01 unless otherwise stated. Read the [JIRA Core 7.2 RC01 release notes](/server/jira/platform/jira-core-7-2-release-candidate-rc01-release-notes) for details. 

## Upgrades

We don't support upgrading to or from EAP releases, and you should never use an EAP release in production. If you do intend to upgrade an instance to an EAP release, you should review  _all_  applicable upgrades notes for the production releases, and upgrade in the specified order:

-   <a href="https://confluence.atlassian.com/display/SERVICEDESK/JIRA+Service+Desk+3.1.x+upgrade+notes" class="external-link">Upgrading to JIRA Service Desk 3.1</a>
-   <a href="https://confluence.atlassian.com/display/JIRA064/Important+Version-Specific+Upgrade+Notes" class="external-link">Upgrading from JIRA 6.3 or earlier</a>

## Downloads

-   <a href="https://www.atlassian.com/software/jira/downloads/binary/atlassian-servicedesk-3.2.0-RC-20160629075822-jira-7.2.0-RC-20160629075822-x32.exe" class="external-link">JIRA Service Desk 7.2.0-RC01 (Windows 32 Bit Installer, EXE)</a>
-   <a href="https://www.atlassian.com/software/jira/downloads/binary/atlassian-servicedesk-3.2.0-RC-20160629075822-jira-7.2.0-RC-20160629075822-x64.exe" class="external-link">JIRA Service Desk 7.2.0-RC01 (Windows 64 Bit Installer, EXE)</a>
-   <a href="https://www.atlassian.com/software/jira/downloads/binary/atlassian-servicedesk-3.2.0-RC-20160629075822-jira-7.2.0-RC-20160629075822-x32.bin" class="external-link">JIRA Service Desk 7.2.0-RC01 (Linux 32 Bit Installer, BIN)</a>
-   <a href="https://www.atlassian.com/software/jira/downloads/binary/atlassian-servicedesk-3.2.0-RC-20160629075822-jira-7.2.0-RC-20160629075822-x64.bin" class="external-link">JIRA Service Desk 7.2.0-RC01 (Linux 64 Bit Installer, BIN)</a>
-   <a href="https://www.atlassian.com/software/jira/downloads/binary/atlassian-servicedesk-3.2.0-RC-20160629075822-jira-7.2.0-RC-20160629075822.zip" class="external-link">JIRA Service Desk 7.2.0-RC01 (ZIP Archive)</a>
-   <a href="https://www.atlassian.com/software/jira/downloads/binary/atlassian-servicedesk-3.2.0-RC-20160629075822-jira-7.2.0-RC-20160629075822.tar.gz" class="external-link">JIRA Service Desk 7.2.0-RC01 (TAR.GZ Archive)</a>
