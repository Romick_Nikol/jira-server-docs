---
aliases:
- /server/jira/platform/how-to-search-in-a-plugin-4227214.html
- /server/jira/platform/how-to-search-in-a-plugin-4227214.md
category: devguide
confluence_id: 4227214
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=4227214
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=4227214
date: '2017-12-08'
legacy_title: How to search in a plugin
platform: server
product: jira
subcategory: other
title: How to search in a plugin
---
# How to search in a plugin

<table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<tbody>
<tr class="odd">
<td><p>Changed:</p></td>
<td><p>JIRA 4.0. Searching underwent a major change in JIRA 4.0 with the introduction of JQL.<br />
If you are trying to search in versions of JIRA earlier than 4.0, please see the <a href="https://developer.atlassian.com/display/ARCHIVES/JIRA+Documentation+Archives">archived documentation</a>.</p></td>
</tr>
</tbody>
</table>

 

Please refer to the JIRA product documentation: <a href="http://confluence.atlassian.com/display/JIRA/__Searching+in+JIRA" class="external-link">Searching in JIRA</a>.
