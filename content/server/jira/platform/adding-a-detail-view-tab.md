---
aliases:
- /server/jira/platform/tutorial-adding-a-detail-view-tab-13632916.html
- /server/jira/platform/tutorial-adding-a-detail-view-tab-13632916.md
category: devguide
confluence_id: 13632916
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=13632916
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=13632916
date: '2018-05-25'
guides: tutorials
legacy_title: Tutorial - Adding a Detail View Tab
platform: server
product: jira
subcategory: learning
title: "Adding a Detail View tab"
---
# Adding a Detail View tab

<table>
<colgroup>
<col style="width: 20%" />
<col style="width: 80%" />
</colgroup>
<tbody>
<tr class="odd">
<td><p>Applicable:</p></td>
<td><p>Jira 7.0.0 and later.</p></td>
</tr>
<tr class="even">
<td><p>Level of experience:</p></td>
<td><p>Advanced. You should have completed at least one intermediate tutorial before working through this tutorial. See the <a href="/server/framework/atlassian-sdk/tutorials/">list of tutorials in DAC</a>.</p></td>
</tr>
<tr class="odd">
<td><p>Time estimate:</p></td>
<td><p>It should take you approximately 1 hour to complete this tutorial.</p></td>
</tr>
</tbody>
</table>

## Overview of the tutorial

In this tutorial, you'll learn how to use a web panel plugin module to add a custom tab to Jira Software boards.
While the web panels module is common to Atlassian products, this module will extend a Jira Software-specific
location in the interface.

The tutorial is intended to show you how to make a customization to Jira Software, but also to walk you through the development
flow for developing a Jira Software app with the Atlassian Plugin SDK.

Our app will add a tab in the Jira Software UI similar to the existing ones, such as Details and People tab.
Notice that each tab has a heading, icon link on the left, and tab content.
We'll need to create them in our app as well.

<img src="/server/jira/platform/images/wallboardempty.png" width="650" />

{{% note %}}

About these instructions

You can use any supported combination of operating system and IDE to create this app. These instructions were
written using IntelliJ IDEA 2017.3 on macOS Sierra. If you use another operating system or IDE combination,
you should use the equivalent operations for your specific environment.

This tutorial was last tested with Jira 7.7.1 using Atlassian Plugin SDK 6.3.10. 

{{% /note %}}

### Before you begin

To get the most out of this tutorial, you should know the following: 

1.   The basics of Java development, such as classes, interfaces, methods, and so on.
1.   How to create an Atlassian plugin project using the [Atlassian Plugin SDK](/server/framework/atlassian-sdk/set-up-the-atlassian-plugin-sdk-and-build-a-project/).
1.   How to use and administer Jira Software. 

### App source

We encourage you to work through this tutorial. If you want to skip ahead or check your work when you are finished, you
can find the app source code on Atlassian Bitbucket.

To clone the repository, run the following command:

``` bash
git clone https://bitbucket.org/atlassian_tutorial/adding-an-issue-view-tab-in-jira-agile.git
```

Alternatively, you can download the source as a [ZIP archive](https://bitbucket.org/atlassian_tutorial/adding-an-issue-view-tab-in-jira-agile/get/master.zip).  

## Step 1. Create the app project

In this step, you will create your app skeleton using the Atlassian Plugin SDK.

1.  [Set up the Atlassian Plugin SDK and build a project](/server/framework/atlassian-sdk/set-up-the-atlassian-plugin-sdk-and-build-a-project/)
if you did not do that yet.
1.  Open a Terminal and navigate to the directory where you would like to keep your project.
1.  To create an app skeleton, run the following command:

    ``` bash
    atlas-create-jira-plugin
    ```

1.  To identify your app, enter the following information when prompted.

    <table>
    <colgroup>
    <col style="width: 20%" />
    <col style="width: 80%" />
    </colgroup>
    <tbody>
    <tr class="odd">
    <td><p>group-id</p></td>
    <td><p><code>com.example.plugins.tutorial.jiraagile</code></p></td>
    </tr>
    <tr class="even">
    <td><p>artifact-id</p></td>
    <td><p><code>MyViewTab</code></p></td>
    </tr>
    <tr class="odd">
    <td><p>version</p></td>
    <td><p><code>1.0-SNAPSHOT</code></p></td>
    </tr>
    <tr class="even">
    <td><p>package</p></td>
    <td><p><code>com.example.plugins.tutorial.jiraagile</code></p></td>
    </tr>
    </tbody>
    </table>

1.  Confirm your entries when prompted.

    The SDK generates the project home directory with project files, such as the POM (that is, Project Object
    Model definition file), stub source code, and app resources.

1.  Navigate to the directory created in the previous step.
1.  Delete the test directories.

    Setting up testing for your app isn't part of this tutorial. To delete the generated test skeleton,
    run the following commands:

    ``` bash
    rm -rf ./src/test/java
    rm -rf ./src/test/resources/
    ```

1.  Delete the unneeded Java class files.

    ``` bash
    rm -rf ./src/main/java/com/example/plugins/tutorial/jiraagile/*
    ```

1.  Import project in your favorite IDE.

## Step 2. Review and tweak the stub code

It's a good idea to familiarize yourself with the project configuration file, known as the POM.
The POM defines general settings for the project, including project dependencies and build settings.

The SDK generates and maintains the POM on its own, for the most part. However, you need to manually tweak some of the
included metadata for your project.

1.  Navigate to the project directory created by the SDK and open the `pom.xml` file.
1.  Add your company or organization name and your website URL as the `name` and `url` values of
the `organization` element:

    ``` xml
    <organization>
        <name>Example Company</name>
        <url>http://www.example.com/</url>
    </organization>
    ```

1.  Update the `description` element:

    ``` xml
    <description>Adds a view tab to JIRA Software boards.</description>
    ```

1.  Save the file.

## Step 3. Add the web panel module

In this step, you will use the plugin module generator (that is, another `atlas` command) to generate the stub
code for modules required by the app.

For this tutorial, you will need a Web Panel plugin module. You'll add this using the
`atlas-create-jira-plugin-module` command.

1.  In a Terminal window, navigate to the app root folder where the `pom.xml` is located.
1.  Run the following command:

    ``` bash
    atlas-create-jira-plugin-module
    ```

1.  Select the `Web Panel` option.
1.  Enter the following information when prompted.

    <table>
    <colgroup>
    <col style="width: 50%" />
    <col style="width: 50%" />
    </colgroup>
    <tbody>
    <tr class="odd">
    <td><p>Plugin Module Name</p></td>
    <td><p><code>EinsteinQuotes</code></p></td>
    </tr>
    <tr class="even">
    <td><p>Location</p></td>
    <td><p><code>atl.gh.issue.details.tab</code></p></td>
    </tr>
    </tbody>
    </table>

1.  Select `Y` for *Show Advanced Setup*.
1.  Accept the defaults for:

      * module key
      * description
      * i18n name key
      * i18n description key
      * weight

1.  Select `Y` for *Add resource*.

1.  To configure the resource, enter the following when prompted.

    <table>
    <colgroup>
    <col style="width: 50%" />
    <col style="width: 50%" />
    </colgroup>
    <tbody>
    <tr class="odd">
    <td><p>Resource Name</p></td>
    <td><p><code>view</code></p></td>
    </tr>
    <tr class="even">
    <td><p>Resource Type</p></td>
    <td><code>velocity</code></td>
    </tr>
    <tr class="odd">
    <td><p>Location</p></td>
    <td><p><code>templates/reference-gh-issue-details-panel-1.vm</code></p></td>
    </tr>
    </tbody>
    </table>

1.  Select `N` for *Add a resource parameter*.
1.  Select `Y` for *Add another resource*.

1.  Enter the following parameters for the resource when prompted.

    <table>
    <colgroup>
    <col style="width: 50%" />
    <col style="width: 50%" />
    </colgroup>
    <tbody>
    <tr class="odd">
    <td><p>Resource Name</p></td>
    <td><p><code>icon</code></p></td>
    </tr>
    <tr class="even">
    <td><p>Resource Type</p></td>
    <td><code>download</code></td>
    </tr>
    <tr class="odd">
    <td><p>Location</p></td>
    <td><p><code>images/lightbulb.png</code></p></td>
    </tr>
    </tbody>
    </table>

1.  Select `N` for *Add a resource parameter*.
1.  Select `N` for *Add another resource*.

1.  Select `Y` for *Add a Velocity context parameter*. 
1.  Accept the default for the fully qualified context provider class that should be something like this:  
    `com.example.plugins.tutorial.jiraagile.web.contextproviders.MyContextProvider`
1.  Select `N` for *Add Conditions prompt*.

1.  Select `N` for *Add another plugin module*.

The SDK generates the module artifacts, indicating the new items in the output.

## Step 4. Make adjustments to the generated module

The SDK got you started with your web panel module, but it left you a few things to do.

1.  Navigate to `src/main/resources/` and open the `atlassian-plugin.xml` file.
1.  Add a few more child elements to the web-panel that you just added:

    ``` xml
    <label key="gh.issue.panel.reference" />
    <tooltip key="gh.issue.panel.reference.tooltip" />
    ```

## Step 5. Add project resources

1.  Copy this image to the `src/main/resource/images` directory.

<img src="/server/jira/platform/images/lightbulb.png" class="confluence-external-resource" />

This will be the icon for the tab in the issue view.

Note that any icons that you create for your own custom tabs should be of a similar size, about 14 × 14 pixels.

## Step 6. Create the Velocity template

In this step, you will create the Velocity template that produces custom view tab content.

1.  Navigate to `src/main/resources` and create a `directory` folder.
1.  In the new `directory` folder, create a file named `reference-gh-issue-details-panel-1.vm`.
1.  Add the following content to the file:

    ``` xml
    <div class="ghx-container">
        <h4>Einstein Quotes</h4>
        <blockquote>
            The secret to creativity is knowing how to hide your sources.
        </blockquote>
        <blockquote>
            Imagination is more important than knowledge. Knowledge is limited.
        </blockquote>
        <blockquote>
            I am enough of an artist to draw freely upon my imagination.
        </blockquote>
        <blockquote>
            When I examine myself and my methods of thought, I come to the conclusion that the gift of fantasy has meant more to me than any talent for abstract, positive thinking.
        </blockquote>
        <blockquote>
            The true sign of intelligence is not knowledge but imagination.
        </blockquote>
        <blockquote>
            To raise new questions, new possibilities, to regard old problems from a new angle, requires creative imagination and marks real advance in science.
        </blockquote>
        <blockquote>
            True art is characterized by an irresistible urge in the creative artist.
        </blockquote>
        <blockquote>
            The monotony and solitude of a quiet life stimulates the creative mind.
        </blockquote>
        <blockquote>
            It is the supreme art of the teacher to awaken joy in creative expression and knowledge.
        </blockquote>
    </div>
    ```

## Step 7. Add UI text

1.  In the same resources directory, open the properties file called `MyViewTab.properties`.
1.  Add the following properties:

    ``` javascript
    gh.issue.panel.reference=Reference Issue Tab Panel
    gh.issue.panel.reference.tooltip=Reference Issue Tab Panel Tooltip
    ```

 This text will appear in the UI as the heading text for your tab and the hover text for the icon.

## Step 8. Write the Java code

If you remember, the `web-panel` module you added to the app descriptor specified a `context-provider` class.
You need to create this class in your project. The class enables the Velocity template to interject its HTML into the
Jira application context.

For more information about `context-providers` for web panels, see the
[Web Panel plugin module](/server/jira/platform/web-panel#context-provider) documentation.

1.  Navigate to the root directory of your project and create a file named `MyContextProvider.java`.
1.  Add the following content to the file:

    ``` java
    package com.example.plugins.tutorial.jiraagile.web.contextproviders;

    import java.util.Map;

    import com.atlassian.jira.util.collect.MapBuilder;
    import com.atlassian.plugin.PluginParseException;
    import com.atlassian.plugin.web.ContextProvider;

    public class MyContextProvider implements ContextProvider {
        private Long itemCount = 4L;

        @Override
        public void init(Map<String, String> params) throws PluginParseException {
            if (params.containsKey("itemCount")) {
                this.itemCount = Long.parseLong(params.get("itemCount"));
            }
        }

        @Override
        public Map<String, Object> getContextMap(Map context) {
            return MapBuilder.<String, Object>newBuilder()
                    .add("atl.gh.issue.details.tab.count", itemCount).toMap();
        }
    }
    ```

    Notice that this class:

    *   Implements the `ContextProvider` interface.  
    *   Overrides the `init()` and `getContextMap()` methods. The methods can access and add to the context map,
    which are the context variables available for use in our Velocity templates.
    *   Sets the value of a well-known variable called `atl.gh.issue.details.tab.count` to the number on the
    tab indicator itself.

1.  Save and close the file.

## Step 9. Build, install, and run the app

In this step, you will start Jira Software and test the app.

1.  Ensure that AMPS is configured to run Jira with Jira Software installed. If you haven't done this before,
see [Configure AMPS to run Jira Core with additional applications installed](/server/framework/atlassian-sdk/configure-amps-to-run-jira-core-with-additional-applications-installed/) page.
1.  Make sure you have saved all your code changes to this point.
1.  In a Terminal window, navigate to the app root folder where the `pom.xml` file is located.
1.  Run the following command:

    ``` bash
    atlas-run
    ```

    Jira takes a few minutes to start, Terminal output will notify you of a successful build.

1.  In a browser window, go to Jira home page. The URL is indicated in the Terminal output,
such as [localhost:2990/jira](https://localhost:2990/jira).
1.  Log in using the default admin/admin credentials.
1.  Select a Scrum software development project as the new project to create.
Jira Software prompts you to create a project upon first login.
1.  Go to the board for the project that you created.
1.  Confirm that the Einstein Quotes app appears on the right side of the page, as shown on the image.

<img src="/server/jira/platform/images/viewtabquotes02.png" width="650" />

{{% tip %}}

Congratulations, that's it!

Have a treat!

{{% /tip %}}

## Next steps

As a next step, try adding another tab to your app. Notice that your context provider class is already built
for adding multiple tabs, so you need to add another Velocity template, icon file, and web-panel
module to the app descriptor.
