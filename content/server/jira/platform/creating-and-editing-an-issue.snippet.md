---
aliases:
- /server/jira/platform/creating-and-editing-an-issue-4227127.html
- /server/jira/platform/creating-and-editing-an-issue-4227127.md
category: devguide
confluence_id: 4227127
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=4227127
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=4227127
date: '2017-12-08'
legacy_title: Creating and Editing an Issue
platform: server
product: jira
subcategory: other
title: Creating and Editing an Issue
---
# Creating and Editing an Issue

 

 

 

{{% warning %}}

***This page has been archived, as it does not apply to the latest version of JIRA (Server or Cloud). The functionality described on this page may be unsupported or may not be present in JIRA at all.***

{{% /warning %}}

<table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<tbody>
<tr class="odd">
<td><p>Deprecated:</p></td>
<td><p>JIRA 4.1.<br />
If you are using JIRA 4.1 or later, please see <a href="/server/jira/platform/performing-issue-operations">Guide - Performing issue operations</a>.<br />
The documentation below applies to JIRA 4.0 and earlier.</p></td>
</tr>
</tbody>
</table>

## Retrieving an issue

You can retrieve the issue generic value using the following code:

``` javascript
// By Id
GenericValue issueGV1 = issueManager.getIssue(new Long(10000));
// By Issue Key
GenericValue issueGV2 = issueManager.getIssue("TST-1");
```

## Creating a new Issue

{{% note %}}

In order to create an issue, you need a MutableIssue object.  This can be obtained via the IssueFactory class.

``` javascript
MutableIssue issueObject = issueFactory.getIssue();
```

As mentioned below you can get a handle on an IssueFactory either by Constructor Injection or explicitly via a call like:

``` javascript
ComponentManager.getInstance().getIssueFactory()
```

{{% /note %}}

Here is sniplet of code that creates a new issue:

``` javascript
MutableIssue issueObject = issueFactory.getIssue();

// Regular Fields
issueObject.setProject(projectManager.getProject(new Long(10000)));
issueObject.setIssueType(constantsManager.getIssueType("1"));
issueObject.setSummary("Test Issue");
issueObject.setReporter(UserUtils.getUser("admin"));
issueObject.setAssignee(UserUtils.getUser("admin"));
issueObject.setPriority(constantsManager.getPriority("1"));
issueObject.setDescription("Test description");
issueObject.setAffectedVersions(EasyList.build(versionManager.getVersion(new Long(10000)), versionManager.getVersion(new Long(10001))));
issueObject.setFixVersions(EasyList.build(versionManager.getVersion(new Long(10002))));
issueObject.setComponents(EasyList.build(projectManager.getComponent(new Long(10000)), projectManager.getComponent(new Long(10001))));

// Custom Fields
CustomField customField = customFieldManager.getCustomFieldObject(new Long(10020));
issueObject.setCustomFieldValue(customField, "Test Value");

Map params = new HashMap();
params.put("issue", issueObject);
GenericValue issue = issueManager.createIssue(authenticationContext.getUser(), params);
```

This code example uses a lot of Manager objects. You can get a reference to them by declaring a [dependency](/server/jira/platform/picocontainer-and-jira.html) in the constructor of your plugin.

Classes not managed by Picocontainer (eg. workflow conditions / functions, Services and Listeners, or JSP scriptlets) can still get pico-instantiated objects statically using static methods on ComponentManager. For example:

``` javascript
final ProjectManager projectManager = ComponentManager.getInstance().getProjectManager();
final IssueFactory = ComponentManager.getInstance().getIssueFactory();
//or
final ApplicationProperties applicationProperties = ComponentManager.getComponentInstanceOfType(ApplicationProperties.class);
```

The code above also sets a value for a custom field on the issue. Please note that the value must be an object of the type that the Custom Field expects. As the above code was using a Text custom field, a simple `java.lang.String` is fine. 

## Editing an existing Issue

The code below edits a Due Date field of an issue and sets it to 24 hours from now. A comment that is visible by everyone who has permission to see the issue is also added.

``` javascript
MutableIssue issue = issueFactory.getIssue(issueGV);
issue.setDueDate(new Timestamp(System.currentTimeMillis() + 24*60*60*1000));
Map actionParams = EasyMap.build("issue", issue.getGenericValue(), "issueObject", issue, "remoteUser", authenticationContext.getUser());
actionParams.put("comment", "Test Comment");
actionParams.put("commentLevel", null);
ActionResult aResult = CoreFactory.getActionDispatcher().execute(ActionNames.ISSUE_UPDATE, actionParams);
```

You can also specify a group name as the `commentLevel` parameter to restrict the visibility of comments. If you use the above code to update an issue all the relevant change history entries will be made, and an Updated Issue Event generated.

The code above created an issue object from an issue `GenericValue`.

 

To learn how to update custom fields please see [Working with Custom Fields](/server/jira/platform/working-with-custom-fields.snippet).

## Events

It's worth noting that in the examples above, when the issues are created or modified events are fired, and any notifications that are associated with those events will be triggered.

