---
aliases:
- /server/jira/platform/preparing-for-jira-7.0-27557949.html
- /server/jira/platform/preparing-for-jira-7.0-27557949.md
category: devguide
confluence_id: 27557949
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=27557949
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=27557949
date: '2017-12-08'
legacy_title: Preparing for JIRA 7.0
platform: server
product: jira
subcategory: updates
title: Preparing for JIRA 7.0
---
# Preparing for JIRA 7.0

This page covers changes in **JIRA 7.0** that can affect add-on compatibility and functionality. General JIRA users should consult the documentation <a href="https://confluence.atlassian.com/display/JIRA/JIRA+Documentation" class="external-link">here</a>. As a rule, Atlassian makes every effort to inform add-on developers of known API changes **as far in advance as possible**. Where possible, we attach release targets. Unless otherwise indicated, every change on this page is expected to be released with the first public release of JIRA 7.0 on JIRA Server. 

We will update this page as development progresses, so please stay tuned for more updates on our [developer blog](https://developer.atlassian.com/developer.atlassian.com/blog), and check the** [JIRA 7.0 - API changes](/server/jira/platform/jira-7-0-api-changes), [JIRA 7.0 - Platform changes](/server/jira/platform/jira-7-0-platform-changes), and [JIRA 7.0 - General changes](/server/jira/platform/jira-7-0-general-changes)** **pages** **regularly **to keep on top of the changes.

**Release milestones**

-   For JIRA Server, the JIRA development team releases a number of EAP milestones prior to the final release, for customers and developers to keep abreast of upcoming changes. For more information on these releases, see [JIRA EAP Releases](/server/jira/platform/jira-eap-releases-35160569.html).
-   For JIRA Cloud, the JIRA team releases updates to JIRA Cloud on a weekly basis. Add-ons that integrate with JIRA Cloud with Atlassian Connect should use the JIRA REST API, which is subject to the <a href="https://developer.atlassian.com/display/HOME/Atlassian+REST+API+policy" class="external-link">Atlassian REST API Policy</a>.

## About JIRA 7.0

JIRA 7.0 introduces a fundamental change to JIRA: **applications**. Applications are now fundamental to JIRA's licensing, user management, and user interface.

-   **Licensing - **JIRA 7.0 eliminates the single JIRA license in favor of a base platform where multiple applications can be licensed individually. You can install one or more applications in JIRA via the *Manage applications* page. The base JIRA application, *JIRA Core*, provides basic project tracking capabilities for business teams. All JIRA applications include access to JIRA Core, or you can license JIRA Core only for users that need basic access to JIRA.
-   **User management -** Users consume a seat in a JIRA application license when they are granted access to a JIRA application. In JIRA Server, the new *Application access* page allows administrators to grant access to licensed applications to specific groups. 
-   **User interface -** In JIRA 7.0, we will be solidifying the concept of *project types* in JIRA, as well as expanding the capabilities of project types to provide application-specific features available in that project. Installing and licensing an application in JIRA allows user to create projects of that application's project type. For example, if there were an application called "JIRA Space Program," administrators could create new projects with the "Space Program" project type. Users with access to JIRA Space Program will have access to Space Program-specific features in Space Program projects.  

These changes will have a significant impact on APIs that retrieve information about the license status of the current user and the JIRA parent application license. Please see the notes about the new `ApplicationUser `API, `LicenseDetails`, and `JiraLicenseStore` in the pages listed below. Atlassian will provide more information about licensing and projects in JIRA 7.0 as we get closer to the public release date.

## Changes

The following pages contain all of the changes in JIRA 7.0:

-   [JIRA 7.0 - API changes](/server/jira/platform/jira-7-0-api-changes)
-   [JIRA 7.0 - Platform changes](/server/jira/platform/jira-7-0-platform-changes)
-   [JIRA 7.0 - General changes](/server/jira/platform/jira-7-0-general-changes)
