---
aliases:
- /server/jira/platform/preparing-for-jira-7.5-50614261.html
- /server/jira/platform/preparing-for-jira-7.5-50614261.md
category: devguide
confluence_id: 50614261
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=50614261
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=50614261
date: '2017-12-08'
legacy_title: Preparing for JIRA 7.5
platform: server
product: jira
subcategory: updates
title: Preparing for JIRA 7.5
---
# Preparing for JIRA 7.5

This page covers changes in **JIRA 7.5** that can affect add-on compatibility and functionality. This includes changes to the JIRA platform and the JIRA applications (JIRA Core, JIRA Software, JIRA Service Desk).

As a rule, Atlassian makes every effort to inform add-on developers of known API changes **as far in advance as possible**. Where possible, we attach release targets. Unless otherwise indicated, every change on this page is expected to be released with the first public release of the JIRA Server 7.5 products. We also make release milestones available prior to the release:

-   For JIRA Server, the JIRA development team releases a number of EAP milestones prior to the final release, for customers and developers to keep abreast of upcoming changes. For more information on these releases, see [JIRA EAP Releases](/server/jira/platform/jira-eap-releases-35160569.html).
-   For JIRA Cloud, the JIRA team releases updates to the JIRA Cloud products on a weekly basis. Add-ons that integrate with the JIRA Cloud products via Atlassian Connect should use the JIRA REST API, which is subject to the <a href="https://developer.atlassian.com/display/HOME/Atlassian+REST+API+policy" class="external-link">Atlassian REST API Policy</a>.

We will update this page as development progresses, so please stay tuned for more updates. We also recommend following the JIRA news on [Atlassian Developers blog](https://developer.atlassian.com/blog/categories/jira/) for important announcements and helpful articles.

## Summary

The risk level indicates the level of certainty we have that things will break if you are in the "Affected" column and you don't make the necessary changes.

<table>
<colgroup>
<col style="width: 30%" />
<col style="width: 70%" />
</colgroup>
<thead>
<tr class="header">
<th>Change. Platform/Application</th>
<th>Risk level. Affects</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>Change: Epics and versions panel in the Kanban backlog</p>
<p> Platform/Application: JIRA Software</p></td>
<td><p>Risk level: low</p>
<p>Affects: Mostly users.</p>
<p>All versions and epics will now be displayed in the epics and versions panels in the Kanban backlog. They're enabled by default for each board that enables (or has already enabled) the Kanban backlog.</p>
<p>The epics panel also changes how epics are displayed on the board - no longer as cards (like all other issues), but as filters in the epics panel. You can disable the epics panel in Board settings &gt; Columns, but the versions panel will always be in the backlog.</p></td>
</tr>
<tr class="even">
<td><p>Change: Renaming and deleting sprints, adding a sprint goal</p>
<p> Platform/Application: JIRA Software</p></td>
<td><p>Risk level: low</p>
<p>Affects: Users.</p>
<p>New functions regarding sprints - renaming and deleting sprints, and adding a sprint goal. Deleted sprints might influence reports.</p></td>
</tr>
<tr class="odd">
<td><p>Change: Events for creating and deleting issue links</p>
<p> Platform/Application: JIRA</p></td>
<td><p>Risk level: low</p>
<p>Affects: New events for creating and deleting issue links that might be used by plugin developers, or by users as webhooks in JIRA.</p></td>
</tr>
<tr class="even">
<td><p>Change: New JDBC driver for SQL Server</p>
<p> Platform/Application: JIRA</p></td>
<td><p>Risk level: medium</p>
<p>Affects: Users with Microsoft SQL Server as database. You won't be able to start JIRA after the upgrade until you run the JIRA config tool to update the URL pattern. Read below for details.</p></td>
</tr>
</tbody>
</table>

## JIRA platform changes

*All changes in JIRA platform are also relevant for JIRA Core, JIRA Software, and JIRA Service Desk.*

#### New JDBC driver for Microsoft SQL Server

We've shipped a new driver for Microsoft SQL Server, which requires that you update the driver's URL in the `dbconfig.xml` file. In most cases, it's enough to run the JIRA configuration tool, which will update the URL automatically, but if you've customized the URL by adding some properties to it, you might need to make the changes manually. 

**Running the JIRA configuration tool**

If you never customized the JDBC URL, run the JIRA config tool to update it.  
1. Go to `<installation-directory>/bin`, and run the `config.bat/config.sh` file.  
2. Click **Save**. Your configuration will be updated.  
3. Restart JIRA.

**Updating the URL manually**

If the configuration tool doesn't fix the error, update the URL manually.  
1. Go to JIRA's home directory, and edit the `dbconfig.xml` file.  
2. Adjust the value of the URL parameter to the following pattern:

``` xml
jdbc:sqlserver://[serverName[\instanceName][:portNumber]][;property=value[;property=value]]
```

For details about the URL and its properties, see <a href="https://docs.microsoft.com/en-us/sql/connect/jdbc/building-the-connection-url" class="external-link">Building the connection URL</a>.

When you start JIRA after the upgrade, you'll get an error message until you fix this issue.

#### Events for creating and deleting issue links

New events for creating and deleting issue links that might be used by plugin developers, or by users as webhooks in JIRA. The new events are <a href="https://docs.atlassian.com/jira/latest/com/atlassian/jira/event/issue/link/IssueLinkCreatedEvent.html" class="external-link">IssueLinkCreatedEvent</a> and <a href="https://docs.atlassian.com/jira/latest/com/atlassian/jira/event/issue/link/IssueLinkDeletedEvent.html" class="external-link">IssueLinkDeletedEvent</a>.

When creating webhooks, you can use the following events. The string in parentheses is the name of the webhookEvent in the response.

-   create (*issuelink\_created*)
-   deleted (*issuelink\_deleted*)

For more info about creating webhooks in JIRA, see [Webhooks](https://developer.atlassian.com/display/JIRADEV/Webhooks).

#### JIRA will fix corrupted indexes by itself

We've made this change already in 7.4.2, but we just wanted to make sure you know about it. JIRA now has an automatic recovery of corrupted indexes, which saves you a lot of manual work. However, the broken documents will be deleted from the indexes, and this might result in e.g. JQL not being able to find some issues. You can recognize that your indexes were being fixed by searching the `atlassian-jira.log` file for a line like this one:

``` bash
2017-07-18 23:51:49,990 JIRA-Bootstrap ERROR      [c.a.jira.index.LuceneCorruptionChecker] Index '/jirahome/caches/indexes/issues' is broken: 2 bad segments.
    Performing healing the index. 2 documents will be lost
```

All deleted documents can be recreated by running a reindex in JIRA. For more details, see <a href="https://confluence.atlassian.com/display/JIRAKB/Lucene+index+corruption" class="external-link">Lucene index corruption</a>. 

#### REST API changes

We've also made some changes to our APIs:

-   With `GET /rest/agile/1.0/sprint/{sprintId}`, you can now retrieve a new field called "goal", which shows a sprint's goal.
-   With `DELETE /rest/agile/1.0/sprint/{sprintId}`, you can now delete not only sprints that haven't started yet, but also the completed ones.
-   With `PUT /rest/agile/1.0/sprint/{sprintId}` and `POST /rest/agile/1.0/sprint/{sprintId}`, you can now use the new "goal" property to add a goal to a sprint.
-   With `POST /rest/agile/1.0/sprint/{sprintId}`, you can update the "name" and "goal" properties for any completed sprint.

## JIRA Core changes

#### No specific JIRA Core application changes.

## JIRA Software changes

#### Improvements to sprints - rename and delete, add a sprint goal

We've introduced some new options that will help you manage your sprints. You can add a goal to a sprint to let your team know what you're aiming for, delete an obsolete sprint so that it doesn't affect your reports and future planning, or rename a sprint to something that better describes it.

For more info about this change, see [JIRA Software 7.5 EAP release notes](/server/jira/platform/jira-software-7-5-eap-release-notes).

#### Epics and versions panels in the Kanban backlog

With JIRA 7.5, we're introducing two new panels in the Kanban backlog - versions and epics. They'll be enabled by default if you have previously enabled the Kanban backlog. The panels will help you refine your backlog, group your issues, and easily manage them by using versions and epics as filters. Because of this change, epics will no longer be displayed as cards on the board (like all other issues), but will be moved to the panel instead. If you don't like this change, you can disable the epics panel in **Boards settings &gt; Columns**. 

For more info about this change, see [JIRA Software 7.5 EAP release notes](/server/jira/platform/jira-software-7-5-eap-release-notes).

## JIRA Service Desk changes

JIRA Service Desk has a number of elements of it's API which are currently defined as experimental  but that is about to change. With an upcoming release of JIRA Service Desk, a number of the methods in our API will be moving out of the experimental state and into standard API state. For most of these components the API will remain unchanged, but for some there will be adjustments which will represent a breaking change. 

## What's Changing?

A number of components will have breaking changes. The changes are relatively minor and revolve around changes to IDs that will be returning integers rather than longs. This helps us to align with the values that we are currently storing in the database and will stop us having to do a lot of unnecessary type conversion in the future. 

The following will be changing in a forthcoming release (after JIRA 7.5.0):

<table>
<colgroup>
<col style="width: 20%" />
<col style="width: 40%" />
<col style="width: 40%" />
</colgroup>
<thead>
<tr class="header">
<th><div class="tablesorter-header-inner">
Classname
</div></th>
<th><div class="tablesorter-header-inner">
Current method signature
</div></th>
<th><div class="tablesorter-header-inner">
Replacement
</div></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>ServiceDeskService</td>
<td><p><code>Either&lt;<br>
&nbsp;&nbsp;AnError,<br>
&nbsp;&nbsp;ServiceDesk<br>
&gt;<br>
getServiceDeskById(<br>
&nbsp;&nbsp;ApplicationUser user,<br>
&nbsp;&nbsp;long serviceDeskId<br>
);</code></p>
<p><strong>Breaking change:</strong></p>
<p>The getServiceDeskById method will require an integer rather than a long as the serviceDeskId. </p></td>
<td><p><code>Either&lt;<br>
&nbsp;&nbsp;AnError,<br>
&nbsp;&nbsp;ServiceDesk<br>
&gt;<br>
getServiceDeskById(<br>
&nbsp;&nbsp;ApplicationUser user,<br>
&nbsp;&nbsp;int erviceDeskId<br>
);</code></p>
<p><strong>Current javadoc:</strong></p>
<p> <a href="https://docs.atlassian.com/jira-servicedesk/3.6.2-REL-0004/com/atlassian/servicedesk/api/ServiceDeskService.html#getServiceDeskById-com.atlassian.jira.user.ApplicationUser-long-" class="external-link">ServiceDeskService: getServiceDeskById</a> </p></td>
</tr>
<tr class="even">
<td>PortalService</td>
<td><p><code>Either&lt;<br>
&nbsp;&nbsp;AnError,<br>
&nbsp;&nbsp;Portal<br>
&gt;<br>
getPortalForId(<br>
&nbsp;&nbsp;ApplicationUser user,<br>
&nbsp;&nbsp;long portalId<br>
);</code></p>
<p><strong>Breaking change:</strong></p>
<p>The getPortalForId method will request and integer rather than a long as the portalId.</p></td>
<td><p><code>Either&lt;<br>
&nbsp;&nbsp;AnError,<br>
&nbsp;&nbsp;Portal<br>
&gt;<br>
getPortalForId(<br>
&nbsp;&nbsp;ApplicationUser user,<br>
&nbsp;&nbsp;int portalId<br>
);</code></p>
<p><strong>Current javadoc:</strong></p>
<p><a href="https://docs.atlassian.com/jira-servicedesk/3.6.2-REL-0004/com/atlassian/servicedesk/api/portal/PortalService.html#getPortalForId-com.atlassian.jira.user.ApplicationUser-long-" class="external-link">PortalService: getPortalForId</a> </p></td>
</tr>
<tr class="odd">
<td>Queue</td>
<td><p><code>long getId()</code></p>
<p><strong>Breaking change:</strong></p>
<p>The return value for this method will return an integer rather than a long.</p></td>
<td><p>int getId()</p>
<p><strong>Current javadoc:</strong></p>
<p><a href="https://docs.atlassian.com/jira-servicedesk/3.6.2-REL-0004/com/atlassian/servicedesk/api/queue/Queue.html#getId--" class="external-link">Queue: getId</a> </p></td>
</tr>
<tr class="even">
<td> </td>
<td><p><code>long getServiceDeskId()</code> </p>
<p><strong>Breaking change:</strong></p>
<p>The return value for this method will return an integer rather than a long.</p></td>
<td><p>int getServiceDeskId()</p>
<p><strong>Current javadoc:</strong></p>
<p><a href="https://docs.atlassian.com/jira-servicedesk/3.6.2-REL-0004/com/atlassian/servicedesk/api/queue/Queue.html#getServiceDeskId--" class="external-link">Queue: getServiceDeskId</a> </p></td>
</tr>
<tr class="odd">
<td>QueueQuery</td>
<td><p><code>Optional&lt;Long&gt; queueId();</code> </p>
<p><strong>Breaking change:</strong> </p>
<p>The return value for this method will return an integer rather than a long.</p></td>
<td><p>Optional&lt;Integer&gt; queueId();</p>
<p><strong>Current javadoc:</strong></p>
<p><a href="https://docs.atlassian.com/jira-servicedesk/3.6.2-REL-0004/com/atlassian/servicedesk/api/queue/QueueQuery.html#queueId--" class="external-link">QueueQuery: queueId</a> </p></td>
</tr>
<tr class="even">
<td> </td>
<td><p><code>long serviceDeskId();</code></p>
<p><strong>Breaking change:</strong></p>
<p>The return value for this method will return an integer rather than a long.</p></td>
<td><p><code>int serviceDeskId();</code></p>
<p><strong>Current javadoc:</strong></p>
<p><a href="https://docs.atlassian.com/jira-servicedesk/3.6.2-REL-0004/com/atlassian/servicedesk/api/queue/QueueQuery.html#serviceDeskId--" class="external-link">QueueQuery: serviceDeskId</a> </p></td>
</tr>
<tr class="odd">
<td>QueueQuery Builder interface</td>
<td><p><code>Builder serviceDeskId(<br>
&nbsp;&nbsp;long serviceDeskId<br>
);</code></p>
<p><strong>Breaking change:</strong></p>
<p>The serviceDeskID interface will require an integer rather than a long as the serviceDeskId</p></td>
<td><p><code>Builder serviceDeskId(<br>
&nbsp;&nbsp;int serviceDeskId<br>
);</code></p>
<p><strong>Current javadoc:</strong></p>
<p><a href="https://docs.atlassian.com/jira-servicedesk/3.6.2-REL-0004/com/atlassian/servicedesk/api/queue/QueueQuery.Builder.html#serviceDeskId-long-" class="external-link">QueueQuery.Builder: serviceDeskId</a> </p></td>
</tr>
<tr class="even">
<td></td>
<td><p><code>Builder queueId(<br>
&nbsp;&nbsp;long queueId<br>
);</code></p>
<p><strong>Breaking change:</strong></p>
<p>The queueId interface will require an integer rather than a long as the queueId</p></td>
<td><p><code>Builder queueId(<br>
&nbsp;&nbsp;int queueId<br>
);</code></p>
<p><strong>Current javadoc:</strong></p>
<p><a href="https://docs.atlassian.com/jira-servicedesk/3.6.2-REL-0004/com/atlassian/servicedesk/api/queue/QueueQuery.Builder.html#queueId-long-" class="external-link">QueueQuery.Builder: queueId</a> </p></td>
</tr>
<tr class="odd">
<td>QueueRequestQuery</td>
<td><p><code>long queueId();</code></p>
<p><strong>Breaking change:</strong></p>
<p>The return values for this method will return an integer rather than a long</p></td>
<td><p><code>int queueId();</code></p>
<p><strong>Current javadoc:</strong></p>
<p><a href="https://docs.atlassian.com/jira-servicedesk/3.6.2-REL-0004/com/atlassian/servicedesk/api/queue/QueueRequestQuery.html#queueId--" class="external-link">QueueRequestQuery: queueId</a> </p></td>
</tr>
<tr class="even">
<td> </td>
<td><p><code>long serviceDeskId();</code></p>
<p><strong>Breaking change:</strong></p>
<p>The return values for this method will return an integer rather than a long</p></td>
<td><p><code>int serviceDeskId();</code></p>
<p><strong>Current javadoc:</strong></p>
<p><a href="https://docs.atlassian.com/jira-servicedesk/3.6.2-REL-0004/com/atlassian/servicedesk/api/queue/QueueRequestQuery.html#serviceDeskId--" class="external-link">ueueRequestQuery: serviceDeskId</a> </p></td>
</tr>
<tr class="odd">
<td>QueueRequestQuery Builder interface</td>
<td><p><code>Builder serviceDeskId(<br>
&nbsp;&nbsp;long serviceDeskId<br>
);</code></p>
<p><strong>Breaking change:</strong></p>
<p>The serviceDeskID interface will require an integer rather than a long as the serviceDeskId</p></td>
<td><p><code>Builder serviceDeskId(<br>
&nbsp;&nbsp;int serviceDeskId<br>
);</code></p>
<p><strong>Current javadoc:</strong></p>
<p><a href="https://docs.atlassian.com/jira-servicedesk/3.6.2-REL-0004/com/atlassian/servicedesk/api/queue/QueueRequestQuery.Builder.html#serviceDeskId-long-" class="external-link">QueueRequestQuery: serviceDeskId</a> </p></td>
</tr>
<tr class="even">
<td> </td>
<td><p><code>Builder queueId(<br>
&nbsp;&nbsp;long queueId<br>
);</code></p>
<p><strong>Breaking change:</strong></p>
<p>The queueId interface will require an integer rather than a long as the queueId</p></td>
<td><p><code>Builder queueId(<br>
&nbsp;&nbsp;int queueId<br>
);</code></p>
<p><strong>Current javadoc:</strong></p>
<p><a href="https://docs.atlassian.com/jira-servicedesk/3.6.2-REL-0004/com/atlassian/servicedesk/api/queue/QueueRequestQuery.Builder.html#queueId-long-" class="external-link">QueueRequestQuery: queueId</a> </p></td>
</tr>
<tr class="odd">
<td>RequestType</td>
<td><p><code>long getPortalId();</code></p>
<p><strong>Breaking change:</strong></p>
<p>The getPortalId method will return an integer instead of a long.</p></td>
<td><p><code>int getPortalId();</code></p>
<p><strong>Current javadoc:</strong></p>
<p><a href="https://docs.atlassian.com/jira-servicedesk/3.6.2-REL-0004/com/atlassian/servicedesk/api/requesttype/RequestType.html#getPortalId--" class="external-link">RequestType: getPortalId</a> </p></td>
</tr>
<tr class="even">
<td>SlaInformation</td>
<td><p><code>long getId();</code> </p>
<p><strong>Breaking change:</strong></p>
<p>The getId method will return an integer instead of a long.</p></td>
<td><p><code>int getId();</code></p>
<p><strong>Current javadoc:</strong></p>
<p><a href="https://docs.atlassian.com/jira-servicedesk/3.6.2-REL-0004/com/atlassian/servicedesk/api/sla/info/SlaInformation.html#getId--" class="external-link">SlaInformation: getId</a></p></td>
</tr>
<tr class="odd">
<td>SlaInformationQuery</td>
<td><p><code>Optional&lt;Long&gt; id();</code></p>
<p><strong>Breaking change:</strong></p>
<p>The id method will return an integerinstead of a long.</p></td>
<td><p><code>Optional&lt;Integer&gt; id();</code></p>
<p><strong>Current javadoc:</strong></p>
<p><a href="https://docs.atlassian.com/jira-servicedesk/3.6.2-REL-0004/com/atlassian/servicedesk/api/sla/info/SlaInformationQuery.html#id--" class="external-link">SlaInformationQuery: id</a> </p></td>
</tr>
<tr class="even">
<td>SlaInformationQuery Builder</td>
<td><p><code>Builder id(<br>
&nbsp;&nbsp;Long slaMetricId<br>
);</code></p>
<p><strong>Breaking change:</strong></p>
<p>The id interface will require an integer instead of a long.</p></td>
<td><p><code>Builder id(<br>
&nbsp;&nbsp;Integer slaMetricId<br>
);</code></p>
<p><strong>Current javadoc:</strong></p>
<p><a href="https://docs.atlassian.com/jira-servicedesk/3.6.2-REL-0004/com/atlassian/servicedesk/api/sla/info/SlaInformationQuery.Builder.html#id-java.lang.Long-" class="external-link">SlaInformationQuery.Builder: id</a> </p></td>
</tr>
</tbody>
</table>

## What's deprecated? 

As well as the breaking changes listed above, we're also going to be deprecating a number of existing experimental API which will be removed in a future release. The following classes are being deprecated in preparation for removal:

-   `CreateInternalCommentParameters`
-   `CreatePublicCommentParameters`
-   `ServiceDeskCommentParameters`
-   `ValidatedInternalCommentParameters`
-   `ValidatedPublicCommentParameters`
-   `RequestTypeUpdateParameters`
-   `RequestTypeQueryParameters`

In addition, the following methods are being deprecated. Some of these have equivalents which can be used, and where this is the case these are documented.

<table>
<colgroup>
<col style="width: 20%" />
<col style="width: 40%" />
<col style="width: 40%" />
</colgroup>
<thead>
<tr class="header">
<th><div class="tablesorter-header-inner">
Classname
</div></th>
<th><div class="tablesorter-header-inner">
Method signature
</div></th>
<th><div class="tablesorter-header-inner">
Replacement
</div></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>ServiceDeskCommentService</td>
<td><p><code>Either&lt;<br>
&nbsp;&nbsp;AnError,<br>
&nbsp;&nbsp;ValidatedPublicCommentParameters<br>
&gt;<br>
validatePublicComment(<br>
&nbsp;&nbsp;@Nonnull<br>
&nbsp;&nbsp;&nbsp;&nbsp;ApplicationUser user,<br>
&nbsp;&nbsp;@Nonnull<br>
&nbsp;&nbsp;&nbsp;&nbsp;CreatePublicCommentParameters<br>
&nbsp;&nbsp;&nbsp;&nbsp;commentInternalParameters<br>
);</code></p></td>
<td><p><code>#createServiceDeskComment(<br>
&nbsp;&nbsp;ApplicationUser,<br>
&nbsp;&nbsp;ServiceDeskCommentCreateParameters<br>
)</code></p>
<p><strong>Javadoc:</strong></p>
<p><a href="https://docs.atlassian.com/jira-servicedesk/3.6.2-REL-0004/com/atlassian/servicedesk/api/project/creation/ServiceDeskCreationService.html" class="external-link">ServiceDeskCreationService</a> </p></td>
</tr>
<tr class="even">
<td> </td>
<td><p><code>Either&lt;<br>
&nbsp;&nbsp;AnError,<br>
&nbsp;&nbsp;ValidatedInternalCommentParameters<br>
&gt;<br>
validateInternalComment(<br>
&nbsp;&nbsp;@Nonnull<br>
&nbsp;&nbsp;&nbsp;&nbsp;ApplicationUser user,<br>
&nbsp;&nbsp;@Nonnull<br>
&nbsp;&nbsp;&nbsp;&nbsp;CreateInternalCommentParameters<br>
&nbsp;&nbsp;&nbsp;&nbsp;commentPublicParameters<br>
);</code></p></td>
<td><p><code>#createServiceDeskComment(<br>
&nbsp;&nbsp;ApplicationUser,<br>
&nbsp;&nbsp;ServiceDeskCommentCreateParameters<br>
)</code></p>
<p><strong>Javadoc:</strong></p>
<p><a href="https://docs.atlassian.com/jira-servicedesk/3.6.2-REL-0004/com/atlassian/servicedesk/api/project/creation/ServiceDeskCreationService.html" class="external-link">ServiceDeskCreationService</a> </p></td>
</tr>
<tr class="odd">
<td> </td>
<td><p><code>Either&lt;<br>
&nbsp;&nbsp;AnError,<br>
&nbsp;&nbsp;ServiceDeskComment<br>
&gt;<br>
createPublicComment(<br>
&nbsp;&nbsp;@Nonnull<br>
&nbsp;&nbsp;&nbsp;&nbsp;ApplicationUser user,<br>
&nbsp;&nbsp;@Nonnull<br>
&nbsp;&nbsp;&nbsp;&nbsp;ValidatedPublicCommentParameters<br>
&nbsp;&nbsp;&nbsp;&nbsp;commentParameters<br>
);</code></p></td>
<td><p><code>#createServiceDeskComment(<br>
&nbsp;&nbsp;ApplicationUser,<br>
&nbsp;&nbsp;ServiceDeskCommentCreateParameters<br>
)</code></p>
<p><strong>Javadoc:</strong></p>
<p><a href="https://docs.atlassian.com/jira-servicedesk/3.6.2-REL-0004/com/atlassian/servicedesk/api/project/creation/ServiceDeskCreationService.html" class="external-link">ServiceDeskCreationService</a> </p></td>
</tr>
<tr class="even">
<td> </td>
<td><p><code>Either&lt;<br>
&nbsp;&nbsp;AnError,<br>
&nbsp;&nbsp;ServiceDeskComment<br>
&gt;<br>
createInternalComment(<br>
&nbsp;&nbsp;@Nonnull<br>
&nbsp;&nbsp;&nbsp;&nbsp;ApplicationUser<br>
&nbsp;&nbsp;&nbsp;&nbsp;user,<br>
&nbsp;&nbsp;@Nonnull<br>
&nbsp;&nbsp;&nbsp;&nbsp;ValidatedInternalCommentParameters<br>
&nbsp;&nbsp;&nbsp;&nbsp;commentParameters<br>
);</code></p></td>
<td><p><code>#createServiceDeskComment(<br>
&nbsp;&nbsp;ApplicationUser,<br>
&nbsp;&nbsp;ServiceDeskCommentCreateParameters<br>
)</code></p>
<p><strong>Javadoc:</strong></p>
<p><a href="https://docs.atlassian.com/jira-servicedesk/3.6.2-REL-0004/com/atlassian/servicedesk/api/project/creation/ServiceDeskCreationService.html" class="external-link">ServiceDeskCreationService</a> </p></td>
</tr>
<tr class="odd">
<td>CustomerContextService</td>
<td><code>&lt;T&gt; T inCustomerContext(<br>
&nbsp;&nbsp;Callable&lt;T&gt; callable<br>
);</code></td>
<td><p><code>#runInCustomerContext(<br>
&nbsp;&nbsp;NoExceptionsCallable<br>
)</code></p>
<p><strong>Javadoc:</strong></p>
<p><a href="https://docs.atlassian.com/jira-servicedesk/3.6.2-REL-0004/com/atlassian/servicedesk/api/customer/CustomerContextService.html" class="external-link">CustomerContextService</a> </p></td>
</tr>
<tr class="even">
<td> </td>
<td><code>void inCustomerContext(<br>
&nbsp;&nbsp;Runnable runnable<br>
);</code></td>
<td><p><code>#runInCustomerContext(<br>
&nbsp;&nbsp;NoExceptionsCallable<br>
)</code></p>
<p><strong>Javadoc:</strong></p>
<p><a href="https://docs.atlassian.com/jira-servicedesk/3.6.2-REL-0004/com/atlassian/servicedesk/api/customer/CustomerContextService.html" class="external-link">CustomerContextService</a></p></td>
</tr>
<tr class="odd">
<td> </td>
<td><code>&lt;T&gt; T outOfCustomerContext(<br>
&nbsp;&nbsp;Callable&lt;T&gt; callable<br>
);</code></td>
<td><p><code>#runOutOfCustomerContext(<br>
&nbsp;&nbsp;NoExceptionsCallable<br>
)</code></p>
<p><strong>Javadoc:</strong></p>
<p><a href="https://docs.atlassian.com/jira-servicedesk/3.6.2-REL-0004/com/atlassian/servicedesk/api/customer/CustomerContextService.html" class="external-link">CustomerContextService</a></p></td>
</tr>
<tr class="even">
<td>ServiceDeskLicence</td>
<td><code>String getLicenseType();</code></td>
<td><p>This method is not applicable anymore and will always return ABP</p>
<p><strong>Javadoc:</strong></p>
<p><a href="https://docs.atlassian.com/jira-servicedesk/3.6.2-REL-0004/com/atlassian/servicedesk/api/license/ServiceDeskLicense.html" class="external-link">ServiceDeskLicense</a>  </p></td>
</tr>
<tr class="odd">
<td>ServiceDeskCustomerRequestService</td>
<td><p><code>Either&lt;<br>
&nbsp;&nbsp;AnError,<br>
&nbsp;&nbsp;CustomerRequest<br>
&gt;<br>
getRequestForKey(<br>
&nbsp;&nbsp;@Nullable<br>
&nbsp;&nbsp;&nbsp;&nbsp;ApplicationUser user,<br>
&nbsp;&nbsp;@Nonnull<br>
&nbsp;&nbsp;&nbsp;&nbsp;String issueKey<br>
);</code></p></td><br>
<td><p><code>#getCustomerRequests(<br>
&nbsp;&nbsp;ApplicationUser,<br>
&nbsp;&nbsp;CustomerRequestQuery<br>
)</code></p>
<p><strong>Javadoc:</strong></p>
<p><a href="https://docs.atlassian.com/jira-servicedesk/3.6.2-REL-0004/com/atlassian/servicedesk/api/request/ServiceDeskCustomerRequestService.html" class="external-link">ServiceDeskCustomerRequestService</a></p></td>
</tr>
<tr class="even">
<td></td>
<td><p><code>Either&lt;<br>
&nbsp;&nbsp;AnError,<br>
&nbsp;&nbsp;CustomerRequest<br>
&gt;<br>
getRequestForIssue(<br>
&nbsp;&nbsp;@Nullable<br>
&nbsp;&nbsp;&nbsp;&nbsp;ApplicationUser user,<br>
&nbsp;&nbsp;@Nonnull<br>
&nbsp;&nbsp;&nbsp;&nbsp;Issue issue<br>
);</code></p></td>
<td><p><code>#getCustomerRequests(<br>
&nbsp;&nbsp;ApplicationUser,<br>
&nbsp;&nbsp;CustomerRequestQuery<br>
)</code></p>
<p><strong>Javadoc:</strong></p>
<p><a href="https://docs.atlassian.com/jira-servicedesk/3.6.2-REL-0004/com/atlassian/servicedesk/api/request/ServiceDeskCustomerRequestService.html" class="external-link">ServiceDeskCustomerRequestService</a>  </p></td>
</tr>
<tr class="odd">
<td></td>
<td><code>RequestTypeUpdateParameters<br>
&nbsp;&nbsp;.Builder<br>
&nbsp;&nbsp;&nbsp;&nbsp;requestTypeUpdateParametersBuilder();</code></td>
<td><p><code>#newUpdateBuilder()</code></p>
<p><strong>Javadoc:</strong></p>
<p><a href="https://docs.atlassian.com/jira-servicedesk/3.6.2-REL-0004/com/atlassian/servicedesk/api/request/ServiceDeskCustomerRequestService.html" class="external-link">ServiceDeskCustomerRequestService</a>  </p></td>
</tr>
<tr class="even">
<td></td>
<td><p><code>Either&lt;<br>
&nbsp;&nbsp;AnError,<br>
&nbsp;&nbsp;CustomerRequest<br>
&gt;<br>
updateRequestType(<br>
&nbsp;&nbsp;@Nullable<br>
&nbsp;&nbsp;&nbsp;&nbsp;ApplicationUser user,<br>
&nbsp;&nbsp;RequestTypeUpdateParameters<br>
&nbsp;&nbsp;&nbsp;&nbsp;requestTypeUpdateParameters<br>
);</code></p></td>
<td><p><code>#updateCustomerRequest(<br>
&nbsp;&nbsp;ApplicationUser,<br>
&nbsp;&nbsp;CustomerRequestUpdateParameters<br>
)</code></p>
<p><strong>Javadoc:</strong></p>
<p><a href="https://docs.atlassian.com/jira-servicedesk/3.6.2-REL-0004/com/atlassian/servicedesk/api/request/ServiceDeskCustomerRequestService.html" class="external-link">ServiceDeskCustomerRequestService</a></p></td>
</tr>
<tr class="odd">
<td> </td>
<td><code>Either&lt;<br>
&nbsp;&nbsp;AnError,<br>
&nbsp;&nbsp;CustomerRequest<br>
&gt;<br>
getRequestForIssueOverrideSecurity(<br>
&nbsp;&nbsp;Issue issue<br>
);</code></td>
<td><p><code>#getCustomerRequests(<br>
&nbsp;&nbsp;ApplicationUser,<br>
&nbsp;&nbsp;CustomerRequestQuery<br>
)</code></p>
<p><strong>Javadoc:</strong></p>
<p><a href="https://docs.atlassian.com/jira-servicedesk/3.6.2-REL-0004/com/atlassian/servicedesk/api/request/ServiceDeskCustomerRequestService.html" class="external-link">ServiceDeskCustomerRequestService</a> </p></td>
</tr>
<tr class="even">
<td> </td>
<td><code>Either&lt;<br>
&nbsp;&nbsp;AnError,<br>
&nbsp;&nbsp;CustomerRequest<br>
&gt;<br>
getRequestForKeyOverrideSecurity(<br>
&nbsp;&nbsp;String issueKey<br>
);</code></td>
<td><p><code>#getCustomerRequests(<br>
&nbsp;&nbsp;ApplicationUser,<br>
&nbsp;&nbsp;CustomerRequestQuery<br>
)</code></p>
<p><strong>Javadoc:</strong></p>
<p><a href="https://docs.atlassian.com/jira-servicedesk/3.6.2-REL-0004/com/atlassian/servicedesk/api/request/ServiceDeskCustomerRequestService.html" class="external-link">ServiceDeskCustomerRequestService</a></p></td>
</tr>
<tr class="odd">
<td>RequestTypeGroup</td>
<td><code>RequestTypeGroup withOrder(<br>
&nbsp;&nbsp;Option&lt;Integer&gt; order<br>
);</code></td>
<td><p>This deprecated and Experimental API will be removed in a future release</p>
<p><strong>Javadoc:</strong></p>
<p><a href="https://docs.atlassian.com/jira-servicedesk/3.6.2-REL-0004/com/atlassian/servicedesk/api/requesttype/RequestTypeGroup.html" class="external-link">RequestTypeGroup</a></p></td>
</tr>
<tr class="even">
<td>RequestTypeService</td>
<td><p><code>Either&lt;<br>
&nbsp;&nbsp;AnError,<br>
&nbsp;&nbsp;List&lt;<br>
&nbsp;&nbsp;&nbsp;&nbsp;RequestType<br>
&nbsp;&nbsp;&gt;<br>
&gt;<br>
getRequestTypes(<br>
&nbsp;&nbsp;@Nonnull<br>
&nbsp;&nbsp;&nbsp;&nbsp;ApplicationUser user,<br>
&nbsp;&nbsp;@Nonnull<br>
&nbsp;&nbsp;&nbsp;&nbsp;RequestTypeQueryParameters<br>
&nbsp;&nbsp;&nbsp;&nbsp;requestTypeQueryParameters<br>
);</code></p></td>
<td><p><code>#getRequestTypes(<br>
&nbsp;&nbsp;ApplicationUser,<br>
&nbsp;&nbsp;RequestTypeQuery<br>
)</code></p>
<p><strong>Javadoc:</strong></p>
<p><a href="https://docs.atlassian.com/jira-servicedesk/3.6.2-REL-0004/com/atlassian/servicedesk/api/requesttype/RequestTypeService.html" class="external-link">RequestTypeService</a></p></td>
</tr>
<tr class="odd">
<td> </td>
<td><code>RequestTypeQueryParameters<br>
&nbsp;&nbsp;.Builder queryBuilder();</code></td>
<td><p><code>#newQueryBuilder()</code></p>
<p><strong>Javadoc:</strong></p>
<p><a href="https://docs.atlassian.com/jira-servicedesk/3.6.2-REL-0004/com/atlassian/servicedesk/api/requesttype/RequestTypeService.html" class="external-link">RequestTypeService</a></p></td>
</tr>
<tr class="even">
<td> </td>
<td><p><code>Either&lt;<br>
&nbsp;&nbsp;AnError,<br>
&nbsp;&nbsp;RequestType<br>
&gt;<br>
getRequestTypeForRequest(<br>
&nbsp;&nbsp;@Nonnull<br>
&nbsp;&nbsp;&nbsp;&nbsp;ApplicationUser user,<br>
&nbsp;&nbsp;@Nonnull<br>
&nbsp;&nbsp;&nbsp;&nbsp;CustomerRequest<br>
&nbsp;&nbsp;&nbsp;&nbsp;customerRequest<br>
);</code></p></td>
<td><p><code>#getRequestTypes(<br>
&nbsp;&nbsp;ApplicationUser,<br>
&nbsp;&nbsp;RequestTypeQuery<br>
)</code></p>
<p><strong>Javadoc:</strong></p>
<p><a href="https://docs.atlassian.com/jira-servicedesk/3.6.2-REL-0004/com/atlassian/servicedesk/api/requesttype/RequestTypeService.html" class="external-link">RequestTypeService</a></p></td>
</tr>
<tr class="odd">
<td> </td>
<td><p><code>Either&lt;<br>
&nbsp;&nbsp;AnError,<br>
&nbsp;&nbsp;RequestType<br>
&gt;<br>
getRequestTypeForIssue(<br>
&nbsp;&nbsp;@Nonnull<br>
&nbsp;&nbsp;&nbsp;&nbsp;ApplicationUser user,<br>
&nbsp;&nbsp;@Nonnull<br>
&nbsp;&nbsp;&nbsp;&nbsp;Issue issue<br>
);</code></p></td>
<td><p><code>#getRequestTypes(<br>
&nbsp;&nbsp;ApplicationUser,<br>
&nbsp;&nbsp;RequestTypeQuery<br>
)</code></p>
<p><strong>Javadoc:</strong></p>
<p><a href="https://docs.atlassian.com/jira-servicedesk/3.6.2-REL-0004/com/atlassian/servicedesk/api/requesttype/RequestTypeService.html" class="external-link">RequestTypeService</a></p></td>
</tr>
<tr class="even">
<td> </td>
<td><code>Either&lt;<br>
&nbsp;&nbsp;AnError,<br>
&nbsp;&nbsp;List&lt;<br>
&nbsp;&nbsp;&nbsp;&nbsp;RequestType<br>
&nbsp;&nbsp;&gt;<br>
&gt;<br>
getAllRequestTypes(<br>
&nbsp;&nbsp;ApplicationUser user<br>
);</code></td>
<td><p><code>#getRequestTypes(<br>
&nbsp;&nbsp;ApplicationUser,<br>
&nbsp;&nbsp;RequestTypeQuery<br>
)</code></p>
<p><strong>Javadoc:</strong></p>
<p><a href="https://docs.atlassian.com/jira-servicedesk/3.6.2-REL-0004/com/atlassian/servicedesk/api/requesttype/RequestTypeService.html" class="external-link">RequestTypeService</a> </p></td>
</tr>
<tr class="odd">
<td> </td>
<td><p><code>Either&lt;<br>
&nbsp;&nbsp;AnError,<br>
&nbsp;&nbsp;RequestType<br>
&gt;<br>
getRequestTypeForRequestOverrideSecurity(<br>
&nbsp;&nbsp;@Nonnull<br>
&nbsp;&nbsp;&nbsp;&nbsp;CustomerRequest<br>
&nbsp;&nbsp;&nbsp;&nbsp;customerRequest<br>
);</code></p></td>
<td><p><code>#getRequestTypes(<br>
&nbsp;&nbsp;ApplicationUser,<br>
&nbsp;&nbsp;RequestTypeQuery<br>
)</code></p>
<p><strong>Javadoc:</strong></p>
<p><a href="https://docs.atlassian.com/jira-servicedesk/3.6.2-REL-0004/com/atlassian/servicedesk/api/requesttype/RequestTypeService.html" class="external-link">RequestTypeService</a>  </p></td>
</tr>
<tr class="even">
<td> </td>
<td><p><code>Either&lt;<br>
&nbsp;&nbsp;AnError,<br>
&nbsp;&nbsp;RequestType<br>
&gt;<br>
getRequestTypeForIssueOverrideSecurity(<br>
&nbsp;&nbsp;@Nonnull Issue issue<br>
);</code></p></td>
<td><p><code>#getRequestTypes(<br>
&nbsp;&nbsp;ApplicationUser,<br>
&nbsp;&nbsp;RequestTypeQuery<br>
)</code></p>
<p><strong>Javadoc:</strong></p>
<p><a href="https://docs.atlassian.com/jira-servicedesk/3.6.2-REL-0004/com/atlassian/servicedesk/api/requesttype/RequestTypeService.html" class="external-link">RequestTypeService</a> </p></td>
</tr>
<tr class="odd">
<td> </td>
<td><p><code>Either&lt;<br>
&nbsp;&nbsp;AnError,<br>
&nbsp;&nbsp;RequestType<br>
&gt;<br>
createRequestType(<br>
&nbsp;&nbsp;long projectId,<br>
&nbsp;&nbsp;int issueTypeId,<br>
&nbsp;&nbsp;String name,<br>
&nbsp;&nbsp;String description,<br>
&nbsp;&nbsp;long iconId,<br>
&nbsp;&nbsp;List Integer groupIds<br>
);</code></p></td>
<td><p><code>#createRequestType(<br>
&nbsp;&nbsp;ApplicationUser,<br>
&nbsp;&nbsp;RequestTypeCreateParameters<br>
)</code></p>
<p><strong>Javadoc:</strong></p>
<p><a href="https://docs.atlassian.com/jira-servicedesk/3.6.2-REL-0004/com/atlassian/servicedesk/api/requesttype/RequestTypeService.html" class="external-link">RequestTypeService</a></p></td>
</tr>
<tr class="even">
<td> </td>
<td><p><code>Either&lt;<br>
&nbsp;&nbsp;AnError,<br>
&nbsp;&nbsp;List&lt;<br>
&nbsp;&nbsp;&nbsp;&nbsp;RequestType<br>
&nbsp;&nbsp;&gt;<br>
&gt;<br>
getRequestTypesForGroup(<br>
&nbsp;&nbsp;Integer groupId,<br>
&nbsp;&nbsp;Long projectId<br>
);</code></p></td>
<td><p><code>#getRequestTypes(<br>
&nbsp;&nbsp;ApplicationUser,<br>
&nbsp;&nbsp;RequestTypeQuery<br>
)</code></p>
<p><strong>Javadoc:</strong></p>
<p><a href="https://docs.atlassian.com/jira-servicedesk/3.6.2-REL-0004/com/atlassian/servicedesk/api/requesttype/RequestTypeService.html" class="external-link">RequestTypeService</a></p></td>
</tr>
<tr class="odd">
<td> </td>
<td><p><code>Either&lt;<br>
&nbsp;&nbsp;AnError,<br>
&nbsp;&nbsp;List&lt;<br>
&nbsp;&nbsp;&nbsp;&nbsp;RequestType<br>
&nbsp;&nbsp;&gt;<br>
&gt;<br>
getRequestTypesByGroup(<br>
&nbsp;&nbsp;Integer groupId,<br>
&nbsp;&nbsp;Integer serviceDeskId<br>
);</code></p></td>
<td><p><code>#getRequestTypes(<br>
&nbsp;&nbsp;ApplicationUser,<br>
&nbsp;&nbsp;RequestTypeQuery<br>
)</code></p>
<p><strong>Javadoc:</strong></p>
<p><a href="https://docs.atlassian.com/jira-servicedesk/3.6.2-REL-0004/com/atlassian/servicedesk/api/requesttype/RequestTypeService.html" class="external-link">RequestTypeService</a></p></td>
</tr>
<tr class="even">
<td> </td>
<td><p><code>Either&lt;<br>
&nbsp;&nbsp;AnError,<br>
&nbsp;&nbsp;RequestType<br>
&gt;<br>
getRequestTypeByIdAndProjectId(<br>
&nbsp;&nbsp;Integer requestTypeId,<br>
&nbsp;&nbsp;Long projectId<br>
);</code></p></td>
<td><p><code>#getRequestTypes(<br>
&nbsp;&nbsp;ApplicationUser,<br>
&nbsp;&nbsp;RequestTypeQuery<br>
)</code></p>
<p><strong>Javadoc:</strong></p>
<p><a href="https://docs.atlassian.com/jira-servicedesk/3.6.2-REL-0004/com/atlassian/servicedesk/api/requesttype/RequestTypeService.html" class="external-link">RequestTypeService</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><code>Either&lt;<br>
&nbsp;&nbsp;AnError,<br>
&nbsp;&nbsp;RequestType<br>
&gt;<br>
deleteRequestTypeByIdAndProjectId(<br>
&nbsp;&nbsp;Integer requestTypeId,<br>
&nbsp;&nbsp;Long projectId<br>
);</code></p></td>
<td><p><code>#deleteRequestType(<br>
&nbsp;&nbsp;ApplicationUser,<br>
&nbsp;&nbsp;RequestTypeDeleteParameters<br>
)</code></p>
<p><strong>Javadoc:</strong></p>
<p><a href="https://docs.atlassian.com/jira-servicedesk/3.6.2-REL-0004/com/atlassian/servicedesk/api/requesttype/RequestTypeService.html" class="external-link">RequestTypeService</a></p></td>
</tr>
<tr class="even">
<td> </td>
<td><p><code>Either&lt;<br>
&nbsp;&nbsp;AnError,<br>
&nbsp;&nbsp;RequestType<br>
&gt;<br>
updateRequestTypeByIdAndProjectId(<br>
&nbsp;&nbsp;Integer requestTypeId,<br>
&nbsp;&nbsp;Long projectId,<br>
&nbsp;&nbsp;RequestType requestType<br>
);</code></p></td>
<td><p><code>#updateRequestType(<br>
&nbsp;&nbsp;ApplicationUser,<br>
&nbsp;&nbsp;RequestTypeUpdateParameters<br>
)</code></p>
<p><strong>Javadoc:</strong></p>
<p><a href="https://docs.atlassian.com/jira-servicedesk/3.6.2-REL-0004/com/atlassian/servicedesk/api/requesttype/RequestTypeService.html" class="external-link">RequestTypeService</a> </p></td>
</tr>
<tr class="odd">
<td>FpKit</td>
<td><p><code>Callable&lt;Unit&gt;<br>
runnableToCallable(<br>
&nbsp;&nbsp;Runnable runnable<br>
)</code></p></td>
<td><p>This deprecated and Experimental API will be removed in a future release</p>
<p><strong>Javadoc:</strong></p>
<p><a href="https://docs.atlassian.com/jira-servicedesk/3.6.2-REL-0004/com/atlassian/servicedesk/api/util/fp/FpKit.html" class="external-link">FpKit</a></p></td>
</tr>
</tbody>
</table>
