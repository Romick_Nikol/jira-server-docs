---
aliases:
- /server/jira/platform/how-to-migrate-a-plugin-from-jira-4.x-to-jira-5.0-6848537.html
- /server/jira/platform/how-to-migrate-a-plugin-from-jira-4.x-to-jira-5.0-6848537.md
category: devguide
confluence_id: 6848537
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=6848537
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=6848537
date: '2017-12-08'
legacy_title: How to migrate a plugin from JIRA 4.x to JIRA 5.0
platform: server
product: jira
subcategory: updates
title: How to migrate a plugin from JIRA 4.x to JIRA 5.0
---
# How to migrate a plugin from JIRA 4.x to JIRA 5.0

This document tells you how to convert a plugin that runs in version 4.x of JIRA, so that it will run in version 5.0 of JIRA.

Please refer also to the [plugin developer notes for JIRA 5.0](/server/jira/platform/preparing-for-jira-5-0), which are updated with each beta release.

## Overview

The most important changes for plugin developers are:

-   Removal of OSUser objects and replacement with Crowd User objects
-   Upgrade to Lucene 3.x
-   Cleanup of CustomFieldType

These changes are detailed in [Java API Changes in JIRA 5.0](/server/jira/platform/java-api-changes-in-jira-5-0).

## Compatibility

In addition to the above changes, a number of concrete/abstract classes have been converted to interfaces.

Because of these changes, it is unlikely that you will be able to create a single artefact (JAR file) that is compatible with both JIRA 5.x and JIRA 4.x.

We recommend that you create a separate branch in your plugin's version control system (VCS) for JIRA 4.x compatibility, and convert "trunk" to JIRA 5.0 compatibility.

## Recompile Plugin against JIRA 5.0

Since a number of concrete/abstract classes have been converted to interfaces, you may receive an error similar to the following if you attempt to run your JIRA 4.x-compatible plugin in JIRA 5.0.

``` bash
java.lang.IncompatibleClassChangeError: Found interface name.of.interface, but class was expected
```

For example,

``` bash
java.lang.IncompatibleClassChangeError: Found interface com.atlassian.jira.issue.link.IssueLinkType, but class was expected
```

If you do receive an error like this, then you need to recompile your plugin against JIRA 5.0 to ensure that your plugin utilises the new interface implementations in JIRA 5.0.

## Strategy for OSUser

The OSUser changes are likely to be have the biggest effect throughout your plugin.

The "Crowd Embedded" library was introduced in JIRA in 4.3.

At that time the old OSUser API was also maintained as a parallel API that could be used for user management. Existing plugin points (for example, `IssueTabPanel`) that had dependencies on OSUser were left alone for binary compatibility with JIRA 4.2.

Because of this, it is possible to do much of your migration from OSUser to Crowd Users while compiling against JIRA 4.3 or 4.4. This allows those with "large" plugins to take a more gradual approach to the migration.

## Steps for Gradual Migration

**Note:** Following all the steps below is primarily recommended for those with a large code base, where a gradual migration is preferred. For many small to medium-sized code bases, it is probably simple enough to just branch and move straight to JIRA 5.0 without the pre-migration on JIRA 4.4.

If you wish to maintain a JIRA 4.2 or lower compatible branch:

1.  **Branch your VCS.**
2.  **Update "trunk" dependency to JIRA 4.4.**  
    As a stepping stone, we can do much of the migration will still remaining in a compilable state.  
    While you are here, we recommend that you change the dependency name to "jira-core" instead of "atlassian-jira". Atlassian-jira is a "shaded jar" copy of jira-core that is provided just for back compatibility.
3.  **Replace usage of deprecated API.**  
    This is mostly about replacing OSUser with Crowd User. You will not be able to replace every usage, but you should be able to do the majority of the work.  
    The Javadoc in JIRA 4.4 will give you alternatives. For example, replace User.getGroups() with GroupManager.getGroupNamesForUser(userName)  
    Note that the OSUser class implements the Crowd User interface. There is also a class called "OSUserConverter" that will help you convert from a Crowd User object to an OSUser.
4.  **Test your changes.**  
    You will still be able to compile, run tests, and install your plugin as you go.
5.  **Update dependency to JIRA 5.0.**  
    Now you will get compilation failures.  
    Mostly you will need to replace `import com.opensymphony.user.User;` with `import com.atlassian.crowd.embedded.api.User;`  
    See [Java API Changes in JIRA 5.0](/server/jira/platform/java-api-changes-in-jira-5-0) for information on other changes, or see the alternatives listed in the <a href="http://docs.atlassian.com/jira/4.4.3/" class="external-link">JIRA 4.4 javadoc</a>.
6.  **Test your changes.**

If you only wish to maintain a JIRA 4.3 or higher compatible branch:

1.  **Do not branch yet.**
2.  **Replace usage of deprecated API.**  
    These changes will be on both branch and trunk.  
    This is mostly about replacing OSUser with Crowd User. You will not be able to replace every usage, but you should be able to do the majority of the work.  
    The Javadoc will give you alternatives. For example, replace User.getGroups() with GroupManager.getGroupNamesForUser(userName)  
    Note that the OSUser class implements the Crowd User interface. There is also a class called "OSUserConverter" that will help you convert from a Crowd User object to an OSUser.
3.  **Test your changes.**  
    You will still be able to compile, run tests, and install your plugin as you go.
4.  **Branch your VCS.**
5.  **(Optional) Update "trunk" dependency to JIRA 4.4.**  
    If branch is on JIRA 4.3, it may be helpful to first move trunk to version 4.4 in order to try to remove more deprecated APIs.
6.  **Update "trunk" dependency to JIRA 5.0.**  
    While you are here, we recommend that you change the dependency name to "jira-core" instead of "atlassian-jira". Atlassian-jira is a "shaded jar" copy of jira-core that is provided just for back compatibility.  
    After re-importing from Maven, you will get compilation failures.  
    Mostly you will need to replace `import com.opensymphony.user.User;` with `import com.atlassian.crowd.embedded.api.User;`  
    See [Java API Changes in JIRA 5.0](/server/jira/platform/java-api-changes-in-jira-5-0) for information on other changes, or see the alternatives listed in the <a href="http://docs.atlassian.com/jira/4.4.3/" class="external-link">JIRA 4.4 javadoc</a>.
7.  **Test your changes.**

## Next Steps

In JIRA 5.0 we have separated the Java API away from the internal implementation in order to help maintain a predictable compatibility policy. See [Java API Policy for JIRA](/server/jira/platform/java-api-policy-for-jira-4227213.html).

You can change your dependency to "jira-api" instead of "jira-core"/"atlassian-jira" to ensure you only use API classes.
