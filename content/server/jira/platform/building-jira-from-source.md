---
aliases:
- /server/jira/platform/building-jira-from-source-33749624.html
- /server/jira/platform/building-jira-from-source-33749624.md
category: devguide
confluence_id: 33749624
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=33749624
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=33749624
date: '2018-06-01'
legacy_title: Building JIRA from source
platform: server
product: jira
subcategory: learning
title: "Building Jira from source"
---
# Building Jira from source

<table>
<colgroup>
<col style="width: 20%" />
<col style="width: 80%" />
</colgroup>
<tbody>
<tr class="odd">
<td><p>Status:</p></td>
<td><p><strong>LEGACY.</strong> This tutorial applies to Jira versions that have reached end of life.</p></td>
</tr>
</tbody>
</table>

Commercial users at any level receive access to Jira's source code (note, evaluation license holders are not permitted access to Jira's source code). This topic explains how to build this source code into a deployable Jira application.

Building all of Jira from source is only necessary if you need to make extensive modifications to Jira's source code and are using a <a href="https://en.wikipedia.org/wiki/WAR_(file_format)" class="external-link">WAR approach</a>.

You should not need to rebuild Jira if:

-   You need to change many JSP files. JSP files are the template files for many Jira webpages. They can also be more easily changed directly in the standalone Jira installation directory.
-   Create a Jira plugin that adds functionality to Jira. For more information, read the [Jira Plugin Guide](/server/jira/platform/getting-started-with-plugins2-35717809.html). Changes and enhancements to Jira's functionality can often be made with Jira plugins without requiring core Jira source code modifications.
-   Recompiling a small number of source files can be done using the instructions in the standalone external-source directory.

{{% tip %}}

If you want to browse through the Jira Core source code, you can download it from our <a href="https://my.atlassian.com/download/source/jira-core" class="external-link">Core source downloads</a> page. You must have a <a href="https://www.atlassian.com/licensing/purchase-licensing#source-1" class="external-link">valid Jira license</a> and be logged in to <a href="http://my.atlassian.com/" class="external-link">my.atlassian.com</a> with your Atlassian Id. You can download the Jira Software source code (this does not contain the Jira Core source code) from the <a href="https://my.atlassian.com/download/source/jira" class="external-link">Software source downloads</a> page.

{{% /tip %}}

## Before you begin

-   Be aware that the procedures described on this page are not included in our <a href="https://confluence.atlassian.com/display/Support/Atlassian+Support+Offerings" class="external-link">Atlassian Support Offerings</a> and consequently Atlassian **cannot guarantee support for them**. This material is provided for your information only and using it is done so at your own risk.
-   The following instructions were tested for Jira 7.4.0.
-   There is a known issue where the Workflow Designer source is missing from the Jira source code (see <a href="https://jira.atlassian.com/browse/JRA-29615" class="external-link">JRA-29615</a>) - please keep this in mind when building Jira from source.

## Building a Jira WAR file from a Jira Source release

1.  Ensure you have JDK 1.8 or higher.
2.  Download Maven 3.2.5 or higher from the <a href="http://archive.apache.org/dist/maven/binaries/" class="external-link">Apache archives</a> of the <a href="http://maven.apache.org/" class="external-link">Maven website</a>.
3.  Ensure the JAVA\_HOME environment variable is set and points to your JDK installation. For example,  
    **On Windows:**

    ``` bash
    > set JAVA_HOME=[your JDK installation directory]
    ```

{{% tip %}}

Alternatively, the Windows environment variables can be configured by choosing `My Computer > Properties > Advanced > Environment Variables`.

{{% /tip %}}

**On Mac/Linux:**

``` bash
export JAVA_HOME=[your JDK installation directory]
```

4.  Extract Maven to an appropriate location on your operating system. For example,  
    **On Windows, extract to:**

    ``` bash
    C:\apache-maven-3.2.5
    ```

    **On Mac/Linux, extract to:**

    ``` bash
    /usr/local/apache-maven-3.2.5
    ```

5.  Add Maven's bin directory to your path. For example,  
    **On Windows:**

    ``` bash
    > set PATH=C:\apache-maven-3.2.5\bin
    ```

{{% tip %}}

Again, you can also set this via `My Computer > Properties > Advanced > Environment Variables`.

{{% /tip %}}

**On Mac/Linux:**

``` bash
export PATH=$PATH:/usr/local/apache-maven-3.2.5/bin
```

## Third-party libraries

Install all of the following restricted third-party (`.jar`) libraries to your local Maven repository (`.m2`), ensuring that you download the version specified below. All of these libraries are required to successfully build Jira from source. If any of these libraries are missing, the build process will fail.

{{% note %}}

Due to licensing restrictions, we can't distribute these libraries from Atlassian's public Maven repository. If you have built previous versions of Jira from source, you may already have some of these libraries in your local Maven repository.

{{% /note %}}

<table>
<colgroup>
<col style="width: 20%" />
<col style="width: 80%" />
</colgroup>
<thead>
<tr class="header">
<th><p>Libraries</p></th>
<th><p>Download URL</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>jms</p></td>
<td><p><a href="http://repository.jboss.org/nexus/content/groups/public-jboss/javax/jms/jms/1.1/" class="uri external-link">http://repository.jboss.org/nexus/content/groups/public-jboss/javax/jms/jms/1.1/</a></p>
<p><strong>Maven groupId and artifactId:</strong> javax.jms:jms.</p>
<p><strong>Version:</strong> 1.1</p></td>
</tr>
<tr class="even">
<td><p>jmxri and jmxtools</p></td>
<td><p><a href="http://www.oracle.com/technetwork/java/javasebusiness/downloads/java-archive-downloads-java-plat-419418.html#7657-jmx-1.2.1-oth-JPR" class="uri external-link">http://www.oracle.com/technetwork/java/javasebusiness/downloads/java-archive-downloads-java-plat-419418.html#7657-jmx-1.2.1-oth-JPR</a></p>
<p><strong>Maven groupId and artifactId:</strong> com.sun.jmx:jmxri and com.sun.jdmk:jmxtools.</p>
<p> <strong><strong>Version:</strong> </strong>1.2.1</p></td>
</tr>
<tr class="odd">
<td><p>jndi</p></td>
<td><p><a href="http://www.oracle.com/technetwork/java/javasebusiness/downloads/java-archive-downloads-java-plat-419418.html#7110-jndi-1.2.1-oth-JPR" class="uri external-link">http://www.oracle.com/technetwork/java/javasebusiness/downloads/java-archive-downloads-java-plat-419418.html#7110-jndi-1.2.1-oth-JPR</a></p>
<p>(Download 'JNDI 1.2.1 Class Libraries' under 'Java Naming and Directory Interface 1.2.1')</p>
<p><strong>Maven groupId and artifactId:</strong> jndi:jndi.</p>
<p><strong><strong>Version:</strong> </strong>1.2.1</p></td>
</tr>
<tr class="even">
<td><p>jta</p></td>
<td><a href="http://www.oracle.com/technetwork/java/javaee/jta/index.html" class="uri external-link">http://www.oracle.com/technetwork/java/javaee/jta/index.html</a>
<p>(Download 'Class Files 1.0.1B')</p>
<p><strong>Maven groupId and artifactId:</strong> jta:jta.</p>
<p><strong><strong>Version:</strong> </strong>1.0.1B</p></td>
</tr>
<tr class="odd">
<td>ojdbc7</td>
<td><p><a href="http://www.oracle.com/technetwork/database/features/jdbc/jdbc-drivers-12c-download-1958347.html" class="uri external-link">http://www.oracle.com/technetwork/database/features/jdbc/jdbc-drivers-12c-download-1958347.html</a> </p>
<p>(Download 'ojdbc7.jar' under 'Oracle Database 12g Release 1 (12.1.0.1.0) JDBC Drivers')</p>
<p><strong>Maven groupId and artifactId:</strong> com.oracle:ojdbc7.</p>
<p><strong><strong>Version:</strong> </strong>12.1.0.1</p></td>
</tr>
</tbody>
</table>


**To install the libraries**, download each one of them (using the links above) into a directory on your file system, for example `downloads `in your home directory area.

{{% warning %}}

The **jmxri**, **jmxtools**, **jndi**, and **jta** libraries are downloaded as `.zip `archives and before you can install them into your local Maven repository, you'll need to either extract the key `.jar` file, or change the libraries into the `.jar` form.

{{% /warning %}}

-   For **jmxri** and **jmxtools**:
    -   **On Windows:**  
        Open the `downloads` directory and extract the `jmxri.jar` and `jmxtools.jar` files from the `jmx-1_2_1-bin\lib` subdirectory of the downloaded `jmx-1_2_1-ri.zip` file.
    -   **On Linux:**

        ``` bash
        cd $HOME/Downloads
        unzip jmx-1_2_1-ri.zip jmx-1_2_1-bin/lib/jmxri.jar jmx-1_2_1-bin/lib/jmxtools.jar
        ```

-   For **jndi**:
    -   **On Windows:**  
        Open the `downloads` directory and extract the `jndi.jar` file from the `lib` subdirectory of the downloaded `jndi-1_2_1.zip` file.
    -   **On Mac/Linux:**

        ``` bash
        cd $HOME/Downloads
        unzip jndi-1_2_1.zip lib/jndi.jar
        ```

-   For **jta**:
    -   **On Windows:**  
        Open the `downloads` directory and rename the `jta-1_0_1B-classes.zip` file to `jta-1_0_1B-classes.jar`.
    -   **On Mac/Linux:**

        ``` bash
        cd $HOME/Downloads
        mv jta-1_0_1B-classes.zip jta-1_0_1B-classes.jar
        ```

1.  Download the Jira Core source archive from our <a href="https://my.atlassian.com/download/source/jira-core" class="external-link">Core source downloads</a> page. To download Jira Software, go to <a href="https://my.atlassian.com/download/source/jira" class="external-link">Software source downloads</a>.

{{% note %}}

To access this page, you'll need to log in as a user with a commercial license.

{{% /note %}}

2.  #### Extract the Jira source archive to a location of your choice

    This will create a Jira source directory with the name `atlassian-jira-X.Y.Z-source`, where X.Y.Z is your version of Jira. For example, `C:\atlassian-jira-7.4.0-source`.

3.  In the Jira source directory, create the `localrepo` directory. 

4.  Set appropriate permissions for installing the node modules. In your Jira source directory, enter the following commands:

    **Code block**

    ``` bash
    chmod a+x jira-project/conf/frontend/node
    chmod a+x ./jira-project/jira-components/jira-plugins/jira-post-upgrade-landing-page-plugin/src/main/frontend/bin/node
    chmod a+x ./jira-project/jira-components/jira-webapp/src/main/resources/johnson-page/bin/node
    ```

5.  Install libraries into your local Maven repository. In your Jira source directory, enter the following commands:  

    ``` bash
    mvn install:install-file -Dmaven.repo.local=localrepo -DgroupId=javax.jms -DartifactId=jms -Dversion=1.1 -Dpackaging=jar -Dfile=$HOME/Downloads/jms-1.1.jar
    mvn install:install-file -Dmaven.repo.local=localrepo -DgroupId=com.sun.jmx -DartifactId=jmxri -Dversion=1.2.1 -Dpackaging=jar -Dfile=$HOME/Downloads/jmx-1_2_1-bin/lib/jmxri.jar
    mvn install:install-file -Dmaven.repo.local=localrepo -DgroupId=com.sun.jdmk -DartifactId=jmxtools -Dversion=1.2.1 -Dpackaging=jar -Dfile=$HOME/Downloads/jmx-1_2_1-bin/lib/jmxtools.jar
    mvn install:install-file -Dmaven.repo.local=localrepo -DgroupId=jndi -DartifactId=jndi -Dversion=1.2.1 -Dpackaging=jar -Dfile=$HOME/Downloads/jndi.jar
    mvn install:install-file -Dmaven.repo.local=localrepo -DgroupId=jta -DartifactId=jta -Dversion=1.0.1 -Dpackaging=jar -Dfile=$HOME/Downloads/jta-1_0_1B-classes.jar
    mvn install:install-file -Dmaven.repo.local=localrepo -DgroupId=com.oracle -DartifactId=ojdbc7 -Dversion=12.1.0.1 -Dpackaging=jar -Dfile=$HOME/Downloads/ojdbc7.jar
    ```

6.  Build Jira by executing the following Maven 2 command:  
    **On Windows:**

    ``` bash
    C:\atlassian-jira-7.4.0-source\> build.bat
    ```

    **On Mac/Linux:**

    ``` bash
    > build.sh
    ```

{{% note %}}

The build script will download several dependencies from Atlassian's public Maven repository.  

On rare occasions, the build process may fail and you may receive an error similar to the one encountered when an Atlassian product is unable to install a plugin from Atlassian's public Maven repository. This problem is caused by the JVM being unable to access its default 'cacerts' file, which contains a certificate that trusts Atlassian's public Maven repository.  

To resolve this problem:

-   Try one of the solutions mentioned in the Confluence Knowledge Base article: <a href="https://confluence.atlassian.com/display/KB/Unable+to+Connect+to+SSL+Services+due+to+PKIX+Path+Building+Failed" class="external-link">Unable to Install Plugin Due to Maven SSL</a>.
-   If that does not resolve the problem, you may have a 'jssecacerts' file which does not contain a certificate that trusts Atlassian's public Maven repository. If a 'jssecacerts' file is present in your JVM, the 'cacerts' file may be ignored.

{{% /note %}}

7.  A WAR file called `jira-webapp-dist-X.Y.Z.war` (where X.Y.Z is your version of Jira), will be built in the `jira-project/jira-distribution/jira-webapp-dist/target` subdirectory of your extracted Jira source directory.   

    For example, if the subdirectory created above ([Extract the Jira source archive to a location of your choice](#extract-the-jira-source-archive-to-a-location-of-your-choice)) was `C:\atlassian-jira-7.4.0-source`, the WAR file will be found in:  
    `C:\atlassian-jira-7.4.0-source\jira-project\jira-distribution\jira-webapp-dist\target\jira-webapp-dist-7.4.0.war`      

{{% note %}}

You can also find an unpacked version of your Jira source build in the `jira-project/jira-distribution/jira-webapp-dist/target/jira-webapp-dist-X.Y.Z` subdirectory of your extracted Jira source directory.

{{% /note %}}

8.  The WAR file generated can now be installed into your application server to run the Jira application you just built.

## Developing using the IDE Connectors

Learn about the IDE Connectors from the IDE Connector source code and documentation:

-   <a href="https://bitbucket.org/atlassian/connector-eclipse" class="external-link">Eclipse IDE connector</a>
-   <a href="https://bitbucket.org/atlassian/connector-idea" class="external-link">IDEA IDE connector</a>
-   <a href="https://bitbucket.org/atlassian/connector-vs" class="external-link">VS connector</a>

## Obtaining the source of Jira's dependencies

When building Jira from source, Maven will automatically fetch the binary (compiled) dependencies that it requires during the build process, so you don't have to do it manually (with the exception of the third-party libraries mentioned above ([Third-party libraries](#third-party-libraries))).

It is worth noting that Jira's source distribution not only ships with Jira's source code, it also includes the source of the internal Atlassian projects that Jira depends on (e.g. atlassian-bonnie, atlassian-core, etc.). The internal Atlassian dependencies for Jira's source distribution are made available as Maven-based archives. There will be a Maven-based archive for each dependency, named **&lt;dependencyname-version&gt;-sources.jar**. The only buildable part of the source package is Jira itself, not its dependencies.

Other dependencies are available on Atlassian's <a href="http://maven.atlassian.com/repository/public" class="external-link">public repository</a>. The source of these dependencies is usually available on the library's website (try <a href="http://www.google.com/" class="external-link">googling</a> for the library name), or can be identified in the <a href="http://maven.apache.org/pom.html#SCM" class="external-link">SCM</a> information of the relevant library.

If you have any questions regarding the build process, try searching/posting to the <a href="https://community.atlassian.com/t5/JIRA/ct-p/jira" class="external-link">Atlassian Community</a>, which is monitored by the development community, and by Atlassian as often as possible.
