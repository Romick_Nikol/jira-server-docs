---
aliases:
- /server/jira/platform/jira-7.0-platform-changes-34669641.html
- /server/jira/platform/jira-7.0-platform-changes-34669641.md
category: devguide
confluence_id: 34669641
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=34669641
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=34669641
date: '2017-12-08'
legacy_title: JIRA 7.0 - Platform changes
platform: server
product: jira
subcategory: updates
title: JIRA 7.0 - Platform changes
---
# JIRA 7.0 - Platform changes

This page covers the **platform changes** for **JIRA 7.0**. These include upgrades to bundled components, like the plugin framework, SAL, etc.

The changes are part of a larger set of developer changes to JIRA for 7.0. You should also read [Preparing for JIRA 7.0](/server/jira/platform/preparing-for-jira-7-0) for an overview of the release, as well as [JIRA 7.0 - Platform changes](/server/jira/platform/jira-7-0-api-changes) and [JIRA 7.0 - General changes](/server/jira/platform/jira-7-0-general-changes).

## Summary

The risk level indicates the level of certainty we have that things will break if you are in the "Affected" column and you don't make the necessary changes.

<table>
<colgroup>
<col style="width: 30%" />
<col style="width: 70%" />
</colgroup>
<thead>
<tr class="header">
<th>Change</th>
<th>Affected</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><a href="#spring-upgrade">Spring upgrade</a></td>
<td><p>Add-ons that use Spring classes where a version range is not explicitly set</p>
<p><strong>When:</strong> 7.0</p>
<p><strong>Risk level:</strong> high</p></td>
</tr>
<tr class="even">
<td><p><a href="#org-osgi-core-library-incompatibility">org.osgi.core library incompatibility</a></p></td>
<td><p>Add-ons that bundle the org.osgi.core library</p>
<p><strong>When:</strong> 7.0</p>
<p><strong>Risk level:</strong> high</p></td>
</tr>
<tr class="odd">
<td><a href="#servlet-api-upgrade">Servlet API upgrade</a></td>
<td><p>Add-ons that import servlet api classes where a version range is not explicitly set</p>
<p><strong>When:</strong> 7.0</p>
<p><strong>Risk level:</strong> high</p></td>
</tr>
<tr class="even">
<td><p><a href="#plugin-license-storage-lib-library-no-longer-supported">plugin-license-storage-lib library no longer supported</a></p></td>
<td><p>Add-ons that bundle the plugin-license-storage-lib library</p>
<p><strong>When:</strong> 7.0</p>
<p><strong>Risk level:</strong> high</p></td>
</tr>
<tr class="odd">
<td><a href="#sal-upgrade">SAL upgrade</a></td>
<td><p>Add-ons that look up a login URL</p>
<p><strong>When:</strong> 7.0</p>
<p><strong>Risk level:</strong> medium</p></td>
</tr>
<tr class="even">
<td><p><a href="#guava-library-upgrade">Guava library upgrade</a></p></td>
<td><p>Any add-on that uses guava functionality</p>
<p><strong>When:</strong> 7.0</p>
<p><strong>Risk level:</strong> medium</p></td>
</tr>
<tr class="odd">
<td><p><a href="#soy-renderer-upgrade-from-2-x-to-4-1-2">Soy Renderer upgrade from 2.x to 4.1.2</a></p></td>
<td><p>Add-ons using soy as template renderer</p>
<p><strong>When:</strong> 7.0</p>
<p><strong>Risk level:</strong> medium</p></td>
</tr>
<tr class="even">
<td><a href="#springdm-replaced-by-eclipse-gemini">SpringDM replaced by Eclipse Gemini</a></td>
<td><p>Add-ons that use <code>BundleContextAware</code></p>
<p><strong>When:</strong> 7.0</p>
<p><strong>Risk level:</strong> low</p></td>
</tr>
<tr class="odd">
<td><a href="#bndlib-changes">bndlib changes</a></td>
<td><p>See details below</p>
<p><strong>When:</strong> 7.0</p>
<p><strong>Risk level:</strong> low</p></td>
</tr>
</tbody>
</table>

## Details

### Spring upgrade

JIRA 7.0 will include a new version of the Atlassian Plugins Framework, which includes an upgrade of Spring from version 2 to 3. In this upgrade, if your add-on uses Spring classes and you have not explicitly set a version range, your add-on will not load. This is because BND, a tool that generates manifests for OSGi bundles at compile time, will generate an import statement for Spring with the version range "\[2.5,3)" (this reads as "greater than or equal to 2.5, but less than 3"). Your add-on will try to find the relevant Spring packages, but will limit its search to that range (i.e. versions less than 3). The package that is supplied is now version 3, so the search will fail.  
  
To fix this, you must explicitly add a version range. We suggest "2.5," which reads as "greater than or equal to 2.5," removing the upper bound of 3. The complete import statement will look something like this:

``` java
...
    org.springframework*;version=2.5
...
```

If you know the exact packages that are required by your add-on, you should list them out explicitly rather than relying on a wildcard expression.

### `org.osgi.core` library incompatibility

JIRA 7.0 will include a new version of the Atlassian Plugins Framework, which will not work when classes from `org.osgi.core` are redefined. Classes in `org.osgi.core` are redefined when an add-on bundles the `org.osgi.core` library. This will cause a version mismatch and class-loading issues. To fix this, your add-on will need to import the `org.osgi.core` library from JIRA instead of bundling it (see [LinkageError](https://developer.atlassian.com/display/DOCS/LinkageError)).

### Servlet API upgrade

JIRA 7.0 will expose a newer version of the Servlet API, version 3.0. As with the [Spring upgrade](#spring-upgrade) described above, the add-on manifest may prevent the loading of the servlet API with version 3.0.

To fix this, you must explicitly add a version range. We suggest "2.5," which reads as "greater than or equal to 2.5," removing the upper bound of 3.0. The complete import statement will look something like this:

``` java
javax.servlet*;version=2.5 
```

### `plugin-license-storage-lib` library no longer supported

If your add-on bundles the `plugin-license-storage-lib` library from the Universal Plugin Manager (UPM), your add-on will not work with JIRA 7.0. This library has been deprecated for a while, and won't work with JIRA 7.0. 

To fix this, you will need to use UPM's `licensing-api` module instead. Follow the instructions in this tutorial: [Tutorial: Adding licensing support to your add-on](https://developer.atlassian.com/display/MARKET/Tutorial%3A+Adding+licensing+support+to+your+add-on) to do this. Note, use the versions provided by the application for the import packages; don't specify a version range for the import packages.

You will also need to remove references to the old `plugin-license-storage-lib` library.

### SAL upgrade

Atlassian Shared Access Layer (SAL) will be upgraded in JIRA 7.0. The API changes are:

-   `com.atlassian.sal.api.auth.LoginUriProvider` has had methods removed and a replacement added.
-   `com.atlassian.sal.api.page.PageCapabilities` has been removed.
-   `com.atlassian.sal.api.page.PageCapability` has been removed.

The OSGi version will also be increased to 3.0. If your add-on has declared a maximum version of less than 3.0 in its manifest file, it will not load in JIRA 7.0. To fix this, we recommend that you remove the upper limit for SAL (this will also help your add-on to avoid this type of problem in future).

### Guava library upgrade

Guava will be upgraded 18.0 in JIRA 7.0. Guava is a utility library, provided by Google, that allows for operations on collections, etc. If your add-on depends on functionality that is provided by Guava, then you may need to make changes to your add-on.

### Soy Renderer upgrade from 2.x to 4.1.2

The Atlassian Soy Template Renderer has been upgraded to version 4.1.2. This is a major upgrade of the soy compiler with many useful features including:

-   **Strict autoescaping mode** -- This security feature helps avoid accidental double-escaping. It does this by identifying fragments which are already safe, so they are not escaped again. This means that you should be able to remove most `noAutoescape` declarations in your templates.
-   **Strongly-typed template parameters** -- Writing types for your soy template parameters is not mandatory, but you can now do so, if you want to.
-   **Exception backtraces now include template calls** -- Enough said, better exceptions are always a win!
-   **Support for custom data converters in Tofu** -- You won't be using this feature yet, although Atlassian Soy currently uses it internally. 

Other changes in this version of the Soy Template Renderer:

-   Soy template compilation is now stricter, with regards to templates passing in required parameters to sub-templates. If you don't need a required parameter, just mark it as optional.
-   The internal Soy compiler is no longer exposed through OSGi. Atlassian Soy has its own APIs, which you should be using and is there to shield consumers from changes to the underlying compiler. From now on, you'll need to use these public APIs instead.
-   New `toString` function, which helps to bridge the gap in breaking changes to 3.x. This function calls `toString` on the object passed in as a parameter. In 3.x, POJOs no longer call `toString` when printing the object, and show a JSON string representation instead.
-   New `helpUrl` function, which lets you to retrieve the helpUrl for a given key. This works on both client and server and is CDN friendly. It is currently backed by the SAL API.
-   Soy caches no longer expire and the Closure Templates compiler now uses stackless exceptions for flow control. Both of these changes should improve performance.
-   The API has been split in this release. Previously, the `soy-template-renderer-api` artifact depended on the plugin framework and web resources, even though it could be consumed from a non-plugin framework environment. All parts of the API which are only related to the Atlassian plugin deployment have now been moved to the `soy-template-renderer-plugin-api` artifact. No classes have been deleted in the process so the API remains binary compatible. You may run into a problem when the product upgrades to 3.3.0 -- some consumers may need to add the following additional dependency to function correctly:

    ``` xml
    <dependency>
        <groupId>com.atlassian.soy</groupId>
        <artifactId>soy-template-renderer-plugin-api</artifactId>
        <version>${soy.version}</version>
        <scope><!-- Set this so whatever scope the soy-template-renderer-api dependency is --></scope>
    </dependency>
    ```

Troubleshooting: 

<table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<thead>
<tr class="header">
<th>Error / Error message</th>
<th>Action / Explanation</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>&quot;template aui.toolbar.splitButton: Call to 'aui.toolbar.dropdownInternal' is missing required param 'text'.&quot;</td>
<td>Upgrade to AUI 5.6.11.</td>
</tr>
<tr class="even">
<td>&quot;template jiraIntegration.templates.fields.stringField: Call to 'aui.form.textField' is missing required param 'id'&quot;</td>
<td>Upgrade to JIRA Integration Plugin 1.5.9.</td>
</tr>
<tr class="odd">
<td>&quot;&lt;SOME TEMPLATE&gt;: Call to '&lt;SOME SUB TEMPLATE&gt;' is missing required param '&lt;SOME PARAM&gt;'.&quot;</td>
<td>You are hitting the strictness changes in the soy compiler.<br />
Check your template for the problem.</td>
</tr>
<tr class="even">
<td>&quot;java.lang.ClassCastException: java.lang.Long cannot be cast to java.lang.Integer from a custom soy function&quot;</td>
<td>Your custom function shouldn't be assuming a specific type of number.<br />
Cast to Number and call the appropriate method to get an int or long.</td>
</tr>
<tr class="odd">
<td>Enums are no longer comparing with strings</td>
<td><p>Bug in 3.0.0 release, fixed in <a href="https://extranet.atlassian.com/display/DEV/2014/07/18/Atlassian+Soy+Templates+3.0.3+released" class="external-link">3.0.3</a>.</p></td>
</tr>
<tr class="even">
<td>&quot;java.lang.IllegalAccessException: Class com.atlassian.soy.impl.data.AccessorSoyValueProvider can not access a member&quot;</td>
<td>Bug in 3.0.0 release, fixed in <a href="https://extranet.atlassian.com/display/DEV/2014/07/18/Atlassian+Soy+Templates+3.0.3+released" class="external-link">3.0.3</a>.</td>
</tr>
<tr class="odd">
<td><p>Printing a Java object in a template no longer calls the <code>toString</code> method</p></td>
<td>The JSON representation of the object will be output.</td>
</tr>
</tbody>
</table>

### SpringDM replaced by Eclipse Gemini

JIRA 7.0 will include a new version of the Atlassian Plugins Framework, which uses Eclipse Gemini rather than SpringDM to perform dependency injection over OSGi. This should be mostly transparent to add-ons, however plugins which use `org.springframework.osgi.context.BundleContextAware` to inject the `org.osgi.framework.BundleContext` should instead use `org.eclipse.gemini.blueprint.context.BundleContextAware`. The two classes are API-identical, so the only required changes should be updating the import in the Java source and your OSGi `instructions` `Import-Package` (if manually managed).

### `bndlib `changes

JIRA 7.0 will include a new version of the Atlassian Plugins Framework, which uses a newer version of `bndlib` for automatic manifest generation (primarily to support JDK 8).  This new version of bndlib will now add tildes to the end of any duplicate packages it finds -- you will notice messages like this at startup: `"missing requirement [112.0] osgi.wiring.package; (&(osgi.wiring.package=org.codehaus.jackson.annotate~)"`. The safest way to resolve this is to make your plugin a transformerless plugin, however you can also carefully audit your imports to fix it. See this issue for details: <a href="https://ecosystem.atlassian.net/browse/PLUG-1131" class="external-link">PLUG-1131</a>.
