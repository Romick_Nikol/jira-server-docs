---
aliases:
- /server/jira/platform/sitemesh-decoration-plugin-module-30621774.html
- /server/jira/platform/sitemesh-decoration-plugin-module-30621774.md
category: reference
confluence_id: 30621774
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=30621774
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=30621774
date: '2017-12-08'
legacy_title: SiteMesh Decoration Plugin Module
platform: server
product: jira
subcategory: modules
title: SiteMesh decoration
---
# SiteMesh decoration

<table>
<colgroup>
<col style="width: 40%" />
<col style="width: 60%" />
</colgroup>
<tbody>
<tr class="odd">
<td><p>Available:</p></td>
<td><p>The SiteMesh decoration module is available in JIRA 6.0 and later.</p></td>
</tr>
</tbody>
</table>

## Purpose of this Module Type

The SiteMesh decoration module allows add-ons to decorate URLs that by default will not be decorated by JIRA.  This is typically the case when you have your own pretty URLs in place.   For more information, please see [Guide - Using pretty URLs in a JIRA plugin](/server/jira/platform/using-pretty-urls-in-a-jira-plugin).

## Configuration

The root element for the URL routing  plugin module is `sitemesh`. It allows the following attributes and child elements for configuration:

**Attributes**

<table>
<colgroup>
<col style="width: 20%" />
<col style="width: 80%" />
</colgroup>
<thead>
<tr class="header">
<th><p>Name</p></th>
<th><p>Description</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>path</p></td>
<td><p>The top level URL path that will be matched for decoration. This will cause all <code>text/html</code> responses on that path to be SiteMesh decorated. </p>
<p><strong>Required: yes</strong></p>
<p><strong>Default: N/A</strong></p></td>
</tr>
<tr class="even">
<td><p>key</p></td>
<td><p>The unique identifier of the routing module.</p>
<p><strong>Required: yes</strong></p>
<p><strong>Default: N/A</strong></p></td>
</tr>
</tbody>
</table>

## SiteMesh decoration example

This is a `sitemesh` entry that will decorate every text/html response within the top level path `/pluginpath`

``` xml
<sitemesh key="unique-key-for-module-type" path="/pluginpath"/>
```

Some paths in JIRA will automatically have SiteMesh decoration in place.  For the full list and more examples see [Guide - Using pretty URLs in a JIRA plugin](/server/jira/platform/using-pretty-urls-in-a-jira-plugin)
