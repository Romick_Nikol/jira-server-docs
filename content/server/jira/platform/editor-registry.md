---
aliases:
- /server/jira/platform/editor-registry-documentation-44054184.html
- /server/jira/platform/editor-registry-documentation-44054184.md
category: reference
confluence_id: 44054184
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=44054184
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=44054184
date: '2017-12-08'
legacy_title: Editor Registry Documentation
platform: server
product: jira
subcategory: modules
title: Editor registry
---
# Editor registry

## Editor Registry module

The Registry keeps track of all editor instances on a page, and acts as an event bus for registration/unregistration/mode switch events. 

It also utilizes some <a href="http://backbonejs.org/#Events" class="external-link">Backbone.Events</a> so you could also reference their documentation for the details.

Available events you can bind to:

-   register
-   switch
-   unregister

All callbacks receive a corresponding editor's \`entry\` as an argument. 

Every time an "event" happens it is namespaced by firing 4 different events, so you can bind to the particular editor's events:

-   eventName
-   eventName:mode
-   eventName:mode:id
-   eventName::id

E.g. when showing up "Comment" editor form with Text mode being default the following events will be fired:

-   register
-   register:source
-   register:source:comment
-   register::comment

then, when switching to Visual mode:

-   switch
-   switch:wysiwyg
-   switch:wysiwyg:comment
-   switch::comment

## Entry

Entry class that keeps the current mode and references to editor's main components.

-   id
    -   Editor's id. `id` is assigned to the editor upon registration and you can get it from the entry in a `register` callback. **Type:** String.  
-   currentMode
    -   Editor's current mode value. **Type: **String.  

        {{% note %}}

`mode` part of the event name and `entry.currentMode` property use `'source'` and `'wysiwyg'` values for Text and Visual mode correspondingly.

But we strongly recommend using `require('jira/editor/constants').Modes` object instead of raw values.

        {{% /note %}}

-   textArea
    -   Editor's textArea element used in Text mode. **Type:** Element.  
-   toolbar
    -   Editor's toolbar element. **Type: **Element.  
-   rteInstance
    -   jQuery Promise backed by rich text editor instance. **Type:** Promise.

<table>
<colgroup>
<col style="width: 30%" />
<col style="width: 70%" />
</colgroup>
<thead>
<tr class="header">
<th>Method</th>
<th>Description</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><pre><code>isTextMode</code></pre></td>
<td><p>Checks if editor described by the current entry is in the Text mode</p>
<p><strong>Return type:</strong></p>
<p><code>Boolean</code></p>
<p><strong>Arguments:</strong></p>
<p>-</p></td>
</tr>
<tr class="even">
<td><pre><code>isVisualMode</code></pre></td>
<td><p>Checks if editor described by the current entry is in the Visual mode</p>
<p><strong>Return type:</strong></p>
<p><code>Boolean</code></p>
<p><strong>Arguments:</strong></p>
<p>-</p></td>
</tr>
<tr class="odd">
<td><pre><code>onSwitch</code></pre></td>
<td><p>Binds callback to the <code>switch</code> event related to the editor described by this entry (i.e. to the <code>`switch::$id`</code> event)</p>
<strong>Return type:</strong>
<p>Entry</p>
<p><strong>Arguments:</strong></p>
<p>callback: Function</p></td>
</tr>
<tr class="even">
<td><pre><code>onSwitchToText</code></pre></td>
<td><p>Binds callback to the <code>switch</code> (to Text mode) event related only to the editor described by this entry (i.e. to the `<code>switch:source:$id`</code> event) </p>
<p><strong>Return type:</strong></p>
<p>Entry</p>
<p><strong>Arguments:</strong></p>
<p>callback: Function</p></td>
</tr>
<tr class="odd">
<td><pre><code>onSwitchToVisual</code></pre></td>
<td><p>Binds callback to the <code>switch</code> (to Visual mode) event related only to the editor described by this entry (i.e. to the `<code>switch:wysiwyg:$id`</code> event)</p>
<p><strong>Return type:</strong></p>
<p>Entry</p>
<p><strong>Arguments:</strong></p>
<p>callback: Function</p></td>
</tr>
<tr class="even">
<td><pre><code>onUnregister</code></pre></td>
<td><p>Binds callback to the <code>unregister</code> event related only to the editor described by this entry (i.e. to the `<code>unregister::$id`</code> event)</p>
<p><strong>Return type:</strong></p>
<p>Entry</p>
<p><strong>Arguments:</strong></p>
<p>callback: Function</p></td>
</tr>
<tr class="odd">
<td><pre><code>applyIfTextMode</code></pre></td>
<td><p>Executes callback on the current entry only if the editor is in the Text mode</p>
<p><strong>Return type:</strong></p>
<p>Entry</p>
<p><strong>Arguments:</strong></p>
<p>callback: Function</p></td>
</tr>
<tr class="even">
<td><pre><code>applyIfVisualMode</code></pre></td>
<td><p>Executes callback on the current entry only if the editor is in the Visual mode</p>
<p><strong>Return type:</strong></p>
<p>Entry</p>
<p><strong>Arguments:</strong></p>
<p>callback: Function</p></td>
</tr>
</tbody>
</table>
