---
aliases:
- /server/jira/platform/activity-streams-demos-6848633.html
- /server/jira/platform/activity-streams-demos-6848633.md
category: devguide
confluence_id: 6848633
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=6848633
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=6848633
date: '2017-12-08'
legacy_title: Activity Streams Demos
platform: server
product: jira
subcategory: updates
title: Activity Streams Demos
---
# Activity Streams Demos

Below are three demos that give you an insight into the new Activity Streams API in JIRA 5.0.  
For details of the Activity Streams API, see the [Activity Streams documentation](https://developer.atlassian.com/display/STREAMS).

## Demo 1

[3rd Party Demo 1.mov](https://dac-lf.prod.atl-paas.net/server/jira/platform/attachments/6848633/7077895.mov)

## Demo 2

[3rd Party Demo 2.mov](https://dac-lf.prod.atl-paas.net/server/jira/platform/attachments/6848633/7077894.mov)

## Demo 3

[3rd Party Demo 3.mov](https://dac-lf.prod.atl-paas.net/server/jira/platform/attachments/6848633/7077893.mov)
