---
aliases:
- /server/jira/platform/adding-custom-fields-to-email-4227085.html
- /server/jira/platform/adding-custom-fields-to-email-4227085.md
category: devguide
confluence_id: 4227085
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=4227085
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=4227085
date: '2018-06-01'
guides: tutorials
legacy_title: Adding Custom Fields to Email
platform: server
product: jira
subcategory: learning
title: "Adding custom fields to email"
---
# Adding custom fields to email

This tutorial shows you how to print a custom field in an email. Note that there is an open feature request to integrate
this into Jira, which is tracked here: [JRA-4619](http://jira.atlassian.com/browse/JRA-4619).

## Step 1. Find the Custom Field ID

You can find the Custom Field ID by examining the URLs of custom field pages or by querying the database.

#### Examining the URL of a custom field page

1.  Go to the Custom Field configuration in the Jira administration console, as described on
[Configuring a Custom Field](https://confluence.atlassian.com/display/ADMINJIRASERVER/Configuring+a+custom+field/#Configuringacustomfield-configureConfiguringacustomfield)
page.
1.  To open the menu for the Custom Field, click ![cog icon](/server/jira/platform/images/cog.png "cog icon").
1.  Hover your mouse over the Configure item in the menu. The URL will display in the footer of your browser.
The URL will show the Custom Field ID.

For example, in this screenshot, the ID of the custom field is 10000.

<img src="/server/jira/platform/images/customfieldid.png" height="250" />

#### Querying the database

Run the following query on your database:

``` bash
SELECT \* FROM customfield WHERE cfname LIKE '%mycustomfield%';
```

Where `mycustomfield` is your custom field name.

## Step 2. Edit the Velocity template

Jira supports `html` and `text` email formats.
You should choose instructions according to format set in your Jira.

{{% tip %}}

If using the EAR/WAR version, make changes in the edit-webapp directory, then rebuild and re-deploy the WAR file.

{{% /tip %}}

#### Text format

1.  Find the Velocity template of the email type you wish to modify. For instance, you may want to modify the
`issue created` template, as well as the template reused in `issue commented`. They are located here:

    ``` bash
    atlassian-jira/WEB-INF/classes/templates/email/text/issuecreated.vm  
    atlassian-jira/WEB-INF/classes/templates/email/text/includes/issuesummary.vm
    ```

1.  Add the following snippet where you want it to appear in the file:

    ``` java
    #set ($customfield = $customFieldManager.getCustomFieldObject("customfield_10000"))
    #if ($issue.getCustomFieldValue($customfield))
    $stringUtils.leftPad($customfield.name, $padSize): $issue.getCustomFieldValue($customfield)
    #end
    ```

    {{% note %}}

You can find the javadoc for `$stringUtils.leftPad` here: [StringUtils](https://commons.apache.org/proper/commons-lang/apidocs/org/apache/commons/lang3/StringUtils.html).

    {{% /note %}}

1.  Navigate to `atlassian-jira/WEB-INF/classes/templates/email/text/includes/`, add the following to the
`issuesummary.vm` file:

    ``` java
    #set ($customfield = $customFieldManager.getCustomFieldObject("customfield_10000"))
    #if ($issue.getCustomFieldValue($customfield))
    >$stringUtils.leftPad($customfield.name, $padSize): $issue.getCustomFieldValue($customfield)
    #end
    ```

    Note that *you need to change the custom field ID* to the ID you found in step 1.
    In the example, the ID is 10000, yours will probably be different.

1.  If you wish to iterate over all related custom fields, you can use the following example:

    ``` java
    #foreach ($value in $customFieldManager.getCustomFieldObjects($issue))
    >$stringUtils.leftPad($value.getName(), $padSize): $!value.getValueFromIssue($issue)
    #end
    ```

#### Html format

1.  Find the Velocity template of the email type you wish to modify.
For instance, you may want to modify the `issue created` template that is located here:

    ``` bash
    atlassian-jira/WEB-INF/classes/templates/email/html/issuecreated.vm  
    ```

1.  Add the following snippet where you want it to appear in the file:

    ``` java
    #parse("templates/email/html/includes/fields/customfield.vm")
    ```

1.  Navigate to `atlassian-jira/WEB-INF/classes/templates/email/html/includes/fields/` and create a new Velocity
file `customfield.vm`.
1.  Add the following code to the file:

    ``` html
    #disable_html_escaping()
    #set ($customfield = $customFieldManager.getCustomFieldObject("customfield_10000"))
    #if($issue.getCustomFieldValue($customfield))
    <tr>
        <th>#text($customfield.name):</th>
        <td class="has-icon">      
            $textutils.htmlEncode($issue.getCustomFieldValue($customfield), false)
        </td>
    </tr>
    #end
    ```

Note that *you need to change the custom field ID* to the ID you found in step 1.
In the example, the ID is 10000, yours will probably be different.

## Step 3. Restart Jira

1.  To make the changes take effect, restart Jira.

    If you wish to avoid the continual restarts during testing, navigate to `atlassian-jira/WEB-INF/classes/`, open
    the `velocity.properties` file and edit the following section as the comment says:

``` java
# To enable autoreloading, set cache to false and uncomment the autoreload line
class.resource.loader.cache=true
#velocimacro.library.autoreload=true
```
