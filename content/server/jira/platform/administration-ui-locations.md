---
aliases:
- /server/jira/platform/administration-area-locations-4227108.html
- /server/jira/platform/administration-area-locations-4227108.md
category: reference
confluence_id: 4227108
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=4227108
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=4227108
date: '2018-06-29'
legacy_title: Administration Area Locations
platform: server
product: jira
subcategory: modules
title: "Administration UI locations"
---
# Administration UI locations

## Top navigation bar

The `system.admin.top.navigation.bar` location defines web sections that are shown
in the navigation bar on Administration page and in ![cog wheel](/server/jira/platform/images/cog.png) drop-down.

<img src="/server/jira/platform/images/web-fragments-admin-top-navigation-bar.png" />

<img src="/server/jira/platform/images/web-fragments-admin-top-navigation-bar-with-sections.png"/>

There are few predefined sections.

<table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<thead>
<tr class="header">
<th><p>Section Name</p></th>
<th><p><code>key</code> attribute</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>Applications</p></td>
<td><p>admin_applications_section</p></td>
</tr>
<tr class="even">
<td><p>Projects</p></td>
<td><p>admin_project_menu</p></td>
</tr>
<tr class="odd">
<td><p>Issues</p></td>
<td><p>admin_issues_menu</p></td>
</tr>
<tr class="even">
<td><p>Add-ons</p></td>
<td><p>admin_plugins_menu</p></td>
</tr>
<tr class="odd">
<td><p>User Management</p></td>
<td><p>admin_users_menu</p></td>
</tr>
<tr class="even">
<td><p>System</p></td>
<td><p>admin_system_menu</p></td>
</tr>
</tbody>
</table>

## Creating a new section in predefined tab

The `admin_plugins_menu` location defines web sections and items in the **Add-ons** tab of Jira administration area.
We will use this location for example.

<img src="/server/jira/platform/images/web-fragments-admin-plugins-menu.png" width="454" height="250" />

The following module definition steps show how to add one or more items in the Add-ons tab sidebar,
each of which leads to a single page.

{{% note %}}
Web section won't appear on UI until you define at least one web-item in the section.
{{% /note %}}

1.  Define one or more `web-section` modules for the **Add-ons** sidebar.

1.  Each of these `web-section` modules must include:
    *   A `key` attribute with a unique value.
    *   A `location` attribute with the value `"admin_plugins_menu"`.
    *   *Optional*. If you wish to create a "section label" that appears in bold text at the top of the section,
    define a `label` element with a `key` attribute whose value defines a unique property. The string value of
    this unique property (defined in your app's `i18n.properties` file) will be shown in bold text at the top
    of your section.

    For example:

    ``` xml
    <web-section key="my_new_section" ... location="admin_plugins_menu" ...>
        ...
        <label key="my_new_section_name"/> <!-- This element is optional -->
        ...
    </web-section>
    ```

1.  Define a `web-item` module for each item that appears in one of the web sections in the **Plugins** drop-down menu.
    *   Each of these `web-item` modules must include a `section` attribute whose value is `admin_plugins_menu`,
    followed immediately by a slash symbol `/`, and followed immediately by the `key` attribute's value specified in
    the relevant web section module.

    For example:

    ``` xml
    <web-item key="my_new_tabbed_item" ... section="admin_plugins_menu/my_new_section" ...>
    ...
    </web-item>
    ```

1.  Define a `meta` element in your HTML template file (such as a Velocity template file) with the attribute
`name="admin.active.section"`.
    *   This `meta` tag should contain a `content` attribute whose value is the `section` attribute's value
    specified in the `web-item` module you defined.

    For example:

    ``` xml
    <meta name="admin.active.section" content="admin_plugins_menu/my_new_section"/>
    ```

1.  Define another `meta` tag in the HTML template file with the attribute `name="admin.active.tab"`.
    *   This `meta` tag should contain a `content` attribute whose value is the `key` attribute's value
    specified in the `web-item` that links to that page.

    For example:

    ``` xml
    <meta name="admin.active.tab" content="my_new_tabbed_item"/>
    ```

1.  To render your page with Administration layout, define a `meta` with `content="decorator"`.

    ``` xml
    <meta name="decorator" content="atl.admin"/>
    ```

## Creating a new section in custom tab

If you want to create a new custom tab, you should create a `web-section` with the
`system.admin.top.navigation.bar` location. Then follow steps in previous section
and use your custom tab key instead of `admin_plugins_menu`.

### Customizing the positions of your own items and sections

To customize the position of your own web items or sections in this location, add a `weight` attribute to
your `web-item` or `web-section` and adjust its value with respect to Jira's existing web items or sections.
Lower weight values result in these items/sections appearing higher.

### Source file

To find the values of Jira's existing web items and sections for the `system.admin` location, view the following
file in Jira's source archive:  
`<source-installation-directory>/jira-project/jira-components/jira-core/src/main/resources/webfragment/system-admin-sections.xml`

##  Existing web sections of the administration location

**Projects**

<table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<thead>
<tr class="header">
<th><p>Section Name</p></th>
<th><p><code>location</code></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>none</p></td>
<td><p>admin_project_menu/project_section</p></td>
</tr>
</tbody>
</table>

**Issues**

<table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<thead>
<tr class="header">
<th><p>Section Name</p></th>
<th><p><code>location</code></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>ISSUE TYPES</p></td>
<td><p>admin_issues_menu/element_options_section/issue_types_section</p></td>
</tr>
<tr class="even">
<td><p>WORKFLOWS</p></td>
<td><p>admin_issues_menu/element_options_section/workflows_section</p></td>
</tr>
<tr class="odd">
<td><p>SCREENS</p></td>
<td><p>admin_issues_menu/element_options_section/screens_section</p></td>
</tr>
<tr class="even">
<td><p>FIELDS</p></td>
<td><p>admin_issues_menu/element_options_section/fields_section</p></td>
</tr>
<tr class="odd">
<td><p>PRIORITIES</p></td>
<td><p>admin_issues_menu/priorities_section</p></td>
</tr>
<tr class="even">
<td><p>ISSUE FEATURES</p></td>
<td><p>admin_system_menu/top_system_section/issue_features</p></td>
</tr>
<tr class="odd">
<td><p>ISSUE ATTRIBUTES</p></td>
<td><p>admin_system_menu/top_system_section/issue_features</p></td>
</tr>
<tr class="even">
<td><p>none</p></td>
<td><p>admin_issues_menu/misc_schemes_section</p></td>
</tr>
</tbody>
</table>

**Add-ons**

<table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<thead>
<tr class="header">
<th><p>Section Name</p></th>
<th><p><code>location</code></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>Atlassian Marketplace</p></td>
<td><p>admin_plugins_menu/upm_section</p></td>
</tr>
</tbody>
</table>

**User Management**

<table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<thead>
<tr class="header">
<th><p>Section Name</p></th>
<th><p><code>location</code></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>USER MANAGEMENT</p></td>
<td><p>admin_users_menu/users_groups_section</p></td>
</tr>
<tr class="even">
<td><p>none</p></td>
<td><p>admin_users_menu/users_groups_configuration</p></td>
</tr>
<tr class="odd">
<td><p>USER DIRECTORIES</p></td>
<td><p>admin_users_menu/users_groups_configuration/embedded_crowd_section</p></td>
</tr>
</tbody>
</table>

**System**

<table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<thead>
<tr class="header">
<th><p>Section Name</p></th>
<th><p><code>location</code></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>none</p></td>
<td><p>admin_system_menu/top_system_section</p></td>
</tr>
<tr class="even">
<td><p>SYSTEM SUPPORT</p></td>
<td><p>admin_system_menu/top_system_section/troubleshooting_and_support</p></td>
</tr>
<tr class="odd">
<td><p>SECURITY</p></td>
<td><p>admin_system_menu/top_system_section/security_section</p></td>
</tr>
<tr class="even">
<td><p>USER INTERFACE</p></td>
<td><p>admin_system_menu/top_system_section/user_interface</p></td>
</tr>
<tr class="odd">
<td><p>IMPORT AND EXPORT</p></td>
<td><p>admin_system_menu/top_system_section/import_export_section</p></td>
</tr>
<tr class="even">
<td><p>MAIL</p></td>
<td><p>admin_system_menu/top_system_section/mail_section</p></td>
</tr>
<tr class="odd">
<td><p>ADMIN HELPER</p></td>
<td><p>admin_plugins_menu/admin-helper-admin-section</p></td>
</tr>
<tr class="even">
<td><p>SHARED ITEMS</p></td>
<td><p>admin_users_menu/shared_section</p></td>
</tr>
<tr class="odd">
<td><p>ADVANCED</p></td>
<td><p>admin_system_menu/advanced_menu_section/advanced_section</p></td>
</tr>
</tbody>
</table>

## Maintaining administration area location compatibility across Jira versions

To ensure that `web-items` defined for Jira's administration area will remain compatible for different Jira
versions, specify the following in your app:

1.  Add a new `IsPriorToJiraVersion` `Condition` class to your app to identify if your Jira version is earlier than,
for example, Jira 4.4. This class:

    *   Implements `com.atlassian.plugin.web.Condition`
    *   Returns a `true` value if your Jira version is found to be earlier than Jira 4.4.

1.  Add a `<condition/>` element that calls your app's `IsPriorToJiraVersion` class to the `web-item` module
defined for versions of Jira prior to 4.4. This condition:

    *   Passes the version number of 4.4 to your app's `IsPriorToJiraVersion` class via child `<parameter/>`
    elements.
    *   If a `false` value is returned by this class (because your version of Jira is later than 4.4), the `web-item`
    will be disabled and hence will not be visible in any drop-down menu of the Jira Administration area.

1.  Add the `IsPriorToJiraVersion` `Condition` class to your app:

    ``` java
    @Scanned
    public class IsPriorToJiraVersion implements Condition {

        private int maxMajorVersion;
        private int maxMinorVersion;
        private int majorVersion;
        private int minorVersion;

        public IsPriorToJiraVersion(@ComponentImport ApplicationProperties applicationProperties) {
            String versionString = applicationProperties.getVersion();
            String versionRegex = "^(\\d+)\\.(\\d+)";
            Pattern versionPattern = Pattern.compile(versionRegex);
            Matcher versionMatcher = versionPattern.matcher(versionString);
            versionMatcher.find();
            majorVersion = Integer.decode(versionMatcher.group(1));
            minorVersion = Integer.decode(versionMatcher.group(2));
        }

        public void init(final Map<String, String> paramMap) throws PluginParseException {
            maxMajorVersion = Integer.decode(paramMap.get("majorVersion"));
            maxMinorVersion = Integer.decode(paramMap.get("minorVersion"));
        }

        public boolean shouldDisplay(final Map<String, Object> context) {
            return (majorVersion < maxMajorVersion) || (majorVersion == maxMajorVersion) && (minorVersion < maxMinorVersion);
        }
    }
    ```

1.  Add a condition that calls your app's `IsPriorToJiraVersion` class to your `web-item` module:

    ``` xml
    <web-item ... section="system.admin/globalsettings" ... >
        ...
        <condition class="package.qualified.name.to.your.plugins.class.IsPriorToJiraVersion">
            <param name="majorVersion">4</param>
            <param name="minorVersion">4</param>
        </condition>
        ...
    </web-item>
    ```

    {{% note %}}

The `section` attribute of this web item module (for versions of Jira prior to 4.4) defines which section of the
Administration area the web item will appear in.

    {{% /note %}}
