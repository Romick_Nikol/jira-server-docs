---
aliases:
- /server/jira/platform/report-plugin-module-4227186.html
- /server/jira/platform/report-plugin-module-4227186.md
category: reference
confluence_id: 4227186
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=4227186
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=4227186
date: '2018-06-04'
legacy_title: Report Plugin Module
platform: server
product: jira
subcategory: modules
title: "Report"
---
# Report

## Purpose of this module type

A report plugin module defines a report within Jira. A Jira report can display statistical information based on
all elements within Jira, for example, issues, projects, users, issue types, and so on.

Reports have HTML results and (optionally) Excel results as well. These results are rendered by Velocity
templates included with the app. A report can also accept parameters selected by the user before running.

## Configuration

The root element for the report plugin module is `report`.
It allows the following attributes and child elements for configuration.

#### Attributes

<table>
<colgroup>
<col style="width: 20%" />
<col style="width: 80%" />
</colgroup>
<thead>
<tr class="header">
<th><p>Name</p></th>
<th><p>Description</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>class</p></td>
<td>The Java class that implements this plugin module. See the app framework guide to <a href="/server/framework/atlassian-sdk/creating-plugin-module-instances/">creating plugin module instances</a>.
<p>The class must implement
<a href="https://docs.atlassian.com/software/jira/docs/api/latest/com/atlassian/jira/plugin/report/Report.html">com.atlassian.jira.plugin.report.Report</a>,
 but we recommend that you extend the convenience class
 <a href="https://docs.atlassian.com/software/jira/docs/api/latest/com/atlassian/jira/plugin/report/impl/AbstractReport.html">com.atlassian.jira.plugin.report.impl.AbstractReport</a>
 in your app.</p>
<p><em>Required: yes<em></p>
<p><em>Default: none </em></p></td>
</tr>
<tr class="even">
<td><p>key</p></td>
<td><p>The unique identifier of the plugin module. You refer to this key to use the resource from other contexts in your plugin, such as from the plugin Java code or JavaScript resources.</p>
<p> </p>
<pre><code>&lt;component-import key=&quot;appProps&quot; interface=&quot;com.atlassian.sal.api.ApplicationProperties&quot;/&gt;</code></pre>
<p> </p>
<p>In the example, <code>appProps</code> is the key for this particular module declaration, for <code>component-import</code>, in this case. That is, the identifier of the component.</p>
<p><strong>Required: yes</strong></p>
<p><strong>Default: N/A</strong></p></td>
</tr>
<tr class="odd">
<td><p>i18n-name-key</p></td>
<td><p>The localization key for the human-readable name of the plugin module.</p>
<p><em>Required: no </em></p>
<p><em>Default: none </em></p></td>
</tr>
<tr class="even">
<td><p>name</p></td>
<td><p>The human-readable name of the plugin module. That is, the human-readable name of the component.</p>
<p><strong>Required: -</strong></p>
<p><strong>Default:</strong> The plugin key.</p></td>
</tr>
</tbody>
</table>

#### Elements

<table>
<colgroup>
<col style="width: 20%" />
<col style="width: 80%" />
</colgroup>
<thead>
<tr class="header">
<th><p>Name</p></th>
<th><p>Description</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>description</p></td>
<td><p>A human-readable description of this report module. May be specified as the value of this element for
plain text or with the <code>key</code> attribute to use the value of a key from the i18n system.</p>
<p><em>Required: no</em></p></td>
</tr>
<tr class="even">
<td><p>label</p></td>
<td><p>The user-visible name of this report. May be specified as the value of this element for plain text or
with the <code>key</code> attribute to use the value of a key from the i18n system.</p>
<p><em>Required: yes</em></p></td>
</tr>
<tr class="odd">
<td><p>resource type=&quot;velocity&quot;</p></td>
<td><p>Used to render the report results. The results format is whatever the template can output.</p>
<p><em>Required: yes</em></p></td>
</tr>
<tr class="even">
<td><p>resource type=&quot;i18n&quot;</p></td>
<td><p>A Java properties file within the app that specifies internationalization values.</p>
<p><em>Required: no</em></p></td>
</tr>
<tr class="odd">
<td><p>properties</p></td>
<td><p>Used to generate the report's configuration parameters
(for details on these properties, see the
  <a href="/server/jira/platform/object-configurable-parameters">Configuring apps with object configurable parameters</a>
page).</p>
<p> <em>Note:</em> In Jira 5.2.3 and later, when navigating to this report via Browser Project, there
are additional URL parameters of <code>projectOrFilterId</code> (that is, project ID prefixed with 'project-') and
<code>projectOrFilterName</code> (which in this case is the project name). If you have a custom project picker
inside your properties or configure page, you should respect these URL parameters to preselect the project.</p>
<p><em>Required: no</em></p></td>
</tr>
</tbody>
</table>

## Description

To make a custom report available within Jira, you need to create a report plugin module.
As with all plugin modules, the report plugin will consist of the following components all contained within a
single JAR file.

*   Java classes encapsulating report logic.
*   Resource templates for display of the report.
*   App descriptor to enable the report module in Jira.

### Report logic

The Java classes include the necessary logic to retrieve the data used in configuring and displaying the report.
The module class can implement the
[Report](https://docs.atlassian.com/software/jira/docs/api/latest/com/atlassian/jira/plugin/report/Report.html)
interface or it can extend [AbstractReport](https://docs.atlassian.com/software/jira/docs/api/latest/com/atlassian/jira/plugin/report/impl/AbstractReport.html).

The main methods of interest are:

*   `generateReportHtml` - generates HTML view of report.
*   `generateReportExcel` - generates Excel view of report.
*   `getParams` - retrieves the required data to be passed to the view template.
*   `validate` - validates any parameters used in configuring the report.

### Resource templates

The second component consists of Velocity templates used to render the report.

The templates include:

*   Report view - the actual report view.
*   Excel view - an Excel view for the report (optional).

The app system parses the `atlassian-plugin.xml` file for any configuration parameters that are required to display
the report. The app system constructs a suitable configuration screen requesting the user to specify values
for these parameters.

If an Excel view template is provided, users can view and further manipulate the data through Excel. If the Excel
template is provided, ensure that your report also implements the following method:

``` java
public boolean isExcelViewSupported()
{
    return true;
}
```

### Internationalization

It is also possible to include i18n property files to allow other users to easily translate the strings used in
the report for different languages.

## Example

This example is taken from Jira's internal time tracking report.

``` xml
<report key="time-tracking" i18n-name-key="report.timetracking.label" name="Time Tracking Report" class="com.atlassian.jira.plugin.report.impl.TimeTrackingReport">
    <description key="report.timetracking.description">This report shows the time tracking details for a specific project.</description>
    <label key="report.timetracking.label" />
    <category key="forecast.management" />
    <thumbnail cssClass="report-thumbnail-timetracking" />

    <resource type="velocity" name="view" location="templates/plugins/jira/reports/time-tracking-report.vm" />
    <resource type="velocity" name="excel" location="templates/plugins/jira/reports/time-tracking-report-excel.vm" />

    <properties>
        <property>
            <key>versionId</key>
            <name>timetracking.version</name>
            <description>report.timetracking.version.description</description>
            <type>select</type>
            <i18n>false</i18n>
            <values class="com.atlassian.jira.portal.VersionOptionalValuesGenerator"/>
        </property>
        <property>
            <key>sortingOrder</key>
            <name>report.timetracking.sortingorder</name>
            <description>report.timetracking.sortingorder.description</description>
            <type>select</type>
            <values class="com.atlassian.jira.portal.SortingValuesGenerator"/>
        </property>
        <property>
            <key>completedFilter</key>
            <name>report.timetracking.filter</name>
            <description>report.timetracking.filter.description</description>
            <type>select</type>
            <i18n>false</i18n>
            <values class="com.atlassian.jira.portal.FilterValuesGenerator"/>
        </property>
        <property>
            <key>subtaskInclusion</key>
            <name>report.subtask.include</name>
            <description>report.subtask.include.description</description>
            <type>select</type>
            <i18n>false</i18n>
            <values class="com.atlassian.jira.plugin.report.impl.SubTaskIncludeValuesGenerator"/>
            <enabled-condition class="com.atlassian.jira.plugin.report.impl.SubTasksEnabledCondition"/>
        </property>
    </properties>
</report>
```

In this example, the report logic is encapsulated in the
[TimeTrackingReport](https://docs.atlassian.com/software/jira/docs/api/latest/com/atlassian/jira/plugin/corereports/report/impl/TimeTrackingReport.html)
Java class, the view template location is specified in the `templates/plugins/jira/reports/time-tracking-report.vm` directory.
Following that, the parameters required to configure the report are specified - in this case, the version, the sort order, and a filter.

## Notes

{{% note %}}

For more details, see the [tutorial on creating a JIRA report](/server/jira/platform/creating-a-jira-report).

{{% /note %}}
