---
aliases:
- /server/jira/platform/tutorial-creating-a-project-template-22511717.html
- /server/jira/platform/tutorial-creating-a-project-template-22511717.md
category: devguide
confluence_id: 22511717
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=22511717
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=22511717
date: '2018-05-08'
guides: tutorials
legacy_title: Tutorial - Creating a project template
platform: server
product: jira
subcategory: learning
title: "Creating a project template"
---
# Creating a project template

<table>
<colgroup>
<col style="width: 20%" />
<col style="width: 80%" />
</colgroup>
<tbody>
<tr class="odd">
<td><p>Applicable:</p></td>
<td><p>Jira 7.0.0 and later.</p></td>
</tr>
<tr class="even">
<td><p>Level of experience:</p></td>
<td><p>Intermediate. You should have completed at least one beginner tutorial before working through
this tutorial. See the <a href="/server/framework/atlassian-sdk/tutorials/">list of developer tutorials</a>.</p></td>
</tr>
<tr class="odd">
<td><p>Time estimate:</p></td>
<td><p>It should take you approximately 1 hour to complete this tutorial.</p></td>
</tr>
</tbody>
</table>

{{% note %}}

Project template apps were originally supported in Jira 6.0 with the `project-template` module. However,
Jira 6.0.7 introduces the `project-blueprint` plugin module that provides additional capabilities and is
meant to substitute the `project-template` module. We strongly encourage you to use the `project-blueprint`
module for project template app development, as described in this tutorial.

{{% /note %}}

## Overview of the tutorial

Jira project templates allow administrators to quickly create Jira projects with a predefined set of project
configuration settings and properties. Jira comes with several built-in project templates, but you can create
your own as Jira apps using the `project-blueprint` plugin module.

Besides hooking into the Jira project creation process, a project template app can define issue type schemes
(with a set of custom issue types) and workflow schemes that link the issue types to Jira workflows.

This tutorial shows you how to build a project template app. Our project template will define a custom issue type
scheme with three issue types and a workflow scheme with two imported workflows.

The completed Jira app will consist of the following components:

1.   Java classes encapsulating the app logic.
1.   Resources for display of the app UI (user interface).
1.   An app descriptor (XML file) to enable the plugin module in the Atlassian application.
1.   A configuration JSON file for your Project Template.
1.   A couple Jira workflow bundles for importing the workflows in your project template.

When you are finished, all these components are packaged in a single JAR file.

{{% note %}}

About these instructions

You can use any supported combination of OS and IDE to create this app. These instructions were written using IntelliJ
IDEA 2017.3 on macOS Sierra. If you use another OS or IDE combination, you should use the equivalent operations
for your specific environment.

This tutorial was last tested with Jira 7.7.1.

{{% /note %}}

### Before you begin

To complete this tutorial, you need to know the following:

1.   The basics of Java development: classes, interfaces, methods, how to use the compiler, and so on.
1.   How to create an Atlassian plugin project using the [Atlassian Plugin SDK](/server/jira/platform/creating-a-project-template).
1.   How to use and administer Jira, particularly how to configure Jira projects.

### App source

We encourage you to work through this tutorial. If you want to skip ahead or check your work when you have finished, you
can find the app source code on Atlassian Bitbucket.

To clone the repository, run the following command:

``` bash
git clone git@bitbucket.org:atlassian_tutorial/jira-project-templates-tutorial.git
```

Alternatively, you can download the source as a [ZIP archive](https://bitbucket.org/atlassian_tutorial/jira-project-templates-tutorial/get/master.zip):

## Step 1. Create the app project

In this step, you'll use the `atlas` SDK to generate stub code for your app. The `atlas` commands are part of the
Atlassian Plugin SDK and automate much of the work of app development for you.

1.  [Set up the Atlassian Plugin SDK and Build a Project](/server/framework/atlassian-sdk/set-up-the-atlassian-plugin-sdk-and-build-a-project/)
if you have not done it yet.
1.  Open a Terminal and navigate to the directory where you want to create the project.
1.  To create an app skeleton, run the following command:

    ``` bash
    atlas-create-jira-plugin
    ```

1.  To identify your app, enter the following information.

    <table>
    <colgroup>
    <col style="width: 50%" />
    <col style="width: 50%" />
    </colgroup>
    <tbody>
    <tr class="odd">
    <td><p>group-id</p></td>
    <td><p><code>com.example.plugins.tutorial</code></p></td>
    </tr>
    <tr class="even">
    <td><p>artifact-id</p></td>
    <td><p><code>my-project-template</code></p></td>
    </tr>
    <tr class="odd">
    <td><p>version</p></td>
    <td><p><code>1.0-SNAPSHOT</code></p></td>
    </tr>
    <tr class="even">
    <td><p>package</p></td>
    <td><p><code>com.example.plugins.tutorial</code></p></td>
    </tr>
    </tbody>
    </table>

1.  Confirm your entries.

    The SDK generates the project home directory with project files, such as the POM (Project Object Model definition file),
stub source code, and app resources.

1.  Delete the test directories.

    Setting up testing for your app isn't part of this tutorial. To delete the generated test skeleton,
run the following commands:

    ``` bash
    rm -rf src/test/resources
    rm -rf src/test/java
    ```

1.  Delete the unneeded Java class files.

    ``` bash
    rm -rf src/main/java/com/example/plugins/tutorial
    ```

1.  Import the project to your favorite IDE.

## Step 2. Tweak the POM

It's a good idea to familiarize yourself with the project configuration file, known as the POM.
Among other functions, the POM declares project dependencies and controls build settings.
It also contains descriptive information for your app. 

Tweak the metadata and add a dependency as follows:

1.  Navigate to the root folder of your project and open the `pom.xml` file.  

1.  Add your company or organization name and website URL to the `organization` element:

    ``` xml
    <organization>
        <name>Example Company</name>
        <url>http://www.example.com/</url>
    </organization>
    ```

1.  To add a meaningful description, edit the project `description` element. For example:

    ``` xml
    <description>This is the Project Template plugin tutorial for Atlassian Jira.</description>
    ```

    The `organization` and `description` values you enter are propagated to the app descriptor file, `atlassian.plugin.xml`.
    From there, Jira uses them in the administration console display for your app.

1.  Under the `dependencies` element, add a dependency to the Project Templates API artifact:  

    ``` xml
    <dependency>
        <groupId>com.atlassian.jira.plugins</groupId>
        <artifactId>project-templates-api</artifactId>
        <version>6.2.0</version>
        <scope>provided</scope>
    </dependency>
    ```

1.  Save the file.

If working in an IDE, you need to refresh the dependencies in your project now.

## Step 3. Add the plugin module to the app descriptor

Now you will add plugin modules to your app descriptor. The app descriptor is an XML file that identifies the
app to Jira and defines the functionality that the app requires. We need to add two plugin modules: one for our
project template and another for a [web resource](/server/jira/platform/web-resource).

Project templates have their own module type called `project-blueprint` which takes care of the necessary configuration.
Add it to your descriptor as follows:

1.  Navigate to `src/main/resources` and create a new `soy` folder.

1.  Create new Soy template `MyProjectTemplate.soy` with the following content:

    ``` text
        {namespace JIRA.Templates.ProjectTemplates.Tutorial}

        /**
         * Render the information page for My Project Template.
         */
        {template .renderMyProjectTemplateExplanation}
            <p>{getText('my.project.template.info.page.description')}</p>
        {/template}
    ```

    We will use this template for project info page.

1.  Open the `atlassian-plugin.xml` file and add the following as a child of the `atlassian-plugin` element: 

    ``` xml
    <atlassian-plugin ...>
      ...
        <web-resource key="my-project-template-resources" name="my-project-template Web Resources">
            <dependency>com.atlassian.auiplugin:ajs</dependency>

            <transformation extension="soy">
                <transformer key="soyTransformer"/>
            </transformation>
            <transformation extension="js">
                <transformer key="jsI18n"/>
            </transformation>

            <resource type="download" name="MyProjectTemplate.soy.js" location="/soy/MyProjectTemplate.soy"/>

            <resource type="download" name="images/" location="/images">
                <param name="content-type" value="image/png; charset=binary"/>
            </resource>

            <context>atl.general</context>
            <context>atl.admin</context>
        </web-resource>

        <project-blueprint key="my-project-template" weight="90">
            <label key="my.project.template.name"/>
            <projectTypeKey>business</projectTypeKey>
            <description key="my.project.template.description"/>
            <longDescription key="my.project.template.description.long"/>

            <infoPage soy-template="JIRA.Templates.ProjectTemplates.Tutorial.renderMyProjectTemplateExplanation" />

            <icon location="images/my-template-icon.png"/>
            <backgroundIcon location="images/my-template-background.png"/>

            <add-project>
                <hook class="com.example.plugins.tutorial.MyAddProjectHook"/>
                <descriptor file="/config/my-project-template-config.json"/>
            </add-project>
        </project-blueprint>

      ...
    </atlassian-plugin>
    ```

    At a minimum, you would only need to add the `project-blueprint` element, but since we're also adding an optional
    information page to the wizard, we need to define a `web-resource` element as well. Our `web-resource` element
    identifies the [Soy template](https://docs.atlassian.com/aui/latest/docs/soy.html) file that presents our text page in the wizard.

Let's take a closer look at the attributes and child elements of the `project-blueprint` app that we defined here.

<table>
<colgroup>
<col style="width: 20%" />
<col style="width: 80%" />
</colgroup>
<thead>
<tr class="header">
<th>Name*</th>
<th>Description</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>key</td>
<td><p>The identifier of the plugin module. The key should be unique in the context of your app.</p></td>
</tr>
<tr class="even">
<td>weight</td>
<td><p>Determines the order in which the project templates appear.</p>
<p><em>Default:</em> 100</p></td>
</tr>
<tr class="odd">
<td>label</td>
<td>The <code>key</code> attribute defines the localization key for the human-readable name of the plugin module. This name is used to identify the project template in the Project Templates dialog.</td>
</tr>
<tr class="even">
<td>projectTypeKey</td>
<td><em>(Jira 7.0+)</em> All the project templates must provide the key for the type of the projects created with the template. Valid values for the project type key are &quot;<code>business</code>&quot;,  &quot;<code>software</code>&quot;, and  &quot;<code>service_desk</code>&quot;.</td>
</tr>
<tr class="odd">
<td>description</td>
<td>The <code>key</code> attribute defines the localization key for the human-readable short description of the plugin module. This description is used on the first page of the project templates dialog where all the available project templates are listed. </td>
</tr>
<tr class="even">
<td>longDescription</td>
<td><p>The <code>key</code> attribute defines the localization key for the human-readable long description of the plugin module. The long description is used on the right side of the Create Project form of the Project Templates dialog wizard. If no <code>longDescription</code> is specified, the short <code>description</code> is used. </p>
<p><em>Default:</em> description.</p></td>
</tr>
<tr class="odd">
<td>infoPage</td>
<td><p>The <code>soy-template</code> attribute refers to a <a href="https://docs.atlassian.com/aui/latest/docs/soy.html" class="external-link">Soy Template</a> that renders a page in the Project Templates dialog. Typically, you would use this page to explain the characteristics of the Project Template to Jira administrator.</p>
<p>Make sure that the Soy Template is also declared as a web-resource in both <code>atl.general</code> and <code>atl.admin</code> contexts. The information page will be shown after the administrator selects your Project Template from the initial Project Templates selection dialog. If you do not define an <code>infoPage</code> element, the Project Templates wizard skips that step and goes directly to the Create Project form of the wizard.</p>
<p><em>Default:</em> No information page.</p></td>
</tr>
<tr class="even">
<td>icon</td>
<td><p>The <code>location</code> attribute defines an icon to be used when listing the available project templates in the selection screen of the Project Templates dialog. </p>
<p><em>Default:</em> default icon.</p></td>
</tr>
<tr class="odd">
<td>backgroundIcon</td>
<td><p>The <code>location</code> attribute defines the background image to use on the Create Project form of the wizard.</p>
<p><em>Default:</em> default background image.</p></td>
</tr>
<tr class="even">
<td>add-project</td>
<td>Holds the custom configuration instructions for the project that will be created.</td>
</tr>
</tbody>
</table>

*\*key, label, projectTypeKey, description are required.*

The `add-project` element also holds a number of configuration parameters that are specific for the actual project creation.

<table>
<colgroup>
<col style="width: 20%" />
<col style="width: 80%" />
</colgroup>
<thead>
<tr class="header">
<th>Name</th>
<th>Description</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>hook</td>
<td><p>The <code>class</code> attribute refers to the Java class that implements the <code>AddProjectHook</code> interface. This allows project templates to hook into the validation and configuration of Jira projects.</p></td>
</tr>
<tr class="even">
<td>descriptor</td>
<td><p>The <code>file</code> attribute refers to a JSON file which defines parts of the configuration for the project.</p></td>
</tr>
</tbody>
</table>  

## Step 4. Generate workflow bundles

Project templates can incorporate one or more Jira workflows as part of their project configuration. To incorporate a
workflow in your project template, you first need to define the workflow in an existing Jira project, and then use the
[workflow sharing](https://confluence.atlassian.com/display/ADMINJIRASERVER/Sharing+your+workflow)
feature to export the workflow from there.

Exporting a workflow results in a Jira workflow bundle (`.jwb`) file, one for each exported workflow. You can then use the
`.jwb` file in your project template app. Keep in mind that an exported Jira workflow doesn't include custom fields,
post functions, validators, and conditions. If you want your workflow to have this functionality when using a project template,
you can use the [AddProjectHook](https://docs.atlassian.com/software/jira/docs/api/latest/com/atlassian/jira/project/template/hook/AddProjectHook.html) 
to create it. 

1.  To get a workflow bundle file for the app, do one of the following:

    1.   Create a workflow export bundle of the workflow you want to use in your project by following the
    [instructions for exporting a workflow](https://confluence.atlassian.com/display/ADMINJIRASERVER/Sharing+your+workflow).
    1.   Get the workflow bundles we've built for you. The bundles are in the `src/main/resources/wfb/` directory of the
    [Bitbucket repository for this tutorial](https://bitbucket.org/atlassian_tutorial/jira-project-templates-tutorial). 

1.  In a new directory under your `src/main/resources/wfb`, save the workflow bundle file. 

## Step 5. Set up the JSON configuration file

The JSON configuration file allows you to declare a number of configuration options for your project template: 

*   Issue types
*   Issue type scheme
*   Workflows
*   Workflow type scheme
*   The mapping of issue types to workflows

The JSON configuration file for a project template is optional and is needed only if the `hook` element is declared in the 
`add-project` element of a `project-blueprint` plugin module. In this step, we add one.

1.  Navigate to `src/main/resources/config` and create a new JSON file named `my-project-template-config.json`.
1.  Add the following code to the file: 

    *my-project-template-config.json*

    ``` javascript
    {
        "issue-type-scheme":
        {
            "name": "my.project.template.issuetype.scheme.name",
            "description": "my.project.template.issuetype.scheme.description",
            "issue-types": [
                {
                    "key": "issueType1",
                    "name": "New Feature",
                    "description": "A new feature of the product, which has yet to be developed.",
                    "icon": "/images/icons/newfeature.png",
                    "workflow": "wf1"
                },
                {
                    "key": "issueType2",
                    "name": "Bug",
                    "description": "A problem which impairs or prevents the functions of the product.",
                    "icon": "/images/icons/bug.png"
                },
                {
                    "key": "issueType3",
                    "name": "Sub-Task",
                    "description": "The sub-task of the issue",
                    "icon": "/images/icons/subtask_alternate.png",
                    "sub-task": true,
                    "workflow": "wf2"
                }
            ]
        },
        "workflow-scheme":
        {
            "name": "my.project.template.workflow.scheme.name",
            "description": "my.project.template.workflow.scheme.description",
            "default-workflow": "wf1",
            "workflows": [
                {
                    "key": "wf1",
                    "name": "my.project.template.workflow.wf1.name",
                    "workflow-bundle": "/wfb/Issue-Tracking-Workflow.jwb"
                },
                {
                    "key": "wf2",
                    "name": "my.project.template.workflow.wf2.name",
                    "workflow-bundle": "/wfb/Software-Development-Workflow.jwb"
                }
            ]
        }
    }
    ```

{{% note %}}
In `my-project-template-config.json` and `atlassian-plugin.xml` we defined few icons.
Download them from [Bitbucket](https://bitbucket.org/atlassian_tutorial/jira-project-templates-tutorial/src/master/src/main/resources/images/)
and place under `src/main/resources/images`.
{{% /note %}}

Let's take a closer look at the meaning of the configuration options here: 

*   The values of `name` and `description` attributes of `issue-type-scheme` and `workflow-scheme` refer to i18n keys.
*   The `key` attribute of an issue type only serves as an internal identifier within the project templates infrastructure. 
*   The `icon` attribute of an issue type refers to an icon location in the `src/main/resources` directory of your app.
*   The `sub-task` attribute of an issue type lets you declare whether the issue type is a normal issue type
(default) or a sub-task.
*   If the issue types that are declared in the `issue-type-scheme` don't exist in the Jira instance, they are
created using the declared configuration.
*   The `key` attribute of a workflow can be used to map issue types to workflows, or to declare the `default-workflow` 
for a `workflow-scheme`.
*   The `default-workflow` attribute of a `workflow-scheme` lets you define which workflow to use if an issue type
is used without an explicit workflow mapping. The value refers to the `key` attribute of a workflow.
*   The `workflow-bundle` of a workflow refers to a Jira workflow bundle location in the `src/main/resources` 
directory of your app.

## Step 6. Write Java class

In this step you write the Java class. This example simply creates a basic project which checks whether the project
key is not "TEST". When the project is successfully created, it redirects the user from the Create project dialog
to the Issues Browser.

1.  Navigate to `src/main/java/com/example/plugins/tutorial` and create a new class named `MyAddProjectHook`.
1.  Add the following code to the class:

    *MyAddProjectHook*

    ``` java
    package com.example.plugins.tutorial;

    import com.atlassian.jira.project.template.hook.AddProjectHook;
    import com.atlassian.jira.project.template.hook.ConfigureData;
    import com.atlassian.jira.project.template.hook.ConfigureResponse;
    import com.atlassian.jira.project.template.hook.ValidateData;
    import com.atlassian.jira.project.template.hook.ValidateResponse;

    public class MyAddProjectHook implements AddProjectHook
    {
        @Override
        public ValidateResponse validate(final ValidateData validateData)
        {
            ValidateResponse validateResponse = ValidateResponse.create();
            if (validateData.projectKey().equals("TEST"))
            {
                validateResponse.addErrorMessage("Invalid Project Key");
            }

            return validateResponse;
        }

        @Override
        public ConfigureResponse configure(final ConfigureData configureData)
        {
            return ConfigureResponse.create();
        }
    }
    ```

Notice that the new class implements the `AddProjectHook` interface. The interface defines two methods:

*   `validate()` allows your app to validate the parameters to be used for creating the project. It takes the
parameters as an argument, and can return errors in the `ValidateResponse` object. The errors, if any, are merged
with any other errors resulting from Jira's project validation and are returned to the client.
*   `configure()` performs setup actions for the project post-creation. It takes the new `ConfigureData` as an
argument, which holds all the created entities (project, workflow scheme, workflows, issue type scheme, and issue types).   
    The `configure()` function can be used for a wide variety of configuration options. For example, you can use
    this to create Workflow Post Functions, Resolutions, and more.

## Step 7. Add the i18n keys

In a couple of places we referenced i18n keys. We need to provide values for those i18n keys in the i18n properties file.

1.  Navigate to `src/main/resources` and open the `my-project-template.properties` file.
1.  Add the following lines:

    ``` java
    #put any key/value pairs here
    my.project.template.name=My Project Template
    my.project.template.description=This is my custom Project Template
    my.project.template.description.long=This custom Project Template will create a new Project with customized workflows and issue types.
    my.project.template.info.page.description=Explain the configurations specifics of your template here.
    my.project.template.issuetype.scheme.name=My Project Template Issue Type Scheme
    my.project.template.issuetype.scheme.description=This is my Issue Type Scheme
    my.project.template.workflow.scheme.name=My Project Template Workflow Scheme
    my.project.template.workflow.scheme.description=My Project Template Workflow Scheme
    my.project.template.workflow.wf1.name=My Workflow 1
    my.project.template.workflow.wf2.name=My Workflow 2
    ```

1.  To ensure that this file is used for [translations](/server/framework/atlassian-sdk/internationalising-your-plugin/), make
sure the following line is in your `atlassian-plugin.xml`:

    ``` xml
      <resource type="i18n" name="i18n" location="my-project-template"/>
    ```

## Step 8. Build, install, and run the app

Now you're ready to try out the new project app:

1.  Open a Terminal and navigate to the project root directory.
1.  Run the following SDK command:

    ``` bash
    atlas-run
    ```

    This builds your app code, starts a Jira instance, and installs your app. This could take a few minutes.

1.  Find the Jira home page URL in the Terminal output and open it in your browser.
1.  Log in with the default admin/admin.
1.  Create a new project based on the `My Project Template` template. This is the template you created.   
    <img src="/server/jira/platform/images/template-project-example.png" width="650" />
1.  Check that the information page is shown.  
    <img src="/server/jira/platform/images/screen-shot-2013-08-09-at-4.08.05-pm.png" width="650" />
1.  Enter a project name and a key, as usual.  
    <img src="/server/jira/platform/images/screen-shot-2013-08-09-at-4.08.24-pm.png" width="650" />

1.  Click **Submit**. Confirm that you are redirected to the issue browser page.

{{% tip %}}

Congratulations, that's it!

Have a treat!

{{% /tip %}}

### Next steps

To learn more about the customizing projects in Jira, go to these tutorials:

* [Adding menu items to JIRA](/server/jira/platform/adding-menu-items-to-jira/).
* [Adding content to the Jira View Issue page](/server/jira/platform/adding-content-to-the-jira-view-issue-page/).
* [Adding a JQL function to Jira](/server/jira/platform/adding-a-jql-function-to-jira/).
