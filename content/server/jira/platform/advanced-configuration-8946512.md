---
title: Advanced Configuration 8946512
aliases:
    - /server/jira/platform/advanced-configuration-8946512.html
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=8946512
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=8946512
confluence_id: 8946512
platform: server
product: jira
category: devguide
subcategory:
date: "2018-04-25"
---
# JIRA Developer Documentation : Advanced Configuration

There are some other configuration options for [JIRA Mobile Connect](https://developer.atlassian.com/display/JMC/JIRA+Mobile+Connect+Developer+Documentation) (JMC) that you can choose to set if the defaults are not what you require. To do this, explore the `[[JMC sharedInstance] configureXXX]` methods.

## JMCOptions Object

The JMCOptions object supports most of the advanced settings. This object is passed to JMC when configure is called. That is, during applicationDidFinishLaunching. The JMCOptions class lets you configure:

<table>
<colgroup>
<col style="width: 25%" />
<col style="width: 25%" />
<col style="width: 25%" />
<col style="width: 25%" />
</colgroup>
<thead>
<tr class="header">
<th><p>Option</p></th>
<th><p>Property</p></th>
<th><p>Type</p></th>
<th><p>Default</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>Screenshots</p></td>
<td><p><code>photos</code></p></td>
<td><p>Boolean</p></td>
<td><p><code>YES</code></p></td>
</tr>
<tr class="even">
<td><p>Voice recordings</p></td>
<td><p><code>voice</code></p></td>
<td><p>Boolean</p></td>
<td><p><code>YES</code></p></td>
</tr>
<tr class="odd">
<td><p>Location tracking</p></td>
<td><p><code>location</code></p></td>
<td><p>Boolean</p></td>
<td><p><code>NO</code></p></td>
</tr>
<tr class="even">
<td><p>Crash reporting</p></td>
<td><p><code>crashreporting</code></p></td>
<td><p>Boolean</p></td>
<td><p><code>YES</code></p></td>
</tr>
<tr class="odd">
<td><p>UIBarStyle for JMC Views</p></td>
<td><p><code>barStyle</code></p></td>
<td><p>UIBarStyle</p></td>
<td><p><code>UIBarStyleDefault</code></p></td>
</tr>
<tr class="even">
<td><p>JIRA project key</p></td>
<td><p><code>projectKey</code></p></td>
<td><p>NSString</p></td>
<td><p><code>nil</code></p></td>
</tr>
<tr class="odd">
<td><p>JIRA site URL</p></td>
<td><p><code>url</code></p></td>
<td><p>NSString</p></td>
<td><p><code>nil</code></p></td>
</tr>
<tr class="even">
<td><p>API key</p></td>
<td><p><code>apiKey</code></p></td>
<td><p>NSString</p></td>
<td><p><code>nil</code></p></td>
</tr>
<tr class="odd">
<td><p>Custom fields</p></td>
<td><p><code>customFields</code></p></td>
<td><p>NSDictionary</p></td>
<td><p><code>nil</code></p></td>
</tr>
</tbody>
</table>

Below is an implementation example (within AppDelegate.m) that includes all of the above options configured.

``` javascript
JMCOptions* options = [JMCOptions optionsWithUrl:@"http://connect.onjira.com"
                                       projectKey:@"NERDS"
                                           apiKey:@"591451a6-bc59-4ca9-8840-b67f8c1e440f"
                                           photos:YES
                                            voice:YES
                                         location:YES
                                   crashReporting:YES
                                     customFields:nil
                                         barStyle:UIBarStyleDefault];

[[JMC sharedInstance] configureWithOptions:options];
```

See the the `JMC.h` file for all JMCOptions available.

## JMCCustomDataSource Object

The JMCCustomDataSource can be used to provide JIRA with extra data at runtime. The following properties are supported:

<table>
<colgroup>
<col style="width: 33%" />
<col style="width: 33%" />
<col style="width: 33%" />
</colgroup>
<thead>
<tr class="header">
<th><p>Option</p></th>
<th><p>Purpose</p></th>
<th><p>Property</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>Extra attachment</p></td>
<td><p>Ability to add custom attachments when issues are raised (for example, a database file)</p></td>
<td><p><code>customAttachment</code></p></td>
</tr>
<tr class="even">
<td><p>Custom fields</p></td>
<td><p>Dictionary containing any specialised custom fields to be set on any issues created (keyed by custom field name)</p></td>
<td><p><code>customFields</code></p></td>
</tr>
<tr class="odd">
<td><p>Components</p></td>
<td><p>Issue components to set (for example, 'iOS')</p></td>
<td><p><code>components</code></p></td>
</tr>
<tr class="even">
<td><p>JIRA issue type</p></td>
<td><p>Sets the type of the issue (for example Crash --&gt; Bug, Feedback --&gt; Improvement)</p></td>
<td><p><code>jraIssueTypeNameFor</code></p></td>
</tr>
<tr class="odd">
<td><p>Notifier start frame</p></td>
<td><p>Controls the in-app placement of notifications</p></td>
<td><p><code>notifierStartFrame</code></p></td>
</tr>
<tr class="even">
<td><p>Notifier end frame</p></td>
<td><p>Controls the in-app placement of notifications</p></td>
<td><p><code>notifierEndFrame</code></p></td>
</tr>
</tbody>
</table>

Below is an implementation example of each of the above custom options (within a class implementing the JMCCustomDataSource protocol).

``` javascript
- (NSDictionary *)customFields
{
    return [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:@"9999", @"42" ,nil]
                                       forKeys:[NSArray arrayWithObjects:@"Top Score", @"jmctestfield", nil]];
}

-(NSArray*) components
{
    return [NSArray arrayWithObjects:@"iOS", @"JIRA", nil];
}

- (NSString *)jiraIssueTypeNameFor:(JMCIssueType)type
{
    if (type == JMCIssueTypeCrash) {
        return @"crash";
    } else if (type == JMCIssueTypeFeedback) {
        return @"improvement";
    }
    return nil;
}


- (JMCAttachmentItem *) customAttachment
{

    return [[[JMCAttachmentItem alloc] initWithName:@"custom-attachment"
                                              data:[@"Add any other data as an attachment" dataUsingEncoding:NSUTF8StringEncoding]
                                              type:JMCAttachmentTypePayload
                                       contentType:@"text/plain"
                                    filenameFormat:@"customattachment.txt"] autorelease];
}

-(CGRect)notifierStartFrame
{
    CGRect frame = self.view.frame;
    return CGRectMake(frame.origin.x, frame.size.height, frame.size.width, 40); // start just off screen.
}

-(CGRect)notifierEndFrame
{
    CGRect frame = self.view.frame;
    return CGRectMake(frame.origin.x, frame.size.height - 40 + 20, frame.size.width, 40); // end 40 pixels from bottom of the screen
}
```

JMCCustomDataSource implementation in AppDelegate.m (inside the didFinishLaunchingWithOptions method):

``` javascript

 [[JMC sharedInstance] configureJiraConnect:@"http://connect.onjira.com"
                              projectKey:@"NERDS"
                                  apiKey:@"b84bcd12-1e02-47e9-8954-7e1671b42b55"
                                location:YES
                              dataSource:viewController];
```

OR

``` javascript
[JMC sharedInstance].customDataSource = viewController;
```
