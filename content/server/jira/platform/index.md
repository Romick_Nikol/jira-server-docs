---
aliases:
- /server/jira/platform/latest-updates-32344056.html
- /server/jira/platform/latest-updates-32344056.md
category: devguide
confluence_id: 32344056
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=32344056
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=32344056
date: '2018-09-11'
legacy_title: "Latest updates"
platform: server
product: jira
subcategory: index
title: "Latest updates"
---
# Latest updates

We release new versions of our Jira Server products frequently. As a Jira developer, it's important that you're aware of the changes. The resources below will help you keep track of what's happening.

## Atlassian Developer blog

Major changes that affect Jira Server developers are announced in the Atlassian Developer blog, like new Jira modules or the deprecation of API end points. You'll also find handy tips and articles related to Jira development.

Check it out and subscribe here: [Atlassian Developer blog](https://developer.atlassian.com/blog/categories/jira/) *(Jira-related posts)*

Other important updates:

-   [SOAP and XML-RPC API Deprecation Notice](https://developer.atlassian.com/display/JIRADEV/SOAP+and+XML-RPC+API+Deprecation+Notice)
-   [Discontinuation of support for Atlassian IDE Connectors](https://developer.atlassian.com/display/JIRASERVER/Discontinuation+of+support+for+Atlassian+IDE+Connectors)

## Changes for the Jira Server version currently under development

<table>
<colgroup>
<col style="width: 20%" />
<col style="width: 80%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Preparation guide</td>
<td><a href="https://confluence.atlassian.com/display/AdminJIRA/Preparing+for+Jira+8.1">Preparing for Jira 8.1</a></td>
</tr>
<tr class="even">
<td>EAP downloads</td>
<td><a href="http://www.atlassian.com/software/jira/download-eap" class="external-link">Download EAP</a></td>
</tr>
</tbody>
</table>

An **Early Access Program (EAP) release** is a public development release leading up to the official release of a Jira version. Development releases are a snapshot of our work in progress, primarily focused on allowing Jira users to see the new features in advance and provide us with some useful feedback. It also gives app developers an opportunity to test and fix their apps in advance of an official release.

EAP releases, which include more mature Beta and Release Candidate (RC) builds are available for <a href="http://www.atlassian.com/software/jira/download-eap" class="external-link">download</a> and their release notes are listed on this page.  
*Note, Beta and Release Candidate builds generally do not introduce new changes, and provide bug fixes and tweaks to features introduced in earlier EAP releases instead.*

EAP releases can include changes like: 

-   Changes to our APIs
-   Database changes
-   Changes to Jira bundled apps and libraries
-   Changes in our supported platforms

## Earlier EAP releases

-   **Jira platform version 8.0: [Preparing for Jira 8.0](https://confluence.atlassian.com/display/AdminJIRA/Preparing+for+Jira+8.0)**

-   **Jira platform version 7.13: [Preparing for Jira 7.13](/server/jira/platform/preparing-for-jira-7-13)**
    
-   **Jira platform version 7.12: [Preparing for Jira 7.12](/server/jira/platform/preparing-for-jira-7-12)**
    -   [Jira Core 7.12 EAP release notes](/server/jira/platform/jira-core-eap-7-12-release-notes)
    -   [Jira Software 7.12 EAP release notes](/server/jira/platform/jira-software-eap-7-12-release-notes)
    
-   **Jira platform version 7.11: [Preparing for Jira 7.11](/server/jira/platform/preparing-for-jira-7-11)**
    -   [Jira Core 7.11 EAP release notes](/server/jira/platform/jira-core-eap-7-11-release-notes)
    -   [Jira Software 7.11 EAP release notes](/server/jira/platform/jira-software-eap-7-11-release-notes)
    
-   **Jira platform version 7.10: [Preparing for Jira 7.10](/server/jira/platform/preparing-for-jira-7-10)**
    -   [Jira Core 7.10 EAP release notes](/server/jira/platform/jira-core-eap-7-10-release-notes)
    -   [Jira Software 7.10 EAP release notes](/server/jira/platform/jira-software-eap-7-10-release-notes)
<br><br>

-   **JIRA platform version 7.9: [Preparing for JIRA 7.9](/server/jira/platform/preparing-for-jira-7-9)**
    -   [JIRA Core 7.9 EAP release notes](/server/jira/platform/jira-core-eap-7-9-release-notes)
    -   [JIRA Software 7.9 EAP release notes](/server/jira/platform/jira-software-eap-7-9-release-notes)
<br><br>

-   **JIRA platform version 7.8: [Preparing for JIRA 7.8](/server/jira/platform/preparing-for-jira-7-8)**
    -   [JIRA Core 7.8 EAP release notes](/server/jira/platform/jira-core-eap-7-8-release-notes)
    -   [JIRA Software 7.8 EAP release notes](/server/jira/platform/jira-software-eap-7-8-release-notes)
<br><br>

-   **JIRA platform version 7.7: [Preparing for JIRA 7.7](/server/jira/platform/preparing-for-jira-7-7)**
    -   [JIRA Core 7.7 EAP release notes](/server/jira/platform/jira-core-eap-7-7-release-notes)
    -   [JIRA Software 7.7 EAP release notes](/server/jira/platform/jira-software-eap-7-7-release-notes)
<br><br>

-   **JIRA platform version 7.6: [Preparing for JIRA 7.6](https://developer.atlassian.com/display/JIRADEV/Preparing+for+JIRA+7.6)**
    -   [JIRA Core 7.6 EAP RC release notes](https://developer.atlassian.com/display/JIRADEV/JIRA+Core+7.6+EAP+RC+release+notes)
    -   [JIRA Software 7.6 EAP RC release notes](https://developer.atlassian.com/display/JIRADEV/JIRA+Software+7.6+EAP+RC+release+notes)
    -   [JIRA Core 7.6 EAP release notes](https://developer.atlassian.com/display/JIRADEV/JIRA+Core+7.5+EAP+release+notes)
    -   [JIRA Software 7.6 EAP release notes](https://developer.atlassian.com/display/JIRADEV/JIRA+Software+7.5+EAP+release+notes)
    -   [JIRA Service Desk 3.8 EAP release notes](https://developer.atlassian.com/display/JIRADEV/JIRA+Service+Desk+3.8+EAP+release+notes)
<br><br>

-   **JIRA platform version 7.5: [Preparing for JIRA 7.5](https://developer.atlassian.com/display/JIRADEV/Preparing+for+JIRA+7.5)**
    -   [JIRA Core 7.5 EAP release notes](https://developer.atlassian.com/display/JIRADEV/JIRA+Core+7.5+EAP+release+notes)
    -   [JIRA Software 7.5 EAP release notes](https://developer.atlassian.com/display/JIRADEV/JIRA+Software+7.5+EAP+release+notes)
    -   [JIRA Service Desk 3.8 EAP release notes](https://developer.atlassian.com/display/JIRADEV/JIRA+Service+Desk+3.8+EAP+release+notes)
<br><br>

-   **JIRA platform version 7.4: [Preparing for JIRA 7.4](https://developer.atlassian.com/display/JIRADEV/Preparing+for+JIRA+7.4)** 
    -   [JIRA Core 7.4 EAP release notes](https://developer.atlassian.com/display/JIRADEV/JIRA+Core+7.4+EAP+release+notes)
    -   [JIRA Software 7.4 EAP release notes](https://developer.atlassian.com/display/JIRADEV/JIRA+Software+7.4+EAP+release+notes)
<br><br> 
-   **JIRA platform version 7.3: [Preparing for JIRA 7.3](https://developer.atlassian.com/display/JIRADEV/Preparing+for+JIRA+7.3)**
    -   [JIRA Core 7.3 release candidate (RC02) release notes](https://developer.atlassian.com/display/JIRADEV/JIRA+Core+7.3+release+candidate+%28RC02%29+release+notes)
    -   [JIRA Software 7.3 release candidate (RC02) release notes](https://developer.atlassian.com/display/JIRADEV/JIRA+Software+7.3+release+candidate+%28RC02%29+release+notes)
    -   [JIRA Service Desk 3.3 release candidate (RC02) release notes](https://developer.atlassian.com/display/JIRADEV/JIRA+Service+Desk+3.3+release+candidate+%28RC02%29+release+notes)
    -   [JIRA Core 7.3 EAP4 release notes](https://developer.atlassian.com/display/JIRADEV/JIRA+Core+7.3+EAP4+release+notes)
    -   [JIRA Software 7.3 EAP4 release notes](https://developer.atlassian.com/display/JIRADEV/JIRA+Software+7.3+EAP4+release+notes)
    -   [JIRA Service Desk 3.3 EAP4 release notes](https://developer.atlassian.com/display/JIRADEV/JIRA+Service+Desk+3.3+EAP4+release+notes)
<br><br>
-   **JIRA platform version 7.2: [Preparing for JIRA 7.2](https://developer.atlassian.com/display/JIRADEV/Preparing+for+JIRA+7.2)**
    -   [JIRA Core 7.2 EAP 01 (m01) release notes](https://developer.atlassian.com/display/JIRADEV/JIRA+Core+7.2+EAP+01+%28m01%29+release+notes)
    -   [JIRA Software 7.2 EAP 01 (m01) release notes](https://developer.atlassian.com/display/JIRADEV/JIRA+Software+7.2+EAP+01+%28m01%29+release+notes)
    -   [JIRA Core 7.2 Release Candidate (RC01) release notes](https://developer.atlassian.com/display/JIRADEV/JIRA+Core+7.2+Release+Candidate+%28RC01%29+release+notes)
    -   [JIRA Software 7.2 Release Candidate (RC01) release notes](https://developer.atlassian.com/display/JIRADEV/JIRA+Software+7.2+Release+Candidate+%28RC01%29+release+notes)
    -   [JIRA Service Desk 3.2 Release Candidate (RC01) release notes](https://developer.atlassian.com/display/JIRADEV/JIRA+Service+Desk+3.2+Release+Candidate+%28RC01%29+release+notes)
    
    -  Related pages:
        -   [AUI 5.7 to 5.9 upgrade](https://developer.atlassian.com/display/JIRADEV/AUI+5.7+to+5.9+upgrade)
        -   [Deprecating JUnit3 test framework](https://developer.atlassian.com/display/JIRADEV/Deprecating+JUnit3+test+framework)
<br><br>

-   **JIRA platform version 7.1: [Preparing for JIRA 7.1](https://developer.atlassian.com/display/JIRADEV/Preparing+for+JIRA+7.1)**
    -   [JIRA Core 7.1 EAP 01 (m01) Release Notes](https://developer.atlassian.com/display/JIRADEV/JIRA+Core+7.1+EAP+01+%28m01%29+Release+Notes)
    -   [JIRA Software 7.1 EAP 01 (m02) Release Note](https://developer.atlassian.com/display/JIRADEV/JIRA+Software+7.1+EAP+01+%28m02%29+Release+Notes)
<br><br>
-   **JIRA platform version 7.0: [Preparing for JIRA 7.0](https://developer.atlassian.com/display/JIRADEV/Preparing+for+JIRA+7.0)**
    -   [JIRA 7.0 EAP 01 (m01b) Release Notes](https://developer.atlassian.com/display/JIRADEV/JIRA+7.0+EAP+01+%28m01b%29+Release+Notes)
    -   [JIRA 7.0 EAP 02 (m03) Release Notes](https://developer.atlassian.com/display/JIRADEV/JIRA+7.0+EAP+02+%28m03%29+Release+Notes)
    -   [JIRA 7.0 RC3 Release Notes](https://developer.atlassian.com/display/JIRADEV/JIRA+7.0+RC3+Release+Notes)
    
    -   Related pages:
        -   [JIRA 7.0 - API changes](https://developer.atlassian.com/display/JIRADEV/JIRA+7.0+-+API+changes)
        -   [JIRA 7.0 - Platform changes](https://developer.atlassian.com/display/JIRADEV/JIRA+7.0+-+Platform+changes)
        -   [JIRA 7.0 - General changes](https://developer.atlassian.com/display/JIRADEV/JIRA+7.0+-+General+changes)
        -   [SOAP and XML-RPC API Deprecation Notice](/server/jira/platform/soap-and-xml-rpc-api-deprecation-notice)
        -   [JIRA SOAP to REST Migration Guide](/server/jira/platform/jira-soap-to-rest-migration-guide)
<br><br>
-   **JIRA platform version 6.4: [Preparing for JIRA 6.4](https://developer.atlassian.com/display/JIRADEV/Preparing+for+JIRA+6.4)**  

    -   <a href="https://confluence.atlassian.com/jira/jira-6-4-eap-14-m14-712409471.html" class="external-link">JIRA 6.4 EAP 14 (m14)</a>
    -   <a href="https://confluence.atlassian.com/jira/jira-6-4-eap-13-m13-700088986.html" class="external-link">JIRA 6.4 EAP 13 (m13)</a>
    -   <a href="https://confluence.atlassian.com/jira/jira-6-4-eap-12-m12-687025131.html" class="external-link">JIRA 6.4 EAP 12 (m12)</a>
    -   <a href="https://confluence.atlassian.com/jira/jira-6-4-eap-9-m09-687014822.html" class="external-link">JIRA 6.4 EAP 9 (m09)</a>
    <br><br>
-   **JIRA platform version 6.3: [Preparing for JIRA 6.3](https://developer.atlassian.com/display/JIRADEV/Preparing+for+JIRA+6.3)**
    -   <a href="https://confluence.atlassian.com/jira063/jira-6-3-eap-1-m01-release-notes-683542177.html" class="external-link">JIRA 6.3 EAP 1 (m01) Release Notes</a>
    -   <a href="https://confluence.atlassian.com/jira063/jira-6-3-eap-2-m02-release-notes-683542176.html" class="external-link">JIRA 6.3 EAP 2 (m02) Release Notes</a>
    -   <a href="https://confluence.atlassian.com/jira063/jira-6-3-eap-3-m03-release-notes-683542178.html" class="external-link">JIRA 6.3 EAP 3 (m03) Release Notes</a>
    -   <a href="https://confluence.atlassian.com/jira063/jira-6-3-eap-4-m04-release-notes-683542181.html" class="external-link">JIRA 6.3 EAP 4 (m04) Release Notes</a>
    -   <a href="https://confluence.atlassian.com/jira063/jira-6-3-eap-5-m05-release-notes-683542182.html" class="external-link">JIRA 6.3 EAP 5 (m05) Release Notes</a>
    -   <a href="https://confluence.atlassian.com/jira063/jira-6-3-eap-6-m06-1-release-notes-683542179.html" class="external-link">JIRA 6.3 EAP 6 (m06-1) Release Notes</a>
    -   <a href="https://confluence.atlassian.com/jira063/jira-6-3-eap-7-m07-release-notes-683542180.html" class="external-link">JIRA 6.3 EAP 7 (m07) Release Notes</a>
    
    -   Related pages:
        -   [Inline create for JIRA issues](https://developer.atlassian.com/display/JIRADEV/Inline+create+for+JIRA+issues)
<br><br>
-   **JIRA platform version 6.2: [Preparing for JIRA 6.2](https://developer.atlassian.com/display/JIRADEV/Preparing+for+JIRA+6.2)**

    -   <a href="https://confluence.atlassian.com/jira064/jira-6-2-eap-1-m01-release-notes-720414249.html" class="external-link">JIRA 6.2 EAP 1 (m01) Release Notes</a>
    -   <a href="https://confluence.atlassian.com/jira064/jira-6-2-eap-3-m03-release-notes-720414264.html" class="external-link">JIRA 6.2 EAP 3 (m03) Release Notes</a>
    -   <a href="https://confluence.atlassian.com/jira064/jira-6-2-eap-4-m04-release-notes-720414302.html" class="external-link">JIRA 6.2 EAP 4 (m04) Release Notes</a>
    -   <a href="https://confluence.atlassian.com/jira064/jira-6-2-eap-5-m05-release-notes-720414280.html" class="external-link">JIRA 6.2 EAP 5 (m05) Release Notes</a>
    -   <a href="https://confluence.atlassian.com/jira064/jira-6-2-eap-6-m06-release-notes-720414376.html" class="external-link">JIRA 6.2 EAP 6 (m06) Release Notes</a>
    -   <a href="https://confluence.atlassian.com/jira064/jira-6-2-eap-7-m07-release-notes-720414399.html" class="external-link">JIRA 6.2 EAP 7 (m07) Release Notes</a>
    -   <a href="https://confluence.atlassian.com/jira064/jira-6-2-beta-1-release-notes-720414350.html" class="external-link">JIRA 6.2 Beta 1 Release Notes</a>
    -   <a href="https://confluence.atlassian.com/jira064/jira-6-2-rc-1-release-notes-720414324.html" class="external-link">JIRA 6.2 RC 1 Release Notes</a>
    
    -   Related pages:

        -   [JIRA issue statuses as lozenges](https://developer.atlassian.com/display/JIRADEV/JIRA+issue+statuses+as+lozenges)
<br><br>
-   **JIRA platform version 6.1: [Preparing for JIRA 6.1](https://developer.atlassian.com/display/JIRADEV/Preparing+for+JIRA+6.1)**
    -   <a href="https://confluence.atlassian.com/jira/jira-6-1-rc-2-release-notes-399999985.html" class="external-link">JIRA 6.1 RC 2 Release Notes</a>
    -   <a href="https://confluence.atlassian.com/jira/jira-6-1-rc-1-release-notes-395706876.html" class="external-link">JIRA 6.1 RC 1 Release Notes</a>
    -   <a href="https://confluence.atlassian.com/jira/jira-6-1-beta-1-release-notes-391088280.html" class="external-link">JIRA 6.1 Beta 1 Release Notes</a>
    -   <a href="https://confluence.atlassian.com/jira/jira-6-1-eap-5-m05-release-notes-389284592.html" class="external-link">JIRA 6.1 EAP 5 (m05) Release Notes</a>
    -   <a href="https://confluence.atlassian.com/jira/jira-6-1-eap-4-m04-release-notes-385321629.html" class="external-link">JIRA 6.1 EAP 4 (m04) Release Notes</a>
    -   <a href="https://confluence.atlassian.com/jira/jira-6-1-eap-3-m03-release-notes-376836130.html" class="external-link">JIRA 6.1 EAP 3 (m03) Release Notes</a>
    -   <a href="https://confluence.atlassian.com/jira/jira-6-1-eap-2-m02-release-notes-376209475.html" class="external-link">JIRA 6.1 EAP 2 (m02) Release Notes</a>
    -   <a href="https://confluence.atlassian.com/jira/jira-6-1-eap-1-m01-release-notes-365003477.html" class="external-link">JIRA 6.1 EAP 1 (m01) Release Notes</a>
    
    -   Related pages:
        -   [JIRA email template changes](https://developer.atlassian.com/display/JIRADEV/JIRA+email+template+changes)
<br><br>
-   **JIRA platform version 6.0: [Preparing for JIRA 6.0](https://developer.atlassian.com/display/JIRADEV/Preparing+for+JIRA+6.0)**

    -   <a href="https://confluence.atlassian.com/jira064/jira-6-0-rc-1-release-notes-720414836.html" class="external-link">JIRA 6.0 RC 1 Release Notes</a>
    -   <a href="https://confluence.atlassian.com/jira064/jira-6-0-beta-1-release-notes-720414858.html" class="external-link">JIRA 6.0 Beta 1 Release Notes</a>
    -   <a href="https://confluence.atlassian.com/jira064/jira-6-0-eap-8-m10-release-notes-720414821.html" class="external-link">JIRA 6.0 EAP 8 (m10) Release Notes</a>
    -   <a href="https://confluence.atlassian.com/jira064/jira-6-0-eap-7-m09-release-notes-720414809.html" class="external-link">JIRA 6.0 EAP 7 (m09) Release Notes</a>
    -   <a href="https://confluence.atlassian.com/jira064/jira-6-0-eap-6-m08-release-notes-720414889.html" class="external-link">JIRA 6.0 EAP 6 (m08) Release Notes</a>
    -   <a href="https://confluence.atlassian.com/jira064/jira-6-0-eap-5-m07-release-notes-720414802.html" class="external-link">JIRA 6.0 EAP 5 (m07) Release Notes</a>
    -   <a href="https://confluence.atlassian.com/jira064/jira-6-0-eap-4-m06-release-notes-720414787.html" class="external-link">JIRA 6.0 EAP 4 (m06) Release Notes</a>
    -   <a href="https://confluence.atlassian.com/jira064/jira-6-0-eap-3-m05-release-notes-720414795.html" class="external-link">JIRA 6.0 EAP 3 (m05) Release Notes</a>
    -   <a href="https://confluence.atlassian.com/jira064/jira-6-0-eap-2-m04-release-notes-720414883.html" class="external-link">JIRA 6.0 EAP 2 (m04) Release Notes</a>
    -   <a href="https://confluence.atlassian.com/jira064/jira-6-0-eap-1-m02-release-notes-720414880.html" class="external-link">JIRA 6.0 EAP 1 (m02) Release Notes</a>
    
    -   Related pages:
        -   [ADG Migration from 5.x to 6.0](https://developer.atlassian.com/display/JIRADEV/ADG+Migration+from+5.x+to+6.0)
        -   [JIRA Agile ADG Migration Guidelines](https://developer.atlassian.com/display/JIRADEV/JIRA+Agile+ADG+Migration+Guidelines)
        -   [Using Soy Templates](https://developer.atlassian.com/display/JIRADEV/Using+Soy+Templates)
        -   [Building UI for an app shipping to multiple versions of JIRA](https://developer.atlassian.com/display/JIRADEV/Building+UI+for+a+plugin+shipping+to+multiple+versions+of+JIRA)
        -   [Plugins 3 Migration Guide](https://developer.atlassian.com/display/JIRADEV/Plugins+3+Migration+Guide)
        -   [Renamable Users in JIRA 6.0](https://developer.atlassian.com/display/JIRADEV/Renamable+Users+in+JIRA+6.0)
<br><br>
-   **JIRA platform version 5.2: [Preparing for JIRA 5.2](https://developer.atlassian.com/display/JIRADEV/Preparing+for+JIRA+5.2)**
    -   <a href="https://confluence.atlassian.com/jira064/jira-5-2-eap-1-m3-release-notes-720415544.html" class="external-link">JIRA 5.2 EAP 1 (m3) Release Notes</a>
    -   <a href="https://confluence.atlassian.com/jira064/jira-5-2-eap-2-m4-release-notes-720415566.html" class="external-link">JIRA 5.2 EAP 2 (m4) Release Notes</a>
    -   <a href="https://confluence.atlassian.com/jira064/jira-5-2-eap-3-m5-release-notes-720415598.html" class="external-link">JIRA 5.2 EAP 3 (m5) Release Notes</a>
    -   <a href="https://confluence.atlassian.com/jira064/jira-5-2-eap-4-m6-release-notes-720415469.html" class="external-link">JIRA 5.2 EAP 4 (m6) Release Notes</a>
    -   <a href="https://confluence.atlassian.com/jira064/jira-5-2-rc-1-m8-release-notes-720415630.html" class="external-link">JIRA 5.2 RC 1 (m8) Release Notes</a>
    -   <a href="https://confluence.atlassian.com/jira064/jira-5-2-rc4-m11-release-notes-720415504.html" class="external-link">JIRA 5.2 RC4 (m11) Release Notes</a>
<br><br>    
-   **JIRA platform version 5.1: [Preparing for JIRA 5.1](https://developer.atlassian.com/display/JIRADEV/Preparing+for+JIRA+5.1)**
    -   <a href="https://confluence.atlassian.com/jira064/jira-5-1-rc-3-release-notes-720414556.html" class="external-link">JIRA 5.1 RC 3 Release Notes</a>
    -   <a href="https://confluence.atlassian.com/jira064/jira-5-1-rc-2-release-notes-720414614.html" class="external-link">JIRA 5.1 RC 2 Release Notes</a>
    -   <a href="https://confluence.atlassian.com/jira064/jira-5-1-rc-1-release-notes-720414576.html" class="external-link">JIRA 5.1 RC 1 Release Notes</a>
    -   <a href="https://confluence.atlassian.com/jira064/jira-5-1-beta-1-release-notes-720414596.html" class="external-link">JIRA 5.1 Beta 1 Release Notes</a>
    -   <a href="https://confluence.atlassian.com/jira064/jira-5-1-eap-2-release-notes-720414634.html" class="external-link">JIRA 5.1 EAP 2 Release Notes</a>
    -   <a href="https://confluence.atlassian.com/jira064/jira-5-1-eap-1-release-notes-720414537.html" class="external-link">JIRA 5.1 EAP 1 Release Notes</a>
    
    -   Related pages:
        -   [Working with Inline Edit in JIRA 5.1](https://developer.atlassian.com/display/JIRADEV/Working+with+Inline+Edit+in+JIRA+5.1)
<br><br>
-   **JIRA platform version 5.0: [Preparing for JIRA 5.0](https://developer.atlassian.com/display/JIRADEV/Preparing+for+JIRA+5.0)**

    -   <a href="https://confluence.atlassian.com/jira064/jira-5-0-rc-3-release-notes-720415140.html" class="external-link">JIRA 5.0 RC 3 Release Notes</a>
    -   <a href="https://confluence.atlassian.com/jira064/jira-5-0-rc-2-release-notes-720415124.html" class="external-link">JIRA 5.0 RC 2 Release Notes</a>
    -   <a href="https://confluence.atlassian.com/jira064/jira-5-0-rc-1-release-notes-720415108.html" class="external-link">JIRA 5.0 RC 1 Release Notes</a>
    -   <a href="https://confluence.atlassian.com/jira064/jira-5-0-beta-3-release-notes-720415093.html" class="external-link">JIRA 5.0 Beta 3 Release Notes</a>
    -   <a href="https://confluence.atlassian.com/jira064/jira-5-0-beta-2-release-notes-720415160.html" class="external-link">JIRA 5.0 Beta 2 Release Notes</a>
    -   <a href="https://confluence.atlassian.com/jira064/jira-5-0-beta-1-release-notes-720415175.html" class="external-link">JIRA 5.0 Beta 1 Release Notes</a>
    -   <a href="https://confluence.atlassian.com/jira064/jira-5-0-eap-5-release-notes-720415085.html" class="external-link">JIRA 5.0 EAP 5 Release Notes</a>
    -   <a href="https://confluence.atlassian.com/jira064/jira-5-0-eap-4-release-notes-720415187.html" class="external-link">JIRA 5.0 EAP 4 Release Notes</a>
    -   <a href="https://confluence.atlassian.com/jira064/jira-5-0-eap-3-release-notes-720415078.html" class="external-link">JIRA 5.0 EAP 3 Release Notes</a>
    -   <a href="https://confluence.atlassian.com/jira064/jira-5-0-eap-2-release-notes-720415074.html" class="external-link">JIRA 5.0 EAP 2 Release Notes</a>

    -   Related pages:
        -   [JIRA REST API in JIRA 5.0](https://developer.atlassian.com/display/JIRADEV/JIRA+REST+API+in+JIRA+5.0)
        -   [Java API Changes in JIRA 5.0](https://developer.atlassian.com/display/JIRADEV/Java+API+Changes+in+JIRA+5.0)
        -   [Changes to MessageHandler and message processing services](https://developer.atlassian.com/display/JIRADEV/Changes+to+MessageHandler+and+message+processing+services)
        -   [Changes to markup, CSS and Decorators in JIRA 5.0](https://developer.atlassian.com/display/JIRADEV/Changes+to+markup%2C+CSS+and+Decorators+in+JIRA+5.0)
        -   [Custom Fields that use CSS or JavaScript Web Resources in JIRA 5.0](https://developer.atlassian.com/display/JIRADEV/Custom+Fields+that+use+CSS+or+JavaScript+Web+Resources+in+JIRA+5.0)
        -   [Loading Issue Tab Panels with AJAX](https://developer.atlassian.com/display/JIRADEV/Loading+Issue+Tab+Panels+with+AJAX)
        -   [Activity Streams Demos](https://developer.atlassian.com/display/JIRADEV/Activity+Streams+Demos)
        -   [How to migrate an app from JIRA 4.x to JIRA 5.0](https://developer.atlassian.com/display/JIRADEV/How+to+migrate+a+plugin+from+JIRA+4.x+to+JIRA+5.0)
