---
aliases:
- /server/jira/platform/working-with-custom-fields-4227109.html
- /server/jira/platform/working-with-custom-fields-4227109.md
category: devguide
confluence_id: 4227109
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=4227109
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=4227109
date: '2017-12-08'
legacy_title: Working with Custom Fields
platform: server
product: jira
subcategory: other
title: Working with Custom Fields
---
# Working with Custom Fields

{{% warning %}}

This page is restricted due to critical Errors

This page is restricted to Atlassian staff due to critical errors. Please do not unrestrict this page while <a href="https://ecosystem.atlassian.net/browse/DEVREL-586" class="uri external-link">https://ecosystem.atlassian.net/browse/DEVREL-586</a> remains open.

{{% /warning %}}

It is fairly straight forward to programmatically read or update a value of a custom field in JIRA.

Here is a code snapshot that reads a value and then updates it:

``` javascript
CustomFieldManager customFieldManager = ComponentManager.getInstance().getFieldManager().getCustomFieldManager();
CustomField customField = customFieldManager.getCustomFieldObject(new Long(10040));

// Obtain the FieldLayoutItem associated with the custom field for this issue
FieldLayoutItem fieldLayoutItem = ComponentManager.getInstance().getFieldLayoutManager().getFieldLayout(issue).getFieldLayoutItem(customField);

// Read a value
Object value = customField.getValue(issue);
System.out.println("Custom Field Value: " + value);

// Create a modified value object with the old value and the new value
ModifiedValue modifiedValue = new ModifiedValue(value, "Option 2");

// Update the value
customField.updateValue(fieldLayoutItem, issue, modifiedValue, new DefaultIssueChangeHolder());
// Show updated value
System.out.println("Custom Field Value: " + customField.getValue(issue));
```

Please note the type of the `value` object depends on the custom field in question. For text and select list custom fields this is a simple String (as you can see above). For a Multi-Select Custom Field it is a `java.util.List` of Strings.

Note that the CustomField object was retrieved using its id, which is unique for each custom field. The easiest way to determine the id of a custom field is to navigate to the View Custom Fields page in the Administration section and hover over one of the "Operation" links next to the custom field. The id will be one of the URL parameters in the link.

Here is another example which reads the values of a Cascading Select List custom field:

``` javascript
CustomFieldManager customFieldManager = ManagerFactory.getCustomFieldManager();
CustomField customField = customFieldManager.getCustomFieldObject(new Long(10010));
CustomFieldParams cfParams = (CustomFieldParams) customField.getValue(issue);

// Get the 1st level value
Collection value = (List) cfParams.getValuesForKey(null);
Option option = (Option) value.iterator().next();
System.out.println("1st level value = " + option.getValue());

// Get the 2nd level value
value = (List) cfParams.getValuesForKey("1");
option = (Option) value.iterator().next();
System.out.println("2nd level value = " + option.getValue());
```











































