---
aliases:
- /server/jira/platform/jira-software-7.5-eap-release-notes-51913987.html
- /server/jira/platform/jira-software-7.5-eap-release-notes-51913987.md
category: devguide
confluence_id: 51913987
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=51913987
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=51913987
date: '2017-12-08'
legacy_title: JIRA Software 7.5 EAP release notes
platform: server
product: jira
subcategory: updates
title: JIRA Software 7.5 EAP release notes
---
# JIRA Software 7.5 EAP release notes

**9 August 2017**

Atlassian is proud to present **JIRA Software 7.5 EAP**. This public development release is part of our [Early Access Program (EAP)](/server/jira/platform/jira-eap-releases-35160569.html) leading up to the official **JIRA Software 7.5** release. We are making these EAP and Release Candidate milestones publicly available so that developers can start assessing the impact of the changes that we are making.

For the full list of changes for JIRA 7.5, read the developer change management guide: [Preparing for JIRA 7.5](/server/jira/platform/preparing-for-jira-7-5). If you are new to JIRA, you should also read our [Java API Policy for JIRA](/server/jira/platform/java-api-policy-for-jira-4227213.html) and the [Atlassian REST API policy](https://developer.atlassian.com/display/HOME/Atlassian+REST+API+policy).

<a href="https://www.atlassian.com/software/jira/download-eap" class="external-link"><img src="/server/jira/platform/images/download-eap-btn.png" class="image-center confluence-thumbnail" width="120" /></a>

{{% warning %}}

Before you install/upgrade: Atlassian does not support upgrades both 'from' and 'to' EAP releases. EAP releases should not be used in production environments as they are not officially supported. For all production use and testing of JIRA, please use the <a href="https://confluence.atlassian.com/display/JIRASOFTWARE/JIRA+Software+7.0.x+release+notes" class="external-link">latest official release</a> instead.

{{% /warning %}}

## Kanban backlog, the Scrum way

With JIRA 7.5, we're introducing two new panels in the Kanban backlog to keep your epics and versions in the right place. The epics and versions panels will help you refine your backlog, group your issues, and easily manage them. Instead of going through the whole list of issues (and we know these might get huge for some of you), you'll be able to quickly select an epic and see the list of issues assigned to it, and reassign them if needed. All unassigned issues will live in a separate category, and you'll easily find those that got lost somewhere.

To see the new panels, go to your Kanban backlog, and then expand or hide the panels by using the vertically-aligned 'EPICS' and 'VERSIONS' buttons.

<img src="/server/jira/platform/images/epicsversions.png" height="400" />

You can create new epics/versions, and drag and drop your issues onto them to make associations. Wrong epic? No worries, just drag your issue to another one, it'll be reassigned. When displayed in panels, epics and versions work like filters - each selected one will show you the associated issues, so you know exactly what still needs to be done.

This also changes how your epics are displayed in JIRA. Until now, you could see each epic as a card on the board, like with all other issues. From now on, epics will be moved to the panel and work there more as filters rather than separate entities. You'll still be able to find them in the Issues tab, but they won't be anywhere on your Kanban board. If you prefer the old way, your board admin can disable the epics panel in **Board settings &gt; Columns**.

## New events and webhooks

Long awaited, events for creating and deleting issue links have landed in JIRA. You can now use them while building your plugins, or to get informed whenever an issue link is created or deleted. The new events are <a href="https://docs.atlassian.com/jira/7.5.0-EAP01/com/atlassian/jira/event/issue/link/IssueLinkCreatedEvent.html" class="external-link">IssueLinkCreatedEvent</a> and <a href="https://docs.atlassian.com/jira/7.5.0-EAP01/com/atlassian/jira/event/issue/link/IssueLinkDeletedEvent.html" class="external-link">IssueLinkDeletedEvent</a>.

When creating webhooks, you can use the following events. The string in parentheses is the name of the webhookEvent in the response.

-   create (*issuelink\_created*)
-   deleted (*issuelink\_deleted*)

For more info about creating webhooks in JIRA, see [Webhooks](https://developer.atlassian.com/display/JIRADEV/Webhooks).

## Upgrades

We don't support upgrading to or from EAP releases, and you should never use an EAP release in production. If you do intend to upgrade an instance to an EAP release, you should review *all* applicable upgrades notes for the production releases, and upgrade in the specified order:

-   <a href="https://confluence.atlassian.com/display/JIRASOFTWARE/JIRA+Software+7.4.x+upgrade+notes" class="external-link">Upgrading to JIRA Software 7.4</a>
-   <a href="https://confluence.atlassian.com/display/JIRASOFTWARE/JIRA+Software+7.3.x+upgrade+notes" class="external-link">Upgrading to JIRA Software 7.3</a>
-   <a href="https://confluence.atlassian.com/display/JIRASOFTWARE/JIRA+Software+7.2.x+upgrade+notes" class="external-link">Upgrading to JIRA Software 7.2</a>
-   <a href="https://confluence.atlassian.com/display/JIRASOFTWARE/JIRA+Software+7.1.x+upgrade+notes" class="external-link">Upgrading to JIRA Software 7.1</a>
-   <a href="https://confluence.atlassian.com/migration/jira-7/server_jira_upgrade" class="external-link">Upgrading to JIRA Software 7.0</a>
