---
title: Available Permissions 4227135
aliases:
    - /server/jira/platform/available-permissions-4227135.html
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=4227135
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=4227135
confluence_id: 4227135
platform: server
product: jira
category: devguide
subcategory:
date: "2018-04-25"
---
# JIRA Developer Documentation : Available Permissions

For certain JIRA plugins (e.g. [Portlets](/server/jira/platform/portlet-plugin-module-deprecated-4227185.html)), you can specify permissions required to use the plugin (e.g. for portlets, these are the permissions required to add the portlet to your dashboard). The table below lists the available permissions and the <a href="http://docs.atlassian.com/software/jira/docs/api/latest/com/atlassian/jira/security/Permissions.html" class="external-link">defined constants</a> for these permissions.

<table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<thead>
<tr class="header">
<th><p>Key word</p></th>
<th><p>Corresponding JIRA Permission</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>admin</p></td>
<td><p>ADMINISTER</p></td>
</tr>
<tr class="even">
<td><p>use</p></td>
<td><p>USE</p></td>
</tr>
<tr class="odd">
<td><p>project</p></td>
<td><p>PROJECT_ADMIN</p></td>
</tr>
<tr class="even">
<td><p>browse</p></td>
<td><p>BROWSE</p></td>
</tr>
<tr class="odd">
<td>transition</td>
<td><p>TRANSITION_ISSUE</p></td>
</tr>
<tr class="even">
<td><p>create</p></td>
<td><p>CREATE_ISSUE</p></td>
</tr>
<tr class="odd">
<td><p>attach</p></td>
<td><p>CREATE_ATTACHMENT</p></td>
</tr>
<tr class="even">
<td><p>edit</p></td>
<td><p>EDIT_ISSUE</p></td>
</tr>
<tr class="odd">
<td><p>update</p></td>
<td><p>EDIT_ISSUE</p></td>
</tr>
<tr class="even">
<td><p>scheduleissue</p></td>
<td><p>SCHEDULE_ISSUE</p></td>
</tr>
<tr class="odd">
<td><p>assignable</p></td>
<td><p>ASSIGNABLE_USER</p></td>
</tr>
<tr class="even">
<td><p>assign</p></td>
<td><p>ASSIGN_ISSUE</p></td>
</tr>
<tr class="odd">
<td><p>resolve</p></td>
<td><p>RESOLVE_ISSUE</p></td>
</tr>
<tr class="even">
<td><p>comment</p></td>
<td><p>COMMENT_ISSUE</p></td>
</tr>
<tr class="odd">
<td><p>close</p></td>
<td><p>CLOSE_ISSUE</p></td>
</tr>
<tr class="even">
<td><p>work</p></td>
<td><p>WORK_ISSUE</p></td>
</tr>
<tr class="odd">
<td><p>link</p></td>
<td><p>LINK_ISSUE</p></td>
</tr>
<tr class="even">
<td><p>delete</p></td>
<td><p>DELETE_ISSUE</p></td>
</tr>
<tr class="odd">
<td><p>sharefilters</p></td>
<td><p>CREATE_SHARED_FILTER</p></td>
</tr>
<tr class="even">
<td><p>groupsubscriptions</p></td>
<td><p>MANAGE_GROUP_FILTER_SUBSCRIPTIONS</p></td>
</tr>
<tr class="odd">
<td><p>move</p></td>
<td><p>MOVE_ISSUE</p></td>
</tr>
<tr class="even">
<td><p>setsecurity</p></td>
<td><p>SET_ISSUE_SECURITY</p></td>
</tr>
<tr class="odd">
<td><p>pickusers</p></td>
<td><p>USER_PICKER</p></td>
</tr>
<tr class="even">
<td><p>viewversioncontrol</p></td>
<td><p>VIEW_VERSION_CONTROL</p></td>
</tr>
<tr class="odd">
<td><p>modifyreporter</p></td>
<td><p>MODIFY_REPORTER</p></td>
</tr>
<tr class="even">
<td><p>viewvotersandwatchers</p></td>
<td><p>VIEW_VOTERS_AND_WATCHERS</p></td>
</tr>
<tr class="odd">
<td><p>managewatcherlist</p></td>
<td><p>MANAGE_WATCHER_LIST</p></td>
</tr>
</tbody>
</table>

