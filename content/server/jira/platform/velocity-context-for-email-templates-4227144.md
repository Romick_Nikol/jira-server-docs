---
title: Velocity Context for Email Templates 4227144
aliases:
    - /server/jira/platform/velocity-context-for-email-templates-4227144.html
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=4227144
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=4227144
confluence_id: 4227144
platform: server
product: jira
category: devguide
subcategory:
date: "2018-04-25"
---
# JIRA Developer Documentation : Velocity Context for Email Templates

JIRA 3.6 introduced the TemplateContext and TemplateIssue objects into the Velocity context available for each email template.

The TemplateContext provides access to some general object references for use in the templates - e.g. Utilities,  Change Log details, etc.

The TemplateIssue provides access to the Issue object - further extended by helper methods for displaying information in rendered HTML format (i.e. environment, description and comments) and users who may not exist in the system. All issue related details can be retrieved from the TemplateIssue.

The following tables lists other objects that are also available within the context:

<table>
<colgroup>
<col style="width: 33%" />
<col style="width: 33%" />
<col style="width: 33%" />
</colgroup>
<thead>
<tr class="header">
<th><p>Velocity Variable</p></th>
<th><p>JIRA Object/Value</p></th>
<th><p>Description</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>userutils</p></td>
<td><p>com.atlassian.core.user.UserUtils</p></td>
<td><p>collection of utility methods for querying, creating, and removing users</p></td>
</tr>
<tr class="even">
<td><p>velocityhelper</p></td>
<td><p>com.atlassian.jira.util.JiraVelocityHelper</p></td>
<td><p>a collection of miscellaneous methods (e.g. urlencode, indentTextBlock, printChangelog</p></td>
</tr>
<tr class="odd">
<td><p>textutils</p></td>
<td><p>com.opensymphony.util.TextUtils</p></td>
<td><p>collection of utility functions for manipulating text</p></td>
</tr>
<tr class="even">
<td><p>constantsManager</p></td>
<td><p>com.atlassian.jira.config.ConstantsManager</p></td>
<td><p>JIRA's constants for priorities, resolutions, issue types, statuses</p></td>
</tr>
<tr class="odd">
<td><p>projectManager</p></td>
<td><p>com.atlassian.jira.project.ProjectManager</p></td>
<td><p>management of project entities (e.g. getProjectByName, getComponents, getCategories)</p></td>
</tr>
<tr class="even">
<td><p>customFieldManager</p></td>
<td><p>com.atlassian.jira.issue.CustomFieldManager</p></td>
<td><p>functions for working with custom fields</p></td>
</tr>
<tr class="odd">
<td><p>applicationProperties</p></td>
<td><p>com.atlassian.jira.config.properties.ApplicationProperties</p></td>
<td><p>access JIRA's application properties</p></td>
</tr>
<tr class="even">
<td><p>jirautils</p></td>
<td><p>com.atlassian.jira.util.JiraUtils</p></td>
<td><p>a handful of miscellaneous utility methods (e.g. isPublicMode, isSetup)</p></td>
</tr>
<tr class="odd">
<td><p>jirakeyutils</p></td>
<td><p>com.atlassian.jira.util.JiraKeyUtils</p></td>
<td><p>methods for working with project keys</p></td>
</tr>
<tr class="even">
<td><p><del>buildutils</del></p></td>
<td><p><del>com.atlassian.jira.util.BuildUtils</del></p></td>
<td><p><em>buildutils</em> is no longer available within the context. <em>build</em> variable should be used instead (see below)</p></td>
</tr>
<tr class="odd">
<td><p>build</p></td>
<td><p><a href="https://docs.atlassian.com/software/jira/docs/api/latest/com/atlassian/jira/util/BuildUtilsInfo.html" class="external-link">com.atlassian.jira.util.BuildUtilsInfo</a></p></td>
<td><p><strong>Since JIRA 6.1</strong> JIRA build and edition information</p></td>
</tr>
<tr class="even">
<td><p>jiraUserUtils</p></td>
<td><p>com.atlassian.jira.user.util.UserUtil</p></td>
<td><p>user level operations (e.g. getActiveUserCount, getDisplayableNameSafely, addUserToGroup)</p></td>
</tr>
<tr class="odd">
<td><p>mailPluginsHelper</p></td>
<td><p>com.atlassian.jira.mail.JiraMailPluginsHelperImpl</p></td>
<td><p>method for checking whether plugins are enabled</p></td>
</tr>
<tr class="even">
<td><p>userformat</p></td>
<td><p>com.atlassian.jira.plugin.profile.UserFormatManager</p></td>
<td><p>formatting user names</p></td>
</tr>
<tr class="odd">
<td><p>context</p></td>
<td><p><a href="http://docs.atlassian.com/software/jira/docs/api/latest/com/atlassian/jira/mail/TemplateContext.html" class="external-link">com.atlassian.jira.mail.TemplateContext</a></p></td>
<td><p>The Template Context object itself</p></td>
</tr>
<tr class="even">
<td><p>issue</p></td>
<td><p><a href="http://docs.atlassian.com/software/jira/docs/api/latest/com/atlassian/jira/mail/TemplateIssue.html" class="external-link">com.atlassian.jira.mail.TemplateIssue</a></p></td>
<td><p>An Issue object with extended helper methods for display - this is the issue for which the email notification event was triggered</p></td>
</tr>
<tr class="odd">
<td><p>params</p></td>
<td><p>java.util.Map</p></td>
<td><p>Parameters of the IssueEvent that triggered this email notification</p></td>
</tr>
<tr class="even">
<td><p>remoteUser</p></td>
<td><p>com.opensymphony.user.User</p></td>
<td><p>The user who has triggered this event</p></td>
</tr>
<tr class="odd">
<td><p>stringUtils</p></td>
<td><p>org.apache.commons.lang.StringUtils</p></td>
<td><p>Utility object providing formatting methods</p></td>
</tr>
<tr class="even">
<td><p>padSize</p></td>
<td><p>java.lang.Integer</p></td>
<td><p>Used in text emails for padding the display</p></td>
</tr>
<tr class="odd">
<td><p>timeSpentFieldId</p></td>
<td><p>java.lang.String</p></td>
<td><p>The Id of the TimeSpentField - used in changelog for work logged notifications</p></td>
</tr>
<tr class="even">
<td><p>attachments</p></td>
<td><p>Collection(org.ofbiz.core.entity.GenericValue)</p></td>
<td><p>Collection of Generic Values that represents the attachments associated with the issue</p></td>
</tr>
<tr class="odd">
<td><p>htmlComment</p></td>
<td><p>java.lang.String</p></td>
<td><p>The comment associated with this event as a rendered HTML formatted string. A basic string is returned if the comment cannot be rendered correctly - including links for any referenced JIRA issues.</p></td>
</tr>
<tr class="even">
<td><p>comment</p></td>
<td><p><a href="http://docs.atlassian.com/software/jira/docs/api/latest/com/atlassian/jira/issue/comments/Comment.html" class="external-link">com.atlassian.jira.issue.comments.Comment</a></p></td>
<td><p>The comment associated with this event. The <a href="http://docs.atlassian.com/software/jira/docs/api/latest/com/atlassian/jira/issue/comments/Comment.html#getBody()" class="external-link">body</a> of the comment will be the raw unprocessed comment as entered by the user (e.g. the raw wiki markup)</p></td>
</tr>
<tr class="odd">
<td><p>commentauthor</p></td>
<td><p>com.opensymphony.user.User</p></td>
<td><p>The author of the comment </p></td>
</tr>
<tr class="even">
<td><p>roleVisibilityLevel</p></td>
<td><p>java.lang.String</p></td>
<td><p>The comment's project role level (overwritten by worklog)</p></td>
</tr>
<tr class="odd">
<td><p>groupVisibilityLevel</p></td>
<td><p>java.lang.String</p></td>
<td><p>The comment's group level (overwritten by worklog)</p></td>
</tr>
<tr class="even">
<td><p>originalhtmlComment</p></td>
<td><p>java.lang.String</p></td>
<td><p> </p></td>
</tr>
<tr class="odd">
<td><p>originalcommentauthor</p></td>
<td><p>com.opensymphony.user.User</p></td>
<td><p> </p></td>
</tr>
<tr class="even">
<td><p>originalroleVisibilityLevel</p></td>
<td><p> </p></td>
<td><p>comment (overwritten by worklog)</p></td>
</tr>
<tr class="odd">
<td><p>originalgroupVisibilityLevel</p></td>
<td><p> </p></td>
<td><p>comment (overwritten by worklog)</p></td>
</tr>
<tr class="even">
<td><p>worklog</p></td>
<td><p>com.atlassian.jira.issue.worklog.Worklog</p></td>
<td><p> </p></td>
</tr>
<tr class="odd">
<td><p>timeSpentUpdated</p></td>
<td><p>java.lang.Boolean</p></td>
<td><p> </p></td>
</tr>
<tr class="even">
<td><p>startDateUpdated</p></td>
<td><p>java.lang.Boolean</p></td>
<td><p> </p></td>
</tr>
<tr class="odd">
<td><p>commentUpdated</p></td>
<td><p>java.lang.Boolean</p></td>
<td><p> </p></td>
</tr>
<tr class="even">
<td><p>visibilityUpdated</p></td>
<td><p>java.lang.Boolean</p></td>
<td><p> </p></td>
</tr>
<tr class="odd">
<td><p>originalworklog</p></td>
<td><p>com.atlassian.jira.issue.worklog.Worklog</p></td>
<td><p> </p></td>
</tr>
<tr class="even">
<td><p>changelog</p></td>
<td><p>org.ofbiz.core.entity.GenericValue</p></td>
<td><p>The Generic value representing the changelog associated with the change</p></td>
</tr>
<tr class="odd">
<td><p>changelogauthor</p></td>
<td><p>com.opensymphony.user.User</p></td>
<td><p>The change log author</p></td>
</tr>
<tr class="even">
<td><p>security</p></td>
<td><p>org.ofbiz.core.entity.GenericValue</p></td>
<td><p>The Generic value representing the security level, if any, associated with this issue</p></td>
</tr>
<tr class="odd">
<td><p>rendererManager</p></td>
<td><p><a href="http://docs.atlassian.com/software/jira/docs/api/latest/com/atlassian/jira/issue/RendererManager.html" class="external-link">com.atlassian.jira.issue.RendererManager</a></p></td>
<td><p>JIRA 3.13.3 onwards! allows use of renderers such as wiki renderer. (e.g. $rendererManager.getRenderedContent(&quot;atlassian-wiki-renderer&quot;, &quot;<strong>bold wiki</strong>&quot;, $issue.getIssueRenderContext()))</p></td>
</tr>
<tr class="even">
<td><p>recipient</p></td>
<td><p>com.opensymphony.user.User</p></td>
<td><p>The recipient of the email</p></td>
</tr>
<tr class="odd">
<td><p>i18n</p></td>
<td><p>com.atlassian.jira.utilI.I18nHelper</p></td>
<td><p>i18n translation helper for the current recipient</p></td>
</tr>
<tr class="even">
<td><p>dateformatter</p></td>
<td><p>com.atlassian.jira.web.util.OutlookDate</p></td>
<td><p>Date and time formatter for the current recipient's locale</p></td>
</tr>
<tr class="odd">
<td><p>eventTypeName</p></td>
<td><p>java.lang.String</p></td>
<td><p><em>Available from JIRA 4.1.</em> The localised event type name. (For example, for the Issue Assigned event, the key <code>mail.event.type.issueassigned.name</code> will be used.) If a translation is not available, the event type name will be used</p></td>
</tr>
<tr class="even">
<td>attachmentsManager</td>
<td>com.atlassian.jira.mail.util.MailAttachmentsManager</td>
<td>Available from JIRA 7.0.3. If this object is present, the mail attachments manager can be used to add attachments to an email and generate Content-IDs for those attachments.</td>
</tr>
</tbody>
</table>

























































































































































































































































