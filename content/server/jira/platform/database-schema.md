---
aliases:
- /server/jira/platform/database-schema-4227160.html
- /server/jira/platform/database-schema-4227160.md
category: reference
confluence_id: 4227160
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=4227160
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=4227160
date: '2018-06-05'
legacy_title: Database schema
platform: server
product: jira
subcategory: database
title: "Database schema"
---
# Database schema

## About the Jira database schema

The following PDFs show the database schemas for different Jira versions:

*   [Jira 7.9.2 schema.pdf](/server/jira/platform/attachments/jira-7-9-2-database-schema.pdf)
*   [Jira70\_schema.pdf](https://dac-lf.prod.atl-paas.net/server/jira/platform/attachments/4227160/45525621.pdf)
*   [Jira61\_db\_schema.pdf](https://dac-lf.prod.atl-paas.net/server/jira/platform/attachments/4227160/22184109.pdf)
*   [Jira\_512\_DBSchema.pdf](https://dac-lf.prod.atl-paas.net/server/jira/platform/attachments/4227160/12812303.pdf)

The database schema is also described in `WEB-INF/classes/entitydefs`/`entitymodel.xml` in the Jira web application.
The `entitymodel.xml` file has an XML definition of all Jira database tables, table columns, and their data type.
Some of the relationships between tables also appear in the file.

#### Generating Jira database schema information

To generate schema information for the Jira database, for example, like the previous PDFs, follow the instructions
below. You can generate schema information in PDF, TXT, and [DOT](http://www.graphviz.org/doc/info/lang.html) formats.
Note that if you want to generate the schema in PDF format, you need to have [Graphviz](http://www.graphviz.org/) installed.

1.  Download the attached app: 
    *  For Jira 5: [jira-schema-diagram-generator-plugin-1.0.jar](https://dac-lf.prod.atl-paas.net/server/jira/platform/attachments/4227160/12812304.jar)
    *  For Jira 6: [jira-schema-diagram-generator-plugin-1.0.1.jar](https://dac-lf.prod.atl-paas.net/server/jira/platform/attachments/4227160/28411024.jar)
    *  For Jira 7: [jira-schema-diagram-generator-plugin-1.1.0.jar](https://dac-lf.prod.atl-paas.net/server/jira/platform/attachments/4227160/45525638.jar)
1.  Install the app in your Jira instance by following the instructions on
[Managing Jira apps](https://confluence.atlassian.com/display/ADMINJIRASERVER/Managing+add-ons).
1.  Go to the Jira administration console.  
    1.  Go to **System** > **Troubleshooting and Support** > **Generate Schema Diagram**  
        [Keyboard shortcut](https://confluence.atlassian.com/display/JIRACORESERVER/Using+keyboard+shortcuts):
        **g** + **g**, and then start typing *generate*.
    1.  Enter the tables or columns to omit from the generated schema information (optional).
    1.  If you want to generate a PDF, enter the path to the Graphviz executable.
    1.  Click **Generate Schema**.
    1.  The Database Schema page will be displayed with links to the schema file in TXT, DOT, and PDF format.

## Entity Engine and working with the Jira database

Jira uses Entity Engine module of the [OfBiz](http://ofbiz.apache.org/) suite to communicate with the database.
To learn more, read the [online documentation](https://cwiki.apache.org/confluence/display/OFBIZ/Entity+Engine+Guide).

If you use Jira API, you will notice that a lot of code deals with `GenericValue` objects. The `GenericValue`
is an OfBiz Entity Engine object. Each `GenericValue` object represents a record in the database.

To get a value of a field from a `GenericValue`, you will need to use the relevant getter method for the field's type.

For example:

``` java
GenericValue project = ...
String name = project.getString("name");
Long id = project.getLong("id");
```

The list of valid fields for each entity can be obtained by looking for the entity's definition in the
`WEB-INF/classes/entitydefs/entitymodel.xml` file. For the above example, you need to look at the "Project" entity.

**Notes about working with the Jira database**

{{% warning %}}

Direct database queries are not recommended in Jira. Instead, we recommend adding or modifying data via 
[Jira's REST APIs](/server/jira/platform/rest-apis/).
Check the [Command Line Interface](https://marketplace.atlassian.com/apps/6398/jira-command-line-interface-cli?hosting=cloud&tab=overview)
for existing remote scripting tools. If you absolutely must modify data in your database via direct database queries,
always [back up](https://confluence.atlassian.com/display/ADMINJIRASERVER/Backing+up+data) your data before
performing any modification to the database.

{{% /warning %}}

For a great way to watch Jira database queries in action, try adding
[SQL Logging](/server/jira/platform/logging-jira-sql-queries-4227212.html).

Custom fields have their own set of tables. For details, see
[Database – Custom fields](https://developer.atlassian.com/display/JIRADEV/Database+-+Custom+fields).

### Work logs

Work log entries are kept in the `worklog` table. For instance, some worklogs in Jira
(from [JRA-10393](https://jira.atlassian.com/browse/JRASERVER-10393)) are stored in `worklog` table in the following way.

<table style="width:100%;">
<colgroup>
<col style="width: 9%" />
<col style="width: 9%" />
<col style="width: 9%" />
<col style="width: 9%" />
<col style="width: 9%" />
<col style="width: 9%" />
<col style="width: 9%" />
<col style="width: 9%" />
<col style="width: 9%" />
<col style="width: 9%" />
<col style="width: 9%" />
</colgroup>
<thead>
<tr class="header">
<th><p>id</p></th>
<th><p>issueid</p></th>
<th><p>author</p></th>
<th><p>grouplevel</p></th>
<th><p>rolelevel</p></th>
<th><p>worklogbody</p></th>
<th><p>created</p></th>
<th><p>updateauthor</p></th>
<th><p>updated</p></th>
<th><p>timeworked</p></th>
<th><p>startdate</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>83332</p></td>
<td><p>38315</p></td>
<td><p>mtokar</p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p>Implemented method to calculate number of active users + tests</p></td>
<td><p>2008-01-22 19:44:04.867-06</p></td>
<td><p>mtokar</p></td>
<td><p>2008-01-22 19:44:04.867-06</p></td>
<td><p>5400</p></td>
<td><p>2008-01-22 19:43:00-06</p></td>
</tr>
<tr class="even">
<td><p>83333</p></td>
<td><p>38315</p></td>
<td><p><a href="mailto:andreask@atlassian.com" class="external-link">andreask@atlassian.com</a></p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p>Implemented a method to check if the user limit of the license has been exceeded.</p></td>
<td><p>2008-01-22 21:33:18.23-06</p></td>
<td><p><a href="mailto:andreask@atlassian.com" class="external-link">andreask@atlassian.com</a></p></td>
<td><p>2008-01-22 21:33:18.23-06</p></td>
<td><p>7200</p></td>
<td><p>2008-01-22 21:31:00-06</p></td>
</tr>
<tr class="odd">
<td><p>83334</p></td>
<td><p>38315</p></td>
<td><p><a href="mailto:andreask@atlassian.com" class="external-link">andreask@atlassian.com</a></p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p>Added new license types</p></td>
<td><p>2008-01-22 23:49:27.794-06</p></td>
<td><p><a href="mailto:andreask@atlassian.com" class="external-link">andreask@atlassian.com</a></p></td>
<td><p>2008-01-22 23:51:06.029-06</p></td>
<td><p>7200</p></td>
<td><p>2008-01-22 23:48:00-06</p></td>
</tr>
<tr class="even">
<td>83335</td>
<td>38315</td>
<td><a href="mailto:andreask@atlassian.com" class="external-link">andreask@atlassian.com</a></td>
<td> </td>
<td> </td>
<td>Integrate new license types in JIRA.</td>
<td>2008-01-22 23:51:23.799-06</td>
<td><a href="mailto:andreask@atlassian.com" class="external-link">andreask@atlassian.com</a></td>
<td>200</td>
<td> </td>
<td> </td>
</tr>
</tbody>
</table>

![worklogs](/server/jira/platform/images/worklogs.png)

Where:

*   `issueid` maps to `jiraissue`.`id`
*   `timeworked` is in seconds

Whenever a worklog entry is added, the `jiraissue.timespent` and `jiraissue.timeestimate` values are incremented
and decremented respectively.

## Related topics

Some of the relationships between Jira's tables in the database are documented in the following sections:

* [Change history](/server/jira/platform/database-change-history/)
* [Custom fields](/server/jira/platform/database-custom-fields/)
* [Issue fields](/server/jira/platform/database-issue-fields/)
* [Issue status and workflow](/server/jira/platform/database-issue-status-and-workflow/)
* [User and Group tables](/server/jira/platform/database-user-and-group-tables/)
