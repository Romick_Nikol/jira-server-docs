---
aliases:
- /server/jira/platform/drafts-or-problem-pages-8945764.html
- /server/jira/platform/drafts-or-problem-pages-8945764.md
category: devguide
confluence_id: 8945764
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=8945764
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=8945764
date: '2017-12-08'
legacy_title: DRAFTS or Problem Pages
platform: server
product: jira
subcategory: other
title: DRAFTS or problem pages
---
# DRAFTS or problem pages

These are pages that are not in circulation because they are drafts or they contain critical problems that require corrections.

-   [How to create a new custom field type - C](/server/jira/platform/how-to-create-a-new-custom-field-type-c.snippet)
-   [How to create a new custom field type - B](/server/jira/platform/how-to-create-a-new-custom-field-type-b.snippet)
-   [Working with Custom Fields](/server/jira/platform/working-with-custom-fields.snippet)
-   [How to create a new custom field type - A](/server/jira/platform/how-to-create-a-new-custom-field-type-a.snippet)
