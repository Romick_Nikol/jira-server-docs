---
aliases:
- /server/jira/platform/message-handler-plugin-module-4227134.html
- /server/jira/platform/message-handler-plugin-module-4227134.md
category: reference
confluence_id: 4227134
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=4227134
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=4227134
date: '2017-12-08'
legacy_title: Message Handler Plugin Module
platform: server
product: jira
subcategory: modules
title: Message handler
---
# Message handler

<table>
<colgroup>
<col style="width: 20%" />
<col style="width: 80%" />
</colgroup>
<tbody>
<tr class="odd">
<td><p>Available:</p></td>
<td><p>JIRA 5.0 and later</p></td>
</tr>
</tbody>
</table>

In JIRA, email messages may be periodically fetched (for example, via POP or IMAP) and processed (for example, create a new issue or create a comment). You can add new ways of processing email messages by implementing the MessageHandler interface in JIRA (or subclassing one if its implementations).

## Purpose of this module type

This module type enables developers to build their own custom message handlers for JIRA. JIRA includes a number of prebuilt handlers, as described in <a href="/server/jira/platform/creating-a-custom-mail-handler-for-jira/" class="external-link">Creating Issues and Comments from Email</a>. Developers can create their own handlers using this plugin module, as shown in <a href="https://developer.atlassian.com/jiradev/jira-platform/guides/email/tutorial-custom-message-mail-handler-for-jira" class="createlink">Plugin Tutorial - Writing a Custom Message (Mail) Handler for JIRA</a>

Administrators can apply the custom message handler in **Administration** &gt; **System** &gt; **Incoming Mail** pages. A message handler plugin can add specialized configuration settings through the add-edit-url module attribute, which specifies the page equivalent to this one:

![](/server/jira/platform/images/mailhandlerconfig.png) 

## Configuration

The root element for the keyboard shortcut plugin module is `message-handler`. Its attributes are:

#### Attributes

<table>
<colgroup>
<col style="width: 20%" />
<col style="width: 80%" />
</colgroup>
<thead>
<tr class="header">
<th><p>Name</p></th>
<th><p>Description</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>key</p></td>
<td><p>The unique identifier of the plugin module. You refer to this key to use the resource from other contexts in your plugin, such as from the plugin Java code or JavaScript resources.</p>
<p> </p>
<pre><code>&lt;component-import
  key=&quot;appProps&quot;
  interface=&quot;com.atlassian.sal.api.ApplicationProperties&quot;/&gt;</code></pre>
<p> </p>
<p>In the example, <code>appProps</code> is the key for this particular module declaration, for <code>component-import</code>, in this case.</p>
<p><strong>Required: yes</strong></p></td>
</tr>
<tr class="even">
<td><p>i18n-name-key</p></td>
<td><p>The localisation key for the human-readable name of the plugin module.</p>
<p><strong>Required: -</strong></p></td>
</tr>
<tr class="odd">
<td><p>class</p></td>
<td><p>The Java class of the message handler module. The custom message handler class must implement the <code>com.atlassian.jira.service.util.handler.MessageHandler</code> interface, or extend a class that does.</p>
<p><strong>Required: -</strong></p></td>
</tr>
<tr class="even">
<td><p>add-edit-url</p></td>
<td><p>Resource that implements the configuration UI for the handler.</p>
<p><strong>Required: -</strong></p></td>
</tr>
<tr class="odd">
<td><p>weight</p></td>
<td><p>Numeric position of this handler in the handler selection list in the handler configuration page. Built-in handlers come with a weight of 1 to 5, with the lower numbers appearing first in the list.</p>
<p><strong>Required: -</strong></p></td>
</tr>
</tbody>
</table>

## Example

Here is an example of `message-handler` defined in [atlassian-plugin.xml](/server/framework/atlassian-sdk/configuring-the-plugin-descriptor/):

``` javascript
    <message-handler i18n-name-key="demohandler.name" key="demoHandler" class="com.example.plugins.tutorial.jira.mailhandlerdemo.DemoHandler" add-edit-url="/secure/admin/EditHandlerDetailsUsingParams!default.jspa" weight="0"/>
```

## A note about message handlers in JIRA 4.4 and earlier

In JIRA 4.4 and earlier, adding your own custom message handler usually required rebuilding JIRA from source or at least modification of the `popservice.xml` file.

Also the UI for configuring such handlers was very basic. All handler parameters had to be provided as a comma-delimited list of name-value pairs. There was no easy way to test configuration validity. See the documentation on [Message Handler Plugin Module](/server/jira/platform/message-handler) and <a href="http://confluence.atlassian.com/display/JIRA044/Services#Services-MessageHandlers" class="external-link">configuring services</a>.

For information about services in JIRA 4.4 and earlier, including MessageHandlers, links to API documentation and source of the system handlers, see <a href="http://confluence.atlassian.com/display/JIRA/Services" class="external-link">the JIRA product documentation</a>.

After writing your MessageHandler implementation, install it into JIRA as follow:

1.  Compile your handler and add it to the JIRA classpath (either as a JAR in `WEB-INF/lib` or as a class in `WEB-INF/classes`). JIRA has an `external-source` directory whose README describes a quick way to compile source.
2.  Add your handler to the `services/com/atlassian/jira/service/services/pop/popservice.xml`.
3.  Restart JIRA.

You should now be able to select your own `MessageHandler` when configuring a POP service.

## Related Topics

-   [Tutorial - Custom message (mail) handler for JIRA](/server/jira/platform/creating-a-custom-mail-handler-for-jira)
-   <a href="https://confluence.atlassian.com/display/JIRA/Creating+Issues+and+Comments+from+Email" class="external-link">Creating Issues and Comments from Email</a>
