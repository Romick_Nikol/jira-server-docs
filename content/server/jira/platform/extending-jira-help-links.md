---
aliases:
- /server/jira/platform/guide-jira-help-links-27564333.html
- /server/jira/platform/guide-jira-help-links-27564333.md
category: devguide
confluence_id: 27564333
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=27564333
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=27564333
date: '2017-12-08'
guides: guides
legacy_title: Guide - JIRA help links
platform: server
product: jira
subcategory: learning
title: Extending JIRA help links
---
# Extending JIRA help links

<table>
<colgroup>
<col style="width: 20%" />
<col style="width: 80%" />
</colgroup>
<tbody>
<tr class="odd">
<td><p>Available:</p></td>
<td><p>JIRA 6.2.5 and later.</p></td>
</tr>
</tbody>
</table>

This page describes how JIRA links to outside help resources. These methods allow JIRA to utilise the user's locale and JIRA version to decide what documentation to link to without having to hard code the URL. They also allow plugin writers to customise JIRA's help URLs, and generate their own help URLs.

## HelpUrls and HelpUrlsParser

In Java code, you can do something like this.

``` java
this.helpUrls.getHelpUrl("jira101").getUrl();
```

In a JavaServer Page (JSP), you can just use the Webwork tags to avoid having to add getters to your action.

``` xml
<a href="<ww:help-url key='jira101'/>" title="<ww:help-title key='jira101'/>">JIRA 101</a>
```

## JIRA's Core Help Links

{{% note %}}

This section is for plugin writers using or changing JIRA's core help URLs (which will point to JIRA's core help documentation). Plugin writers wanting to have their own independent help URLs should look at the [Usage within Plugins](#usage-within-plugins).

{{% /note %}}

To use JIRA's core HelpUrls, you simply need to inject the following code into your component, and you can start using it. 

``` java
@Inject
HelpUrls urls;
...
return urls.getUrl("jira101").getTitle();
```

The link's names, titles and URLs are pulled from resources provided by plugins. For example `system-helppaths-plugin.xml` defines a help resource of `help-paths.properties`:

**help-paths.properties**

``` java
# Prefixes
url-prefix=https://docs.atlassian.com/jira/docs-${docs.version}/
url-prefix.ondemand=https://confluence.atlassian.com/display/Cloud/

# Section: JIRA 101
jira101.url=JIRA 101
jira101.url.ondemand=Get+a+feel+for+JIRA
jira101.title=JIRA 101
jira101.title.ondemand=JIRA+Cloud
```

The properties ending in `.ondemand` are used to override the values that are used in stand-alone installations (hosted on your own server, and the variable `docs.version` corresponds to the JIRA version that is running. So when you ask `HelpUrls` for the `jira101 `you will get one of the following.

<table>
<thead>
<tr class="header">
<th>Where </th>
<th>URL</th>
<th>Title</th>
<th>Version of JIRA</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>Server</td>
<td><a href="https://docs.atlassian.com/jira/docs-062/Get+a+feel+for+JIRA">Get a feel for JIRA</a><br />
</td>
<td>JIRA 101</td>
<td>6.2.x</td>
</tr>
<tr class="even">
<td>Cloud</td>
<td><a href="https://confluence.atlassian.com/display/Cloud/JIRA+Cloud">JIRA Cloud</a></td>
<td>JIRA 101</td>
<td>Not applicable</td>
</tr>
</tbody>
</table>

It is possible for plugins to extend and adapt the help URLs used in core JIRA. For example:

**atlassian-plugin.xml**

``` xml
<atlassian-plugin key="example" name="Brenden's Example" plugins-version="2">
    <resource type="helpPaths" location="HelpPaths" name="help"/>
</atlassian-plugin>
```

**help-paths\_ja\_JP.properties**

``` java
# Prefixes
url-prefix=https://docs.atlassian.jp/jira/docs-${docs.version}/

# Section: JIRA 101
jira101.title=JIRAをはじめ
jira101.title.ondemand=JIRAのメーカーの紹介
```

When this plugin is installed, JIRA will generate the following Help URLs:

**User Locale** - Locale.ROOT

**BTF URL** - https://docs.atlassian.com/jira/docs-062/JIRA+101

**BTF Title** - Get online help about JIRA 101

**OD URL** - https://confluence.atlassian.com/display/AOD/JIRA+Studio+101

**OD Title** - Get online help about JIRA Studio 101

**User Locale** - Locale.ENGLISH 

**BTF URL** - https://docs.atlassian.com/jira/docs-062/JIRA+101

**BTF Title** - Get online help about JIRA 101 

**OD URL** - https://confluence.atlassian.com/display/AOD/JIRA+Studio+101

**OD Title** - Get online help about JIRA Studio 101

**User Locale** - Locale.JAPAN

**BTF URL** - https://docs.atlassian.jp/jira/docs-062/JIRA+101

**BTF Title** - JIRAをはじめ に関するオンライン ヘルプの参照

**OD URL** - https://confluence.atlassian.com/display/AOD/JIRA+Studio+101

**OD Title** - JIRAのメーカーの紹介 に関するオンライン ヘルプの参照

**Notes**

-   The `url-prefix` property will change the URL of every Help path in JIRA. This is useful for relocating documentation onto a different domains or onto an internal JIRA server.
-   The `HelpPaths.properties` files have to be encoded in ISO-8859-1 with non-latian characters encoded using `native2ascii` or some similar tool. This is a requirement of JIRA language packs.
-   The `url-prefix` will have `${docs.version}` or `${doc.version}` replaced with JIRA's current documentation version. This is the version that JIRA uses to reference its documentation (e.g. 6.2.1, 6.2.2, 6.2.x == 062; 6.3.1, 6.3.2, 6.3.x == 063).
-   Properties that end in `.ondemand` will only be visible when running in a JIRA Cloud instance.
-   The title of a URL is not rendered directly. It is passed through the `online.help.for.topic` translation.

## Usage within Plugins

{{% note %}}

This section is about plugins providing their own Help URLs independent of JIRA's core help URLs. See the [JIRA's Core Help Links](#jira-s-core-help-links) if you would like to use or customise JIRA's core help URLs.

{{% /note %}}

A plugin may wish to provide their own HelpUrls to their own documentation. The HelpUrlsParser takes a Properties instance and returns a HelpUrls. For example:

``` java
@Inject
HelpUrlsParser parser;
...
HelpUrls urls = parser.load(loadHelpProperties());
...
return urls.getUrl("servicedesk.intro").getTitle();
```

For plugins that are versioned independently of JIRA (JIRA Agile, ServiceDesk, etc) it makes sense to use your own instance of HelpUrls so that your help links point to the documentation for a particular version of your plugin. Here is an example properties file in such a plugin.

**sd-help-links.properties**

``` java
# Prefixes
url-prefix=https://docs.atlassian.com/servicedesk/docs-${servicedesk.docs.version}/
url-prefix.ondemand=https://confluence.atlassian.com/display/Cloud-SD/

# Section: Intro pages
servicedesk.intro.url=ServiceDesk+Intro
servicedesk.intro.title=Introduction to ServiceDesk
```

Notice the use of the `${serviceDesk.docs.version}` property, used to refer to the plugin version that is currently running. To make sure the property gets substituted with the correct plugin version at build time a bit of Maven gymnastics is required:

1. Configure the `docsversion-maven-plugin` in your pom.xml so that the property value gets set correctly.

**pom.xml**

``` xml
<plugin>
    <groupId>com.atlassian.maven.plugins</groupId>
    <artifactId>docsversion-maven-plugin</artifactId>
    <version>1.0</version>
    <executions>
        <execution>
            <id>set-docs-version-property</id>
            <phase>initialize</phase>
            <goals>
                <goal>set-property</goal>
            </goals>
            <configuration>
                <!-- name of property used in sd-help-links.properties -->
                <property>servicedesk.docs.version</property>
            </configuration>
        </execution>
    </executions>
</plugin>
```

2. Enable resource filtering to get Maven to interpolate your resource files by adding this to your `pom.xml` (you may want to include/exclude specific files. See the <a href="http://maven.apache.org/plugins/maven-resources-plugin/examples/filter.html" class="external-link">Maven documentation on filtering</a>). 

``` xml
<resource>
    <directory>src/main/resources</directory>
    <filtering>true</filtering>
</resource>
```

After this infrastructure is in place you can use your HelpUrls to create links that are versioned for your plugin. Using the above example would yield the following help links.

| Where  | URL                                                                 | Title             | Version        |
|--------|---------------------------------------------------------------------|-------------------|----------------|
| Server | [ServiceDesk Intro](https://docs.atlassian.com/servicedesk/docs-010/ServiceDesk+Intro)| ServiceDesk Intro | 1.x            |
| Cloud  | [ServiceDesk Intro](https://confluence.atlassian.com/display/Cloud-SD/ServiceDesk+Intro) | ServiceDesk Intro | Not applicable |

## HelpUtil

HelpUrls and HelpUrlsParser was introduced in JIRA 6.2.4. Before then the HelpUtil class provided similar functionality. For example:

-   HelpUtil.getInstance() or new HelpUtil(): Returns JIRA's core Help URLs.
-   new HelpUtil(Properties): Returns the Help URLs within the passed properties.

HelpUtil now delegates all functionality off to HelpUrls and HelpUrlsParser as appropriate.
