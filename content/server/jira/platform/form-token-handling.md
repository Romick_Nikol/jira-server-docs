---
aliases:
- /server/jira/platform/form-token-handling-4227125.html
- /server/jira/platform/form-token-handling-4227125.md
category: devguide
confluence_id: 4227125
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=4227125
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=4227125
date: '2017-12-08'
legacy_title: Form Token Handling
platform: server
product: jira
subcategory: security
title: Form token handling
---
# Form token handling

<table>
<colgroup>
<col style="width: 20%" />
<col style="width: 80%" />
</colgroup>
<tbody>
<tr class="odd">
<td><p>Available:</p></td>
<td><p>Jira 4.1 and later.</p></td>
</tr>
</tbody>
</table>

## Overview and Purpose

Jira 4.1 and later employs a token authentication mechanism which is utilised whenever Jira actions are performed either through link request or form submission. This provides Jira with a means to validate the origin and intent of the request, thus adding an additional level of security against XSRF (<a href="https://en.wikipedia.org/wiki/Cross-site_request_forgery" class="external-link">Cross-site request forgery</a>). While the core Jira product and its bundled plugins use this token handling mechanism by default, non-bundled plugins or those developed by third parties may not.

This document provides instructions to Jira plugin developers on how to incorporate this token handling mechanism into Jira plugins.

## Form Tokens

Jira 4.1 requires that WebWork actions possess tokens, which are then verified when the form is submitted back to the Jira server. This is an "**opt in**" mechanism, whereby actions must declare that they require a token to be present in the request.

## Instructions for Plugin Developers

The following subsections provide details on how to protect code against XSRF by implement form token handling into your Jira plugin.

Please be aware that once form token handling has been implemented into a Jira plugin:

-   Any functions that use screen scraping, such as the 'create sub-task' function in FishEye, will be broken.
-   REST API end points will not be affected *unless* they use form encoding.

#### Jira WebWork Actions

To enable xsrf token checking for a particular Action class

1.  Locate the method that is called by the action execution (by default this method is called `doExecute()`)
2.  Add the `@com.atlassian.jira.security.xsrf.RequiresXsrfCheck` annotation to this method

#### Providing the token in HTML Forms

The token is included by default when using a `jiraform`

The token can be included into your own JSPs that don't use jiraforms, by adding the following code:

``` xml
<webwork:component name="'atl_token'" value="/xsrfToken" template="hidden.jsp"/>
```

The following code can be added to Velocity Templates:

``` xml
<input type="hidden" name="atl_token" value="$atl_token" />
```

#### Providing the token in HTML links

You can do the following in JSPs:

``` java
MyAction.jspa?myParameter=true&atl_token=<webwork:property value="/xsrfToken"/>
```

or Velocity Templates:

``` java
MyAction.jspa?myParameter=true&atl_token=${atl_token}
```

#### Accessing the token programatically

To get hold of the current user's token, you will need to make the following call:

``` java
import com.atlassian.jira.security.xsrf.XsrfTokenGenerator;
XsrfTokenGenerator xsrfTokenGenerator = ComponentManager.getComponentInstanceOfType(XsrfTokenGenerator.class);
String token = xsrfTokenGenerator.generateToken(request);
```

## Scripting

Scripts that access Jira remotely may have trouble acquiring or returning a security token, or maintaining an HTTP session with the server. There is a way for scripts to opt out of token checking by providing the following HTTP header in the request:

``` bash
X-Atlassian-Token: no-check
```

##### RELATED TOPICS

[XSRF protection in Confluence](https://developer.atlassian.com/display/CONFDEV/Form+Token+Handling).

For more information, refer to the <a href="https://www.owasp.org/index.php/Main_Page" class="external-link">Open Web Application Security Project</a> page.
