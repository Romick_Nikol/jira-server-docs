---
title: JIRA 7.0 Eap Release Notes 38441056
aliases:
    - /server/jira/platform/jira-7.0-eap-release-notes-38441056.html
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=38441056
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=38441056
confluence_id: 38441056
platform: server
product: jira
category: devguide
subcategory:
date: "2018-04-25"
---
# JIRA Developer Documentation : JIRA 7.0 EAP release notes

Find the release notes for the JIRA 7.0 EAP milestones below. For more information about EAP releases, see [JIRA EAP Releases](/server/jira/platform/jira-eap-releases-35160569.html).

-   [JIRA 7.0 EAP 01 (m01b) Release Notes](/server/jira/platform/jira-7-0-eap-01-m01b-release-notes)
-   [JIRA 7.0 EAP 02 (m03) Release Notes](/server/jira/platform/jira-7-0-eap-02-m03-release-notes)
-   [JIRA 7.0 RC3 Release Notes](/server/jira/platform/jira-7-0-rc3-release-notes)
