---
aliases:
- /server/jira/platform/aui-5.7-to-5.9-upgrade-39995902.html
- /server/jira/platform/aui-5.7-to-5.9-upgrade-39995902.md
category: devguide
confluence_id: 39995902
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=39995902
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=39995902
date: '2017-12-08'
legacy_title: AUI 5.7 to 5.9 upgrade
platform: server
product: jira
subcategory: updates
title: AUI 5.7 to 5.9 upgrade
---
# AUI 5.7 to 5.9 upgrade

As part of JIRA 7.2.0, the AUI library was upgraded from version <a href="https://docs.atlassian.com/aui/5.7.31/" class="external-link">5.7.31</a> to <a href="https://docs.atlassian.com/aui/5.9.22/" class="external-link">5.9.22</a>.

{{% note %}}

This list does not include added/fixed functionality. See the full <a href="https://bitbucket.org/atlassian/aui/src/5.9.x/changelog.md" class="external-link">AUI Changelog</a>.

{{% /note %}}

### Removed

-   5.9.8 AUI was adding deprecated classes when `AJS.messages` was being called. This has since been removed. `AJS.messages.warning` will add `.aui-message-warning` only, not `.warning`.
-   `5.9.0 <aui-inline-dialog>`'s `isVisible()`, `show()`, and `hide()` have been removed. Use the `open` property instead.
-   `5.9.0 <aui-inline-dialog>`'s `aui-layer-show` and `aui-layer-hide` events have been removed. Use `aui-show` and `aui-hide` instead.
-   5.8.13 removed CSS vendor prefixes for `box-sizing` and `transition`

### Deprecated

-   5.9.0 All usage of named AMD modules. Use UMD source instead.
-   `5.9.0 AJS()`, `AJS.filterBySearch()`, `AJS.include()`, `AJS.setVisible()`, `AJS.setCurrent()`, and `AJS.isVisible()` have been deprecated. Use jQuery or native alternatives instead.
-   5.9.0 RESTful table helper methods (`AJS.triggerEvt()`, `AJS.bindEvt()`, and `AJS.triggerEvtForInst()`) have been deprecated and moved to RESTful table.
-   5.9.0 Accessing the jQuery element on form validation fields via `field.$el` has been deprecated. Use `field.el` instead, which contains the native DOM element.
-   5.9.0 `.aui-badge` class-based API for Badges has been deprecated. Use the `<aui-badge>` web component API instead.

### Changed

-   5.9.13 Upgraded Skate to 0.13.16. Skate no longer shares an element registry with older versions. This was causing problems with the lifecycles being initialised multiple times.
-   5.9.1 AUI datepicker internationalisation files have been changed from `.properties` files to `.js` files, for more modular consumption in plugins.
-   5.9.1 An ID given to an &lt;aui-select&gt; is no longer moved to the &lt;input&gt; of the expanded DOM. Instead it behaves in line with the &lt;aui-toggle&gt;, adding the given ID with a suffix '-input' to the &lt;input&gt; of the expanded DOM.
-   5.9.1 (perf) Removed fraction support for jQuery &lt; 1.5
-   5.9.1 (cosmetic) Min- and maxlength error messages for form validation are now more precise
-   5.9.1 (cosmetic) Form validation message adapts when minlength == maxlength
-   5.9.0 Refactored build from grunt to gulp.
-   5.9.0 Experimental components no longer need a `require()` call to load them.
-   `5.9.0 <aui-inline-dialog2>` was renamed to `<aui-inline-dialog>`.
-   `5.9.0 <aui-inline-dialog>` no longer needs a `require()` call to load it.
-   `5.9.0 <aui-inline-dialog>`'s `persistent` attribute is now a boolean attribute.
-   5.8.17 Converted date picker to use properties files instead of hardcoded values.
-   5.8.13 Backport of jQuery UI feature detection performance improvement.
-   5.8.13 "More" menu for responsive header is now lazily created to reduce work in onReady.
-   5.8.13 Removed base64-encoded data URIs for icons from sidebar CSS. Changed to reference actual icon files now.
-   5.8.12 Clicking on empty space in sidebar no longer collapses or expands it.
-   5.8.11 dropdown2's `.aui-dropdown2-checkbox` and `.aui-dropdown2-radio` have had their `isDisabled()` method replaced with `isEnabled()`.

## New AUI features overview

-   5.8.0 NEW COMPONENT: <a href="http://docs.atlassian.com/aui/5.9.17/single-select.html" class="external-link">AUI Single Select</a>.
-   5.8.0 Dropdown2 has been rewritten to improve accessibility. There is a new markup pattern, detailed in <a href="http://docs.atlassian.com/aui/5.9.17/dropdown2.html" class="external-link">AUI's Dropdown2</a> documentation. While the old markup pattern will work backwards compatibly, we encourage all developers to move to the new markup pattern to improve access for people using screen readers.
-   5.9.0 <a href="https://docs.atlassian.com/aui/5.9.17/docs/inline-dialog.html" class="external-link">Inline Dialog 2</a> was converted in to a web component named `<aui-inline-dialog>`.

## Upgrade overview

### Upgrade notes (AUI 5.7 to 5.8)

-   aui-ie9.css has been removed from the flatpack. It is no longer necessary to include this file.
-   If you are using the sidebar from the flatpack, you will now need to include aui-experimental.js.
-   The contents of an AUI Dropdown2 can now be created entirely using Soy (instead of sending html to aui.dropdown2.contents).
-   The markup generated using the AUI Dropdown2 Soy templates has changed. This new markup pattern is now much more accessible to screen readers.
-   Removed dependencies from AUI components to AUI soy templates (including responsive header).
-   AUI Sandbox has been removed, in the future we will be uploading examples to jsbin.
-   AUI Flag `persistent` option has been removed (deprecated in 5.7.7). Update all usages of this option to use `close` instead (see <a href="http://docs.atlassian.com/aui/5.8.0/flag.html" class="uri external-link">docs.atlassian.com/aui/5.8.0/flag.html</a>).
-   An <a href="http://requirejs.org/docs/whyamd.html#youcando" class="external-link">AMD loader</a> must be included in order to use aui-experimental.js now. 
    -   JIRA includes the <a href="https://github.com/requirejs/almond" class="external-link">Almond.js</a> AMD loader in its superbatch for you, so you should not need to add one yourself.

### Upgrade notes (AUI 5.8 to 5.9)

See the detailed notes at <a href="https://docs.atlassian.com/aui/latest/docs/upgrade-guide.html" class="uri external-link">docs.atlassian.com/aui/latest/docs/upgrade-guide.html</a>

## Other related upgrades

### Necessary compilation changes

You will need to compile against the latest version of JIRA in order to get updates to JIRA's page objects.

You will also need to ensure you use the latest testkit and atlassian-selenium versions.

Here are the minimum recommended versions of maven dependencies:

| GroupId                   | ArtifactId                     | Version                     |
|---------------------------|--------------------------------|-----------------------------|
| com.atlassian.browsers    | atlassian-browsers-auto        | 2.8.1                       |
| com.atlassian.jira        | atlassian-jira-pageobjects     | (Same as your JIRA version) |
| com.atlassian.jira        | jira-api                       | (Same as your JIRA version) |
| com.atlassian.jira        | jira-func-test-plugin          | (Same as your JIRA version) |
| com.atlassian.jira.tests  | jira-testkit-client            | 7.2.3                       |
| com.atlassian.jira.tests  | jira-testkit-plugin            | 7.2.3                       |
| com.atlassian.selenium    | atlassian-pageobjects-api      | 2.4.0                       |
| com.atlassian.selenium    | atlassian-pageobjects-elements | 2.4.0                       |
| com.atlassian.selenium    | atlassian-webdriver-core       | 2.4.0                       |
| com.google.code.gson      | gson                           | 2.3.1                       |
| com.google.guava          | guava                          | 18.0                        |
| org.apache.httpcomponents | httpclient                     | 4.4.1                       |
| org.apache.httpcomponents | httpcore                       | 4.4.1                       |

## Upgrade troubleshooting

### Problems related to AUI

#### An AUI element is appearing in a place I didn't expect it to

Consult the AUI upgrade guide: <a href="https://docs.atlassian.com/aui/latest/docs/upgrade-guide.html" class="uri external-link">docs.atlassian.com/aui/latest/docs/upgrade-guide.html</a>

Some important things to note:

-   AUI Inline Dialog2 was renamed and turned in to a custom element: &lt;aui-inline-dialog&gt;. **No "aui-" prefixed CSS classes** should be added to this custom element now!
-   Dropdown2 had an accessibility audit. The HTML pattern changed, and the **aria-owns** attribute needs to be changed to **aria-controls**.

#### My inline dialog element is rendering strangely

Ensure you are using the &lt;aui-inline-dialog&gt; web component, then:

-   Remove any "aui-" prefixed CSS classes from the element, particularly "aui-inline-dialog", "aui-inline-dialog2", and "aui-layer".
-   Remove any nested &lt;div class="aui-inline-dialog-contents"&gt; element from the component.

#### I get a cryptic javascript error of "cannot ready property 'persistent' of undefined"

This error can occur when either an AUI legacy Inline Dialog or dropdown is to be opened or closed.

Check that your &lt;aui-inline-dialog&gt; elements do NOT have a class of "aui-inline-dialog" added to them.

If the "aui-inline-dialog" CSS class is added to the element, AUI will treat the dialog like a legacy Inline Dialog component, whose API is incompatible with the new one.

### Problems related to Selenium

#### I get a "Cannot extend final class" error when compiling my tests

The Google gson library is too old, along with the httpcomponents libraries.

You need a minimum of:

-   httpcomponents.httpclient 4.4.1
-   httpcomponents.httpcore 4.4.1
-   com.google.code.gson gson 2.3.1

#### I get "UnreachableBrowserException" with "NumberFormatException" while executing JS code in Webdrivers

Once again, check your google gson version. Make sure it is &gt;= 2.3.

#### I get a "UniformClientException" with either a 404 or 503

Check further up your JIRA logs. You will probably see problems installing or initialising the TestKit and/or jira-func-test-plugin plugins.

You need to update the version of TestKit and the jira-func-test-plugin that gets installed with your integration tests.

Check your pom.xml and the configuration section for the maven-jira-plugin and ensure it is installing the latest versions.

#### Some elements do not appear within the visible section of the browser window

Firebug may be getting in the way of your webdriver tests. To remedy, you can add a nofirebug maven profile to your test suite:

``` xml
<dependency>
    <groupId>com.atlassian.browsers</groupId>
    <artifactId>firefox-profile</artifactId>
    <version>44.0-nofirebug</version>
</dependency>
```

### AUI + Selenium problems

#### Tests are failing with "Cannot click element ... is behind \[an aui blanket\]"

Occasionally, animations of dialogs opening or closing may get in the way of your tests.

To circumvent this, you can use the AUIBlanket\#waitUntilClosed helper from JIRA's page objects in your test body.

#### Tests are failing with "Cannot click element ... is behind \[something that looks like a flag or message\]"

If flags are in your way,

-   Use the AUIFlags\#closeAllFlags helper from JIRA's page objects in your test body.

Alternatively, if you do not care about any flags in your tests,

-   Add the ClearFlagsRule to the plugin's test runner (if you're compiling against JIRA QR20160519114447 or later)
-   Add the NPSRule to the plugin's test runner (if you're compiling against JIRA QR20160519114447 or later)

If your test class extends JIRA's BaseJiraWebTest class, or you use the JiraWebTestRules\#forJira method to pull in rules, you will get these rules enabled automatically.

#### I expected to see a flag, but it is not appearing in my test

Flags are disabled by default in JIRA webdriver tests (if you are using the base JIRA test rules).

To enable flags in your class or individual test, use the @EnableAUIFlags annotation.
