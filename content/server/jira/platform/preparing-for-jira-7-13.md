---
title: "Preparing for Jira 7.13"
product: jira
category: devguide
subcategory: updates
date: "2018-09-26"
---
# Preparing for Jira 7.13

This page covers changes in *Jira 7.13* that can affect add-on compatibility and functionality. This includes changes to the Jira platform and the Jira applications (Jira Core, Jira Software, Jira Service Desk).

As a rule, Atlassian makes every effort to inform add-on developers of known API changes. Where possible, we attach release targets. Unless otherwise indicated, every change on this page is expected to be released with the first public release of the Jira Server 7.13 products. We also make release milestones available prior to the release:

- For *Jira Server*, the Jira development team releases a number of EAP milestones prior to the final release, for customers and developers to keep abreast of upcoming changes. For more information on these releases, see [Jira EAP Releases](/server/jira/platform/).

- For *Jira Cloud*, the Jira team releases updates to the Jira Cloud products on a weekly basis. Add-ons that integrate with the Jira Cloud products via Atlassian Connect should use [Jira REST API](https://developer.atlassian.com/cloud/jira/platform/rest/).

We will update this page as development progresses, so please stay tuned for more updates. We also recommend following the Jira news on [Atlassian Developer blog](https://developer.atlassian.com/blog/categories/jira/) for important announcements and helpful articles.

## Summary

| Change/Application                                                           | Description             |
|------------------------------------------------------------------------------|------------------------------------|
| **Support for OpenJDK**<br>Jira Core, Jira Software, Jira Service Desk | Affects: Plugin developers, Jira admins<br>
| **Apache Tomcat upgrade**<br>Jira Core, Jira Software, Jira Service Desk | Affects: Plugin developers, Jira admins<br>


## Changes

#### Support for OpenJDK

We've added support for OpenJDK 8, and you can already test it in the latest EAP. Despite the support for OpenJDK, we'll continue to bundle Jira with Oracle JDK.

#### Apache Tomcat upgrade

We've upgraded Apache Tomcat to version 8.5.32, which requires that you make changes to the *server.xml* file.

**What's the problem?**

The Apache Tomcat server is filtering out requests that contain special characters, making them fail. That's because Tomcat is using a different encoding and URI standard than most browsers (details [here](https://cwiki.apache.org/confluence/display/TOMCAT/Encoding+and+URIs)). The problem is most visible when searching with JQL as you’d use a number of special characters when doing it (e.g. []<>), but it can also affect other pages in Jira. To make it work, you'll need to add some properties to the *server.xml* file.

**Versions**

We've already made this change in the latest bugfix release for Jira 7.12, but Jira 7.13 will be the first feature release to include it. If you're upgrading from Jira 7.12.1 or earlier, you need to complete the steps below.

**Steps to take**

You'll need to edit the *server.xml* file, and add properties that make Tomcat accept special characters in the requests.

1. Go to *Jira-installation-directory/conf*, and edit the *server.xml* file.
2. Find all connectors your application is using. Just search for *Connector* in the file, or look at the example below.
3. Add *relaxedPathChars="[]|" relaxedQueryChars="[]|{}^\`"<>"* to the connector properties in *server.xml*. For example:

```
<Connector port="8080" relaxedPathChars="[]|" relaxedQueryChars="[]|{}^&#x5c;&#x60;&quot;&lt;&gt;" maxThreads="150" minSpareThreads="25" connectionTimeout="20000" enableLookups="false" maxHttpHeaderSize="8192" protocol="HTTP/1.1" useBodyEncodingForURI="true" redirectPort="8443" acceptCount="100" disableUploadTimeout="true" bindOnInit="false"/>
```

4. Restart Jira.
5. (Data Center) Repeat these steps on each node.