---
aliases:
- /server/jira/platform/security-overview-32345317.html
- /server/jira/platform/security-overview-32345317.md
category: devguide
confluence_id: 32345317
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=32345317
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=32345317
date: '2019-04-02'
legacy_title: Security overview
platform: server
product: jira
subcategory: security
title: "Security overview"
---

# Security overview

Security is an essential part of both integrating with, and building apps for, Jira Server. Secure
integrations allow Atlassian products to protect customer data from unauthorized access and from 
malicious or accidental changes. Following best practices for security also allows 
administrators to install apps with confidence, letting users enjoy the benefits of apps in a secure
manner.

The methods for implementing security are different depending on whether you're integrating with 
Jira or building an app. When integrating with Jira, you'll follow one of the authentication 
mechanisms provided by Jira to call the REST API. When building an app, there are additional steps 
to follow to make your app secure, such as using form token handling.

## Authentication and authorization

There are two main security concepts when accessing data in Jira Server:

* **authentication:** determines the identity of the caller
* **authorization:** determines which actions the caller can take

The identity of your app or integration is taken as the user involved in the authentication process.
The actions the app or integration can take are controlled by that user's permissions in Jira.

## Security for calling the REST API

The REST API allows you to integrate with Jira by querying and mutating data in your Jira site. Jira 
bases the authorization for calls to the REST API on the user used in the authentication process. If
the calling user does not have permission to take a particular action in Jira, then they can’t take
that action using the REST API. For example, a user can call the REST API to create an issue if that
user has access to Jira and permission to create issues in Jira.

The Jira Server platform, Jira Software Server, and Jira Service Desk Server REST APIs can use one 
of the following methods of authentication. 

### OAuth

OAuth uses request tokens generated from Jira to authenticate users. We recommend using OAuth when
you integrate with Jira. It takes more effort to implement, but it is more flexible and secure
compared to the other two authentication methods.

See [OAuth](/server/jira/platform/oauth/), to learn how to implement a client that uses OAuth.

### Basic authentication

Basic authentication uses a predefined set of user credentials to authenticate. We 
recommend that you don't use basic authentication, except for tools like personal scripts or bots. 
It may be easier to implement, but it is much less secure.

See [Basic authentication](/server/jira/platform/basic-authentication/), to work through an example
of calling Jira with basic authentication.

### Cookie-based authentication

Jira uses cookie-based authentication in the browser. You can rely on this to call the REST API from
the browser (for example, via JavaScript). However, we recommend you use OAuth or Basic 
authentication in most cases.

See [Cookie-based authentication](/server/jira/platform/cookie-based-authentication), to learn how
to call Jira using cookies.

## Security for apps

Jira Server apps run alongside the product code, so you don't need to call the REST API. Instead, 
you call the Java API directly. However, there are additional steps to follow to make your app 
secure, such as using form token handling.

### Form token handling

Form token handling is an additional authentication mechanism for apps that lets Jira validate the
origin and intent of requests. This is used to provide another level of security against [cross-site
request forgery (XSRF)](https://en.wikipedia.org/wiki/Cross-site_request_forgery).

See [Form token handling](/server/jira/platform/form-token-handling/), to work through how to
implement form token handling in Jira.