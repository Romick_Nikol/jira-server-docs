---
aliases:
- /server/jira/platform/4227139.html
- /server/jira/platform/4227139.md
category: devguide
confluence_id: 4227139
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=4227139
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=4227139
date: '2017-12-08'
legacy_title: How do I get access to a Service or Manager component? Skip to end of
  metadata
platform: server
product: jira
subcategory: other
title: How do I get access to a service or manager component?
---
# How do I get access to a service or manager component?

### <img src="http://confluence.atlassian.com/images/icons/favicon.png" class="confluence-external-resource" /> How do I get access to a Service or Manager component?

#### Constructor Injection

The recommended approach is to use constructor injection, therefore simply add a constructor parameter to your class and the component will be injected into that parameter at runtime.

#### Static Lookup

{{% note %}}

This should be done as a last resort when your class can't participate in dependency injection for some reason.

{{% /note %}}

You can also statically get a component by using the `ComponentAccessor.getComponent(Class<T> componentClass)` method.

An example:

``` javascript
IssueLinkTypeManager issueLinkTypeManager = ComponentAccessor.getComponent(IssueLinkTypeManager.class);
```
