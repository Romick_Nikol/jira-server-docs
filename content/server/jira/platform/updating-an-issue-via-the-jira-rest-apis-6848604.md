---
title: Updating an Issue via the JIRA REST APIs 6848604
aliases:
    - /server/jira/platform/updating-an-issue-via-the-jira-rest-apis-6848604.html
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=6848604
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=6848604
confluence_id: 6848604
platform: server
product: jira
category: devguide
subcategory:
date: "2018-04-25"
---
# JIRA Developer Documentation : Updating an Issue via the JIRA REST APIs

<table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<tbody>
<tr class="odd">
<td><p>Available:</p></td>
<td><p>JIRA 5.0 and later.</p></td>
</tr>
</tbody>
</table>

 

This page describes the JIRA REST API for updating an issue. For the related tutorial, see [JIRA REST API Example - Edit issues](/server/jira/platform/jira-rest-api-example-edit-issues-6291632.html).

## Simple update (implicit set via "fields")

The simple way to update an issue is to do a GET, update the values inside "fields", and then PUT back.

-   If you PUT back exactly what you GOT, then you are also sending "names", "self", "key", etc. These are ignored.
-   You do not need to send all the fields inside "fields". You can just send the fields you want to update. Absent fields are left unchanged. For example, to update the summary:

    ``` javascript
    { "fields": {"summary": "new summary"} }
    ```

-   Some fields cannot be updated this way (for example, comments). Instead you must use explicit-verb updates (see below). You can tell if a field cannot be implicitly set by the fact it doesn't have a SET verb.
-   If you do send along such un-SET-able fields in this manner, they are ignored. This means that you can PUT what you GET, but not all of the document is taken into consideration.

## Operation (verb) based update (explicit verb via "update")

Each field supports a set of operations (verbs) for mutating the field. You can retrieve the editable fields and the operations they support using the "editmeta" resource. For example:

``` javascript
http://MYJIRA.INSTALL.COM:8080/rest/api/2/issue/ISSUE-KEY/editmeta
```

The basic operations are defined below (but custom fields can define their own).

The general shape of an update is field, array of verb-value pairs. For example:

``` javascript
{ "update": {
    "field1": [ {"verb": value}, {"verb": value}, ...],
    "field2": [ {"verb": value}, {"verb": value}, ...],
}}
```

-   SET: Sets the value of the field. The incoming value must be the same shape as the value of the field from a GET. For example, a string for "summary", and array of strings for "labels".
-   CLEAR: 

    {{% warning %}}

    We don't need CLEAR, it's the same as `SET: []`.

    {{% /warning %}}

-   ADD: Adds an element to a field that is an array. The incoming value must be the same shape as the items of the array in a GET. For example, to add a label:

    ``` javascript
    { "update": { "labels": [ {"add": "newlabel"} ] } }
    ```

-   REMOVE: Removes an element from a field that is an array. The incoming value must be the same shape as the items of the array in a GET (although you can remove parts of the object that are not needed to uniquely identify the object).
    -   remove a label:

        ``` javascript
        { "update": { "labels": [{"remove": "test" }] } }
        ```

    -   remove a comment (by comment id):

        ``` javascript
        { "update": { "comments": [{"remove": {"id": 10001} }] } }
        ```

-   EDIT: Edits an element in a field that is an array. The element is indexed/identified by the value itself (usually by id/name/key).
    -   edit the text of a particular comment:

        ``` javascript
        { "update": { "comments": [{"edit": {"id": 10002, "body": "newbody"} } ] } }
        ```

### Fields available with verb operations (work in progress)

<table>
<colgroup>
<col style="width: 33%" />
<col style="width: 33%" />
<col style="width: 33%" />
</colgroup>
<thead>
<tr class="header">
<th><p>Field</p></th>
<th><p>Verb</p></th>
<th><p>Field Value</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>Assignee</p></td>
<td><p>Set</p></td>
<td><p>User Id</p></td>
</tr>
<tr class="even">
<td><p>Comment</p></td>
<td><p>Add</p></td>
<td><p>Json object - (look at an issue to get the shape)</p></td>
</tr>
<tr class="odd">
<td><p>Components</p></td>
<td><p>Set, Add</p></td>
<td><p>Component Id or Name</p></td>
</tr>
<tr class="even">
<td><p>Description</p></td>
<td><p>Set</p></td>
<td><p>String</p></td>
</tr>
<tr class="odd">
<td><p>Fix Versions</p></td>
<td><p>Set, Add, Remove</p></td>
<td><p>Version Id or Name</p></td>
</tr>
<tr class="even">
<td><p>Labels</p></td>
<td><p>Set, Add, Remove</p></td>
<td><p>Label name</p></td>
</tr>
<tr class="odd">
<td><p>Priority</p></td>
<td><p>Set</p></td>
<td><p>Priority Id or Name</p></td>
</tr>
<tr class="even">
<td><p>Summary</p></td>
<td><p>Set</p></td>
<td><p>String</p></td>
</tr>
<tr class="odd">
<td><p>Affected Versions</p></td>
<td><p>Set, Add, Remove</p></td>
<td><p>Version Id or Name</p></td>
</tr>
<tr class="even">
<td><p>Worklog</p></td>
<td><p>Add</p></td>
<td><p>Json object - (look at an issue to get the shape)</p></td>
</tr>
</tbody>
</table>

## Combining implicit and explicit updates

You can have both "fields" and "update" in the one PUT, but a given field must appear only in one or the other.

## Validation and errors

If any of the implicit or explicit updates cause a validation error, an error document is returned, detailing the validation errors associated with each field.

##### RELATED TOPICS

[JIRA REST API Example - Edit issues](/server/jira/platform/jira-rest-api-example-edit-issues-6291632.html)

























































































































































































































































