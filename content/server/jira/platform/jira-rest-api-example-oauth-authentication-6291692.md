---
title: JIRA REST API Example OAuth Authentication 6291692
aliases:
    - /server/jira/platform/jira-rest-api-example-oauth-authentication-6291692.html
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=6291692
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=6291692
confluence_id: 6291692
platform: server
product: jira
category: devguide
subcategory:
date: "2018-04-25"
---
# JIRA Developer Documentation : JIRA REST API Example - OAuth authentication

This page shows you how to allow REST clients to authenticate themselves using <a href="https://en.wikipedia.org/wiki/OAuth" class="external-link">OAuth</a>. This is one of three methods that you can use for authentication against the JIRA REST API; the other two being **basic authentication** and **cookie-based authentication **(see [related information](#related-topics)).



## Overview

The instructions below describe how to use a Java client to provide OAuth authentication when making requests to JIRA's REST endpoints. It assumes you are familiar with the OAuth terminology (e.g. Consumer, Service Provider, request token, access token, etc.). For more information about OAuth refer to the OAuth specification.

**Looking for a Provider in a Language other than Java?**

Atlassian provides samples of OAuth providers in a number of other languages. <a href="https://bitbucket.org/atlassian_tutorial/atlassian-oauth-examples" class="external-link">Visit the sample repo on Bitbucket</a> to download and work with these samples.

## Step 1: Configuring JIRA

The first step is to register a new consumer in JIRA. This is done through the Application Links administration screens in JIRA. Create a new Application Link.
When creating the Application Link use a placeholder URL or the correct URL to your client, if your client can be reached via HTTP and choose the Generic Application type. After this Application Link has been created, edit the configuration and go to the incoming authentication configuration screen and select OAuth. Enter in this the public key and the consumer key which your client will use when making requests to JIRA.
After you have entered all the information click OK and ensure OAuth authentication is enabled.

## Step 2: Configuring the client

Your client will require the following information to be able to make authentication requests to JIRA.

<table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<thead>
<tr class="header">
<th><p>OAuth Config</p></th>
<th><p>value</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>request token url</p></td>
<td><p>JIRA_BASE_URL + &quot;/plugins/servlet/oauth/request-token&quot;</p></td>
</tr>
<tr class="even">
<td><p>authorization url</p></td>
<td><p>JIRA_BASE_URL + &quot;/plugins/servlet/oauth/authorize&quot;&quot;</p></td>
</tr>
<tr class="odd">
<td><p>access token url</p></td>
<td><p>JIRA_BASE_URL + &quot;/plugins/servlet/oauth/access-token&quot;</p></td>
</tr>
<tr class="even">
<td><p>oauth signing type</p></td>
<td><p>RSA-SHA1</p></td>
</tr>
<tr class="odd">
<td><p>consumer key</p></td>
<td><p>as configured in Step 1</p></td>
</tr>
</tbody>
</table>

## Example Java OAuth client

This example java code demonstrates how to write a client to make requests to JIRA's rest endpoints using OAuth authentication.
To be able to use OAuth authentication the client application has to do the "OAuth dance" with JIRA. This dance consists of three parts.

1.  Obtain a request token
2.  Ask the user to authorize this request token
3.  Swap the request token for an access token

After the client application has a valid access token, this can be used to make authenticated requests to JIRA.

#### Before you begin

You'll need to configure JIRA and download the example client first. This example client uses the consumer key "hardcoded-consumer" and the public key is:

``` javascript
MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAxZDzGUGk6rElyPm0iOua0lWg84nOlhQN1gmTFTIu5WFyQFHZF6OA4HX7xATttQZ6N21yKMakuNdRvEudyN/coUqe89r3Ae+rkEIn4tCxGpJWX205xVF3Cgsn8ICj6dLUFQPiWXouoZ7HG0sPKhCLXXOvUXmekivtyx4bxVFD9Zy4SQ7IHTx0V0pZYGc6r1gF0LqRmGVQDaQSbivigH4mlVwoAO9Tfccf+V00hYuSvntU+B1ZygMw2rAFLezJmnftTxPuehqWu9xS5NVsPsWgBL7LOi3oY8lhzOYjbMKDWM6zUtpOmWJA52cVJW6zwxCxE28/592IARxlJcq14tjwYwIDAQAB
```

You have to create an Application Link as described in [Step 1](#step-1-configuring-jira) above and use this consumer key and the public key and leave the callback URL field empty.

Download the attached jar files:

 

[File](https://developer.atlassian.com/display/JIRADEV/JIRA+REST+API+Example+-+OAuth+authentication?sortBy=name&sortOrder=ascending)

[Modified](https://developer.atlassian.com/display/JIRADEV/JIRA+REST+API+Example+-+OAuth+authentication?sortBy=date&sortOrder=descending)

Java Archive <a href="/download/attachments/6291692/rest-oauth-client-1.0-sources.jar?api=v2" class="filename" title="Download">rest-oauth-client-1.0-sources.jar</a>

Sep 27, 2011 by <a href="%20%20%20%20/display/~fschmitz%0A" class="fn confluence-userlink url">Felix Schmitz [Atlassian]</a>

Labels

-   No labels
-   <a href="#edit-labels" class="show-labels-editor" title="Edit Labels">Edit Labels</a>

<span class="aui-button previewAttachmentLink">Preview</span> <a href="$itemRenderedUrl&amp;isFromPageView=true" class="aui-button editAttachmentLink">$itemLabel</a> <a href="$itemRenderedUrl&amp;isFromPageView=true" class="removeAttachmentLink aui-button">$itemLabel</a>

Java Archive <a href="/download/attachments/6291692/rest-oauth-client-1.0.one-jar.jar?api=v2" class="filename" title="Download">rest-oauth-client-1.0.one-jar.jar</a>

Sep 27, 2011 by <a href="%20%20%20%20/display/~fschmitz%0A" class="fn confluence-userlink url">Felix Schmitz [Atlassian]</a>

Labels

-   No labels
-   <a href="#edit-labels" class="show-labels-editor" title="Edit Labels">Edit Labels</a>

<span class="aui-button previewAttachmentLink">Preview</span> <a href="$itemRenderedUrl&amp;isFromPageView=true&amp;isFromPageView=true" class="aui-button editAttachmentLink">$itemLabel</a> <a href="$itemRenderedUrl&amp;isFromPageView=true&amp;isFromPageView=true" class="removeAttachmentLink aui-button">$itemLabel</a>

Drag and drop to upload or <span class="aui-button browse-files aui-button-link">browse for files</span> <img src="/server/jira/platform/images/icons/wait.gif" class="plugin_attachments_dropzone_uploadwaiticon" />

Upload file

File description

<a href="/pages/downloadallattachments.action?pageId=6291692" class="download-all-link" title="Download all the latest versions of attachments on this page as single zip file.">Download All</a>


The **rest-oauth-client-1.0.one-jar.jar** contains the sample client and the **rest-oauth-client-1.0-sources.jar** contains the source code.

{{% warning %}}

If you are using JIRA 5.2 or earlier:** **The sample client uses HTTP POST to communicate with JIRA.  Support for OAuth parameters in the body of an HTTP POST was added in JIRA 6.0.  In order to run this sample client against a version of JIRA earlier than 6.0, the sample client (specifically `com.atlassian.oauth.client.example.AtlassianOAuthClient`) will need to be changed to use HTTP GET when communicating with JIRA.

{{% /warning %}}

1. Obtain a request token from JIRA

Execute this command:

``` javascript
java -jar rest-oauth-client-1.0.one-jar.jar requestToken JIRA_BASE_URL
```

Replace JIRA\_BASE\_URL with the URL to your JIRA instance.
After executing this command you should see a response like

``` javascript
Token is W9QuE8hba6laXm2RcPGANVusKHnXUJcx
Token secret is rx4T2R3ax7an3H0vJLq9XB9DOP3aiNMl
Retrieved request token. go to http://localhost:8090/jira/plugins/servlet/oauth/authorize?oauth_token=W9QuE8hba6laXm2RcPGANVusKHnXUJcx
```

#### 2. Authorize this token

Go to the URL in system out and login into JIRA and approve the access. Afterwards JIRA will say that you have successfully authorised the access. It mentions a verification code which we need for the next step.

#### 3. Swap the request token with an access token

Execute the following command

``` javascript
java -jar rest-oauth-client-1.0.one-jar.jar accessToken JIRA_BASE_URL REQUEST_TOKEN TOKEN_SECRET VERIFIER
```

Replace JIRA\_BASE\_URL, REQUEST\_TOKEN, TOKEN\_SECRET and VERIFIER with the correct values.

In the response you should see

``` javascript
Access token is : QddAGsDSS0FkXCb1zRCCzzeShZRnUXYH
```

This access token will allow you to make authenticated requests to JIRA.

#### 4. Make an authentication request to a rest-end point

To make an authenticated request to a rest resource in JIRA execute this command:

``` javascript
java -jar rest-oauth-client-1.0.one-jar.jar request ACCESS_TOKEN JIRA_REST_URL
```

Replace ACCESS\_TOKEN, JIRA\_REST\_URL and ISSUE\_KEY with the correct values.
JIRA\_REST\_URL, e.g. <a href="http://localhost:8090/jira/rest/api/2/issue/HSP-1" class="uri external-link">http://localhost:8090/jira/rest/api/2/issue/HSP-1</a>
This will return the issue JSON object for the issue with the key "HSP-1"

You should see a response like:

``` javascript
{
"expand": "html,names,schema",
"id": "10000",
"self": "http://localhost:8090/jira/rest/api/2/issue/HSP-1",
"key": "HSP-1",
"fields": {
"summary": "Bug due two weeks ago",
"issuetype": {
"self": "http://localhost:8090/jira/rest/api/2/issuetype/1",
"id": "1",
"description": "A problem which impairs or prevents the functions of the product.",
"iconUrl": "http://localhost:8090/jira/images/icons/bug.gif",
"name": "Bug",
"subtask": false
},
"status": {
"self": "http://localhost:8090/jira/rest/api/2/status/5",
"iconUrl": "http://localhost:8090/jira/images/icons/status_resolved.gif",
"name": "Resolved",
"id": "5"
},
"labels": [(0)],
"votes": {
"self": "http://localhost:8090/jira/rest/api/2/issue/HSP-1/votes",
"votes": 0,
"hasVoted": false
},
"assignee": {
"self": "http://localhost:8090/jira/rest/api/2/user?username=admin",
"name": "admin",
"emailAddress": "admin@example.com",
"avatarUrls": {
"16x16": "http://localhost:8090/jira/secure/useravatar?size=small&avatarId=10062",
"48x48": "http://localhost:8090/jira/secure/useravatar?avatarId=10062"
},
"displayName": "Administrator",
"active": true
},
"resolution": {
"self": "http://localhost:8090/jira/rest/api/2/resolution/1",
"id": "1",
"description": "A fix for this issue is checked into the tree and tested.",
"name": "Fixed"
},
"fixVersions": [(0)],
"security": null,
"resolutiondate": "2011-09-26T15:44:39.220+1000",
"sub-tasks": [(0)],
"reporter": {
"self": "http://localhost:8090/jira/rest/api/2/user?username=admin",
"name": "admin",
"emailAddress": "admin@example.com",
"avatarUrls": {
"16x16": "http://localhost:8090/jira/secure/useravatar?size=small&avatarId=10062",
"48x48": "http://localhost:8090/jira/secure/useravatar?avatarId=10062"
},
"displayName": "Administrator",
"active": true
},
"project": {
"self": "http://localhost:8090/jira/rest/api/2/project/HSP",
"id": "10000",
"key": "HSP",
"name": "homosapien",
"roles": {},
"avatarUrls": {
"16x16": "http://localhost:8090/jira/secure/projectavatar?size=small&pid=10000&avatarId=10011",
"48x48": "http://localhost:8090/jira/secure/projectavatar?pid=10000&avatarId=10011"
}
},
"versions": [(0)],
"environment": null,
"created": "2011-09-26T15:44:23.888+1000",
"updated": "2011-09-26T15:44:39.295+1000",
"priority": {
"self": "http://localhost:8090/jira/rest/api/2/priority/5",
"iconUrl": "http://localhost:8090/jira/images/icons/priority_trivial.gif",
"name": "Trivial",
"id": "5"
},
"description": null,
"duedate": "2011-09-25",
"components": [(1)
{
"self": "http://localhost:8090/jira/rest/api/2/component/10000",
"id": "10000",
"name": "New Component 1"
}
],
"comment": {
"startAt": 0,
"maxResults": 0,
"total": 0,
"comments": [(0)]
},
"watches": {
"self": "http://localhost:8090/jira/rest/api/2/issue/HSP-1/watchers",
"watchCount": 0,
"isWatching": false
}
},
"transitions": "http://localhost:8090/jira/rest/api/2/issue/HSP-1/transitions",
"editmeta": "TODO",
"changelog": "TODO"
}
```

## Advanced topics

#### Using helper libraries

If you want to use OAuth to make requests to JIRA, the best way to do this is to find a **helper library** which takes care of signing the requests and reading the tokens from the response.
The example below is using the net.oauth library.

``` xml
<dependencies>
        <dependency>
            <groupId>net.oauth.core</groupId>
            <artifactId>oauth</artifactId>
            <version>20090617</version>
        </dependency>
        <dependency>
            <groupId>net.oauth.core</groupId>
            <artifactId>oauth-httpclient4</artifactId>
            <version>20090617</version>
        </dependency>
</dependencies>
```

#### CAPTCHA

CAPTCHA is 'triggered' after several consecutive failed log in attempts, after which the user is required to interpret a distorted picture of a word and type that word into a text field with each subsequent log in attempt. If CAPTCHA has been triggered, you cannot use JIRA's REST API to authenticate with the JIRA site.

You can check this in the error response from JIRA -- If there is an **`X-Seraph-LoginReason`** header with a a value of **`AUTHENTICATION_DENIED `**`or`**` AUTHENTICATED_FAILED`**, this means the application rejected the login without even checking the password. This is the most common indication that JIRA's CAPTCHA feature has been triggered.

## Related topics

-   [JIRA REST API Example - Cookie-based Authentication](/server/jira/platform/jira-rest-api-example-cookie-based-authentication-37234858.html)
-   [JIRA REST API Example - Basic Authentication](/server/jira/platform/jira-rest-api-example-basic-authentication-6291732.html)
