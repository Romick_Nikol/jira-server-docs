---
aliases:
- /server/jira/platform/--section-attribute-values-for-web-items-in-the-dashboard-drop-down-4227193.html
- /server/jira/platform/--section-attribute-values-for-web-items-in-the-dashboard-drop-down-4227193.md
category: devguide
confluence_id: 4227193
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=4227193
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=4227193
date: '2017-12-08'
legacy_title: __Section_Attribute_Values_for_Web_Items_in_the_Dashboard_Drop-Down
platform: server
product: jira
subcategory: other
title: Section attribute values for Web Items in the dashboard drop-down
---
# Section attribute values for Web Items in the dashboard drop-down

![](/server/jira/platform/images/section-attribute-values-for-web-items-in-the-dashboard-drop-down.png)














