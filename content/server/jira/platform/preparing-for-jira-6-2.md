---
aliases:
- /server/jira/platform/preparing-for-jira-6.2-23299270.html
- /server/jira/platform/preparing-for-jira-6.2-23299270.md
category: devguide
confluence_id: 23299270
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=23299270
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=23299270
date: '2017-12-08'
legacy_title: Preparing for JIRA 6.2
platform: server
product: jira
subcategory: updates
title: Preparing for JIRA 6.2
---
# Preparing for JIRA 6.2

## Overview

This page is intended to be read by JIRA plugin developers to inform them of changes in JIRA 6.2 that could affect their plugins. JIRA users should consult the documentation <a href="https://confluence.atlassian.com/display/JIRA/JIRA+Documentation" class="external-link">here</a>.

It is our goal to notify you, our plugin developers, **as far in advance as possible** of everything we know about that can possibly affect your plugins. Where possible, we will attach release targets.

We will update this page as the release progresses. Please **watch this page** **or check back regularly** to keep on top of the changes.

## Important Note

To allow us to dedicate resources to providing the best experience on modern browsers, JIRA 6.2 (including minor releases) will be the **last release that supports Internet Explorer 8 (IE8)**. For more information, please see <a href="https://confluence.atlassian.com/display/JIRA/End+of+Support+Announcements+for+JIRA#EndofSupportAnnouncementsforJIRA-201310ie8" class="external-link">this page</a>.

### About the Early Access Program

The JIRA development team release an EAP milestone every two weeks for customers and plugin developers to keep abreast of upcoming changes. These EAP releases are available on the <a href="http://www.atlassian.com/software/jira/download-eap" class="external-link">JIRA Early Access Program Downloads</a> page.

## Summary of Changes

The risk level indicates the level of certainty we have that things will break if you are in the "Who is affected?" column and you don't make the necessary changes.

<table>
<colgroup>
<col style="width: 30%" />
<col style="width: 30%" />
<col style="width: 40%" />
</colgroup>
<thead>
<tr class="header">
<th>What is the change?</th>
<th>When will this happen?*</th>
<th>Who is affected?</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>Issue Status as Lozenges</p></td>
<td><p>6.2-OD4 (JIRA Cloud)</p></td>
<td><p>Plugins that output issue statuses in their UI</p>
<p><strong>Risk level: low</strong></p>
<p><strong>Details:</strong> See <a href="/server/jira/platform/jira-issue-statuses-as-lozenges">JIRA issue statuses as lozenges</a> for details.</p></td>
</tr>
<tr class="even">
<td>Use of AMD via AlmondJS</td>
<td>6.2-OD4 (JIRA Cloud)</td>
<td>Plugins using RequireJS, AlmondJS or similar javascript libraries that provide define() and require() functions
<p><strong>Risk level: medium</strong></p>
<p><strong>Details:</strong> See <a href="#use-of-amd-in-jira">Use of AMD in JIRA</a> below.</p></td>
</tr>
<tr class="odd">
<td>Upgrade to AUI 5.3</td>
<td><p>6.2-OD8 (JIRA Cloud)</p></td>
<td>Plugins that provide UI
<p><strong>Risk level: high</strong></p>
<p><strong>Details:</strong> See <a href="#plugin-upgrade-aui-5-3">Plugin upgrade -- AUI 5.3</a> below.</p></td>
</tr>
<tr class="even">
<td>JIRA internal events</td>
<td>6.2-OD4 (JIRA Cloud)</td>
<td><p><strong>Risk level: low</strong></p>
<p><strong>Details:</strong> See <a href="#changes-to-jira-events">Changes to JIRA events</a> below.</p></td>
</tr>
<tr class="odd">
<td>New entity properties</td>
<td>6.2-OD8</td>
<td><p><strong>Risk level: N/A</strong></p>
<p><strong>Details:</strong> See <a href="#entity-properties">Entity properties</a> below.</p></td>
</tr>
<tr class="even">
<td>Internet Explorer support</td>
<td>6.2 final</td>
<td>All plugins
<p><strong>Risk level: medium</strong></p>
<p><strong>Details:</strong> See <a href="#internet-explorer-11-supported-in-jira-6-2">Internet Explorer 11 supported in JIRA 6.2</a> below.</p></td>
</tr>
<tr class="odd">
<td>'creator' column added to 'jiraissues' table</td>
<td>6.2 final</td>
<td><p><strong>Risk level: low</strong></p>
<p><strong>Details:</strong> See <a href="#creator-column-added-to-jiraissues-table">'creator' column added to jiraissues table</a> below.</p></td>
</tr>
<tr class="even">
<td>Source distribution -- Internal Atlassian dependencies are now Maven-based archives</td>
<td>6.2 final</td>
<td><p><strong>Risk level: medium</strong></p>
<p><strong>Details:</strong> See <a href="#source-distribution-internal-atlassian-dependencies-are-now-maven-based-archives">Source distribution -- Internal Atlassian dependencies are now Maven-based archives</a> below</p></td>
</tr>
</tbody>
</table>

## Detailed Explanations of Changes

**In this section:**

-   [Use of AMD in JIRA](#use-of-amd-in-jira)
-   [Plugin upgrade -- AUI 5.3](#plugin-upgrade-aui-5-3)
-   [Changes to JIRA's footer](#changes-to-jira-s-footer)
-   [Changes to JIRA events](#changes-to-jira-events)
-   [Internet Explorer 11 supported in JIRA 6.2](#internet-explorer-11-supported-in-jira-6-2)
-   ['creator' column added to jiraissues table](#creator-column-added-to-jiraissues-table)
-   [Entity properties](#entity-properties)
-   [Source distribution -- Internal Atlassian dependencies are now Maven-based archives](#source-distribution-internal-atlassian-dependencies-are-now-maven-based-archives)

### Use of AMD in JIRA

JIRA now includes AlmondJS in the superbatch, enabling use of Asynchronous Module Definitions (AMD) in JIRA's JavaScript.

When a module is defined, it cannot be redefined; The first definition will always take precedence. The consequence is that if plugins define modules with the same name as JIRA, JIRA's definitions will overwrite the plugin's definition. This may result in plugins using JIRA's versions of libraries rather than their own definitions.

JIRA defines the following AMD modules in 6.2-OD4:

-   aui
-   jquery
-   backbone
-   underscore

It is recommended that plugin developers namespace their own AMD modules to avoid any potential conflicts.

### Plugin upgrade -- AUI 5.3

JIRA 6.2 will include a new version of the Atlassian User Interface library. The new version is two releases ahead from JIRA 6.1, and as such developers should review the release notes for both versions when upgrading their plugins:

-   [AUI 5.2 Release Notes](https://developer.atlassian.com/display/AUI/AUI+5.2+Release+Notes) (note that the jQuery upgrade is reversed as part of the AUI 5.3 upgrade. JIRA 6.2 will use jQuery 1.7.2)
-   [AUI 5.3 Release Notes](https://developer.atlassian.com/display/AUI/AUI+5.3+Release+Notes)

We expect that the changes most likely to cause problems for plugin developers are the upgrades to external libraries:

-   <a href="http://backbonejs.org/" class="external-link">Backbone</a> 0.9.2 → 1.0.0
-   <a href="http://jqueryui.com/" class="external-link">jQuery UI</a> 1.8.11 → 1.8.24
-   <a href="http://underscorejs.org/" class="external-link">Underscore</a> 1.4.3 → 1.5.2

### Changes to JIRA's footer

As part of the AUI upgrade, the JIRA's footer HTML markup has changed. Plugins that provide a footer via `<jira-footer>` in their `atlassian-plugin.xml` will need to update their output to no longer include the `<div class="footer-body">` wrapper around the `<ul>`. The `<ul>` should become the top level element provided by footer providers. The `div.footer-body` element has been replaced with a `section.footer-body` element, which is now provided by JIRA, rather than plugins. It's conceivable that this change may affect some CSS selectors.

### Changes to JIRA events

JIRA 6.2 will include auditing -- the ability to track configuration changes done by administrators. To support this, we have enhanced and modified a number of internal events. In general, events now include more details about the action. Hence, some events have now different constructors (the previous ones were removed). These **are not API breaking changes**, although they may be perceived to be so. These constructors were only meant to be used internally, but they were never marked as internal. Starting with JIRA 6.2, they will now include a proper `@Internal` annotation. Here is the list of events that will change (if for any reason you created those events in your code, please contact us):

-   `AbstractCustomFieldEvent`
-   `CustomFieldUpdatedEvent`
-   `DraftWorkflowPublishedEvent`
-   `NotificationDeletedEvent`
-   `NotificationSchemeDeletedEvent`
-   `PermissionDeletedEvent`
-   `PermissionSchemeDeletedEvent`
-   `WorkflowSchemeAddedToProjectEvent`
-   `WorkflowSchemeDeletedEvent`
-   `WorkflowUpdatedEvent`

We do not expect a lot of people to have used these constructors, but in case you have used them in your plugin, **please let us kno**w: contact `bartek (at) atlassian (dot) com` or leave a comment under this page.

### Internet Explorer 11 supported in JIRA 6.2

We are planning on supporting Internet Explorer 11 for JIRA Cloud and downloadable JIRA. We have scheduled this for the final production release of downloadable JIRA 6.2. Please ensure that your plugin is compatible with Internet Explorer 11, if you want it to work with JIRA 6.2 or JIRA Cloud.

### 'creator' column added to jiraissues table

{{% note %}}

Note, this change was also implemented in <a href="https://confluence.atlassian.com/display/JIRA/JIRA+6.1.6+Upgrade+Notes" class="external-link">JIRA 6.1.6</a>.

{{% /note %}}

A new `creator `field was introduced in JIRA 6.2 EAP 6 m6 and will be included in the final JIRA 6.2 production release. This field was implemented as a column in the `jiraissues` table in the JIRA database. Please note the following:

-   **New JIRA 6.2 instances**: the `creator` field will be located after the `assignee` column
-   **Existing JIRA instances that are upgraded to 6.2**: the `creator `field will be inserted as the last column in `jiraissues` table

We strongly recommend that you do not rely on the order of columns in the tables of the JIRA database, but instead refer to the columns explicitly by name.

### Entity properties

JIRA 6.2 will include a new feature allowing add-ons to add key/value stores to JIRA entities, such as issues or projects. These values are indexed by JIRA and can be queried via a REST API or (in some cases) through JQL. More details are provided on the [Index Document Configuration Plugin Module](/server/jira/platform/index-document-configuration) page as well as in the <a href="https://docs.atlassian.com/jira/REST/ondemand" class="external-link">JIRA REST API documentation</a>.

### Source distribution -- Internal Atlassian dependencies are now Maven-based archives

JIRA's source distribution includes the source of the internal Atlassian projects that JIRA depends on (e.g. `atlassian-bonnie`, `atlassian-core`, etc.). From this release onwards, these internal Atlassian dependencies for JIRA's source distribution are now Maven-based archives, rather than exports of the source repository for each dependency. There will be a Maven-based archive for each dependency, named **&lt;dependencyname-version&gt;-sources.jar**. To provide us with feedback, please raise a ticket at https://jira.atlassian.com.

Please note the following:

-   The only buildable part of the source package is JIRA itself, not its dependencies.
-   A more complete source distribution has been provided as existing exclusions will be removed.
-   If you have customized an internal Atlassian dependency, you may not be able to apply that customization to the dependency anymore. If you have done this, try searching/posting to the <a href="https://answers.atlassian.com/tags/jira-development/" class="external-link">Atlassian Answers</a> for an alternative solution.
-   If you have customized a third-party library that has a dependency from JIRA source, you can still change the dependency in JIRA source. However, if the library has a dependency on an internal Atlassian dependency, you won't be able to change the library. If you have done this, try searching/posting to the <a href="https://answers.atlassian.com/tags/jira-development/" class="external-link">Atlassian Answers</a> for an alternative solution.
