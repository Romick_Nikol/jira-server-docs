---
title: Data Storage for JIRA Plugins 7897177
aliases:
    - /server/jira/platform/data-storage-for-jira-plugins-7897177.html
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=7897177
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=7897177
confluence_id: 7897177
platform: server
product: jira
category: devguide
subcategory:
date: "2018-04-25"
---
# JIRA Developer Documentation : Data Storage for JIRA Plugins

<table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<tbody>
<tr class="odd">
<td><p>Available:</p></td>
<td><p>Active Objects is currently bundled with JIRA 4.4 and later.</p></td>
</tr>
</tbody>
</table>

Active Objects is a new ORM layer into our products that enables easier, faster and more scalable data access and storage than our existing [PluginSettings](https://developer.atlassian.com/display/DOCS/SAL+Services) API.

See the [Active Objects documentation](https://developer.atlassian.com/display/DOCS/Active+Objects).
