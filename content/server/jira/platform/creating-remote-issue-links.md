---
aliases:
- /server/jira/platform/guide-jira-remote-issue-links-6848555.html
- /server/jira/platform/guide-jira-remote-issue-links-6848555.md
category: devguide
confluence_id: 6848555
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=6848555
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=6848555
date: '2017-12-08'
guides: guides
legacy_title: Guide - JIRA Remote Issue Links
platform: server
product: jira
subcategory: learning
title: Creating Remote Issue Links
---
# Creating Remote Issue Links

<table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<tbody>
<tr class="odd">
<td><p>Available:</p></td>
<td><p>JIRA 5.0 and later.</p></td>
</tr>
</tbody>
</table>

Remote Issue Links offers a simple but powerful way to integrate your application with JIRA. If you have an object in your application that relates to a JIRA ticket, you can link that object to the JIRA issue using Remote Issue Links.

## Overview

Let's say you have a support ticket raised by a customer in your help desk system and the bug that causes that issue is being tracked in JIRA. With a single API call, you can create a link in that JIRA issue pointing to the support ticket. You can provide an icon, a title, a summary, even a status icon for the remote object.

Want to understand more? Here's a quick video on creating Remote Issue Links via REST.

{{< youtube G26HX51Xx5Q >}}

## REST API Tutorial

Want to see some JSON? See how easy it is to create a Remote Issue Link in the [REST API tutorial](/server/jira/platform/jira-rest-api-for-remote-issue-links).

## JAVA API

You can also manage remote issue links from within JIRA using the Java API. The key classes are:

-   <a href="http://docs.atlassian.com/jira/5.0-beta3/com/atlassian/jira/bc/issue/link/RemoteIssueLinkService.html" class="external-link">RemoteIssueLinkService</a> - service used to create, retrieve, update, delete and validate remote issue links.
-   <a href="http://docs.atlassian.com/jira/5.0-beta3/com/atlassian/jira/issue/link/RemoteIssueLink.html" class="external-link">RemoteIssueLink</a> - the remote issue link model.
-   <a href="http://docs.atlassian.com/jira/5.0-beta3/com/atlassian/jira/issue/link/RemoteIssueLinkBuilder.html" class="external-link">RemoteIssueLinkBuilder</a> - a builder to help construct remote issue link model objects.

## Guide to Fields

All the fields used in the Remote Issue Link API are described in the guide to [Remote Issue Links fields](/server/jira/platform/using-fields-in-remote-issue-links).

## Guide to the Issue Link Renderer Plugin

You can now create an issue link renderer plugin to customise how issue links appear for your application. Find out more in the guide to the [Issue Link Renderer plugin](/server/jira/platform/creating-custom-renderers-for-remote-issue-links)

## Guide to the Issue Link Creation Dialog

You can also create a dialog for easy insertion of links to your application. Find out more in the guide to the [issue link creation dialog](/server/jira/platform/creating-a-custom-issue-link-dialog-for-remote-issue-links).

