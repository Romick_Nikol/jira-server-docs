---
aliases:
- /server/jira/platform/tutorial-adding-menu-items-to-jira-2818697.html
- /server/jira/platform/tutorial-adding-menu-items-to-jira-2818697.md
category: devguide
confluence_id: 2818697
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=2818697
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=2818697
date: '2018-05-28'
guides: tutorials
legacy_title: Tutorial - Adding menu items to JIRA
platform: server
product: jira
subcategory: learning
title: "Adding menu items to Jira"
---
# Adding menu items to Jira

<table>
<colgroup>
<col style="width: 20%" />
<col style="width: 80%" />
</colgroup>
<tbody>
<tr class="odd">
<td><p>Applicable:</p></td>
<td><p>Jira 7.0.0 and later.</p></td>
</tr>
<tr class="even">
<td><p>Level of experience:</p></td>
<td><p>Beginner. This is a good tutorial to try if you have never developed an app before.</p></td>
</tr>
<tr class="odd">
<td><p>Time estimate:</p></td>
<td><p>It should take you approximately half an hour to complete this tutorial.</p></td>
</tr>
</tbody>
</table>

## Overview of the tutorial

This tutorial shows you how to write a simple app that adds a new menu to Jira header. The menu includes two menu
items that link to external web pages. This app is simple but useful.
Using this app you can link from Jira header to websites that are important for your organization, such as your company's
intranet or external website.

You can add UI elements to Jira by adding module definitions to the app descriptor.
In this tutorial, you'll add the following modules:

*   A [web section](/server/jira/platform/web-section) to define the new tab (section) in your Jira top navigation bar.
*   A number of [web items](/server/jira/platform/web-item) to add the links and menu items to the new section.

Your completed app will consist of the following components: 

1.   An app descriptor to enable the app in Jira.
1.   The required plugin modules that define the new menu section and menu items.

This app does not need any Java code because the plugin modules provided by the Atlassian Plugin Framework provide all
the functionality required.

{{% note %}}

About these instructions

You can use any supported combination of operating system and IDE to create this app. These instructions were written using
IntelliJ IDEA 2017.3 on macOS Sierra. If you use another operating system or IDE combination, you should use
the equivalent operations for your specific environment.

This tutorial was last tested with Jira 7.7.1 using the Atlassian SDK 6.3.10.

{{% /note %}}

### Before you begin

To complete this tutorial, you need to know the following: 

1.   The basics of Java development: classes, interfaces, methods, how to use the compiler, and so on.
1.   How to create an Atlassian plugin project using the [Atlassian Plugin SDK](/server/framework/atlassian-sdk/set-up-the-atlassian-plugin-sdk-and-build-a-project/).
1.   The basics of using and administering Jira.

### App source

We encourage you to work through this tutorial. If you want to skip ahead or check your work when you are finished,
you can find the app source code on Atlassian Bitbucket.

To clone the repository, run the following command:

``` bash
git clone https://bitbucket.org/atlassian_tutorial/jira-menu-items-plugin.git
```

Alternatively, you can download the source as a [ZIP archive](https://bitbucket.org/atlassian_tutorial/jira-menu-items-plugin/get/master.zip)

## Step 1. Create the app project

In this step, you'll use an `atlas-` command to generate stub code for your app.

1.  [Set up the Atlassian Plugin SDK and build a project](/server/framework/atlassian-sdk/set-up-the-atlassian-plugin-sdk-and-build-a-project/)
if you did not do that yet.
1.  Open a Terminal and navigate to the directory where you would like to keep the project code.
1.  To create an app skeleton, run the following command:

    ``` bash
    atlas-create-jira-plugin
    ```

1.  To identify your app, enter the following information when prompted.

    <table>
    <colgroup>
    <col style="width: 20%" />
    <col style="width: 80%" />
    </colgroup>
    <tbody>
    <tr class="odd">
    <td><p>group-id</p></td>
    <td><p><code>com.atlassian.plugins.tutorial</code></p></td>
    </tr>
    <tr class="even">
    <td><p>artifact-id</p></td>
    <td><p><code>jira-menu-items</code></p></td>
    </tr>
    <tr class="odd">
    <td><p>version</p></td>
    <td><p><code>1.0-SNAPSHOT</code></p></td>
    </tr>
    <tr class="even">
    <td><p>package</p></td>
    <td><p><code>com.atlassian.plugins.tutorial</code></p></td>
    </tr>
    </tbody>
    </table>

1.  Confirm your entries when prompted.
    The SDK generates the initial app project files in a directory named `jira-menu-items`.
1.  Navigate to the directory created in the previous step.
1.  Delete the test directories.

    Setting up testing for your app isn't part of this tutorial. To delete the generated test skeleton,
    run the following commands:

    ``` bash
    rm -rf ./src/test/java
    rm -rf ./src/test/resources/
    ```

1.  Delete the unneeded Java class files.

    ``` bash
    rm -rf ./src/main/java/com/atlassian/plugins/tutorial/*
    ```

1.  Import project in your favorite IDE.

## Step 2. Review and tweak the generated stub code

It is a good idea to familiarize yourself with the project configuration file known as the POM (that is,
  Project Object Model definition file).

In this step, you'll review and tweak the `pom.xml` file.

The POM is located at the root of your project and declares the project dependencies and other information.

1.  Navigate to the root folder of your app and open the `pom.xml` file.
1.  Add your company or organization name and your website URL to the `organization` element
(the following code blocks show how it looks in plain text):

    ``` xml
    <organization>
        <name>Example Company</name>
        <url>http://www.example.com/</url>
    </organization>
    ```

1.  To add a meaningful description for your app, update the project `description` element. For example:

    ``` xml
    <description>This plugin adds company links to a new menu in the JIRA header.</description>
    ```

1.  Save the file.

1.  Your stub code contains an app descriptor file called `atlassian-plugin.xml`. This is an XML file that identifies
the app to the host application (that is, Jira) and defines the required app functionality.

    Open the `atlassian-plugin.xml` file.

    You should see something like this (comments removed):  

``` xml
<?xml version="1.0" encoding="UTF-8"?>
<atlassian-plugin key="${atlassian.plugin.key}" name="${project.name}" plugins-version="2">
    <plugin-info>
        <description>${project.description}</description>
        <version>${project.version}</version>
        <vendor name="${project.organization.name}" url="${project.organization.url}"/>
        <param name="plugin-icon">images/pluginIcon.png</param>
        <param name="plugin-logo">images/pluginLogo.png</param>
    </plugin-info>
    <!-- add our i18n resource -->
    <resource type="i18n" name="i18n" location="jira-menu-items"/>
    <!-- add our web resources -->
    <web-resource key="jira-menu-items-resources" name="jira-menu-items Web Resources">
        <dependency>com.atlassian.auiplugin:ajs</dependency>
        <resource type="download" name="jira-menu-items.css" location="/css/jira-menu-items.css"/>
        <resource type="download" name="jira-menu-items.js" location="/js/jira-menu-items.js"/>
        <resource type="download" name="images/" location="/images"/>
        <context>jira-menu-items</context>
    </web-resource>
</atlassian-plugin>
```

## Step 3. Start Jira instance

In this step, you'll start Jira and see what we've got so far.

1.  In Terminal window, navigate to the `jira-menu-items` directory created by the SDK.
1.  Run the following the SDK command:

    ``` bash
    atlas-run
    ```

    This command starts a Jira instance and loads your app. In the output, look for a line that looks something like this:

    ``` bash
    [INFO] jira started successfully in 134s at http://localhost:2990/jira
    ```

    It tells you that Jira instance has been started and shows you the Jira home page URL.
1.  In a browser, go to Jira home page that is indicated in the Terminal output. 
1.  Log in using the default admin/admin username and password combination.
1.  When prompted for the type of project to create, either create a project or click **Cancel**.
    Jira prompts you to create a new project like this only when you start a new instance.
1.  At the top right of the page, click ![cog icon](/server/jira/platform/images/bluecog-icon.png) > **Add-ons**. 
1.  Click **Manage Add-ons** from the left menu.
1.  In the User-installed Add-ons list, look for the **jira-menu-items** app that you created. Alternatively,
enter the name in the filter field to find it quickly.
1.  Click the app in the list to expand its details view.  
    The details of your app appear.  

    <img src="/server/jira/platform/images/jiraplugin3.png" width="650" />

The app worked, but so far it doesn't do much. You will enhance it in the next steps.

Leave Jira running in browser for now. 

## Step 4. Add the plugin modules to the app descriptor

In this step, you will add the new section and menu items to your app descriptor as follows.

1.  Navigate to `src/main/resources/` and open the `atlassian-plugin.xml` file.
1.  Add the following web section as a child element to `atlassian-plugin`:

    ``` xml
     <web-section name="My Links Main Section" i18n-name-key="my-links-main-section.name" key="my_links_section"
                     location="my_links_link" weight="10"/>
    ```

    This defines the top-level section where you will put all menu items and links.

    The `weight` attribute determines the order in which web items appear. Items are displayed top to bottom or
    left to right in order of ascending weight. The lightest weight is displayed first, the heaviest weights
    sink to the bottom.

    The weights for most applications' system sections start from 100, and the weights for the links generally
    start from 10. The weight is incremented by 10 for each in sequence so that there is ample space to
    insert your own sections and links.

1.  Add the following `web-item`:

    ``` xml
    <web-item key="my_links_link" name="Link on My Links Main Section" section="system.top.navigation.bar" weight="47">
        <label>My Company</label>
        <link linkId="my_links_link">http://www.atlassian.com</link>
    </web-item>
    ```

    This defines the link on the top-level section with text "My Company". We have pointed
    the link at the company website.
    The `linkId` is required and must be the same as the location element of the web-section. It also provides an XML
    ID for the link being generated.

1.  Add the following `web-item`:  

    ``` xml
    <web-item key="website_link" name="Company Web Site" section="my_links_link/my_links_section" weight="10">
        <label>Web Site</label>
        <link linkId="website_link">http://www.atlassian.com</link>
    </web-item>
    ```

    This creates a single menu item with text "Web Site" that links to your company website.

1.  Add the following `web-item`:  

    ``` xml
    <web-item key="documentation_link" name="Documentation Web Site" section="my_links_link/my_links_section" weight="10">
        <label>Documentation</label>
        <link linkId="documentation_link">http://confluence.atlassian.com</link>
    </web-item>
    ```

    This creates a menu item labeled "Documentation" that links to your documentation website.

1.  Save the file.

Your `atlassian-plugin.xml` file should look something like this:

``` xml
<?xml version="1.0" encoding="UTF-8"?>
<atlassian-plugin key="${atlassian.plugin.key}" name="${project.name}" plugins-version="2">
    <plugin-info>
        <description>${project.description}</description>
        <version>${project.version}</version>
        <vendor name="${project.organization.name}" url="${project.organization.url}"/>
        <param name="plugin-icon">images/pluginIcon.png</param>
        <param name="plugin-logo">images/pluginLogo.png</param>
    </plugin-info>
    <!-- add our i18n resource -->
    <resource type="i18n" name="i18n" location="jira-menu-items"/>
    <!-- add our web resources -->
    <web-resource key="jira-menu-items-resources" name="jira-menu-items Web Resources">
        <dependency>com.atlassian.auiplugin:ajs</dependency>
        <resource type="download" name="jira-menu-items.css" location="/css/jira-menu-items.css"/>
        <resource type="download" name="jira-menu-items.js" location="/js/jira-menu-items.js"/>
        <resource type="download" name="images/" location="/images"/>
        <context>jira-menu-items</context>
    </web-resource>
    <web-section name="My Links Main Section" i18n-name-key="my-links-main-section.name" key="my_links_section"
                 location="my_links_link" weight="10"/>
    <web-item key="my_links_link" name="Link on My Links Main Section" section="system.top.navigation.bar" weight="47">
        <label>My Company</label>
        <link linkId="my_links_link">http://www.atlassian.com</link>
    </web-item>
    <web-item key="website_link" name="Company Web Site" section="my_links_link/my_links_section" weight="10">
        <label>Web Site</label>
        <link linkId="website_link">http://www.atlassian.com</link>
    </web-item>
    <web-item key="documentation_link" name="Documentation Web Site" section="my_links_link/my_links_section"
              weight="10">
        <label>Documentation</label>
        <link linkId="documentation_link">http://confluence.atlassian.com</link>
    </web-item>
</atlassian-plugin>
```

## Step 5. Reload your app and test it

1.  In a Terminal window, run the `atlas-package` command that triggers [QuickReload](/server/framework/atlassian-sdk/automatic-plugin-reinstallation-with-quickreload/).
1.  Wait until Jira reinstalls your app. In Terminal output you will see many lines concluding with some thing like this:

    ``` bash
    [INFO] [talledLocalContainer]     Quick Reload Finished (791 ms)
    ```

1.  Refresh your browser page to see changes.
1.  Notice the new menu in the header. Clicking on the entries takes you to the URLs you entered.

    <img src="/server/jira/platform/images/jiramenuitems2.png" width="650" />

{{% tip %}}

Congratulations, that's it!

Have a treat!

{{% /tip %}}

## Next steps

When you finish developing and testing your app, you can install it into your company's Jira instance so that
the new menu items are available for other people to use.

Your app JAR file is located at `target\jira-menu-items-1.0.jar`. You can install the app using the
[Universal Plugin Manager](https://confluence.atlassian.com/display/UPM/Installing+Add-ons). 

### Related topics

* [Web fragments](/server/jira/platform/web-fragments-4227124.html)  
* [Web Section plugin module](/server/jira/platform/web-section)  
* [Web Item plugin module](/server/jira/platform/web-item)
* [Jira Webwork actions](/server/jira/platform/jira-webwork-actions-4227099.html)
