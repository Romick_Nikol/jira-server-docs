---
aliases:
- /server/jira/platform/4227208.html
- /server/jira/platform/4227208.md
category: devguide
confluence_id: 4227208
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=4227208
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=4227208
date: '2017-12-08'
legacy_title: How to set a custom field value on an issue?
platform: server
product: jira
subcategory: other
title: How to set a custom field value on an issue?
---
# How to set a custom field value on an issue?

Confusingly, the method `setCustomFieldValue` on the <a href="http://docs.atlassian.com/software/jira/docs/api/latest/com/atlassian/jira/issue/MutableIssue.html" class="external-link"><code>MutableIssue</code></a> does **not** actually set the custom field value of an issue. To update a custom field value on an issue, you'd need to use the method `updateValue` on the <a href="http://docs.atlassian.com/software/jira/docs/api/latest/com/atlassian/jira/issue/fields/CustomField.html" class="external-link"><code>CustomField</code></a> object itself.

Since JIRA 3.4.x the updateValue method signature has changed to:

``` javascript
/**
* Update the issue in the data store
*
* @param fieldLayoutItem for this field within this context
* @param issue Issue this field is part of
* @param modifiedValue new value to set field to. Cannot be null.
* @param issueChangeHolder an object to record any changes made to the issue by this method.
*/
void updateValue(FieldLayoutItem fieldLayoutItem, Issue issue, ModifiedValue modifiedValue,
                 IssueChangeHolder issueChangeHolder);
```

The fieldLayoutItem is used to determine whether the wiki renderer is used or not and can be null **for non-renderable fields only**. The `issueChangeHolder` is used to store and then possibly write the change history. You can simply replace this with `new DefaultIssueChangeHolder` and it should still work out.

For example, here is some code that updates a "Timestamp" non-wiki text custom field on issue "TP-14":

``` javascript
IssueManager issueManager = ComponentManager.getInstance().getIssueManager();
CustomFieldManager cfManager = ComponentManager.getInstance().getCustomFieldManager();
MutableIssue issue = issueManager.getIssueObject("TP-14");
CustomField cf = cfManager.getCustomFieldObjectByName("Timestamp");
IssueChangeHolder changeHolder = new DefaultIssueChangeHolder();
cf.updateValue(null, issue, new ModifiedValue(issue.getCustomFieldValue(cf), ""+new java.util.Date()),changeHolder);
System.out.println(changeHolder.getChangeItems().get(0));
```
