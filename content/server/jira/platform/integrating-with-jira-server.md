---
aliases:
- /server/jira/platform/jira-platform-32344049.html
- /server/jira/platform/jira-platform-32344049.md
category: devguide
confluence_id: 32344049
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=32344049
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=32344049
date: '2017-12-08'
legacy_title: JIRA platform
platform: server
product: jira
subcategory: intro
title: Integrating with Jira Server
---
# Integrating with Jira Server

Welcome to Jira Server development! This overview will cover everything you need to know to integrate with Jira Server. This includes the P2 framework for building plugins, as well as Jira features and services that you can use when integrating with Jira.

{{% tip %}}

Hello world

If you already know the theory and want to jump straight into development, read our [Getting started guide](/server/jira/platform/getting-started) to build your first Jira Server plugin.

{{% /tip %}}

We recommend that you read this entire page if you are new to Jira Server development, but if you want to skip to a specific section, use the links below:

-   [Developing with Plugins2](#developing-with-plugins2): The framework for building plugins for Jira Server
-   [Modules](#modules): Integration points for interacting with Jira, including the Jira UI
-   [Entity properties](#entity-properties): Key-values stores in Jira
-   [Jira APIs](#jira-apis): This includes REST APIs for the Jira platform and the Jira products
-   [Webhooks](#webhooks): User-defined callbacks over HTTP
-   [Security](#security): Authentication and authorization
-   [Jira Data Center](#jira-data-center): Tools for working with the Jira UI, like the Atlassian Connect JS API, conditions, and context parameters.

## Building a plugin

If you want to build a plugin for Jira Server, then you will be using the Atlassian  development platform via the [Atlassian SDK](https://developer.atlassian.com/display/DOCS/Getting+Started). The platform consists of the Plugins2 framework (also known as the P2 framework) and a set of components that provide useful tools to developers, as shown below. 

![](/server/jira/platform/images/plugindevelopmentplatform.png)

*For more information on the components in this platform, see [Atlassian Platform Common Components](https://developer.atlassian.com/display/DOCS/Atlassian+Platform+Common+Components).*

A Jira Plugins2  is a bundle of code, resources and configuration files that can be installed in Jira. Fundamentally, s can do following:

-   Add content or features, like pages, panels, reports, JQL functions, gadgets, in certain defined places in the Jira UI via modules.
-   Make secure requests to Jira's APIs.
-   Listen and respond to webhooks fired by the Atlassian application.

For more information, see [Jira Plugins2 overview](/server/jira/platform/plugins2-add-ons). 

##  Developing with Plugins2

If you are building a plugin with Plugins2, you'll also need to understand a number of key development processes:

-   **Implementing security:** Implementing security is a crucial part of integrating with Jira. Robust authentication and authorization help prevent malicious access to customer data. To learn more about authentication and authorization for Jira Server, see the [Security overview](/server/jira/platform/security-overview).
-   **Listing your plugin on the Atlassian Marketplace and implementing licensing:** Listing your plugin on the Atlassian Marketplaces and licensing it are necessary, if you want to make your plugin available to the public. See the [Marketplace documentation](https://developer.atlassian.com/display/HOME/Marketplace) for more instructions.
-   **Designing your plugin:** Since Plugins2 plugins can insert content directly into the Atlassian host application, it is critical that plugins are visually compatible with the Atlassian application's design. Our designers and developers have created a number of resources to help you with this:
    -   <a href="https://design.atlassian.com/product/" class="external-link">Atlassian Design Guidelines</a> -- Our design guidelines define core interactions with the Atlassian applications.
    -   <a href="https://docs.atlassian.com/aui/" class="external-link">Atlassian User Interface (AUI)</a> -- AUI is a library of reusable front-end UI components.

## Modules

Modules are the most important component of your Jira plugin or integration. Simply put, these are the integration points that your plugin uses to provide rich interactions with Jira. There are two types of modules: basic iframes that allow you to display content in different places in Jira, and more advanced modules that let you provide advanced Jira-specific functionality. Jira Service Desk and Jira Software also have their own application-specific modules (UI-related only).

For more information, see [About Jira modules](/server/jira/platform/about-jira-modules).

## Entity properties

Entity properties are key-value stores attached to Jira objects, that can be created, updated, and deleted via the Jira REST APIs. This is a powerful system for storing data on the Jira host; it easily support imports, exports, and migrations between instances because the data is stored locally with the Jira instance. Here's how entity properties can help you with your Jira Server integration:

-   Simplify your plugin or integration by reducing how much data you store on your own server.
-   Improve performance by evaluating `entity_property_equal_to` conditions in-process.
-   Implement advanced features like JQL integration using search extractions.

To learn more about Jira entity properties, see [entity properties](/server/jira/platform/entity-properties). 

## Jira APIs

Use the Jira APIs to interact with Jira from your integration. You should also read the [Security overview](/server/jira/platform/security-overview) to learn how to secure any interactions with the Jira APIs.

### REST APIs

You should use the REST APIs for most integrations. Each of the Jira Server applications have a REST API that allows you to interact with the objects in that application (e.g. sprints for Jira Software). The Jira Server platform REST API is common to all Jira products.

-   <a href="https://docs.atlassian.com/jira/REST/server/" class="external-link">Jira Server platform REST API (latest)</a>
-   <a href="https://docs.atlassian.com/jira-software/REST/server/" class="external-link">Jira Software Server REST API (latest)</a>
-   <a href="https://docs.atlassian.com/jira-servicedesk/REST/server/" class="external-link">Jira Service Desk Server REST API (latest)</a>

To learn more, see [About the Jira Server REST APIs](/server/jira/platform/rest-apis). See the Webhooks section below as well.

### Java APIs

The Java APIs are primarily used when building a P2 plugin. Similar to the REST APIs, each of the Jira Server applications have a Java API.

-   <a href="https://docs.atlassian.com/jira/server/" class="external-link">Jira Server platform Java API (latest)</a>
-   <a href="https://docs.atlassian.com/greenhopper/" class="external-link">Jira Software Server Java API (all versions)</a>
-   <a href="https://docs.atlassian.com/jira-servicedesk/server/" class="external-link">Jira Service Desk Server Java API (latest)</a>

## Webhooks

Webhooks are outgoing messages from Jira that allow your plugin or integration to react to events, like someone transitioning an issue or closing a sprint. Like the REST APIs, there is a set of platform-level webhooks and additional advanced webhooks for Jira Software and Jira Service Desk.

To learn more about webhooks, see [Webhooks](/server/jira/platform/webhooks). 

## Jira Data Center

Jira Data Center is the enterprise implementation of Jira, providing high availability and performance at scale. If you are developing for Jira Data Center, you need to be aware of the guidelines for building plugins for high availability and clustered environments.

To learn more, see [About Jira Data Center](/server/jira/platform/about-jira-data-center).

## Get started

Time to get hands-on with Jira Server development! Read our [Getting started guide](/server/jira/platform/getting-started) to learn how to set up a development environment and build a Jira Server plugin.
