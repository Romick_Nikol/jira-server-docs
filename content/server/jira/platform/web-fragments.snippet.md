---
aliases:
- /server/jira/platform/--web-fragments-4227175.html
- /server/jira/platform/--web-fragments-4227175.md
category: devguide
confluence_id: 4227175
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=4227175
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=4227175
date: '2017-12-08'
legacy_title: __Web_Fragments
platform: server
product: jira
subcategory: other
title: Web Fragments
---
# Web Fragments

-   [\_\_Existing\_Issue\_Operations\_Bar\_Web\_Sections](/server/jira/platform/existing-issue-operations-bar-web-sections.snippet)
-   [\_\_Existing\_User\_Name\_Drop\_Down\_Web\_Sections](/server/jira/platform/existing-username-dropdown-web-sections.snippet)
-   [\_\_Issue\_Operations\_Bar](/server/jira/platform/issue-operations-bar.snippet)
-   [\_\_Project Configuration Administration Summary Tab](/server/jira/platform/project-configuration-administration-summary-tab.snippet)
-   [\_\_Section\_Attribute\_Values\_for\_Web\_Items\_in\_the\_Dashboard\_Drop-Down](/server/jira/platform/section-attribute-values-for-web-items-in-the-dashboard-drop-down.snippet)
















