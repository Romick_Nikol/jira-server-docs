---
aliases:
- /server/jira/platform/how-to-create-a-new-custom-field-type-c-23299441.html
- /server/jira/platform/how-to-create-a-new-custom-field-type-c-23299441.md
category: devguide
confluence_id: 23299441
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=23299441
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=23299441
date: '2017-12-08'
legacy_title: How to create a new custom field type - C
platform: server
product: jira
subcategory: other
title: How to create a new custom field type - C
---
# How to create a new custom field type - C

Level of experience

BEGINNER

In this tutorial:

 

Time estimate

0:30

Atlassian application

JIRA 4.X+

## Get an overview of the tutorial before you begin

In this tutorial, you create a plugin that adds a new JIRA custom field to the **Create New User** dialog.  Your new field will be editable only by system administrators.  The finished field appears as follows:

<img src="/server/jira/platform/images/admin-create-issue.png" width="500" />

In creating this plugin you'll learn:

-   What kinds of custom fields you can create.
-   How to extend the GenericTextCFT custom field type.
-   How to use the `customfield-type` plugin module type for JIRA.

**Prerequisite knowledge**

You must have an installed version of the Atlassian Plugin SDK. You should be able to complete this tutorial even if you've never created a plugin before.  Familiarity with Java development and JIRA is also helpful. 

**Getting the plugin source code**

We encourage you to work through the tutorial. If you'd like to skip ahead or check your work when you're done, you can find the plugin source on Atlassian Bitbucket. Bitbucket serves as a public Git repository containing the tutorial's source code. To clone the repository for this plugin, issue the following command: 

    $ git clone https://rbuchanan@bitbucket.org/atlassian_tutorial/jira-custom-field-tutorial.git

If you don't have Git, you can <a href="https://bitbucket.org/atlassian_tutorial/jira-custom-field-tutorial/get/master.zip" class="external-link">download the source code</a> for this plugin project.

{{% note %}}

About these Instructions

You can use any supported combination of OS and IDE to create this plugin. These instructions were written using Eclipse Indigo on Mac OS X. If you are using another OS or IDE combination, you should use the equivalent operations for your specific environment.

This tutorial was last tested with **JIRA 6.1.2.** 

{{% /note %}}

## Step 1. Create the plugin project and delete unnecessary files

In this step, you'll generate a JIRA plugin skeleton using the `atlas-create-jira-plugin` command. The command creates a directory to hold your plugin and generates the skeleton files into the directory. Some of the skeleton files you won't need for this tutorial, so you'll delete the unnecessary files in this step also.

1.  Open a terminal window on your local machine.
2.  Change to a directory where you can work on your plugin files.  
    If you are using Eclipse (or equivalent IDE), use your workspace directory. 
3.  Create a JIRA plugin skeleton by entering the following command:

        $ atlas-create-jira-plugin

4.  When prompted, enter the following information to identify your plugin: 

    <table>
    <colgroup>
    <col style="width: 50%" />
    <col style="width: 50%" />
    </colgroup>
    <tbody>
    <tr class="odd">
    <td><p>group-id</p></td>
    <td><pre><code>com.atlassian.jira.plugin.customfield.example</code></pre></td>
    </tr>
    <tr class="even">
    <td><p>artifact-id</p></td>
    <td><p>jira-custom-field-example</p></td>
    </tr>
    <tr class="odd">
    <td><p>version</p></td>
    <td><p><code>1.0-SNAPSHOT</code></p></td>
    </tr>
    <tr class="even">
    <td><p>package</p></td>
    <td><p><code>com.example.plugins.tutorial.confluence</code></p></td>
    </tr>
    </tbody>
    </table>

5.  When prompted to confirm your entries, press `Y` or `y`.

    The command builds the plugin skeleton and notifies you of a successful build:

        [INFO] ------------------------------------------------------------------------
        [INFO] BUILD SUCCESSFUL
        [INFO] ------------------------------------------------------------------------
        [INFO] Total time: 1 minute 11 seconds
        [INFO] Finished at: Thu Aug 28 11:30:23 PDT 2013
        [INFO] Final Memory: 82M/217M
        [INFO] ------------------------------------------------------------------------

6.  Change to the `jira-custom-field-example` directory you created in the previous step.

        $ cd jira-custom-field-example

    The directory contains several test directories you don't use in this tutorial.

7.  Delete the test directories.  
    Use the following commands to delete the test directories. 

        $ rm -rf ./src/test/java
        $ rm -rf ./src/test/resources/

    The generation step created some Java class files that are also unnecessary. 

8.  Delete the unnecessary Java files. 

        rm ./src/main/java/com/atlassian/jira/plugin/customfield/example/*.java

    You'll build your own Java class for your plugin later on in this tutorial.

9.  Edit the `src/main/resources/atlassian-plugin.xml` descriptor file.  
    This file was generated for you and contains some unneeded declarations. 
10. Locate the `myPluginComponent ` declaration.  

    ``` javascript
        <!-- publish our component -->
        <component key="myPluginComponent" class="com.atlassian.jira.plugin.customfield.example.MyPluginComponentImpl" public="true">
            <interface>com.atlassian.jira.plugin.customfield.example.MyPluginComponent</interface>
        </component>
    ```

11. Delete the declaration.
12. Check your work.

    ``` javascript
    <atlassian-plugin key="${project.groupId}.${project.artifactId}" name="${project.name}" plugins-version="2">
        <plugin-info>
            <description>${project.description}</description>
            <version>${project.version}</version>
            <vendor name="${project.organization.name}" url="${project.organization.url}" />
            <param name="plugin-icon">images/pluginIcon.png</param>
            <param name="plugin-logo">images/pluginLogo.png</param>
        </plugin-info>
        <!-- add our i18n resource -->
        <resource type="i18n" name="i18n" location="jira-custom-field-example"/>
        
        <!-- add our web resources -->
        <web-resource key="jira-custom-field-example-resources" name="jira-custom-field-example Web Resources">
            <dependency>com.atlassian.auiplugin:ajs</dependency>
            
            <resource type="download" name="jira-custom-field-example.css" location="/css/jira-custom-field-example.css"/>
            <resource type="download" name="jira-custom-field-example.js" location="/js/jira-custom-field-example.js"/>
            <resource type="download" name="images/" location="/images"/>
            <context>jira-custom-field-example</context>
        </web-resource>
        
        <!-- import from the product container -->
        <component-import key="applicationProperties" interface="com.atlassian.sal.api.ApplicationProperties" />
        
    </atlassian-plugin>
    ```

13. Save and close the `atlassian-plugin.xml`  file.  

### (Optional) Import your project into your IDE 

 

1.   Make your project available to Eclipse.

        atlas-mvn eclipse:eclipse

    You'll see a successful build message: 

        [INFO] ------------------------------------------------------------------------
        [INFO] BUILD SUCCESSFUL
        [INFO] ------------------------------------------------------------------------
        [INFO] Total time: 54 seconds
        [INFO] Finished at: Tue Jul 16 11:03:59 PDT 2013
        [INFO] Final Memory: 82M/224M
        [INFO] ------------------------------------------------------------------------

2.  Start Eclipse.  
    You can open a new terminal window to perform this action.

        $ cd ~/eclipse
        $ ./eclipse

3.  Click **File &gt; Import**.   
    Eclipse starts the **Import** wizard.
4.  Expand the **General** folder tree to choose **Existing Projects into Workspace**.
5.  Click **Next. ** **  
    **
6.  Click **Browse **and enter the root directory of your workspace.  
    Your Atlassian plugin folder should now appear under **Projects.**
7.  Ensure your plugin checkbox is ticked and click **Finish**.   
    Eclipse imports your project, and it's now visible in your Package Explorer view.

## Step 3. Create a Java class for your field

You'll write a Java class to process and manage your custom field data. All custom fields ultimately derive from the `CustomFieldType` interface.  This interface provides methods to store and retrieve custom fields values from the database.

Atlassian provides several implementations of `CustomFieldType` for the common field types. For example, there is the `MultiSelectCFType`, `DateTimeCFType`, `NumberCFType`, and so forth. (For a complete list of implementations, see the <a href="http://docs.atlassian.com/software/jira/docs/api/latest/com/atlassian/jira/issue/customfields/CustomFieldType.html" class="external-link">CustomFieldType</a> interface Javadoc.) In this tutorial, you'll extend the `GenericTextCFType` class to add a text field.

On your local machine, do the following:

1.  Create a class called `JiraCustomField.java` in the `src/main/java/com/atlassian/jira/plugin/customfield/example/` directory.
2.  Edit the class file in Eclipse or your favorite editor.
3.  Add the following code to your class:

    ``` javascript
    package com.atlassian.jira.plugin.customfield.example;

    import com.atlassian.jira.issue.customfields.impl.GenericTextCFType;
    import com.atlassian.jira.issue.customfields.manager.GenericConfigManager;
    import com.atlassian.jira.issue.customfields.persistence.CustomFieldValuePersister;


    public class JiraCustomField extends GenericTextCFType {

        public JiraCustomField(CustomFieldValuePersister customFieldValuePersister,
                GenericConfigManager genericConfigManager) {

            super(customFieldValuePersister, genericConfigManager);
        }


    }
    ```

4.  Save and close the `JiraCustomField` class file.
5.  If you are using Eclipse, update your project changes.

        $ atlas-mvn eclipse:eclipse

## Step 3. Create a Velocity template to display your custom field

You use a velocity template to specify how to display your custom field to users. The velocity template can also contain logic for a field.  In this tutorial, you only want to administrators the ability to edit your custom field. So, in this step, you'll create add logic that:

-   Allows alll users to see the field's value. 
-   Display `N/A` as the default value.   
-   Allows edits if the current user belongs to the **jira-administrators** group.
-     
-   Create a `templates` directory under `src/main/resources`.

  

1.  Make sure you are in the plugin directory root.
2.  Create a `src/main/resources/templates` directory.
3.  Create a  

    $ mkdir src/main/resources/templates

 

1.  Create a `edit-jiraadminonlytext.vm` file under your new `templates` directory: 

        $ cat > src/main/resources/templates/edit-jiraadminonlytext.vm

2.  Copy and paste the following onto the command line.

    Here you'll add the conditional check to see if the user is an admin. If no value exists, the user will see "N/A".

    ``` javascript
    #controlHeader ($action $customField.id $customField.name $fieldLayoutItem.required $displayParameters.noHeader)
    #if ($jiraUserUtils.getGroupNamesForUser($authcontext.loggedInUser.name).contains('jira-administrators'))
        <input type="text"
        name="$customField.id"
        value="$!value" />
    #else
    #if($value && ! $value.equals(""))
    #set ($displayValue = ${value})
    #else
    #set ($displayValue = 'N/A')
    #end
    <span title="This field is editable only by JIRA administrators">$!displayValue</span>
        <input type="hidden"
        name="$customField.id"
        value="$!value" />
    #end
    #controlFooter ($action $fieldLayoutItem.fieldDescription $displayParameters.noHeader)
    ```

3.  Hit **CTRL + D ** to save and exit the VM editor.

## Step 4. Add a `customfield-type` plugin module to your descriptor file

In this step you'll complete your plugin code with the addition of a `customfield-type` plugin module. You'll add this module to the `atlassian-plugin.xml` descriptor file. The descriptor file was automatically generated when you built your plugin skeleton, and describes dependencies to the Atlassian system. Inserting a `customfield-type` plugin module lets you add your custom field to JIRA. This module requires a Java class for implementation and a unique key identifier. You'll also reference your Velocity template as a resource type element. There are four view types for any custom field, three of which are required: 

| View type     | Required?                                                                                                 | Details                                                                                                                                               |
|---------------|-----------------------------------------------------------------------------------------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------|
| `view`        | <img src="/server/jira/platform/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" />   | Provides a basic read-only view of the field value.                                                                                                   |
| `column-view` | <img src="/server/jira/platform/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /> | Read-only view for displaying in the issue navigator. The issue navigator will display the `view` value if omitted.                                   |
| `edit`        | <img src="/server/jira/platform/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" />   | Renders the edit widget in issue creation, editing issues, and editing defaults. This is the value you'll replace with your custom Velocity template. |
| `xml`         | <img src="/server/jira/platform/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" />   | Shows the value in XML format for RSS or XML exports.                                                                                                 |

You'll use two built-in Velocity templates in JIRA for the `view` and `xml` view types, and your custom template for the `edit` value. 

1.  Edit `atlassian-plugin.xml` under `src/main/resources`.  
      
2.  Locate the closing `</atlassian-plugin>` tag.   
      
3.  Add the opening and closing tags for your `customfield-type.`

    Place the block before the end of the closing `</atlassian-plugin>` tag. You'll give your custom field a unique key, name, and define `JiraCustomField` as the implementing Java class.

    ``` javascript
       <customfield-type key="admintextfield" name="Admin Editable Text Field" class="com.atlassian.jira.plugin.customfield.example.JiraCustomField">
        </customfield-type>
    ```

4.  Add a description inside the first `customfield-type` tag.   
    This description appears in JIRA when you apply the custom field type from the admin pages. 

    ``` javascript
           <description>A text field only editable by those with admin permissions</description>
    ```

      

5.  Define which Velocity templates to use for each view type:  

    ``` javascript
           <resource type="velocity" name="view" location="templates/plugins/fields/view/view-basictext.vm"/>
            <resource type="velocity" name="edit" location="templates/edit-jiraadminonlytext.vm"/>
            <resource type="velocity" name="xml" location="templates/plugins/fields/xml/xml-basictext.vm"/>
    ```

      

6.  Save and close your descriptor file.
7.  Update your project changes.

        $ atlas-mvn eclipse:eclipse

      

8.  From your project root, issue the following command: 

        $ atlas-create-jira-plugin-module

Here's the completed `atlassian-plugin.xml` descriptor file:

``` javascript
<atlassian-plugin key="${project.groupId}.${project.artifactId}" name="${project.name}" plugins-version="2">
  <plugin-info>
    <description>${project.description}</description>
    <version>${project.version}</version>
    <vendor name="${project.organization.name}" url="${project.organization.url}"/>
    <param name="plugin-icon">images/pluginIcon.png</param>
    <param name="plugin-logo">images/pluginLogo.png</param>
  </plugin-info>
  <!-- add our i18n resource -->
  <resource type="i18n" name="i18n" location="jira-custom-field-example"/>
  <!-- add our web resources -->
  <web-resource key="jira-custom-field-example-resources" name="jira-custom-field-example Web Resources">
    <dependency>com.atlassian.auiplugin:ajs</dependency>
    <resource type="download" name="jira-custom-field-example.css" location="/css/jira-custom-field-example.css"/>
    <resource type="download" name="jira-custom-field-example.js" location="/js/jira-custom-field-example.js"/>
    <resource type="download" name="images/" location="/images"/>
    <context>jira-custom-field-example</context>
  </web-resource>
  <!-- import from the product container -->
  <component-import key="applicationProperties" interface="com.atlassian.sal.api.ApplicationProperties"/>
    <customfield-type key="admintextfield" name="Admin Editable Text Field" class="com.atlassian.jira.plugin.customfield.example.JiraCustomField">
        <description>A text field only editable by those with admin permissions</description>
        <resource type="velocity" name="view" location="templates/plugins/fields/view/view-basictext.vm"/>
        <resource type="velocity" name="edit" location="templates/edit-jiraadminonlytext.vm"/>
        <resource type="velocity" name="xml" location="templates/plugins/fields/xml/xml-basictext.vm"/>
    </customfield-type>
</atlassian-plugin>
```

 

## Step 5. Build, install, and run your plugin

In this step you'll install your plugin and run JIRA. You'll log into your JIRA instance as an admin and configure visibility for your custom field. 

1.  Open a terminal and navigate to your project root. 

        $ cd jira-custom-field-example

2.  Start JIRA. 

        $ atlas-run

    This builds your plugin code, starts a JIRA instance, and installs your plugin. This could take a few minutes.

3.  Locate the URL for JIRA.   
    Your terminal outputs the location of your local JIRA instance, defaulted to <a href="http://localhost:2990/jira." class="external-link">http://localhost:2990/jira</a>

        [INFO] jira started successfully in 194s at http://localhost:2990/jira
        [INFO] Type Ctrl-D to shutdown gracefully
        [INFO] Type Ctrl-C to exit

4.  Navigate to your JIRA instance in a browser window.  
    We recommend using Google Chrome or Mozilla Firefox for consistency.
5.  Log in with **admin / admin**.
6.  Once logged in, navigate to the custom field options under ![](/server/jira/platform/images/image2013-8-26-12:48:49.png) &gt; **Issues &gt; Custom Fields**. 
7.  Click **Add Custom Field** and choose **Advanced**.
8.  Choose your plugin, **Admin Editable Text Field** from the list and click **Next**.  
    <img src="/server/jira/platform/images/choose-your-plugin.png" alt="Choose your plugin and click next" width="500" />
9.  Enter details as prompted.   
    Choose **Any issue type** for Choose applicable issue types, and **Global context** to apply to all issues in JIRA.   
    <img src="/server/jira/platform/images/enter-field-name-details.png" alt="Add custom field details" width="500" />
10. Associate the field with all screens and click **Next.**  
    **<img src="/server/jira/platform/images/associate-fields.png" alt="Associate all fields and click next" width="500" />  
    **

## Step 6. Create a test user and test your custom field type

Now that you've added your custom field, let's put it to the test. You'll create a new user without admin permissions so you can verify your field settings. 

1.  From any page in your local JIRA instance, click  ![](/server/jira/platform/images/image2013-8-26-12:48:49.png) &gt; **Users.**  
    **  
    **
2.  Click **Create User** and enter details for a test user.   
    Keep it simple and use your own email address. Here, we'll use **testuser** as both the username and password.  
    <img src="/server/jira/platform/images/create-a-test-user.png" alt="create a test user and click next" width="500" />
3.  Next, create a project.  
    Click **Projects &gt; Create Project**.  
    Now you'll be able to create an issue to test your field.
4.  Create an issue for your project.  
    Click **Create Issue **and verify you can enter text into your custom field type as an admin.  
    <img src="/server/jira/platform/images/admin-create-issue.png" alt="Create an issue as an admin" width="500" />
5.  Log out and change to your test user credentials.  
    Click **avatar &gt; Log out** and use **testuser/testuser** to log back in.
6.  Create an issue as your test user.   
    Verify that you're unable to modify the field you customized. You should see "N/A" or any other default value you entered.  
    <img src="/server/jira/platform/images/create-issue-non-admin.png" alt="create an issue as a non-admin" width="500" />
