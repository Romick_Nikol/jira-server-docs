---
title: JIRA REST API Example Add Comment 8946422
aliases:
    - /server/jira/platform/jira-rest-api-example-add-comment-8946422.html
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=8946422
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=8946422
confluence_id: 8946422
platform: server
product: jira
category: devguide
subcategory:
date: "2018-04-25"
---
# JIRA Developer Documentation : JIRA REST API Example - Add Comment

<table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<tbody>
<tr class="odd">
<td><p>Applicable:</p></td>
<td><p>JIRA 5.0 and later.</p></td>
</tr>
</tbody>
</table>

 

The Rest API allows you to add edit and remove issue comments.

Comments may be added either via the `rest/api/2/issue/{issuekey}/comment` resource or as part of editing an issue.

{{% note %}}

The examples shown here use curl with an input file denoted by the "--data @filename" syntax and the file data is shown separately

{{% /note %}}

# Adding a comment with the "comment" resource.

This resource simply adds a comment and nothing else.
(It is also possible to add a comment as a side-effect of another operation like "edit issue" or "transition issue").
This resource is particularly useful if the logged in user may not have "edit" permission for the issue, but does have the "add comment" permission.

### Basic example

##### Request

``` javascript
curl -D- -u fred:fred -X POST --data {see below} -H "Content-Type: application/json" http://kelpie9:8081/rest/api/2/issue/QA-31/comment
```

example input data

``` javascript
{
    "body": "This is a comment regarding the quality of the response."
}
```

##### Response

You should just receive a response with a status of "201" with the full json representation of the added comment

### Example also setting the security level

##### Request

``` javascript
curl -D- -u fred:fred -X POST --data {see below} -H "Content-Type: application/json" http://kelpie9:8081/rest/api/2/issue/QA-31/comment
```

example input data

``` javascript
{
    "body": "This is a comment that only administrators can see.",
    "visibility": {
        "type": "role",
        "value": "Administrators"
    }
}
```

# Adding a comment when editing an issue

Comments can also be added using the normal edit operations

### Basic example using Edit Issue

Here we update the description and add a comment saying why.

{{% note %}}

Only one comment can be added at a time when updating an issue via this resource.

{{% /note %}}

##### Request

``` javascript
curl -D- -u fred:fred -X PUT --data {see below} -H "Content-Type: application/json" http://kelpie9:8081/rest/api/2/issue/QA-31
```

example input data

``` javascript
{
   "update": {
      "comment": [
         {
            "add": {
               "body": "It is time to finish this task"
            }
         }
      ]
   }
}
```

### Example setting the description and also adding a comment using Edit Issue

Here we update the description and add a comment saying why.

##### Request

``` javascript
curl -D- -u fred:fred -X PUT --data {see below} -H "Content-Type: application/json" http://kelpie9:8081/rest/api/2/issue/QA-31
```

example input data

``` javascript
{
   "update": {
   "description": [
         {
            "set": "JIRA should also come with a free pony"
         }
      ],
      "comment": [
         {
            "add": {
               "body": "This request was originally written in French, which most of our developers can't read"
            }
         }
      ]
   }
}
```
