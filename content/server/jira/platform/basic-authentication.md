---
aliases:
- /server/jira/platform/basic-authentication-52430797.html
- /server/jira/platform/basic-authentication-52430797.md
category: devguide
confluence_id: 52430797
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=52430797
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=52430797
date: '2019-03-06'
legacy_title: Basic authentication
platform: server
product: jira
subcategory: security
title: "Basic authentication"
---

# Basic authentication

This page shows you how to make REST requests to Jira Server using basic authentication with a 
username and password.

## Overview

Jira’s REST API is protected by the same restrictions that are provided via Jira’s standard web 
interface. This means that if you do not log in, you are accessing Jira anonymously. Furthermore, 
if you log in and do not have permission to view something in Jira, you will not be able to view it 
using the Jira REST API either.

### When to use basic authentication

[Basic authentication](https://en.wikipedia.org/wiki/Basic_access_authentication) provides a simple 
mechanism to do authentication when experimenting with the REST API, writing a personal script, or 
for use by a bot. However, as basic authentication repeatedly sends the username and password on 
each request, which could be cached in the web browser, it is not the most secure method of authentication
we support.

We recommend you use [OAuth](/server/jira/platform/oauth/) over basic authentication for most 
cases. OAuth requires more work to implement, but it uses a token-based workflow that is much more 
secure.

## Before you begin

To send a request using basic authentication, you'll need the following:

* A Jira Server site; ours is running at `localhost:8080`
* The username and password of a user who has permission to create issues on your Jira Server site
* Terminal on Mac or Linux, Command Prompt on Windows. 

We will use the cURL command in Terminal or Command Prompt, but any other tool to send requests can
be used. Note, if you are using a version of Windows prior to Windows 10 you will need to install [cURL](https://curl.haxx.se/download.html).

## Send a request

Most client software for sending requests automatically generate the authorization header when you
provide a username and password.

For example, in cURL you can specify a username and password combination with the `-u` argument to
send a request to a Jira site as follows:

``` bash
curl -u username:password -X GET -H "Content-Type: application/json" http://localhost:8080/rest/api/2/issue/createmeta
```

## Construct the authorization header

If you need, you can construct and send the basic authorization header yourself as follows:

1.  Build a string of the form `username:password`.
1.  Encode the string to Base64.
1.  Supply an authorization header with format `Authorization: Basic {encoded-string}`. Make sure to
replace `{encoded-string}` with your encoded string from _Step 2_.

For example, if your username and password are both _fred_ then the string _"fred:fred"_ encodes
to _ZnJlZDpmcmVk_ in Base64. You can then make a request with cURL specifying the authorization
header with `-H` as follows:

``` bash
curl -H "Authorization: Basic ZnJlZDpmcmVk" -X GET -H "Content-Type: application/json" http://localhost:8080/rest/api/2/issue/createmeta
```

## Important considerations

Once you’re comfortable making simple requests, there are a couple of topics you may need to consider
further for your use-case.

### No authentication challenge

Because Jira permits a default level of access to anonymous users, it does not supply a typical 
authentication challenge. 

Some HTTP client software expect to receive an authentication challenge before they send an 
authorization header. This means that the software may not behave as expected. In this case, you may need to 
configure the software to supply the authorization header, as described above, rather than rely on its default 
mechanism.

### CAPTCHA

CAPTCHA is a tool that can distinguish a human being from an automated agent such as a web spider 
or robot. CAPTCHA is triggered after several consecutive failed login attempts, after which the user 
is required to interpret a distorted picture of a word and type that word into a text field with each 
subsequent login attempt. If CAPTCHA has been triggered, you cannot use Jira's REST API to 
authenticate with the Jira site.

If CAPTCHA was triggered you will receive an error response from Jira when making requests through
the REST API. You can determine if CAPTCHA is causing the error by checking if there is an 
`X-Seraph-LoginReason` header with a value of `AUTHENTICATION_DENIED` in the response. If present, 
this means the application rejected the login without even checking the password. While this is the
most common indication that Jira's CATPCHA feature has been triggered, you can also check the number
of failed login attempts for the bot-user in Jira's user administration.

## Next steps

* Read the [Security overview](/server/jira/platform/security-overview/) to learn about other
methods of authentication.
*   Check out the Jira REST APIs:
    *   [Jira platform Server REST API](https://docs.atlassian.com/software/jira/docs/api/REST/latest/).
    *   [Jira Software Server REST API](https://docs.atlassian.com/jira-software/REST/latest/).
    *   [Jira Service Desk Server REST API](https://docs.atlassian.com/jira-servicedesk/REST/server/).