---
aliases:
- /server/jira/platform/adg-migration-from-5.x-to-6.0-16973858.html
- /server/jira/platform/adg-migration-from-5.x-to-6.0-16973858.md
category: devguide
confluence_id: 16973858
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=16973858
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=16973858
date: '2017-12-08'
legacy_title: ADG Migration from 5.x to 6.0
platform: server
product: jira
subcategory: updates
title: ADG Migration from 5.x to 6.0
---
# ADG Migration from 5.x to 6.0

## General Overview of Changes

In JIRA 6.0, we are moving away from JIRA-specific markup patterns in favour of those provided by [AUI](https://developer.atlassian.com/display/AUI/Atlassian+User+Interface+%28AUI%29+Developer+Documentation). This move is an essential part of implementing the [Atlassian Design Guidelines (ADG)](https://developer.atlassian.com/design/) in JIRA. Adopting AUI's patterns enables developers to quickly build UI in JIRA that complies with the ADG and works across Atlassian's product suite.

Parts of a page's DOM structure and markup may change in JIRA 6.0. The direct consequence of this is that your CSS or XPath selectors may no longer be valid. In particular:

-   CSS style rules may no longer target the desired elements.
-   Some CSS styles may become unnecessary.
-   JavaScript that relies on CSS selectors may break.
-   Automated UI tests using XPath or CSS selectors may break.

Keep in mind that if you are already using classic AUI, a lot of these changes will work for free. That is, unless you have overrides. If you have overrides, you need to remove those to get these changes.

### How to Reduce the Impact of Markup Changes

It's recommended that, where possible, all DOM operations should be separated from your code's business logic. For instance:

-   Use page objects in your UI tests to de-couple the flow of the test from selecting the elements on the page.
-   Use BackboneJS and place your DOM manipulation code in Backbone Views.
-   Write short CSS selectors; avoid using more than one combinator per CSS rule.

### How to Manage ADG Changes with Multiple Versions of JIRA

If your plugin needs to work with multiple versions of JIRA, there are certain scenarios you will need to keep in mind with the new ADG. Specifically, if your plugin overrides something inherited from JIRA/AUI, it is likely that there will be some necessary changes and ongoing maintenance. Our JIRA Agile development team has documented their experience with adapting the new [AUI](https://developer.atlassian.com/display/AUI/Atlassian+User+Interface+%28AUI%29+Developer+Documentation); see the [JIRA Agile ADG Migration Guidelines](/server/jira/platform/jira-agile-adg-migration-guidelines) for a case study and recommendations.

#### Using Conditional Web Resources

Rather than employing a `ServletFilter` or explicit `requireResource` commands per `Action`, Atlassian's GreenHopper development team defines backported web resources in a single block in the plugin XML:

``` xml
<web-resource key="gh-backports-jira5x" name="GH AUI for JIRA5x Resources">
    <transformation extension="less">
        <transformer key="lessTransformer" />
    </transformation>
    <!-- Stylesheets -->
    <resource type="download" name="gh-backports-jira5x.css" location="includes/css/jira5x/gh-backports-jira5x.less" />
    <condition class="com.pyxis.greenhopper.jira.conditions.IsPriorToJiraVersion">
        <param name="majorVersion">6</param>
        <param name="minorVersion">0</param>
    </condition>
</web-resource>
```

The `Condition` class implements the same version checking logic described above, so the web resource is only loaded when the appropriate JIRA version is present.

This web resource is then included as a dependency, like so:

``` xml
<dependency>com.pyxis.greenhopper.jira:gh-backports-jira5x</dependency>
```

The downside of this approach is that because it is conditional, the loading order of this web resource is deferred until after super batches and context batches. This may or may not be a problem, depending on whether your other styles are expecting the rules defined in your backports to be present in that order (so that they can be overridden, for example).

### Additional Notes

-   Our DOM is not a reliable API. If you're parsing or adjusting it via JavaScript, do not expect that it will continue to work.
-   SpeakEasy plugins are not supported and will break.
-   See [Using Soy Templates](/server/jira/platform/using-soy-templates) for examples and usage scenarios.

## JIRA Core CSS Changes

The core CSS in JIRA 6.0 has changed: JIRA now implements AUI's "<a href="https://bitbucket.org/atlassian/aui-archive/src/c16bed901455/auiplugin/src/main/resources/atlassian-plugin.xml?at=auiplugin-5.0-m25#cl-523" class="external-link">aui-experimental-page</a>" web resource, which includes a different set of CSS rules and resets.

JIRA 5.x sets both the margins and paddings of the following elements to zero:

``` css
html,body,img,li,dt,dd,pre,form,fieldset,textarea,input,select,option,button {margin: 0;padding: 0;}
```

JIRA 6.0's inclusion of AUI's reset.css provides a rough equivalency, though the margins and paddings of base HTML elements will differ in some circumstances.

Consult the AUI <a href="https://bitbucket.org/atlassian/aui-archive/src/c16bed901455/auiplugin/src/main/resources/experimental/css/atlassian/aui-reset.css?at=auiplugin-5.0-m25" class="external-link">reset.css</a> and <a href="https://bitbucket.org/atlassian/aui-archive/src/c16bed901455/auiplugin/src/main/resources/experimental/css/atlassian/aui-page-typography.css?at=auiplugin-5.0-m25" class="external-link">page-typography.css</a> files for the full detail.

## Core Typography Changes

JIRA now conforms to the [ADG Typography guidelines](https://developer.atlassian.com/design/typography.html). The base font size has been increased from 13px to 14px.  The font family has been changed to `Arial, sans-serif`.

You should address all CSS rules that adjust `font-` and `line-height` properties. Many of them can now be removed.

## Page Layout Changes

The Page Layout provides a standardised page with a blue header, white-on-grey content area, and footer in the standard Atlassian look. Plugin points are defined for adding messages through the document, as well as navigation items in the header.

<img src="/server/jira/platform/images/page-layout.png" width="300" height="183" />

In JIRA 5.x, an early version of the AUI Page pattern was adopted, with JIRA-specific extensions. You can read more about this pattern in [JIRA 5.0 Upgrade Notes](/server/jira/platform/changes-to-markup-css-and-decorators-in-jira-5-0). 

JIRA 6.0 has adopted a revised [AUI Page pattern](https://developer.atlassian.com/pages/viewpage.action?pageId=1278003), which affects some of the JIRA-specific extensions to the page pattern. See the **Code Examples** below for details.

### Summary of Impact

Here is the high-level summary of the changes:

-   JIRA 5.0's page layout markup styles and pattern (`content-container`, `content-body` and `content-related`) have been deprecated in favor of the [AUI Page pattern](https://developer.atlassian.com/pages/viewpage.action?pageId=1278003).
-   If you need to draw chrome around your page's content, use the `aui-page-panel` CSS class or even better, consume the Soy templates directly from AUI. For more information, see [Using Soy Templates](/server/jira/platform/using-soy-templates).
-   Check that your pages are using the correct decorators. See [JIRA's Page Decorators](https://developer.atlassian.com/display/JIRADEV/Changes+to+markup,+CSS+and+Decorators+in+JIRA+5.0) for documentation on each decorator's purpose.

### New Additions

JIRA now leverages the new `aui-page-panel` from the [AUI Page pattern](https://developer.atlassian.com/pages/viewpage.action?pageId=1278003). This will be rolled out to all of JIRA gradually. So far, the coverage includes:

-   Error decorator
-   Message decorator
-   Browse Project
-   Browse Version
-   Browse Component
-   Signup pages

### Code Examples

**JIRA 5.x**:

We are moving away from the JIRA page pattern:

``` xml
<div class="content-container">
    <div class="content-related">
        <!-- things like navigation go here -->
        I'm related!
    </div>
    <div class="content-body aui-panel">
        <!-- Your page's actual content -->
        <p>I am the content!</p>
    </div>
</div>
```

**Example**:

<img src="/server/jira/platform/images/screen-shot-2012-12-06-at-5.20.16-pm.png" width="300" height="81" />

The `aui-panel` class has been removed

**JIRA 6.0**:

We are adopting the AUI Page pattern:

``` xml
<div class="aui-page-panel">
    <div class="aui-page-panel-inner">
        <div class="aui-page-panel-nav">
            ...
        </div>
        <section class="aui-page-panel-content">
            ...
        </section>
    </div>
</div>
```

**Example**:

<img src="/server/jira/platform/images/screen-shot-2012-12-06-at-5.18.28-pm.png" width="300" height="81" />

There is no equivalent; see below.

### CSS Class Changes

The following CSS classes have changed:

-   `content-container` has been replaced by `aui-page-panel`
-   `content-body` has been replaced by `aui-page-panel-content`
-   `content-related` has been replaced by `aui-page-panel-nav`

The first important difference is in the DOM structure of the new layout pattern: the addition of `aui-page-panel-inner`. As a result, any CSS selectors using the child combinator for the page elements will break. For example:

-   `.content-container > .content-body` needs to be replaced with `.aui-page-panel-inner > .aui-page-panel-content`
-   `.content-container > .content-related` needs to be replaced with `.aui-page-panel-inner > .aui-page-panel-nav`

The second important difference is that the page panel chrome - the border and white background - is provided by the `aui-page-panel` class. This is the only way to render the page panel chrome. The `aui-panel` class no longer exists.

The following are NOT relative containers:

-   `aui-page-panel-content`
-   `aui-page-panel-nav`
-   `aui-page-panel-sidebar`

The closest relative position container is the parent `.aui-page-panel` element. This means that elements inside the content/nav/sidebar containers are rendered relative to the `aui-page-panel` element instead. If you need to use `position:absolute` within one of the child containers (nav/content/sidebar), you need to add your own relative container inside them.

## Page Header Changes

Header is a small CSS component for use with Page and Layout. It creates the common header style used across Atlassian products, primarily for use at the top of the content area of a page (usually on the grey background).

<img src="/server/jira/platform/images/page-header.png" width="500" height="168" />

JIRA 6.0 sees the adoption of the AUI Page Header pattern to render a combination of `<hx>` elements, avatars, breadcrumbs and buttons or links. JIRA's own styles for outputting headers with avatars, breadcrumbs or pagination will be removed in favour of using AUI's equivalent patterns.

For details on page header styles and combinations, consult the <a href="https://docs.atlassian.com/aui/5.2/docs/pageHeader.html" class="external-link">AUI Page Header</a> documentation.

### Summary of Impact

Here is the high-level summary of the changes:

-   All page headers use [AUI Page Header](https://developer.atlassian.com/pages/viewpage.action?pageId=8947147) markup.  
    -   This includes headers rendered outside and inside the AUI Page layout.
-   A page's header should use the AUI Page Header markup. Avoid only outputting an `<hx>` tag for page headings.
-   Avatars in headers should use the AUI Avatar markup pattern in the location dictated by the AUI Page Header markup pattern.
-   Breadcrumbs in headers should be placed in the location dictated by the AUI Page Header markup pattern.
-   Buttons and links headers should be placed in the `aui-page-header-actions` element defined by the AUI Page Header markup pattern. More detail on this can be found in the [Page Action Changes](#page-action-changes) section.

### Code Examples

**JIRA 5.x**

JIRA's `breadcrumbs` CSS class has been removed.

``` xml
<header>
    <ul class="breadcrumbs">
        <li><a href="http://example.com/">Breadcrumb</a></li>
        <li><a href="http://example.com/">Breadcrumb</a></li>
    </ul>
    <h1>Page Header with breadcrumbs before</h1>
</header>
```

**Example**

<img src="/server/jira/platform/images/screen-shot-2012-12-06-at-4.39.50-pm.png" width="300" height="38" />

The `project-config-header` CSS class and associated markup patterns have been removed.

``` xml
<header>
    <div id="project-config-header">
        <div class="aui-toolbar operation-menu"><!-- a toolbar to render some buttons -->
            <div class="toolbar-split toolbar-split-right">
                <ul class="toolbar-group">
                    <li class="toolbar-item">
                        <a class="toolbar-trigger" href="#">Edit Project</a>
                    </li>
                </ul>
            </div>
        </div>
        <img width="48" height="48" id="project-config-header-avatar" src="/project/avatar" /><!-- the avatar -->
        <h1>Angry Nerds</h1>
    </div>
</header>
```

**Example**

<img src="/server/jira/platform/images/screen-shot-2012-12-06-at-5.10.26-pm.png" width="300" height="46" />

**JIRA 6.0**

Use the AUI breadcrumbs inside the AUI page header pattern.

``` xml
<header class="aui-page-header">
    <div class="aui-page-header-inner">
        <div class="aui-page-header-main">
            <ol class="aui-nav aui-nav-breadcrumbs">
                <li><a href="http://example.com/">Breadcrumb</a></li>
                <li><a href="http://example.com/">Breadcrumb</a></li>
                <li class="aui-nav-selected">Breadcrumb</li>
            </ol>
            <h1>Page Header with breadcrumbs before</h1>
        </div>
    </div>
</header>
```

**Example**

<img src="/server/jira/platform/images/screen-shot-2012-12-06-at-5.07.29-pm.png" width="300" height="38" />

Use the AUI page header equivalent:

``` xml
<header class="aui-page-header">
    <div class="aui-page-header-inner">
        <div class="aui-page-header-image">
            <!-- avatar goes here -->
        </div>
        <div class="aui-page-header-main">
            <h1>Project name</h1>
        </div>
        <div class="aui-page-header-actions">
            <!-- some buttons -->
            <div class="aui-buttons">
                <button class="aui-button">Edit Project</button>
            </div>
        </div>
    </div>
</div>
```

**Example**

<img src="/server/jira/platform/images/screen-shot-2012-12-06-at-5.12.37-pm.png" width="300" height="50" />

## Page Action Changes

A common pattern in JIRA is to render one or several buttons in conjunction with a heading. For instance:

<img src="/server/jira/platform/images/page-actions.png" width="500" height="115" />

This is what we refer to as a *page action*: a button or link that is related to the heading on a page.

Traditionally, there have been several approaches to outputting these page actions. In JIRA 6.0, we use AUI's Page Header pattern, which provides an explicit location for these actions:

``` xml
<div class="aui-page-header">
    <div class="aui-page-header-inner">
        ...
        <div class="aui-page-header-actions">
             <!-- your buttons and links go here! -->
        </div>
    </div>
</div>
```

If you have been outputting buttons or links to the page before its header - either in templates or in Javascript - you should update your code to place them in this `aui-page-header-actions` container.

### Summary of Impact

Here is the high-level summary of the changes:

-   All actions and operations buttons should be rendered using AUI Buttons.  
    -   Actions with additional options should use AUI Button + Dropdown2.
-   Plugins pushing actions into the page's header area (via jQuery, etc.) need to be updated:
    -   The actions should be prepended to an `aui-page-header-actions` container.
    -   Do not insert actions directly before a heading element - e.g., `$(myButton).insertBefore("h1")` - as they will not end up where you want them to.

### Removals / Deprecations

The AUI page header pattern supports use of AUI buttons in the `aui-page-header-actions` area. Because of this, JIRA 6.0 uses AUI buttons in page headers and has removed some equivalent JIRA-specific patterns:

-   JIRA's `page-navigation` markup pattern is deprecated and styles removed in JIRA 6.0.
-   JIRA's `operations` markup pattern is deprecated; use AUI Subtle Buttons instead. 
-   JIRA's `operations-list` markup pattern is deprecated; use AUI Subtle Buttons instead.

### Code Examples

In conjunction with this explicit container comes the deprecation and removal of several markup patterns for outputting buttons in a page's header, as described below.

**JIRA 5.x**

The `page-navigation` markup pattern no longer works

``` xml
<ul class="page-navigation">
    <li class="previous"><a id="previous-issue" href="#">Previous</a></li>
    <li class="showing"><a href="#">Current</a></li>
    <li class="next"><a id="next-issue" href="#>Next</a></li>
</ul>
 
```

You no longer need to use AUI toolbar to render buttons above a header.

Placing an AUI toolbar above a heading does not automatically float the toolbar to the right.

``` xml
<div class="aui-toolbar operation-menu"><!-- a toolbar to render some buttons -->
    <div class="toolbar-split toolbar-split-right">
        <ul class="toolbar-group">
            <li class="toolbar-item">
                <a class="toolbar-trigger" href="#">Edit</a>
            </li>
            <li class="toolbar-item">
                <a class="toolbar-trigger" href="#">Do something</a>
            </li>
        </ul>
        <ul class="toolbar-group">
            <li class="toolbar-item">
                <a class="toolbar-trigger" href="#">Separate button</a>
            </li>
        </ul>
    </div>
</div>
```

**Example**

<img src="/server/jira/platform/images/screen-shot-2012-12-06-at-4.30.55-pm.png" width="287" height="51" />

The `operation-menu` markup and styles are gone.

``` xml
<div class="operation-menu">
    <ul>
        <li><a href="#">Edit</a></li>
        <li><a href="#">Do something</a></li>
    </ul>
</div>
```

The "quicklinks", "operations" and "operations-container" CSS classes are deprecated. Their styles and associated markup will be removed.

``` xml
<ul class="operations">
    <li><a href="#">Edit</a></li>
    <li><a href="#">Do something</a></li>
</div>
 
<div class="operations-container">
    ...
</div>
 
<div id="quicklinks">
    <ul class="operations">...</ul>
</div>
```

**JIra 6.0**

Use AUI pagination.

``` xml
...
<div class="aui-page-header-actions">
    <ul class="aui-nav aui-nav-pagination">
        <li class="aui-nav-previous"><a id="previous-issue" href="#">Previous</a></li>
        <li class="aui-nav-current"><a href="#">Current</a></li>
        <li class="aui-nav-next"><a id="next-issue" href="#">Next</a></li>
    </ul>
</div>
...
```
Use AUI buttons.

``` xml
...
<div class="aui-page-header-actions">
    <div class="aui-buttons">
        <a class="aui-button" href="#">Edit</a>
        <button class="aui-button">Do something</button>
    </div>
    <div class="aui-buttons">
        <button class="aui-button">Separate button</button>
    </div>
</div>
...
```

If your button's behaviour is controlled with javascript, use the `<button>` element.

Only use `aui-button` with the `<a>` element if there is an actual page to visit.

**Example**

<img src="/server/jira/platform/images/screen-shot-2012-12-06-at-4.33.17-pm.png" width="306" height="45" />

Use AUI buttons.

Use AUI buttons.

## Changes to JIRA Help Links

JIRA 6.0 defines a consistent template for outputting links to the JIRA documentation.

<img src="/server/jira/platform/images/jira-help.png" width="300" height="162" />

### Summary of Impact

-   Whenever you need to output a help link, use the [Soy template](/server/jira/platform/using-soy-templates) for it.
-   If outputting a help link for a page heading, use the page header template and pass a rendered help link to its `helpContent` parameter.

### New Additions

-   A new Soy template called `JIRA.Templates.Links.helpLink` was created to output help links.
-   JIRA's page header Soy template accepts a parameter for a rendered help link, called `helpContent`.
-   Each of the following JSP templates and Velocity macros now wraps a call to the `helpLink` Soy template:
    -   `help.jsp`
    -   `helplink.jsp`
    -   `toolbarHelp.jsp`
    -   `#localHelp`
    -   `#localTimeTrackingHelp`
    -   `#jiraHelpLink`

## JIRA Button Changes

In JIRA 5.2, a refactor of JIRA's buttons took place to align them with the AUI button markup pattern. In JIRA 6.0, we now use the AUI button styles and markup pattern.

<img src="/server/jira/platform/images/jira-buttons.png" class="confluence-thumbnail" width="300" />

The impact of this change should be minor, though there are a few things to note:

-   The dimensions and appearance of JIRA buttons has changed. Any CSS that overrides or extends the look and feel of JIRA's buttons is now invalid and should be removed.
-   Neither JIRA nor AUI define styles for an `aui-button-cancel` CSS class. The equivalent style of this class is provided by the `aui-button-link` CSS class. If you need to define functionality for a cancel button or link, you may add your own.
-   The disabled state of AUI buttons is represented with an ARIA attribute: `aria-disabled="true"`. The styles for a disabled button or link are dependent on this ARIA attribute. JIRA 6.0 includes the styles for these, however previous versions of JIRA do not. To make AUI buttons appear disabled in previous versions of JIRA, add the following CSS to your plugin:  
      

    ``` css
    .aui-button[aria-disabled="true"],
    .aui-button[aria-disabled="true"]:hover,
    .aui-button[aria-disabled="true"]:focus,
    .aui-button[aria-disabled="true"]:active,
    .aui-button[aria-disabled="true"][aria-pressed] {
        background: #f2f2f2;
        background: -moz-linear-gradient(top, #FFF 0%, #f2f2f2 100%); 
        background: -webkit-linear-gradient(top, #FFF 0%,#f2f2f2 100%);
        background: -o-linear-gradient(top, #FFF 0%,#f2f2f2 100%);
        background: -ms-linear-gradient(top, #FFF 0%,#f2f2f2 100%); 
        background: linear-gradient(top, #FFF 0%,#f2f2f2 100%);
        border-color: #ddd;
        box-shadow: none;
        color: #999;
        cursor: default;
        text-shadow: none;
    }
    ```

### Summary of Impact

-   Remove overrides to `aui-button` - particularly margins and paddings.
-   Replace usage of the `aui-button-cancel` CSS class with `aui-button-link cancel`
-   If using AUI buttons in older versions of JIRA, you may need to provide your own CSS for their disabled state.

## JIRA Dialog Changes

In JIRA 5.x, JIRA overrides AUI's Dialog implementation such that every AUI dialog is instantiated as a JIRA Dialog.  To this end, JIRA used HTML class names prefixed with "aui-" to represent its own dialogs. The overlap of JIRA's implementation with AUI's caused some compatibility issues.

In JIRA 6, JIRA and AUI dialogs are completely separate entities.

#### HTML Class Changes

With the separation of the AUI and JIRA Dialog implementations, the following changes have occurred:

<table>
<colgroup>
<col style="width: 33%" />
<col style="width: 33%" />
<col style="width: 33%" />
</colgroup>
<thead>
<tr class="header">
<th>Old class in JIRA</th>
<th>New class in JIRA</th>
<th>Intent of class</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>aui-popup</td>
<td>jira-dialog</td>
<td>The HTML node representing the JIRA Dialog</td>
</tr>
<tr class="even">
<td>aui-popup-heading</td>
<td><p>jira-dialog-heading</p></td>
<td>The container for the JIRA Dialog's heading element and other content (such as buttons)</td>
</tr>
<tr class="odd">
<td>aui-popup-content</td>
<td>jira-dialog-content</td>
<td>The body of the JIRA Dialog - includes everything except the heading element.</td>
</tr>
<tr class="even">
<td>aui-dialog-open</td>
<td>jira-dialog-open</td>
<td>Modifier to the jira-dialog class to make the Dialog appear on screen.</td>
</tr>
<tr class="odd">
<td>aui-dialog-content-ready</td>
<td>jira-dialog-content-ready</td>
<td><p>Flag that indicates the contents of the JIRA Dialog have been successfully placed inside the jira-dialog-content element.</p>
<p>This is relevant when the contents of a dialog are generated or loaded by asynchronous javascript.</p></td>
</tr>
<tr class="even">
<td>content-area-container</td>
<td>content-area-container</td>
<td>If an entire page response is loaded inside a dialog, only the HTML within an element with this class will be loaded.</td>
</tr>
</tbody>
</table>

#### Differences between the AUI and JIRA Dialogs

AUI's `AJS.Dialog` does not work the same as the `JIRA.Dialog`. Read the <a href="https://docs.atlassian.com/aui/7.3.3/docs/dialog.html" class="createlink">AUI Dialog documentation</a> for an up-to-date reference of its API.

Below is an abbreviated list of the most significant differences:

**Task**

Instantiation

**JIRA Dialog**

``` javascript
var $content = jQuery("<p>The contents of the dialog</p>");
var dialog = new JIRA.Dialog({
    widthClass: "large",
    id: "example-jira-dialog",
    content: $content
});
```

**AUI Dialog**

``` javascript
var dialog = new AJS.Dialog({
    width: 400,
    height: 300,
    id: "example-aui-dialog",
    closeOnOutsideClick: true
});
```

**Task**

Adding content to the dialog's body

**JIRA Dialog**

Typically added during instantiation of the Dialog itself.

``` javascript
new JIRA.Dialog({
    content: jQuery("<p>A single paragraph</p>");
});
```

``` javascript
new JIRA.Dialog({
    content: function(ready) {
        var $content = "<p>from a function</p>";
        ready($content);
    }
});
```

**AUI Dialog**

Must pass content in to `Dialog#addPanel`.

``` javascript
dialog.addPanel("Panel 0", jQuery("<p>A single paragraph</p>"), "custom-class-for-content");
```

**Task**

Adding footer buttons

**JIRA Dialog**

Should be part of the Dialog's contents.

``` javascript
var $button = jQuery("<button>Button name</button>").click(function() {
    // Code called when the button is clicked
});
var $content = jQuery("<div class='buttons-container'></div>");
$content.append($button);
 
new JIRA.Dialog({
    content: function(ready) {
        ready($content);
    }
});
```

**AUI Dialog**

Must construct buttons via `Dialog#addButton`.

``` javascript
dialog.addButton("Button name", function(theDialog) {
    // Code called when the button is clicked
});
```

## Changes to Form Styles

Both JIRA 5.x and JIRA 6.x use AUI forms to render most form fields. Their widths are controlled by <a href="https://bitbucket.org/atlassian/aui-archive/src/c16bed901455/auiplugin/src/main/resources/css/atlassian/forms.css?at=auiplugin-5.0-m25#cl-126" class="external-link">four HTML classes: tiny-field, short-field, medium-field, long-field, and full-width-field</a>.

In JIRA 6 (due to AUI 5.0), a subtle yet important change has been made to form field widths: Instead setting the `width` property of the field, all fields are `width: 100%;`, and only the max-width is set.

The net effect of this change is that form fields will no longer outgrow the width of the containers they are placed in. For instance, a `long-field` in a JIRA Dialog where the dialog is only 400px wide will fit within the dialog and not cause horizontal scrollbars within the dialog.

It is advised that all explicit widths are removed from form fields, both in CSS files and inline styles. Instead, use AUI's form field width modifiers.

## Miscellany

-   The `status-lozenge` class was removed from the admin sections. Use `aui-lozenge` instead.
