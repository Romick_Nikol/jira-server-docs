---
aliases:
- /server/jira/platform/about-the-jira-server-java-apis-52430879.html
- /server/jira/platform/about-the-jira-server-java-apis-52430879.md
category: reference
confluence_id: 52430879
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=52430879
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=52430879
date: '2017-12-08'
legacy_title: About the JIRA Server Java APIs
platform: server
product: jira
subcategory: api
title: About the JIRA Server Java APIs
---
# About the JIRA Server Java APIs

The JIRA platform provides Java APIs that you can use to interact with JIRA programmatically. These APIs are common to all JIRA applications. In addition, JIRA Software and JIRA Service Desk provide APIs for application-specific functionality. For example, the JIRA Software API has methods for sprints, etc. 

The JIRA Java APIs are typically used when building Plugins2 add-ons (for JIRA Server). If you haven't used the JIRA Java APIs before, make sure you read the [Java API Policy for JIRA](#java-api-policy-for-jira) below.

## Java API reference

JIRA Platform Java APIs:

-   <a href="https://docs.atlassian.com/jira/server/" class="external-link">Latest production version</a>
-   <a href="https://docs.atlassian.com/jira/" class="external-link">All versions (including EAPs)</a>

JIRA Software Java API:

-   <a href="https://docs.atlassian.com/jira-software/" class="external-link">All versions</a>

JIRA Service Desk Java API:

-   <a href="https://docs.atlassian.com/jira-servicedesk/server/" class="external-link">Latest production version</a>
-   <a href="https://docs.atlassian.com/jira-servicedesk/" class="external-link">All versions</a>

## Java API policy for JIRA

This page is a friendly statement of the Atlassian policy for JIRA's Java APIs. The intention of this document is to let developers know about the APIs that are safe to use, and to indicate the internal components that are subject to change without notice.

This policy is applicable to **JIRA 5.0 and later**.

### Terms and definitions

-   **Major release**  
    A major release is a '.0' release.  
    For example: 5.0, 6.0, 7.0, ...
-   **Minor release**  
    Minor releases are the 'children' of a major release that introduce new features.  
    For example: 5.1, 5.2, 5.3, ...
-   **Bug fix release**  
    A bug fix release is a child of a minor or major release and is primarily intended to fix problems without altering behaviour.  
    For example: 5.0.1, 5.0.2, 5.1.1, ...

### The `jira-api` Maven artifact

In JIRA 5.0 we have separated the Java components that we consider to be part of our API from the internal components that are subject to change without notice.

The API components live in a Maven module called '`jira-api`'.

Plugins can depend on this module which means that they will use only the safe JIRA components.

``` xml
    <dependency>
        <groupId>com.atlassian.jira</groupId>
        <artifactId>jira-api</artifactId>
        <version>${atlassian.product.version}</version>
        <scope>provided</scope>
    </dependency>
```

### @PublicApi, @PublicSpi, and @ExperimentalApi

In general, any class or interface in the '`jira-api`' module can be considered a part of JIRA's Java API, and we will strive to maintain binary compatibility for these classes and interfaces. There is a distinction between different types of API, however, and these are indicated by the presence of the following annotations:

#### @PublicApi

API includes interfaces/classes that plugins need to use to get things done. Normally JIRA will produce instances of these things, which plugins will consume but not implement/extend. Plugins can safely use these and we will guarantee binary compatibility (subject to our deprecation policy). If plugins implement/extend these classes, we can not guarantee binary compatibility (we will be adding methods to these interfaces, etc).

#### @PublicSpi

SPI (<a href="http://en.wikipedia.org/wiki/Service_Provider_Interface" class="external-link">Service Provider Interface</a>) includes interfaces/classes that are designed specifically for plugins to implement or extend. In this case we have to be even more careful than with @PublicApi, since even adding methods to interfaces will break binary compatibility.

#### @ExperimentalApi

If we know that something is likely to change but is still useful enough to move into the API in its preliminary form, we'll mark it with @ExperimentalApi. There is no guarantee of binary compatibility for experimental API.

#### @Internal

Internal classes and interfaces are not to be used by clients other than JIRA itself. There is no guarantee of binary compatibility for internal types.

### Compatibility policy

The JIRA API (all classes and other components in the `jira-api` module) will remain *forward-compatible* throughout all the minor releases within a major release family.

For example, a plugin that is built against JIRA 5.0 will continue to work with JIRA 5.1, v5.2, and so on, without being recompiled.

#### Major releases

A major release may change the existing API.

Normally, we will attempt to mark the method or class as deprecated in a previous minor release, but this is not guaranteed.

In general, it may not be possible to create a single plugin artifact that is compatible with different major release families.

For example, it may not be possible to create a single JAR for your plugin that is compatible with both JIRA 5.0 and JIRA 4.4.

#### Minor releases

A minor release may add a new API, but it will be binary compatible with the API from previous releases in the same major release family.

For example, a plugin built against JIRA 5.0 will still work with JIRA 5.1, 5.2, and so on, but may not be compatible with JIRA 6.0.

In turn, a plugin using new functionality introduced in JIRA 5.1 will still work in JIRA 5.2, 5.3, and so on, but may not be compatible with JIRA 6.0.

### Deprecation policy

Types or methods that are deprecated in the Java API will have the `@deprecated` tag added to their Javadoc.

The Javadoc will explain how to replace usage of the deprecated component, and list the version in which it was set to deprecated.

Example:

``` java
    /**
     * Returns the logged in user.
     *
     * @deprecated Use {@link #getLoggedInUser()} instead. Since v4.3
     */
    public User getRemoteUser()
```

Any JIRA API classes or methods that exist but are deprecated in a particular release will continue to exist and work in the following minor releases, but will be removed in the next major release.

For example, a method that is marked as deprecated in JIRA 6.1 will still work in JIRA 6.2, but will be deleted in JIRA 7.0

### Getting access to the internal classes

The internal classes (sometimes call 'implementation classes') exist in a module called '`jira-core`'.

Plugin developers may have a dependency on `jira-core` and use any of these implementation classes, but the underlying code may change even in bug-fix releases.

`jira-core` has a transitive dependency on `jira-api`, so depending on `jira-core` will also bring `jira-api` into your plugin.
