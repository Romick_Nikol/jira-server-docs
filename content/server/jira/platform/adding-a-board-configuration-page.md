---
aliases:
- /server/jira/platform/tutorial-adding-a-board-configuration-page-38444600.html
- /server/jira/platform/tutorial-adding-a-board-configuration-page-38444600.md
category: devguide
confluence_id: 38444600
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=38444600
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=38444600
date: '2018-06-15'
guides: tutorials
legacy_title: Tutorial - Adding a board configuration page
platform: server
product: jira
subcategory: learning
title: "Adding a board configuration page"
---
# Adding a board configuration page

In this tutorial, you will build a board configuration page for your Jira Software Server app. We won't go into the
details of adding settings to the page, but this tutorial will provide a good starting point if you want your app to
have separate configurations per board.

To create this board configuration page, you'll create a new web panel located in `jira.agile.board.configuration`, and
then add code to render the web panel. That's it!

This is what it will look like:

<img src="/server/jira/platform/images/jswdev-boardconfigpage.png" width="543" height="250" />

{{% note %}}

About these instructions

You can use any supported combination of operating system and IDE to create this app. These instructions were
written using IntelliJ IDEA 2017.3 on macOS Sierra. If you use another operating system or IDE combination,
you should use the equivalent operations for your specific environment.

This tutorial was last tested with Jira 7.10.0 using Atlassian Plugin SDK 6.3.10. 

{{% /note %}}

## Step 1. Add a web panel with board context parameters

To create a board configuration page:

1.  Create a `web-panel` with the `label`.
1.  Set `web-panel` location to `jira.agile.board.tools`. 
1.  Reference i18n properties from resource module.
1.  Create a Soy template and put a reference in `web-resource` plugin module.

    The complete look of `atlassian-plugin.xml`:

    ``` xml
    <atlassian-plugin key="${atlassian.plugin.key}" name="${project.name}" plugins-version="2">
        <plugin-info>
            <description>${project.description}</description>
            <version>${project.version}</version>
            <vendor name="${project.organization.name}" url="${project.organization.url}" />
        </plugin-info>

        <resource type="i18n" name="i18n" location="custom-board-tool-section"/>

        <web-resource key="soy" name="custom-board-tool-section Web Resources">
            <resource type="soy" location="/soy/configuration-board.soy" name="My.addOn.tpl.board.config.myConfigurationPage"/>
            <context>custom-board-tool-section</context>
        </web-resource>

        <web-panel key="my-configuration-page" location="jira.agile.board.configuration" weight="1">
            <label key="my.configuration.page.label">My Configuration Page</label>
            <resource type="soy" name="view" location=":soy/My.addOn.tpl.board.config.myConfigurationPage" />
        </web-panel>
    </atlassian-plugin>
    ```

    For more details about the app descriptor, check the
    [Configuring the app descriptor file](/server/framework/atlassian-sdk/configuring-the-plugin-descriptor/)
    page.
1.  In `custom-board-tool-section.properties` that we referenced from descriptor, add the following:

    ``` properties
    my.configuration.page.label=My Configuration Page
    ```

## Step 2. Add code to render the web panel

The following example shows how to render the web panel using Soy templates.

The `configuration-board.soy` file is placed in the `src/main/resources/soy/` directory.
You could use Velocity templates instead, if you wish.

Note that two context parameters are available in the template for you to use: `$board.id` and `$board.type`.

You can see the mentioned parameters in the following example:

``` xml
{namespace My.addOn.tpl.board.config}

/**
    Custom configuration page.
    @param board
*/
{template .myConfigurationPage}
    My first config page for board: {$board.id} with type: {$board.type}.
{/template}
```

Congratulations! You've just built a board configuration page for your Jira Software P2 app.

## Next steps

* If you look for somewhere to store configuration information, make use of the board properties.
* Your app can define custom [project permissions](https://confluence.atlassian.com/display/ADMINJIRASERVER/Managing+project+permissions) 
or [global permissions](https://confluence.atlassian.com/display/ADMINJIRASERVER/Managing+global+permissions).
This may be useful if you want to limit access to your app's board configuration page.
* Learn about [Adding menu items to Jira](/server/jira/platform/adding-menu-items-to-jira/).
* Learn about [Web panel module](/server/jira/platform/web-panel/).
