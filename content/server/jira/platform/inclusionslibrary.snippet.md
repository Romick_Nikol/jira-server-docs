---
aliases:
- /server/jira/platform/-inclusionslibrary-4227132.html
- /server/jira/platform/-inclusionslibrary-4227132.md
category: devguide
confluence_id: 4227132
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=4227132
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=4227132
date: '2017-12-08'
legacy_title: _InclusionsLibrary
platform: server
product: jira
subcategory: other
title: _InclusionsLibrary
---
# \_InclusionsLibrary

The children of this page contain information which is included in other pages and sometimes even in other spaces. This is a library of reusable information chunks.

If you want to change any of these pages, be aware that:

1.  Changing page names is problematic - you will need to change all the {include} and {excerpt-include} macros manually.
2.  The content is used in many places - make sure your change is generic enough to fit the contexts in which the pages are used.

Child pages:

-   [\_ArchiveNotice](/server/jira/platform/archivenotice.snippet)
-   [Diagrams](/server/jira/platform/diagrams.snippet)
