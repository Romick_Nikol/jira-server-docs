---
title: "Jira Software 7.10 EAP release notes"
product: jira
platform: server
category: devguide
subcategory: updates
date: "2018-05-11"
---
# Jira Software 7.10 EAP release notes

*11 May 2018*

Atlassian is proud to present *Jira Software 7.10 EAP*. This public development release is part of our Early Access Program (EAP) leading up to the official *Jira Software 7.10* release. We are making these EAP and Release Candidate milestones publicly available so that developers can start assessing the impact of the changes that we are making.

For the full list of changes for Jira 7.10, read the developer change management guide: [Preparing for Jira 7.10](/server/jira/platform/preparing-for-jira-7-10). If you are new to Jira, you should also read our [Java API Policy for Jira](/server/jira/platform/java-api-policy-for-jira-4227213.html).

<center><a href="https://www.atlassian.com/software/jira/download-eap" class="external-link">Download EAP</a></center>

{{% warning %}}

Before you install/upgrade: Atlassian does not support upgrades both *from* and *to* EAP releases. EAP releases should not be used in production environments as they are not officially supported. For all production use and testing of Jira, please use the <a href="https://confluence.atlassian.com/display/JIRASOFTWARE/JIRA+Software+7.9.x+release+notes" class="external-link">latest official release</a> instead.

{{% /warning %}}

## Changes

### EAP-02

### Project archiving in Jira Data Center

You can archive any project in Jira if you're no longer working on it. It's a good practice to archive inactive or completed projects, so they don't clutter your Jira instance. Project archiving is available in Jira Software Data Center and Jira Service Desk Data Center.

Archiving a project helps you:

- Improve performance by reducing the amount of data Jira has to process. 
- Reduce the clutter by hiding unusued projects and issues.


**To archive a project:**

1. Go to **Administration > Projects**.

2. Find a project you want to archive, and select **... > Archive**.

![Projects list](/server/jira/platform/images/projectslist_archive.png)

After you archive a project, it will be restricted from viewing and moved to the *Archived projects* page. You can see what exactly happens to an archived project below.

<details><summary>**Index**</summary>

- Project's data is removed from Jira's index, which is a collection of text entered into issue fields, used for faster searching. This improves performance by removing data that is stored directly in Jira.
</details>

<details><summary>**Project**</summary>

- Project is restricted, and can't be viewed by anyone.
- Project no longer appears in project pickers, list of projects, search results, or JQL auto-complete. It won't be visible anywhere besides the *Archived projects* page.
</details>

<details><summary>**Issues**</summary>

- Issues become read-only. You can't modify them, but you can view them either through a direct link or a mention in other projects or applications. This helps you refer to older issues even if they're already archived.
- Similarly to the project itself, issues no longer appear in search results.
</details>

<details><summary>**Configuration**</summary>

- Archiving a project doesn't affect the configuration that this project uses (schemes, screens, workflows, etc.) The configuration remains where it is and is still shared with other projects. If you change it, the changes will also apply to an archived project once it's restored.
</details>

<br>
**Restoring a project**

The project is kept in the database, so you can restore it whenever it's needed again. You can do it from the *Archived projects* page. The project will be available immediately after restoring, but you need to re-index it (**Project settings > Re-index project**) to make it visible in search results.

<br>

### EAP-01

### Jira gets a fresh, new look

We're bringing a refreshed look and feel to many of our Server and Data Center products, including the whole Jira family. This work is based on the new Atlassian design and includes things like updated colors, typography and icons. In Jira 7.10, we're mostly updating the following pages, but you'll also notice some smaller tweaks all around Jira. 

- Issue view
- Issue Navigator
- Global Issue Navigator
- Dashboard
- Projects list
- Login page
- Updated buttons, icons, and typography

**Things to watch out for if you're an app developer:**

- *Issue view:* We're now using AUI tabs instead of custom Jira elements.
- *Global Issue Navigator:* Some AUI overrides were removed from Jira.
- *Project Issue Navigator:* We've replaced the deprecated Toolbar1 with the new Toolbar2, removed the 'legacyIssueHeader' template, and replaced deprecated drop-downs with the Dropdown2 web component.
- *Browse projects:* We've changed the vertical navigation to the AUI one.

You can find more changes in the [AUI upgrade guide](https://docs.atlassian.com/aui/7.8.0/docs/upgrade-guide.html).
 
We haven't made any changes to the navigation, like you may have seen in our Cloud products, so you should still know your way around. As for the pages listed above, just have a look at these screenshots, or better yet install the EAP and see for yourself!

*Dashboard*

![Dashboard](/server/jira/platform/images/jira_dashboard.png)

*Issue Navigator*

![Issue navigator](/server/jira/platform/images/jira_issue_navigator.png)

*Issue view*

![Issue view](/server/jira/platform/images/issue_view.png)
