---
aliases:
- /server/jira/platform/about-jira-plugin-development-4227105.html
- /server/jira/platform/about-jira-plugin-development-4227105.md
category: devguide
confluence_id: 4227105
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=4227105
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=4227105
date: '2017-12-08'
legacy_title: About JIRA Plugin Development
platform: server
product: jira
subcategory: other
title: About JIRA plugin development
---
# About JIRA plugin development

{{% note %}}

Redirection Notice

This page will redirect to <https://developer.atlassian.com/x/OoB> in about 2 seconds.

{{% /note %}}
