---
aliases:
- /server/jira/platform/preparing-for-jira-7.2-39362261.html
- /server/jira/platform/preparing-for-jira-7.2-39362261.md
category: devguide
confluence_id: 39362261
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=39362261
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=39362261
date: '2017-12-08'
legacy_title: Preparing for JIRA 7.2
platform: server
product: jira
subcategory: updates
title: Preparing for JIRA 7.2
---
# Preparing for JIRA 7.2

This page covers changes in **JIRA 7.2** that can affect add-on compatibility and functionality. This includes changes to the JIRA platform and the JIRA applications (JIRA Core, JIRA Software, JIRA Service Desk). 

As a rule, Atlassian makes every effort to inform add-on developers of known API changes **as far in advance as possible**. Where possible, we attach release targets. Unless otherwise indicated, every change on this page is expected to be released with the first public release of the JIRA Server 7.2 products. We also make release milestones available prior to the release:

-   For JIRA Server, the JIRA development team releases a number of EAP milestones prior to the final release, for customers and developers to keep abreast of upcoming changes. For more information on these releases, see [JIRA EAP Releases](/server/jira/platform/jira-eap-releases-35160569.html).
-   For JIRA Cloud, the JIRA team releases updates to the JIRA Cloud products on a weekly basis. Add-ons that integrate with the JIRA Cloud products via Atlassian Connect should use the JIRA REST API, which is subject to the <a href="https://developer.atlassian.com/display/HOME/Atlassian+REST+API+policy" class="external-link">Atlassian REST API Policy</a>.

We will update this page as development progresses, so please stay tuned for more updates. We also recommend following the JIRA news on [Atlassian Developers blog](https://developer.atlassian.com/blog/categories/jira/) for important announcements and helpful articles.

## Summary

The risk level indicates the level of certainty we have that things will break if you are in the "Affected" column and you don't make the necessary changes.

<table>
<colgroup>
<col style="width: 30%" />
<col style="width: 70%" />
</colgroup>
<thead>
<tr class="header">
<th>Change. Platform/Application</th>
<th>Risk level. Affects</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="/server/jira/platform/deprecating-junit3-test-framework">Change: Deprecating JUnit3 test framework</a></p>
<p>Platform/Application: JIRA platform</p></td>
<td><p>Risk level: low</p>
<p> Affects: Functional test API</p>
<p> </p></td>
</tr>
<tr class="even">
<td><p><a href="#removing-extensionaccessor">Change: Removing ExtensionAccessor</a></p>
<p>Platform/Application: JIRA platform</p></td>
<td><p>Risk level: low</p>
<p> Affects: Plugins using ExtensionAccessor</p></td>
</tr>
<tr class="odd">
<td><p><a href="/server/jira/platform/aui-5-7-to-5-9-upgrade">Change: AUI Upgrade 5.7 -&gt; 5.9</a></p>
<p>Platform/Application: JIRA platform</p></td>
<td><p>Risk level: medium</p>
<p> Affects: The latest version of AUI removes the aui-inline-dialog2 web component and replaces it with aui-inline-dialog Any usage of aui-inline-dialog2 will need to be replaced with the updated aui-inline-dialog. In additon aui-inline-dialog2 is no longer experimental and is now shipped with AUI. For more information on upgrading aui-inline-dialog2 see: <a href="https://docs.atlassian.com/aui/latest/docs/inline-dialog-upgrade-guide.html" class="uri external-link">https://docs.atlassian.com/aui/latest/docs/inline-dialog-upgrade-guide.html</a>. Because of these changes any plugin updated to work with the version of AUI shipped with JIRA 7.2.0 will not automatically be backwards compatible with older versions of JIRA.</p>
<p>The AUI upgrade also comes with updated versions of webdriver and selenium. These updates may have an impact on your test infrastructure if you have been relying on AUI for your versions of these libraries.</p></td>
</tr>
<tr class="even">
<td><p>Change: Updated navigation experience</p>
<p> Platform/Application: JIRA platform</p></td>
<td><p>Risk level: low</p>
<p> Affects: We introduced an updated navigation experience which updates our existing project sidebar and places it onto all project related pages. This also introduces some performances changes to our UI stack to allow for asynchronous loading of elements. This may impact plugins that use the sidebar. Specifically you should ensure that any interactions with the sidebar are via JIRA.API.getSidebar() rather than the non-promise based JIRA.API.Sidebar.</p></td>
</tr>
</tbody>
</table>

<sup>\*\ This\ is\ a\ JIRA\ platform\ feature,\ but\ it\ currently\ only\ works\ for\ JIRA\ Software.</sup>

## JIRA platform changes

### Removing ExtensionAccessor

ExtensionAccessor was introduced in JIRA 7.1 to provide the ability to query for current extensions. In case you used  ExtensionAccessor in your plugin, please revert back to PluginAccessor.

## JIRA Core changes

No specific JIRA Core application changes.

## JIRA Software changes

No specific JIRA Software application changes.

## JIRA Service Desk changes

No specific JIRA Service Desk application changes.
