---
aliases:
- /server/jira/platform/jira-plugin-module-types-7897169.html
- /server/jira/platform/jira-plugin-module-types-7897169.md
category: reference
confluence_id: 7897169
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=7897169
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=7897169
date: '2017-12-08'
legacy_title: JIRA Plugin Module Types
platform: server
product: jira
subcategory: modules
title: About JIRA modules
---
# About JIRA modules

JIRA modules allow add-ons to extend the functionality of the JIRA platform or a JIRA application. JIRA modules are commonly used to extend the user interface by adding links, panels, etc. However, some JIRA modules can also be used to extend other parts of JIRA, like permissions and workflows. JIRA Service Desk and JIRA Software also have their own application-specific modules (UI-related only).

## Using JIRA modules

You can use a JIRA module by declaring it in your add-on descriptor, with the appropriate properties. For example, the following code uses a web section and web item to add a 'myItem' menu to the JIRA header.

``` xml
<web-section name="mySection" i18n-name-key="my-section.name" key="my-section" location="my-item-link"
weight="1000">
    <description key="my-section.description">The mySection Plugin</description>
</web-section>
<web-item name="myItem" i18n-name-key="my-item.name" key="my-item" section="system.top.navigation.bar"
weight="1000">
    <description key="my-item.description">The myItem Plugin</description>
    <label key="my-item.label"></label>
</web-item>
```

## Basic modules

There are three kinds of basic module:

-   A [web item](https://developer.atlassian.com/display/JIRASERVER/Web+item) module defines an individual link that is displayed at a particular location or section of the JIRA user interface. A web item might define an individual item within a JIRA drop-down menu or a button on the issue operations bar.
-   A [web section](/server/jira/platform/web-section) module defines a collection of links that is displayed together at a particular location of the JIRA user interface. A web section might define a group of items within a JIRA drop-down menu (separated by lines) or a group of buttons on the issue operations bar.
-   A [web panel](/server/jira/platform/web-panel) module defines a section of HTML content displayed on a JIRA page.

You can also insert custom web fragments into existing ones in JIRA (e.g. web sections which are 'built in' to JIRA). However, you cannot redefine JIRA's existing web fragments. We recommend downloading the <a href="https://my.atlassian.com/download/source/jira" class="external-link">JIRA source</a> (valid <a href="https://www.atlassian.com/licensing/purchase-licensing#source-1" class="external-link">JIRA license</a> required), so you can access the appropriate source files that define JIRA's existing web fragments. This will help you:

-   Alter the positions of your own web fragments
-   Help you ensure `key` values in your own `<web-items>` and `<web-sections>` remain unique.

Basic modules can be used in the following locations in the JIRA UI:

-   [Extension points for the 'View Issue' page](/server/jira/platform/issue-view-ui-locations)

-   [Extension points for the end-user UI](/server/jira/platform/general-navigation-ui-locations)

-   [Extension points for project configuration](/server/jira/platform/project-settings-ui-locations)

-   [Extension points for the admin console](https://developer.atlassian.com/display/JIRASERVER/Extension+points+for+the+admin+console)

## Advanced modules

Advanced modules have pre-defined locations in JIRA (e.g. Project tab panel module) or are used for a feature that is not related to the UI (e.g. Index document configuration module). These modules are also declared in your add-on descriptor, however you do not need to define a location for them.

In the list of modules below, any module which is not a basic module is an advanced module.

## List of modules

-   [Component Import Plugin Module](/server/jira/platform/component-import)
-   [Component Plugin Module](/server/jira/platform/component)
-   [Component Tab Panel Plugin Module](/server/jira/platform/component-tab-panel)
-   [Custom field plugin module](/server/jira/platform/custom-field)
-   [Dialog Box Plugin Module](/server/jira/platform/dialog-box-plugin-module-4227198.html)
-   [Gadget Plugin Module](/server/jira/platform/gadget)
-   [Index Document Configuration Plugin Module](/server/jira/platform/index-document-configuration)
-   [Issue Link Renderer Plugin Module](/server/jira/platform/issue-link-renderer)
-   [Issue Operations Plugin Module](/server/jira/platform/issue-operations-plugin-module-4227121.html)
-   [Issue Tab Panel Plugin Module](/server/jira/platform/issue-tab-panel)
-   [JIRA Agile LinkProvider Plugin Module](/server/jira/platform/jira-agile-linkprovider-plugin-module-3997697.html)
-   [JQL Function Plugin Module](/server/jira/platform/jql-function)
-   [Keyboard Shortcut Plugin Module](/server/jira/platform/keyboard-shortcut)
-   [Message Handler Plugin Module](/server/jira/platform/message-handler)
-   [Module Type Plugin Module](/server/jira/platform/module-type)
-   [Portlet Plugin Module - deprecated](/server/jira/platform/portlet-plugin-module-deprecated-4227185.html)
-   [Project Tab Panel Plugin Module](/server/jira/platform/project-tab-panel)
-   [Report Plugin Module](/server/jira/platform/report)
-   [REST Plugin Module Type](/server/jira/platform/rest-module-type)
-   [RPC Endpoint Plugin Module](/server/jira/platform/rpc-endpoint-plugin-module-4227189.html)
-   [Search Request View Plugin Module](/server/jira/platform/search-request-view)
-   [Servlet Context Listener Plugin Module](/server/jira/platform/servlet-context-listener)
-   [Servlet Context Parameter Plugin Module](/server/jira/platform/servlet-context-parameter)
-   [Servlet Filter Plugin Module](/server/jira/platform/servlet-filter)
-   [Servlet Plugin Module](/server/jira/platform/servlet)
-   [SiteMesh Decoration Plugin Module](/server/jira/platform/sitemesh-decoration)
-   [URL Routing Plugin Module](/server/jira/platform/url-routing)
-   [User Format Plugin Module](/server/jira/platform/user-format)
-   [User Profile Plugin Module](/server/jira/platform/user-profile)
-   [Version Tab Panel Plugin Module](/server/jira/platform/version-tab-panel)
-   [Web Item Plugin Module](/server/jira/platform/web-item)
-   [Web Panel Plugin Module](/server/jira/platform/web-panel)
-   [Web Resource Plugin Module](/server/jira/platform/web-resource)
-   [Web Section Plugin Module](/server/jira/platform/web-section)
-   [Webwork plugin module](/server/jira/platform/webwork)
-   [Workflow Plugin Modules](/server/jira/platform/workflow-modules)
