---
aliases:
- /server/jira/platform/changes-to-messagehandler-and-message-processing-services-8946842.html
- /server/jira/platform/changes-to-messagehandler-and-message-processing-services-8946842.md
category: devguide
confluence_id: 8946842
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=8946842
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=8946842
date: '2017-12-08'
legacy_title: Changes to MessageHandler and message processing services
platform: server
product: jira
subcategory: updates
title: Changes to MessageHandler and message processing services
---
# Changes to MessageHandler and message processing services

In JIRA 5.0 a number of classes related to message handling have been extracted into a **JIRA Mail Plugin** (JMP) which is bundled with JIRA.

Moreover, there are a number of functional changes around the configuration of incoming mail handlers, including a much better UI and support for a test run (dry run) - the user can test the behaviour of the handler by clicking **Test** on its configuration UI. As a result, the API has changed too.

{{% note %}}

Most of these changes are available from **JIRA 5.0 RC2**. Note that JIRA 5.0 RC1 included an API that was still unstable around the message handler and related classes. The API changed significantly in RC2.  
  
From the team working on JMP: Please accept our apologies for the drastic changes so late in the JIRA 5.0 lifecycle. With serious commitments for stability of the API for the whole 5.x lifetime, we decided to further polish the API before final the 5.0 version is released.

{{% /note %}}

## Easily pluggable message handlers

JIRA 5.0 (as of RC1, with further changes in RC2) supports a new plugin point: the `<message-handler>` plugin module. This plugin module is actually defined by the JIRA Mail Plugin. More details on how to use the new module and how to build a handler configuration UI are given in a tutorial: [Writing a Custom Message (Mail) Handler for JIRA](/server/jira/platform/creating-a-custom-mail-handler-for-jira).

## MessageHandler

In `com.atlassian.jira.service.util.handler.MessageHandler`, the `init()` and `handleMessage()` methods now take an additional argument.

In the case of `init()` you should use the new argument to report all problems while initializing the handler.  
The context `com.atlassian.jira.service.util.handler.MessageHandlerContext` is passed to `handleMessage`. It should be used also to report progress (main events) while processing the message by the handler. Additionally, it provides a few of the methods typically used when creating JIRA entities, which transparently honour the mode in which the handler is run. In a production run, they do mutate JIRA. In a test run (when a person is testing the behaviour of the handler by clicking **Test** on its configuration UI) they create dummy objects.

### Dependency Injection now supported while instantiating message handlers

The `MessageHandler` instances are now instantiated by an implementation of `com.atlassian.jira.service.util.handler.MessageHandlerFactory`. The factory shipped with JIRA (as part of JMP) is capable of injecting dependencies into created instances. So we highly recommend switching to a dependency injection strategy for cleaner design and better testability, rather than resorting to the old way of using static calls on the `ComponentAccessor` or `ComponentManager` class. Note also that the `ComponentManager` class is no longer part of the JIRA API.

### Moved and deprecated AbstractMessageHandler

The `AbstractMessageHandler` class traditionally used by some handlers has been moved to the JIRA Mail Plugin. The fully qualified names has therefore changed to `com.atlassian.jira.plugins.mail.handlers.AbstractMessageHandler`. Moreover, it has been deprecated.  
As we favour composition over inheritance, we encourage plugin developers to directly implement the simple `MessageHandler` interface and use the methods provided by the JIRA service and manager layer. For example, the newly added `com.atlassian.jira.service.util.handler.MessageUserProcessor`.

### AbstractCommentHandler also subject to change

The same recommendation applies to the `com.atlassian.jira.plugins.mail.handlers.AbstractCommentHandler`, which is a candidate for further changes at any time in the future.

## Deprecated `ImapService` and `PopService` service classes

In JIRA 5.0 we simplified the hierarchy of message handling services. Now there is one common super class: `com.atlassian.jira.service.services.file.AbstractMessageHandlingService`.

The above class has 2 descendants:

-   `com.atlassian.jira.service.services.mail.MailFetcherService` - responsible for fetching messages from mail (POP and IMAP, in both secure and insecure versions) and delegating the handling to the appropriate `MessageHandler`.
-   `com.atlassian.jira.service.services.file.FileService` - responsible for fetching messages from the local file system and delegating the handling to the appropriate `MessageHandler`.

You should not use or derive from the `ImapService` or `PopService` classes. We have left these classes in JIRA only to avoid nasty-looking errors in the log while starting up JIRA. An upgrade task in JIRA 5.0 detects all services using this class and changes them to `com.atlassian.jira.service.services.mail.MailFetcherService`.

## Available handlers are no longer configured via `imapservice.xml` and `popservice.xml`

Handlers now are treated in the same way regardless of the protocol which was used to fetch the message instance. The files `imapservice.xml` and `popservice.xml` remain in JIRA only for backward compatibility, allowing administrators to edit built-in mail handlers from the **Services** page.

You should not modify those files, unless you need to have your handler listed in the **Edit Service** screen (for FileService and MailFetcherService services).

## New JavaMail version

JIRA 5.0 comes with JavaMail 1.4.4 which also may slightly change the behaviour/capability while parsing messages.

## Known limitations

The new plugin module and API do not yet support the following features:

-   The ability for the plugin to render the content of the properties column in the **Mail Handlers** table on the **Incoming Mail** page.
-   The ability to declare that the configuration screen (the second step of the wizard, handler specific) should be rendered full screen rather than in a popup dialog. Those handlers that require sophisticated and complex configuration are advised to use this wizard screen as merely an entry point (link, logo, very basic configuration or button leading further) into full-screen configuration implemented in a standard way by the plugin (WebWork actions or servlets).

## Final notes

Leaving the static `imapservice.xml` and `popservice.xml` files and still allowing the user to edit the message service configuration in the old way from the JIRA **Services** page introduces one inconsistency: Custom mail handlers (plugged using new `<message-handler>` extension) will not be listed there. We hope to address this inconsistency soon, most probably by removing the capability to edit the mail service configuration and message handler parameters from the **Services** page. That problem is tracked under <a href="https://jira.atlassian.com/browse/JRA-26444" class="external-link">JRA-26444</a>.

Message processing services and associated message handlers should generally be configured *only* from the **Incoming Mail** administration screen, in the **Mail Handlers** section.
