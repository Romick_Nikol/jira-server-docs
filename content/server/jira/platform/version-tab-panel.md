---
aliases:
- /server/jira/platform/version-tab-panel-plugin-module-4227196.html
- /server/jira/platform/version-tab-panel-plugin-module-4227196.md
category: reference
confluence_id: 4227196
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=4227196
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=4227196
date: '2017-12-08'
legacy_title: Version Tab Panel Plugin Module
platform: server
product: jira
subcategory: modules
title: Version tab panel
---
# Version tab panel

<table>
<colgroup>
<col style="width: 40%" />
<col style="width: 60%" />
</colgroup>
<tbody>
<tr class="odd">
<td><p>Available:</p></td>
<td><p>Version Tab plugin modules are available in JIRA 3.100 and later.</p></td>
</tr>
<tr class="even">
<td>Deprecated:</td>
<td>This plugin module is not available in JIRA 7.1 and later. There is no replacement.</td>
</tr>
</tbody>
</table>

The Version Tab Panel plugin module allows you to add new tabs to the 'Browse Versions' page.

<img src="/server/jira/platform/images/versiontabpanels.png" class="image-center" />  
These tab panels ('Summary' and 'Popular Issues') are implemented as plugins. New version tab panels can be implemented to display version-specific info.

Here is an example panel module descriptor that is placed in [atlassian-plugin.xml](/server/framework/atlassian-sdk/configuring-the-plugin-descriptor/):

``` xml
<!--
The class defined should implement
com.atlassian.jira.plugin.versionpanel.VersionTabPanel
and it may be useful to use the functionality provided by
com.atlassian.jira.plugin.versionpanel.impl.GenericTabPanel.
-->
<version-tabpanel key="version-openissues-panel" i18n-name-key="versionpanels.openissues.name"
                      name="Open Issues Panel" class="com.atlassian.jira.plugin.versionpanel.impl.GenericTabPanel">
        <description key="versionpanels.openissues.description">Show the open issues for this version.</description>
        <label key="common.concepts.openissues"/>
        <!-- this is a number defining the order of all panels.
             The system panels are 10, 20, 30 and 40. -->
        <order>10</order>
        <!-- this template produces the HTML for the panel -->
        <resource type="velocity" name="view"
                  location="templates/plugins/jira/projectentitypanels/openissues-version-panel.vm"/>
        <!-- this properties files contains i18n keys for the panel -->
        <resource type="i18n" name="i18n" location="com.atlassian.jira.plugins.versionpanels.openissues"/>
    </version-tabpanel>
```
