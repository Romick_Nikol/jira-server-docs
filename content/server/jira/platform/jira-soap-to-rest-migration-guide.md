---
aliases:
- /server/jira/platform/jira-soap-to-rest-migration-guide-33728379.html
- /server/jira/platform/jira-soap-to-rest-migration-guide-33728379.md
category: devguide
confluence_id: 33728379
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=33728379
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=33728379
date: '2017-12-08'
legacy_title: JIRA SOAP to REST Migration Guide
platform: server
product: jira
subcategory: updates
title: JIRA SOAP to REST Migration Guide
---
# JIRA SOAP to REST Migration Guide

## Global configuration

<table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<thead>
<tr class="header">
<th>SOAP method</th>
<th>Recommended REST resource. REST method</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>getConfiguration(java.lang.String token)</td>
<td><p><a href="https://docs.atlassian.com/jira/REST/latest/#d2e4993" class="external-link">/rest/api/2/configuration</a></p>
<p>REST method: GET</p></td>
</tr>
<tr class="even">
<td>getServerInfo(java.lang.String token)</td>
<td><p><a href="https://docs.atlassian.com/jira/REST/7.0-SNAPSHOT/#api/2/serverInfo-getServerInfo" class="external-link">/rest/api/2/serverInfo</a></p>
<p>REST method: GET</p></td>
</tr>
<tr class="odd">
<td>getCustomFields(java.lang.String token)</td>
<td><p><a href="https://docs.atlassian.com/jira/REST/7.0-SNAPSHOT/#api/2/field-getFields" class="external-link">/rest/api/2/field</a></p>
<p>REST method: GET</p></td>
</tr>
<tr class="even">
<td>getIssueTypes(java.lang.String token) </td>
<td><p><a href="https://docs.atlassian.com/jira/REST/7.0-SNAPSHOT/#api/2/issuetype-getIssueAllTypes" class="external-link">/rest/api/2/issuetype</a></p>
<p>REST method: GET</p></td>
</tr>
<tr class="odd">
<td>getPriorities(java.lang.String token) </td>
<td><p><a href="https://docs.atlassian.com/jira/REST/7.0-SNAPSHOT/#api/2/priority-getPriorities" class="external-link">/rest/api/2/priority</a></p>
<p>REST method: GET</p></td>
</tr>
<tr class="even">
<td>getProjectsNoSchemes(java.lang.String token) </td>
<td><p><a href="https://docs.atlassian.com/jira/REST/7.0-SNAPSHOT/#api/2/project-getAllProjects" class="external-link">/rest/api/2/project</a></p>
<p>REST method: GET</p></td>
</tr>
<tr class="odd">
<td>getResolutions(java.lang.String token) </td>
<td><p><a href="https://docs.atlassian.com/jira/REST/7.0-SNAPSHOT/#api/2/resolution-getResolutions" class="external-link">/rest/api/2/resolution</a></p>
<p>REST method: GET</p></td>
</tr>
<tr class="even">
<td>getStatuses(java.lang.String token) </td>
<td><p><a href="https://docs.atlassian.com/jira/REST/7.0-SNAPSHOT/#api/2/status-getStatuses" class="external-link">/rest/api/2/status</a></p>
<p>REST method: GET</p></td>
</tr>
<tr class="odd">
<td>refreshCustomFields(java.lang.String token) </td>
<td><p>This method was intentionally omitted in REST and we do not intend to implement it in the future.</p>
<p>Rest method: n/a</p></td>
</tr>
</tbody>
</table>

## Issues

<table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<thead>
<tr class="header">
<th>SOAP method</th>
<th>Recommended REST resource. REST method</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>addBase64EncodedAttachmentsToIssue(java.lang.String token, java.lang.String issueKey, java.lang.String[] fileNames, java.lang.String[] base64EncodedAttachmentData) </td>
<td><p><span>/rest/api/2/issue/{issueIdOrKey}/attachments</span></p>
<p>REST method: POST</p></td>
</tr>
<tr class="even">
<td>addComment(java.lang.String token, java.lang.String issueKey, RemoteComment remoteComment) </td>
<td><p><a href="https://docs.atlassian.com/jira/REST/7.0-SNAPSHOT/#api/2/issue-addComment" class="external-link">/rest/api/2/issue/{issueIdOrKey}/comment</a></p>
<p>REST method: POST</p></td>
</tr>
<tr class="odd">
<td>createIssue(java.lang.String token, RemoteIssue rIssue) </td>
<td><p><a href="https://docs.atlassian.com/jira/REST/7.0-SNAPSHOT/#api/2/issue-createIssue" class="external-link">/rest/api/2/issue</a></p>
<p>REST method: POST</p></td>
</tr>
<tr class="even">
<td>createIssueWithParent(java.lang.String token, RemoteIssue rIssue, java.lang.String parentIssueKey) </td>
<td><p>Follow the documented instructions in <code>/rest/api/2/issue</code>  to create a sub-task</p>
<p>REST method: POST</p></td>
</tr>
<tr class="odd">
<td>createIssueWithSecurityLevel(java.lang.String token, RemoteIssue rIssue, java.lang.Long securityLevelId) </td>
<td><p><a href="https://docs.atlassian.com/jira/REST/7.0-SNAPSHOT/#api/2/issue-createIssue" class="external-link">/rest/api/2/issue</a></p>
<p>REST method:</p></td>
</tr>
<tr class="even">
<td>createIssueWithParentWithSecurityLevel(java.lang.String token, RemoteIssue rIssue, java.lang.String parentIssueKey, java.lang.Long securityLevelId) </td>
<td><p><a href="https://docs.atlassian.com/jira/REST/7.0-SNAPSHOT/#api/2/issue-createIssue" class="external-link">/rest/api/2/issue</a></p>
<p><a href="https://docs.atlassian.com/jira/REST/7.0-SNAPSHOT/#api/2/issue-createIssue" class="external-link">REST method: POST</a></p></td>
</tr>
<tr class="odd">
<td>deleteIssue(java.lang.String token, java.lang.String issueKey) </td>
<td><p><a href="https://docs.atlassian.com/jira/REST/7.0-SNAPSHOT/#api/2/issue-deleteIssue" class="external-link">/rest/api/2/issue/{issueIdOrKey}</a></p>
<p>REST method: DELETE</p></td>
</tr>
<tr class="even">
<td>editComment(java.lang.String token, RemoteComment remoteComment) </td>
<td><p><a href="https://docs.atlassian.com/jira/REST/7.0-SNAPSHOT/#api/2/issue-updateComment" class="external-link">/rest/api/2/issue/{issueIdOrKey}/comment/{id}</a></p>
<p>REST method: PUT</p></td>
</tr>
<tr class="odd">
<td>getAttachmentsFromIssue(java.lang.String token, java.lang.String issueKey) </td>
<td><p><a href="https://docs.atlassian.com/jira/REST/7.0-SNAPSHOT/#api/2/issue-getIssue" class="external-link">/rest/api/2/issue/{issueIdOrKey}?fields=attachment</a></p>
<p>REST method: GET</p></td>
</tr>
<tr class="even">
<td>getAvailableActions(java.lang.String token, java.lang.String issueKey) </td>
<td><p><a href="https://docs.atlassian.com/jira/REST/7.0-SNAPSHOT/#api/2/issue-getTransitions" class="external-link">/rest/api/2/issue/{issueIdOrKey}/transitions</a></p>
<p>REST method: GET</p></td>
</tr>
<tr class="odd">
<td>getComment(java.lang.String token, java.lang.Long id) </td>
<td><p><a href="https://docs.atlassian.com/jira/REST/7.0-SNAPSHOT/#api/2/issue-getComments" class="external-link">/rest/api/2/issue/{issueIdOrKey}/comment/{id}</a></p>
<p>REST method: GET</p></td>
</tr>
<tr class="even">
<td>getComments(java.lang.String token, java.lang.String issueKey) </td>
<td><p><a href="https://docs.atlassian.com/jira/REST/7.0-SNAPSHOT/#api/2/issue-getComments" class="external-link">/rest/api/2/issue/{issueIdOrKey}/comment</a></p>
<p>REST method: GET</p></td>
</tr>
<tr class="odd">
<td>getFieldsForAction(java.lang.String token, java.lang.String issueKey, java.lang.String actionIdString)  </td>
<td><p><a href="https://docs.atlassian.com/jira/REST/7.0-SNAPSHOT/#api/2/issue-getTransitions" class="external-link">/rest/api/2/issue/{issueIdOrKey}/transitions</a></p>
<p>REST method: GET</p></td>
</tr>
<tr class="even">
<td>getFieldsForCreate(java.lang.String token, java.lang.String projectKey, java.lang.Long issueTypeId) </td>
<td><p><a href="https://docs.atlassian.com/jira/REST/7.0-SNAPSHOT/#api/2/issue-getCreateIssueMeta" class="external-link">/rest/api/2/issue/createmeta</a></p>
<p>REST method: GET</p></td>
</tr>
<tr class="odd">
<td>getFieldsForEdit(java.lang.String token, java.lang.String issueKey) </td>
<td><p><a href="https://docs.atlassian.com/jira/REST/7.0-SNAPSHOT/#api/2/issue-getEditIssueMeta" class="external-link">/rest/api/2/issue/{issueIdOrKey}/editmeta</a></p>
<p>REST method: GET</p></td>
</tr>
<tr class="even">
<td>getIssue(java.lang.String token, java.lang.String issueKey) </td>
<td><p><a href="https://docs.atlassian.com/jira/REST/7.0-SNAPSHOT/#api/2/issue-getIssue" class="external-link">/rest/api/2/issue/{issueIdOrKey} </a>  </p>
<p>REST method: GET</p></td>
</tr>
<tr class="odd">
<td>getIssueById(java.lang.String token, java.lang.String issueId) </td>
<td><p><a href="https://docs.atlassian.com/jira/REST/7.0-SNAPSHOT/#api/2/issue-getIssue" class="external-link">/rest/api/2/issue/{issueIdOrKey} </a>  </p>
<p>REST method: GET</p></td>
</tr>
<tr class="even">
<td>getResolutionDateById(java.lang.String token, java.lang.Long issueId) </td>
<td><p><a href="https://docs.atlassian.com/jira/REST/7.0-SNAPSHOT/#api/2/issue-getIssue" class="external-link">/rest/api/2/issue/{issueIdOrKey}?fields=resolution</a></p>
<p>REST method: GET</p></td>
</tr>
<tr class="odd">
<td>getResolutionDateByKey(java.lang.String token, java.lang.String issueKey) </td>
<td><p><a href="https://docs.atlassian.com/jira/REST/7.0-SNAPSHOT/#api/2/issue-getIssue" class="external-link">/rest/api/2/issue/{issueIdOrKey}?fields=resolution</a></p>
<p>REST method: GET</p></td>
</tr>
<tr class="even">
<td>getSecurityLevel(java.lang.String token, java.lang.String issueKey) </td>
<td><p><a href="https://docs.atlassian.com/jira/REST/7.0-SNAPSHOT/#api/2/issue-getIssue" class="external-link">/rest/api/2/issue/{issueIdOrKey}?fields=security</a></p>
<p>REST method: GET</p></td>
</tr>
<tr class="odd">
<td>progressWorkflowAction(java.lang.String token, java.lang.String issueKey, java.lang.String actionIdString, RemoteFieldValue[] actionParams) </td>
<td><p><a href="https://docs.atlassian.com/jira/REST/7.0-SNAPSHOT/#api/2/issue-doTransition" class="external-link">/rest/api/2/issue/{issueIdOrKey}/transitions</a></p>
<p>REST method: POST</p></td>
</tr>
<tr class="even">
<td>updateIssue(java.lang.String token, java.lang.String issueKey, RemoteFieldValue[] actionParams) </td>
<td><p><a href="https://docs.atlassian.com/jira/REST/7.0-SNAPSHOT/#api/2/issue-editIssue" class="external-link">/rest/api/2/issue/{issueIdOrKey} </a>  </p>
<p>REST method: PUT</p></td>
</tr>
</tbody>
</table>

## Issue types

<table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<thead>
<tr class="header">
<th>SOAP method</th>
<th>Recommended REST resource. REST method</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>getIssueTypesForProject(java.lang.String token, java.lang.String projectId) </td>
<td><p><a href="https://docs.atlassian.com/jira/REST/7.0-SNAPSHOT/#api/2/issue-getCreateIssueMeta" class="external-link">/rest/api/2/issue/createmeta?project=</a>{projectIdOrKey}</p>
<p>REST method: GET</p></td>
</tr>
<tr class="even">
<td>getSubTaskIssueTypes(java.lang.String token) </td>
<td><p><a href="https://docs.atlassian.com/jira/REST/7.0-SNAPSHOT/#api/2/issuetype-getIssueAllTypes" class="external-link">/rest/api/2/issuetype</a></p>
<p>Supported only by filtering the response for all issue types.</p>
<p>REST method: GET</p></td>
</tr>
<tr class="odd">
<td>getSubTaskIssueTypesForProject(java.lang.String token, java.lang.String projectId) </td>
<td><p><a href="https://docs.atlassian.com/jira/REST/7.0-SNAPSHOT/#api/2/issue-getCreateIssueMeta" class="external-link">/rest/api/2/issue/createmeta?project=</a> <a href="https://docs.atlassian.com/jira/REST/latest/#d2e2262" class="external-link">{projectIdOrKey}</a></p>
<p>Filter for subtasks.</p>
<p>REST method: GET</p></td>
</tr>
</tbody>
</table>

## Login

<table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<thead>
<tr class="header">
<th>SOAP method</th>
<th>Recommended REST resource. REST method</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>login(java.lang.String username, java.lang.String password)</td>
<td><p>Intentionally omitted. JIRA will not support this.</p>
<p><a href="https://docs.atlassian.com/jira/REST/ondemand/#authentication" class="external-link">Read about authentication.<br />
</a></p>
<p>REST method: n/a</p></td>
</tr>
<tr class="even">
<td>logout(java.lang.String token) </td>
<td><p>Intentionally omitted. JIRA will not support this.</p>
<p>REST method: n/a</p></td>
</tr>
</tbody>
</table>

## Notifications

<table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<thead>
<tr class="header">
<th>SOAP method</th>
<th>Recommended REST resource. REST method</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>getAssociatedNotificationSchemes(java.lang.String token, RemoteProjectRole projectRole)  </td>
<td><p><span>/rest/api/2/project/{projectKeyOrId}/notificationscheme</span></p>
<p> </p>
<div class="aui-message warning">
<div class="icon"></div>
<p>Coming in JIRA 7.0 EAP</p>
</div>
<p>REST method: GET</p></td>
</tr>
<tr class="even">
<td>getNotificationSchemes(java.lang.String token) </td>
<td><p><a href="https://docs.atlassian.com/jira/REST/7.0-SNAPSHOT/#api/2/notificationscheme-getNotificationSchemes" class="external-link">/rest/api/2/notificationscheme</a></p>
<div class="aui-message warning">
<div class="icon"></div>
<p>Coming in JIRA 7.0 EAP</p>
</div>
<p>REST method: GET</p></td>
</tr>
</tbody>
</table>

## Permissions

<table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<thead>
<tr class="header">
<th><p>SOAP method</p></th>
<th><p>Recommended REST resource. REST method</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>addPermissionTo(java.lang.String token, RemotePermissionScheme permissionScheme, RemotePermission permission, RemoteEntity remoteEntity)  </p></td>
<td><p><a href="https://docs.atlassian.com/jira/REST/7.0-SNAPSHOT/#api/2/permissionscheme-createPermissionGrant" class="external-link">/rest/api/2/permissionscheme/{schemeId}/permission</a></p>
<div class="aui-message warning">
<div class="icon"></div>
<p>Coming in JIRA 7.0 EAP</p>
</div>
<p>REST method: POST</p>
<p> </p></td>
</tr>
<tr class="even">
<td><p>createPermissionScheme(java.lang.String token, java.lang.String name, java.lang.String description) </p></td>
<td><p><a href="https://docs.atlassian.com/jira/REST/7.0-SNAPSHOT/#api/2/permissionscheme-createPermissionScheme" class="external-link">/rest/api/2/permissionscheme</a></p>
<div class="aui-message warning">
<div class="icon"></div>
<p>Coming in JIRA 7.0 EAP</p>
</div>
<p>REST method: POST</p></td>
</tr>
<tr class="odd">
<td><p>deletePermissionFrom(java.lang.String token, RemotePermissionScheme permissionSchemeName, RemotePermission permission, RemoteEntity remoteEntity) </p></td>
<td><p><a href="https://docs.atlassian.com/jira/REST/7.0-SNAPSHOT/#api/2/permissionscheme-deletePermissionSchemeEntity" class="external-link">/rest/api/2/permissionscheme/{schemeId}/permission/{permissionId}</a></p>
<p> </p>
<div class="aui-message warning">
<div class="icon"></div>
<p>Coming in JIRA 7.0 EAP</p>
</div>
<p> </p>
<p>REST method: DELETE</p></td>
</tr>
<tr class="even">
<td><p>deletePermissionScheme(java.lang.String token, java.lang.String permissionSchemeName) </p></td>
<td><p><a href="https://docs.atlassian.com/jira/REST/7.0-SNAPSHOT/#api/2/permissionscheme-deletePermissionScheme" class="external-link">/rest/api/2/permissionscheme/{schemeId}</a></p>
<div class="aui-message warning">
<div class="icon"></div>
<p>Coming in JIRA 7.0 EAP</p>
</div>
<p>REST method: DELETE</p></td>
</tr>
<tr class="odd">
<td><p>getAllPermissions(java.lang.String token) </p></td>
<td><p><a href="https://docs.atlassian.com/jira/REST/7.0-SNAPSHOT/#api/2/-getAllPermissions" class="external-link">/rest/api/2/permissions</a> OR <a href="https://docs.atlassian.com/jira/REST/7.0-SNAPSHOT/#api/2/-getPermissions" class="external-link">/rest/api/2/mypermissions</a></p>
<div class="aui-message warning">
<div class="icon"></div>
<p>Coming in JIRA 7.0 EAP</p>
</div>
<p>REST method: GET</p></td>
</tr>
<tr class="even">
<td><p>getAssociatedPermissionSchemes(java.lang.String token, RemoteProjectRole projectRole) </p></td>
<td><p><span>/rest/api/2/project/{projectKeyOrId}/permissionscheme</span></p>
<div class="aui-message warning">
<div class="icon"></div>
<p>Coming in JIRA 7.0 EAP</p>
</div>
<p>REST method: GET</p></td>
</tr>
<tr class="odd">
<td><p>getPermissionSchemes(java.lang.String token) </p></td>
<td><p><a href="https://docs.atlassian.com/jira/REST/7.0-SNAPSHOT/#api/2/permissionscheme-getPermissionSchemes" class="external-link">/rest/api/2/permissionscheme</a></p>
<p> </p>
<div class="aui-message warning">
<div class="icon"></div>
<p>Coming in JIRA 7.0 EAP</p>
</div>
<p>REST method: GET</p></td>
</tr>
</tbody>
</table>

## Projects

<table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<thead>
<tr class="header">
<th>SOAP method</th>
<th>Recommended REST resource. REST method</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>getComponents(java.lang.String token, java.lang.String projectKey)   </td>
<td><p><a href="https://docs.atlassian.com/jira/REST/latest/#d2e2373" class="external-link">/rest/api/2/project/{projectIdOrKey}/components</a></p>
<p>REST method: GET</p></td>
</tr>
<tr class="even">
<td>addVersion(java.lang.String token, java.lang.String projectKey, RemoteVersion remoteVersion) </td>
<td><p><a href="https://docs.atlassian.com/jira/REST/latest/#d2e1940" class="external-link">/rest/api/2/version</a></p>
<p>REST method: POST</p></td>
</tr>
<tr class="odd">
<td>archiveVersion(java.lang.String token, java.lang.String projectKey, java.lang.String versionName, boolean archive) </td>
<td><p><a href="https://docs.atlassian.com/jira/REST/latest/#d2e2010" class="external-link">/rest/api/2/version/{versionId}</a></p>
<p>In request body:</p>
<p><code> {&quot;archived&quot;: true}</code></p>
<p>REST method: PUT</p></td>
</tr>
<tr class="even">
<td><p>createProject(java.lang.String token, java.lang.String key, java.lang.String name, java.lang.String description, java.lang.String url, java.lang.String lead, RemotePermissionScheme permissionScheme, RemoteScheme notificationScheme, RemoteScheme issueSecurityScheme) </p></td>
<td><p><a href="https://docs.atlassian.com/jira/REST/7.0-SNAPSHOT/#api/2/project-createProject" class="external-link">/rest/api/2/project</a></p>
<div class="aui-message warning">
<div class="icon"></div>
<p>Coming in JIRA 7.0 EAP</p>
</div>
<p>REST method: POST</p>
<p> </p></td>
</tr>
<tr class="odd">
<td>createProjectFromObject(java.lang.String token, RemoteProject rproject) </td>
<td><p><a href="https://docs.atlassian.com/jira/REST/7.0-SNAPSHOT/#api/2/project-createProject" class="external-link">/rest/api/2/project</a></p>
<div class="aui-message warning">
<div class="icon"></div>
<p>Coming in JIRA 7.0 EAP</p>
</div>
<p>REST method: POST</p></td>
</tr>
<tr class="even">
<td>deleteProject(java.lang.String token, java.lang.String projectKey) </td>
<td><p><a href="https://docs.atlassian.com/jira/REST/7.0-SNAPSHOT/#api/2/project-deleteProject" class="external-link">/rest/api/2/project/{projectId}</a></p>
<div class="aui-message warning">
<div class="icon"></div>
<p>Coming in JIRA 7.0 EAP</p>
</div>
<p>REST method: DELETE</p></td>
</tr>
<tr class="odd">
<td>deleteProjectAvatar(java.lang.String token, long avatarId) </td>
<td><p><a href="https://docs.atlassian.com/jira/REST/latest/#d2e2456" class="external-link">/rest/api/2/project/{projectIdOrKey}/avatar/{id}</a></p>
<p>REST method: DELETE</p></td>
</tr>
<tr class="even">
<td>getProjectAvatar(java.lang.String token, java.lang.String projectKey) </td>
<td><p><a href="https://docs.atlassian.com/jira/REST/latest/#d2e2394" class="external-link">/rest/api/2/project/{projectIdOrKey}/avatars</a></p>
<p>REST method: GET</p></td>
</tr>
<tr class="odd">
<td>getProjectAvatars(java.lang.String token, java.lang.String projectKey, boolean includeSystemAvatars) </td>
<td><p><a href="https://docs.atlassian.com/jira/REST/latest/#d2e2394" class="external-link">/rest/api/2/project/{projectIdOrKey}/avatars</a>  </p>
<p>REST method: GET</p></td>
</tr>
<tr class="even">
<td>getProjectById(java.lang.String token, java.lang.Long projectId) </td>
<td><p><a href="https://docs.atlassian.com/jira/REST/latest/#d2e2323" class="external-link">/rest/api/2/project/{projectIdOrKey}</a></p>
<p>REST method: GET</p></td>
</tr>
<tr class="odd">
<td>getProjectByKey(java.lang.String token, java.lang.String projectKey) </td>
<td><p><a href="https://docs.atlassian.com/jira/REST/latest/#d2e2323" class="external-link">/rest/api/2/project/{projectIdOrKey}</a>    </p>
<p>REST method: GET</p></td>
</tr>
<tr class="even">
<td>getVersions(java.lang.String token, java.lang.String projectKey) </td>
<td><p><a href="https://docs.atlassian.com/jira/REST/latest/#d2e2348" class="external-link">/rest/api/2/project/{projectIdOrKey}/versions</a></p>
<p>REST method: GET</p></td>
</tr>
<tr class="odd">
<td>releaseVersion(java.lang.String token, java.lang.String projectKey, RemoteVersion version) </td>
<td><p><a href="https://docs.atlassian.com/jira/REST/latest/#d2e2010" class="external-link">/rest/api/2/version/{id}</a></p>
<p>REST method: PUT</p></td>
</tr>
<tr class="even">
<td>setNewProjectAvatar(java.lang.String token, java.lang.String projectKey, java.lang.String contentType, java.lang.String base64ImageData) </td>
<td><p><a href="https://docs.atlassian.com/jira/REST/latest/#d2e4275" class="external-link">/rest/api/2/project/{projectIdOrKey}/avatar/temporary</a></p>
<p>REST method: POST</p></td>
</tr>
<tr class="odd">
<td>setProjectAvatar(java.lang.String token, java.lang.String projectKey, java.lang.Long avatarId) </td>
<td><p><a href="https://docs.atlassian.com/jira/REST/latest/#d2e2451" class="external-link">/rest/api/2/project/{projectIdOrKey}/avatar</a>    </p>
<p>REST method: PUT</p></td>
</tr>
<tr class="even">
<td>getProjectWithSchemesById(java.lang.String token, java.lang.Long projectId) </td>
<td><p><a href="https://docs.atlassian.com/jira/REST/7.0-SNAPSHOT/#api/2/project-getProject" class="external-link">/rest/api/2/project/{projectIdOrKey}</a>  </p>
<p>and calls to</p>
<p><span>/rest/api/2/project/{projectIdOrKey}/issuesecuritylevelscheme</span> <span></span></p>
<p><a href="https://docs.atlassian.com/jira/REST/latest/#d2e2262" class="external-link">/rest/api/2/project/{projectIdOrKey}</a> <span>/permissionscheme</span></p>
<p><a href="https://docs.atlassian.com/jira/REST/latest/#d2e2262" class="external-link">/rest/api/2/project/{projectIdOrKey}</a> <span>/notificationscheme</span> <span> </span> <a href="https://docs.atlassian.com/jira/REST/latest/#d2e2262" class="external-link"> </a></p>
<div class="aui-message warning">
<div class="icon"></div>
<p>Coming in JIRA 7.0 EAP</p>
</div>
<p>REST method: GET</p>
<p> </p></td>
</tr>
</tbody>
</table>

## Project roles

<table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<thead>
<tr class="header">
<th>SOAP method</th>
<th>Recommended REST resource. REST method</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>getProjectRole(java.lang.String token, java.lang.Long id) </td>
<td><p><a href="https://docs.atlassian.com/jira/REST/latest/#d2e1566" class="external-link">/rest/api/2/role/{id}</a></p>
<p>REST method: GET</p></td>
</tr>
<tr class="even">
<td>getProjectRoleActors(java.lang.String token, RemoteProjectRole projectRole, RemoteProject project) </td>
<td><p><a href="https://docs.atlassian.com/jira/REST/latest/#d2e5063" class="external-link">/rest/api/2/project/{projectIdOrKey}/role/{id}</a></p>
<p>REST method: GET</p></td>
</tr>
<tr class="odd">
<td>getProjectRoles(java.lang.String token) </td>
<td><p><a href="https://docs.atlassian.com/jira/REST/latest/#d2e1547" class="external-link">/rest/api/2/role</a></p>
<p>REST method: GET</p></td>
</tr>
<tr class="even">
<td>isProjectRoleNameUnique(java.lang.String token, java.lang.String name) </td>
<td><p>Intentionally omitted from REST. You can calculate this from the <a href="https://docs.atlassian.com/jira/REST/latest/#d2e1547" class="external-link">/rest/api/2/role</a> response.</p>
<p>REST method: n/a</p></td>
</tr>
<tr class="odd">
<td>removeActorsFromProjectRole(java.lang.String token, java.lang.String[] actors, RemoteProjectRole projectRole, RemoteProject project, java.lang.String actorType) </td>
<td><p><a href="https://docs.atlassian.com/jira/REST/latest/#d2e5132" class="external-link">/rest/api/2/project/{projectIdOrKey}/role/{id}</a></p>
<p>REST method: DELETE</p></td>
</tr>
<tr class="even">
<td>removeAllRoleActorsByNameAndType(java.lang.String token, java.lang.String name, java.lang.String type) </td>
<td><p>Can be done indirectly.</p>
<p>REST method: n/a</p></td>
</tr>
<tr class="odd">
<td>removeAllRoleActorsByProject(java.lang.String token, RemoteProject project) </td>
<td><p>Can be done indirectly.</p>
<p>REST method: n/a</p></td>
</tr>
<tr class="even">
<td>removeDefaultActorsFromProjectRole(java.lang.String token, java.lang.String[] actors, RemoteProjectRole projectRole, java.lang.String actorType) </td>
<td><p>Can be done indirectly.</p>
<p>REST method: n/a</p></td>
</tr>
<tr class="odd">
<td>updateProject(java.lang.String token, RemoteProject rProject) </td>
<td><p><a href="https://docs.atlassian.com/jira/REST/7.0-SNAPSHOT/#api/2/project-updateProject" class="external-link">/rest/api/2/project{projectIdOrKey}</a></p>
<div class="aui-message warning">
<div class="icon"></div>
<p>Coming in JIRA 7.0 EAP</p>
</div>
<p>REST method: PUT</p>
<p> </p></td>
</tr>
<tr class="even">
<td>updateProjectRole(java.lang.String token, RemoteProjectRole projectRole) </td>
<td><p><span>/rest/api/2/project/{projectIdOrKey}/role/{id}</span></p>
<div class="aui-message warning">
<div class="icon"></div>
<p>Coming in JIRA 7.0 EAP</p>
</div>
<p>REST method: PUT</p>
<p> </p></td>
</tr>
<tr class="odd">
<td>getDefaultRoleActors(java.lang.String token, RemoteProjectRole projectRole) </td>
<td><p><a href="https://docs.atlassian.com/jira/REST/latest/#d2e5063" class="external-link">/rest/api/2/project/{projectIdOrKey}/role/{id}</a></p>
<p>REST method: GET</p></td>
</tr>
<tr class="even">
<td>addActorsToProjectRole(java.lang.String token, java.lang.String[] actors, RemoteProjectRole projectRole, RemoteProject project, java.lang.String actorType) </td>
<td><p><a href="https://docs.atlassian.com/jira/REST/latest/#d2e5106" class="external-link">/rest/api/2/project/{projectIdOrKey}/role/{id}</a></p>
<p>There is also PUT alternative</p>
<p>REST method: POST</p></td>
</tr>
<tr class="odd">
<td>addDefaultActorsToProjectRole(java.lang.String token, java.lang.String[] actors, RemoteProjectRole projectRole, java.lang.String type) </td>
<td><p><a href="https://docs.atlassian.com/jira/REST/latest/#d2e5106" class="external-link">/rest/api/2/project/{projectIdOrKey}/role/{id}</a></p>
<p>There is also PUT alternative</p>
<p>REST method: POST</p></td>
</tr>
<tr class="even">
<td>createProjectRole(java.lang.String token, RemoteProjectRole projectRole) </td>
<td><p><a href="https://docs.atlassian.com/jira/REST/7.0-SNAPSHOT/#api/2/role-createProjectRole" class="external-link">/rest/api/2/role</a></p>
<div class="aui-message warning">
<div class="icon"></div>
<p>Coming in JIRA 7.0 EAP</p>
</div>
<p>REST method: POST</p></td>
</tr>
<tr class="odd">
<td>deleteProjectRole(java.lang.String token, RemoteProjectRole projectRole, boolean confirm) </td>
<td><p>/rest/api/2/role/{roleId}</p>
<p> </p>
<div class="aui-message warning">
<div class="icon"></div>
<p>Coming in JIRA 7.0 EAP</p>
</div>
<p>REST method: DELETE</p></td>
</tr>
</tbody>
</table>

## Search

<table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<thead>
<tr class="header">
<th>SOAP method</th>
<th>Recommended REST resource.</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>getFavouriteFilters(java.lang.String token) </td>
<td><p><a href="https://docs.atlassian.com/jira/REST/latest/#d2e3967" class="external-link">/rest/api/2/filter/favourite</a></p>
<p>REST method: GET</p></td>
</tr>
<tr class="even">
<td>getIssueCountForFilter(java.lang.String token, java.lang.String filterId) </td>
<td><p>Retrieve filter's JQL from:</p>
<p><a href="https://docs.atlassian.com/jira/REST/latest/#d2e3861" class="external-link">/rest/api/2/filter/{id}</a></p>
<p>then call:</p>
<p><a href="https://docs.atlassian.com/jira/REST/latest/#d2e5439" class="external-link">/rest/api/2/search</a></p>
<p>REST method: GET</p></td>
</tr>
<tr class="odd">
<td>getIssuesFromFilter(java.lang.String token, java.lang.String filterId) </td>
<td><p>Same as above.</p>
<p>REST method: GET</p></td>
</tr>
<tr class="even">
<td>getIssuesFromFilterWithLimit(java.lang.String token, java.lang.String filterId, int offSet, int maxNumResults) </td>
<td><p>Same as above.</p>
<p>REST method: GET</p></td>
</tr>
<tr class="odd">
<td>getIssuesFromJqlSearch(java.lang.String token, java.lang.String jqlSearch, int maxNumResults) </td>
<td><p><a href="https://docs.atlassian.com/jira/REST/latest/#d2e5439" class="external-link">/rest/api/2/search</a></p>
<p>REST method: GET</p></td>
</tr>
<tr class="even">
<td>getIssuesFromTextSearch(java.lang.String token, java.lang.String searchTerms) </td>
<td><p>Intentionally omitted. Use <code>text ~ &quot;foo&quot;</code> as a JQL query on <a href="https://docs.atlassian.com/jira/REST/latest/#d2e5439" class="external-link">/rest/api/2/search</a><code> REST method: GET </code></p></td>
</tr>
<tr class="odd">
<td>getIssuesFromTextSearchWithLimit(java.lang.String token, java.lang.String searchTerms, int offSet, int maxNumResults) </td>
<td><p>See above.</p>
<p>REST method: GET</p></td>
</tr>
<tr class="even">
<td>getIssuesFromTextSearchWithProject(java.lang.String token, java.lang.String[] projectKeys, java.lang.String searchTerms, int maxNumResults) </td>
<td><p>See above.</p>
<p>REST method: GET</p></td>
</tr>
<tr class="odd">
<td>getSavedFilters(java.lang.String token) </td>
<td><p><a href="https://docs.atlassian.com/jira/REST/latest/#d2e3967" class="external-link">/rest/api/2/filter/favourite</a></p>
<p>REST method:</p></td>
</tr>
</tbody>
</table>

## Security

<table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<thead>
<tr class="header">
<th>SOAP method</th>
<th>Recommended REST resource. REST method</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>getSecurityLevels(java.lang.String token, java.lang.String projectKey) </td>
<td><p><span>api/2/project/{projectKeyOrId}/securitylevel </span>  </p>
<div class="aui-message warning">
<div class="icon"></div>
<p>Coming in JIRA 7.0 EAP</p>
</div>
<p>REST method: GET</p></td>
</tr>
<tr class="even">
<td>getSecuritySchemes(java.lang.String token) </td>
<td><p><a href="https://docs.atlassian.com/jira/REST/7.0-SNAPSHOT/#api/2/issuesecurityschemes-getIssueSecuritySchemes" class="external-link">api/2/issuesecurityschemes</a></p>
<div class="aui-message warning">
<div class="icon"></div>
<p>Coming in JIRA 7.0 EAP</p>
</div>
<p>REST method: GET</p></td>
</tr>
</tbody>
</table>

## User management

<table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<thead>
<tr class="header">
<th>SOAP method</th>
<th>Recommended REST resource. REST method</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>removeUserFromGroup(java.lang.String token, RemoteGroup group, RemoteUser ruser) </td>
<td><p><a href="https://docs.atlassian.com/jira/REST/latest/#d2e2698" class="external-link">/rest/api/2/group/user</a></p>
<p>REST method: DELETE</p></td>
</tr>
<tr class="even">
<td>setUserPassword(java.lang.String token, RemoteUser user, java.lang.String newPassword) </td>
<td><p><a href="https://docs.atlassian.com/jira/REST/latest/#d2e4463" class="external-link">/rest/api/2/user/password</a></p>
<p>REST method: PUT</p></td>
</tr>
<tr class="odd">
<td>addUserToGroup(java.lang.String token, RemoteGroup group, RemoteUser ruser) </td>
<td><p><a href="https://docs.atlassian.com/jira/REST/latest/#d2e2671" class="external-link">/rest/api/2/group/user</a></p>
<p>REST method: POST</p></td>
</tr>
<tr class="even">
<td>createGroup(java.lang.String token, java.lang.String groupName, RemoteUser firstUser) </td>
<td><p><a href="https://docs.atlassian.com/jira/REST/latest/#d2e2580" class="external-link">/rest/api/2/group</a></p>
<p>REST method: POST</p></td>
</tr>
<tr class="odd">
<td>createUser(java.lang.String token, java.lang.String username, java.lang.String password, java.lang.String fullName, java.lang.String email) </td>
<td><p><a href="https://docs.atlassian.com/jira/REST/latest/#d2e4108" class="external-link">/rest/api/2/user</a></p>
<p>REST method: POST</p></td>
</tr>
<tr class="even">
<td>deleteGroup(java.lang.String token, java.lang.String groupName, java.lang.String swapGroupName) </td>
<td><p><a href="https://docs.atlassian.com/jira/REST/latest/#d2e2641" class="external-link">/rest/api/2/group</a>    </p>
<p>REST method: DELETE</p></td>
</tr>
<tr class="odd">
<td>deleteUser(java.lang.String token, java.lang.String username) </td>
<td><p><a href="https://docs.atlassian.com/jira/REST/latest/#d2e4178" class="external-link">/rest/api/2/user</a>    </p>
<p>REST method: DELETE</p></td>
</tr>
<tr class="even">
<td>getGroup(java.lang.String token, java.lang.String groupName) </td>
<td><p><a href="https://docs.atlassian.com/jira/REST/latest/#d2e2608" class="external-link">/rest/api/2/group</a>    </p>
<p>REST method: GET</p></td>
</tr>
<tr class="odd">
<td>getUser(java.lang.String token, java.lang.String username) </td>
<td><p><a href="https://docs.atlassian.com/jira/REST/latest/#d2e4081" class="external-link">/rest/api/2/user</a>    </p>
<p>REST method: GET</p></td>
</tr>
<tr class="even">
<td>updateGroup(java.lang.String token, RemoteGroup group) </td>
<td><p><a href="https://docs.atlassian.com/jira/REST/latest/#d2e4889" class="external-link">/rest/api/2/group</a>    </p>
<p>REST method: PUT</p></td>
</tr>
<tr class="odd">
<td>updateUser(java.lang.String token, RemoteUser user) </td>
<td><p><a href="https://docs.atlassian.com/jira/REST/7.0-SNAPSHOT/#api/2/myself-updateUser" class="external-link">/rest/api/2/user</a></p>
<div class="aui-message warning">
<div class="icon"></div>
<p>Coming in JIRA 7.0 EAP</p>
</div>
<p>REST method: PUT</p>
<p> </p></td>
</tr>
</tbody>
</table>

## Worklog

<table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<thead>
<tr class="header">
<th>SOAP method</th>
<th>Recommended REST resource. REST method</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>addWorklogAndAutoAdjustRemainingEstimate(java.lang.String token, java.lang.String issueKey, RemoteWorklog remoteWorklog) </td>
<td><p><a href="https://docs.atlassian.com/jira/REST/latest/#d2e1120" class="external-link">/rest/api/2/issue/{issueIdOrKey}/worklog</a></p>
<p>REST method: POST</p></td>
</tr>
<tr class="even">
<td>addWorklogAndRetainRemainingEstimate(java.lang.String token, java.lang.String issueKey, RemoteWorklog remoteWorklog) </td>
<td><p><a href="https://docs.atlassian.com/jira/REST/latest/#d2e1120" class="external-link">/rest/api/2/issue/{issueIdOrKey}/worklog</a></p>
<p>See documentation on including <code>leave </code>parameter</p>
<p>REST method: POST</p></td>
</tr>
<tr class="odd">
<td>addWorklogWithNewRemainingEstimate(java.lang.String token, java.lang.String issueKey, RemoteWorklog remoteWorklog, java.lang.String newRemainingEstimate) </td>
<td><p><a href="https://docs.atlassian.com/jira/REST/latest/#d2e1120" class="external-link">/rest/api/2/issue/{issueIdOrKey}/worklog</a></p>
<p>See documentation on including <code>new </code>parameter</p>
<p>REST method: POST</p></td>
</tr>
<tr class="even">
<td>deleteWorklogAndAutoAdjustRemainingEstimate(java.lang.String token, java.lang.String worklogId) </td>
<td><p><a href="https://docs.atlassian.com/jira/REST/latest/#d2e1210" class="external-link">/rest/api/2/issue/{issueIdOrKey}/worklog/{id}</a></p>
<p>REST method: DELETE</p></td>
</tr>
<tr class="odd">
<td>deleteWorklogAndRetainRemainingEstimate(java.lang.String token, java.lang.String worklogId) </td>
<td><p><a href="https://docs.atlassian.com/jira/REST/latest/#d2e1210" class="external-link">/rest/api/2/issue/{issueIdOrKey}/worklog/{id}</a></p>
<p>See documentation on including <code>leave </code> parameter</p>
<p>REST method: DELETE</p></td>
</tr>
<tr class="even">
<td>deleteWorklogWithNewRemainingEstimate(java.lang.String token, java.lang.String worklogId, java.lang.String newRemainingEstimate) </td>
<td><p><a href="https://docs.atlassian.com/jira/REST/latest/#d2e1210" class="external-link">/rest/api/2/issue/{issueIdOrKey}/worklog/{id}</a></p>
<p>See documentation on including <code>new </code> parameter</p>
<p>REST method: DELETE</p></td>
</tr>
<tr class="odd">
<td>getWorklogs(java.lang.String token, java.lang.String issueKey) </td>
<td><p><a href="https://docs.atlassian.com/jira/REST/latest/#d2e1103" class="external-link">/rest/api/2/issue/{issueIdOrKey}/worklog</a></p>
<p>REST method: GET</p></td>
</tr>
<tr class="even">
<td>hasPermissionToCreateWorklog(java.lang.String token, java.lang.String issueKey) </td>
<td><p><a href="https://docs.atlassian.com/jira/REST/latest/#d2e3537" class="external-link">/rest/api/2/mypermissions</a></p>
<p>REST method: GET</p></td>
</tr>
<tr class="odd">
<td>hasPermissionToDeleteWorklog(java.lang.String token, java.lang.String worklogId) </td>
<td><p><a href="https://docs.atlassian.com/jira/REST/latest/#d2e3537" class="external-link">/rest/api/2/mypermissions</a>    </p>
<p>REST method: GET</p></td>
</tr>
<tr class="even">
<td>hasPermissionToEditComment(java.lang.String token, RemoteComment remoteComment) </td>
<td><p><a href="https://docs.atlassian.com/jira/REST/latest/#d2e3537" class="external-link">/rest/api/2/mypermissions</a>    </p>
<p>REST method: GET</p></td>
</tr>
<tr class="odd">
<td>hasPermissionToUpdateWorklog(java.lang.String token, java.lang.String worklogId) </td>
<td><p><a href="https://docs.atlassian.com/jira/REST/latest/#d2e3537" class="external-link">/rest/api/2/mypermissions</a>    </p>
<p>REST method: GET</p></td>
</tr>
<tr class="even">
<td>updateWorklogAndAutoAdjustRemainingEstimate(java.lang.String token, RemoteWorklog remoteWorklog) </td>
<td><p><a href="https://docs.atlassian.com/jira/REST/latest/#d2e1175" class="external-link">/rest/api/2/issue/{issueIdOrKey}/worklog/{id}</a></p>
<p>REST method: PUT</p></td>
</tr>
<tr class="odd">
<td>updateWorklogAndRetainRemainingEstimate(java.lang.String token, RemoteWorklog remoteWorklog) </td>
<td><p><a href="https://docs.atlassian.com/jira/REST/latest/#d2e1175" class="external-link">/rest/api/2/issue/{issueIdOrKey}/worklog/{id}</a></p>
<p>See documentation on including <code>leave </code>parameter</p>
<p>REST method: PUT</p></td>
</tr>
<tr class="even">
<td>updateWorklogWithNewRemainingEstimate(java.lang.String token, RemoteWorklog remoteWorklog, java.lang.String newRemainingEstimate) </td>
<td><p><a href="https://docs.atlassian.com/jira/REST/latest/#d2e1175" class="external-link">/rest/api/2/issue/{issueIdOrKey}/worklog/{id}</a></p>
<p>See documentation on including <code>new </code>parameter</p>
<p>REST method: PUT</p></td>
</tr>
</tbody>
</table>

{{% note %}}

## Note on session invalidation

SOAP used to throw an exception when user's session expired. The JIRA REST API returns an **X-AUSERNAME **header. The header's value will either be username of logged in user or 'anonymous'. 

{{% /note %}}
