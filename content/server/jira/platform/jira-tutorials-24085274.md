---
title: JIRA Tutorials 24085274
aliases:
    - /server/jira/platform/jira-tutorials-24085274.html
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=24085274
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=24085274
confluence_id: 24085274
platform: server
product: jira
category: devguide
subcategory:
date: "2018-04-25"
---
# JIRA Developer Documentation : JIRA Tutorials

{{% note %}}

Redirection Notice

This page will redirect to <https://developer.atlassian.com/x/SYB> in about 2 seconds.

{{% /note %}}
