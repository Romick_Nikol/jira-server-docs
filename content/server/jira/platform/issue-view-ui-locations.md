---
aliases:
- /server/jira/platform/view-issue-page-locations-4227094.html
- /server/jira/platform/view-issue-page-locations-4227094.md
category: reference
confluence_id: 4227094
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=4227094
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=4227094
date: '2017-12-08'
legacy_title: View Issue Page Locations
platform: server
product: jira
subcategory: modules
title: Issue view UI locations
---
# Issue view UI locations

<table>
<colgroup>
<col style="width: 20%" />
<col style="width: 80%" />
</colgroup>
<tbody>
<tr class="odd">
<td><h6 id="ViewIssuePageLocations-Available:">Available:</h6></td>
<td><p>JIRA 4.4 and later -- the <code>atl.jira.view.issue.right.context</code> location is available.<br />
JIRA 4.1 and later -- the <code>opsbar-operations</code> and <code>opsbar-transitions</code> locations are available.</p></td>
</tr>
</tbody>
</table>

## Issue Operations Bar Locations

{{% note %}}

Available in JIRA 4.1 and later.

{{% /note %}}

The two **`opsbar-operations`** and **`opsbar-transitions`** locations together define web sections and items in the issue operations bar, which is visible on the 'View Issue' page.

<img src="/server/jira/platform/images/4391012.png" class="gliffy-macro-image" />

You can add new web items to existing web sections within the `opsbar-operations` and `opsbar-transitions` locations of the issue operations bar (see [Existing Web Sections of the Issue Operations Bar Locations](#existing-web-sections-of-the-issue-operations-bar-locations)). You can also add new web sections to the 'More Actions' drop-down menu (in the `opsbar-operations` location).

{{% note %}}

A web item is rendered as either a button or a link on a drop-down menu, based on the web section to which the item is added.

{{% /note %}}

###  Adding New Items to Existing Web Sections

To add your own web item to an existing web section of one of JIRA's issue operations bar locations, your web item must include a `section` attribute whose value is one of the sections specified in the [Existing Web Sections of the Issue Operations Bar Locations](#existing-web-sections-of-the-issue-operations-bar-locations) diagram.

For example, to add a web item module that defines a new item in the 'operations-operations' section, your web item module would contain:

``` xml
<web-item ... section="operations-operations" ...>
    ...
</web-item>
```

{{% note %}}

Web items added to the 'operations-top-level' section will appear in the top level of the `opsbar-operations` location.

{{% /note %}}

To add an item to the `opsbar-transitions` location, the value of your web item's `section` attribute must be `"transitions-all"`. Referencing the `opsbar-transitions` location explicitly in your web item definition will not work.

#### Existing Web Sections of the Issue Operations Bar Locations

<img src="/server/jira/platform/images/4391013.png" class="gliffy-macro-image" />

### Adding Custom Sections and Items

To add your own section to the 'More Actions' drop-down menu, define a web section that includes a `location` attribute with the value `"opsbar-operations"`.

For example, your web section module would contain:

``` xml
<web-section key="my-custom-more-actions-section" ... location="opsbar-operations" ...>
    ...
</web-section>
```

{{% note %}}

It is not possible to add your own web sections to the `opsbar-transitions` location.

{{% /note %}}

Adding new web items to your own sections is similar to the method [above](#adding-new-items-to-existing-web-sections) for adding new web items to existing web sections. However, specify the value of your web section's `key` attribute as the value of your web item's `section` attribute. Hence, following on from the previous example, your web item would contain:

``` xml
<web-item ... section="my-custom-more-actions-section" ...>
    ...
</web-item>
```

### Customising the Positions of Items and Sections

To customise the position of your own web items or sections throughout the issue operations bar, add a `weight` attribute to your `<web-item>` or `<web-section>` and adjust its value with respect to JIRA's existing web items or sections.

-   For web items added to the 'operations-top-level' section, lower weight values result in buttons appearing closer to the left of the user interface.
-   For web items and sections which target the 'More Actions' drop-down menu, lower weight values result in these items/sections appearing higher up the drop-down menu.
-   For web items added to the 'transitions-all' section, lower weight values result in these items appearing closer to the left and then higher up the 'Workflow' drop-down menu.

    {{% note %}}

Note that these values are overridden by `opsbar-sequence` property keys, which can be specified by a JIRA administrator through the administration console. See Customising workflow transitions on the 'View Issue' page for details.

    {{% /note %}}

### Source File

To find the values of JIRA's existing web items and sections for the `opsbar-operations` and `opsbar-transitions` locations, view the following file in JIRA's source archive:  
`<source-installation-directory>/jira-project/jira-components/jira-core/src/main/resources/system-issueoperations-plugin.xml`

## Right Side of the 'View Issue' Page Location

{{% note %}}

Available in JIRA 4.4 and later.

{{% /note %}}

The **`atl.jira.view.issue.right.context`** location defines web panels for the right hand side of the 'View Issue' page.

![](/server/jira/platform/images/web-fragments-view-issue-page-right-panels.png)

### Adding Custom Panels

To add your own web panel to the summary tab location of JIRA's project configuration administration, your web panel must include a location attribute whose value is `"atl.jira.view.issue.right.context"`.

For example:

``` xml
<web-panel ... location="atl.jira.view.issue.right.context" ...>
    ...
        <label key="my-new-panel.title"/>
    ...
</web-panel>
```

#### Panel Headings

If you wish to create a 'panel label' (i.e. the collapsible heading which appears at the top of the panel), define a `<label>` element with a `key` attribute whose value defines a unique property (i.e. `my-new-panel.title` based on the example above). The string value of this unique property (defined in your plugin's `i18n.properties` file) will be shown in bold text at the top of your section.

If you do not wish a heading to appear at the top of the panel, include the following child element in your `<web-panel>` module:

``` xml
<param name="headless">true</param>
```

### Customising the Positions of Panels

To customise the position of your own web panels in this location, add a `weight` attribute to your `<web-panel>` and adjust its value with respect to JIRA's existing web panels. Lower weight values result in these panels appearing higher up the right hand side of the 'View Issue' page.

### Source File

To find the values of JIRA's existing web panels for the `atl.jira.view.issue.right.context` location, view the following file in JIRA's source archive:  
`<source-installation-directory>/jira-project/jira-components/jira-plugins/jira-view-issue-plugin/src/main/resources/atlassian-plugin.xml`
