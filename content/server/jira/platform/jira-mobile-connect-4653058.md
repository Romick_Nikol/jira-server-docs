---
title: JIRA Mobile Connect 4653058
aliases:
    - /server/jira/platform/jira-mobile-connect-4653058.html
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=4653058
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=4653058
confluence_id: 4653058
platform: server
product: jira
category: devguide
subcategory:
date: "2018-04-25"
---
# JIRA Developer Documentation : JIRA Mobile Connect

JIRA Mobile Connect (JMC) is an iOS library that can be embedded into any iOS app to provide following extra functionality:

-   **Real-time crash reporting**, have users or testers submit crash reports directly to your JIRA instance.
-   **User or tester feedback** views that allow users or testers to create bug reports within your app.
-   **Rich data input**, users can attach and annotate screenshots, leave a voice message, and have their location sent.
-   **Two-way communication with users**, thank your users or testers for providing feedback on your app!

To learn more, head to the repository: <a href="https://bitbucket.org/atlassian/jiraconnect-apple" class="uri external-link">https://bitbucket.org/atlassian/jiraconnect-apple</a>

 

<img src="/server/jira/platform/images/jmc-small-crash-report.png" height="250" /><img src="/server/jira/platform/images/jmc-small-report-issue.png" height="250" /><img src="/server/jira/platform/images/jmc-small-replies-view.png" height="250" />
