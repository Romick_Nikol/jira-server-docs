---
aliases:
- /server/jira/platform/developing-against-jira-with-secure-administrator-sessions-4227159.html
- /server/jira/platform/developing-against-jira-with-secure-administrator-sessions-4227159.md
category: devguide
confluence_id: 4227159
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=4227159
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=4227159
date: '2017-12-08'
legacy_title: Developing against JIRA with Secure Administrator Sessions
platform: server
product: jira
subcategory: other
title: Developing against JIRA with secure administrator sessions
---
# Developing against JIRA with secure administrator sessions

Secure administrator sessions (also known as 'websudo') is a security feature introduced in JIRA 4.3. This provides an additional layer of authentication for administration functions. If you are developing a plugin for JIRA 4.3 or later, you will need to take note of the information below.

#### Plugin Development

[WebWork](/server/jira/platform/webwork) actions that require administration access can be protected in JIRA with the <a href="http://docs.atlassian.com/sal-api/2.4.0/sal-api/apidocs/com/atlassian/sal/api/websudo/WebSudoRequired.html" class="external-link">WebSudoRequired</a> annotation. This annotation must be on the class not the package nor a method in the class. (Note: WebSudoNotRequired annotation is not used in JIRA.)

Other plugin types, such as [REST services](/server/jira/platform/rest-module-type) or [servlets](/server/jira/platform/servlet), can be protected via websudo by following the instructions at: [Adding WebSudo Support to your Plugin](https://developer.atlassian.com/display/DOCS/Adding+WebSudo+Support+to+your+Plugin).
