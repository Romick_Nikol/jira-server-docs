---
title: "Creating an AJAX dialog"
aliases:
    - /server/jira/platform/tutorial-how-to-create-an-ajax-dialog-4227197.html
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=4227197
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=4227197
confluence_id: 4227197
platform: server
product: jira
category: devguide
guides: tutorials
legacy_title: Tutorial How to Create an Ajax Dialog 4227197
subcategory: learning
date: "2018-04-25"
---
# Creating an AJAX dialog

{{% tip %}}

Level of experience: Intermediate

Our tutorials are classified as 'beginner', 'intermediate' and 'advanced'. This one is at 'intermediate' level. If you have never developed a plugin before, you may find this one a bit difficult.

{{% /tip %}}

## Overview

This tutorial shows you how to develop an AJAX Dialog in JIRA. To do this, you create an add watchers plugin, a dialog that allows a user to specify watchers on the view issue page in JIRA or add other users to as watchers on the view issue page. As with all plugins, your plugin will consist of the following components:

-   Java classes encapsulating the plugin logic
-   Resources for display of the plugin UI
-   Plugin descriptor to enable the plugin module in JIRAi

All these components will be contained within a single JAR file. Each component is further discussed in the examples below.

## Required Knowledge

To complete this tutorial, you must already understand the basics of Java development: classes, interfaces, methods, how to use the compiler, and so on. You should also understand:

-   How to create an Atlassian plugin project using the [Atlassian Plugin SDK](https://developer.atlassian.com/display/DOCS/Working+with+the+SDK).
-   How to open the plugin project in your IDE.
-   How to compile your project and create a JAR file using Maven.

### Plugin Source

We encourage you to work through this tutorial. If you want to skip ahead or check your work when you are done, you can find the plugin source code on Atlassian Bitbucket. Bitbucket serves a public Git repository containing the tutorial's code. To clone the repository, issue the following command:

    $ git clone https://atlassian_tutorial@bitbucket.org/atlassian_tutorial/jira-dialog-tutorial.git

Alternatively, you can download the source using the **Downloads** page here: <a href="https://bitbucket.org/atlassian_tutorial/jira-dialog-tutorial" class="uri external-link">https://bitbucket.org/atlassian_tutorial/jira-dialog-tutorial</a>

## Step 1. Create the Plugin Project

Use the appropriate `atlas-create-`*`application`*`-plugin` command to create your plugin. For example, `atlas-create-jira-plugin` or `atlas-create-confluence-plugin`.

When you create the plugin using the above SDK, you will be prompted for some information to identify your plugin. Enter the following information:

-   **group-id**: `com.example.dialog`
-   **artifact-id**:
-   **version**: `1.0`
-   **package**: `com.example.dialog`

Follow these steps to build and install your plugin, so that you can test your code. If you have not already started the application, start it now:

-   Open a command window and go to the plugin root folder (where the `pom.xml` is located).
-   Run `atlas-run` (or `atlas-debug` if you might want to launch the debugger in your IDE).

From this point onwards, you can use [QuickReload](https://developer.atlassian.com/docs/developer-tools/automatic-plugin-reinstallation-with-quickreload) to reinstall your plugin behind the scenes as you work, simply by rebuilding your plugin.

{{% warning %}}

FastDev and atlas-cli have been deprecated. Please use [Automatic Plugin Reinstallation with QuickReload](https://developer.atlassian.com/docs/developer-tools/automatic-plugin-reinstallation-with-quickreload) instead.

{{% /warning %}}

 

To trigger the reinstallation of your plugin:

1.  Make the changes to your plugin module.
2.  Open the Developer Toolbar.
    <img src="/server/jira/platform/images/fastdev1.png" width="600" />
3.  Press the FastDev icon.
    <img src="/server/jira/platform/images/fastdev2.png" width="600" />
    The system rebuilds and reloads your plugin:
    <img src="/server/jira/platform/images/fastdev3.png" width="600" />

Use live reload to view real-time updates to templates and other resources:

1.  Open the Developer Toolbar.
2.  Press the live reload icon.
    The  icon starts to revolve indicating it is on.
3.  Edit your project resources.
4.  Save your changes:
    Back in the host application, your plugin displays any user visible changes you make. 

Go back to the browser. The updated plugin has been installed into the application, and you can test your changes.

The full instructions are in the [SDK guide](https://developer.atlassian.com/display/DOCS/Working+with+the+SDK).

## Step 2. Add Plugin Metadata to the POM

Now you need to edit your POM (Project Object Model definition file) to add some metadata about your plugin and your company or organisation.

1.  Edit the `pom.xml` file in the root folder of your plugin.
2.  Add your company or organisation name and your website to the `<organization>`element:

    ``` javascript
    <organization>
        <name>Example Company</name>
        <url>http://www.example.com/</url>
    </organization>
    ```

3.  Update the `<description>`element:

    ``` javascript
    <description>This plugin allows a user to specify watchers via a dialog on the view issue page</description>
    ```

4.  Save the file.

## Step 3. Register the Plugin Module in the Plugin Descriptor

Now you will add the required plugin modules to your plugin descriptor at `src/main/resources/atlassian-plugin.xml`. The plugin descriptor is an XML file that identifies the plugin to JIRA and defines the functionality that the plugin requires.

Here's a basic plugin descriptor:

``` javascript
<atlassian-plugin key="${project.groupId}.${project.artifactId}" name="${project.artifactId}" plugins-version="2">
    <plugin-info>
        <description>${project.description}</description>
        <version>${project.version}</version>
        <vendor name="${project.organization.name}" url="${project.organization.url}" />
    </plugin-info>
</atlassian-plugin>
```

Note that some of the information from the POM is transferred to the plugin descriptor using variable names such as `${project.artifactId`}.

Now you are ready to move onto writing some code to make your plugin do something.

## Step 4. Add internalization support

With any good plugin, you will want to add support for different languages. 

To do this, you will need to add a i18n resource.

1.  Create a package named **com.example.dialog** in the resources folder
2.  Create a file named **i18n.properties** in the package you just created
3.  Edit **atlassian-plugin.xm**l, add a new resource that is a child of the atlassian-plugin element (see below)

``` javascript
<atlassian-plugin ...>
...
<resource type="i18n" name="i18n" location="com.example.dialog" />
...
</atlassian-plugin>
```

## Step 5. Add link to view issue page

You will need a way to trigger the dialog. You will be adding a *triggering link* to the voters and watchers section of the more actions menu. 

To do this, you will write a [web-item module](/server/jira/platform/web-item). A web-item module is a descriptor for a HTML &lt;a&gt; tag (link). 

Edit **atlassian-plugin.xml**, add a new module that is a child of the atlassian-plugin element (see below)

``` javascript
<atlassian-plugin ...>
...
<web-item key="tutorial.operations.webitem" name="My Dialog Item" section="operations-voteswatchers" weight="1">
    <label key="tutorial.labels.add.watchers" />
    <link linkId="add-watchers">/AddWatchers!default.jspa?id=${issue.id}</link>
</web-item>
...
</atlassian-plugin>
```

The table below only contains attributes/elements meaningful to the code example above. For complete documentation see [web-item module](/server/jira/platform/web-item).

<table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<thead>
<tr class="header">
<th><p>Attribute</p></th>
<th><p>Description</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>Section</p></td>
<td><p>This is the where our <em>triggering link</em> will be placed in the JIRA User Interface (<strong>operations-voteswatchers</strong> section)<br />
 <br />
<img src="/server/jira/platform/images/voteswatchers.jpg" /></p></td>
</tr>
<tr class="even">
<td><p>Element</p></td>
<td><p>Description</p></td>
</tr>
<tr class="odd">
<td><p>label</p></td>
<td><ul>
<li><strong>key</strong> attribute refers to a i18n key (in the properties file you setup in step 4)</li>
</ul></td>
</tr>
<tr class="even">
<td><p>link</p></td>
<td><p>Contains url to your action (we will set this up later). This value is rendered as a velocity template, giving you access to it's field. <strong>You will need to use this to add the issueId request parameter</strong>.</p>
<ul>
<li><strong>linkId -</strong> the value of the HTML id attribute (used to later bind JavaScript too)</li>
</ul></td>
</tr>
</tbody>
</table>

On the view issue page, your *triggering link* should appear under the *more actions* menu.

![](/server/jira/platform/images/picture-27.png)

## Step 6. Add JavaScript web-resource

Clicking your *triggering link* you will take you to an error page. Forget about the error for now, concentrate on getting a dialog to display. 

To do this, you will need to add JavaScript to the page. This is done by writing a [Web Resource Plugin Module](https://developer.atlassian.com/display/DOCS/Web+Resource+Plugin+Module).

1.   Create a new file named **tutorial.js** under the **resources** directory.
2.   Edit **atlassian-plugin.xml**, add a new module that is a child of the atlassian-plugin element (see below)

    ``` javascript
    <atlassian-plugin ...>
    ...
    <web-resource name="Resources" key="resources">
        <resource name="tutorial.js" type="download" location="/js/tutorial.js"/>
        <context>atl.general</context>
    </web-resource>
    ...
    </atlassian-plugin>
    ```

    <table>
    <colgroup>
    <col style="width: 50%" />
    <col style="width: 50%" />
    </colgroup>
    <thead>
    <tr class="header">
    <th><p>Element</p></th>
    <th><p>Description</p></th>
    </tr>
    </thead>
    <tbody>
    <tr class="odd">
    <td><p>resource</p></td>
    <td><p><strong>location</strong> - The location of the JavaScript file you created. (This path is relative to the resources directory)</p></td>
    </tr>
    <tr class="even">
    <td><p>context</p></td>
    <td><p>On which page the JavaScript file should be added. <strong>atl.general</strong> will add it to all pages.</p></td>
    </tr>
    </tbody>
    </table>

On the view issue page, confirm your JavaScript file is present by adding alert("test") to the top of your JavaScript file.

![](/server/jira/platform/images/picture-29.png)

## Step 7. Bind Dialog to Triggering Link

Now that you can include JavaScript on the page, you need to use it to bind a dialog to your *triggering link.*

To do this, add the following snippet to your JavaScript file:

``` javascript
jQuery(function () {
    new AJS.FormPopup({
        id: "add-watchers-dialog",
        trigger: "#add-watchers"
    });
});
```

-   Wrapping a function in jQuery() delays the functions execution until the entire page has loaded and your trigger link is available to bind to.
-   AJS.FormPopup is an Atlassian JavaScript control that launches a dialog from a specified trigger. It accepts an object of options.
-   The AJS.FormPopup control **uses the** ***triggering link*'s href attribute** to form the request

    <table>
    <colgroup>
    <col style="width: 33%" />
    <col style="width: 33%" />
    <col style="width: 33%" />
    </colgroup>
    <thead>
    <tr class="header">
    <th><p>Option</p></th>
    <th><p>Required</p></th>
    <th><p>Description</p></th>
    </tr>
    </thead>
    <tbody>
    <tr class="odd">
    <td><p>id</p></td>
    <td><p>Yes</p></td>
    <td><p>ID attribute specified on the Dialog DOM node</p></td>
    </tr>
    <tr class="even">
    <td><p>trigger</p></td>
    <td><p>Yes</p></td>
    <td><p>A jQuery selector that locates your <em>triggering link</em></p></td>
    </tr>
    <tr class="odd">
    <td><p>ajaxOptions</p></td>
    <td><p>No</p></td>
    <td><p>Allows you to specify additional request options (See <a href="http://api.jquery.com/category/ajax/" class="external-link">jQuery AJAX</a>)</p></td>
    </tr>
    </tbody>
    </table>

    {{% tip %}}

    Response code handling

    200 - dialog will remain open and the content replaced.
    204 - dialog will close and the page refreshed

    {{% /tip %}}


    On the view issue page, clicking your link should now open a dialog.

![](/server/jira/platform/images/picture-28.png)

## Step 8. Add Java Action

In the previous screenshot, you may have noticed a **Communications Breakdown Error** message in your dialog. This is because we need to create an action.

To do this, you will write a [Webwork plugin module](/server/jira/platform/webwork).

1.  Create **AddWatchers** class in the **com.example.dialog** package.

    ``` javascript
    public class AddWatchers extends AbstractIssueSelectAction
    {
        private final WatcherManager watcherManager;
        private final BeanFactory beanFactory;
        private String[] watcherUserNames = new String[0];
        private List<String> validUsernames = new ArrayList<String>();

        @Override
        public String doDefault() throws Exception
        {
            return INPUT;
        }
    }
    ```

2.  Register your action, add a new module that is a child of the atlassian-plugin element.

    ``` javascript
    <atlassian-plugin ...>
    ...
    <webwork1 key="tutorial.actions" name="Adds Watchers" class="java.lang.Object">
        <actions>
                <action name="com.example.dialog.AddWatchers" alias="AddWatchers">
                    <view name="input">/templates/add-watchers.vm</view>
                </action>
            </actions>
        </webwork1>
    ...
    </atlassian-plugin>
    ```


    For a full description of all the **elements** in the code snippet above see <a href="http://developer.atlassian.com/display/JIRADEV/Webwork+plugin+module#WebItemPluginModule-Elements" class="external-link">webwork elements</a>. Below are the elements that are meaningful to this tutorial.

    <table>
    <colgroup>
    <col style="width: 50%" />
    <col style="width: 50%" />
    </colgroup>
    <thead>
    <tr class="header">
    <th><p>Element</p></th>
    <th><p>Description</p></th>
    </tr>
    </thead>
    <tbody>
    <tr class="odd">
    <td><p>action</p></td>
    <td><ul>
    <li><strong>name</strong> - Fully qualified Java Class<br />
    <strong>alias</strong> - Name used in URL |</li>
    </ul></td>
    </tr>
    <tr class="even">
    <td><p>view</p></td>
    <td><p>Path to velocity template (Path relative to resources directory)</p></td>
    </tr>
    </tbody>
    </table>

3.  Finally, your need to create a view (velocity template) for your action. Create a file under the Resources directory named add-watchers.vm.
4.  Edit *add-watchers.vm*, add a header tag.

    ``` xml
    <h2>Hello World!</h2>
    ```

{{% tip %}}

The first heading tag, for example &lt;h2&gt;, will be used as the title of the dialog.

{{% /tip %}}

On the view issue page, clicking your *triggering link* should now reveal a *Hello World* dialog

![](/server/jira/platform/images/picture-30.png)

## Step 9. Add Form Markup

Having a blank dialog is no good. It is time for you to now add the form that will submit a comma separated list of *username's* back to the action.

For a dialog form, the HTML **MUST follow this schema**.

``` javascript
<h2 class="dialog-title">[TITLE HERE]</h2>
<form class="aui" action="[ACTION HERE]" method="post">
    <div class="content-body"><!-- [FIELDS HERE] --></div>
    <div class="content-footer"><!-- [SUBMIT & CANCEL BUTTONS HERE]</div>
</form>
```

Considering this, your HTML should look similar to below:

``` javascript
<h2 class="dialog-title">$i18n.getText('tutorial.labels.add.watchers')</h2>

<form class="aui" action="AddWatchers.jspa" method="post">

    <input type="hidden" name="id" value="$action.id" />

    <div class="content-body">

        <div class="field-group">
            <label>$i18n.getText('tutorial.fields.users.label') <span class="aui-icon icon-required"></span></label>
            &nbsp;<textarea name="watcherUserNames" class="textarea long-field"></textarea>
            <div class="description">$i18n.getText('tutorial.fields.users.desc'))</div>
        </div>
    </div>

    <div class="buttons-container content-footer">
        <div class="buttons">
            <a href="#" class="cancel">$i18n.getText('tutorial.common.cancel')</a>
            <input class="button" type="submit" value="$i18n.getText('tutorial.common.add')" />
        </div>
    </div>

</form>
```

{{% tip %}}

Any item with a class of *cancel* will close the dialog when clicked.

{{% /tip %}}

On the view issue page, clicking your *triggering link* should display\_ Add Watchers(s) Form.

![](/server/jira/platform/images/picture-32.png)

## Step 10. Storing submitted values

Now that you have a form displaying correctly, the next step is to persist the submitted input.

To do that you will need to submit the *username's* to the *WatcherManager.*

``` javascript
public class AddWatchers extends AbstractIssueSelectAction
{
    private final WatcherManager watcherManager;
    private final BeanFactory beanFactory;
    private String[] watcherUserNames = new String[0];
    private List<String> validUsernames = new ArrayList<String>();

    public AddWatchers(final WatcherManager watcherManager, final BeanFactory beanFactory)
    {
        this.watcherManager = watcherManager;
        this.beanFactory = beanFactory;
    }


    @Override
    public String doDefault() throws Exception
    {
        return INPUT;
    }

    @Override
    public String doExecute() throws Exception
    {
        for (String validUsername : validUsernames)
        {
            watcherManager.startWatching(UserUtils.getUser(validUsername), getIssue());
        }

        return returnCompleteWithInlineRedirect("/browse/" + getIssueObject().getKey());
    }

    public void setWatcherUserNames(String userNames) throws Exception
    {
        if(StringUtils.isNotBlank(userNames))
        {
            watcherUserNames = userNames.split(",", 10); // split into array of user names

        }
    }
}
```

After submitting your form with valid usernames, page should be refreshed and watchers added.

![](/server/jira/platform/images/picture-33.png)

## Step 10. Add Error Handling

If the user supplies an incorrect input you will want to return an error message to be displayed in the dialog.

To do this, you will want to:

1.  Add a **doValidation** method to your action.

    ``` javascript
    public class AddWatchers extends AbstractIssueSelectAction
    {
     ...
        @Override
        protected void doValidation()
        {
            for (String username : watcherUserNames)
            {
                username = username.trim();

                if (UserUtils.existsUser(username))
                {
                    validUsernames.add(username);
                }
                else
                {
                    addErrorMessage(beanFactory.getInstance(getRemoteUser()).getText("tutorial.errors.user", username));
                }
            }
        }
     ...
    }
    ```

2.  Update your view to display errors.

    ``` javascript
    #if ($action.hasAnyErrors())
    <div class="form-message error">
        #foreach ($error in $action.getErrorMessages())
            <p>$textutils.htmlEncode(${error})</p>
        #end
        #foreach ($error in $action.errors)
            <p>$textutils.htmlEncode(${error})</p>
        #end
    </div>
    #end
    ```

{{% tip %}}

Congratulations, that's it

Have a chocolate!

{{% /tip %}}

























































































































































































































































