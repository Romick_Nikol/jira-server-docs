---
aliases:
- /server/jira/platform/discontinuation-of-support-for-atlassian-ide-connectors-33730936.html
- /server/jira/platform/discontinuation-of-support-for-atlassian-ide-connectors-33730936.md
category: devguide
confluence_id: 33730936
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=33730936
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=33730936
date: '2017-12-08'
legacy_title: Discontinuation of support for Atlassian IDE Connectors
platform: server
product: jira
subcategory: updates
title: Discontinuation of support for Atlassian IDE Connectors
---
# Discontinuation of support for Atlassian IDE Connectors

## We plan to discontinue support for the Atlassian IDE Connectors

Four years ago Jens Schumacher, the Group Product Manager of Development Tools at Atlassian, <a href="http://blogs.atlassian.com/2011/02/atlassian_ide_connector_30/" class="external-link">wrote a blog post</a> where he explained our motivation behind starting the open source <a href="http://www.atlassian.com/software/ideconnector" class="external-link">Atlassian IDE Connector</a> project. We had a goal of delivering a faster and more convenient way to interact with Atlassian applications.

Since we started that effort, a lot has changed. The web and our products have evolved, and as a consequence, IDE Connectors are duplicating functionality that is already available today in Atlassian products.

Over the last several years, we have performed a full redesign of all Atlassian products' UI to match the <a href="https://design.atlassian.com/latest/" class="external-link">Atlassian Design Guidelines</a>. We made the flows more consistent, and we have tied many of our products much closer together. At the same time we have invested a huge effort into product APIs, and have created [a whole new add-on platform](https://developer.atlassian.com/static/connect/docs/latest/index.html) to support all those cases where functionalities could not be provided within the products directly, or integration with external systems had to be assured.

There is still a lot of work ahead of us in order to make our products and respective platforms stronger. For that very reason we have decided to **discontinue the support of IDE connectors**. The product will remain available as **open source**, and will continue to be hosted on our Bitbucket servers.

## What exactly is changing?

The existing released versions of IDE connectors are the last ones released from Atlassian. We also plan to discontinue the customer support and Atlassian Answers effort related to IDE Connectors.

Specifically:

-   **IDE Connector for Eclipse**, the last version released from Atlassian on **27 April 2015** is <a href="https://confluence.atlassian.com/display/IDEPLUGIN/Atlassian+Connector+for+Eclipse+-+v3.2.5+Release+Notes" class="external-link">3.2.5</a>. Its <a href="https://support.atlassian.com/browse/ECSP" class="external-link">support project on SAC</a> and the <a href="https://ecosystem.atlassian.net/projects/PLE" class="external-link">development backlog project on EAN</a> will be closed.
-   **IDE Connector for IntelliJ**, the last version released from Atlassian on **20 April 2015** is <a href="https://confluence.atlassian.com/display/IDEPLUGIN/Atlassian+Connector+for+IntelliJ+IDEA+-+v3.0.16+Release+Notes" class="external-link">3.0.16</a>. Its <a href="https://support.atlassian.com/browse/IDESP" class="external-link">support project on SAC</a> and the <a href="https://ecosystem.atlassian.net/projects/PL" class="external-link">development backlog project on EAN</a>  will be closed.
-   **IDE Connector for Visual Studio**, the last version released from Atlassian on **10** **April 2015** is <a href="https://confluence.atlassian.com/display/IDEPLUGIN/Atlassian+Connector+for+Visual+Studio+-+1.3.12+Release+Notes" class="external-link">1.3.12</a>. Its <a href="https://support.atlassian.com/browse/IDESP" class="external-link">support project on SAC</a> and the <a href="https://ecosystem.atlassian.net/projects/PLVS" class="external-link">development backlog project on EAN</a>  will be closed.

## Here is an opportunity for you

Do you want to pick this project from Atlassian? Let us know!

It's a chance for you to own part of Atlassian history and carry the baton. By acquiring Atlassian IDE Connector, you immediately get access to its existing customer base. This in turn may lead to setting up new consulting engagements, to custom plugin development or access of previously inaccessible markets.

What is the potential impact? We are in the process of building a more robust version of in-built analytics, so reach out to us if you want to learn the preliminary **customer usage** numbers. When it comes to raw downloads, the numbers are as follows:

| IDE connector |  average monthly downloads for last 6 months|
|---------------|--------------------------------------------:|
| Eclipse       |                                         2807|
| IntelliJ      |                                         2209|
| VisualStudio  |                                          633|

## When is the deadline?

We plan to announce our support discontinuation decision publicly on **June 1st 2015**. At that time when we also will roll out the official blog post on http://blogs.atlassian.com, as well as change the settings in associated JIRA projects on the Atlassian Marketplace (http://marketplace.atlassian.com) and on Atlassians' main web site (http://www.atlassian.com).

## Any questions?

Are you interested? Do you want to learn more about this opportunity? Please feel free to get in touch with us. Contact Bartek Gatz (Product Manager at Atlassian) directly by sending an email to: bgatz at atlassian dot com or leave a comment under this page.
